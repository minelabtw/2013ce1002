package ce1002.f.s102502008;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MyFrame extends JFrame
{
	JLabel jlbCount =new JLabel("cars: 0") ;
	JButton jbtAdd =new JButton("Add car") ;
	JButton jbtStart =new JButton("Start Race") ;
	int count=0 ;
	CarRace carRace= new CarRace() ;
	Picture picture =new Picture(carRace) ;	
	public MyFrame()
	{
		this.setLayout(null) ;
		carRace.setFrame(this) ;
		jbtAdd.setBounds(0,0,150,50) ;
		jbtStart.setBounds(0,60,150,50) ;
		jlbCount.setBounds(160,0,150,50) ;
		jbtAdd.addActionListener(new addListener()) ;
		jbtStart.addActionListener(new startListener()) ;
		
		this.add(jbtAdd) ;
		this.add(jbtStart) ;
		this.add(jlbCount) ;
		this.add(picture) ;
		this.setSize(600,800) ;
		this.setVisible(true) ;
	}
	class addListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
		// TODO Auto-generated method stub
			Car car = new Car(count) ;
			carRace.add(car) ;
			count++ ;
			jlbCount.setText("cars: "+count) ;
			
		}
	}
	class startListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if(count!=0)
			{
			Thread thread =new Thread(carRace);
			thread.start() ;
			}
			else
			{
				JPanel panel= new JPanel() ;
				panel.add(new JLabel("There must be at least one car!")) ;
				JFrame frame =new JFrame("JOptionPane showMessage Dialog") ;
				JOptionPane.showMessageDialog(frame, panel) ;
			}
		}
	}
}
