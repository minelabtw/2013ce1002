package ce1002.f.s102502008;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Picture extends JPanel
{
	CarRace carRace ;
	public Picture(CarRace carRace)
	{
		this.carRace=carRace ;
		this.setBounds(0,170,400,400) ;
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponents(g) ;
		for(int i=0 ;i<carRace.list.size() ; i++)
		{
			g.fillRect(0, i*50,(int)(carRace.list.get(i).v)*2 , 20) ;
		}
		
	}
}
