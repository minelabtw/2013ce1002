package ce1002.f.s102502515;

import java.util.Random;

public class Car {
	 
	public int id;
	public float distance;
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed() {// return a random
		Random r = new Random();
		return r.nextFloat() * 40 + 80; //return a number ranging frome 80 to 120
	}
 
	@Override
	public String toString() {
		return "Car no." + id;//return string
	}
	

}