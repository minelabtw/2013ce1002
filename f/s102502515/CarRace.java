package ce1002.f.s102502515;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {// 實作Runnable才能放進thread
	 
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();//Name id the order they are added
		cars.add(car);
	}
 
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isFinished) {// if isFinished is false, run
			runCars();
		}
	}
 
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();;// Car proceeds
			 //System.out.println(car.toString() + ", distance=" +
			 //car.distance);
			if (car.distance >= totalDistance) {//if car arrives at destination
				isFinished = true;// stop loop
				frame.showWinner(car);//show the dialog
				break;//leave the for loop
			}
		}
	}
 
	public int carNum() {
		return cars.size();//the car number is the List size
	}
}