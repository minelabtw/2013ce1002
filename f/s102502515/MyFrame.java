package ce1002.f.s102502515;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {// 主視窗
	JButton startButton;
	JButton addButton;
	CarRace carRace;
	JLabel carsCount;
 
	public MyFrame() throws HeadlessException {
		//MyFrame settings
		setTitle("F-102502515");
		setBounds(0, 0, 700, 700);
		setLayout(null);
		
		//new Components
		carRace = new CarRace(this);
		startButton = new JButton();
		addButton = new JButton();
		carsCount = new JLabel();
		
		//Components settings
		startButton.setText("Start race");
		startButton.setBounds(0, 50, 120, 40);
		addButton.setText("Add car");
		addButton.setBounds(0, 0, 120, 40);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		
		//add Components
		add(addButton);
		add(startButton);
		add(carsCount);
		
		//addActionListener
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {// When press Add Button
		carRace.addCar();// add one carr
		carsCount.setText("cars:" + carRace.carNum());// renew label
	}
 
	public void showWinner(Car car) {//shoe winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);// dialog
	}
 
	class startListener implements ActionListener {//When startRace button pressed
		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {// At least one car
				Thread thread = new Thread(carRace);// create thread
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}	
}
