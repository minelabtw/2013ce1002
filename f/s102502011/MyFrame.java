package ce1002.f.s102502011;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	JButton addButton = new JButton("Add car");
	JButton startButton = new JButton("Start race");
	JLabel carsCount = new JLabel();
	CarRace carRace =  new CarRace(this); // 讓carRace可以呼叫frame的method:showWinner(Car car)

	public MyFrame() {
		setTitle("F-102502511");
		setLayout(null);
		setSize(300, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addButton.setBounds(5, 5, 100, 30);
		addButton.addActionListener(this);
		add(addButton);
		
		startButton.setBounds(5, 45, 100, 30);
		startButton.addActionListener(this);
		add(startButton);
		
		carsCount.setBounds(140, 20, 100, 20);
		carsCount.setText("cars:" + carRace.carNum());
		add(carsCount);

		setVisible(true);
	}

	public void showWinner(Car car) { // 第n台車勝利 
		JOptionPane.showMessageDialog(null, "The winner is " + car.toString()
				+ "!", "訊息", JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == addButton) {
			carRace.addCar();
			carsCount.setText("cars:" + carRace.carNum());
		} else if (e.getSource() == startButton) {
			if (carRace.carNum() <= 0) {
				/* 必須至少一台車參加比賽 */
				JOptionPane.showMessageDialog(null,
						"There must be at least one car!", "訊息",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				Thread thread = new Thread(carRace);  //加入執行緒
				thread.start();
			}
		}
	}

}
