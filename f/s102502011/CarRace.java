package ce1002.f.s102502011;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars;
	float totalDistance = 20000;
	boolean isFinished = false;

	public CarRace(MyFrame frame) {
		/* 讓所使用frame的method showWinner(Car car)可以被呼叫 */
		this.frame = frame;
		cars = new ArrayList<Car>();
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}

	public void run() {
		while (!isFinished) {
			runCars();
		}
	}

	private void runCars() {
		for (int i = 0; i < cars.size(); i++) {
			/* 將list裡的每台車加上隨機速度 */
			cars.get(i).distance += cars.get(i).randomSpeed();
			if (cars.get(i).distance >= totalDistance) {
				isFinished = true; // 如果抵達終點則回傳true
				frame.showWinner(cars.get(i));

				/* 跳出迴圈,避免有多台車同時超過終點 */
				break;
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

}
