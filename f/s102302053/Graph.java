package ce1002.f.s102302053;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Graph extends JPanel {//圖形
	int length = 0;
	int gpostition = 0;
	
	Graph(float x,int y){
		setLayout(null);
		length = (int)x;
		gpostition = y*80;
	}
	
	@Override	
	public void paintComponent(Graphics g){//畫地圖
		super.paintComponent(g);
		
		g.setColor(Color.black);
		g.fillRect(10, gpostition, length, 30);
		
	}
	

}
