package ce1002.f.s102302053;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	JButton addCar = new JButton("Add car");
	JButton startRace = new JButton("Start Race");
	JLabel carsCounter = new JLabel();//顯示當前車輛數
	CarRace carRace = new CarRace();
	JFrame mainFrame = new JFrame("F-102302053");
	
	MyFrame(){//建構主視窗
		mainFrame.setLayout(null);
		mainFrame.setSize(700, 800);
		mainFrame.setVisible(true);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setAddCar();
		mainFrame.add(addCar);
		setStartRace();
		mainFrame.add(startRace);
		setCarsCounter();
		mainFrame.add(carsCounter);
	
	}
	
	public void setAddCar(){//增加車子按鈕設定
		addCar.setBounds(10,10,100,50);
		addCar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				carRace.addCar();
				JPanel graph = new Graph(100,carRace.returnCarsNum());
				graph.setBounds(10, carRace.returnCarsNum()*80+100, 800, 50);
				mainFrame.add(graph);
				carsCounter.setText("cars:"+carRace.returnCarsNum());
			}
		});
	}
	public void setStartRace(){//賽車紐設定
		startRace.setBounds(10,80,100,50);
		startRace.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(carRace.returnCarsNum()>0){
					Thread thread1 = new Thread(carRace);
					thread1.start();
				}
				else if (carRace.returnCarsNum()<=0){
					JFrame failtoStart = new JFrame("訊息");
					JLabel text = new JLabel("There must be at least one car!");
					JOptionPane.showMessageDialog(failtoStart, text);
				}
			}
		});
		
	}
	public void setCarsCounter(){//計算車子數量
		carsCounter.setBounds(140,10,100,60);
		carsCounter.setText("cars:0" );
		
		
	}
}

