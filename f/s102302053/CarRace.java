package ce1002.f.s102302053;

import java.awt.Frame;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CarRace implements Runnable  {
	Frame frame ;
	List<Car> cars =new ArrayList<Car>();
	float totalDistance = 20000;
	boolean finish = false; 
	public void addCar(){//增加車子
		Car newCar = new Car();
		newCar.carNo = cars.size();
		cars.add(newCar);
	}
	public int returnCarsNum(){//回傳車輛數
		return(cars.size());
	}
	public void race(){//賽車
		int temp1 ;
		int temp2 = cars.size();
		for(temp1 = 0; temp1 < temp2; temp1++){
			float temp3 = cars.get(temp1).Speed();
			cars.get(temp1).postition += temp3;
			//JPanel speedgraph =new Graph(temp3,cars.get(temp1).carNo);
			//speedgraph.setBounds(10,cars.get(temp1).carNo*80,600,30);
			//frame.add(speedgraph);
			if(cars.get(temp1).postition > totalDistance){
				finish = true;
				showWinner(cars.get(temp1));
				break;
			}
		}
	
	}
	public void showWinner(Car car){//顯示勝利者
		JFrame showWinner = new JFrame("訊息");
		JLabel text = new JLabel("The winner is " + car.returnCar()+"!");
		JOptionPane.showMessageDialog(showWinner, text);
	}
	public void run(){
		Timer timer = new Timer();
        timer.schedule(new racing(), 100);
        }
	class racing extends TimerTask{
	@Override
	public void run() {//沒有車子抵達終點,就不斷執行
		while(finish == false){
			race();
		}
		} 
	}

	
}



