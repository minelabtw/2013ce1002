package ce1002.f.s102502013;

import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener{

	JButton startButton;
	JButton addButton;
	CarRace carRace;
	JLabel carsCount;
 
	public MyFrame() throws HeadlessException {
		setTitle("F-102502013");
		setBounds(0, 0, 600, 600);
		setBackground(Color.WHITE);
		setLayout(null);
		
		carRace = new CarRace(this);
		startButton = new JButton();
		addButton = new JButton();
		carsCount = new JLabel();
		startButton.setText("Start race");
		startButton.setBounds(0, 50, 120, 40);
		addButton.setText("Add Car");
		addButton.setBounds(0, 0, 120, 40);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(addButton);
		add(startButton);
		add(carsCount);
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {// 當按下addButton
		carRace.addCar();// 增加car
		carsCount.setText("cars:" + carRace.carNum());// 更新label
}
	public void showWinner(Car car) {// 顯示winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);// 對話框
	}
 
	class startListener implements ActionListener {// 處理按下startButton事件
 
		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {// 至少要有一輛
				Thread thread = new Thread(carRace);// 建立thread
				thread.start();
				try {
					thread.sleep(100);
					repaint();
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}
}
