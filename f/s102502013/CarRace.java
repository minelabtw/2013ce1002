package ce1002.f.s102502013;

import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

public class CarRace extends JPanel implements Runnable{

	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();// id設為加入順序
		cars.add(car);
	}
 
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (isFinished == false) {// 只要isFinished==false就會一直跑
			runCars();
		}
	}
 
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();// Car前進
			if (car.distance >= totalDistance) {// 過終點線
				isFinished = true;// 停止loop
				frame.showWinner(car);
			}
		}
	}
 
	public int carNum() {
		return cars.size();

	}
	protected void paintComponent(Graphics g, Car car){
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(5, 120, car.randomSpeed(), 10);
	}
	
}
