package ce1002.f.s102502013;

import java.util.Random;

public class Car {

	public int id;
	public float distance;
 
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public int getCarId(){
		return id;
	}
	public int randomSpeed() {// 回傳80~120的隨機數
		Random r = new Random();
		int speed = r.nextInt(120);
		while(speed<80){
			speed = r.nextInt(120);
		}
		return speed;
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}
}
