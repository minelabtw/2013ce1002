package ce1002.f.s102502522;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Random;

import javax.swing.*;

public class Final extends JFrame implements ActionListener{
    static Random ran=new Random();
	static Final frame=new Final();
	static int car=0;//車子數
	static Label car_=new Label("cars:"+car);
    static Button addcar=new Button("Add car");
    static Button start=new Button("Start race");
    static Label carlab[]=new Label[100];
    static int speed[]=new int[100];
    static int distance[]=new int[100];
    static Timer go=new Timer(100,frame);//開始跑
    static JOptionPane o=new JOptionPane(); 
	public static void main(String args[])//frame的設定
	{
		frame.setLayout(null);
    	frame.setBounds(0,0,1000,700);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setVisible(true);  
    	frame.setstart();
	}
	public void setstart()//各種設定
	{
		addcar.setBounds(0,0,200,50);
		start.setBounds(0,80,200,50);
		car_.setBounds(250,0,150,50);
		addcar.setFont(new Font("Arial",Font.ITALIC,20));
		start.setFont(new Font("Arial",Font.ITALIC,20));
		car_.setFont(new Font("Arial",Font.ITALIC,20));
		frame.add(car_);
		frame.add(addcar);
		frame.add(start);
		addcar.addActionListener(frame);
		start.addActionListener(frame);
	}
	@Override
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==start)
		{
			if(car==0)
			{
				o.showConfirmDialog(this,"There must be at least one car!","訊息",JOptionPane.CLOSED_OPTION);
			}
			else
			{
				System.out.println("dd");
				for(int account=0;account<=car-1;account++)
				{
					speed[account]=0;
					carlab[account]=new Label();
					carlab[account].setBackground(Color.black);
					carlab[account].setBounds(0,250+25*account,speed[account],15);
					frame.add(carlab[account]);
					distance[account]=0;
					go.start();
				}
				
				
			}
		}
		else if(e.getSource()==addcar)
		{
			car++;
			car_.setText("cars:"+car);
		}
		else if(e.getSource()==go)
		{
			for(int account=0;account<=car-1;account++)
			{
				speed[account]=(ran.nextInt(41)+80)*7;
				carlab[account].setBounds(0,250+25*account,speed[account],15);
				distance[account]=distance[account]+speed[account]/7;
				if(distance[account]>=20000)
				{
					go.stop();
					o.showConfirmDialog(this,"TheWinner is Car no."+account+1+" !","訊息",JOptionPane.CLOSED_OPTION);
				}
				
			}
			
		}
		
	}
}
