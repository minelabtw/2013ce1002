package ce1002.f.s102502017;

import java.awt.Color;
import java.awt.event.*;

import javax.swing.*;

public class MyFrame extends JFrame{
	
	JButton addCar;
	JButton startCar;
	JLabel countCar;
	CarRace carRace;
	JPanel carSpeed;
	
	Timer timer = new Timer(100,new TimerListener());
	
	MyFrame(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setTitle("A14-102502017");
		//this.setLocationRelativeTo(null);
		this.setBackground(Color.white);
		newMembers();
		
		//tiny tiny..
		//ah.. show the student number
		this.setSize(400+64, 700);
		this.setVisible(true);
	}
	
	void newMembers(){
		ActionListener listener;
        carRace = new CarRace();

        addCar = new JButton("add car");
        listener = new addCar();
        addCar.addActionListener(listener);
        addCar.setBounds(5, 5, 160, 60);
        add(addCar);

        startCar = new JButton("start");
        listener = new startRace();
        startCar.addActionListener(listener);
        startCar.setBounds(5, 70, 160, 60);
        add(startCar);

        countCar = new JLabel("cars: 0");
        countCar.setBounds(180, 5, 160, 60);
        add(countCar);
        
        carSpeed = new Racing(carRace);
        carSpeed.setBounds(5,200,500,5000000);
        add(carSpeed);
	}
	
	class addCar implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			carRace.add_car();
            int count = carRace.carNum();
            countCar.setText("cars: "+count);
		}
	}
	
	class startRace implements ActionListener{

		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
            Thread racing = new Thread(carRace);
            if(carRace.carNum() > 0) {
                racing.start();
                timer.start();
            } else {
                JOptionPane.showMessageDialog(null, "There must be at least one car!", "No Car", JOptionPane.ERROR_MESSAGE);
            }	
		}
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			// 	TODO Auto-generated method stub
			repaint();
			}
	}
}
