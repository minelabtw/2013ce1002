package ce1002.f.s102502017;

import java.awt.*;
import javax.swing.*;



public class Racing extends JPanel{
	
	int[] carspeed ;
	int[] carDistance;
	CarRace carRace;
	
	
	Racing(CarRace carRace){
		this.carRace = carRace;
	}
	
			
	public void paint(Graphics g){
		super.paint(g);
		//draw the Rect for the balls
		g.setColor(new Color(0,0,0,0.7f));
		g.fillRect(0, carRace.carNum()*20 +45, 400, carRace.carNum()*20);
		g.setColor(Color.black);
		//the speeds
		for(Car car : CarRace.cars){
			g.fillRect(0,20*car.id, (int)(car.speed) * 3, 10);
		}
		//draw the white ball
		g.setColor(Color.white);
		for(Car car:CarRace.cars)
			g.fillOval((int)(car.distance/50), carRace.carNum()*20 + 50 + 20*car.id, 10, 10);
	}
	
	
}
