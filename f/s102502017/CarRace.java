package ce1002.f.s102502017;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

class CarRace implements Runnable {

    static public List<Car> cars = new ArrayList<Car>();
    float totalDistance = 20000;
    
    static boolean isWin = false;
    //constructor?
    CarRace(){}

    //add a car
    public void add_car() {
        int counter = carNum();
        Car car = new Car(counter);
        cars.add(car);
    }

    //run thread
    public void run() {
        for(Car car : cars) {
            car.distance = 0;
        }

        race_start();
    }

    private void race_start() {
        while(true) {
            for(Car car : cars) {
                car.distance += car.randomSpeed();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(car.distance > totalDistance) {
                    show_winner(car);
                    isWin = true;
                    return ;
                }
            }
        }
    }

    //total of cars
    public int carNum() {
        return cars.size();
    }

    //show winner car
    public void show_winner(Car car) {
        JOptionPane.showMessageDialog(null, "The winner is "+car.getCarName()+"!", "Race Finish", JOptionPane.INFORMATION_MESSAGE);
    }
}
