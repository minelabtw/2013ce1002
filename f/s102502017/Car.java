package ce1002.f.s102502017;

public class Car {
	
	public int id;
    public float distance;
    public float speed;

    //set id
    Car(int id) {
        this.id = id;
    }

    //80 + 40*x
    public float randomSpeed() {
    	speed =(float)(Math.random()*40)+80; 
        return speed;
    }

    //toString ? toString : getCarName;
    public String getCarName() {
        return "Car no." + id;
    }

}
