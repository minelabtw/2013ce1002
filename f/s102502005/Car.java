package ce1002.f.s102502005;

import java.util.Random;

public class Car {
	public int id;
	public float distance = 0;
	public int currentSpd = 0;
	private static Random random = new Random();

	public int randomSpeed() {
		currentSpd = random.nextInt() % 41;// random出 -40~40 的int。
		if (currentSpd < 0) {
			currentSpd *= -1;// 假如為負數就要乘上-1變正數。
		}
		currentSpd += 80;// 最後再加上80就會是80~120的隨機數。
		return currentSpd;
	}

	public String toString() {
		return "Car no." + id;
	}
}
