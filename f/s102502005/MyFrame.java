package ce1002.f.s102502005;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {

	JButton addButton = new JButton("add car");
	JButton startButton = new JButton("start");
	JLabel carsCount = new JLabel("cars: 0");
	CarRace carRace;

	public MyFrame() {

		// 基本視窗設定。
		setLayout(null);
		setTitle("F-102502005");
		setSize(1200, 860);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);

		// 元件設定。
		addButton.setBounds(0, 0, 100, 30);
		startButton.setBounds(0, 40, 100, 30);
		carsCount.setBounds(120, 0, 100, 30);

		addButton.addActionListener(this);
		startButton.addActionListener(this);

		add(addButton);
		add(carsCount);
		add(startButton);

		// 建立carRace。
		carRace = new CarRace(this);

	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(this, "The winner is " + car.toString()
				+ "!");
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == addButton) {
			carRace.addCar();
			carsCount.setText("cars: " + carRace.carNum());
		}

		else if (e.getSource() == startButton) {
			if (carRace.carNum() <= 0) {
				JOptionPane.showMessageDialog(this,
						"There must be at least one car!");
			} else {
				Thread thread = new Thread(carRace);
				thread.start();
			}

		}

	}

	public void paint(Graphics g) {
		super.paint(g);

		// car speed
		g.setColor(Color.BLUE);
		for (int i = 0; i < carRace.carNum(); i++) {
			g.fillRect(0, 100 + i * 10, carRace.getCar(i).currentSpd * 6, 9);// 第i台賽車的速度矩形左上角位於frame上的(0,100+i*10)。
		}

		// race course
		g.setColor(Color.GRAY);// 有幾台車就有幾個跑道，跑道的左上角在速度矩形左上角的下方50px處。
		g.fillRect(0, 150 + carRace.carNum() * 10, 1000, carRace.carNum() * 10);

		// race car
		g.setColor(Color.RED);
		for (int i = 0; i < carRace.carNum(); i++) {
			g.fillOval((int) carRace.getCar(i).distance / 20,
					150 + carRace.carNum() * 10 + i * 10, 10, 10);
		}
	}

}
