package ce1002.f.s102502005;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {

	MyFrame frame;
	List<Car> cars;
	float totalDistance = 20000;
	boolean isFinished = false;

	public CarRace(MyFrame frame) {
		this.frame = frame;
		cars = new ArrayList<Car>();
	}

	public void addCar() {
		Car temp = new Car();
		temp.id = cars.size();
		cars.add(temp);
	}

	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();
			if (car.distance >= totalDistance) {
				frame.showWinner(car);
				isFinished = true;
				return;
			}
		}
	}

	public Car getCar(int index) {
		return this.cars.get(index);
	}

	public int carNum() {
		return cars.size();
	}

	@Override
	public void run() {
		long beforeTime;
		long timeDiff;
		long sleep;

		beforeTime = System.currentTimeMillis();// 汽車開始跑第一步時的時間戳章。

		while (!isFinished) {
			runCars();
			frame.repaint();

			timeDiff = System.currentTimeMillis() - beforeTime;// 花了多少時間跑一步。
			sleep = 100 - timeDiff;// Thread要睡多久。

			if (sleep > 0) {
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			beforeTime = System.currentTimeMillis();
		}

	}
}
