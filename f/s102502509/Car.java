package ce1002.f.s102502509;

import java.util.Random;

public class Car 
{
	public int id;
	public float distance;
	public int randomspeed;//create the variable for random
	public String win;
	
	Car(int id)
	{
		this.id = id;// constructor
	}
	
	public float randomSpeed()
	{
		Random ran = new Random();
		randomspeed = (ran.nextInt(41)) + 80;
		return randomspeed;
	}
	
	public String toString()
	{
		win = "The winner is " + id + " !"; // use id to store the message
		return win;
	}
}