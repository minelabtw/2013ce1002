package ce1002.f.s102502509;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class MyFrame extends JFrame implements ActionListener
{
	private JButton jb1 = new JButton("add car");
	private JButton jb2 = new JButton("start");
	private JLabel lb1 = new JLabel();
	CarRace carRace = new CarRace(this);
	
	MyFrame()
	{
		super("F-102502509");
		setLayout(null);
		setBounds(400, 160, 600, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		jb1.setBounds(15, 10, 90, 40);
		jb2.setBounds(15, 100, 90, 40);
		lb1.setBounds(200, 50, 300, 30);
		lb1.setText("cars:0");
		
		add(jb1);
		add(jb2);
		add(lb1);
		
		jb1.addActionListener(this);
		jb2.addActionListener(this);
		
	}
	
	public void showWinner(Car car)
	{
		JOptionPane.showMessageDialog(this, car.toString());
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// TODO Auto-generated method stub
		String buttonName = event.getActionCommand();
		if(buttonName.equals("add car"))
		{
			// call the carRace's function
			carRace.addCar();
			lb1.setText("cars:" + carRace.carNum());
		}
		else if(buttonName.equals("start"))
		{
			if(carRace.carNum() == 0)
			{// show the dialog box
				JOptionPane.showMessageDialog(this, "There must be at least one car!");
			}
			else 
			{// thread to run class
				Thread work = new Thread(carRace);
				work.start();
			}
			
		}
	}
}