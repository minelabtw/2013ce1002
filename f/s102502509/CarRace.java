package ce1002.f.s102502509;

import java.awt.Graphics;
import java.awt.Paint;
import java.util.ArrayList;

public class CarRace implements Runnable
{
	MyFrame mf;
	ArrayList<Car> cars = new ArrayList<Car>();// store the cars into the list
	
	float totalDistence = 5000;
	boolean isFinish = false;
	
	CarRace(MyFrame mf)
	{
		this.mf = mf;
	}

	@Override
	public void run() 
	{
		while(isFinish == false)
		{
			runCars();
		}
	}
	
	public void addCar()
	{
		cars.add(new Car(carNum()));
	}
	
	private void runCars()
	{
		for(int i = 0; i < carNum(); i++) // car run with random speed
		{
			cars.get(i).distance += cars.get(i).randomSpeed();
		}
		
		for(int i = 0; i < carNum(); i++)
		{
			if(cars.get(i).distance >= totalDistence)
			{
				mf.showWinner(cars.get(i));
				isFinish = true;// remember turn to true to stop the message
				break;
			}
		}
		
	}
	public void paint(Graphics g)
	{
		
		for(int i = 0; i < carNum(); i++)
		{
			g.fillRect(15, 150 + i * 25, (int)(cars.get(i).randomSpeed()), 20);
		}
		
		
	}
	public int carNum() // return the carNum 
	{
		return cars.size();
	}
	
	
}

