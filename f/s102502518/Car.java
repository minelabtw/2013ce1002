package ce1002.f.s102502518;

import java.util.Random;

public class Car {
  public int id;
  public float distance;
  
  public Car() {
    this.distance = 0.0F;
    this.id = 0;
  }
  
  public float randomSpeed() {//random number as the speed
    Random r = new Random();
    return r.nextFloat() * 40.0F+80;
  }
  
  public String toString() {//"Car no."+id 
    return "Car no." + this.id;
  }
}