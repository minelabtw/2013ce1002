package ce1002.f.s102502518;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;

	public float GettotalDistance() {
		return totalDistance;
	}
	public CarRace(MyFrame f) {
		this.frame = f;
		this.isFinished = false;
		this.totalDistance = 20000.0F;
		this.cars = new ArrayList();
	}

	public void addCar() {
		Car car = new Car();
		car.id = this.cars.size();
		this.cars.add(car);
	}

	public void run() {//override
		while (!this.isFinished) {
			runCars();
		}
	}

	private void runCars() {
		for (Car car : this.cars) {// if car's distance < total distance
			car.distance += car.randomSpeed();
			System.out.println(car.toString() + ", distance=" + car.distance);
			if (car.distance >= this.totalDistance) {
				this.isFinished = true;
				this.frame.showWinner(car);
			}
		}
	}

	public int carNum() {// car's number
		return this.cars.size();
	}
}
