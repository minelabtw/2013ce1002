package ce1002.f.s102502518;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame
  extends JFrame
  implements ActionListener
{
  JButton button;
  JButton add;
  CarRace race;
  JLabel count;
  
  
  public MyFrame()
    throws HeadlessException {//frame content
    setTitle("F-102502518");
    setBounds(0, 0, 150, 90);
    setLayout(null);
    this.race = new CarRace(this);
    this.button = new JButton();
    this.add = new JButton();
    this.count = new JLabel();
    this.button.setText("Start race");
    this.button.setBounds(0, 30, 100, 20);
    this.add.setText("Add car");
    this.add.setBounds(0, 0, 80, 20);
    this.count.setText("cars:" + this.race.carNum());
    this.count.setBounds(90, 0, 90, 40);
    add(this.add);
    add(this.button);
    add(this.count);
    this.add.addActionListener(this);
    this.button.addActionListener(new startListener());
  }
  
  public void actionPerformed(ActionEvent e) {//override
    this.race.addCar();
    this.count.setText("cars:" + this.race.carNum());
  }
  
  public void showWinner(Car car) {//show who is winner
    String msg = "The winner is " + car.toString() + "!";
    JOptionPane.showMessageDialog(this, msg);
  }
  
  class startListener implements ActionListener {//startListener
    startListener() {
    }
    
    public void actionPerformed(ActionEvent e) {
      if (MyFrame.this.race.carNum() > 0) {
        Thread thread = new Thread(MyFrame.this.race);
        thread.start();
      }
      else {
        JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
      }
    }
  }
}