package ce1002.f.s102502024;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class Frame extends JFrame{
	private JButton add=new JButton("Add car");
	private JButton start=new JButton("Start race");
	private JLabel carsCount=new JLabel("cars:0");
	CarRace race;
	
	Frame() throws  HeadlessException
	{
		setLayout(null);
		setTitle("F-102502024");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000,700);
		add.setBounds(0,0,150,50);
		start.setBounds(0,70,150,50);
		carsCount.setBounds(200,20,100,50);
		add.addActionListener(new addlistener());
		start.addActionListener(new startlistener());
		this.race=new CarRace(this);
		add(add);
		add(start);
		add(carsCount);
	}
	
	class addlistener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			race.addcar();
			carsCount.setText("cars:"+race.carnum());
		}
		
	}
	
	class startlistener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			if(race.carnum()>0)
			{
				Thread thread = new Thread(Frame.this.race);
		        thread.start();
			}
			else
			{
				JOptionPane.showMessageDialog(Frame.this, 
				          "There must be at least one car!");  //檢查車至少一輛
			}
		}
		
	}
	
	public void showWinner(Car car)
	{
		JOptionPane.showMessageDialog(null,"The winner is Car no.+"+car.toString()+"!");
	}
}
