package ce1002.f.s102502024;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;



public class CarRace implements Runnable{
	Frame frame;
	ArrayList<Car> arr = new ArrayList<Car>();
	float totalDistance;
	boolean isFinished=false;
	
	CarRace(Frame f)
	{
		this.frame=f;
		totalDistance=20000;
	}
	
	public void addcar()
	{
		Car car=new Car();
		car.id=arr.size();
		arr.add(car);
	}
	
	public int carnum()
	{
		return arr.size();
	}
	
	public void runcar()
	{
		for(Car car : this.arr)  //開始比賽
		{
			car.distance=car.distance+car.carspeed();
			System.out.println(car.toString() + ", distance=" + car.distance);
			if (car.distance >= this.totalDistance)
		      {
		        this.isFinished = true;
		        this.frame.showWinner(car);
		      }
		}
	}
	@Override
	public void run() {
		while(isFinished=false)
		{
			try{
			runcar();
			Thread.currentThread().sleep(100);
			}
			catch(InterruptedException ex){
				
			}
			}
		}
	}
