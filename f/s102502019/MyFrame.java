package ce1002.f.s102502019;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class MyFrame extends JFrame implements ActionListener{
	private JButton addButton = new JButton("add car");
	private JButton startButton = new JButton("start");
	private JLabel carsCount = new JLabel("cars:0");
	private CarRace carrace = new CarRace(this);
	private Graph graph = new Graph(this);

	MyFrame() {
	this.setTitle("A14-s102502019");// ����
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setSize(600, 600);
	this.setLayout(null);
	this.add(addButton);
	this.add(startButton);
	this.add(carsCount);
	this.add(graph);
	addButton.setSize(100, 50);
	addButton.setLocation(10, 10);
	addButton.addActionListener(this);
	startButton.setSize(100, 50);
	startButton.setLocation(10, 70);
	startButton.addActionListener(this);
	carsCount.setSize(100, 30);
	carsCount.setLocation(120, 25);
	graph.setBounds(10,150,300,200);
	this.setVisible(true);
	}

	public void showWinner(Car car) {
	JOptionPane.showMessageDialog(new JFrame(),
	"The winner is " + car.toString() + "!");
	}

	public void actionPerformed(ActionEvent e) {
	if (e.getSource() == addButton) {
	carrace.addCar();
	carsCount.setText("cars:" + carrace.carNum());
	}
	if (e.getSource() == startButton) {
	if (carrace.carNum() > 0) {
	new Thread(carrace).start();
	new Thread(graph).start();
	} else {//0 �N��
	JOptionPane.showMessageDialog(new JFrame(),
	"There must be at least one car!");
	}
	}
	}
}
