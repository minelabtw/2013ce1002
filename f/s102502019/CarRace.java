package ce1002.f.s102502019;

import java.util.ArrayList;
import java.util.List;
public class CarRace implements Runnable{
	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();// 初始化LIST
	float totalDistance = 20000;
	boolean isFinished = false;
	int carid = 0;

	CarRace(MyFrame a) {
	frame = a;
	}

	public void addCar() {
	cars.add(new Car(carid++));// 家東西
	}

	private void runCars() {
	for (int i = 0; i < cars.size(); i++) {// 每台車迴圈
	Car a = cars.get(i);// 給車
	a.distance = a.distance + a.randomSpeed();// 家距離
	if (a.distance > totalDistance) {//超過就勝利
	isFinished = true;
	frame.showWinner(a);
	}
	}
	}

	public int carNum() {
	return cars.size();
	}

	@Override
	public void run() {
	// TODO Auto-generated method stub
		
	while (!isFinished) {
	runCars();
	try {
		Thread.sleep(100);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	}
}
