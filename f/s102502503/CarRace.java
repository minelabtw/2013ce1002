package ce1002.f.s102502503;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars;
	float totaldis = 20000;  //總里程數20000
	boolean isFinished = false;  //初始為false

	public CarRace(MyFrame frame) {
		
		this.frame = frame;
		cars = new ArrayList<Car>();
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}

	public void run() {
		while (!isFinished) {
			runCars();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void runCars() {
		for (int i = 0; i < cars.size(); i++) {
			
			cars.get(i).dis+=cars.get(i).randomSpeed();
			if (cars.get(i).dis>= totaldis) {  //若大於總里程數
				isFinished = true; //設定為true
				frame.showWinner(cars.get(i));  //顯示贏家
				break;
			}
		}
	}

	public int carNum() {
		return cars.size(); 
	}

}
