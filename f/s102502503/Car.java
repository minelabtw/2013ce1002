package ce1002.f.s102502503;

import java.util.Random;

public class Car {
	public int id=0;
	public float dis=0;  //初始為0
	Random rans=new Random();
	public Car() {
	}
	
	public float randomSpeed(){
		return (float) (rans.nextInt(41)+80);  //回傳隨機速度
	}
	
	public String toString(){
		return "Car no." + id;  //回傳id
	}

}
