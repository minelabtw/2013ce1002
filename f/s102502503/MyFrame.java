package ce1002.f.s102502503;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	JButton addbut = new JButton("Add car");
	JButton startbut = new JButton("Start race");
	JLabel carsCount = new JLabel();
	CarRace carRace;

	public MyFrame() {
		super("F-102502503");  //視窗標題
		setLayout(null);
		setBounds(400,150,300,150); //設定大小位置
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		carRace = new CarRace(this);
		addbut.setBounds(20,20,100,30);  //增加按鈕的大小位置
		startbut.setBounds(20,60,100,30);  //開始按鈕的大小位置
		carsCount.setBounds(180,40,100,20);
		carsCount.setText("cars:" + carRace.carNum());

		add(addbut);
		add(startbut);
		add(carsCount);

		addbut.addActionListener(this);
		startbut.addActionListener(this);

		setVisible(true);
	}

	public void showWinner(Car car) {
		
		JOptionPane.showMessageDialog(null, "The winner is " + car.toString()+ "!", "訊息", JOptionPane.INFORMATION_MESSAGE);  //對話框
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == addbut) {  //若按了增加按鈕
			carRace.addCar();
			carsCount.setText("cars:" + carRace.carNum());  //設定標籤字串
		} 
		else if (e.getSource() == startbut) {  //若按了start按鈕
			if (carRace.carNum() <= 0) {
				
				JOptionPane.showMessageDialog(null,"There must be at least one car!", "訊息",JOptionPane.INFORMATION_MESSAGE);  //對話框
			} else {
				Thread t = new Thread(carRace); //多執行緒
				t.start();
			}
		}
	}
}