package ce1002.f.s101201522;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class SpeedGraph extends JPanel {
	private Car[] cars;
	
	SpeedGraph (Car[] cars) {//initial
		this.cars = cars;
	}
		
	@Override
	protected void paintComponent (Graphics g) {//draw graph
		super.paintComponent(g);
		g.setColor(Color.black);
		for (int i=0;i<cars.length;i++) {//draw speed graph
			g.fillRect(0, 15*i, cars[i].getSpeed()*4, 10);
		}
		
		g.setColor(Color.gray);
		g.fillRect(0, 15*cars.length+30, 400, 10*cars.length);
		g.setColor(Color.black);
		for (int i=0;i<cars.length;i++) {//draw car position
			g.fillOval(cars[i].getPosition()/50-5, 15*cars.length+30+10*i, 10, 10);
		}
	}
}
