package ce1002.f.s101201522;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.util.Timer;
import java.util.TimerTask;

public class Final extends JFrame {
	JButton add = new JButton("Add car");
	JButton start = new JButton("Start race");
	JLabel num_car = new JLabel("cars:0");
	private int num;//num of cars
	private Car[] cars;
	private SpeedGraph sg;
	private Timer timer = new Timer();
	private boolean win;
	private int winner;
	private int distance;//winner run distance
	
	Final () {//initial
		num = 0;
		winner = 0;
		distance = 0;
		win = false;
		setLayout(null);
		setContent();
		pack();
	}
	
	public void setContent () {//set component in frame
		add.setBounds(0, 0, 100, 30);
		add.addActionListener(new ActionListener () {//motion of pressing add

			@Override
			public void actionPerformed(ActionEvent e) {
				num++;
				num_car.setText("cars:"+num);
			}
			
		});
		add(add);
		
		start.setBounds(0, 40, 100, 30);
		start.addActionListener(new ActionListener () {//motion of pressing start

			@Override
			public void actionPerformed(ActionEvent e) {
				if (num <= 0) 
					JOptionPane.showMessageDialog(null, "There must be at least one car!");
				else {
					cars = new Car[num];
					for (int i=0;i<cars.length;i++) {
						cars[i] = new Car();
					}
					sg = new SpeedGraph(cars);
					sg.setBounds(0, 100, 500, 25*num+30);
					add(sg);
					timer.schedule(new TimerTask () {

						@Override
						public void run() {
							for (int i=0;i<cars.length;i++) {
								cars[i].changeSpeed();//update car's speed
								if (cars[i].getPosition() >= 20000) {//check winner
									win = true;
								}
								
								if (win && cars[i].getPosition()>distance) {//update winner
									distance = cars[i].getPosition();
									winner = i;
								}
							}
							repaint();
							
							if(win) {
								JOptionPane.showMessageDialog(null, "The winner is Car no."+winner+"!");
								cancel();
							}
						}
						
					}, 0, 100);
				}
			}
			
		});
		add(start);
		
		num_car.setBounds(150, 0, 100, 30);
		add(num_car);
	}
	
	public static void main(String[] args) {
		/*set frame*/
		Final frame = new Final();
		frame.setTitle("F-101201522");
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
