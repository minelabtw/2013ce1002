package ce1002.f.s101201522;

public class Car {
	private int speed;
	private int position;
	
	Car () {
		speed = 0;
		position = 0;
	}
	
	public int getSpeed () {//get car's speed
		return speed;
	}
	
	public void changeSpeed () {//change car's speed 80~120 in random and run
		speed = 80+(int)(Math.random()*40);
		position += speed;
	}
	
	public int getPosition () {//get car's position
		return position;
	}
}
