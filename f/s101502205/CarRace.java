package ce1002.f.s101502205;

import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class CarRace implements Runnable{
	
	private MyFrame frame;
	private List<Car> cars;
	private boolean isFinished;
	private MyPanel thePanel;
	
	public CarRace(MyFrame frame) {
		this.frame = frame;
		isFinished = false;
		cars = new ArrayList<Car>();
		
		// add new panel to the frame to paint the race status
		thePanel = new MyPanel(cars);
		this.frame.add(thePanel);
	}
	
	public void addCar() {
		// add new car
		cars.add(new Car(cars.size()));
	}
	
	public void run() {
		
		// initialize all distance and star new race
		isFinished = false;
		int n = cars.size();
		if (n>0){
			// set distance to zero
			for(int i=0; i < n; i++) {
				cars.get(i).setZeroDistance();
			}

			// let's race! go go power rangers!!
			while ( ! isFinished ) {
				runCars();
				// delay 0.1sec
				try{
					Thread.sleep(100);
				}catch(InterruptedException ie){
					ie.printStackTrace();
					System.out.println(ie.getMessage());
				}
				// repaint the race
				thePanel.repaint();
			}
			// show all winners at the end
			// there might be more than one winner
			for(int i=0; i<n; i++) {
				if (cars.get(i).isEnd()) {
					JOptionPane.showMessageDialog(null, "The winner is "+cars.get(i).toString()+"!");
				}
			}
		}else{
			// if the care is less than one
			// pop out the message
			JOptionPane.showMessageDialog(null, "There must be at least one car!");
		}
		
	}
	private void runCars() {
		int n = cars.size();
		
		for(int i=0; i < n; i++) {
			// add distance of all cars
			cars.get(i).randomSpeed();
			cars.get(i).gogo();
			if (cars.get(i).isEnd()) {
				isFinished = true;
				// don't break here
				// cause it may have two or more winner at same time
			}
		}
	}
	public int carNum() {
		// return number of cars
		return cars.size();
	}
}
