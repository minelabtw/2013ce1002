package ce1002.f.s101502205;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame{
	
	private JButton addButton;
	private JButton startButton;
	private JLabel carsCount;
	private CarRace carRace;
	
	public MyFrame() {
		
		// setting for frame
		setTitle("F-101502205");
		setSize(600, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		getContentPane().setLayout(null);
		
		// create carRace
		carRace = new CarRace(this);
		
		// create add button and listener
		addButton = new JButton("Add car");
		addButton.setBounds(10, 10, 100, 40);
		addButton.addActionListener(new addBtnListener());
		getContentPane().add(addButton);
		
		// create start button and listener
		startButton = new JButton("Start race");
		startButton.setBounds(10, 60, 100, 40);
		startButton.addActionListener(new startBtnListener());
		getContentPane().add(startButton);
		
		// add label
		carsCount = new JLabel();
		carsCount.setText("cars:"+carRace.carNum());
		carsCount.setBounds(140, 15, 100, 25);
		getContentPane().add(carsCount);
	}

	class addBtnListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			// add car
			carRace.addCar();
			// update the cars count
			carsCount.setText("cars:"+carRace.carNum());
		}
	}
	class startBtnListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			// create new thread and start
			Thread thread = new Thread(carRace);
			thread.start();
		}
	}
}
