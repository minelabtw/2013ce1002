package ce1002.f.s101502205;

import java.awt.Graphics;
import java.awt.Color;
import java.util.List;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	private List<Car> cars;
	private int width = 580;
	private int height = 450;
	
	public MyPanel(List<Car> cars) {
		// car informations
		this.cars = cars;
		// settings
		setBounds(0, 120, width, height);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int carNum = cars.size();
		double n;
		if (carNum != 0) {			// if the car is less than one, don't paint anything
			// draw speed bars
			g.setColor(Color.blue);
			for (int i=0; i < carNum; i++) {
				n = cars.get(i).getCurrentSpeed()*width/120;
				g.fillRect(1, 15*i, (int)n, 12);
			}
			
			// draw distance result
			g.setColor(Color.lightGray);
			g.fillRect(12, 15*carNum+20, (int)(0.8*width), 12*carNum);
			
			// plot the position of cars
			for (int i=0; i < carNum; i++) {
				g.setColor(Color.white);
				g.fillOval((int)(cars.get(i).getFinished()*0.8*width)+6, 15*carNum+20+i*12, 12, 12);
				g.setColor(Color.red);
				g.fillOval((int)(cars.get(i).getFinished()*0.8*width)+7, 15*carNum+20+i*12+1, 10, 10);
			}
		}
	}
}
