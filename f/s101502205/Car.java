package ce1002.f.s101502205;

public class Car {
	
	private int id;
	private double distance;
	private double totalDistance = 20000;
	private double currentSpeed;
	
	public Car(int id) {
		this.id = id;
	}
	public int getID() {
		return id;
	}
	public void randomSpeed() {
		currentSpeed = Math.random()*40 + 80;
	}
	public double getCurrentSpeed() {
		return currentSpeed;
	}
	public void gogo(){
		distance += currentSpeed;
	}
	public void setZeroDistance() {
		distance = 0;
	}
	public double getFinished() {
		return distance/totalDistance;
	}
	public boolean isEnd() {
		if (distance>=totalDistance) {
			return true;
		}else{
			return false;
		}
	}
	public String toString() {
		return "Car no." + id;
	}
}
