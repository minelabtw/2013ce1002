package ce1002.f.s102502525;

import java.util.*;
import javax.swing.*;

class Race implements Runnable {

    
    float totalDistance;
    
    List<Car> cars;
    
    Race() {
        cars = new ArrayList<Car>();
        totalDistance = 5000;
    }

    //加車輛
    public void addCar() {
        int amount = carAmount();
        Car car = new Car(amount);
        cars.add(car);
    }

    public void run() {
        for(Car car : cars) {
            car.distance = 0;
        }

        raceStart();
    }

    //開始比賽
    private void raceStart() {
        while(true) {
            for(Car car : cars) {
                car.distance += car.random_speed();
                if(car.distance > totalDistance) {
                    show_winner(car);
                    return ;
                }
            }
        }
    }

    //車輛多寡
    public int carAmount() {
        return cars.size();
    }

    //贏家
    public void show_winner(Car car) {
        JOptionPane.showMessageDialog(null, "The winner is "+car.get_name()+"!", "訊息", JOptionPane.INFORMATION_MESSAGE);
    }

}
