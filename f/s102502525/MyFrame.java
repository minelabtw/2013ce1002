package ce1002.f.s102502525;

import javax.swing.*;
import java.awt.event.*;

class MyFrame extends JFrame {

    JLabel carsAmount;
    JButton addCar;
    JButton startRace;
    Race race;

    // 跌怠
    MyFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("F-102502525");
        new_all_member();
        setBounds(500,250,300, 200);
        setVisible(true);
    }

    //秙
    void new_all_member() {
        ActionListener listener;
        race = new Race();

        addCar = new JButton("Add car");//糤ó进
        listener = new carListener();
        addCar.addActionListener(listener);
        addCar.setBounds(15, 20, 120, 50);
        add(addCar);

        startRace = new JButton("Start race");//秨﹍ゑ辽
        listener = new raceListener();
        startRace.addActionListener(listener);
        startRace.setBounds(15, 90, 120, 50);
        add(startRace);

        carsAmount = new JLabel("cars: 0");//瞷Τó进计
        carsAmount.setBounds(180, 20, 80, 30);
        add(carsAmount);

    }
    
 
    class carListener implements ActionListener {

    	carListener() {
        }

        public void actionPerformed(ActionEvent event) {
            race.addCar();
            int amount = race.carAmount();
            carsAmount.setText("cars: "+amount);
        }
    }


    class raceListener implements ActionListener {

    	raceListener() {
        }

        public void actionPerformed(ActionEvent event) {
            int amount = race.carAmount();
            Thread racing = new Thread(race);
            if(amount > 0) {
                racing.start();
            } else {//ó进计ぃ箂
                JOptionPane.showMessageDialog(null, "There must be at least one car!", "癟", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}