package ce1002.f.s102502525;

import java.util.Random;

public class Car {

    public int id;
    public float distance;

    // 設 id
    Car(int id) {
        this.id = id;
    }

    //取得隨機數 
    public float random_speed() {
        Random rand = new Random();
        return rand.nextInt(100);
    }

    //取得車輛名稱
    public String get_name() {
        return "Car no." + id;
    }
}
