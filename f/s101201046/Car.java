package ce1002.f.s101201046;

import java.awt.*;
import javax.swing.*;
import static java.lang.Math.*;

public class Car {
	private int rd;
	private int v;
	
	JPanel v_g; //speed graph
	JPanel r_g; // race graph
	
	Car() {
		v_g = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.red);
				g.fillRect(0, 0, (int)(970 * (v/120.0)), 20);
				
			}
			
		};
		v_g.setPreferredSize(new Dimension(1000, 20));
		v_g.setMaximumSize(new Dimension(1000, 20));
		
		r_g = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(new Color(255, 128, 64));
				g.fillRect(0, 0, 950, 20);
				g.setColor(Color.black);
				g.fillOval((int)(950*(rd/20000.0)), 0, 20, 20);
				
			}
		};
		r_g.setPreferredSize(new Dimension(970, 20));
		r_g.setMaximumSize(new Dimension(970, 20));
		
		rd = 0;
		v = 0;
	}
	
	/**check whether is approach the end **/
	boolean approach() {
		return rd >= 20000;
	}
	
	/** TO DO **/
	void run() {
		v = 80 + (int)(random()*40);
		rd += v;
	}
}
