package ce1002.f.s101201046;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

public class MainPanel extends JPanel{
	JButton a_c;
	JButton s_c;
	JLabel c_num;
	
	Box v_g;
	Box r_g;
	
	ArrayList<Car> cars;
	Timer racing;
	
	
	MainPanel() {
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
	/*setting the content pane*/	
		Box box = Box.createHorizontalBox();
		a_c = new JButton("Add car");
		box.add(a_c);
		box.add(Box.createRigidArea(new Dimension(30, 0)));
		c_num = new JLabel("cars:0");
		box.add(c_num);
		box.add(Box.createHorizontalGlue());
		this.add(box);
		
		this.add(Box.createRigidArea(new Dimension(0, 5)));
		
		box = Box.createHorizontalBox();
		s_c = new JButton("Start race");
		box.add(s_c);
		box.add(Box.createHorizontalGlue());
		
		this.add(box);
		
		this.add(Box.createRigidArea(new Dimension(0, 10)));
		
		box = Box.createHorizontalBox();
		v_g = Box.createVerticalBox();
		box.add(v_g);
		box.add(Box.createHorizontalGlue());
		this.add(box);
		
		this.add(Box.createRigidArea(new Dimension(0, 20)));
		
		box = Box.createHorizontalBox();
		r_g = Box.createVerticalBox();
		box.add(r_g);
		box.add(Box.createHorizontalGlue());
		this.add(box);
		
		this.add(Box.createRigidArea(new Dimension(0, 10)));
		
		/*construct the list of  cars*/
		cars = new ArrayList<Car>();
		
		/**action setting**/
		a_c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				synchronized (cars) {
					Car c = new Car();
					v_g.add(c.v_g);
					r_g.add(c.r_g);
					cars.add(c);
					c_num.setText("cars:" + cars.size());
					JFrame f = (JFrame)MainPanel.this.getTopLevelAncestor();
					f.pack();
				}
			}
		});
		
		s_c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (cars.isEmpty())
					JOptionPane.showMessageDialog(MainPanel.this.getTopLevelAncestor(), "There must be at least one car!");
				else if (!racing.isRunning())
					racing.start();
			}
		});
		
		/**Timer setting**/
		racing = new Timer(100,
				new ActionListener() {
				public void actionPerformed(ActionEvent e){
					synchronized (cars) {
						int num = -1;
						for (Car c: cars) {
							c.run();
							if (c.approach()) {
								JFrame f = (JFrame) MainPanel.this.getTopLevelAncestor();
								num = cars.indexOf(c) +1;
								JOptionPane.showMessageDialog(f, "The winner is Car no." + num + "!");
								racing.stop();
								break;
							}
						}
						
						repaint();
					}
				}
			});
		
		
		this.setVisible(true);
	}
}
