package ce1002.f.s102502516;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MyFrame extends JFrame implements ActionListener {

	private JButton addButton, startButton;
	private JLabel carsCount;
	static Distance distance;
	static Speed speed;
	static int distanceW = 500;
	static int distanceH = 300;
	static int speedW = 500;
	static int speedH = 300;

	public CarRace carRace = new CarRace(this);

	public MyFrame() {
		setTitle("A14-102502516");
		setBounds(1000, 80, 560, 870);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 視窗

		addButton = new JButton("add car");
		addButton.setBounds(20, 20, 100, 50);
		addButton.addActionListener(this);
		add(addButton); // 加車子按鈕

		startButton = new JButton("start");
		startButton.setBounds(20, 20 + 50 + 20 /* 90 */, 100, 50);
		startButton.addActionListener(new startButton());
		add(startButton); // 開始按鈕

		carsCount = new JLabel("cars: 0");
		carsCount.setBounds(20 + 100 + 20, 20, 80, 50);
		add(carsCount); // 車數

		speed = new Speed();
		speed.setBounds(20, 90 + 50 + 20 + 20/* 180 */, speedW, speedH);
		add(speed); // 速度

		distance = new Distance();
		distance.setBounds(20, 180 + distanceH + 20, distanceW, distanceH);
		add(distance); // 距離

	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(this, "The winner is " + car.toString()
				+ "!");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		carRace.addCar();
		carsCount.setText("cars: " + carRace.carNum()); // 加車子
	}

	class startButton implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (MyFrame.this.carRace.carNum() > 0) {
				Thread thread = new Thread(MyFrame.this.carRace);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}

		} // 開始
	}
}
