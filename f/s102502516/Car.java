package ce1002.f.s102502516;

import java.util.Random;

public class Car {
	public int id = 0; // 代號
	public float distance = 0f; // 已跑距離
	float currentSpeed = 0f; // 當下速度

	public float RandomSpeed() {
		Random random = new Random();
		currentSpeed = 80 + random.nextFloat() * 40f;
		return currentSpeed;
	} // 80~120的速度

	public float getCurrentSpeed() {
		return currentSpeed;
	}

	public String toString() {
		return "Car no." + Integer.toString(id);
	}
}
