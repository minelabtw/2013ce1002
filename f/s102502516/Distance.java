package ce1002.f.s102502516;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Distance extends JPanel {
	static int r = 5; // 半徑
	static int R = 2 * r; // 直徑
	static int bounds = 4; // 間距

	public Distance() {
		setBounds(0, 0, MyFrame.distanceW, MyFrame.distanceH);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Car cars : CarRace.cars) {

			g.setColor(Color.green);
			g.fillRect(0, cars.id * (R + bounds), this.getWidth(), R); // 綠色賽道
			g.setColor(Color.BLUE);
			g.fillOval(
					(int) (cars.distance / CarRace.totalDistance * MyFrame.distanceW),
					cars.id * (R + bounds), R, R); // 藍色小圓點
		}
	}
}
