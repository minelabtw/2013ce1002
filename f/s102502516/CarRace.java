package ce1002.f.s102502516;

//
import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame; // 存MyFrame主視窗
	static List<Car> cars = new ArrayList<Car>(); // 所有車
	static float totalDistance = 20000f; // 總距離
	boolean isFinished = false;

	public CarRace(MyFrame myFrame) {
		frame = myFrame;
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		this.cars.add(car);
	} // 在cars裡新增一個Car並設定id

	private void runCars() {
		for (Car car : cars) {
			car.distance += car.RandomSpeed();
			System.out.println(car.toString() + ", distance=" + car.distance);
			if (car.distance >= totalDistance) {
				this.isFinished = true;
				frame.showWinner(car);

			}
		}
	} // 移動每一台車

	public int carNum() {
		return cars.size();
	}

	@Override
	public void run() {
		while (!isFinished) {
			runCars();
			MyFrame.speed.repaint();
			MyFrame.distance.repaint();
			try {
				Thread.sleep(100); // 每1ms更新
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} // 一直跑
	}
}
