package ce1002.f.s102502516;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Speed extends JPanel {

	public Speed() {
		setBounds(0, 0, MyFrame.speedW, MyFrame.speedH);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (Car cars : CarRace.cars) {
			g.setColor(Color.green);
			g.fillRect(0, cars.id * (Distance.R + Distance.bounds),
					(int) (cars.getCurrentSpeed() / 120 * this.getWidth()), 10); // ��⨮�t
		}
	}
}