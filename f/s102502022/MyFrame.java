package ce1002.f.s102502022;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	JButton start;
	JButton add;
	CarRace carRace;
	JLabel carsCount;
	int height = 20;
	Car car;
 
	public MyFrame() throws HeadlessException {
		setTitle("F-102502022");      // 主視窗
		setSize(500, 500);
		setLayout(null);
		
		carRace = new CarRace(this);
		
		start = new JButton();   //start按鈕
		start.setText("Start race");
		start.setBounds(0, 50, 120, 40);
		add(start);
		
		add = new JButton();    //add按鈕
		add.setText("Add car");
		add.setBounds(0, 0, 120, 40);
		add(add);
		
		carsCount = new JLabel();
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(carsCount);
		
		start.addActionListener(new startListener());
		add.addActionListener(this);
		
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {         // 按下addButton的時候
		carRace.addCar();              // 增加car的數量
		carsCount.setText("cars:" + carRace.carNum());      // 更新label
	}
 
	public void showWinner(Car car) {                   // 顯示冠軍車輛
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);       // 顯示對話框
	}
 
	class startListener implements ActionListener {      // 按下start事件
 
		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {
				Thread thread = new Thread(carRace);        // 建立thread
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
			}    //顯示對話內容
		}
	}
}
