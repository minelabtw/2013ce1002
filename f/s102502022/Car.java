package ce1002.f.s102502022;

import java.util.Random;

public class Car {
	
	public int id;
	public float distance;
 
	public Car() {
		distance = 0;    //初始值為還沒跑的時候都是0
		id = 0;
	}
 
	public float randomSpeed() {// 回傳0~100的隨機數
		Random rand = new Random();
		return rand.nextFloat() * 100;
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}

}
