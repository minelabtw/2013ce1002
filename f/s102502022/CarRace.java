package ce1002.f.s102502022;

import java.util.ArrayList;
import java.util.List;
 
public class CarRace implements Runnable {       // 實作Runnable
	 
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame myframe) {
		this.frame = myframe;  //傳入視窗
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();      // id設為順序
		cars.add(car);
	}
 
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
 
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();
			if (car.distance >= totalDistance) {      // 越過終點線的時候
				isFinished = true; 
				frame.showWinner(car);
			}
		}
	}
 
	public int carNum() {
		return cars.size();
	}
 
}