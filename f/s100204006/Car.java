package ce1002.f.s100204006;

import java.util.Random;

public class Car
{
  public int id;
  public float distance;
  public float speed;

  public Car()
  {
    this.distance = 0.0F;
    this.id = 0;
    this.speed = 0.0F;
  }

  public float randomSpeed() 
  {
    Random r = new Random();
    return r.nextFloat() * 100.0F;
  }

  public String toString()
  {
    return "Car no." + this.id;
  }
}
