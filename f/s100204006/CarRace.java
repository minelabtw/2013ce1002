package ce1002.f.s100204006;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable
{
	private MyFrame frame;
	private List<Car> cars;
	private float totalDistance;	
	private boolean isFinished;

	public CarRace(MyFrame frame)
	{
	    this.frame = frame;
	    this.cars = new ArrayList<Car>();
	    this.isFinished = false;
	    this.totalDistance = 20000.0F;		    
	}

	public void addCar()
	{
	    Car car = new Car();
	    car.id = this.cars.size();	   
	    this.cars.add(car);
	}

	public void run()
	{
		while (!this.isFinished)
		runCars();
	}

	private void runCars()
	{
		for (Car car : this.cars)
		{
			car.distance += car.randomSpeed();
			car.speed = this.randomSpeed();			
			if (car.distance >= this.totalDistance) 
			{
				this.isFinished = true;
				this.frame.showWinner(car);
			}
		}
	}
	public float randomSpeed()
	{
		return this.randomSpeed();
	}
	public int carNum() 
	{
		return this.cars.size();
	}

}
