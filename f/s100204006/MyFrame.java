package ce1002.f.s100204006;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



public class MyFrame  extends JFrame implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel1;
	private Panelb panel2;
	private JLabel label;
	private JButton button1;
	private JButton button2;
	private CarRace carRace;
	
	MyFrame() throws HeadlessException
	{
		setTitle("F-100204006");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 600);
		setLayout(null);
		
		this.carRace = new CarRace(this);
		
		panel1 = new JPanel();
		panel1.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel1.setLayout(new BorderLayout(0, 0));
		setContentPane(panel1);
		
		button1 = new JButton("Add car");
		button1.addActionListener(new ButtonActionListener(this));
		this.button1.setBounds(0, 0, 120, 40);
		panel1.add(button1, BorderLayout.NORTH);
		
		button2 = new JButton("Start race");
		button2.addActionListener(new ButtonActionListener(this));
		this.button2.setBounds(0, 50, 120, 40);
		panel1.add(button2, BorderLayout.SOUTH);
		
		label = new JLabel("cars:"+ this.carRace.carNum());
		this.label.setBounds(0, 0, 0, 0);
		panel1.add(label, BorderLayout.EAST);
		
		add(this.button1);
		add(this.button2); 
		add(this.label);
		
		this.button1.addActionListener(this); 
		this.button2.addActionListener(new ButtonActionListener());
	}	
	public void actionPerformed(ActionEvent e)
	{
	   	this.carRace.addCar();
	   	this.label.setText("cars:" + this.carRace.carNum());
	}

	public void showWinner(Car car)
    {
      String msg = "The winner is " + car.toString() + "!";
      JOptionPane.showMessageDialog(this, msg);
    }	
	class Panelb extends JPanel 
	{
		 /**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Panelb() 
		 {
            // set a preferred size for the custom panel.
			setPreferredSize(new Dimension(300,300));
		 }
		@Override 
		public Dimension getPreferredSize() 
		{ 
			return new Dimension(300, 300); 
		}
		public void paintComponent(Graphics g) 
		{
			super.paintComponent(g);
			
			//�����
			g.drawRect(50,50,50, (int) carRace.randomSpeed()); 
			g.setColor(Color.BLACK);  
			g.fillRect(50,50,50,(int) carRace.randomSpeed());
	
						
		 }	
	}
	public void newRolePos(int x, int y) 
	{
		panel2 = new Panelb();
		panel2.setLayout(null);
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel2.setBounds(406, 6, 394, 394); 
		
		this.add(panel2);

	 } 
	
	class ButtonActionListener implements ActionListener
	{
		ButtonActionListener() 
	    {
			
	    }
		
		JFrame frame;
 
		public ButtonActionListener(JFrame frame) 
		{
			this.frame = frame;
		}				
	    public void actionPerformed(ActionEvent e) 
	    {
	      if (MyFrame.this.carRace.carNum() > 0) 
	      {
	        Thread thread = new Thread(MyFrame.this.carRace);
	        thread.start();
	      } 
	      else 
	      {
	        JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
	      }
	    }	    
	}	
}

