package ce1002.f.s995002046;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


@SuppressWarnings("serial")
public class MyFrame extends JFrame{
	JButton b1;
	JButton b2;
	JLabel l1;
	JFrame f1;
	List<MyPanel> ps=new ArrayList<MyPanel>();
	CarRace cr=new CarRace(this);
	MyFrame(){
		f1=this;
		init();
		setb1();
		setb2();
	}

	private void setb2() {//start race button
		// TODO Auto-generated method stub
		b2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(cr.getCars()<=0){//no car and show at least one car
					JOptionPane.showMessageDialog(MyFrame.this,
							"There must be at least one car!");
				}
				else{
					cr.setPanel(ps);//set panels to CarRace
					Thread thread=new Thread(cr);//start thread
					thread.start();
				}
			}
			
		});
	}

	private void setb1() {// set add car button
		// TODO Auto-generated method stub
		b1.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				cr.addcar();//add car
				l1.setText("cars:"+cr.getCars());
				MyPanel p=new MyPanel();//add panel
				ps.add(p);
				f1.add(p);
				p.setBounds(0, 60+ps.size()*50, 450, 80);
			}
			
		});
	}

	private void init() {
		// TODO Auto-generated method stub
		this.setLayout(null);//initialize layout
		this.setTitle("F-995002046");//set title
		this.setSize(500, 500);
		this.setVisible(true);
		b1=new JButton("Add car");
		this.add(b1);
		b1.setBounds(10, 10, 110, 30);
		b2=new JButton("Start race");
		this.add(b2);
		b2.setBounds(10, 50, 110, 30);
		l1=new JLabel("cars:0");
		this.add(l1);
		l1.setBounds(200, 10, 100, 30);
	}
}
