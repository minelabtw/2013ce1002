package ce1002.f.s995002046;

import java.util.Random;

public class Car {
	int id=0;
	int distance=0;
	
	Car(int size){
		id=size;
		distance=0;
	}
	int getSpeed(){//get speed of car
		Random r=new Random();//random
		return r.nextInt()*40+80;//80~120
	}
	int getDistance(){//get distance
		return distance;
	}
	void setDistance(int d){//set distance
		distance=d;
	}
}
