package ce1002.f.s995002046;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	int carSpeed=0;
	MyPanel(){
		this.setLayout(null);
		this.setOpaque(false);
	}
	void setSpeed(int s){//set speed
		carSpeed=s;
		repaint();
		//paintComponent(this.getGraphics());
	}
	@Override public void paintComponent(Graphics g) {
		g.setColor(Color.black);//draw fill rectangle
		g.fillRect(0, 0, carSpeed, 30);
	}
}
