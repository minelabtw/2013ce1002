package ce1002.f.s995002046;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

public class CarRace implements Runnable{
	MyFrame f1;
	List<Car> cars;
	Boolean isOver=false;
	List<MyPanel> ps;
	CarRace(MyFrame f){
		cars=new ArrayList<Car>();
		this.f1=f;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!isOver){
			runRace();
		}
	}
	private void runRace() {//run the race
		// TODO Auto-generated method stub
		for(int i=0; i<=cars.size()-1;i++){//set distance of all cars
			cars.get(i).setDistance(cars.get(i).getDistance()+cars.get(i).getSpeed());
			ps.get(i).setSpeed(cars.get(i).getSpeed());//update panel to show speed of car
			if(cars.get(i).getDistance()>=20000){//game is over once distance of car >20000
				isOver=true;
				//show dialog
				JOptionPane.showMessageDialog(f1,
						"The winner is Car no."+cars.get(i).id);
				break;
			}
			
		}
		try {//delay 0.1 second
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	void addcar(){//add car
		Car c=new Car(cars.size()+1);
		cars.add(c);
		
	}
	int getCars(){//get car size
		return cars.size();
	}
	public void setPanel(List<MyPanel> ps2) {//set panels
		// TODO Auto-generated method stub
		ps=ps2;
	}
}
