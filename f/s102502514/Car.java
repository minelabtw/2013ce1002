package ce1002.f.s102502514;

public class Car {
	public int id = 0;
	public float distance = 0;
	
	public Car() {
	}
	
	public float randomSpeed(){
		return (float) (Math.random()*40+80);
	}
	
	public String toString(){
		return "Car no." + id;
	}
}
