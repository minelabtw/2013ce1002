package ce1002.f.s102502514;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

import ce1002.f.s102502514.PaintSpeed.TimerListener;

public class PaintCar extends JPanel{
	float speed;
	List<Car> cars;
	CarRace carRace;
	Timer timer = new Timer(100, new TimerListener());
	
	public PaintCar(List<Car> cars, CarRace carRace) {
		// TODO Auto-generated constructor stub
		setLayout(null);
		setBounds(0, 400, 550, 300);
		this.cars = cars;
		this.carRace = carRace;

		setVisible(false);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		timer.start(); // 開始使用Timer
		g.setColor(Color.blue);
		g.fillRect(0, 0, 500, 13*CarRace.cars.size());
		for (int i = 0; i < CarRace.cars.size(); i++) {  //畫出賽道與賽車
			speed = cars.get(i).randomSpeed();
			cars.get(i).distance += speed;
			g.setColor(Color.black);
			g.fillOval((int) ((cars.get(i).distance)*0.025), (i * 10) + (i * 3), 10, 10);
		}
	}
	public class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (!(carRace.isFinished)){
				repaint();
			}
		}

	}
}
