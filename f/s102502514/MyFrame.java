package ce1002.f.s102502514;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	JButton addButton = new JButton("Add car");
	JButton startButton = new JButton("Start race");
	JLabel carsCount = new JLabel();
	CarRace carRace;
	PaintSpeed paintSpeed;
	PaintCar paintCar;

	public MyFrame() {
		// TODO Auto-generated constructor stub
		super("F-102502514");
		setLayout(null);
		setSize(600, 700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		carRace = new CarRace(this); // 讓carRace可以呼叫frame的method:showWinner(Car
										// car)
		paintSpeed = new PaintSpeed(carRace.cars, carRace);
		paintCar = new PaintCar(carRace.cars, carRace);

		addButton.setBounds(0, 0, 100, 30);
		startButton.setBounds(0, 40, 100, 30);
		carsCount.setBounds(120, 20, 100, 20);
		carsCount.setText("cars:" + carRace.carNum());

		add(addButton);
		add(startButton);
		add(carsCount);
		add(paintSpeed);
		add(paintCar);

		addButton.addActionListener(this);
		startButton.addActionListener(this);

		setVisible(true);
	}

	public void showWinner(Car car) {
		/* 顯示第幾台車勝利 */
		JOptionPane.showMessageDialog(null, "The winner is " + car.toString()
				+ "!", "訊息", JOptionPane.INFORMATION_MESSAGE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == addButton) {
			carRace.addCar();
			carsCount.setText("cars:" + carRace.carNum());
		} else if (e.getSource() == startButton) {
			if (carRace.carNum() <= 0) {
				/* 必須至少一台車參加比賽 */
				JOptionPane.showMessageDialog(null,
						"There must be at least one car!", "訊息",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				Thread thread = new Thread(carRace); // 加入執行緒
				thread.start();
				paintSpeed.setVisible(true);
				paintCar.setVisible(true);
			}
		}
	}
}
