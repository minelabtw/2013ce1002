package ce1002.f.s102502514;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;

public class PaintSpeed extends JPanel {
	float speed;
	List<Car> cars;
	CarRace carRace;
	Timer timer = new Timer(100, new TimerListener());

	public PaintSpeed(List<Car> cars, CarRace carRace) {
		// TODO Auto-generated constructor stub
		setLayout(null);
		setBounds(0, 80, 550, 300);
		this.cars = cars;
		this.carRace = carRace;

		setVisible(false);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		timer.start(); // 開始使用Timer
		for (int i = 0; i < CarRace.cars.size(); i++) {  //畫出速度長條圖
			speed = cars.get(i).randomSpeed();
			g.setColor(Color.black);
			g.fillRect(0, (i * 10) + (i * 3), (int) (4.5 * speed), 10);
		}
	}

	public class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (!(carRace.isFinished)){
				repaint();
			}
		}

	}
}
