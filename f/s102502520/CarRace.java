package ce1002.f.s102502520;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable{
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 5000;
		cars = new ArrayList<Car>();
	}
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();//id
		cars.add(car);
	}
	@Override
	public void run() {
		while (!isFinished) {// 只要isFinished==false就會一直跑
			runCars();
		}
	}
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();// Car
			if (car.distance >= totalDistance) {// 到終點
				isFinished = true;// loop
				frame.showWinner(car);
			}
		}
	}
	public int carNum() {
		return cars.size();
	}
}