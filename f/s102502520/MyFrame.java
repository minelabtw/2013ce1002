package ce1002.f.s102502520;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener{
	JButton stButton;//define
	JButton addButton;
	CarRace Race;
	JLabel Count;
 
	public MyFrame() throws HeadlessException {
		setTitle("F-102502520");
		setBounds(800,200, 600, 400);
		setLayout(null);
		Race = new CarRace(this);
		stButton = new JButton();
		addButton = new JButton();
		Count = new JLabel();
		stButton.setText("start");
		stButton.setBounds(50, 50, 100, 30);
		addButton.setText("add car");
		addButton.setBounds(50, 0, 100, 30);
		Count.setText("cars:" + Race.carNum());
		Count.setBounds(200, 0, 120, 40);
		add(addButton);
		add(stButton);
		add(Count);
		addButton.addActionListener(this);
		stButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {// 按下addButton
		Race.addCar();// 增加car
		Count.setText("cars:" + Race.carNum());// 更新label
	}
 
	public void showWinner(Car car) {// 顯示winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}
 
	class startListener implements ActionListener {//按下stButton
 
		@Override
		public void actionPerformed(ActionEvent e) {
			if (Race.carNum() > 0) {// 至少要有一輛
				Thread thread = new Thread(Race);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}
}
