package ce1002.f.s102502520;

import java.util.Random;

public class Car {
	public int id;//define
	public float distance;
	public Car() {
		distance = 0;
		id = 0;
	}
	public float randomSpeed() {// random
		Random r = new Random();
		return r.nextFloat() * 100;
	}
	@Override
	public String toString() {
		return "Car no." + id;
	}
}