package ce1002.f.s102502012;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Chart extends JPanel { // speed, pos state
	private CarRace race;
	
	public Chart(CarRace race){
		this.race = race;
	}
	
	public void draw(){
		repaint();
	}
	
	public void paintComponent(Graphics g){
		for(int i = 0; i < race.getCarsNum(); i++){
			g.setColor(Color.black);
			g.fillRect(50, 200 + i * 15, (int)(race.getCarsSpeedById(i) * 5), 10);
		}
		
		for(int i = 0; i < race.getCarsNum(); i++){
			g.setColor(Color.black);
			g.fillRect(50, 400 + i * 15, 400, 10);
			g.setColor(Color.yellow);
			g.fillOval(50 + (int)(race.getCarsPosById(i) / 5), 400 + i * 15, 10, 10);
			
		}
	}
}
