package ce1002.f.s102502012;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Operation extends JPanel {
	private JButton btnAdd;
	private JButton btnStart;
	private JLabel lblCars;
	private CarRace race;
	
	public Operation(CarRace race){
		setLayout(null);
		
		this.race = race;
		
		btnAdd = new JButton();
		btnAdd.setBounds(10, 10, 100, 40);
		btnAdd.setText("Add car");
		btnAdd.addActionListener(new btnAddClick());
		
		btnStart = new JButton();
		btnStart.setBounds(10, 60, 100, 40);
		btnStart.setText("Start");
		btnStart.addActionListener(new btnStartClick());
		
		lblCars = new JLabel();
		lblCars.setBounds(150, 30, 100, 50);
		
		add(btnAdd);
		add(btnStart);
		add(lblCars);
		
		updateNum();
	}
	
	public void updateNum(){
		lblCars.setText("cars: " + race.getCarsNum());
	}
	
	private class btnAddClick implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			race.add();
			updateNum();
		}		
	}
	
	private class btnStartClick implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if(race.getCarsNum() > 0){
				Thread thread = new Thread(race);
				thread.start();
			}
			else
				JOptionPane.showMessageDialog(new JFrame(), "There must be at least one car!");
		}		
	}
}
