package ce1002.f.s102502012;

import java.util.Random;

public class Car {
	private double pos;
	private double speed;
	private int id;
	
	public Car(int id){
		this.id = id;
	}
	
	public void init(){
		pos = speed = 0;
	}
	
	public void randomSpeed(){
		Random rand = new Random();
		speed = 0;
		int time = rand.nextInt(41) + 80;
		for(int i = 0; i < time; i++)
			speed += rand.nextDouble();
	}
	
	public double getPos(){
		return pos;
	}
	
	public double getSpeed(){
		return speed;
	}
	
	public void run(){
		randomSpeed();
		pos += speed;
	}
	
}
