package ce1002.f.s102502012;

import java.awt.BorderLayout;

import javax.swing.*;

public class MyFrame extends JFrame {
	private CarRace race;
	private Operation operation;
	private Chart chart;
	public MyFrame(){
		race = new CarRace(this);
		operation = new Operation(race);
		chart = new Chart(race);
		
		operation.setBounds(0, 0, 400, 300);
		chart.setBounds(0, 300, 300, 300);
		
		add(operation);
		add(chart);
	}
	
	public void draw(){
		chart.draw();
	}
	
	
}
