package ce1002.f.s102502012;

import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CarRace implements Runnable {
	private ArrayList<Car> cars; 
	private boolean gameover;
	private final int goal = 2000;
	private MyFrame window;
	
	public CarRace(MyFrame window){
		cars = new ArrayList<Car>();
		this.window = window;
	}
	
	public void add(){
		cars.add(new Car(getCarsNum()));
	}
	
	void init(){
		gameover = false;
		for(int i = 0; i < cars.size(); i++)
			cars.get(i).init();
	}
	
	public void run(){
		init();
		while(!gameover){
			for(int i = 0; i < cars.size() ; i++){
				cars.get(i).run();
				if(cars.get(i).getPos() >= goal){
					JOptionPane.showMessageDialog(new JFrame(), "The winner is Car no." + i + "!");
					gameover = true;
					break;
				}
				window.draw();
			}
			
			try{
				Thread.sleep(100);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public int getCarsNum(){
		return cars.size();
	}
	
	public double getCarsSpeedById(int id){
		return cars.get(id).getSpeed();
	}
	
	public double getCarsPosById(int id){
		return cars.get(id).getPos();
	}
}
