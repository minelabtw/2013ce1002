package ce1002.f.s102502023;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame {
    private JButton addButton = new JButton("Add car"); // initialize JButton addButton
    private JButton startButton = new JButton("Start race"); // initialize JButton startButton
    private JLabel jlbl = new JLabel("cars:0"); // initialize JLabel jlbl
    //private JScrollPane pane = new JScrollPane();
    private JPanel panel = new JPanel(); // initialize JPanel panel
    
    private int count = 0; // initialize integer count
    
    CarRace carRace;
    
    public MyFrame() {
    	//setLayout(new GridLayout(1, 2));
    	//panel.setLayout(new GridLayout(2, 1));
    	setLayout(null);
    	//setBounds(0, 0, 500, 500);
    	carRace = new CarRace(this);
    	
    	addButton.setBounds(10, 10, 100, 50);
    	startButton.setBounds(10, 80, 100, 50);
    	jlbl.setBounds(120, 10, 100, 50);
    	
    	addButton.addActionListener(new addButtonListener());
    	startButton.addActionListener(new startButtonListener());
    	
    	//panel.add(addButton);
    	//panel.add(startButton);
    	
    	//add(panel);
    	//add(jlbl);
    	add(addButton);
    	add(startButton);
    	add(jlbl);
    	//pane.add(this);
    }
    
    public void showWinner(Car car) {
    	JOptionPane.showMessageDialog(null, "The winner is Car no." + car.id);
    }
    
    private class addButtonListener implements ActionListener {
    	@Override
    	public void actionPerformed(ActionEvent e) {
    		carRace.addCar(count);
    		count++;
    		jlbl.setText("cars:" + count);
    	}
    }
    
    private class startButtonListener implements ActionListener {
    	@Override
    	public void actionPerformed(ActionEvent e) {
    		if (count <= 0)
    			JOptionPane.showMessageDialog(null, "There must be at least one car!");
    		else {
    			Thread thread = new Thread(carRace);
    			thread.start();
    			try {
    			thread.sleep(100);

    			}
    			catch (Exception ex) {
    				ex.printStackTrace();
    			}
    		}
    	}
    }
}
