package ce1002.f.s102502023;

import javax.swing.JFrame;

public class Final {
    public static void main(String[] args) {
    	MyFrame frame = new MyFrame();
    	//frame.setBounds(500, 500);
    	frame.setSize(500, 500);
    	//frame.pack();
    	frame.setVisible(true);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setTitle("F-102502023");
    	frame.setLocationRelativeTo(null);
    }
}
