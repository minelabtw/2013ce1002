package ce1002.f.s102502023;

import java.util.Random;

public class Car {
    public int id; // initialize integer id
    public float distance; // initialize float distance
    Random random = new Random();
    
    
    public Car() {
    	
    }
    
    public void setCar(int id) {
    	this.id = id;
    	distance = 0;
    }
    
    public float speed() {
		return (random.nextFloat() * 40) + 80;
		//return distance;
	}
    
    public float getDistance() {
    	return distance;
    }
}
