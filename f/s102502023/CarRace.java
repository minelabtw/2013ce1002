package ce1002.f.s102502023;

import java.util.ArrayList;
import java.awt.*;

public class CarRace implements Runnable {
    private ArrayList<Car> list = new ArrayList<Car>(); // initialize ArrayList<Car> list
    private float totalDistance = 20000; // initialize titalDistance to 20000
    private boolean isFinish = false;// initialize boolean isFinish
    MyFrame frame; // initialize MyFrame frame
    
    public CarRace(MyFrame frame) {
    	this.frame = frame;
    }
    
    public void addCar(int count) {
    	Car car = new Car();
    	car.setCar(count);
    	list.add(car);
    }
    
    @Override
    public void run() {
    	while (!isFinish)
    		runCar();
    	
    }
    
    public void runCar() {
    	for (Car tmp : list) {
    		tmp.distance += tmp.speed();
    		if (tmp.distance >= totalDistance) {
    			isFinish = true;
    			frame.showWinner(tmp);
    			break;
    		}
    	}
    }
    
    
    
}
