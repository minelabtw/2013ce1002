package ce1002.f.s102502527;

public class Car
{
	public int id;
	public float distance;
	
	Car(int id)
	{
		this.id = id;//將id值存入
		distance = 0;
	}
	
	public float randomSpeed()
	{
		return (float) Math.random() * ( 120 - 80 );//隨機從80到120中取數
	}
	
	public String toString()
	{
		return "" + id;
	}
}
