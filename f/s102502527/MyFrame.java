package ce1002.f.s102502527;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame
{
	JButton addButton;
	JButton startButton;
	JLabel carsCount;
	CarRace carRace;
	
	MyFrame()
	{
		setSize(300, 300);//設定大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//將叉叉給予離開功能
		setLocationRelativeTo(null);
		setLayout(null);
		setTitle("F-102502527");//設定標題
		carRace = new CarRace(this);
		Build();
	}
	
	private void Build()
	{
		addButton = new JButton();//加入按鍵並設定文字及大小
		addButton.setText( "Add car" );
		addButton.setBounds( 10 , 10 , 100 , 30 );
		addButton.addActionListener//按下後的動作
		(
			new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				carRace.addCar();//呼叫加車輛函式
				
				carsCount.setText( "cars:" + carRace.carNum() );//顯示數量
			}
		}
		);
		
		add(addButton);
		
		startButton = new JButton();//加入按鍵並設定文字及大小
		startButton.setText("Start race");
		startButton.setBounds(10, 50, 100, 30);
		startButton.addActionListener//按下後的動作
		(
			new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (carRace.carNum() > 0)
				{
					Thread thread = new Thread(carRace);
					thread.start();
				}
				else//顯示車輛為0時的警告
				{
					JOptionPane.showMessageDialog(null, "There must be at least one car!" , "訊息                                                      ", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
		);
		
		add(startButton);
		
		carsCount = new JLabel( "cars:0" );
		carsCount.setBounds( 120 , 10 , 70 , 30 );
		
		add(carsCount);
	}
	
	public void showWinner( Car car )//顯示贏家
	{
		JOptionPane.showMessageDialog( null, "The winner is Car no.1" + car.toString() + "!" , "訊息                                                      ", JOptionPane.INFORMATION_MESSAGE );
	}
}

