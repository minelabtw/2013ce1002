package ce1002.f.s102502527;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable
{
	MyFrame frame;
	
	ArrayList<Car> cars;//陣列
	
	float totalDistance = 20000;//總距離長
	
	boolean isFinished;//判斷式
	
	CarRace( MyFrame frame )
	{
		cars = new ArrayList<Car>();
		this.frame = frame;
	}
	
	public void addCar()
	{
		cars.add( new Car(carNum()) );
	}
	
	@Override
	public void run()
	{
		isFinished = false;//初設定為false
		
		for( Car c : cars )
		{
			c.distance = 0;
		}
		while( ! isFinished )
		{
			runCars();
		}
	}
	
	private void runCars()
	{
		for( Car c : cars )
		{
			c.distance += c.randomSpeed();
			
			if ( c.distance > totalDistance )
			{
				frame.showWinner(c);
				
				isFinished = true;//將判斷式變成true
				
				break;
			}
		}
	}
	
	public int carNum()
	{
		return cars.size();
	}
}
