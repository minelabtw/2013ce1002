package ce1002.f.s102502026;

public class Car {
	public int id;
	public float distance;

	public Car(int name) {
		id = name;	//car id
	}

	public float randomSpeed() {
		return (float) (Math.random() * 40 + 80 );	//speed 80~120
	}

	public String toString() {
		return "Car no." + id;	//car id to return
	}
}
