package ce1002.f.s102502026;

import java.util.ArrayList;
import java.util.List;

public class Race implements Runnable{
	Frame frame;
	public static List<Car> cars = new ArrayList<Car>();
	float totalDistance = 20000;
	boolean isFinished = false;
	private int car;

	Race(Frame fr) {
		frame = fr;
	}

	public void addCar() {
		cars.add(new Car(car++));	//add car
	}

	public void run() {
		while (isFinished == false) {
			runCars();	//when isFinished is false
		}
	}

	private void runCars() {	//process to the race of each car, adding speed and distance and finish at 20000d
		for (int x = 0; x < cars.size(); x++) {
			Car car = cars.get(x);
			car.distance = car.distance + car.randomSpeed();
			
			if (car.distance > totalDistance) {
				isFinished = true;
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();	//num of cars
	}

}


