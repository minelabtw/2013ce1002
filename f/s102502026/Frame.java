package ce1002.f.s102502026;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;



public class Frame extends JFrame implements ActionListener {
	private JButton bt1 = new JButton();
	private JButton bt2 = new JButton();
	private JLabel jl = new JLabel();
	private Race R = new Race(this);

	Frame() {	
		//add buttons, labels
		setLayout(null);
		bt1.setText("Add car");
		bt1.setBounds(5, 5, 120, 30);
		bt2.setText("Start race");
		bt2.setBounds(5, 50, 120, 30);
		jl.setText("cars:0");
		jl.setBounds(170, 30, 100, 10);
		add(bt1);
		add(bt2);
		add(jl);
		bt1.addActionListener(this);
		bt2.addActionListener(this);
	}

	public void showWinner(Car car) {
		//show the car that finished first
		final JFrame fr = new JFrame("Message"); // open another frame
		fr.setLayout(new BorderLayout());
		ImageIcon icon = new ImageIcon("src/ce1002/f/s102502026/a.png"); // add
																			// picture
		JButton bt = new JButton("Accept"); // add button
		JPanel pn = new JPanel(); // add panel
		pn.setSize(100, 50);
		JPanel pn1 = new JPanel(); // add panel2
		pn1.setSize(100, 50);
		JLabel label = new JLabel(); // add label
		fr.setSize(200, 100);
		fr.setLocationRelativeTo(null);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fr.setVisible(true);
		label.setText("The winner is " + car.toString() + "!"); // label output
		// add to panel
		pn.add(new JLabel(icon));
		pn.add(label);
		pn1.add(bt);
		fr.add(pn, BorderLayout.CENTER);
		fr.add(pn1, BorderLayout.SOUTH);
		bt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fr.dispose(); // close frame when press button
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent x) {
		// TODO Auto-generated method stub
		if (x.getSource() == bt1) {	//if click button 1
			R.addCar();		//add car
			jl.setText("cars:" + R.carNum());	 //count cars
		} else if (x.getSource() == bt2) {	//if click button 2
			if (R.carNum() > 0) {	//if have more than 0 cars
				Thread th = new Thread(R);	//thread and start it
				th.start();
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				//output there must be at least one car
				final JFrame fr = new JFrame("Message"); // open another frame
				fr.setLayout(new BorderLayout());
				ImageIcon icon = new ImageIcon(
						"src/ce1002/f/s102502026/a.png"); // add picture
				JButton bt = new JButton("Accept"); // add button
				JPanel pn = new JPanel(); // add panel
				pn.setSize(100, 50);
				JPanel pn1 = new JPanel(); // add panel2
				pn1.setSize(100, 50);
				JLabel label = new JLabel(); // add label
				fr.setSize(300, 100);
				fr.setLocationRelativeTo(null);
				fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				fr.setVisible(true);
				label.setText("There must be at least one car!"); // label
																	// output
				// add to panel
				pn.add(new JLabel(icon));
				pn.add(label);
				pn1.add(bt);
				fr.add(pn, BorderLayout.CENTER);
				fr.add(pn1, BorderLayout.SOUTH);
				bt.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						fr.dispose(); // close frame when press button
					}
				});
			}

		}

	}

}


