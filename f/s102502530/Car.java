package ce1002.f.s102502530;

import java.util.Random;

public class Car {
	//declare
	public int id;
	public float distance;
	public float speed;
	private static Random random = new Random();
	
	//constructor
	public Car(int id) {
		this.id = id;
		distance = 0;
	}
	//produce random float 80~120
	public static float randomSpeed() {
		return random.nextFloat() * 20 + 80;
	}
	//product "Car no.\(id)"
	public String toString() {
		return new String("Car no." + id);
	}
}