package ce1002.f.s102502530;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.smartcardio.Card;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	//declare
	public JPanel leftPanel;
	public JPanel rightPanel;
	public JPanel middlePanel;
	public JPanel bottomPanel;
	
	//constructor
	public MyFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 560);
		setResizable(false);
		setLayout(new FlowLayout());
		setTitle("F-102502530");
		
		//create left panel
		leftPanel = new JPanel();
		leftPanel.setPreferredSize(new Dimension(140, 100));
		leftPanel.setLayout(new FlowLayout());
		add(leftPanel);
		
		//create right panel
		rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(140, 100));
		rightPanel.setLayout(new FlowLayout());
		add(rightPanel);
	}
	
	//show winner
	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(this, "The winner is " + car.toString() + "!", "Message", JOptionPane.INFORMATION_MESSAGE);
	}
}