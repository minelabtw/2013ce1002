package ce1002.f.s102502530;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Final {
	public static void main(String[] args) {
		
		//create frame
		final MyFrame frame = new MyFrame();
		
		//create car race
		final CarRace carRace = new CarRace(frame);
		
		//add car button
		JButton addButton = new JButton();
		addButton.setText("Add Car");
		addButton.setPreferredSize(new Dimension(120, 40));
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				carRace.addCar();
			}
		});
		frame.leftPanel.add(addButton);
		
		//start button
		JButton startButton = new JButton();
		startButton.setText("Start");
		startButton.setPreferredSize(new Dimension(120, 40));
		startButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (carRace.carNum() == 0)
					JOptionPane.showMessageDialog(frame, "There must be at least one car!", "Error", JOptionPane.ERROR_MESSAGE);
				else
					(new Thread(carRace)).start();
			}
		});
		frame.leftPanel.add(startButton);
		
		//cars count label
		JLabel carsCountLabel = new JLabel();
		carsCountLabel.setText("Cars: 0");
		carsCountLabel.setPreferredSize(new Dimension(120, 40));
		frame.rightPanel.add(carsCountLabel);
		
		//open frame
		frame.setVisible(true);
	}
}