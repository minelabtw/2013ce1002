package ce1002.f.s102502530;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CarRace implements Runnable {
	//declare
	private MyFrame frame;
	public List<Car> cars;
	private int number;
	public float totalDistance;
	private boolean isFinished;
	
	//constructor
	public CarRace(MyFrame frame) {
		this.frame = frame;
		cars = new ArrayList<Car>();
		number = 0;
		totalDistance = 20000;
		isFinished = false;

		//create middle panel
		frame.middlePanel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.PINK);
				int x, y ;
				for (Car car : cars) {
					x = (int) Math.floor(car.speed / 120 * 290 + 0.5);
					y = car.id * 10;
					g.fillRect(0, y, x, 9);
				}
			};
		};
		frame.middlePanel.setPreferredSize(new Dimension(290, 200));
		frame.add(frame.middlePanel);
		
		//create bottom panel
		frame.bottomPanel = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(0, 0, 10, cars.size() * 10);
				g.setColor(Color.RED);
				g.fillRect(280, 0, 10, cars.size() * 10);
				g.setColor(Color.BLACK);
				int x, y;
				for (Car car : cars) {
					x = (int) Math.floor(car.distance / totalDistance * 280 + 1.5);
					y = car.id * 10 + 1;
					if (x > 6) {
						g.setColor(Color.MAGENTA);
						g.fillRect(10, y + 1, x - 6, 8);
						g.setColor(Color.BLACK);
					}
					g.fillOval(x, y, 8, 8);
				}
			};
		};
		frame.bottomPanel.setPreferredSize(new Dimension(290, 200));
		frame.add(frame.bottomPanel);
	}
	
	//add car
	public void addCar() {
		if (isFinished)
			return;
		Car car = new Car(number);
		++number;
		cars.add(car);
		((JLabel) frame.rightPanel.getComponent(0)).setText("Cars: " + number);
	}
	
	//clear cars
	public void clearCars() {
		number = 0;
		isFinished = false;
		cars.clear();
		((JLabel) frame.rightPanel.getComponent(0)).setText("Cars: " + number);
	}
	
	//run cars
	private void runCars() {
		for (Car car : cars) {
			car.speed = Car.randomSpeed();
			car.distance += car.speed;
			if (car.distance >= totalDistance) {
				isFinished = true;
				frame.showWinner(car);
				return;
			}
		}
	}
	
	//produce number of cars
	public int carNum() {
		return number;
	}
	
	@Override //run
	public void run() {
		while (! isFinished) {
			frame.bottomPanel.repaint();
			frame.middlePanel.repaint();
			try {
				Thread.sleep(100);
			} catch(Exception e){}
			runCars();
		}
		frame.bottomPanel.repaint();
		frame.middlePanel.repaint();
	};
}
