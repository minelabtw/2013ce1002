package ce1002.f.s102502528;

import java.awt.HeadlessException;
import java.awt.event.*;

import javax.swing.*;

class MyFrame extends JFrame implements ActionListener {
	JButton start;
	JButton add;
	CarRace cr;
	JLabel count;

	public MyFrame() throws HeadlessException {
		setTitle("Final-102502528");
		setBounds(0, 0, 500, 300);
		setLayout(null);
		setLocationRelativeTo(null);
		cr = new CarRace(this);
		start = new JButton();
		add = new JButton();
		count = new JLabel();
		start.setText("Start race");
		start.setBounds(10, 60, 120, 40);
		add.setText("Add car");
		add.setBounds(10, 10, 120, 40);
		count.setText("cars:" + cr.carNum());
		count.setBounds(170, 50, 120, 40);
		add(add);
		add(start);
		add(count);
		add.addActionListener(this);
		start.addActionListener(new startListener());
	}

	public void actionPerformed(ActionEvent e) {//addButton pressed
		cr.addCar();// add cars
		count.setText("cars:" + cr.carNum());// refresh label
	}

	public void showWinner(Car car) {// show winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}

	class startListener implements ActionListener {// startButton event

		public void actionPerformed(ActionEvent e) {
			if (cr.carNum() > 0) {// at least one car
				Thread thread = new Thread(cr);// thread
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
			}
		}
	}
}
