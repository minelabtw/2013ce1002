package ce1002.f.s102502528;

import java.util.ArrayList;
import java.util.List;

class CarRace implements Runnable {

	
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
		
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();// set id
		cars.add(car);
	}
 
	public void run() {
		
		while (!isFinished) {
			runCars();
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
 
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();// Car go
			
			if (car.distance >= totalDistance) {
				isFinished = true;//stop the loop
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}
	
}

