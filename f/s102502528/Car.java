package ce1002.f.s102502528;

import java.util.Random;

class Car {

	public int id;
	public float distance;
 
	public Car() {
		distance = 0;
		id = 0;
	}

	public int randomSpeed() {// random
		Random r = new Random();
		return (r.nextInt(40)+80);
	}
 
	public String toString() {
		return "Car no." + id;
	}
}
