package ce1002.f.s102502523;

import java.util.Random;

public class Car
{
  public int id;
  public float distance;

  public Car()
  {
    this.distance = 0.0F;
    this.id = 0;
  }

  public float randomSpeed() {
    Random r = new Random();
    return r.nextFloat() * 40.0F + 80.0F ;//車速在80~120之間
  }

  public String toString()
  {
    return "Car no." + this.id;
  }
}