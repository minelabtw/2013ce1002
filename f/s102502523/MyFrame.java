package ce1002.f.s102502523;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener
{
  JButton startButton;
  JButton addButton;
  CarRace carRace;
  JLabel carsCount;

  public MyFrame()
    throws HeadlessException
  {    //視窗設定
    setTitle("F-102502523");
    setBounds(0, 0, 300, 200);
    setLayout(null);
    this.carRace = new CarRace(this);
    this.startButton = new JButton();
    this.addButton = new JButton();
    this.carsCount = new JLabel();
    this.startButton.setText("Start race");
    this.startButton.setBounds(0, 60, 120, 55);
    this.addButton.setText("Add car");
    this.addButton.setBounds(0, 0, 120, 55);
    this.carsCount.setText("cars:" + this.carRace.carNum());
    this.carsCount.setBounds(150, 0, 120, 80);
    add(this.addButton);
    add(this.startButton);
    add(this.carsCount);
    this.addButton.addActionListener(this);
    this.startButton.addActionListener(new startListener());
  }

  public void actionPerformed(ActionEvent e)
  {
    this.carRace.addCar();//增加車輛
    this.carsCount.setText("cars:" + this.carRace.carNum());//新label  
  }

  public void showWinner(Car car) {  //顯示贏家
    String msg = "The winner is " + car.toString() + "!";
    JOptionPane.showMessageDialog(this, msg);
  }
  class startListener implements ActionListener {
    startListener() {
    }

    public void actionPerformed(ActionEvent e) {
      if (MyFrame.this.carRace.carNum() > 0) {//至少要有一輛車
        Thread thread = new Thread(MyFrame.this.carRace);
        thread.start();
      } else {
        JOptionPane.showMessageDialog(MyFrame.this, 
          "There must be at least one car!");
      }
    }
  }
}