package ce1002.f.s102502523;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class CarRace
  implements Runnable
{
  MyFrame frame;
  List<Car> cars;
  float totalDistance;
  boolean isFinished;

  public CarRace(MyFrame farme)
  {
    this.frame = farme;
    this.isFinished = false;
    this.totalDistance = 20000.0F;//總距離
    this.cars = new ArrayList();
  }

  public void addCar() {
    Car car = new Car();
    car.id = this.cars.size();
    this.cars.add(car);
  }

  public void run()
  {
    while (!this.isFinished)
      runCars();
  }

  private void runCars()
  {
    for (Car car : this.cars) {
      car.distance += car.randomSpeed(); // 車子前進
      System.out.println(car.toString() + ", distance=" + car.distance);
      if (car.distance >= this.totalDistance) { //衝過終點
        this.isFinished = true;   //比賽停止
        this.frame.showWinner(car);
      }
    }
  }

  public int carNum() {
    return this.cars.size();// 設定ID  
  }
}