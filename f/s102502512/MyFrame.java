package ce1002.f.s102502512;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;



public class MyFrame  extends JFrame 
implements ActionListener{
	  private JButton startButton = new JButton("start");
	  private JButton addButton = new JButton("add car");
	  private CarRace carRace;
	  private JLabel carsCount;
	  private static int carsid = 0;
	  private int speeds[] ;
	  private boolean de = false;
	  
	  MyFrame()
	    throws HeadlessException					//set the frame
	  {
	    setTitle("A14-102502512");
	    setBounds(200, 100, 500, 600);
	    setLayout(null);
	    carRace = new CarRace(this);
	    carsCount = new JLabel();
	    startButton.setBounds(10, 100, 120, 40);
	    addButton.setBounds(10, 40, 120, 40);
	    carsCount.setText("cars:" + this.carRace.carNum());
	    carsCount.setBounds(300, 70, 120, 40);
	    add(addButton);
	    add(startButton);
	    add(carsCount);
	    
	    addButton.addActionListener(this);
	    startButton.addActionListener(new startListener());
	  }
	  
	  public void actionPerformed(ActionEvent e)		//按了add car後增carrace中的cars
	  {
	    carRace.addCar();
	    carsCount.setText("cars:" + this.carRace.carNum());
	    carsid++;
	    DrawV dvDrawV = new DrawV(true);
	    dvDrawV.setBounds(2, carsid*20+150,5,200);
	    add(dvDrawV);
	  }
	  
	  public void showWinner(Car car)
	  {
	    String msg = "The winner is " + car.toString() + "!";	//取得"那輛car"的ID
	    JOptionPane.showMessageDialog(this, msg);
	  }
	  
	  class startListener implements ActionListener			//按了star後增新thread讓他跑，如果carcount != 0
	  {	    
	    public void actionPerformed(ActionEvent e)
	    {
	      if (MyFrame.this.carRace.carNum() > 0)
	      {
	    	  Thread thread = new Thread(MyFrame.this.carRace);
	    	  thread.start();
	    	  de = true;
	      }
	      else
	      {
	    	  JOptionPane.showMessageDialog(MyFrame.this, "There must be at least one car!");
	      }
	    }
	  }


}
