package ce1002.f.s102502512;

import java.util.Random;

public class Car {
	public int id;
	  public float distance;
	  public int speed;
	  
	  Car()
	  {
	    distance = 0;
	    id = 0;
	    speed =0;
	  }
	  
	  public float randomSpeed()			//隨機製造速度	
	  {
	    Random r = new Random();
	    return r.nextFloat() * 100;
	  }
	  
	  public String toString()				//回傳CAR的ID
	  {
	    return "Car no." + this.id;
	  }

}
