package ce1002.f.s102502512;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable
{
	 MyFrame frame;
	  List<Car> cars;
	  float totalDistance;
	  boolean isFinished;
	  
	  CarRace(MyFrame farme)				//set car's running distance
	  {
	    frame = farme;
	    isFinished = false;
	    totalDistance = 2000;
	    cars = new ArrayList();
	  }
	  
	  public void addCar()						//add the car with class "Class"
	  {
	    Car car = new Car();					
	    car.id = this.cars.size();
	    this.cars.add(car);
	  }
	  
	  public void run()							//let cars run
	  {
	    while (!isFinished) {
	      runCars();
	    }
	  }
	  
	  private void runCars()					//判斷哪台車先跑完
	  {
	    for (Car car : this.cars)
	    {
	      car.distance += car.randomSpeed();
	      System.out.println(car.toString() + ", distance=" + car.distance);
	      if (car.distance >= this.totalDistance)
	      {
	        this.isFinished = true;				//if car no.x finish the race pass the signal to run()
	        this.frame.showWinner(car);
	      }
	    }
	  }
	  
	  public int carNum()					//回傳car的數量
	  {
	    return this.cars.size();
	  }

}
