package ce1002.f.s102502521;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MyFrame extends JFrame {
	private JButton addButton = new JButton("Add car");
	private JButton startButton = new JButton("Start race");
	private JLabel carsCount = new JLabel();
	private CarRace carRace = new CarRace();

	// 設定FRAME介面
	public MyFrame() {
		addButton.setBounds(0, 0, 150, 50);
		startButton.setBounds(0, 75, 150, 50);
		carsCount.setBounds(250, 25, 100, 50);
		carsCount.setText("cars:" + carRace.carNum());

		add(addButton);
		add(startButton);
		add(carsCount);

		// 為按鈕加LISTENER
		addButton.addActionListener(new addButtonListener());
		startButton.addActionListener(new startButtonListener());
	}

	private class addButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			// 加車子，並顯示車數
			carRace.addCar();
			carsCount.setText("cars:" + carRace.carNum());
		}
	}

	private class startButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			// 若沒有車子，跳出視窗

			if (carRace.carNum() <= 0) {
				JOptionPane.showMessageDialog(null,
						"There must be at least one car!");
			}
		}
	}
}
