package ce1002.f.s102502506;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();  //初始化LIST
	float totalDistance = 20000;
	boolean isFinished = false;
	int carid = 0;

	CarRace(MyFrame a) {
		frame = a;
	}

	public void addCar() {
		cars.add(new Car(carid++));  //+車子數
	}

	private void runCars() {
		for (int i = 0; i < cars.size(); i++) {
			Car a = cars.get(i);  //給車子ID
			a.distance = a.distance + a.randomSpeed();  //一直+距離
			if (a.distance > totalDistance) {  //有一輛車到達20000就勝利
				isFinished = true;
				frame.showWinner(a);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

	public void run() {
		while (!isFinished) {
			runCars();
		}
	}
	/*public void paintComponent(Graphics g){
		for (int i = 0; i < cars.size(); i++) {
			Car a = cars.get(i);  //給車子ID
			g.setColor(Color.BLUE);
			g.fillRect(30, 50,(int)(a.randomSpeed()), 15);
	}
	}*/
}
