package ce1002.f.s102502506;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	private JButton addButton = new JButton("Add car");
	private JButton startButton = new JButton("Start race");
	private JLabel carsCount = new JLabel("cars:0");
	private CarRace carrace = new CarRace(this);

	MyFrame() {
		this.setTitle("F-102505506");  //����
		this.setSize(800, 800);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(addButton);
		this.add(startButton);
		this.add(carsCount);
		addButton.setSize(100, 50);
		addButton.setLocation(10, 10);
		addButton.addActionListener(this);
		startButton.setSize(100, 50);
		startButton.setLocation(10, 70);
		startButton.addActionListener(this);
		carsCount.setSize(100, 30);
		carsCount.setLocation(160, 25);
		this.setVisible(true);
	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(new JFrame(),"The winner is " + car.toString() + "!");
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addButton) {
			carrace.addCar();
			carsCount.setText("cars:" + carrace.carNum());
		}
		if (e.getSource() == startButton) {
			if (carrace.carNum() > 0) {
				new Thread(carrace).start();
			} else {
				JOptionPane.showMessageDialog(new JFrame(),"There must be at least one car!");
			}
		}
	}
}
