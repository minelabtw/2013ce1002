package ce1002.f.s102502513;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable
{
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame frame)
	{
		this.frame = frame;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
	}
 
	public void addCar()
	{
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}
 
	@Override
	public void run() 
	{
		while (!isFinished) 
		{
			runCars();
		}
	}
 
	private void runCars() 
	{
		for (Car car : cars) 
		{
			car.distance += car.randomSpeed();    //Car前進
			
			if (car.distance >= totalDistance) //通過終點
			{
				isFinished = true;
				frame.showWinner(car);
			}
		}
	}
 
	public int carNum() 
	{
		return cars.size();
	}
}
