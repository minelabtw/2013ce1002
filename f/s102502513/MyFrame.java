package ce1002.f.s102502513;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener  
{
	JButton startButton = new JButton("Start race");
	JButton addButton = new JButton("Add car");
	JLabel carsCount = new JLabel();
	CarRace carRace;
	
	public MyFrame() throws HeadlessException 
	{
		setTitle("F-102502513");
		setBounds(0, 0, 600, 600);
		setLayout(null);
		carRace = new CarRace(this);
		addButton.setBounds(0, 0, 130, 35);
		add(addButton);
		startButton.setBounds(0, 50, 130, 35);
		add(startButton);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(160, 0, 130, 40);
		add(carsCount);
		
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());	
	}
	
	@Override
	public void actionPerformed(ActionEvent e)    //設定'addButton'的功能
	{         
		carRace.addCar();
		carsCount.setText("cars:" + carRace.carNum());
	}
	
	public void showWinner(Car car)   // 顯示winner
	{
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}
	
	class startListener implements ActionListener   //設定'startButton'的功能
	{
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if (carRace.carNum() > 0)   //車數>0 時...
			{
				Thread thread = new Thread(carRace);
				thread.start();
			} 
			else 
			{
				JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
			}
		}
	}
 
}
