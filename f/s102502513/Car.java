package ce1002.f.s102502513;

import java.util.Random;
public class Car 
{
	public int id;
	public float distance;
 
	public Car() 
	{
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed()   //回傳80~120的隨機數
	{
		Random r = new Random();
		return 80 + r.nextFloat()*40;
	}
 
	@Override
	public String toString() 
	{
		return "Car no." + id;
	}
}
