package ce1002.f.s102502015;

public class Car {
	private int speed;
	private int index;
	private int distance=0;

	Car(int i) {
		setIndex(i);
	}

	public void newspeed() {
		speed = (int) (Math.random() * 40 + 80);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
}
