package ce1002.f.s102502015;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	private JButton add = new JButton("Add car");
	private JButton start = new JButton("Start race");
	private JLabel carnums = new JLabel("cars:0");
	private CarRace race = new CarRace();
	private MyPanel paint = new MyPanel(race);

	public MyFrame() {
		this.setLayout(null);
		paint.setLocation(0,100);
		paint.setSize(700, 700);
		add(paint);
		carnums.setLocation(180, 15);
		carnums.setSize(100, 15);
		add.setLocation(10, 10);
		add.setSize(150, 30);
		start.setLocation(10, 40);
		start.setSize(150, 30);
		add(add);
		add(carnums);
		add(start);
		add.addActionListener(this);
		start.addActionListener(this);
		this.setSize(700, 700);
		this.setTitle("F-102502015");
		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == add) {
			race.addcar();
			carnums.setText("cars:"+race.getcarnums());
		}
		if (arg0.getSource() == start) {
			if (race.getcarnums() <= 0) {
				JOptionPane.showMessageDialog(new JFrame(),
						"There must be at least one car!");
			} else {
				race.start();
				new Thread(paint).start();
			}
		}
		// TODO Auto-generated method stub

	}
}
