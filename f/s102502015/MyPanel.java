package ce1002.f.s102502015;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyPanel extends JPanel implements Runnable {
	private CarRace b;

	MyPanel(CarRace a) {
		b = a;
	}

	public void paint(Graphics g) {
		g.clearRect(0, 0, 700, 700);
		for (int i = 0; i < b.getcarnums(); i++) {
			Car a = b.cars.get(i);
			g.setColor(Color.BLACK);
			g.fillRect(0, 200 + i * 15, 4 * (a.getSpeed()), 10);
			g.fillRect(0, 350 + i * 15, 500, 10);
			g.setColor(Color.BLUE);
			g.fillOval(a.getDistance() / 40, 350 + i * 15, 10, 10);
		}
		g.dispose();
	}

	@Override
	public void run() {
		while (!b.end) {
			try {
				for (int i = 0; i < b.getcarnums(); i++) {
					Car a = b.cars.get(i);
					System.out.println(a.getSpeed());
					a.newspeed();
					a.setDistance(a.getDistance() + a.getSpeed());
					repaint();
					if (a.getDistance() > b.racelong) {
						b.end = true;
						JOptionPane.showMessageDialog(new JFrame(),
								"The winner is Car no." + a.getIndex() + "!");
						break;
					}
				}
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// TODO Auto-generated method stub

	}

}
