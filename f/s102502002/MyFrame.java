package ce1002.f.s102502002;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame {

	private JButton addB = new JButton();
	private JButton startB = new JButton();
	private JLabel label = new JLabel();
	private int carCount = 0;
	private CarRace carRace = new CarRace();
	private Result r = new Result();

	MyFrame() { // set the main frame
		setVisible(true);
		setLayout(null);
		setBounds(700, 300, 500, 500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLabel();
		setAddButton();
		setStartButton();
		add(addB);
		add(startB);
		add(label);
		add(r);

	}
	
	private void setLabel(){ // set the label that shows the amount of cars
		label.setVisible(true);
		label.setLayout(null);
		label.setText("cars: "+carCount);
		label.setBounds(170, 10, 80, 30);
	}

	private void setAddButton() { // the button to add cars to the race
		addB.setVisible(true);
		addB.setLayout(null);
		addB.setText("Add car");
		addB.setBounds(10, 10, 120, 30);
		addB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				carRace.addCar();
				carCount = carRace.carNum();
				label.setText("cars: " + carCount);
			}
		});
	}

	private void setStartButton() { // set the button to start the race
		startB.setVisible(true);
		startB.setLayout(null);
		startB.setText("Start race");
		startB.setBounds(10, 60, 120, 30);
		startB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(carCount==0){ // if there is only one car pop the warning
					JOptionPane.showMessageDialog(MyFrame.this, "there must be at least one car!");
				}
				else{ // start the race
					Thread thread = new Thread(carRace);
					thread.start();
				}
			}
		});
	}
	
	public void showWinner(Car car) { // show which car wins
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}

}
