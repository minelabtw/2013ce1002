package ce1002.f.s102502002;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {

	protected MyFrame frame;
	private Car car = new Car();
	protected float totalDistance = 20000;
	protected boolean isFinished = false;
	protected Result r;
	List<Car> cars = new ArrayList<Car>();

	public void addCar() {
		cars.add(car); // add car to list
		car.id = cars.size();
	}
	
	private void runCars() { 
		for (Car car : cars) { // run the cars until one of them reach the distance
			car.distance = car.distance + car.randomSpeed();
			//System.out.println("distance: "+car.distance);
			//r.x=car.distance;
			//r.y=cars.size();
			if (car.distance > totalDistance) {
				frame.showWinner(car);
				isFinished = true;
			}
		}
	}

	public int carNum() { // number of cars
		return cars.size();
	}

	@Override
	public void run() {
		while (!isFinished) { // keep running the cars before the race is finished
			runCars();
		}
	}
}
