package ce1002.f.s102502002;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Result extends JPanel {
	
	float x;
	int y;
	
	Result(){ // set a panel to run the race
		setVisible(true);
		setLayout(null);
		setBounds(10, 120, 360, 480);
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.blue);
		for(int i=0; i<y; i++){ // fill a rectangel for each car as they race
			g.fillRect( 10, 10+(i+1)*20, (int) ((int) 470*x/20000), 20);
		}
	}
}
