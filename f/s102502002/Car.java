package ce1002.f.s102502002;

import java.util.Random;

public class Car {

	public int id;
	public float distance=0;
	
	public float randomSpeed(){
		Random r = new Random(); // get a random speed
		return r.nextFloat() * 100;
	}
	
	public String toString(){
		return "Car no." + id;
	}

}