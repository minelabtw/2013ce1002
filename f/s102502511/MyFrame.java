package ce1002.f.s102502511;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener{
	
	JButton addbButton = new JButton("add car");
	JButton startButton = new JButton("Start");
	JLabel carcount = new JLabel("Cars : 0"); //記錄車輛
	CarRace carRace = new CarRace(this); //呼叫CarRace類別 並且回傳MyFrame給予使用
	
	public MyFrame() {
		// TODO Auto-generated constructor stub
		setTitle("A14-102502511");
		//定位置大小
		setLayout(null);
		setBounds(400,300,550,600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false); 
		setVisible(true);
		
		addbButton.setBounds(0,0,150,50);
		add(addbButton);
		
		startButton.setBounds(0,60,150,50);
		add(startButton);
		
		carcount.setBounds(200,20,100,10);
		add(carcount);
		
		//對按鈕實作actionListener 
		addbButton.addActionListener(this);
		startButton.addActionListener(this);
	}
	
	//若有人勝出 使用Car類別
	public void Winner(Car car){
		//顯示贏的訊息
		JOptionPane.showMessageDialog(this, car.toString());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		//當點擊的時候是 addButton
		if(e.getSource() == addbButton){ 
			//使用記錄車輛的涵式
			carRace.addcar(); 
			//回傳內容
			carcount.setText("Cars : " + carRace.carNum());
			//點擊的是startButton
		}else if(e.getSource() == startButton){
			//若車輛為0 回傳錯誤訊息
			if(carRace.carNum() == 0){
				JOptionPane.showMessageDialog(this, "There must be at least one car!");
				//其他則執行每輛車的跑動行為
			}else{
				//不可以再用迴圈 不然會跑太多次  CarRace中已經有迴圈可以進行每一台車的行動
				//thread運用在CarRace類別中的run
				Thread time = new Thread(carRace);
				time.start();
			}
		}
	}
}
