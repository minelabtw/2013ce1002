package ce1002.f.s102502511;

import java.util.ArrayList;

import javax.swing.JPanel;

public class CarRace implements Runnable{
	
	MyFrame frame;
	//用list 記錄下的car 記錄的內容為car class
	ArrayList<Car> cars = new ArrayList<Car>();
	float totalDistance = 20000;
	boolean isFinished = false;
	
	public CarRace(MyFrame myFrame) {
		frame = myFrame;
	}
	
	//增加車輛 並且標名車號
	public void addcar(){
		cars.add(new Car(carNum()));
	}
	
	//跑動行為
	@Override
	public void run() {
		
		// TODO Auto-generated method stub
		//若沒有人跑完 就繼續跑動
		while(isFinished == false){
			this.runCars();
			//每100毫秒重新random一個速度
			/*try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}
	
	public void runCars(){
		//每輛車的跑動
		for(int i = 0 ; i < carNum() ; i++){
			cars.get(i).distance += cars.get(i).randomSpeed();
		}
		for(int i = 0 ; i < carNum() ; i++){
			//若有車到達終點
			if(cars.get(i).distance >= totalDistance){
				//回傳那輛車的class給MyFrame中的winner
				frame.Winner(cars.get(i));
				//有人到達
				isFinished = true;
				//以免再次跳入
				break;
			}
		}
	}
	
	public int carNum(){
		//回傳list裡面的數量
		return cars.size();
	}
}
