package ce1002.f.s102502511;

import java.util.Random;

public class Car {
	//記錄車的id
		int id; 
		float distance = 0;
		int ranspeed;
		//回傳內容
		String carid;
		
		public Car(int id) {
			// TODO Auto-generated constructor stub
			//保存該車id
			this.id = id;
		}
		
		public float randomSpeed(){
			
			//隨機random出一個速度
			Random ran = new Random();
			ranspeed = ran.nextInt(41) + 80;
			
			return ranspeed;
		}
		
		public String toString(){
			//回傳贏的內容
			carid = "The winner is Car no." + id + "!";
			return carid;
		}
}
