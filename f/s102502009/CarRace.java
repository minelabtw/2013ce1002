package ce1002.f.s102502009;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();
	float totalDistance =20000;
	boolean isFinished = false;
	int carid = 0;

	CarRace(MyFrame a) {
		frame = a;
	}

	public void addCar() {
		cars.add(new Car(carid++));// add car
	}

	private void runCars() {
		for (int i = 0; i < cars.size(); i++) {
			Car a = cars.get(i);
			a.distance = a.distance + a.randomSpeed();
			if (a.distance > totalDistance) {
				isFinished = true;
				frame.showWinner(a);

			}
		}
	}
		
	//}
	public int carNum() {
		return cars.size();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isFinished) {
			runCars();
		}
	}
}
