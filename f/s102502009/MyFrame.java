package ce1002.f.s102502009;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

public class MyFrame extends JFrame implements ActionListener {
	private JButton addButton = new JButton("Add car");
	private JButton startButton = new JButton("Start race");
	private JLabel carsCount = new JLabel("cars:0");
	private CarRace carrace = new CarRace(this);
	private Car car = new Car(this);
	Timer t = new Timer(100, this);
	MyFrame() {
		this.setTitle("F-s102502009"); // frame
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(800, 600);
		this.setLayout(null);
		this.add(addButton);
		this.add(startButton);
		this.add(carsCount);
		addButton.setSize(100, 50);
		addButton.setLocation(10, 10);
		addButton.addActionListener(this);
		startButton.setSize(100, 50);
		startButton.setLocation(10, 70);
		startButton.addActionListener(this);
		carsCount.setSize(100, 30);
		carsCount.setLocation(120, 25);
		this.setVisible(true);
	}
		
	 public void paint(Graphics g) { //長條圖
			super.paint(g);
			for(int i=0 ;i<=carrace.carNum(); i++)
			{
				g.setColor(Color.blue);
				g.fillRect(0, i*10+200, (int)car.randomSpeed()*5, 10);
			}
	}

	

	public void showWinner(Car car) { // show winner
		JOptionPane.showMessageDialog(new JFrame(),
				"The winner is " + car.toString() + "!");
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addButton) {
			carrace.addCar();
			carsCount.setText("cars:" + carrace.carNum());
			
		}
		if (e.getSource() == startButton) {
			if (carrace.carNum() > 0) {
				new Thread(carrace).start();
			} else { // must be at least one car
				JOptionPane.showMessageDialog(new JFrame(),
						"There must be at least one car!");
			}
		}
	}
}
	
	
	

