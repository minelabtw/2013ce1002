package ce1002.f.s102502020;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener{
	
	private JButton addbutton = new JButton("Add car");             //宣告兩個按鈕
	private JButton startbutton = new JButton("Start race");
	private JLabel carcount = new JLabel("cars:0");                 //宣告1個標籤
	private int n = 0;
	
	public MyFrame() {
		addbutton.setBounds(5, 5, 120, 30);                         //排版
		startbutton.setBounds(5, 40, 120, 30);
		carcount.setBounds(170, 6, 150, 30);
		final Carcount carnumber = new Carcount(this);
		addbutton.addActionListener(new ActionListener() {          //按下按鈕時，會觸發以下執行
			
			@Override
			public void actionPerformed(ActionEvent e) {
				carnumber.number = carnumber.number + 1;            //每按一次，車子的數量加1
				carcount.setText("cars:" + carnumber.number);       //標籤輸出的內容改變
				n = carnumber.number;
			}
		});
		startbutton.addActionListener(this);                        //按下按鈕時，會觸發以下執行
		add(addbutton);                                             //把物件加到
		add(startbutton);
		add(carcount);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(n == 0)                                                  //當車子數量為0，則跳出新訊息
		{
			JOptionPane.showMessageDialog(this, "There must be at least one car!");
		}
		else                                                        //否則輸出編號幾的車子獲勝
		{
			Random random = new Random();
			JOptionPane.showMessageDialog(this, "The winner is Car no." + random.nextInt(n));
		}
		Thread thread = new Thread();                               //車子開始比賽
		thread.run();
	}
}
