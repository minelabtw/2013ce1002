package ce1002.f.s100502022;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class MyFrame extends JFrame{
	private JPanel contentPane ,textPane;
	private Button addButton = new Button("add car");
	private Button startButton = new Button("start");
	private JLabel carsCount = new JLabel("cars:0");
	private CarRace carRace = new CarRace(this);
	private speedP speedPanel = new speedP(carRace);
	public MyFrame(){
		//main panel
		contentPane = new JPanel();
		//put text label
		textPane = new JPanel();
		//set contentPane
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		//add button
		addButton.setBounds(5, 5, 100, 40);
		addButton.addActionListener(new addButtonListener());
		contentPane.add(addButton);
		//start button
		startButton.setBounds(5, 50, 100, 40);
		startButton.addActionListener(new startButtonListener());
		contentPane.add(startButton);
		//text label
		textPane.setBounds(150, 5, 100, 40);
		textPane.add(carsCount);
		contentPane.add(textPane);
		speedPanel.setBounds(5, 250, 500, 400);
		contentPane.add(speedPanel);
		this.setTitle("F-100502022");
		this.setBounds(100, 0, 500,700);
		this.setVisible(true);
		this.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
	}
	private class addButtonListener implements ActionListener{
		 
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//Add one car , update the amounts of cars 
			carRace.addCar();
			carsCount.setText("cars:"+carRace.carNum());
		}
	}
	private class startButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//If empty,ShowErrorMsg
			//If not empty, let all these cars run
			if(!showErrorMsg())
			{
				Timer timer = new Timer();
				timer.schedule(carRace, 0, 100);
			}
		}
		//showErrorMsg
		private boolean showErrorMsg(){
			boolean isEmpty=false;
			//Check number
			if(carRace.carNum()==0) isEmpty=true;
			
			if(isEmpty)	JOptionPane.showMessageDialog(null, "There must be at least one car!");
			
			return isEmpty;
		}
		
	}
	//show  which car is winner
	public void showWinner(Car car){
		JOptionPane.showMessageDialog(null, "The winner is "+car.toString());
	}
}
