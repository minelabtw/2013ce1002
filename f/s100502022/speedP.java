package ce1002.f.s100502022;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;

public class speedP extends JPanel {
	protected CarRace r;
	public speedP(CarRace r){
		this.r = r;
		this.setBounds(0,300,500, 300);
	}
	public void paintComponent(Graphics g){
		for (int i = 0; i < r.carNum(); i++) {
			g.setColor(Color.black);
			g.fillRect(5, 0+i*20, (int) r.cars.get(i).randomSpeed()*4, 20);
		}
	}
	
}
