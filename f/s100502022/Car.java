package ce1002.f.s100502022;

import java.util.TimerTask;

public class Car {
	//car's fact
	private float speed;
	public int id;
	public float distance;
	public Car(int id){
		this.id = id;
	}
	//set speed
	public void setSpeed(){
		speed = (float) (Math.random()*40+80);
	}
	public float randomSpeed(){
		return speed;
	}
	//car NO.
	public String toString(){
		String a = "Car No." + id;
		return a;
	}

}
