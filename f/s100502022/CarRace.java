package ce1002.f.s100502022;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
public class CarRace extends TimerTask {
	private MyFrame frame;
	protected List<Car> cars;
	private float totalDistance = 20000;
	private boolean isFinished = false;
	private speedP sp = new speedP(this);
	//new a list to all the cars, get the main frame
	public CarRace(MyFrame frame){
		cars = new ArrayList<Car>();
		this.frame = frame;
		sp.setBounds(5, 250, 500, 400);
		frame.add(sp);
	}
	//new a car,and set its id  
	public void addCar(){
		int id = cars.size();
		Car a = new Car(id);
		cars.add(a);
	}
	@Override
	//if no one reach 5000, continue running
	public void run() {
		// TODO Auto-generated method stub
		if(!isFinished){
			for (int i = 0; i < cars.size(); i++) {
				cars.get(i).setSpeed();
			}
			this.runCars();
			frame.repaint();
		}
		
	}
	//each car run the distance of their speed
	private void runCars(){
		for (int i = 0; i < cars.size(); i++) {
			if(cars.get(i).distance<totalDistance)	cars.get(i).distance +=cars.get(i).randomSpeed();
			else	{
				isFinished = true;
				frame.showWinner(cars.get(i));
				break;
			}
		}
	}
	//car amount
	public int carNum(){
		return cars.size();
	}
}
