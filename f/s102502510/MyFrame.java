package ce1002.f.s102502510;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
 
public class MyFrame extends JFrame implements ActionListener {// �D����
	JButton startButton;
	JButton addButton;
	CarRace carRace;
	JLabel carsCount;
 
	public MyFrame() throws HeadlessException {
		setTitle("F-102502510");
		setBounds(0, 0, 300, 200);
		setLayout(null);
		carRace = new CarRace(this);
		startButton = new JButton();
		addButton = new JButton();
		carsCount = new JLabel();
		startButton.setText("Start race");
		startButton.setBounds(0, 50, 120, 40);
		addButton.setText("add car");
		addButton.setBounds(0, 0, 120, 40);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(addButton);
		add(startButton);
		add(carsCount);
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {
		carRace.addCar();
		carsCount.setText("cars:" + carRace.carNum());
	}
 
	public void showWinner(Car car) {
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}
 
	class startListener implements ActionListener {
 
		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {
				Thread thread = new Thread(carRace);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}
}