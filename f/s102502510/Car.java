package ce1002.f.s102502510;

import java.util.Random;

public class Car {
 
	public int id;
	public float distance;
 
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed() {
		Random r = new Random();
		return r.nextFloat() * 40+80;
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}
}