package ce1002.f.s102502510;

import java.util.ArrayList;
import java.util.List;
 
public class CarRace implements Runnable {// 實作Runnable才能放進thread
 
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}
 
	@Override
	public void run() {
		
		while (!isFinished) {
			runCars();
		}
	}
 
	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();
			if (car.distance >= totalDistance) {
				isFinished = true;
				frame.showWinner(car);
			}
		}
	}
 
	public int carNum() {
		return cars.size();
	}
 
}