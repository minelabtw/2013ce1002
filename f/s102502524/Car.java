package ce1002.f.s102502524;

import java.util.Random;

class Car {

    public int id;
    public float distance;

    //set id
    Car(int id) {
        this.id = id;
        speed speed = new speed();
        speed.count(id);
    }

    //get a random speed
    public float speed() {
        Random rand = new Random();
        return (rand.nextInt(40)+80);
    }

    //get name string
    public String name() {
        return "Car no." + id;
    }
    
    public int now()
    {
    	return id;
    }
}