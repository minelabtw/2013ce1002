package ce1002.f.s102502524;

import javax.swing.*;

import java.io.*;
import java.awt.event.*;

public class MyFrame extends JFrame{
	
	JButton add;
	JButton start;
	JLabel count;
	CarRace race;
	
	
	//configure
    MyFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("F-102502524");
        new_all_member();
        setSize(500, 450);
        setVisible(true);
        
        speed speed = new speed();
        speed.setLocation(60, 90);
		add(speed);
    }
	
  //new and add all member
    void new_all_member() {
        ActionListener listener;
        race = new CarRace();

        add = new JButton("add car");
        listener = new add_listener();
        add.addActionListener(listener);
        add.setBounds(60, 10, 80, 30);
        add(add);

        start = new JButton("start");
        listener = new start_listener();
        start.addActionListener(listener);
        start.setBounds(60, 50, 80, 30);
        add(start);

        count = new JLabel("cars: 0");
        count.setBounds(200, 10, 80, 30);
        add(count);

    }
    
  //listener of add car button
    class add_listener implements ActionListener {

        add_listener() {
        }

        public void actionPerformed(ActionEvent event) {
            race.add();
            int number = race.count();
            count.setText("cars: "+ number);
        }
    }

    //listener of start race button
    class start_listener implements ActionListener {

        start_listener() {
        }

        public void actionPerformed(ActionEvent event) {
            int count = race.count();
            Thread racing = new Thread(race);
            if(count > 0) {
                racing.start();
            } else {
                JOptionPane.showMessageDialog(null, "There must be at least one car!",null, JOptionPane.ERROR_MESSAGE);
            }
            }
    }
}