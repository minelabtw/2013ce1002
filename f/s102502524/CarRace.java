package ce1002.f.s102502524;

import java.awt.Graphics;
import java.util.*;

import javax.swing.*;

class CarRace implements Runnable {

    List<Car> cars;
    float finish;

    //settings
    CarRace() {
        cars = new ArrayList<Car>();
        finish = 20000;
    }

    //add a car
    public void add() {
        int amount = count();
        Car car = new Car(amount);
        cars.add(car);
    }

    //thread run
    public void run() {
        for(Car car : cars) {
            car.distance = 0;
        }

        race_start();
    }

    //race start
    private void race_start() {
        while(true) {
            for(Car car : cars) {
            	float now = car.speed();
            	speed speed = new speed();
            	speed.nowspeed(now);
            	speed.repaint();
                car.distance += now;
                if(car.distance > finish) {
                    winner(car);
                    return;
                }
            }
        }
    }

    //total cars amount
    public int count() {
        return cars.size();
    }

    //show winner
    public void winner(Car car) {
        JOptionPane.showMessageDialog(null, "The winner is "+car.name()+"!",null, JOptionPane.INFORMATION_MESSAGE);
    }

}