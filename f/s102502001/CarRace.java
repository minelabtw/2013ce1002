package ce1002.f.s102502001;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;


public class CarRace implements Runnable{
	MyFrame frame ;
	List<Car> Car ;
	float totalDistance = 5000 ;
	boolean isFinished = false ;
	
	public CarRace(MyFrame Frame){
		frame = Frame;                    //set the attributes
		Car = new ArrayList<Car>();	
		
	}
	
	public void addCar(){
		Car car = new Car();             //add car number and set its ID
		car.ID = Car.size();
		Car.add(car);
	}
	
	public void run(){
		while(!isFinished){             //if the car is not finished, implements State
			State();
		}
	}
	
	public void State(){
		for(Car car: Car){
			car.distance += car.RandomSpeed();
			if(car.distance >= 5000){  //if the distance >5000, finished the race
				isFinished =true;
				frame.showWinner(car); //show the winner message
			}
		}
	}
	
	public int carNumber(){            //set the car number
		return Car.size();
	}	
	
	
	
}
