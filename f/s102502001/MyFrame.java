package ce1002.f.s102502001;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame extends JFrame implements ActionListener{
	private JButton  addcar= new JButton();
	private JButton  start= new JButton();
	private JLabel count = new JLabel(); 
	CarRace carrace ;
	
	public MyFrame() throws HeadlessException {
		setTitle("F-102502001");
		setBounds(0,0,500,500);
		setLocationRelativeTo(null);
		setLayout(null);
		setVisible(true);
		carrace = new CarRace(this);
		addcar.setText("Add car");             //set two buttons 
		addcar.setBounds(5, 5, 100, 40);
		start.setText("Start race");
		start.setBounds(5, 55, 100, 40);
		count.setText("cars:"+carrace.carNumber());  //set the text on label
		count.setBounds(150,5,80,30);
		add(addcar);
		add(start);
		add(count);		
		addcar.addActionListener(this);              //add two actionlisteners
		start.addActionListener(new startListener());
		//Car.add(new RandomGraphPanel());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {
		carrace.addCar();                              //add car to CarRace and change the car number
		count.setText("cars:" + carrace.carNumber());
	}
 
	
 
	class startListener implements ActionListener {
 
		@Override
		public void actionPerformed(ActionEvent e) { //check if the number of the car > 0, if not, show the message
			if (carrace.carNumber() > 0) {
				Thread thread = new Thread(carrace);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
			}
		}
	}
	
	public void showWinner(Car car) {
		String message = "The winner is " + car.toString() + "!";  //show which one is the winner
		JOptionPane.showMessageDialog(this, message);
	}
	
	
	
}
	
	


