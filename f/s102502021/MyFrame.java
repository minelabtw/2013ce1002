package ce1002.f.s102502021;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MyFrame extends JFrame implements ActionListener {
	protected JButton addcar = new JButton("Add car");
	protected JButton startbutton = new JButton("Start race");
	protected JLabel carscount = new JLabel("cars:0");
	protected Car car = new Car();
	int a;

	MyFrame() {
		setTitle("F-102502021");
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300, 150);
		setLocation(0, 0);
		addcar.setLocation(0, 0);
		addcar.setSize(100, 30);
		addcar.addActionListener(this);
		startbutton.setLocation(0, 50);
		startbutton.setSize(100, 30);
		startbutton.addActionListener(this);
		carscount.setLocation(150, 0);
		carscount.setSize(100, 30);
		this.add(addcar);
		this.add(startbutton);
		this.add(carscount);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == addcar) {  //設按下後反應
			car.setCar();
			carscount.setText("cars:" + car.getNum());   //改變標籤
		}
		if (e.getSource() == startbutton) {  //設按下後反應
			if (car.getNum() <=0) {   //取得數字
				JOptionPane.showMessageDialog(new JFrame(),
						"There must be at least one car!");
			}
		}
		
	}
}

