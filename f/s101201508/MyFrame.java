package ce1002.f.s101201508;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener{
	private JButton addButton=new JButton("Add Car");
	private JButton startButton=new JButton("Start race");
	private JLabel label=new JLabel("cars:0");
	private MyPanel panel=new MyPanel();
	private CarRect carRect=new CarRect();
	MyFrame()
	{
		super.setTitle("F-101201508");
		super.setLayout(null);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setSize(500, 500);
		super.setVisible(true);
		
		carRect.addFrame(this);
		carRect.addPanel(panel);
		
		addButton.setBounds(0,0,120,50);
		addButton.addActionListener(this);
		super.add(addButton);
		
		startButton.setBounds(0,50,120,50);
		startButton.addActionListener(this);
		super.add(startButton);
		
		label.setBounds(150,0,120,50);
		super.add(label);
		
		panel.setBounds(0,150,100,100);
		super.add(panel);
		
	}
	public void showWinner(Car car)
	{
		JOptionPane.showMessageDialog(this, "The winner is Car "+car.toString());
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource()==addButton)
		{
			carRect.addCar();
			
			String line="cars:"+carRect.getCar();
			label.setText(line);
		}
		else if (e.getSource()==startButton)
		{
			if (carRect.getCar()==0)
			{
				JOptionPane.showMessageDialog(this, "There must be least one car!");
			}
			else 
			{
				panel.setBounds(0,150,500,25*carRect.getCar()+40);
				Thread thread=new Thread(carRect);
				thread.start();
			}
		}
	}
}
