package ce1002.f.s101201508;

import java.util.Random;

public class Car {
	private int id=0;
	private int speed=0;
	private int distance=0;
	Car(int id)
	{
		this.id=id;
	}
	public int getDistance()
	{
		return distance;
	}
	public int getSpeed()
	{
		return speed;
	}
	public void randomD()
	{
		Random random=new Random();
		speed=80+random.nextInt(40);
		distance=distance+speed;
	}
	public String toString()
	{
		String line="no."+id+"!";
		return line;
	}
	public int getId()
	{
		return id;
	}
}
