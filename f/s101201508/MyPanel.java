package ce1002.f.s101201508;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private ArrayList<Car> cars=new ArrayList<Car>();
	public void setCars(ArrayList<Car> cars)
	{
		this.cars=cars;
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int max=0;
		g.setColor(Color.BLACK);
		for (Car car : cars)
		{
			g.fillRect(0, car.getId()*15, car.getSpeed()*4, 10);
			max=car.getId();
		}
		int more=(max+1)*15+40;
		for (Car car : cars)
		{
			g.setColor(Color.GRAY);
			g.fillRect(0, car.getId()*10+more, 400, 10);
			g.setColor(Color.BLACK);
			g.fillOval(car.getDistance()/50, car.getId()*10+more, 10, 10);
		}
	}
}
