package ce1002.f.s101201508;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class CarRect implements Runnable{
	private ArrayList<Car> cars=new ArrayList<Car>();
	private MyFrame frame;
	private MyPanel panel;
	private int distance=20000;
	private boolean isFinished=false;
	CarRect()
	{
		
	}
	public void addCar()
	{
		Car car=new Car(cars.size());
		cars.add(car);
		
	}
	public void addFrame(MyFrame frame)
	{
		this.frame=frame;
	}
	public void addPanel(MyPanel panel)
	{
		this.panel=panel;
	}
	public int getCar()
	{
		return cars.size();
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (isFinished==false)
		{
			panel.setCars(cars);
			for (Car car : cars)
			{
				car.randomD();
				if (car.getDistance()>=distance)
				{

					frame.showWinner(car);
					isFinished=true;
				}
			}
			panel.repaint();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
