package ce1002.f.s102502003;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class MyFrame extends JFrame {
	
	//create labels and buttons
	private JLabel carsCount = new JLabel();
	private JButton jbtAdd = new JButton("add car");
	private JButton jbtStart = new JButton("start");
	
	//car race
	private CarRace carRace = new CarRace(this);
	
	//create a panel to draw rectangles
	private DrawRect drawPane = new DrawRect();
	
	MyFrame(){
		
		setTitle("A14-102502003");
		setBounds(0, 0, 600, 700);
		setLayout(null);
				
		//counter of cars
		add(carsCount);
		carsCount.setText("cars:" + carRace.carNumber());
		carsCount.setBounds(150, 0, 120, 40);
		
		//add and start
		add(jbtAdd);
		jbtAdd.setBounds(0, 0, 120, 40);
		add(jbtStart);
		jbtStart.setBounds(0, 50, 120, 40);
				
		jbtAdd.addActionListener(new AddButtonListener());
		jbtStart.addActionListener(new StartButtonListener());
		
		//add the panel
		add(drawPane);
		drawPane.setBounds(0, 100, 600, 300);
		
	}
	
	
	public class AddButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			carRace.AddCar();
			carsCount.setText("cars: "+carRace.carNumber());
		}
		
	}
	
	public class StartButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			//start the race
			if(carRace.carNumber() > 0){
				Thread carthread = new Thread(carRace);
				carthread.start();
			}
			else{
				//to show the error message
				JOptionPane.showMessageDialog(null, "There must be at least one car!");
			}
		}

	}

	public void showWinner(Car car) {
		// TODO Auto-generated method stub
		//to print the car's id of the winner
		String message = "The winner is " + car.NumberOfCar() + "!";
		JOptionPane.showMessageDialog(this, message);
				
	}

}
