package ce1002.f.s102502003;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class DrawRect extends JPanel {
	
	private Car car = new Car();
	private int length;
	private int location;	
	
	public DrawRect(){
		length = (int)(car.randomSpeed() * 4.17);
		location = (car.id) * 15;		
	}
	
	
	protected void paintComponent(Graphics g) {	//Draw graph

		super.paintComponent(g);
	
		g.drawRect(0, location, length, 10);
		g.setColor(Color.BLUE);
		g.fillRect(0, location, length, 10);
		

	}
	

	

}
