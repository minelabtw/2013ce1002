package ce1002.f.s102502003;

import java.util.Random;

public class Car {
	
	public int id;
	public float currentdistance;
	
	public Car(){
		id = 0;
		currentdistance = 0;
	}
	
	//set car's current speed
	public float randomSpeed(){
		Random r = new Random();
		float currentspeed = r.nextFloat() * 120 + 80;
		return currentspeed;
	}
	
	public String NumberOfCar(){
		return "Car no." + id;		
	}

}
