package ce1002.f.s102502003;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	
	private MyFrame frame;
	private List<Car> cars;
	private int totalDistance;
	private boolean isFinished;
	
	
	public CarRace(MyFrame frame){
		this.frame = frame;
		totalDistance = 20000; //set the total distance
		isFinished = false;  //when cars don't arrive the final line
		cars = new ArrayList<Car>();
	}
	
	public void AddCar(){
		//add a car to the list
		Car car = new Car();
		car.id = cars.size();  //set the car's id
		cars.add(car);
	}
	
	//running a car
	public void runCars(){
		for(Car car : cars){
			
			//decide the car's current distance
			car.currentdistance = car.currentdistance + car.randomSpeed();
			
			//if cars arrive the final line, the race is over.
			if(car.currentdistance >= totalDistance){
				isFinished = true;
				frame.showWinner(car);
			}
		}
			
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		//if car doesn't arrive the final line, car runs.
		while(!isFinished){
			
			try{
				runCars();
				Thread.sleep(100);
			}
			catch(InterruptedException ex){
				ex.printStackTrace();
			}
			
		}
		
	}
	
	// number of cars
	public int carNumber(){
		return cars.size();
	}
	

}
