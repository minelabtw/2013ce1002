package ce1002.f.s102502519;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame implements ActionListener {

	JButton start = new JButton();
	JButton add = new JButton();
	CarRace carRace = new CarRace(this);
	JLabel count = new JLabel();

	public MyFrame() throws HeadlessException {// 設定邊框及按鈕

		setTitle("F-102502519");
		setBounds(0, 0, 250, 150);
		setLayout(null);
		setVisible(true);

		add.setText("Add car");
		add.setBounds(0, 0, 120, 40);

		start.setText("Start race");
		start.setBounds(0, 50, 120, 40);

		count.setText("cars:" + carRace.carNum());
		count.setBounds(150, 0, 120, 40);

		add(add);
		add(start);
		add(count);

		add.addActionListener(this);
		start.addActionListener(new startListener());
	}

	public void actionPerformed(ActionEvent e) {// 按鈕按下執行
		carRace.addCar();
		count.setText("cars:" + carRace.carNum());
	}

	public void showWinner(Car car) {// 輸出贏家
		String m = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, m);
	}

	class startListener implements ActionListener {//按鈕按下後執行的動作
		startListener() {
		}

		public void actionPerformed(ActionEvent e) {
			if (MyFrame.this.carRace.carNum() > 0) {
				Thread thread = new Thread(MyFrame.this.carRace);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}
}
