package ce1002.f.s102502519;

import java.util.Random;

public class Car {
	public int id;
	public float distance;

	public Car() {//設定初始值
		distance = 0.0F;
		id = 0;
	}

	public float randomSpeed() {// 隨機給予一個速度
		Random r = new Random();
	    float f =0.0F;
	    while(f<80.0F || f>120.0F){
	    	f=r.nextFloat() * 150.0F;
	    }
		return f;
	}

	public String toString() {// 取得id
		return "Car no." + id;
	}
}
