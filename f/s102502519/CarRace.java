package ce1002.f.s102502519;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {

	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;

	
	public CarRace(MyFrame frame) {// 設定各類初始值
		this.frame = frame;
		isFinished = false;
		totalDistance = 20000.0F;
		cars = new ArrayList<Car>();
	}

	public void addCar() {// 加入車子與設定id
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}

	public void run() {//判斷是否到達
		while (!isFinished) {
			runCars();
		}
	}

	private void runCars() {// 運算每一輛車跑的距離並找出最先到達的
		for (Car car : cars) {
			car.distance += car.randomSpeed();
			if (car.distance >= totalDistance) {
				isFinished = true;
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}
}
