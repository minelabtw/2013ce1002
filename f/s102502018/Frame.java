package ce1002.f.s102502018;


import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Frame extends JFrame implements ActionListener {
	JButton startButton;
	JButton addButton;
	CarRace carRace;
	JLabel carsCount;
	JPanel panel = new JPanel();
	
	public Frame() throws HeadlessException {
		setTitle("F-102502018");
		setBounds(0, 0, 300, 500);
		setLayout(null);
		carRace = new CarRace(this);
		startButton = new JButton();
		addButton = new JButton();
		carsCount = new JLabel();
		startButton.setText("start");
		startButton.setBounds(0, 50, 120, 40);
		addButton.setText("add car");
		addButton.setBounds(0, 0, 120, 40);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(addButton);
		add(startButton);
		add(carsCount);
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}

	@Override
	public void actionPerformed(ActionEvent e) {//增加car的數量
		carRace.addCar();								
		carsCount.setText("cars:" + carRace.carNum());
		
	}

	public void showWinner(Car car) {
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}

	class startListener implements ActionListener {//開始比賽

		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {
				Thread thread = new Thread(carRace);
				thread.start();
			} else {
				JOptionPane.showMessageDialog(Frame.this,
						"There must be at least one car!");
			}
		}
	}
}