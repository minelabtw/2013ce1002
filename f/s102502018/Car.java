package ce1002.f.s102502018;

import java.awt.Color;
import java.util.Random;

import javax.swing.JPanel;

public class Car {
 
	public int id;
	public float distance;
	
	public Car() {
		distance = 0;
		id = 0;
		
	}
 
	public float randomSpeed() {//�H���t�� ����80~120
		Random r = new Random();
		return r.nextFloat() * 40+80;
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}
}
