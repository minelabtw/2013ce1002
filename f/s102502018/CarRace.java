package ce1002.f.s102502018;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

public class CarRace implements Runnable {

	Frame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;	
	JPanel panel = new JPanel();
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JPanel panel3 = new JPanel();
	JPanel panel4 = new JPanel();
	JPanel panel5 = new JPanel();
	JPanel panel6 = new JPanel();
	JPanel panel7 = new JPanel();
	JPanel panel8 = new JPanel();
	JPanel panel9 = new JPanel();
	
	
	public CarRace(Frame frame) {
		this.frame = frame;
		isFinished = false;
		totalDistance = 20000;
		cars = new ArrayList<Car>();		
		panel.setBounds(0, 100, 80, 10);
		panel.setBackground(Color.BLACK);
		panel.setVisible(false);
		this.frame.add(panel);
		panel1.setBounds(0, 120, 80, 10);
		panel1.setBackground(Color.BLACK);
		panel1.setVisible(false);
		this.frame.add(panel1);
		panel2.setBounds(0, 140, 80, 10);
		panel2.setBackground(Color.BLACK);
		panel2.setVisible(false);
		this.frame.add(panel2);
		panel3.setBounds(0, 160, 80, 10);
		panel3.setBackground(Color.BLACK);
		panel3.setVisible(false);
		this.frame.add(panel3);
		panel4.setBounds(0, 180, 80, 10);
		panel4.setBackground(Color.BLACK);
		panel4.setVisible(false);
		this.frame.add(panel4);
		panel5.setBounds(0, 200, 80, 10);
		panel5.setBackground(Color.BLACK);
		panel5.setVisible(false);
		this.frame.add(panel5);
		panel6.setBounds(0, 220, 80, 10);
		panel6.setBackground(Color.BLACK);
		panel6.setVisible(false);
		this.frame.add(panel6);
		panel7.setBounds(0, 240, 80, 10);
		panel7.setBackground(Color.BLACK);
		panel7.setVisible(false);
		this.frame.add(panel7);
		panel8.setBounds(0, 260, 80, 10);
		panel8.setBackground(Color.BLACK);
		panel8.setVisible(false);
		this.frame.add(panel8);
		panel9.setBounds(0, 280, 80, 10);
		panel9.setBackground(Color.BLACK);
		panel9.setVisible(false);
		this.frame.add(panel9);
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isFinished) {
			try {
				Thread.sleep(100);
				runCars();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();
			if(cars.size()>0)				//顯示賽車當前速度
			{
				panel.setVisible(true);
				panel.setSize((int)car.randomSpeed(), 10);				
			}
			if(cars.size()>1)
			{
				panel1.setVisible(true);
				panel1.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>2)
			{
				panel2.setVisible(true);
				panel2.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>3)
			{
				panel3.setVisible(true);
				panel3.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>4)
			{
				panel4.setVisible(true);
				panel4.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>5)
			{
				panel5.setVisible(true);
				panel5.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>6)
			{
				panel6.setVisible(true);
				panel6.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>7)
			{
				panel7.setVisible(true);
				panel7.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>8)
			{
				panel8.setVisible(true);
				panel8.setSize((int)car.randomSpeed(), 10);					
			}
			if(cars.size()>9)
			{
				panel9.setVisible(true);
				panel9.setSize((int)car.randomSpeed(), 10);					
			}
			
			
			if (car.distance >= totalDistance) {
				isFinished = true;
				frame.showWinner(car);
			}
			
		}
		
	}

	public int carNum() {
		return cars.size();
	}

}
