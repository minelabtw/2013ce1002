package ce1002.f.s102502014;

public class Car {
	public int id;
	public float distance = 0;

	Car(int a) {   //set ID
		id = a;
	}
	public float randomSpeed() {
		return (float) (Math.random() * 100);   // 回傳0~100 不包含100
	}

	public String toString() {
		return "Car no." + id;
	}
}