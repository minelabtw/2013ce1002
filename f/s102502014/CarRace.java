package ce1002.f.s102502014;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame F;
	List<Car> cars = new ArrayList<Car>(); //初始化array list
	float totalDistance = 20000;
	int carid = 0;
	boolean isFinished = false;

	CarRace(MyFrame a) {
		F = a;
	}

	public void addCar() {
		cars.add(new Car(carid++));   //add car
	}

	private void runCars() {
		for (int i = 0; i < cars.size(); i++) {  //every cars loop
			Car a = cars.get(i);   
			a.distance = a.distance + a.randomSpeed();    //add distance
			if (a.distance > totalDistance) {   //winner
				isFinished = true;
				F.showWinner(a);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isFinished) {
			runCars();
		}
	}
}
