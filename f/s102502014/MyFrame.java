package ce1002.f.s102502014;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	private JButton addButton = new JButton("Add car");
	private JButton startButton = new JButton("Start race");
	private JLabel carsCount = new JLabel("cars:0");
	private CarRace carrace = new CarRace(this);

	MyFrame() {
		this.setTitle("F-102502014");// 版面
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(400, 350);
		this.setLayout(null);
		addButton.setSize(120, 40);
		addButton.setLocation(10, 10);
		addButton.addActionListener(this);
		startButton.setSize(120, 40);
		startButton.setLocation(10, 80);
		startButton.addActionListener(this);
		carsCount.setSize(100, 30);
		carsCount.setLocation(200, 20);
		this.add(addButton);
		this.add(startButton);
		this.add(carsCount);
		this.setVisible(true);
	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(new JFrame(),
				"The winner is " + car.toString() + "!");
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addButton) {
			carrace.addCar();
			carsCount.setText("cars:" + carrace.carNum());
		}
		if (e.getSource() == startButton) {
			if (carrace.carNum() > 0) {
				new Thread(carrace).start();
			} else {    //沒加入任何車
				JOptionPane.showMessageDialog(new JFrame(),
						"There must be at least one car!");
			}
		}
	}
}
