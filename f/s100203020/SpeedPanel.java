package ce1002.f.s100203020;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class SpeedPanel extends JPanel{
	Car car = new Car(1);
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//show speed
		g.setColor(Color.blue);
		g.fillRect(5, 5, 300+(car.speed-80)*6, 10);
	
		
	}
		
	SpeedPanel(Car car){
		this.car = car;
		repaint();
	}
	public void setPanel(Car car){
		this.car = car;
		repaint();
	}
}
