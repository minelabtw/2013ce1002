package ce1002.f.s100203020;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable{
	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();
	int totalDistance = 20000;
	boolean isFinished = false;
	static int number = 0;
	CarRace(MyFrame frame){
		this.frame= frame;
	}
	public void addCar(){
		cars.add(new Car(cars.size()));
		number++;
	}

	
	@Override
	public void run() {
		while (!isFinished) {
				try {
					Thread.sleep(100);  //waiting for 100ms
					runCars();
				} catch (InterruptedException ex) {
					ex.printStackTrace();
				}
				
		    }
	}
	private void runCars(){
		 for (Car car : cars)
		    {
		      car.distance = car.distance +car.randomSpeed();
		      if (car.distance >= totalDistance)
		      {
		        isFinished = true; //end
		        frame.showWinner(car); 
		      }
		    }
	}
	public int carNum(){
		return cars.size();
	}
}