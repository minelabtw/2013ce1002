package ce1002.f.s100203020;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class MyFrame extends JFrame{
	protected static final int carString = 0;
	JButton addButton;
	JButton startButton;
	JPanel Panel1 = new JPanel();
	JPanel Panel2 = new JPanel();
	JPanel Panel3 = new JPanel();
	JLabel carsCount;
	CarRace  carRace = new CarRace(this);
	MyFrame(){
		this.setVisible(true);
		this.setLayout(new FlowLayout());
		this.setTitle("F-100203020");
		addButton = new JButton("Add car");
		startButton = new JButton("Start race");
		carsCount = new JLabel();
		final String carString = "                  cars:";
		carsCount.setText(carString+0);
		Panel1.setLayout(new GridLayout(2,2));
		this.add(Panel1,"North");
		this.Panel1.add(addButton);
		this.Panel1.add(carsCount);
		this.Panel1.add(startButton);
		this.add(Panel2);
		this.add(Panel3);
		
		//addButton ActionListener
		addButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				
				carRace.addCar();
				carsCount.setText(carString+carRace.carNum());

			}
		});
		//startButton ActionListener
		startButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				//If number not 0 ,the game will start
				if (carRace.carNum()==0){
					
			    	JOptionPane.showMessageDialog(new JFrame(), "There must be at least one car!"); 
				}
				else{
					Thread thread = new Thread(carRace);
				    thread.start();
				}
			}
		});
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(300,100);
	}
	
	
	public void showWinner(Car car){
    	JOptionPane.showMessageDialog(new JFrame(), "The winner is "+car.toString()+"!"); 
	}

}	

