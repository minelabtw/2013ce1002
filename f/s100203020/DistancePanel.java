package ce1002.f.s100203020;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class DistancePanel extends JPanel{
	Car car = new Car(1);
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
	
		//show distant
		g.setColor(Color.gray);
		g.fillRect(5, 5, 505, 10);
		g.setColor(Color.red);
		g.fillOval(5+car.distance/40, 5, 10, 10);
		
	}
		
	DistancePanel(Car car){
		this.car = car;
		repaint();
	}
	public void setPanel(Car car){
		this.car = car;
		repaint();
	}
}
