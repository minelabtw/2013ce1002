package ce1002.f.s100203020;
import java.util.Random;
public class Car {
	
	public int id;
	public int distance = 0;
	public int speed = 0;
	Car(int id){
		this.id = id;
	}
	public int randomSpeed(){
		Random ran= new Random();
		this.speed = ran.nextInt(40) + 80;
		return 	this.speed; // random 80~120
	}
	public String toString(){
		return "Car no." + id; 
	}
}

