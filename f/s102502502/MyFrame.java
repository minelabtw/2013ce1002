package ce1002.M2.s102502502;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JButton addButton = new JButton("Add car");
	private JButton startButton = new JButton("Start race");
	private JLabel jlbcarsCount = new JLabel("cars: ");
	CarRace carRace = new CarRace(this);
	RaceJPanel racepanel = new RaceJPanel();
	LocationPanel locationpanel = new LocationPanel();

	public MyFrame() {
		 setTitle("F-102502502");                 //set the attributes of MyFrame 
		 setSize(500,500); 
		 setLayout(null);
		 setDefaultCloseOperation(EXIT_ON_CLOSE);
		 setVisible(true);
		 this.addButton.setBounds(0,0,100,30);         // set bounds of the two buttons and the label
		 this.startButton.setBounds(0,40,100,30);
		 this.jlbcarsCount.setBounds(120,20,100,30);
		 this.jlbcarsCount.setText("cars: " + this.carRace.carNum());                
		 add(this.addButton);                         // add the buttons, label, racepanel and locationpanel  and to MyFrame
		 add(this.startButton);
		 add(this.jlbcarsCount); 
		 add(this.racepanel, BorderLayout.CENTER);
		 add(this.locationpanel, BorderLayout.SOUTH);                            
		 this.addButton.addActionListener(new addButtonListener(this.carRace));
		 this.startButton.addActionListener(new startButtonListener());
	}
	class addButtonListener implements ActionListener{
		CarRace carRace;
	    addButtonListener(CarRace carRace){
	      this.carRace = carRace;
	    }
	    public void actionPerformed(ActionEvent e){   // call the function of carRace and change 
	    	 this.carRace.addCar();                   //  the number of cars in jlbcarsCount               
	    	 jlbcarsCount.setText("cars:" + this.carRace.carNum());
	    }
	}
    class startButtonListener implements ActionListener{
		    startButtonListener(){
		    }    
		    public void actionPerformed(ActionEvent e){
		      if (MyFrame.this.carRace.carNum()<=0){                  // if the number of cars <=0,
		    	  showErrorMsg();	                              
		      }
		      else{                                     
		    	  Thread thread = new Thread(MyFrame.this.carRace);  // then create a thread to operate carRace
		          thread.start(); 
		      }
		    }
		    private void showErrorMsg(){                             // if the number of car <=0, show the following message
		      JOptionPane.showMessageDialog(MyFrame.this, "There must be at least one car!");
		    }    
	 }
	 public void showWinner(Car car){  // show which car is winner!
			JOptionPane.showMessageDialog(this, "The winner is " + car.toString() + "!"); 
	}
}


