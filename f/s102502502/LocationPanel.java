package ce1002.M2.s102502502;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;


public class LocationPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	Car car = new Car();
	public  LocationPanel() {
	      Timer timer1 = new Timer(100, new TimerListener(car.distance));
	      timer1.start();
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.fillOval(0, 0, 5,5);
	}
	  class TimerListener  implements ActionListener{         // create TimerListener to listen the motion and change of graph with time 
		    TimerListener(float f) {                            // set the constructor
		    	f = car.distance;
		    }

		    public void actionPerformed(ActionEvent arg0)         // define the action of TimerListener
		    {                                                     // call the function repaint to redraw the graph
		    	 LocationPanel.this.repaint();
		    }
	  }
}

