package ce1002.M2.s102502502;

public class Car {
	public int id = 0;                  // representation of car
	public float distance = 0.0F;       // how far the car runs
	public Car() {
		// TODO Auto-generated constructor stub
	}	
	public float randomSpeed(){        // return the numbers between 0~100
		float r = (float)((Math.random()*40.0F)+80.0F); 
		return r;
	}
	public String toString(){          // 	return "Car no."+ this.id
		return "Car no."+ this.id;
	}
}
