package ce1002.M2.s102502502;

import java.util.ArrayList;
import java.util.List;


public class CarRace implements Runnable{
	MyFrame frame;
	List<Car> cars;
	float totalDistance = 20000.0F;   // the total distance of race, set its initial value = 5000
	boolean isFinished = false;   // set its initial value = false
	public CarRace(MyFrame farme)
	  {
	    this.frame = farme;
	    this.cars = new ArrayList<Car>();
	  }
	  public void addCar()
	  {
	    Car car = new Car();
	    car.id = this.cars.size();
	    this.cars.add(car);
	  }
	  
	  public void run()                 // when isFinished is false,  keep calling runCars()
	  {
	    while (!this.isFinished){
	      runCars();
	    }
	  }
	  private void runCars()
	  {
	    for (Car car : this.cars){
	      car.distance = car.distance + car.randomSpeed();
	      System.out.println(car.toString() + ", distance=" + car.distance);
	      if (car.distance >= this.totalDistance)   // if one of the cars arrive the finishing line
	      {                                         // set isFinished is true , the race is over
	        this.isFinished = true;
	        this.frame.showWinner(car);             // show the winner
	      }
	    }
	  } 
	  public int carNum()                           // return the size of list cars
	  {
	    return this.cars.size();
	  }
}
