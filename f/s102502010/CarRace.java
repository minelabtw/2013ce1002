package ce1002.f.s102502010;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {  // Runnable

	MyFrame frame;
	List<Car> cars;
	boolean end;

	public CarRace(MyFrame farme) {
		this.frame = farme;
		end = false;
		cars = new ArrayList<Car>();
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();// id
		cars.add(car);
	}

	public void run() {
		while (!end) {// runnung
			runCars();
		}
	}

	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();// Car move
			if (car.distance >= 2000)
			{
				end = true;  // stop
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

}