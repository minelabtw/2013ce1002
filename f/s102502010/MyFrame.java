package ce1002.f.s102502010;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	JButton start = new JButton();
	JButton add = new JButton();
	CarRace carRace = new CarRace(this);
	JLabel carsCount = new JLabel();

	public MyFrame() throws HeadlessException {
		setTitle("F-102502010");
		setBounds(0, 0, 300, 200);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		start.setText("start");
		start.setBounds(0, 50, 130, 50);
		add.setText("add car");
		add.setBounds(0, 0, 130, 50);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(add);
		add(start);
		add(carsCount);
		add.addActionListener(this);
		start.addActionListener(new startListener());
		
	}

	public void actionPerformed(ActionEvent e) {// addButton
		carRace.addCar();
		carsCount.setText("cars:" + carRace.carNum());// new label
	}

	public void showWinner(Car car) {  // winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}

	class startListener implements ActionListener {// startButton

		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0)  // at least one
			{
				Thread thread = new Thread(carRace);  // thread
				thread.start();
			} 
			else 
			{
				JOptionPane.showMessageDialog(MyFrame.this,"There must be at least one car!");
			}
		}
	}
}
