package ce1002.f.s102502010;

import java.util.Random;

public class Car {
 
	public int id;
	public float distance;
 
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed() {  // return random
		Random r = new Random();
		return r.nextFloat() * 100;
	}
 
	public String toString() {
		return "Car no." + id;
	}
}
