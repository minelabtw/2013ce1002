package ce1002.f.s102502529;

import java.util.Random;

public class Car {
	public int id;
	public float hasgone;						//以走距離
	public float Speed;
	Random r = new Random();
	public Car(int id) {
		this.hasgone = 0.0F;
		this.id = id;
	}

	public float Speed() {						//隨機速度
		Speed=r.nextFloat() * 40.0F;
		Speed+=80;
		return Speed;
	}

	public int number(){						//傳出名字
		return id;
	}
}
