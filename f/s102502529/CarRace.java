package ce1002.f.s102502529;

import java.util.ArrayList;

public class CarRace implements Runnable {
	boolean isFinished;
	float distance = 20000;
	MyPanel mp;
	MyFrame jf;
	Car car;
	ArrayList<Status> status = new ArrayList<Status>();
	CarRace(MyPanel mp,MyFrame jf) {
		this.mp = mp;
		this.jf=jf;
		int num=0;
		for (Car car : mp.arr) {										//建立狀態arraylist		
			num++;
			status.add(new Status(0F, num));
			jf.add(status.get(num-1));
			jf.repaint();
		}
		jf.add(new MyPanel(jf));
	}

	@Override
	public void run() {
		
		// TODO Auto-generated method stub
		while (!isFinished) {							//判斷是否結束
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			jf.repaint();
			RaceCar();
		}
	}

	void RaceCar() {
		int i=0;
		for (Car car : mp.arr) {						//取得arraylist 裡面的car物件 並開始賽車
			car.hasgone += car.Speed();
			status.get(i).speed=car.Speed();							//修改速度
			jf.repaint();
			if (car.hasgone >= this.distance&&!isFinished) {			//完成條件
				this.isFinished = true;
				this.mp.Winner(car);					//顯示贏家訊息
			}
			i++;
		}
	}
}
