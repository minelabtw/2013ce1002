package ce1002.f.s102502529;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	int carnumber = 0;
	private JButton addCar;
	private JButton Start;
	private JLabel CarNum;

	MyFrame jf;
	Car car;
	ArrayList<Car> arr = new ArrayList<Car>(); // 存放car arraylist

	MyPanel(final MyFrame jf) {
		this.jf = jf;

		this.setSize(800, 150);
		setVisible(true);
		setLayout(null);
		{
			CarNum = new JLabel(); // 顯示車輛
			add(CarNum);
			CarNum.setText("Cars:" + 0);
			CarNum.setBounds(189, 28, 85, 29);
		}
		{ // 增加車子的按鈕
			addCar = new JButton();
			add(addCar);
			addCar.setText("add car");
			addCar.setBounds(10, 30, 80, 40);
			addCar.addActionListener(new ActionListener() { // 實作按鈕動作
				public void actionPerformed(ActionEvent e) {
					arr.add(new Car(carnumber)); // 宣告新car
					carnumber++;
					CarNum.setText("Cars:" + carnumber);

				}
			});
		}
		{
			Start = new JButton(); // 新增開始按鈕
			add(Start);
			Start.setText("Start");
			Start.setBounds(10, 90, 80, 40);
			Start.addActionListener(new ActionListener() { // 實作動作
				public void actionPerformed(ActionEvent evt) {
					if (carnumber == 0) { // 無車輛輸出錯誤訊息
						JOptionPane.showMessageDialog(null,
								"There must be at least one car!");
					} else { // 開始賽車
						setVisible(false);

						Thread thread = new Thread(
								new CarRace(MyPanel.this, jf));
						thread.start();
					}
				}
			});
		}

	}

	public void Winner(Car car) {
		JOptionPane.showMessageDialog(this, "The winner is " + car.number()
				+ "!");
	}
}
