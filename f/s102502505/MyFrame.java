package ce1002.f.s102502505;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MyFrame extends JFrame {

	int carsCount = 0;// 車子數量
	CarRace carrace;

	MyFrame() {

		carrace = new CarRace(this);
		setLayout(null);

		JButton addbutton = new JButton("Add car");// 建兩個按鈕物件
		JButton startbutton = new JButton("Start race");
		final JLabel label = new JLabel("cars:" + carsCount);

		addbutton.setBounds(0, 0, 150, 50);// 設置位置
		startbutton.setBounds(0, 70, 150, 50);
		label.setBounds(200, 30, 50, 50);

		add(addbutton);// 加物件至frame中
		add(label);
		add(startbutton);

		addbutton.addActionListener(new ActionListener() {// 新增add按鈕監控者
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						carsCount++;
						label.setText("cars:" + carsCount);
						carrace.addCar();
					}
				});

		startbutton.addActionListener(new ActionListener() {// 新增save按鈕監控者
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				if (carsCount == 0) {// 限制car數量為0以上
					final JDialog dialog = new JDialog();
					JButton surebutton = new JButton("確定");
					dialog.setLayout(new FlowLayout());

					dialog.setSize(200, 100);
					dialog.add(new JLabel("There must be at least one car!"));
					dialog.add(surebutton);
					dialog.setVisible(true);

					surebutton.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							dialog.dispose();
						}
					});
				} else {// 如符合，則建立thread並開始執行
					Thread thread = new Thread(carrace);
					thread.start();
				}
			}
		});

	}

	public void showWinner(Car car) {// 建立贏家的視窗畫面
		final JDialog dialog = new JDialog();
		JButton surebutton = new JButton("確定");
		dialog.setLayout(new FlowLayout());

		dialog.setSize(200, 100);
		dialog.add(new JLabel("The winner is " + car.toString() + "!"));
		dialog.add(surebutton);
		dialog.setVisible(true);

		surebutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				dialog.dispose();
			}
		});
	}

}
