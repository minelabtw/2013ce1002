package ce1002.f.s102502505;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.lang.*;
import javax.swing.*;

import javax.swing.*;

public class CarRace implements Runnable {

	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();
	float totalDistance = 20000;
	boolean isFinished = false;
	int i = 0;
	Car car = new Car();

	public CarRace(MyFrame frame) {
		this.frame = frame;
	}

	public void addCar() {// 加新的car.java的物件

		car.id = i;// 設id
		cars.add(car);// 並將新增物件放至list中
		i++;
	}

	private void runCars() {

		for (Car car : cars) {// 利用迴圈將每一輛車都得到一個隨機值，並判斷是否到達終點

			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			car.distance = car.distance + car.randomSpeed();
			if (car.distance > totalDistance) {
				isFinished = true;// 回傳比賽結束
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

	@Override
	public void run() {// thread開始後會跑的地方
		// TODO Auto-generated method stub

		while (true) {
			if (isFinished == true) {
				break;
			} else {
				runCars();
				frame.repaint();
			}
		}
	}

}
