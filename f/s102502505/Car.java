package ce1002.f.s102502505;

import java.awt.*;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Timer;

public class Car {

	public int id;
	public float distance = 0;

	public float randomSpeed() {// 設置隨機速度
		Random random = new Random();
		return (random.nextInt(41) + 80);
	}

	public String toString() {// 回傳id
		return "Car no. " + id;
	}

	public float returndistance() {// 回傳現在跑的值
		return distance;
	}

}
