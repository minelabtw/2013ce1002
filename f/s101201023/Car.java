package ce1002.f.s101201023;

import java.util.Random;

public class Car 
{
	public int id;
	public float speed;
	Random random = new Random();
	
	public Car(int id)
	{
		this.id = id;
	}
	
	//return the distance
	public float CarSpeed()
	{
		speed = random.nextInt(40) + random.nextFloat() + 80;
		
		return speed;
	}
	
	//return name of car
	public String CarName()
	{
		return "Car no." + id;
	}
}