package ce1002.f.s101201023;

import java.util.ArrayList;

public class CarRace implements Runnable
{
	public float runroad = 20000;
	public boolean Arrived;
	MyFrame frame;
	ArrayList<Car> cars = new ArrayList<Car>();
	
	public CarRace(MyFrame frame)
	{
		this.frame = frame;
	}
	
	//add cars
	public void AddCar()
	{
		Car car = new Car(cars.size());
		cars.add(car);
	}
	
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		while(Arrived == false)
			StartRace();
	}
	
	//calculate the distance
	public void StartRace()
	{
		for (Car car : cars)
		{
			car.speed = car.speed + car.CarSpeed();
			if(car.speed > runroad)
			{
				frame.showWinner(car);
				Arrived = true;
			}
		}
	}
	
	//calculate the amount of car
	public int CarAmount()
	{
		return cars.size();
	}
}