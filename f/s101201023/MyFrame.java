package ce1002.f.s101201023;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener
{
	JButton button1 = new JButton("add car");
	JButton button2 = new JButton("start");
	JLabel label = new JLabel("cars:0");
	CarRace CRace = new CarRace(this);
		
	//set frame
	public MyFrame()
	{
		setSize(800 , 800);
		setLayout(null);
		setTitle("F-101201023");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		setVisible(true);
		
		add(button1);
		add(button2);
		add(label);
		
		button1.setBounds(10, 10, 100, 30);
		button2.setBounds(10, 60, 100, 30);
		label.setBounds(150, 0, 100, 50);
		
		//give orders when press abutton
		button1.addActionListener(this);
		button2.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
				if(e.getSource() == button1)//when press button1
				{
					CRace.AddCar();
					label.setText("cars:" + CRace.CarAmount());//change the amount of car
				}
				
				else if(e.getSource() == button2)//when press button2
				{
					try
					{
						//if no car
						if(CRace.CarAmount() == 0)
						{
							throw new Exception();
						}
						
						//if have cars
						else
						{
							Thread thread=new Thread(CRace);
		     				thread.start();
						}
					}
					
					catch(Exception g)
					{
						JOptionPane.showMessageDialog(this, "There must be at least one car!");
					}
				}
	}
	
	//show result
	public void showWinner(Car car)
	{
		JOptionPane.showMessageDialog(this, "The winner is " + car.CarName() + "!");
	}
}