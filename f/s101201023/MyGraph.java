package ce1002.f.s101201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyGraph extends JPanel
{
	float speed;
	int number;
	int order;
	
	public MyGraph(float speed , int number , int order)
	{
		this.speed = speed;
		this.number = number;
		this.order = order;
		setBounds(0,100,750,600);
	}
	
	//paint rectangent
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillRect(0, order*(600/number), (int) speed, 600/number);
	}
}