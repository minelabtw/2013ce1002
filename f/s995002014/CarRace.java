package ce1002.F.s995002014;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {
	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;
	float speed;
 
	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000; //賽道長度
		cars = new ArrayList<Car>();
	}
 
	public void addCar() {
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}
 
	@Override
	public void run() {
		while (!isFinished) {
			runCars();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
 
	private void runCars() {
		for (Car car : cars) {
			speed=car.randomSpeed();
			car.distance += speed;// Car前進
			System.out.println(car.toString() + ", distance=" + car.distance);
		   
			//加入長條圖
			car.p.setSpeed(speed);
			car.p.setBounds(100,150+(car.id*30),140,30);
			frame.add(car.p);
			frame.repaint();
			
			if (car.distance >= totalDistance) {// 過終點線
				isFinished = true;// 停止loop
				frame.showWinner(car);
				break;
			}
		}
	}
 
	public int carNum() {
		return cars.size();
	}
}