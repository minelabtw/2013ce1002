package ce1002.F.s995002014;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Car {
	public int id;
	public float distance;
	public MyPanel p=new MyPanel(0);
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed() {
		Random r = new Random();
		return (r.nextFloat() * 40)+80; // 80~120的隨機數
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}
}

class MyPanel extends JPanel {
	private float speed;
	MyPanel(float speed) {		
		this.speed=speed;
	}
	@Override
	protected void paintComponent(Graphics g) {	//華長條圖
		super.paintComponent(g);
		g.setColor(Color.red);
		g.fillRect(0, 0,(int) speed, 10);
		
	}
	void setSpeed(float s) {
		speed=s;
	}
}