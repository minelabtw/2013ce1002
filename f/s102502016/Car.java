package ce1002.f.s102502016;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Car implements Runnable {
	private int length = 20000;
	private int run = 0;
	private int num;

	Car(int num) {
		this.num = num;
	}

	@Override
	public void run() {
		try {
			while (run < length) {
				int speed;
				speed = (int) (Math.random() * 40 + 80);
				run += speed;
				Thread.sleep(100);
			}
			if(run >= length){
				JOptionPane.showMessageDialog(new JFrame(), "The winer is Car no."+ num);
			}
		} catch (InterruptedException e) {
			System.out.println("Error");
			e.printStackTrace();
		}
	}
}
