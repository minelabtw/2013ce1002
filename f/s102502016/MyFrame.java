package ce1002.f.s102502016;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame implements ActionListener {
	private JButton addc = new JButton("Add car");
	private JButton startr = new JButton("Start race");
	private JLabel num = new JLabel("cars:0");
	private int count = 0;
	private ArrayList<Object> list = new ArrayList<>();

	MyFrame() {
		super("F-102502016");
		addc.setBounds(0, 0, 100, 50);
		startr.setBounds(0, 60, 100, 50);
		num.setBounds(200, 20, 100, 50);
		addc.addActionListener(this);
		startr.addActionListener(this);

		add(addc);
		add(startr);
		add(num);

		setLayout(null);
		setResizable(false);
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(3);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addc) {
			count += 1;
			list.add(new Car(count));
			num.setText("cars:" + count);
		} else {
			if (count == 0) {
				JOptionPane.showMessageDialog(new JFrame(),
						"There must be at least one car!");
			} else {
				for (int i = 0; i < list.size(); i++) {
					new Thread(new Car(count));
				}
			}
		}

	}
}
