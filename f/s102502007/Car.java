package ce1002.f.s102502007;

public class Car {
 
	public int id;
	public float distance;
 
	public Car()
	{
		distance = 0;//每台車位於起點
		id = 0;//編號從0開始
	}
	// 回傳80~120的隨機數
	public float randomSpeed() 
	{
		return (float)((int)(Math.random()*41) + 80);
	}
 
	@Override
	public String toString() 
	{
		return "Car no." + id;
	}
}
