package ce1002.f.s102502007;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
public class CarRace extends JPanel implements Runnable {
 
	MyFrame frame;
	List<Car> cars;//儲存賽車的陣列
	float totalDistance;
	boolean isFinished;
	//constructor
	public float[] distance = new float[100];
	public CarRace(MyFrame farme)
	{
		setLayout(null);
		setSize(500,500);
		this.frame = farme;
		isFinished = false;
		totalDistance = 20000;//賽道總長
		cars = new ArrayList<Car>();
	}
	public void addCar() 
	{
		Car car = new Car();
		car.id = cars.size();
		cars.add(car);
	}
 
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//只要isFinished是false，賽車就會一直跑
		while(!isFinished) 
		{
			runCars();
		}
	}
	private void runCars() 
	{
		int i=0;
		for (Car car : cars) 
		{
			//賽車的速度用亂數決定
			distance[i] = car.randomSpeed();
			car.distance += distance[i];//前進一段距離
			if(car.distance >= totalDistance) 
			{
				//有賽車抵達終點，結束迴圈並顯示勝利者
				isFinished = true;
				frame.showWinner(car);
			}
			i++;
			if(i==cars.size())
			{
				i=0;
			}
		}
	}
	//回傳陣列中目前賽車數
	public int carNum() 
	{
		return cars.size();
	}
	//畫賽車速度長條圖
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
			for(int i=0;i<cars.size();i++)
			{
				g.fillRect(0, 80+20*i, (int)distance[i], 20);
				repaint();
			}
	}
 
}
