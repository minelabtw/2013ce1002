package ce1002.f.s102502007;

import java.awt.event.*;
import javax.swing.*;
 
public class MyFrame extends JFrame implements ActionListener {
	private JButton startButton = new JButton("Start race");
	private JButton addButton = new JButton("Add car");
	private CarRace carRace;
	private JLabel carsCount;
 
	public MyFrame()
	{
		super();
		setTitle("F-102502007");
		setSize(600, 600);
		setLayout(null);
		carRace = new CarRace(this);
		carRace.setLocation(0, 80);
		addButton.setBounds(0, 0, 120, 40);
		startButton.setBounds(0, 50, 120, 40);
		carsCount = new JLabel();
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(addButton);
		add(startButton);
		add(carsCount);
		add(carRace);
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		carRace.addCar();
		carsCount.setText("cars:" + carRace.carNum());
	}
	//��ܳӧQ��
	public void showWinner(Car car) {
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);
	}
 
	class startListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (carRace.carNum() > 0) 
			{
				Thread thread = new Thread(carRace);
				thread.start();
			} 
			else 
			{
				JOptionPane.showMessageDialog(MyFrame.this, "There must be at least one car!");
			}
		}
 

	}
}
