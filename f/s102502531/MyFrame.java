package ce1002.f.s102502531;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	JButton startButton = new JButton("Start race");
	JButton addButton = new JButton("Add car");
	CarRace carRace = new CarRace(this);
	JLabel carsCount = new JLabel("cars:" + carRace.carNum());

	public MyFrame() throws HeadlessException {
		setTitle("F-102502531");
		setBounds(0, 0, 1000, 1000);
		setLayout(null);
		setVisible(true);
        CarRace carRace = new CarRace();
		setLocationRelativeTo(null);
		addButton.setBounds(5, 5, 100, 50);
		startButton.setBounds(5, 65, 100, 50);
		carsCount.setBounds(125, 5, 100, 50);
		addButton.addActionListener(new addListener());
		startButton.addActionListener(new startListener());
         add(carRace);
		add(addButton);
		add(startButton);
		add(carsCount);
		

	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(null, "The winner is " + car.toString()
				+ "!");
	}

	class startListener implements ActionListener {

		public void actionPerformed(ActionEvent e) { // �}�l���s
			try {
				if (carRace.carNum() > 0) {
					Thread thread = new Thread(carRace);
					thread.start();
				}
				if (carRace.carNum() == 0) {
					JOptionPane.showMessageDialog(null,
							"There must be at least one car!");
				}
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	class addListener implements ActionListener {
		public void actionPerformed(ActionEvent e) { // �[�����s
			try {
				carRace.addCar();
				carsCount.setText("cars:" + carRace.carNum());

			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}
	}

	
	}
