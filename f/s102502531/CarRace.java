package ce1002.f.s102502531;

import java.security.ProtectionDomain;
import java.util.*;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 


import javax.swing.JFrame;



public class CarRace  extends JPanel implements Runnable {  
	MyFrame myframe;
	List<Car> arraycar = new ArrayList<Car>();
	float totalDistance = 20000f;
	boolean isFinished = false;  //變數
	boolean Start = false;


	public CarRace(MyFrame myfarme) {  //建構子傳入frame
		this.myframe = myfarme;

	}
	public CarRace() { 
	setBounds(5, 300, 800, 500);

	}
	public void addCar() {  //加車子
		Car car = new Car(arraycar.size());
		arraycar.add(car);
	}

	private void runCars() { //車子動
		for (Car car : arraycar) {

			
			car.distance += car.randomSpeed();
			
			if (car.distance >= totalDistance&&isFinished !=true) {
				isFinished = true;
				myframe.showWinner(car);
				
			}
		}
	}

	

	
	

	public int carNum() {
		return arraycar.size();
	}

	public void run() {
		for (int i = 0; isFinished != true; i++) {
			runCars();
		}
	}

}