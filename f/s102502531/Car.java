package ce1002.f.s102502531;


import java.util.*;

public class Car {
	public int id = 0;  
	public float distance = 0f;
	Random random = new Random();
	
	
	public Car(int id) {
		this.id = id;
	}

	public float randomSpeed() {
		return random.nextFloat() * 40.0f+80;
	}

	public String toString() {
		return "Car no." + id;
	}
}