package ce1002.f.s102502526;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame{
	   	JLabel amount;
	    JButton add_car;
	    JButton start_race;
	    CarRace race;

	    MyFrame() {
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        setLocationRelativeTo(null);
	        setLayout(null);
	        setTitle("F-102502526");
	        new_all_member();
	        setSize(450, 200);
	        setVisible(true);
	    }

	    void new_all_member() {
	        ActionListener listener;
	        race = new CarRace();

	        add_car = new JButton("Add car");  
	        listener = new carListener();
	        add_car.addActionListener(listener);
	        add_car.setBounds(15, 20, 120, 50);
	        add(add_car);

	        start_race = new JButton("Start race");
	        listener = new raceListener();
	        start_race.addActionListener(listener);
	        start_race.setBounds(15, 90, 120, 50);
	        add(start_race);

	        amount = new JLabel("cars: 0");
	        amount.setBounds(180, 20, 80, 30);
	        add(amount);

	    }
	    
	 
	    class carListener implements ActionListener {

	    	carListener() {
	        }

	        public void actionPerformed(ActionEvent event) {
	            race.add_car();
	            int amount2 = race.amount();
	            amount.setText("cars: "+amount2);
	        }
	    }


	    class raceListener implements ActionListener {

	    	raceListener() {
	        }

	        public void actionPerformed(ActionEvent event) { 
	            int amount = race.amount();
	            Thread racing = new Thread(race);
	            if(amount > 0) {
	                racing.start();
	            } else {
	                JOptionPane.showMessageDialog(null, "There must be at least one car!", "�T��", JOptionPane.ERROR_MESSAGE);
	            }
	        }
	    }
}
