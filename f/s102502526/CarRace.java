package ce1002.f.s102502526;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


public class CarRace implements Runnable{
    float totalDistance;
    
    List<Car> cars;
    
    CarRace() {
        cars = new ArrayList<Car>();
        totalDistance = 5000;
    }

    
    public void add_car() {
        int amount = amount();
        Car car = new Car(amount);
        cars.add(car);
    }//add cars

    public void run() {
        for(Car car : cars) {
            car.distance = 0;
        }

        race_start();
    }

    
    private void race_start() {
        while(true) {
            for(Car car : cars) {
                car.distance += car.random_speed();
                if(car.distance > totalDistance) {
                    show_winner(car);
                    return ;
                }
            }
        }
    }//start


    public int amount() {
        return cars.size();
    }

   
    public void show_winner(Car car) {
        JOptionPane.showMessageDialog(null, "The winner is "+car.get_id()+"!", "Race Finish", JOptionPane.ERROR_MESSAGE);
    }
}
