package ce1002.f.s102502517;

public class Car {
	public int id;
	public float distance = 0; //初始化距離為0

	public Car() {
	}

	public float randomSpeed() {
		return (float) (Math.random() * 100); //回傳0到100之間的浮點數
	}

	public String toString() {
		return "Car no." + id; //回傳字串
	}
}
