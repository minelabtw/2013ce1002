package ce1002.f.s102502517;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame {
	private JLabel carLabel;
	private CarRace carRace = new CarRace(this);

	public MyFrame() {
		setLayout(null);
		setSize(400, 200);
		setTitle("F-102502517");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JButton addCarButton = new JButton("add car");
		JButton startButton = new JButton("start");
		carLabel = new JLabel("cars:" + carRace.carNum(), JLabel.CENTER);

		addCarButton.setBounds(0, 0, 100, 40);
		addCarButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carRace.addCar();
				carLabel.setText("cars:" + carRace.carNum()); //更新車數
			}
		});
		add(addCarButton);

		startButton.setBounds(0, 50, 100, 40);
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (carRace.carNum() == 0)
					JOptionPane.showMessageDialog(new JFrame(),
							"There must be at least one car!");
				else {
					Thread thread = new Thread(carRace);
					thread.start(); //開始thread
				}
			}
		});
		add(startButton);

		carLabel.setBounds(110, 0, 100, 40);
		add(carLabel);

		setVisible(true);
	}

	public void showWinner(Car car) {
		JOptionPane.showMessageDialog(new JFrame(),
				"The winner is " + car.toString() + "!"); //顯示贏家

	}
}
