package ce1002.f.s102502517;

public class CarRace implements Runnable {
	private MyFrame frame;
	private int numberOfCars = 0;
	private Car[] cars = new Car[numberOfCars];
	private float totalDistance = 5000;
	private boolean isFinished = false;

	public CarRace(MyFrame frame) {
		this.frame = frame;
	}

	public void addCar() { //增加車數
		numberOfCars++;
		cars = new Car[numberOfCars];
		cars[numberOfCars - 1] = new Car();
	}

	public void run() { //implements run
		for (int i = 0; i <= numberOfCars - 1; i++) {
			cars[i] = new Car();
			cars[i].id = i - 1;
		}

		while (isFinished == false)
			runCars();
	}

	private void runCars() {
		for (int i = 0; i <= numberOfCars - 1; i++) {
			cars[i].distance += cars[i].randomSpeed();
			if (cars[i].distance >= totalDistance) {
				isFinished = true;
				frame.showWinner(cars[i]);
			}
		}
	}

	public int carNum() {
		return cars.length; //回傳cars的長度
	}
}
