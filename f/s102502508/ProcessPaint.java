package ce1002.f.s102502508;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ProcessPaint extends JFrame implements ActionListener {
	
    private JButton jbt=new JButton();
    private JButton jbt1=new JButton();
    private JLabel jbl = new JLabel();
    private CarRace kartrace = new CarRace(this);

ProcessPaint()
{   //建立主視窗並在視窗內加上功能鍵
	
	this.setTitle("F-102502508");
	this.setSize(700, 500);
	this.setLocationRelativeTo(null);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	this.setLayout(null);
	this.setVisible(true);
    jbt.setText("add car");
    jbt.setBounds(0, 0,150, 70);
	jbt1.setText("Start race");
	jbt1.setBounds(0, 100, 150,70);
	jbl.setText("cars: "+0);
	jbl.setBounds(210,21, 180,120);
	this.add(jbt);
	this.add(jbt1);
	this.add(jbl);
	jbt.addActionListener(this);
	jbt1.addActionListener(this);
}
public void Winner(Car car)
{
	JOptionPane.showMessageDialog(new JFrame(),"The winner is " + car.toString() + "!");  //顯示結果視窗
}

@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	if (e.getSource() == jbt) 
	{   
		kartrace.addCar();
		jbl.setText("cars:" + kartrace.carNum());
	}
else if (e.getSource() == jbt1) {//比賽開始的條件
		
	if (kartrace.carNum() > 0) 
	{
		new Thread(kartrace).start();
		
	} 
	else 
	{
		JOptionPane.showMessageDialog(new JFrame(),"There must be at least one car!");
  }
	
}

}
}

