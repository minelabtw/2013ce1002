package ce1002.f.s102502508;
import java.util.ArrayList;
import java.util.List;


public class CarRace implements Runnable {

	ProcessPaint frame;
	List <Car> cars= new ArrayList <Car>();
	float totalDistance =20000 ;
	boolean beFinished= false ;
    public  int kart=0 ;
    
	
	CarRace(ProcessPaint c) 
	{
		frame = c;
	}
	
	public void addCar()
	{ 
		cars.add(new Car(kart=kart+1));   //增加賽車數量
	}
	
	
	private void runCars()//讓賽車不斷前進
	{
		for (int i = 0; i < cars.size(); i=i+1) 
		{
			Car c = cars.get(i);
			c.distance = c.distance + c.randomSpeed();
			if (c.distance > totalDistance)
			{
			    beFinished = true;
			    frame.Winner(c);
			}
		}
	}
	public int carNum()//顯示賽車數量
	{
		return cars.size();
	}

public void run()//在未分出勝負時利用函式讓比賽進行
	{
		while (beFinished== false)
		{
			runCars();
		}
	}
}
