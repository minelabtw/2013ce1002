package ce1002.f.s102502504;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MyFrame extends JFrame
{
	private JButton btn1; //add car
	private JButton btn2; //start
	private JLabel lb; //car:
	
	int carsCount=0;
	
	CarRace cr ;
	
	MyFrame()
	{
		cr=new CarRace(this);
		setTitle("F-102502504"); //標題
		setLayout(null); //排版
		setBounds(300, 150, 500, 500); //大小和位置
		setVisible(true); //可見
		setDefaultCloseOperation(EXIT_ON_CLOSE); //可關
		
		btn1 = new JButton("Add car");
		btn1.setBounds(0,0,100,50);
		btn1.addActionListener(new Button1Listener(this));
		add(btn1);
		
		btn2 = new JButton("Start race");
		btn2.setBounds(0,70,100,50);
		btn2.addActionListener(new Button2Listener(this));
		add(btn2);
		
		lb = new JLabel("cars:"+carsCount);
		lb.setBounds(150,0,100,100);
		add(lb);
		
	}
	
	private class Button1Listener implements ActionListener //add car 的 listener
	{
		JFrame frame;
		Button1Listener(JFrame frame)
		{
			this.frame = frame;
		}
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			carsCount++; //按一下就把carsCount加一
			lb.setText("cars:"+carsCount); //重新設定label的字
			cr.addCar();
		}
	}
	
	private class Button2Listener implements ActionListener //start 的 listener
	{
		JFrame frame;
		Button2Listener(JFrame frame)
		{
			this.frame = frame;
		}

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if(carsCount==0) //如果0台車 要顯示錯誤訊息
			{
				JOptionPane.showMessageDialog(frame, "There must be at least one car!");
			}
			else 
			{
					Thread t = new Thread(cr);
					t.start();
			}
		}
	}
	
	public void showWinner(Car car)
	{
		JFrame frame = null ;
		JOptionPane.showMessageDialog(frame, "The winner is "+car.toString()+"!");
	}
	
	
}
