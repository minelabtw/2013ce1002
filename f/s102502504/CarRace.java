package ce1002.f.s102502504;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable
{
	MyFrame frame;
	List<Car> cars = new ArrayList<Car>();
	float totalDistance;
	boolean isFinished;
	
	int i=0;
	
	CarRace(MyFrame frame)
	{
		this.frame = frame;
		this.totalDistance = 5000;
		this.isFinished = false;
	}
	
	public void addCar()
	{
		Car car = new Car();
		car.id = i;
		cars.add(car); //把car一個個加入list-cars中
		i++;
	}
	
	private void runCars()
	{
		for(Car car : cars)
		{
			car.distance =car.randomSpeed() + car.distance;
			if(car.distance >= totalDistance) //如果超越總長度時，顯示誰是贏家
			{
				frame.showWinner(car);
				isFinished=true; //把完成改成true
			}
		
		}
	}
	
	public int carNum()
	{
		return cars.size();
	}
	
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		while(true)
		{
			if(isFinished==false)
			{
				runCars();	
			}
			else
			{
				break;
			}
		}
		
	}
	
}
