package ce1002.f.s995001561;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
 
@SuppressWarnings("serial")
public class MyFrame extends JFrame implements ActionListener {// 主視窗
	JButton startButton;
	JButton addButton;
	CarRace carRace;
	JLabel carsCount;
 
	public MyFrame() throws HeadlessException {
		setTitle("F-995001561");
		setBounds(0, 0, 500, 500);
		setLayout(null);
		carRace = new CarRace(this);
		startButton = new JButton();
		addButton = new JButton();
		carsCount = new JLabel();
		startButton.setText("Start race");
		startButton.setBounds(0, 50, 120, 40);
		addButton.setText("Add car");
		addButton.setBounds(0, 0, 120, 40);
		carsCount.setText("cars:" + carRace.carNum());
		carsCount.setBounds(150, 0, 120, 40);
		add(addButton);
		add(startButton);
		add(carsCount);
		addButton.addActionListener(this);
		startButton.addActionListener(new startListener());
	}
 
	@Override
	public void actionPerformed(ActionEvent e) {// 當按下addButton
		
		carRace.addCar();// 增加car
		carsCount.setText("cars:" + carRace.carNum());// 更新label
	}
 
	public void showWinner(Car car) {// 顯示winner
		String msg = "The winner is " + car.toString() + "!";
		JOptionPane.showMessageDialog(this, msg);// 對話框
	}
 
	class startListener implements ActionListener {// 處理按下startButton事件
 
		@Override
		public void actionPerformed(ActionEvent e) {
			if (carRace.carNum() > 0) {// 至少要有一輛
				Thread thread = new Thread(carRace);// 建立thread
				thread.start();
			} else {
				JOptionPane.showMessageDialog(MyFrame.this,
						"There must be at least one car!");
			}
		}
	}
	
	
	public void addCar(Car car) {
	add(car);
	}
	

	
}

