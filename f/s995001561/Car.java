package ce1002.f.s995001561;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Car extends JPanel{
 
	public int id;
	public float distance;
	public float speed;
 
	public Car() {
		distance = 0;
		id = 0;
	}
 
	public float randomSpeed() {// 回傳0~100的隨機數
		speed = (int)(Math.random()*41)+80;
		return speed;
	}
 
	@Override
	public String toString() {
		return "Car no." + id;
	}
	
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(0,0,(int)speed,20);
		
	}
	
}

