package ce1002.a13.s102502051;

public class Bank {
	 private int balance = 10000; // bank's total money
	 private boolean busying = false; // indicate that the counter is busy or not
	 // This method simulate the process between the bank's counter and customer
	 // this method always should be synchronized, otherwise it will cause unknown phenomenon happen
	 public synchronized int withDrawSync(int amount ) {
	 // check if the counter is busy, if it does, make the consumer to wait
	 if( busying ){
	 try {
	 wait();
	 } catch (InterruptedException e) {
	 e.printStackTrace();
	 }
	 }
	 balance -= amount ;
	 // set the busying flag to true
	 busying = true;
	 try {
	 // sleep with random interval
	 // simulate process
	 Thread.sleep((int) (Math.random() * 5000));
	 }
	 catch(InterruptedException e) {
	 e.printStackTrace();
	 }
	
	
	 // this indicate the process is done
	 // release the lock and let the next customer come in
	 busying = false;
	 notify();
	 balance -=amount;
	 if( balance <= 0 )
	 return 0;
	 return balance;
	 } 
	 public synchronized int withDrawSync1(int amoun) {
	 // check if the counter is busy, if it does, make the consumer to wait
	 if( busying ){
	 try {
	 wait();
	 } catch (InterruptedException e) {
	 e.printStackTrace();
	 }
	 }
	 // set the busying flag to true
	 busying = true;
	 try {
	 // sleep with random interval
	 // simulate process
	 Thread.sleep((int) (Math.random() * 5000));
	 }
	 catch(InterruptedException e) {
	 e.printStackTrace();
	 }
	 balance += amoun ;
	
	 // this indicate the process is done
	 // release the lock and let the next customer come in
	 busying = false;
	 notify();
	
	 if( balance <= 0 )
	 return 0;
	 return balance;
	 } 
}
