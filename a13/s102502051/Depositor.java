package ce1002.a13.s102502051;

import java.util.Random;

public class Depositor implements Runnable {
	private Bank bank; // a member object of bank to withdraw their money
private int number; // the number of consumer
public Depositor(Bank bank,int number){
this.bank = bank;
this.number = number;
}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random r = new Random(); // RNG
		// we assume each consumer will withdraw money for 10 times
		for(int i = 1; i <= 10; i++) {
		// give a certain amount
		int amoun = r.nextInt(10) * 100;
		// go to bank to save his/her money
		int balanceRemain = bank.withDrawSync1( amoun );
		// print out the result
		System.out.println("No."+ number + " deposit " + amoun + " Current Balance "+ balanceRemain + " count " + i);
		} 
	}

}
