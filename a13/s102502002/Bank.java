package ce1002.a13.s102502002;

public class Bank {

	private int balance=10000;
	private boolean busy=false;
	Bank(){
	}
	public synchronized int withDrawSync (int amount) throws InterruptedException {
		
		if(busy){
			wait(); // make the next thread wait
		}
		busy=true;
		Thread.sleep(20);
		balance = balance - amount; // change the balance
		if(balance<0)
			balance=0;
		busy=false;
		notify(); // notify the next thread
		return balance;
	}
}
