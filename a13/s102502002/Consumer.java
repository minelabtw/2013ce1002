package ce1002.a13.s102502002;

public class Consumer implements Runnable {

	private int no=0;
	private int withdraw=(int) (Math.random()*10)*100;
	private int balance=0;
	private Bank bank;
	Consumer(Bank bank,int n) {
		this.no=n;
		this.bank=bank;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i=0;i<5;i++){
			try {
				balance=this.bank.withDrawSync(withdraw); // withdraw from bank
				System.out.println("No."+no+" consumer withdraw "+withdraw+" Current Balance "+balance+" count "+ (i+1));
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
