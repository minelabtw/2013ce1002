package ce1002.a13.s102502550;

public class Bank {

	int money = 20000;
	int maxMoney = 30000;
	
	public synchronized int withDrawSync(int amount) {           
		
		// if busy ....
		while (money - amount <= 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// do process, use Thread.sleep() to simulate processing
		try {
			// 暫停隨機時間
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		money -= amount;
					// notify() and return
		notify();
		return money;
	}
	
	public synchronized int depositSync(int amount){
		
		
		// if busy ....
		while (money + amount > maxMoney) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// do process, use Thread.sleep() to simulate processing
		try {
			// 暫停隨機時間
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		money += amount;
		
		// notify() and return
		notify();
		return money;
	}
	
	public int getCurrentMoney(){
		return money;
	}
}