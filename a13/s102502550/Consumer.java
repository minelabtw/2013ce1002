package ce1002.a13.s102502550;

public class Consumer implements Runnable {

	private int id;
	Bank bank;

	public Consumer(Bank bank, int id) {
		this.id = id;
		this.bank = bank;
	}

	public void run() {
		for (int i = 1; i <= 5; i++) {
			int x = (int) (Math.random() * 5) * 100;
			
			
			System.out.println("No." + id + " withdraw " + x + " Current Balance " + bank.withDrawSync(x) + " count " + i);
			
		}
	}
}