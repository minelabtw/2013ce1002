package ce1002.a13.s102502550;

public class Depositor implements Runnable{
	private int id;
	Bank bank;

	public Depositor(Bank bank, int id) {
		this.id = id;
		this.bank = bank;
	}

	public void run() {
		for (int i = 1; i <= 5; i++) {
			int x = (int) (Math.random() * 5) * 100;
			System.out.println("No." + id + " deposit " + x + " Current Balance " + bank.depositSync(x) + " count " + i);
		}
	}
}
