package ce1002.a13.s101201521;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		Consumer[] consumers = new Consumer[3];
		Depositor[] depositors = new Depositor[3];
		Thread[] threads = new Thread[6];
		for(int i= 0; i < 3; i++){
			consumers[i] = new Consumer(bank);
			depositors[i] = new Depositor(bank);
			threads[i] = new Thread(consumers[i]);
			threads[i+3] = new Thread(depositors[i]);
			threads[i].start();
			threads[i+3].start();
		}
	}

}
