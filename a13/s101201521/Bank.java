package ce1002.a13.s101201521;

public class Bank{
	public int money = 10000;//total money of the account
	private boolean isOpen = true;
	public Bank(){
		
	}
	public synchronized int withDrawSync(int amount){
		//let the bank can only handle one consumer at one time
		if(!isOpen){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isOpen = false;
		try {
			Thread.sleep(100);//withdraw time
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		money = money - amount;
		if (money <= 0){
			money = 0;
		}
		isOpen = true;
		
		return money;
	}
	public synchronized int depositSync(int amount){
		//let the bank can only handle one consumer at one time
		if(!isOpen){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		isOpen = false;
		try {
			Thread.sleep(100);//withdraw time
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		money = money + amount;
		if (money <= 0){
			money = 0;
		}
		isOpen = true;
		
		return money;
	}
}
