package ce1002.a13.s101201521;

import java.util.Random;

public class Consumer implements Runnable{
	private Bank bank;
	private int count = 1;//count which times the consumer is withdrawing 
	private int consumerNumber;//consumer number
	private static int num = 0;//record how many consumer is constructed
	private Random random = new Random();
	public Consumer(Bank bank){
		this.bank = bank;
		this.consumerNumber = ++this.num;
	}
	@Override
	public void run() {
		for(int i = 0; i < 5; i++ ){
			int amount = random.nextInt(10)*100;//the amount to withdraw
			try {
				Thread.sleep((int)(Math.random()*3000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bank.withDrawSync(amount);
			System.out.println("No." + consumerNumber +
					" consumer" +
					" withdraw " + amount + 
					" Current Balance " + bank.money +
					" count " + count);
			count++;
			
		}
	}

}
