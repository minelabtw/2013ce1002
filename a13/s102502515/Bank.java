package ce1002.a13.s102502515;

public class Bank {
	  boolean isFill = false;//judge whether nobody is using machine
	  private int money = 10000;//total money
	  
	  //check whether 
	  public synchronized int withDrawSync(int amount) {
		  if (isFill == true){
			  //someone is using machine
			  try {
				wait();
			} catch (InterruptedException e) {
				 e.printStackTrace(); 
			}
		  }
		  else
		  {
			  isFill = true;
			  try {
				Thread.sleep(100);
				 money = money - amount;
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		  }
		  
		  isFill = false;
		  notify();//to simulate processing		  
		  if (money < 0){
			  return 0;
		  }
		  else
			  return money;
	     
	  }
	  
	  //deposit 
	  public synchronized int depositSync(int amount) {
		  if (isFill == true){
			  //someone is using machine
			  try {
				wait();
			} catch (InterruptedException e) {
				 e.printStackTrace(); 
			}
		  }
		  else
		  {
			  isFill = true;
			  try {
				Thread.sleep(100);
				 money = money + amount;
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		  }
		  
		  isFill = false;
		  notify();//to simulate processing		  
		  if (money < 0){
			  return 0;
		  }
		  else
			  return money;
	     
	  }
	}
	 