package ce1002.a13.s101201508;

import java.util.Random;

public class Consumer implements Runnable {
	private int id;
	private int count = 0;
	private Bank bank;
	Random random = new Random();

	Consumer(Bank b, int id) {// input the data
		this.bank=b;
		this.id = id;
		count = 0;

	}

	public void show() {// to change the money in the Bank and print the
						// data
		int withdraw = random.nextInt(10) * 100;
		count++;
		System.out.println("NO." + id  + "consumer  withdraw " + withdraw
				+ " Current Balance " + bank.withDrawSync(id, withdraw)
				+ " count " + count);
	}

	@Override
	public void run() {// always do
		for (int i = 0; i < 5; i++) {
			show();
		}
	}

}
