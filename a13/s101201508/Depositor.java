package ce1002.a13.s101201508;

import java.util.Random;

public class Depositor implements Runnable{
	private int id;
	private int count = 0;
	private Bank bank;
	Random random = new Random();

	Depositor(Bank b, int id) {// input the data
		this.bank=b;
		this.id = id;
		count = 0;

	}

	public void show() {// to change the money in the Bank and print the
						// data
		int withdraw = random.nextInt(10) * 100;
		count++;
		System.out.println("NO." + id  + "depositor deposit " + withdraw
				+ " Current Balance " + bank.depositSync(id, withdraw)
				+ " count " + count);
	}

	@Override
	public void run() {// always do
		for (int i = 0; i < 5; i++) {
			show();
		}
	}
}
