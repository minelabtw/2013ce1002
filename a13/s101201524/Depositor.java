package ce1002.a13.s101201524;

public class Depositor implements Runnable{
	private int num;
	private Bank bank;
	private int amount;
	private int counter;
	private int left;
	
	Depositor(Bank b, int n){
		//set the status
		num = n;
		bank = b;
	}
	
	public void run(){
		//deposit money with random amount 5 times
		for(counter = 1; counter <= 5; counter++){
			amount = (int)(Math.random()*10 + 1) * 100;
			left = bank.depositSync(amount);
			System.out.println("No." + num + " depositor deposit " + amount + " Current Balance " + left + " count " + counter);
		}
	}
}
