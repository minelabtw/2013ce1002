package ce1002.a13.s101201524;
import java.util.LinkedList;

public class Bank{
	private int balance = 10000;
	public boolean isbusy = false;
	
	public synchronized int withDrawSync(int amount){
		if(isbusy){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//withdraw money
		isbusy = true;
		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		// count how much money left in bank
		balance -= amount;
		isbusy = false;
		notify();
		if(balance <= 0)
			return 0;
		return balance;
	}
	public synchronized int depositSync(int amount){
		if(isbusy){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//deposit money
		isbusy = true;
		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		//count how much money left in bank
		balance += amount;
		isbusy = false;
        notify();
        if(balance <= 0)
			return 0;
		return balance;
	}
}
