package ce1002.a13.s102502542;

public class A13 
{

	public static void main(String[] args) 
	{
		Bank bank = new Bank();
		//建立5個執行序並且將bank跟客戶(1,2,3)代碼傳入
		Thread thread = new Thread(new Consumer(bank,1));
		Thread thread1 = new Thread(new Consumer(bank,2));
		Thread thread2 = new Thread(new Consumer(bank,3));
		Thread thread3 = new Thread(new Depositor(bank,1));
		Thread thread4 = new Thread(new Depositor(bank,2));
		Thread thread5 = new Thread(new Depositor(bank,3));
        //呼叫class中的run()
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();

	}

}
