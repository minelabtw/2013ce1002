package ce1002.a13.s102502542;

import java.util.Random;

public class Consumer implements Runnable 
{
	Bank bank;//為了呼叫bank中的函式
	int id;
	
	Consumer(Bank b, int i)
	{
		this.bank = b;
		this.id = i;//確認客戶為誰(1,2,3)
	}

	public void run() 
	{
		Random ran = new Random();
		for (int i = 1; i <= 5; i++) 
		{
			int a = 100 * ran.nextInt(10);
			System.out.println("No." + id + " witdraw " + a
					+ " Current Balance " + bank.withDrawSync(a) + " count "
					+ i);
		}
	}
}
