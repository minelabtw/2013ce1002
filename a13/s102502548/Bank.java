package ce1002.a13.s102502548;

public class Bank {
	private int balance = 10000; 
    private boolean busying = false; 
    
    public synchronized int withDrawSync(int amount) {
    	if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}

    	busying = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 500)); //����
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;

    	busying = false;
    	
    	notify();
    	
    	if( balance <= 0 )
    		return 0;
    	
    	return balance;//�s��
    } 
}
