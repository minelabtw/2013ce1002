package ce1002.a13.s102502537;

public class Bank {
	static int money = 10000;

	public Bank() {

	}

	public synchronized int depositSync(int amount) {

		// if busy ....
		// wait()

		// do process, use Thread.sleep() to simulate processing

		try {
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// notify() and return

		return money += amount;
	}

	public synchronized int withDrawSync(int amount) {

		// if busy ....
		// wait()

		// do process, use Thread.sleep() to simulate processing

		try {
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// notify() and return
		if ((money -= amount) < 0)
			return 0;
		return money;
	}

}
