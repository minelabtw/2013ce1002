package ce1002.a13.s102502537;

public class Consumer implements Runnable {
	Bank bank = new Bank();
	int num;

	public Consumer(Bank bank2, int num) {
		this.bank = bank2;
		this.num = num;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 1; i < 6; i++) {
			int withdraw = ((int) (Math.random() * 10)) * 100;
			System.out.println("No." + num + " withdraw " + withdraw
					+ " Current Balance " + bank.withDrawSync(withdraw)
					+ " count " + i);
		}

	}
}
