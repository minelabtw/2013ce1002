package ce1002.a13.s102502519;

public class Bank {

	private int balance = 10000; // 設定初始值
	private boolean busying = false; // 判斷是否busy

	public synchronized int withDrawSync(int amount) { // 取錢
		if (busying) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		busying = true;

		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		balance -= amount; // 從預算扣錢

		busying = false; // 結束並處理小於0的情況
		notify();
		if (balance <= 0)
			return 0;
		return balance;

	}

	public synchronized int depositSync(int amount) { // 儲蓄
		if (busying) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		busying = true;

		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		balance += amount; // 從預算加錢

		busying = false; // 結束
		notify();
		return balance;
	}
}