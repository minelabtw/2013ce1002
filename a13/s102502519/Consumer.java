package ce1002.a13.s102502519;

import java.util.Random;

public class Consumer implements Runnable {
	private Bank bank;
	private int number;

	public Consumer(Bank bank, int number) { // 從Bank取設定的值
		this.bank = bank;
		this.number = number;
	}

	@Override
	public void run() {//執行

		Random r = new Random();

		for (int i = 1; i <= 5; i++) {
			int amount = r.nextInt(10) * 100;
			int balanceRemain = bank.withDrawSync(amount);
			System.out.println("No." + number + " withdraw " + amount
					+ " Current Balance " + balanceRemain + " count " + i);
		}

	}

}