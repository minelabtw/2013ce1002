package ce1002.a13.s102502501;

public class A13 {
	public static void main(String[] args) {
		Bank bank = new Bank(); //宣告一個銀行，提供給三個消費者共同使用
		Consumer[] consumer = new Consumer[3]; //消費者陣列
		Depositor[] depositor = new Depositor[3] ;  //儲蓄者陣列
		Thread[] thread = new Thread[6]; //執行緒陣列
		//初始化兩個陣列
		for( int i = 0 ; i < 3 ; i++ ) {
			consumer[i] = new Consumer(i, bank);
			depositor[i] = new Depositor(i,bank);
			thread[i] = new Thread(consumer[i]);
		}
		for (int i = 3 ; i < 6 ; i++){
			thread[i] = new Thread(depositor[i-3]);
		}
		//啟動執行緒
		for( int i = 0 ; i < 6 ; i++ ) {
			thread[i].start();
		}
	}
}
