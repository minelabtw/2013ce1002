package ce1002.a13.s995002046;

public class Consumer implements Runnable{
	int id;
	Bank bank;
	Consumer(int i, Bank bank){
		//set id and bank
		id=i;
		this.bank=bank;
	}
	@Override  
	public void run(){
		for(int i=0; i<=4; i++){
			int money = ((int) (Math.random()*10))*100;//random money
			int r = bank.withDrawSync(money);//call withDraw
			if(r!=-1)//check if there enough money in account and show
				System.out.println("No."+id+" withdraw "+money+" Current Balance "+r+" count "+(i+1));
			else
				System.out.println("No."+id+" withdraw "+money+" But you don't have enough money in your account. "+"count "+(i+1));
		}
	}
}
