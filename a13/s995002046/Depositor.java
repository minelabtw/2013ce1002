package ce1002.a13.s995002046;

public class Depositor implements Runnable{
	int id;
	Bank bank;
	Depositor(int i, Bank bank){
		//set id and bank
		id=i;
		this.bank=bank;
	}
	@Override  
	public void run(){
		for(int i=0; i<=4; i++){
			int money = ((int) (Math.random()*10))*100;//random money
			int r = bank.depositSync(money);//call deposit
			System.out.println("No."+id+" deposit "+money+" Current Balance "+r+" count "+(i+1));
			
		}
	}
}
