package ce1002.a13.s995002046;

import ce1002.a13.s995002046.Bank;
import ce1002.a13.s995002046.Consumer;


class A13{
	public static void main(String[] args) 
	{//create bank and threads and run the threads
		Bank myBank=new Bank();
		Thread thread1 = new Thread( new Consumer(1,myBank) );
		thread1.start();
		Thread thread2 = new Thread( new Consumer(2,myBank) );
		thread2.start();
		Thread thread3 = new Thread( new Consumer(3,myBank) );
		thread3.start();
		Thread thread4 = new Thread( new Depositor(4,myBank) );
		thread4.start();
		Thread thread5 = new Thread( new Depositor(5,myBank) );
		thread5.start();
		Thread thread6 = new Thread( new Depositor(6,myBank) );
		thread6.start();
	}
}