package ce1002.a13.s995002046;

public class Bank {
	int total =10000;//set money in bank
	int amount = -1; 
	Bank(){
		
	}
	public synchronized int withDrawSync(int amount) {
		 while(this.amount != -1) {//check if someone else is using this account 
	            try { //then wait
	                wait(); 
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            } 
	     }
		 try {//thread delay
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 if(total>=amount){//check total money in bank is enough to withdraw
			 total=total-amount;
		     this.amount=-1;//set this bank free and notify
		     notify();  
			 return total;//return how much you have
		 }
		 else{//if not then return -1
			 this.amount=-1;
		     notify();  
			 return -1;
		 }
		 
	  }
	public synchronized int depositSync(int amount) {
		 while(this.amount != -1) {//check if someone else is using this account 
	            try { //then wait
	                wait(); 
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            } 
	     }
		 try {//thread delay
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 total=total+amount;//add to total
		 this.amount=-1;
		 notify();
		 return total;
		 
		 
	  }
}
