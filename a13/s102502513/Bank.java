package ce1002.a13.s102502513;

public class Bank {
	 
    private int balance = 10000; 
    private boolean busying = false; 
    
    // This method simulate the process between the bank's counter and customer
    // this method always should be synchronized, otherwise it will cause unknown phenomenon happen 
    public synchronized int withDrawSync(int amount) {
    	// check if the counter is busy, if it does, make the consumer to wait
    	if( busying )
    	{
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	// set the busying flag to true
    	busying = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;
    	
    	// release the lock and let the next customer come in
    	busying = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
    
    // This method simulate the process between the bank's counter and depositor
    // this method always should be synchronized, otherwise it will cause unknown phenomenon happen
    public synchronized int DepositorSync(int amount) {
    	// check if the counter is busy, if it does, make the depositor to wait
    	if( busying )
    	{
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	// set the busying flag to true
    	busying = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance += amount;
    	
    	// release the lock and let the next depositor come in
    	busying = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    }   
} 