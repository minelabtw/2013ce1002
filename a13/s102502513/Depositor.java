package ce1002.a13.s102502513;

import java.util.Random;

public class Depositor implements Runnable {
	
	private Bank bank; 
	private int number; 
	
	public Depositor(Bank bank,int number)
	{
		 this.bank = bank; 
		 this.number = number;
	}
	
	public void run() 
	{
		Random r = new Random();
		// we assume each depositor will deposit money for 5 times
		for(int i = 1; i <= 5; i++) 
		{
        int amount =  r.nextInt(10) * 100;
        int balanceRemain = bank.DepositorSync( amount );  //�h�Ȧ�s��    
        System.out.println("No."+ number + " depositor deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
		}       
	}
}
