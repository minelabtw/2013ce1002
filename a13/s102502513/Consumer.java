package ce1002.a13.s102502513;

import java.util.Random;

public class Consumer implements Runnable{
	
	private Bank bank; 
	private int number;
	
	public Consumer(Bank bank,int number)
	{
		 this.bank = bank; 
		 this.number = number;
	}
  
	@Override
	public void run() 
	{
		Random r = new Random(); 
		// we assume each consumer will withdraw money for 5 times
        for(int i = 1; i <= 5; i++) 
        {
            int amount =  r.nextInt(10) * 100;  //�h�Ȧ���
            int balanceRemain = bank.withDrawSync( amount );
            System.out.println("No."+ number + " consumer withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       	
	}
}
