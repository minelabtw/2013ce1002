package ce1002.a13.s102502556;

import java.util.Random;

public class Depositor implements Runnable {
	int id = 0; //存款者編號
	int counter = 0; //存款次數
	Bank bank = new Bank();
	Random rand = new Random();
	Depositor (Bank bank, int id) {
		this.id = id;
		this.bank = bank;	
	}

	public void run() {
		int amount = 0; //存款金額
		//做五次存款的動作
		for (int i = 0 ; i < 5 ; i++) {
			counter++;
			amount = rand.nextInt(10) * 100;
			bank.ChangeofCash(2, id, amount, counter);
		}	
	}
}
