package ce1002.a13.s102502556;

public class Bank {
	int cash = 10000; //存款餘額
	boolean state = false; //銀行當前狀態
	
	Bank () {
		
	}
	public synchronized void ChangeofCash (int type, int id, int amount, int counter) {
		//若銀行已有人在使用，則將之丟到 wait() 之中
		while (state == true) {
			try {
				wait();
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		state = true; //銀行已被使用
		try {
			Thread.sleep(300); //暫停 300ms 
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		//若此人是來領錢的
		if (type == 1) {
			//如果餘額不足，則強制其為0
			if (cash - amount >= 0)
			{
				cash -= amount;
			}
			else {
				cash = 0;
			}
			System.out.println("No." + id +  " consumer withdraw " + amount + " Current Balance " + cash + " count " + counter);
		}
		//若此人是來存錢的
		else {
			cash += amount;
			System.out.println("No." + id +  " depositor deposit " + amount + " Current Balance " + cash + " count " + counter);
		}
		state = false; //銀行已經可讓下一個人使用
		notify(); //讓 wait() 中的下一個人進來
	}
}
