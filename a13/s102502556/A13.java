package ce1002.a13.s102502556;

public class A13 {

	public static void main(String[] args) {
		Bank bank = new Bank(); //共同銀行
		Thread[] take = new Thread[3]; //提款者的執行緒陣列
		Thread[] save = new Thread[3]; //存款者的執行緒陣列
		
		//初始化各陣列中的元素
		for( int i = 0 ; i < 3 ; i++ ) 
		{
			take[i] = new Thread(new Consumer(bank, i+1));
			save[i] = new Thread(new Depositor(bank, i+1));
		}
		//啟動執行緒
		for( int i = 0 ; i < 3 ; i++ )
		{
			take[i].start();
			save[i].start();
		}

	}

}
