package ce1002.a13.s102502560;

public class Consumer extends Customer implements Runnable {
	
	static int nextid=0;
	
	public Consumer(Bank bank) {
		targetBank=bank;
		id=nextid++;
	}
	
	@Override
	public void run() {
		while(targetBank.customerDeposits.get(Consumer.this)==null||targetBank.customerDeposits.get(Consumer.this)>0){
			count++;
			targetBank.withDrawSync(this, (int) (1+2*Math.random()*1000));		//don't withdraw 0
			//System.err.println("No."+id+" consumer now has "+money+" (remained deposit: "+targetBank.customerDeposits.get(Consumer.this)+")");
		}
		//System.err.println("No."+id+" consumer's deposit is EMPTY!");
		//System.err.println("No."+id+" consumer now has "+money);
	}
}
