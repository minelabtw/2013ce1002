package ce1002.a13.s102502560;

public class A13 {
	public static void main(String[] args) {
		
		Bank bank=new Bank();
		Thread bankthread = new Thread( bank );
		bankthread.start();
		
		for(int i=0;i<3;i++){
			Consumer consumer=new Consumer(bank);
			Thread thread = new Thread( consumer );
			thread.start();
		}
		
		for(int i=0;i<3;i++){
			Depositor depositor=new Depositor(bank);
			Thread thread = new Thread( depositor );
			thread.start();
		}
	}
}
