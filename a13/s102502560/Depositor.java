package ce1002.a13.s102502560;

public class Depositor extends Customer implements Runnable {

	static int nextid=0;
	
	public Depositor(Bank bank) {
		money=5000;
		targetBank=bank;
		id=nextid++;
	}
	
	@Override
	public void run() {
		while(money>0){
			count++;
			targetBank.depositSync(this, (int) (1+2*Math.random()*1000));		//don't withdraw 0
			//System.err.println("No."+id+" depositor now has "+money+" (remained deposit: "+targetBank.customerDeposits.get(Depositor.this)+")");
		}
		//System.err.println("No."+id+" depositor's money is EMPTY!");
		//System.err.println("No."+id+" depositor now has "+money);
	}

}
