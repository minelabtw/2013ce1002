package ce1002.a13.s102502560;

import java.util.HashMap;

public class Bank implements Runnable {
	
	int bankNowTotalCurrency=10000;
	HashMap<Customer, Integer> customerDeposits=new HashMap<>();
	boolean requestrefill=false;
	
	synchronized public void restoreMoney() {

		if(requestrefill){
			requestrefill=false;
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			bankNowTotalCurrency=10000;
			System.err.println("Bank refill currency to 10000!");
			notifyAll();
		}
	}
	
	synchronized public int withDrawSync(Consumer consumer, int reqamount) {
		try {
			
			while(reqamount>bankNowTotalCurrency){
				System.err.println("No."+consumer.id+" consumer wants to withdraw "+reqamount
						+" but the bank has only "+bankNowTotalCurrency+". Waiting...");
				requestrefill=true;
				wait();
			}
	
			Thread.sleep(250);		//processing...
			
			int amount=reqamount;
			
			if(!customerDeposits.containsKey(consumer))customerDeposits.put(consumer, 5000);	//new account with $5000
			
			if(reqamount>customerDeposits.get(consumer))amount=customerDeposits.get(consumer);	//if deposit not enough: withdraw remain deposit
			
			customerDeposits.put(consumer, customerDeposits.get(consumer)-amount);				//record withdraw in account
			bankNowTotalCurrency-=amount;														//transfer money from bank to consumer
			consumer.money+=amount;
			
			System.out.println("No."+consumer.id+" consumer withdraw "+amount+" Current Balance "+bankNowTotalCurrency+" count "+consumer.count);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return bankNowTotalCurrency;
	}
	
	synchronized public int depositSync(Depositor depositor, int reqamount) {
		try {
			
			Thread.sleep(250);		//processing...
			
			int amount=reqamount;
			
			if(!customerDeposits.containsKey(depositor))customerDeposits.put(depositor, 0);	//new account with $0

			if(reqamount>depositor.money)amount=depositor.money;								//if deposit not enough: withdraw remain deposit
			
			customerDeposits.put(depositor, customerDeposits.get(depositor)+amount);			//record withdraw in account
			
			depositor.money-=amount;															//transfer money from customer to bank
			bankNowTotalCurrency+=amount;
			
			System.out.println("No."+depositor.id+" depositor deposit "+amount+" Current Balance "+bankNowTotalCurrency+" count "+depositor.count);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return bankNowTotalCurrency;
	}
	
	@Override
	public void run() {
		while(true){
			restoreMoney();
			try {Thread.sleep(1000);} catch (InterruptedException e) {e.printStackTrace();}
		}
	}
	
}
