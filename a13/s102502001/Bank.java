package ce1002.a13.s102502001;

public class Bank {
	 
    private int balance = 10000;                     //set balance = 10000
    private boolean busying = false;                 //judge if the account can use or not
    
    
    public synchronized int withDrawSync(int amount) {
    	
    	if( busying ){                              // check if the counter is busy, if it does, make the consumer to wait
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	
    	busying = true;                            //it is busy
    	
    	try { 
    
            Thread.sleep(500);                     //sleep for 0.5s
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;
    	
    	busying = false;                          //do next thing
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
    
    public synchronized int DepositeSync(int amount) {
    	
    	if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	
    	busying = true;
    	
    	try { 
            
            Thread.sleep(500); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance += amount;
    	
    	
    	
    	busying = false;
    	notify();
    	return balance;
    	
    } 
    
} 

