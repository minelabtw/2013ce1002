package ce1002.a13.s102502001;

import java.util.Random;

public class Depositor implements Runnable{
	private Bank bank; 
	private int number; 
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
	
public void run() {
		
		Random r = new Random(); 
		
        for(int i = 1; i <= 5; i++) {      //deposit for 10 times       	
            int amount =  r.nextInt(10) * 100;
            int balanceRemain = bank.DepositeSync( amount );
            System.out.println("No."+ number + " depositor deposite " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
}
}
