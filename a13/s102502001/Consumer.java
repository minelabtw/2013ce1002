package ce1002.a13.s102502001;
import java.util.Random;


public class Consumer implements Runnable{
	private Bank bank;                 //declare bank
	private int number;                // the number of consumer
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
 
	
	@Override
	public void run() {
		
		Random r = new Random();  
		
        for(int i = 1; i <= 5; i++) {      //withdraw for 5 times
        	
            int amount =  r.nextInt(10) * 100;          
            int balanceRemain = bank.withDrawSync( amount );            
            System.out.println("No."+ number + " consumer withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
 
}
