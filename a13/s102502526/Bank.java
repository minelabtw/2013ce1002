package ce1002.a13.s102502526;

public class Bank {
	private int balance = 123456; 
    private boolean occupied = false; 
    
    public synchronized int withDrawSync(int amount) {   //check bank窗口是否被占用
    	if( occupied ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	occupied = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;
    	
    	occupied = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
    
    public synchronized int depositSync(int amount) {   //consumer存款
    	if( occupied ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	occupied = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance += amount;
    	
    	occupied = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
}
