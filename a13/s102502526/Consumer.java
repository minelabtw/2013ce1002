package ce1002.a13.s102502526;
import java.util.Random;

public class Consumer implements Runnable{
	private Bank bank; //bank class
	private int who; //客戶
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.who = number;
	}
 
	@Override
	public void run() {
		
		Random r = new Random(); 
        for(int i = 1; i <= 5; i++) {
            int amount =  r.nextInt(10) * 100;
            int balanceRemain = bank.withDrawSync( amount );
            // 存取$$
            System.out.println("No."+ who +" consumer withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
            // 輸出結果
        }       
		
	}
 
}
