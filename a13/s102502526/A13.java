package ce1002.a13.s102502526;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();

		// 宣告六個新的執行序
		Thread t1 = new Thread(new Consumer(bank, 1));
		Thread t2 = new Thread(new Consumer(bank, 2));
		Thread t3 = new Thread(new Consumer(bank, 3));
		
		Thread t4 = new Thread(new Depositor(bank, 1));
		Thread t5 = new Thread(new Depositor(bank, 2));
		Thread t6 = new Thread(new Depositor(bank, 3));

		// 執行
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
	}

}
