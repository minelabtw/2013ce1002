package ce1002.a13.s102502532;

public class Bank {

	private int money = 10000;
	boolean state = false;      // 銀行當前狀態

	public Bank() {

	}

	public synchronized int withDrawSync(int amount) {
		// if busy ....
		// wait()
		// do process, use Thread.sleep() to simulate processing
		// notify() and return
		while (state == true) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		state = true;             // 當有人開始領錢的時候，狀態設為true
		try {
			Thread.sleep(500);          // 暫停200ms       //Thread.
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		money = money - amount;
		state = false;              // 領完錢後      狀態回到false
		notify();         // 讓wait裡面的下一個動作開始執行
		
		return money;

	}

	public synchronized int depositSync(int amount) {

		while (state == true) {
			try {                             // try     catch
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		state = true;           // 有人存錢        true
		try {
			Thread.sleep(500);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		money = money + amount;
		state = false;              // 領完錢後      狀態回到false
		notify();
		return money;
	}

}
