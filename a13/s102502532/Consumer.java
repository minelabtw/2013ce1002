package ce1002.a13.s102502532;

public class Consumer implements Runnable {

	private int No = 0;
	// private Bank bank;
	// static Bank bank = new Bank(); //static 全域變數 1個bank
	Bank ONEbank = new Bank();

	public Consumer(Bank bank, int n) {

		No = n;
		ONEbank = bank; // 傳入
	}

	public void run() {
		// withdraw money from bank

		for (int i = 1; i <= 5; i++) {

			int withdraw = 0;
			withdraw = (int) (Math.random() * 10) * 100;
			System.out.println("No." + No + " consumer withdraw " + withdraw + " Current Balance " + ONEbank.withDrawSync(withdraw) + " count " + i);
		}

	}
}
