package ce1002.a13.s102502027;

import java.util.Random;

public class Consumer implements Runnable {
	Bank b;
	int ID;

	Consumer(Bank b, int ID) {
		this.b = b;
		this.ID = ID;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 5; i++) {
			Random r = new Random();
			int a = r.nextInt(10) * 100;
			System.out.println("No." + ID + " consumer withdraw " + a
					+ " Current Balance " + b.withDrawSync(a) + " count " + i);

		}

	}
}
