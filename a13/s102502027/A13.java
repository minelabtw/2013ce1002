package ce1002.a13.s102502027;

public class A13 {
	// initialize a bank
	

	public static void main(String[] args) {
		
		Bank bank = new Bank();
		
		// initialize 3 consumer and wrap with java thread
		Thread thread = new Thread(new Consumer(bank, 1));
		Thread thread1 = new Thread(new Consumer(bank, 2));
		Thread thread2 = new Thread(new Consumer(bank, 3));

		// initialize 3 depositor and wrap with java thread
		Thread thread3 = new Thread(new Depositor(bank, 1));
		Thread thread4 = new Thread(new Depositor(bank, 2));
		Thread thread5 = new Thread(new Depositor(bank, 3));

		// start each thread to perform process
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}
}
