package ce1002.a13.s102502027;

public class Bank {

	int cash = 10000;
	boolean busy;

	Bank() {
		busy = false;
	}

	public synchronized int withDrawSync(int amount) { //����

		if (busy) {

			try {
				wait();
				
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}
		busy = true;
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		cash -= amount;
		if (cash <= 0)
			cash = 0;

		busy = false;

		notify();

		return cash;

	}
	public synchronized int depositSync(int amount) { //�s�ڰʧ@

		if (busy) {

			try {
				wait();
				
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

		}
		busy = true;
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		cash += amount;
		
		busy = false;

		notify();

		return cash;

	}
}
