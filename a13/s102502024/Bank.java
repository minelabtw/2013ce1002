package ce1002.a13.s102502024;

public class Bank {
	int money=5000;
	private boolean busying = false;
	public synchronized int withDrawSync(int amount)
	{
		if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	busying = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        }
		money=money-amount;
		busying = false;
    	notify();
		return money;
	}
}
