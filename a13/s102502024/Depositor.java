package ce1002.a13.s102502024;

import java.util.Random;

public class Depositor implements Runnable{
	int n;
	Bank b;
	
	public Depositor(Bank bank,int number){
		 this.b = bank; 
		 this.n = number;
	}
	@Override
	public void run() {
		Random r = new Random();
		for(int i=1;i<=5;i++)
		{
			int amount =  -r.nextInt(10) * 100;
		    System.out.println("No."+n+"depositor deposit "+-amount+" Current Balance "+b.withDrawSync(amount)+"count "+i);
		}
	}

}
