package ce1002.a13.s102502555;

public class Bank {
	  // please fill the blank
	  int money = 10000;
	  boolean people = false;
	  
	  //領錢用
	  public synchronized int withDrawSync(int amount) {
		if(people){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			people = true;
			
			try {
				Thread.sleep(2000);
				money -= amount;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			people = false; 
		    notify();
		        
		}
		
		if(money <= 0){
	    	return 0;
	    }else {
	    	return money;
	    }
		
	  }  //end withdraw
	  
	  //存錢用
	  public synchronized int depositor(int amount) {
		  if(people){
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				people = true;
				
				try {
					Thread.sleep(2000);
					money += amount;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				people = false; 
			    notify();
			        
			}
		  
		  if(money <= 0){
			  return 0;
		  }else {
		      return money;
		  }
			 
	  }  //end depositor
	  
	}  //end bank