package ce1002.a13.s102502502;

public class Consumer implements Runnable{
	protected int consumer;
    protected Bank bank;
    public Consumer (int consumer, Bank bank){
  	  this.consumer = consumer;
  	  this.bank = bank;
		Thread thread = new Thread(this);      // start thread to perform process   
		thread.start();                    
    }
	  // please override this method when executed by thread
	  public void run(){	    // withdraw money from bank     (// Tell system how to run custom thread)
	        for(int i = 1; i <= 5; i++) {   //consume integrals
	        	int withdrawamount =(int)((Math.random()*10))*100 ;
				System.out.println("No."+ consumer + " withdraw "+ withdrawamount + " Current Balance " + bank.withDrawSync(withdrawamount) + " count" + (i));
	            try { 
	                // do process, use Thread.sleep() to simulate processing
	                Thread.sleep((int)(Math.random() * 3000));   
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            } 
	          }
       }
}

