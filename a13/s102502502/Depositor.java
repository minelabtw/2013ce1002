package ce1002.a13.s102502502;

public class Depositor implements Runnable{
  protected int depositor;
  protected Bank bank;
  public Depositor (int depositor, Bank bank){
	  this.depositor = depositor;
	  this.bank = bank;
		Thread thread = new Thread(this);      // start thread to perform process  
		thread.start();                        
  }
	  // please override this method when executed by thread
	  public void run(){	                       // deposit money to bank   (// Tell system how to run custom thread)
	        for(int i = 1; i <= 5; i++) {          // deposit five times
	        	int depositamount =(int)((Math.random()*10))*100 ;
				System.out.println("No."+ depositor + " deposit "+ depositamount + " Current Balance " + bank.dePositSync(depositamount) + " count" + (i));
	            try { 
	                // do process, use Thread.sleep() to simulate processing
	                Thread.sleep((int)(Math.random() * 3000));   
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            } 
	          }
     }
}
