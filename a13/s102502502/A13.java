package ce1002.a13.s102502502;

public class A13 {

	public static void main(String[] args) {
		// initialize a bank
		Bank bank = new Bank();

		// initialize 3 consumer and wrap with java thread
		new Thread(new Consumer(1, bank));
		new Thread(new Consumer(2, bank));
		new Thread(new Consumer(3, bank));

		// initialize 3 depositor and wrap with java thread
	    new Thread(new Depositor(1, bank));
		new Thread(new Depositor(2, bank));
		new Thread(new Depositor(3, bank));
	}
}
