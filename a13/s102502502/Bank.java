package ce1002.a13.s102502502;

public class Bank {
	protected int balance = 10000;
	protected int amount = 0;        
		
	public synchronized int withDrawSync(int amount) {
		this.amount = amount;
		if (balance < amount){         // if the balance isn't enough
			System.out.println("The current balance isn't enough! Please diminize the digits of withdraw!");
			try {
				wait();                 
	    	}
	    	catch(InterruptedException e) { 
               e.printStackTrace(); 
           }
		}
		else if(balance >= amount){    // if the balance equals the amount
			balance-=amount;
			if ( balance <= 0){        // if there is no money
		    	try {
		    		Thread.sleep((int) (Math.random() * 3000));             //  withdrawing money is paused
		    	}
		    	catch(InterruptedException e){ 
	              e.printStackTrace(); 
		    	}
		    	balance = 0;
			}
		    else if( balance > 0){
		       notify();
		    }
		} 	    
	   	return balance;
	  }
public synchronized int dePositSync(int amount) {
	this.amount = amount;	
	 if(amount > 0){    // if the amount 
	    	try {
	    		Thread.sleep((int) (Math.random() * 3000));             //  depositing money is paused
	    	}
	    	catch(InterruptedException e) { 
              e.printStackTrace(); 
          }
		}
	    else
	    	try {
	    		 wait();                 
	    	}
	    	catch(InterruptedException e) { 
               e.printStackTrace(); 
           }
	       notify();

	 if (amount <= 0)        // if the deposit isn't within the normal range
			return 0;
			
   	return balance;
	  }
  }
	
