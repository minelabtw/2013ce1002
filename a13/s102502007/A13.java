package ce1002.a13.s102502007;

public class A13 extends Thread {

	private A13()
	{
		// initialize a bank
		Bank bank = new Bank();
		
		Thread[] t = new Thread[6];
		
		// initialize 3 consumer and wrap with java thread
		for(int i=0;i<3;i++)
		{
			t[i] = new Thread(new Consumer((i+1) + "", bank));
		}
		
		// initialize 3 depositor and wrap with java thread
		for(int i=3;i<6;i++)
		{
			t[i] = new Thread(new Depositor((i-2) + "", bank));
		}
		
		// start each thread to perform process
		for(int i=0;i<6;i++)
		{
			t[i].start();
		}
	}
	static A13 a13()
	{
		return new A13();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A13.a13();
	}

}
