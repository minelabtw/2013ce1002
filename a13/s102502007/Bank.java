package ce1002.a13.s102502007;

public class Bank {
	
	private int balance = 10000;
	boolean bool = false;//current state
	//withdraw method
	public synchronized int withDrawSync(int amount) throws InterruptedException
	{
		/*
		 * when a user is withdrawing,
		 * set the state is true so that
		 * the other users will wait
		 */
		if(bool)
		{
			wait();
		}
		bool = true;
		//take an one second breath
		Thread.sleep(1000);
		
		balance -= amount;
		//tell the next user that the bank is available now
		notify();
		//reset the state
		bool = false;
		if(balance<=0)
			return 0;
		return balance;
	}
	public synchronized int depositSync(int amount) throws InterruptedException
	{
		/*
		 * when a user is depositing,
		 * set the state is true so that
		 * the other users will wait
		 */
		if(bool)
		{
			wait();
		}
		bool = true;
		//take an one second breath
		Thread.sleep(1000);
		
		balance += amount;
		//tell the next user that the bank is available now
		notify();
		//reset the state
		bool = false;
		if(balance<=0)
			return 0;
		return balance;
	}
}
