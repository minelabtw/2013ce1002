package ce1002.a13.s102502012;

import java.util.Random;

public class Consumer implements Runnable{
	private int id;
	private Bank bank;

	public Consumer(Bank bank, int id){
		this.bank = bank;
		this.id = id;
	}
	
	 public void run(){
		 for(int i = 0; i < 5; i++){
			 Random rand = new Random();
			 int money = rand.nextInt(10) * 100;
			 System.out.println("No." + id + " consumer withdraw " + money + " Current Balance " + bank.withDrawSync(money) + " count " + (i + 1));
		 }
	 }
}
