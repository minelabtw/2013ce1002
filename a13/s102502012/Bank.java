package ce1002.a13.s102502012;

public class Bank {
	private int balance;

	public Bank(){
		balance = 10000;
	}
	
	public synchronized int withDrawSync(int money) {
		try{ // simulate process time
			Thread.sleep((int) (Math.random() * 1000));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if(balance >= money)
			balance -= money;
		else
			balance = 0;
		
		return balance;
	 }
	
	public synchronized int depositeSync(int money){
		try{ // simulate process time
			Thread.sleep((int)(Math.random() * 1000));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		balance += money;
		
		return balance;
	}
}