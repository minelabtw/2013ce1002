package ce1002.a13.s102502527;

public class Bank
{
	private int madimum = 10000;
	private boolean buy = false;
	
	public enum Take 
	{
		WITHDRAW, DEPOSIT
	}
	
	Bank(){}
	
	public synchronized int work(Take take, int amount)
	{
		if (buy)
		{
			try 
			{
				wait();
			} 
			catch (InterruptedException i) 
			{
				i.printStackTrace();
			}
		}
		
		buy = true;
		
		try 
		{
			Thread.sleep(200);
		} 
		catch (InterruptedException i) 
		{
			i.printStackTrace();
		}
		
		if ( take == Take.WITHDRAW )
		{
			madimum -= amount;
		}
		else if ( take == Take.DEPOSIT )
		{
			madimum += amount;
		}
		
		
		
		buy = false;
		notify();
		
		return ( madimum < 0 ) ? 0 : madimum;
	}
}
