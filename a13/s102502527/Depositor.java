package ce1002.a13.s102502527;

import java.util.Random;

import ce1002.a13.s102502527.Bank.Take;

public class Depositor implements Runnable
{
	int number;
	
	Bank bank;
	
	private static Random r = new Random();
	
	Depositor( Bank bank , int number )
	{
		this.bank = bank;
		this.number = number;
	}
	
	@Override
	public void run()
	{
		for( int i = 1 ; i <= 5 ; i++ )
		{
			int a = r.nextInt(10) * 100;
			int r = bank.work(Take.DEPOSIT, a);
			System.out.println( "No." + number + " depositor deposit " + a + " Current Balance "+ r + " count " + i );
		}
	}
}
