package ce1002.a13.s102502527;

import java.util.Random;

import ce1002.a13.s102502527.Bank.Take;

public class Consumer implements Runnable
{
	int number;
	
	Bank bank;
	
	private static Random r = new Random();
	
	Consumer(Bank bank, int number)
	{
		this.bank = bank;
		this.number = number;
	}
	
	@Override
	public void run()
	{
		for( int i = 1 ; i <= 5 ; i++ )
		{
			int a = r.nextInt(10) * 100;
			int b = bank.work(Take.WITHDRAW, a);
			
			System.out.println( "No." + number + " consumer withdraw " + a + " Current Balance "+ b + " count " + i);
		}
	}
}
