package ce1002.a13.s102502527;

public class A13
{
	public static void main(String[] args)
	{
		Bank bank = new Bank();
		
		Thread c1 = new Thread(new Consumer(bank, 1));
		Thread c2 = new Thread(new Consumer(bank, 2));
		Thread c3 = new Thread(new Consumer(bank, 3));
		
		Thread d1 = new Thread(new Depositor(bank, 1));
		Thread d2 = new Thread(new Depositor(bank, 2));
		Thread d3 = new Thread(new Depositor(bank, 3));
		
		c1.start();
		c2.start();
		c3.start();
		
		d1.start();
		d2.start();
		d3.start();
	}	
}

