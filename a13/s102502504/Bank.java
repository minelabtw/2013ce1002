package ce1002.a13.s102502504;

public class Bank 
{
	static int money = 10000; //money in the bank
	  
	  public synchronized static int withDrawSync(int amount) //讀到consumer內的bank.withDrawSync(amount)時進入
	  {
			try
			{
				Thread.sleep(10); //遇到這邊就跑下一個thread
				money = money - amount;
			}
		    catch(Exception e)
		    {
				 e.getStackTrace();
		    }
			
			if(money < 0)
			    return 0;
			else
				return money;
	 }
	  
	  public synchronized static int depositSync(int amount) //讀到depositor內的bank.depositDrawSync(amount)時進入
	  {
		  try
			{
			  	Thread.sleep(10); //遇到這邊就跑下一個thread
				money = money + amount;
			}
		    catch(Exception e)
		    {
				 e.getStackTrace();
		    }
			
			if(money < 0)
			    return 0;
			else
				return money;
		  
	  }
}
