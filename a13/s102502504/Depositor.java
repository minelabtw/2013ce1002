package ce1002.a13.s102502504;

import java.util.Random;

public class Depositor implements Runnable 
{
	int number; //第幾個人
	Bank bank = new Bank();
	
	Random r = new Random();
	
	Depositor(Bank bank,int number)
	{
		this.number = number;
		this.bank = bank;
	}
	
	@Override
	public void run() //存錢
	{	
		  for(int i=1;i<=5;i++)
		  {
			  int amount = (int) (r.nextInt(10)*100) ; //隨機領0~1000的數
			  System.out.println("No."+ number +" depositor deposit "+ amount + " Current Balance " +bank.depositSync(amount)+" count "+i);
		  }
		
	}
}
