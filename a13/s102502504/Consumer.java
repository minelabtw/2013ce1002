package ce1002.a13.s102502504;

import java.util.Random;

public class Consumer implements Runnable 
{
	int number; //第幾個人
	Bank bank;
	
	Random r = new Random();
	
	Consumer(Bank bank,int number)
	{
		this.number = number;
		this.bank = bank;
	}
	
	@Override
	public void run() 
	{	  // withdraw money from bank
		  for(int i=1;i<=5;i++)
		  {
			  int amount = (int) (r.nextInt(10)*100); //隨機領0~1000的數
			  System.out.println("No."+ number +" consumer withdraw "+ amount + " Current Balance " +bank.withDrawSync(amount)+" count "+i);
		  }
		
	}

}
