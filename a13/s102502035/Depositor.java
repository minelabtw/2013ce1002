package ce1002.a13.s102502035;

public class Depositor implements Runnable {
	Bank bank;
	int no;

	public Depositor(Bank bank, int no) {
		this.bank = bank;
		this.no = no;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 1; i < 6; i++) {
			int deposit = (int) (Math.random() * 10) * 100;
			bank.depositSync(no, deposit, i);

		}

	}
}
