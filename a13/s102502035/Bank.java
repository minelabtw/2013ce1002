package ce1002.a13.s102502035;

public class Bank {
	private int money = 10000;

	private static boolean using = false;

	public Bank() {

	}

	public synchronized void usesynchronized() {

	}

	public synchronized void depositSync(int no, int amount, int i) {
		// two method synchronized
		usesynchronized();
		// if busy ....
		// wait()
		try {
			if (using)
				wait();

		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		using = true;
		// do process, use Thread.sleep() to simulate processing
		try {
			Thread.sleep((int) (Math.random() * 100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		money += amount;
		// notify() and return
		notify();
		using = false;
		System.out.println("No." + no + " depositor " + amount
				+ " Current Balance " + money + " count " + i);

	}

	public synchronized void withDrawSync(int no, int amount, int i) {
		// two method synchronized
		usesynchronized();
		// if busy ....
		// wait()
		try {
			if (using)
				wait();

		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		using = true;
		// do process, use Thread.sleep() to simulate processing
		try {
			Thread.sleep((int) (Math.random() * 100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if ((money -= amount) < 0)// if someone withdraw all of left amount is 0
			money = 0;
		// notify() and return
		notify();
		using = false;
		System.out.println("No." + no + " withdraw " + amount
				+ " Current Balance " + money + " count " + i);

	}
}
