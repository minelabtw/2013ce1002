package ce1002.a13.s102502547;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		Consumer c1 = new Consumer(1, bank);
		Consumer c2 = new Consumer(2, bank);
		Consumer c3 = new Consumer(3, bank);
		Depositor d1 = new Depositor(1, bank);
		Depositor d2 = new Depositor(2, bank);
		Depositor d3 = new Depositor(3, bank);
		new Thread(c1).start();
		new Thread(c2).start();
		new Thread(c3).start();
		new Thread(d1).start();
		new Thread(d2).start();
		new Thread(d3).start();
	}

}
