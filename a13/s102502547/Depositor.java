package ce1002.a13.s102502547;

public class Depositor implements Runnable {

	private int id;
	Bank bank;

	public Depositor(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}

	public void run() {
		for (int i = 1; i <= 5; i++) {
			int x = (int) (Math.random() * 5) * 100;
			System.out.println("No." + id + " depositor deposit " + x + " Current Balance " + bank.depositSync(x) + " count " + i);
		}
	}
}