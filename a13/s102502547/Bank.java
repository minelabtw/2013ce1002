package ce1002.a13.s102502547;

public class Bank {
	int money = 20000;
	boolean x = true;

	public synchronized int withDrawSync(int amount) {
		// if busy ....
		while (x == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// do process, use Thread.sleep() to simulate processing
		try {
			// 暫停隨機時間
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		money -= amount;
		if (money < 0)
			money = 0;
		x = true;

		// notify() and return
		notify();
		return money;
	}
	
	public synchronized int depositSync(int amount) {
		// if busy ....
		while (x == false) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// do process, use Thread.sleep() to simulate processing
		try {
			// 暫停隨機時間
			Thread.sleep((int) (Math.random() * 1000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		money += amount;
	
		x = true;

		// notify() and return
		notify();
		return money;
	}
}
