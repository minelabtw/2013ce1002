package ce1002.a13.s102502037;

public class Bank {
	static int money = 10000;

	public Bank() {

	}

	public synchronized int withDrawSync(int amount) {

		// do process, use Thread.sleep() to simulate processing

		try {
			Thread.sleep((int) (Math.random() * 500));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// notify() and return
		if ((money -= amount) < 0)// if someone withdraw all of left amount is 0
			return 0;
		return money;
	}
	public synchronized int depositSync(int amount) {

		// do process, use Thread.sleep() to simulate processing

		try {
			Thread.sleep((int) (Math.random() * 500));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// notify() and return
		if ((money += amount) < 0)// if someone withdraw all of left amount is 0
			return 0;
		return money;
	}
}
