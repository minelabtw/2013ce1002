package ce1002.a13.s102502037;



public class A13 {
		public static void main(String[] args) {
			// TODO Auto-generated method stub// System.out.print((int)
			// (Math.random() * 1000));
			// initialize 3 consumerˇBDepositor and wrap with java thread

			Consumer c1 = new Consumer(1);
			Consumer c2 = new Consumer(2);
			Consumer c3 = new Consumer(3);
			Depositor c4 = new Depositor(1);
			Depositor c5 = new Depositor(2);
			Depositor c6 = new Depositor(3);
			Thread thread1 = new Thread(c1);
			Thread thread2 = new Thread(c2);

			Thread thread3 = new Thread(c3);
			Thread thread4 = new Thread(c4);
			Thread thread5 = new Thread(c5);

			Thread thread6 = new Thread(c6);
			// ** this is how you start a thread **
			thread1.start();
			thread6.start();
			thread2.start();
			thread3.start();
			thread4.start();
			thread5.start();
		}
	}

