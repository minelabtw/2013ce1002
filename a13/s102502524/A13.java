package ce1002.a13.s102502524;

public class A13 {
	public static void main(String[] args) {
		/*先共用存款和提款的銀行，然後執行存提款的動作*/
		Bank bank = new Bank();
		Consumer consumers[] = new Consumer[3];
		Depositor depositor[] = new Depositor[3];
		for(int i = 0; i < 3; i++){
			consumers[i] = new Consumer(i + 1, bank);
			depositor[i] = new Depositor(i + 1, bank);
			Thread thread = new Thread(consumers[i]);
			Thread thread1 = new Thread(depositor[i]);
			thread.start();
			thread1.start();
		}
	}

}
