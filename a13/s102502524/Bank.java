package ce1002.a13.s102502524;

public class Bank {
	private int balance;
	private boolean withdrawing;
	private boolean depositing;
	public Bank(){
		balance = 20000;
		withdrawing = false;
		depositing = false;
	}
	
	/*執行提款作業*/
	public synchronized int withDrawSync(int amount) {
		if(withdrawing){ // someone is withdrawing 
			try{
				wait(); // until the one has done withdraw
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		withdrawing = true;
		try{
			Thread.sleep((int) (Math.random() * 1000));
		}catch(Exception e){
			e.printStackTrace();
		}
		if(balance >= amount)
			balance -= amount;
		else
			balance = 0;
		withdrawing = false;
		
		notify(); // 
		
		return balance;
	 }
	
	/*執行存款作業*/
	public synchronized int DepositSync(int amount) {
		if(depositing){ // someone is depositing 
			try{
				wait(); // until the one has done deposit
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		depositing = true;
		try{
			Thread.sleep((int) (Math.random() * 1000));
		}catch(Exception e){
			e.printStackTrace();
		}
		balance += amount;
		depositing = false;
		
		notify(); // 
		
		return balance;
	 }
}