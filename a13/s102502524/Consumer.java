package ce1002.a13.s102502524;

import java.util.Random;

public class Consumer implements Runnable{
	private int id;
	private static Bank bank = new Bank();

	public Consumer(int id , Bank bank){
		this.id = id;
		this.bank = bank;
	}
	
	 public void run(){
		 for(int i = 0; i < 5; i++){
			 Random rand = new Random();
			 int money = rand.nextInt(501) + 500;
			 System.out.println("No." + id + " consumer withdraw " + money + " Current Balance " + bank.withDrawSync(money) + " count " + (i + 1));
		 }
	 }
}
/*���ڤH*/