package ce1002.a13.s102502503;

public class Bank {
	 
    private int balance = 10000; // 銀行原本的餘額
    private boolean busying = false; // counter是否忙碌中
   
    public synchronized int withDrawSync(int amount) {  //提款
    	
    	if( busying ){  //若忙碌中則wait
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	busying = true;
    	
    	try { 
      
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;  //算出餘額
    	
    	busying = false;
    	notify();
    	if( balance <= 0 )  //若餘額小於等於0則回傳0
    		return 0;
    	return balance;  //否則回傳餘額
    	
    } 
    
 
    public synchronized int depositSync(int amount) {  //存款
    	
    	if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	
    	busying = true;
    	
    	try { 
           
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance += amount;  //計算餘額
    	
    	busying = false;
    	notify();
    	
    	return balance;  //回傳餘額
    	
    } 
} 
