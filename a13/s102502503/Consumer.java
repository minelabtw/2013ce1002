package ce1002.a13.s102502503;
import java.util.Random;
public class Consumer implements Runnable{
	
	private Bank bank; //建立Bank物件
	private int number; // consumer人數
	
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}

	@Override
	public void run() {
		
		Random r = new Random(); 
		
        for(int i = 1; i <= 5; i++) {  //提款五次
        	
            int amount =  r.nextInt(10) * 100;  //隨機提款數目
            int balanceRemain = bank.withDrawSync( amount );  //呼叫withDrawSync函式
            System.out.println("No."+ number + " consumer withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
 
}