package ce1002.a13.s102502503;
import java.util.Random;
public class Depositor implements Runnable{
	
	private Bank bank; //建立Bank物件
	private int number; //depositor人數
	
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
 
	@Override
	public void run() {
		
		Random r = new Random();  

        for(int i = 1; i <= 5; i++) {  //存款五次
        	
            int amount =  r.nextInt(10) * 100;  //隨機存款  
            int balanceRemain = bank.depositSync( amount );  //呼叫depoistSync函式
            System.out.println("No."+ number + " depositor deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
 
}