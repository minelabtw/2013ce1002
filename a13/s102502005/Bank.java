package ce1002.a13.s102502005;

public class Bank {
	int balance = 10000;// 銀行剩下多少錢。

	public synchronized void withDrawSync(int ID, int amount, int count) {
		// synchronized代表當多個thread想要執行這個method時，一次只有一個thread可以執行這個method。
		// thread執行這個method的時候，會把整個class鎖起來，所以一個thread在執行method時，另一個thread
		// 連這個class的data field的data都拿不到。

		if (balance - amount < 0) {
			balance = 0;
		} else {
			balance -= amount;
		}

		System.out.println("No." + ID + " consumer withdraw " + amount
				+ " Current Balance " + balance + " count" + count);
		return;
	}

	public synchronized void depositSync(int ID, int amount, int count) {

		balance += amount;
		System.out.println("No." + ID + " depositor deposit " + amount
				+ " Current Balance " + balance + " count" + count);//印出結果。
		return;
	}
}
