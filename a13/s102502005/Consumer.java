package ce1002.a13.s102502005;

import java.util.Random;

public class Consumer implements Runnable {
	int ID;// 客戶ID代號
	int withdrawCount = 0;// 提領次數
	Bank bank;// 那間"被客戶提領的銀行"的reference

	public Consumer(Bank bank, int ID) {// ID和銀行的reference用constructor傳進來。
		this.ID = ID;
		this.bank = bank;
	}

	public void run() {// run負責提五次錢。
		Random random = new Random();
		for (int i = 0; i < 5; i++) {
			withdrawCount++;
			int amount = 100 * (random.nextInt() % 11);//random.nextInt()可能會出現負數，所以要加判斷。
			if(amount<0){
				amount*=-1;
			}
			bank.withDrawSync(ID, amount, withdrawCount);// 直接在Bank裡面印出結果，不然會發生先領完錢卻來不及印出來的結果。
		}

	}
}
