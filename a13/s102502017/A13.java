package ce1002.a13.s102502017;

public class A13 {
	
	public A13(){
		Bank bank = new Bank(10000);
        
        Consumer[] consumer = new Consumer[3];
        for(int i=0;i<3;i++)consumer[i] = new Consumer(i+1, bank);
        
        Thread thread;
        for(int i=0;i<3;i++){
        	thread = new Thread(consumer[i]);
        	thread.start();
        }
        
        Depositor[] depositor = new Depositor[3];
        for(int i=0;i<3;i++)depositor[i] = new Depositor(i+1,bank);
        
        for(int i=0;i<3;i++){
        	thread = new Thread(depositor[i]);
        	thread.start();
        }
        
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new A13();

	}

}
