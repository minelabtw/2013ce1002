package ce1002.a13.s102502017;

public class Depositor implements Runnable{

    int no;
    int deposit;
    Bank bank;

    Depositor(int no, Bank bank) {
        this.no = no;
        this.bank = bank;
    }

    // please override this method when executed by thread
    public void run(){
        for(int i=1 ; i<=5 ; i++) {
            deposit = (int)(Math.random()*11)*100;
            bank.deposit(deposit, no, i);
            //sleep
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}