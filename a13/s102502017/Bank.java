package ce1002.a13.s102502017;

public class Bank {

    int total;
    int money;
    boolean isBusy;
    boolean noMoney;
    boolean noEnoughMoney;

    Bank(int total) {
        this.total = total;
        isBusy = false;
    }
    
    synchronized void deposit(int amount, int no, int i) {
        // if busy ....
        while(isBusy) {
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        isBusy = true;
        total += amount;
        
        System.out.println("No."+no+" depositor deposit "+amount+" Current Balance "+total+" count "+i);
        //sleep 
            try {
                Thread.sleep((int)(Math.random()*11)+10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        
        // notify() and return 
        isBusy = false;
        notify();
    }
    

    // please fill the blank
    public synchronized boolean withDrawSync(int amount, int no, int i) {
        // if busy ....
        while(isBusy) {
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        // notify() and return 
        isBusy = true;
        if(total == 0) {
            noMoney = true;
        } 
        //withdraw
        else if(total >= amount) {
            total -= amount;
        } 
        else {
            noEnoughMoney = true;
        }
        if(noMoney || noEnoughMoney) {
            if(noMoney) {
                System.out.println("No." + no + " withdraw fail, Bank has no money count " + i);
                isBusy = false;
                notify();
                return false;
            } 
            else {
                System.out.println("No." + no + " withdraw fail Current Balance " + total + " count " + i);
                noEnoughMoney = false;
            }
        } 
        else {
            System.out.println("No." + no + " withdraw " + amount + " Current Balance " + total + " count " + i);
        }
        try {
            Thread.sleep((int)(Math.random()*11)+10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        isBusy = false;
        notify();
        return true;
    }
}

