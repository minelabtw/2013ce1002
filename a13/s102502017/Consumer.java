package ce1002.a13.s102502017;

public class Consumer implements Runnable{

    int no;
    int withdraw;
    Bank bank;

    Consumer(int no, Bank bank) {
        this.no = no;
        this.bank = bank;
    }

    // please override this method when executed by thread
    public void run(){
        for(int i=1 ; i<=10 ; i++) {
            withdraw = (int)(Math.random() * 11 )*100;
            // withdraw money from bank
            if(!bank.withDrawSync(withdraw, no, i)) {
                break;
            }
            //sleep
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}