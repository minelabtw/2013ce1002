package ce1002.a13.s102502551;

public class Consumer implements Runnable{
	private int id;
	private int money;
	private Bank bank = new Bank();
	private int count=0;
	
	Consumer(Bank bank,int x){
		id=x;
		this.bank=bank;
	}

	@Override
	public void run() {
		for(int x=0;x<10;x++){
		money=(int) (Math.random()*10000);								//�H��
		
		count++;
    	synchronized (bank) {		
    		bank.change(money, 0);										
    		System.out.println("NO."+id+" consumer withdraw "+money+" Current Balance "+bank.money+" count "+ count );
    	}
		}
	}
}