package ce1002.a13.s102502551;

public class Depositor implements Runnable{
	private int id;
	private int money;
	private Bank bank = new Bank();
	private int count=0;
	
	Depositor(Bank bank,int x){
		id=x;
		this.bank=bank;
	}

	@Override
	public void run() {
		for(int x=0;x<10;x++){
		money=(int) (Math.random()*10000);
		
		count++;
    	synchronized (bank) {
    		bank.change(money, 1);
    		System.out.println("NO."+id+" depositor deposit "+money+" Current Balance "+bank.money+" count "+ count );
    	}
	}
	}
}
