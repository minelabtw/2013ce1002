package ce1002.a13.s102502558;

public class Bank
{
	private int balance = 10000;
	private boolean busying = false;
	
	public enum Method 
	{
		WITHDRAW, DEPOSIT
	}
	Bank()
	{
		
	}
	
	public synchronized int workSync(Method method, int amount)
	{
		if (busying)
		{
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		busying = true;
		
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (method == Method.WITHDRAW)
		{
			balance -= amount;
		}
		else if (method == Method.DEPOSIT)
		{
			balance += amount;
		}
		
		
		
		busying = false;
		notify();
		
		return (balance < 0) ? 0 : balance;
	}
}
