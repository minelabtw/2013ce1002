package ce1002.a13.s102502558;

public class A13
{
	public static void main(String[] args)
	{
		Bank bank = new Bank();
		
		Thread ct1 = new Thread(new Consumer(bank, 1));
		Thread ct2 = new Thread(new Consumer(bank, 2));
		Thread ct3 = new Thread(new Consumer(bank, 3));
		
		Thread dt1 = new Thread(new Depositor(bank, 1));
		Thread dt2 = new Thread(new Depositor(bank, 2));
		Thread dt3 = new Thread(new Depositor(bank, 3));
		
		ct1.start();
		ct2.start();
		ct3.start();
		
		dt1.start();
		dt2.start();
		dt3.start();
	}	
}
