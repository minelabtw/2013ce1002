package ce1002.a13.s102502558;

import java.util.Random;

import ce1002.a13.s102502558.Bank.Method;

public class Depositor implements Runnable
{
	Bank bank;
	int id;
	private static Random r = new Random();
	Depositor(Bank bank, int id)
	{
		this.bank = bank;
		this.id = id;
	}
	@Override
	public void run()
	{
		for(int i=1;i<=5;i++)
		{
			int amount = r.nextInt(10) * 100;
			int remained = bank.workSync(Method.DEPOSIT, amount);
			System.out.println("No."+ id + " depositor deposit " + amount + " Current Balance "+ remained + " count " + i);
		}
	}
}
