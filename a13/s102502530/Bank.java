package ce1002.a13.s102502530;

public class Bank {
	//declare
	private int balance = 20000;
	
	public synchronized int withDrawOrDepositSync(int amount) {
		//withdraw or deposit
		return balance += amount;
	}
}
