package ce1002.a13.s102502530;

import java.util.Random;

public class Consumer implements Runnable {
	//declare
	private int id;
	private Bank bank;
	
	//constructor
	public Consumer(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		//declare
		Random random = new Random();
		int withdraw;
		
		//start
		for (int i = 0; i != 5; i++) {
			try {
				//withdrawing time
				Thread.sleep(random.nextInt(91) + 10);
			} catch (Exception e) {}
			
			//withdraw
			withdraw = random.nextInt(501) + 500;
			System.out.println("No." + id + " consumer withdraw " + withdraw + " Current Balance " + bank.withDrawOrDepositSync(-withdraw) + " count "+ (i + 1));
		}
	}
}
