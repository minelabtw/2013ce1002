package ce1002.a13.s102502530;

import java.util.Random;

public class Depositor implements Runnable {
	//declare
	private int id;
	private Bank bank;
	
	//constructor
	public Depositor(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		//declare
		Random random = new Random();
		int deposit;
		
		//start
		for (int i = 0; i != 5; i++) {
			try {
				//deposit time
				Thread.sleep(random.nextInt(91) + 10);
			} catch (Exception e) {}
			
			//deposit
			deposit = random.nextInt(501) + 500;
			System.out.println("No." + id + " depositor deposit " + deposit + " Current Balance " + bank.withDrawOrDepositSync(deposit) + " count "+ (i + 1));
		}
	}
}
