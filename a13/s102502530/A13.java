package ce1002.a13.s102502530;

public class A13 {
	public static void main(String[] args) {
		//build bank
		Bank bank = new Bank();
		
		//declare thread
		Thread[] threads = new Thread[6];
		
		//there comes three consumers and three depositors
		for (int i = 0; i != 3; i++) {
			threads[2 * i] = new Thread(new Consumer(i + 1, bank));
			threads[2 * i + 1] = new Thread(new Depositor(i + 1, bank));
                }
		
		//these consumers and depositors started to withdraw or deposit
		for (Thread thread : threads)
			thread.start();
	}
}
