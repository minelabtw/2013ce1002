package ce1002.a13.s102502557;

import java.util.Random;

public class Depositor implements Runnable 
{
	int id ; 
	int deposite;
	Bank bank;
	Depositor(Bank bank , int id)
	{
		this.id = id;
		this.bank = bank;
	}
	
	public void run()
	{
		int deposite = 0;
		int current = 50000;
		
		for(int counter = 1 ; counter<=5 ; counter++)
		{
			Random rand = new Random();
			deposite = (rand.nextInt(10)+1)*100;
			current = bank.depositeSync(deposite);
			System.out.println("No." + id +" deposit " + deposite + " Current Balance " + current + " count " + counter);
		}
	}
}
