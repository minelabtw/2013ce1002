package ce1002.a13.s102502557;

public class Bank 
{
private int current = 10000;
private int last_money = 0;
private boolean withdrowing ;
private boolean depositing;
  // please fill the blank
Bank()
{
	withdrowing = false;//初始化布林值
	depositing = false;
}

  /*******************領錢的部分*******************************************/
  public synchronized int withDrawSync(int amount) //amount就是提領的錢  //synchronized就是同步 一次只允許一個執行恤進入處理
  {
    // if busy ....
	if(withdrowing)
	{
		try
		{
		   wait();	// wait()  //如果有人領錢 就要等
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	// do process, use Thread.sleep() to simulate processing
	try 
	{
		if( current - amount <= 0)
		{
		    current = 0;//錢沒了就要顯示0
		}
		else 
		{
			current = current - amount;
		}
		Thread.sleep(1000);//讓thread休息一秒（１０００）
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
    // notify() and return 
	withdrowing =  false;
	//要再改一次withdrow的布林值
	notify();//告訴wait,下一個人可以進來領錢了
	return current;//return到withdrow去
  }
  /*******************************************************/
  public synchronized int depositeSync(int amount)
  {
	  if(depositing)
	  {
		  try
		  {
			  wait();
		  }
		  catch(InterruptedException e)
		  {
			  e.printStackTrace();
		  }
	  }
	  
	  try
	  {
		  current = current + amount;
		  Thread.sleep(1000);
	  }
	  catch(Exception e)
	  {
		  e.printStackTrace();
	  }
	  
	  depositing = false;
	  return current;
  }
  
}
