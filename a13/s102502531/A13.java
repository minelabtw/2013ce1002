package ce1002.a13.s102502531;



public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		Thread t1 = new Thread(new Consumer("1", bank)); //創造六個執行列
		Thread t2 = new Thread(new Consumer("2", bank));
		Thread t3 = new Thread(new Consumer("3", bank));
		Thread t4 = new Thread(new Depositor("1", bank));
		Thread t5 = new Thread(new Depositor("2", bank));
		Thread t6 = new Thread(new Depositor("3", bank));
		t1.start();
		t2.start();
		t3.start(); //開始六個執行緒
		t4.start();
		t5.start();
		t6.start();
	}

}
