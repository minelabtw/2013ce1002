package ce1002.a13.s102502511;

import java.util.Random;

public class Depositor implements Runnable{
	int number; //存錢人的號碼
	Bank bank = new Bank(); //引入類別Bank
	Random ran = new Random(); //隨機
	int amount = ran.nextInt(10) * 100; //存入的錢為0~1000的隨機值
	
	Depositor(Bank bank , int number) {
		// TODO Auto-generated constructor stub
		this.number = number; //存取number
		this.bank = bank; //存取bank
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		for(int i = 1 ; i < 6 ; i++){
			System.out.println("No." + number + " depositor deposit " + amount + " Current Balance " + bank.withDepo(amount) + " count " + i);
		}//輸出內容
	}

}
