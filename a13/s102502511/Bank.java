package ce1002.a13.s102502511;

public class Bank {
	  // please fill the blank
	  int amount = 10000; //起始金額
	  boolean check = false; //檢查 目前沒人所以為false
	  
	  public synchronized int withDepo(int amount){
		  while(check){ //當有人時 等待 直到前面的人完成
			  try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		  }
		  check = true;	//有人存錢 所以改成true	
		  try{
			  Thread.sleep(1000); //每次動作停1秒
			  this.amount += amount; //存入金額
		  }catch(InterruptedException e){
			  e.printStackTrace();
		  }
		  check = false; //存錢結束 改成false
		  notify();//把位置讓出給人競爭
		  return this.amount; //回傳戶頭金額
	  }
	  
	  public synchronized int withDraw(int amount) {
		 while(check){ //有人時 等待
			  try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 }
		 check = true; //有人領錢改成true		 
		 while((this.amount-amount) < 0){ //當領錢的人領出的錢超過戶頭金額便為等待 , 並且開放人下一個人存錢者
			try {
				check = false;
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(1000); //停頓1秒
			this.amount -= amount; //領出金額扣款
		} catch (InterruptedException e) {
				// TODO Auto-generated catch block
			e.printStackTrace();
		}
		check = false; //領完錢 改成false
		notify(); //開放下個人競爭
	    return this.amount; //回傳
	  }
	}
