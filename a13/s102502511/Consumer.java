package ce1002.a13.s102502511;

import java.util.Random;

public class Consumer implements Runnable{
	int number; //提錢人的號碼
	Bank bank = new Bank(); //引入類別Bank
	Random ran = new Random(); //隨機
	int amount = ran.nextInt(10) * 100; //領的錢為0~1000的隨機值 
	
	Consumer(Bank bank, int number){
		this.number = number; //存取number
		this.bank = bank; //存取bank
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i = 1 ; i < 6 ; i++){
			System.out.println("No." + number + " consumer withdraw " + amount + " Current Balance " + bank.withDraw(amount) + " count " + i);
		}//輸出內容
	}

}
