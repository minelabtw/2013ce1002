package ce1002.a13.s102502517;

public class Depositor implements Runnable {
	private int number; // 儲存編號
	private Bank bank;

	public Depositor(int number, Bank bank) {
		this.number = number;
		this.bank = bank;
	}

	public void run() {
		int deposit; // 宣告存款金額
		for (int i = 1; i <= 5; i++) {
			System.out.println("No." + number + " depositor deposit "
					+ (deposit = (int) (Math.random() * 10) * 100) // 隨機設定存款金額
					+ " Current Balance " + bank.depositSync(deposit) // 存款
					+ " count " + i);
		}
	}
}
