package ce1002.a13.s102502517;

public class Consumer implements Runnable {
	private int number; // 儲存編號
	private Bank bank;

	public Consumer(int number, Bank bank) {
		this.number = number;
		this.bank = bank;
	}

	public void run() {
		int withdraw; // 宣告提款金額
		for (int i = 1; i <= 5; i++) {
			System.out.println("No." + number + " consumer withdraw "
					+ (withdraw = (int) (Math.random() * 10) * 100) // 隨機設定提款金額
					+ " Current Balance " + bank.withdrawSync(withdraw) // 提款
					+ " count " + i);
		}
	}
}
