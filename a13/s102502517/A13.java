package ce1002.a13.s102502517;

public class A13 {

	public static void main(String[] args) {
		Bank bank = new Bank();
		Thread[] thread = new Thread[6]; // 宣告Thread陣列

		for (int i = 0; i <= 2; i++)
			thread[i] = new Thread(new Consumer(i + 1, bank)); // 實體化Thread

		for (int i = 3; i <= 5; i++)
			thread[i] = new Thread(new Depositor(i - 2, bank)); // 實體化Thread

		for (int i = 0; i <= 5; i++)
			thread[i].start(); // 開始thread
	}
}
