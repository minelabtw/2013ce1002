package ce1002.a13.s102502517;

public class Bank {
	int balance = 10000; // 初始餘額為10000
	Boolean busy = false;

	public synchronized int withdrawSync(int amount) {
		if (busy) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		busy = true;
		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance -= amount;
		busy = false;
		notify();

		return balance; // 回傳餘額
	}

	public synchronized int depositSync(int amount) {
		if (busy) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		busy = true;
		try {
			Thread.sleep((int) (Math.random() * 3000));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance += amount;
		busy = false;
		notify();

		return balance; // 回傳餘額
	}
}
