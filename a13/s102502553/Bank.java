package ce1002.a13.s102502553;

public class Bank {

	static int money = 10000;

	  public synchronized static int withDrawSync(int amount , int choice) {//處理存款和提款，使用synchronized逐項處理每件事
	  
		try
		{
			Thread.sleep(500);//休息0.5秒
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}
		try
		{
			if(choice == 1)
			    money = money - amount;
			else
				money = money + amount;
		}
	    catch(Exception e)
	    {
			 e.getStackTrace();
	    }
		
		if(money < 0)
		    return 0;
		else
			return money;
	  }
}
