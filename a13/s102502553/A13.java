package ce1002.a13.s102502553;

public class A13 {

public static void main(String[] args) {
		
		Bank bank = new Bank();
		Thread thread1 = new Thread(new Consumer(1 , bank));//使用thread並建立consumer
		Thread thread2 = new Thread(new Consumer(2 , bank));
		Thread thread3 = new Thread(new Consumer(3 , bank));
		
		Thread thread4 = new Thread(new Depositor(1 , bank));//使用thread並建立depositor
		Thread thread5 = new Thread(new Depositor(2 , bank));
		Thread thread6 = new Thread(new Depositor(3 , bank));
		
		thread1.start();//開始thread
		thread2.start();
		thread3.start();
		
		thread4.start();
		thread5.start();
		thread6.start();
	}
}
