package ce1002.a13.s102502553;

import java.util.Random;

public class Depositor implements Runnable{

	
	int number;
	Bank bank = new Bank();
	Random ran = new Random();

	Depositor(int number,Bank bank)//存depositor的number bank
	{
		this.number = number;
		this.bank = bank;
	}
	
	@Override
	public void run() {//Runnable下的函式
		for(int i = 1;i < 6;i++)//deposit money from bank
		{
	     int money = (ran.nextInt(10))*100;
		 System.out.println("No."+ number +" depositor deposit "+ money + " Current Balance " +
		 Bank.withDrawSync(money , 2)+" count "+i);
		}
	}

}
