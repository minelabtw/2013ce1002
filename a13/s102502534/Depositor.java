package ce1002.a13.s102502534;

import java.util.Random;

public class Depositor implements Runnable{
	private Bank bank; // a member object of bank to withdraw their money
	private int number; // the number of consumer
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
		 public void run() {
				
				Random r = new Random(); // RNG 
				// we assume each consumer will withdraw money for 10 times
		        for(int i = 1; i <= 5; i++) {
		        	// give a certain amount
		            int amount =  r.nextInt(10) * 100;
		            // go to bank to get his/her money
		            int balanceRemain = bank.depositSync( amount );
		            // print out the result
		            System.out.println("No."+ number + " depositor"+" deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
		        }       
				
			}
}
	
