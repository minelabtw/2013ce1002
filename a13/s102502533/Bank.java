package ce1002.a13.s102502533;

public class Bank {
	private int balance = 10000; 
	private boolean busying = false; 

	public synchronized int withDrawSync(int amount) {
		if (busying) {//先確認是否可以提錢
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		busying = true;//不可以就等
		try {
			Thread.sleep((int) (Math.random() * 300));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance -= amount;//提錢
		busying = false;
		notify();
		if (balance <= 0)
			return 0;
		return balance;//回傳值
	}
	public synchronized int Depositor(int dollar){
		if (busying) {//先確認是否可以存錢
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		busying = true;//不可以就等
		try {
			Thread.sleep((int) (Math.random() * 300));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		balance += dollar;//存錢
		busying = false;
		notify();
		return balance;//回傳存後值
	}
}
