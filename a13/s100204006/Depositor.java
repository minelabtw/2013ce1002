package ce1002.a13.s100204006;

import java.util.Random;

public class Depositor
{
	private Bank bank;             // a member object of bank to withdraw and deposit their money
	private int number;            // the number of consumer
	public Depositor(Bank bank,int number)
	{
		this.bank = bank;
		this.number = number;
	}
	public void run() 
	{
		Random r = new Random();      // RNG
		
		for(int i = 1; i <= 5; i++)   // we assume each consumer will withdraw and deposit money for 5 times
		{
			int amount = r.nextInt(5) * 100;         // give a certain amount
			int balanceRemain = bank.depoSitSync( amount );        // go to bank to get his/her money
			System.out.println("No."+ number + " deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
		}
	}
}
