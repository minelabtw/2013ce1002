package ce1002.a13.s100204006;

public class Bank 
{
	private int balance = 10000;               // bank's total money
	private boolean busying = false;           // indicate that the counter is busy or not
	
	public synchronized int withDrawSync(int amount) 
	{
		
		if( busying )            // check if the counter is busy, if it does, make the consumer to wait
		{
			try
			{
				wait();
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		 
		busying = true;          // set the busying flag to true
		try 
		{
			Thread.sleep((int) (Math.random() * 3000));        // sleep with random interval
		}
		catch(InterruptedException e) 
		{
			e.printStackTrace();
		}
		balance -= amount;         // this indicate the process is done
		busying = false;
		notify();
		if( balance <= 0 )
			return 0;
		return balance;
	} 
	public synchronized int depoSitSync(int amount) 
	{
		
		if( busying )           // check if the counter is busy, if it does, make the consumer to wait
		{
			try
			{
				wait();
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		busying = true;          // set the busying flag to true
		try 
		{
			Thread.sleep((int) (Math.random() * 3000));          // sleep with random interval
		}
		catch(InterruptedException e) 
		{
			e.printStackTrace();
		}
		balance += amount;           // this indicate the process is done
		busying = false;
		notify();
		if( balance <= 0 )
			return 0;
		return balance;
	 }
}
