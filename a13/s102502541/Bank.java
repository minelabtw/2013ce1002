package ce1002.a13.s102502541;

public class Bank {
	  // please fill the blank
	int money = 10000;
	boolean detect = false;
	public synchronized int withDrawSync(int amount) {
		// if busy ....
	    // wait() 
	    
	    if(detect)//判斷是否領錢
	    {	
	    	try
		    {
	    		wait();
		    }
		    catch(InterruptedException e)
		    {
		    	e.printStackTrace();	
		    }
	    }
	    
	    	detect = true;
	    	try
		    {
		    	Thread.sleep(1000);
		    }
		    catch(InterruptedException e)
		    {
		    	e.printStackTrace();
		    }
	    	
	    money = money-amount;
	    detect = false;
	    notify();
	    // do process, use Thread.sleep() to simulate processing
	    
	    // notify() and return 
	    if(money<0)
	    	money = 0;
	    return money;
	  }
	}
