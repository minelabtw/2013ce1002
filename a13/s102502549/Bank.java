package ce1002.a13.s102502549;

public class Bank {

	private int balance = 10000;

	public Bank() {
	}

	public synchronized int withdraw(int amount) {
		
		//當銀行沒錢要讓consumer等待
		while (balance < amount) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		balance-=amount;
		return balance;
	}

	public synchronized int deposit(int amount) {
		balance+=amount;
		notify();//通知Consumer可以領錢了
		return balance;
	}

}
