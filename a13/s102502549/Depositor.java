package ce1002.a13.s102502549;

import java.util.Random;

public class Depositor implements Runnable {

	private Bank bank = new Bank();
	private int num;
	private int count;
	private Random random = new Random();

	public Depositor(Bank bank, int num) {
		this.bank = bank;
		this.num = num;
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			// 取得bank鎖，以確保一圈for完全執行完才釋放bank
			synchronized (bank) {
				int amount = 100 * random.nextInt(11);

				count++;

				System.out.println("No." + num + " depositor deposit " + amount
						+ " Current Balance " + bank.deposit(amount)
						+ " count " + count);
			}

			try {
				Thread.sleep(1);// 讓其他thread有機會搶到鎖
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
