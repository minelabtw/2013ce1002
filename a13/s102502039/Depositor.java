package ce1002.a13.s102502039;

import java.util.Random;

public class Depositor implements Runnable{
	private Bank bank; // a member object of bank to depositor their money
	private int number; // the number of consumer
	public  Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
	@Override
	public void run() {
		
		Random r = new Random(); // RNG 
		// we assume each consumer will depositor money for 10 times
       for(int i = 1; i <= 5; i++) {
		
        	// give a certain amount
            int amount =  r.nextInt(10) * 100;
            // go to bank to depositor his/her money
            int balanceRemain = bank. depositorSync( amount );
            // print out the result
            System.out.println("No."+ number + " deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
	
}

