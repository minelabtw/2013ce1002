package ce1002.a13.s102502030;

import java.util.Random;

public class Depositor implements Runnable {

	private Bank bank;
	private int number;
	
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}

	@Override
	public void run() {
		
		Random r = new Random();
		
		//put money and print out
        for(int i = 1; i <= 5; i++) {
            int amount =  r.nextInt(10) * 100*-1;
            int balanceRemain = bank.withDrawSync( amount );
            System.out.println("No."+ number + " depositor withdraw " + amount*-1 + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
}
