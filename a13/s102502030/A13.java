package ce1002.a13.s102502030;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Bank bank = new Bank();
		 
		//new 3 consumer
		Thread thread = new Thread(new Consumer(bank, 1));
		Thread thread1 = new Thread(new Consumer(bank, 2));
		Thread thread2 = new Thread(new Consumer(bank, 3));
		
		//new 3 depositor
		Thread thread3 = new Thread(new Depositor(bank, 1));
		Thread thread4 = new Thread(new Depositor(bank, 2));
		Thread thread5 = new Thread(new Depositor(bank, 3));
 
		//start
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}

}
