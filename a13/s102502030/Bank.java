package ce1002.a13.s102502030;

public class Bank {

	private int balance = 10000;
    private boolean busying = false;

    public synchronized int withDrawSync(int amount) {
    	//if busy then wait
    	if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	//lock
    	busying = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 1000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;
    	
    	//unlock and return money
    	busying = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
}
