package ce1002.a13.s100203020;
import java.util.Random;

public class Bank {
	public int money = 10000; // initial is 10000
	public Bank(){}
	Random random = new Random();
	boolean useAccount = false;
	
	public synchronized void withDrawSync(int amount) {
	    if(useAccount){
	    	try {
	    		wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    
	    //using account
	    useAccount = true;

	    try {
			Thread.sleep((int)(Math.random()*500));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    int withdraw;
	    withdraw=random.nextInt(10)*100;
	    if(money-withdraw < 0){
	    	withdraw = money;
	    }
	    money -= withdraw;	    
	    
	    System.out.print("No."+ amount +" consumer withdraw " + withdraw + " Current Balance " + money + " count ");
	    
	    //have used account
	    useAccount = false;
	    notify();
	  }
	public synchronized void depositSync(int amount) {
	    if(useAccount){
	    	try {
	    		wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    
	    //using account
	    useAccount = true;

	    try {
			Thread.sleep((int)(Math.random()*500));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	    int  deposit;
	    deposit=random.nextInt(10)*100;
	    money +=  deposit;	    
	  
	    System.out.print("No."+ amount +" depositor deposit " +  deposit + " Current Balance " + money + " count ");
	    
	    //have used account
	    useAccount = false;
	    notify();
	  }
	}