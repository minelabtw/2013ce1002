package ce1002.a13.s100203020;
public class Consumer implements Runnable{

	Bank bank;
	static int sum=1;
	private int id;
	public Consumer(Bank bank){
		id=sum;
		sum++;
		this.bank = bank;

	}

	
	@Override
	public void run(){

	    // withdraw money from bank
		for (int i =1; i<6; i++){

			bank.withDrawSync(id);	
			System.out.println(i);
		}
	}
}
