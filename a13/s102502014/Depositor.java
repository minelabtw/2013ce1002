package ce1002.a13.s102502014;

public class Depositor implements Runnable {
	private int n;
	private Bank bank;
	private int count = 0;

	public Depositor(int a, Bank b) {
		bank = b;
		n = a;
	}

	@Override
	public void run() {
		// withdraw money from bank
		for (int i = 0; i < 5; i++) {
			try {
				int a = (int) (Math.random() * 1000);
				bank.withINSync(a, ++count, n); //deposit money
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}