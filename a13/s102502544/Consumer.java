package ce1002.a13.s102502544;

import java.util.Random;

public class Consumer implements Runnable{
	int num ; //宣告變數
	Bank bank = new Bank();
	Random random = new Random(); //亂數
	int amount = random.nextInt(10)*100;
	
	Consumer(int num,Bank bank){//建構子
		this.num=num;
		this.bank=bank;
	}
	public void run(){
	    // withdraw money from bank
		for(int i=1 ; i<6 ; i++){
			System.out.println("No."+ num +" consumer withdraw "+ amount +" Current Balance "+ bank.withDrawSync(amount) +" count "+ i);
		}
	  }
}
