package ce1002.a13.s102502544;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();
		
		Thread t1 = new Thread(new Consumer(1, bank));
		Thread t2 = new Thread(new Consumer(2, bank));
		Thread t3 = new Thread(new Consumer(3, bank));
		
		Thread t4 = new Thread(new Depositor(1, bank));
		Thread t5 = new Thread(new Depositor(2, bank));
		Thread t6 = new Thread(new Depositor(3, bank));
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		
	}

}
