package ce1002.a13.s102502544;

public class Bank {
	int bankmoney=10000;
	// please fill the blank
	  public synchronized int withDrawSync(int amount) {//領錢函式
		  try { 
              Thread.sleep(1000);
              bankmoney -= amount;
          } 
          catch(InterruptedException e) { 
              e.printStackTrace(); 
          }
		  if(bankmoney <= 0)
			  return 0;
		  else
			  return bankmoney;
	  }
	  public synchronized int depositSync(int amount) {//存錢函式
		  try { 
              Thread.sleep(1000);
              bankmoney += amount;
          } 
          catch(InterruptedException e) { 
              e.printStackTrace(); 
          }
		  return bankmoney;
	  }
}
