package ce1002.a13.s101201522;

public class Depositor implements Runnable {
	private int num;
	private Bank bank;
	
	Depositor (int num, Bank bank) {//initial
		this.num = num;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		int amount;//amount of save
		for (int i=1;i<=5;i++) {
			try {
				Thread.sleep((int)(Math.random()*3000));//wait some time to withdraw
				amount = (int)(Math.random()*10)*100;//random amount
				System.out.println("No."+num+" depositor deposit "+amount+" Current Balance "+bank.withDrawSync((-1)*amount)+" count "+i);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
