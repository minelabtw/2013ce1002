package ce1002.a13.s101201522;

public class Consumer implements Runnable {
	private int num;
	private Bank bank;
	
	Consumer (int num, Bank bank) {//initial
		this.num = num;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		int amount;//amount of withdraw
		for (int i=1;i<=5;i++) {
			try {
				Thread.sleep((int)(Math.random()*3000));//wait some time to withdraw
				amount = (int)(Math.random()*10)*100;//random amount
				System.out.println("No."+num+" consumer withdraw "+amount+" Current Balance "+bank.withDrawSync(amount)+" count "+i);
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
