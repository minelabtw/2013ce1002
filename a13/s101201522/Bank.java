package ce1002.a13.s101201522;

public class Bank {
	private int remain;
	private boolean isBusy;
	
	Bank () {//initial
		remain = 10000;
		isBusy = false;
	}
	
	public synchronized int withDrawSync (int amount) {
		if (isBusy) {//check whether bank is busy
			try {
				wait();
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {//wait time of process of withdraw 
			isBusy = true;
			Thread.sleep(1000);
			isBusy = false;
			
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (remain >= amount)
			remain -= amount;
		else//bank is empty
			remain = 0;
		notify();
	    return remain;
	}
	
}