package ce1002.q2.s102502028;
import javax.swing.* ;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import java.awt.* ;
public class MyPanel extends JPanel{
	
	int color = 0 ;
	String graph ;
	
	MyPanel(int color,	String graph)  //設定版面
	{
		this.color = color ;
		this.graph = graph ;
		setLayout(null) ;
		setBounds(140,55,300,300) ;
		setBorder(new LineBorder(Color.BLACK,5)) ;		
	}
	
	protected void paintComponent(Graphics g)  //畫圖
	{
		super.paintComponent(g);
		g.setColor(Color.BLACK) ;
		g.drawRect(5, 5, 290, 290);
		System.out.println(color) ;
		if(color == 1)  //紅色
		{
			g.setColor(Color.RED);
			if(graph == "Circle") 
			g.fillOval(50,50,200,200) ;
			else if (graph == "Rectangle")
				g.fillRect(50,50,200,150) ;
		}
		else if (color == 2)  //藍色
		{
			g.setColor(Color.BLUE);
			if(graph == "Circle") 
			g.fillOval(50,50,200,200) ;
			else if (graph == "Rectangle")
				g.fillRect(50,50,200,150) ;
		}
			
	}
	
}
