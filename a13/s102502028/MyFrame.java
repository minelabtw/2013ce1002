package ce1002.q2.s102502028;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame {

	JTextField jtf = new JTextField();  //宣告元件
	JButton red = new JButton("Red");
	JButton blue = new JButton("Blue");
	JLabel graph = new JLabel("Graph: ");
	int color;

	MyFrame() {
		setSize(500, 500);   //設定框架大小 排版...
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		graph.setBounds(10, 20, 50, 25);  //加入元件
		add(graph);
		jtf.setBounds(60, 19, 120, 25);
		add(jtf);
		red.setBounds(10, 55, 120, 40);
		red.addActionListener(new redListener());
		add(red);
		blue.setBounds(10, 110, 120, 40);
		blue.addActionListener(new blueListener());
		add(blue);

		setVisible(true);
	}

	class redListener implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			try {
				if (jtf.getText().isEmpty()) // 名稱不能是空的
				{
					JOptionPane.showMessageDialog(null,
							"Graph name can't be empty!", "Q2",
							JOptionPane.ERROR_MESSAGE);
				} else {  //加入圖形
					MyPanel panel = new MyPanel(1, jtf.getText());
					add(panel);
				}
				if (jtf.getText() == "Circle")
					System.out.println("true");
			} catch (Exception ex) {
			}
		}

	}

	class blueListener implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			try {
				if (jtf.getText().isEmpty()) // 名稱不能是空的
				{
					JOptionPane.showMessageDialog(null,
							"Graph name can't be empty!", "Q2",
							JOptionPane.ERROR_MESSAGE);
				} else {  //加入圖形
					MyPanel panel = new MyPanel(2, jtf.getText());
					add(panel);
				}
			} catch (Exception ex) {
			}
		}

	}

}
