package ce1002.a13.s102502025;

public class Bank {
	public int amount = 10000;

	public synchronized int withDrawSync(int amount) {
		try {//subtract amount
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.amount -= amount;
		if (this.amount <= 0) {
			return 0;
		} else
		return this.amount;
	}
	public synchronized int Depositor(int amount) {
		try {//add amount
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.amount += amount;
		if (this.amount <= 0) {
			return 0;
		} else
		return this.amount;
	}
}