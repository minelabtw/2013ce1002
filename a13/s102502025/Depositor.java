package ce1002.a13.s102502025;

import java.util.Random;

public class Depositor implements Runnable {
	Random random;
	Bank bank;
	int ID;

	public Depositor(Bank b, int ID) {//input amount and ID
		this.bank = b;
		this.ID = ID;
	}

	public void run() {//output answer
		for (int i = 1; i <= 5; i++) {
			random = new Random();
			int a = random.nextInt(10) * 100;
			System.out.println("No." + ID + " depositor deposit " + a
					+ " Current Balance " + bank.Depositor(a) + " count "
					+ i);
		}
	}
}