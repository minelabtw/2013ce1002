package ce1002.a13.s102502003;

public class Bank {
	 
    private int budget = 10000; 
    private boolean busy = false; 
    
    public synchronized int withDrawSync(int amount) {
    	// if bank is busying, let it wait.
    	if( busy ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	busy = true;
    	
    	try {
    		// wait for a random time to deal with the process
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	// change the budget
    	budget = budget - amount;
    	
    	
    	busy = false;
    	notify();
    	if( budget <= 0 )
    		return 0;
    	return budget;
    	
    } 
    
    public synchronized int Deposit(int amount) {
    	// if bank is busying, let it wait.
    	if( busy ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	
    	busy = true;
    	
    	try { 
    		// wait for a random time to deal with the process
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	// change the budget
    	budget = budget + amount;
    	

    	busy = false;
    	notify();
    	
    	return budget;
    	
    } 
    
 
    
} 
