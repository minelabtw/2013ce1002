package ce1002.a13.s102502003;
import java.util.Random;


public class Consumer implements Runnable{
	
	private Bank bank; 
	private int number; // the number of consumer
	
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
 
	@Override
	public void run() {
		
		Random r = new Random(); 
		// a customer withdraw money for five times
        for(int i = 1; i <= 5; i++) {
        	// give a amount
            int amount =  r.nextInt(10) * 100;
            // go to bank to get his/her money
            int balanceRemain = bank.withDrawSync( amount );
            // print out the result
            System.out.println("No."+ number + " withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}
 
}