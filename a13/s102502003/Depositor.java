package ce1002.a13.s102502003;
import java.util.Random;


public class Depositor implements Runnable{
	
	private Bank bank;
	private int number;
	
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random ran = new Random();
		// a customer deposit money for five times
		for(int i = 1; i <= 5; i++) {
        	// give a certain amount
            int amount =  ran.nextInt(10) * 100;
            int balanceRemain = bank.Deposit( amount );
            // print out the result
            System.out.println("No."+ number + " deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
		
	}

}
	
}	
