package ce1002.a13.s102502026;

public class Bank {
	public int money = 10000;

	public synchronized int WithDrawSync(int x) {
		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.money= this.money -x;	//all money minus the customer withdraw
		if(this.money<0){	//if all money <=0
			return 0;
		}
		else
		return this.money;	//else return all money
	}
	public synchronized int Deposit(int x) {
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.money= this.money +x;	//all money plus for deposit
		if(this.money<0){	//if all money <=0
			return 0;
		}
		else
		return this.money;	//else return all money
	}
}

