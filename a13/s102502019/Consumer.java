package ce1002.a13.s102502019;

public class Consumer implements Runnable{
private int id;
private Bank bank;
public Consumer(int id, Bank b)
{
	this.id = id;
	bank = b;
}
	public void run()
	{
		for(int i=1; i<=5; i++)
		{
			int x = (int)(Math.random()*5000);
			System.out.println("No. "+id+" consumer withdraw "+x+" Current Balance "+bank.withDrawSync(x)+" count "+i);
		}
	}
}
