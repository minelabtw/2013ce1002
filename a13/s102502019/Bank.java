package ce1002.a13.s102502019;

public class Bank {
	int money = 9999999;
	boolean busying = false;
public synchronized int withDrawSync(int amount)
{
	if(busying == true)
	{
		try{
			wait();
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	busying = true;
	try{
		Thread.sleep((int)(Math.random()*1000));
	}catch(InterruptedException e)
	{
		e.printStackTrace();
	}
	money = money-amount;
	busying = false;
	notify();
	return money;
}
public synchronized int DepositSync(int amount)
{
	if(busying == true)
	{
		try{
			wait();
		}catch(InterruptedException e)
		{
			e.printStackTrace();
		}
	}
	busying = true;
	try{
		Thread.sleep((int)Math.random()*1000);
	}catch(InterruptedException e)
	{
		e.printStackTrace();
	}
	money = money+amount;
	busying = false;
	notify();
	return money;
}
}
