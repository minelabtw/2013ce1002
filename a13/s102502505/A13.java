package ce1002.a13.s102502505;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// initialize a bank
		Bank bank = new Bank();

		// initialize 3 consumer and wrap with java thread
		Consumer consumer1 = new Consumer(bank, 1);//新增物件，並傳值進去
		Consumer consumer2 = new Consumer(bank, 2);
		Consumer consumer3 = new Consumer(bank, 3);

		Depositor depositor1 = new Depositor(bank, 1);
		Depositor depositor2 = new Depositor(bank, 2);
		Depositor depositor3 = new Depositor(bank, 3);

		Thread thread1 = new Thread(consumer1);//加物件到新增的執行序中
		Thread thread2 = new Thread(consumer2);
		Thread thread3 = new Thread(consumer3);

		// initialize 3 depositor and wrap with java thread
		Thread thread4 = new Thread(depositor1);
		Thread thread5 = new Thread(depositor2);
		Thread thread6 = new Thread(depositor3);

		// start each thread to perform process
		thread1.start();//開始執行序
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();

	}

}
