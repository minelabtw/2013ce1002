package ce1002.a13.s101201023;

import java.util.Random;

public class A13 
{
	private Thread thread[] = new Thread[6];
	private Bank bank = new Bank();;

	A13() 
	{
		//to construction the thread
		for (int i = 0; i < 6; i++) 
		{
			if(i >= 0 && i <= 2)
				thread[i] = new Thread(new Consumer(i));
			else
				thread[i] = new Thread(new Depositor(i));
			
			thread[i].start();
		}
	}

	public class Consumer implements Runnable 
	{
		private int id;
		private int count = 0;

		Random random = new Random();

		Consumer(int id) 
		{
			//input the data
			this.id = id;
			count = 0;

		}

		public void show() 
		{
			//to change the money in the Bank and print the data 
			int withdraw = random.nextInt(10) * 100;
			count++;
			System.out.println("NO." + (id + 1) + " consumer withdraw " + withdraw
					+ " Current Balance " + bank.withDrawSync(id, withdraw)
					+ " count " + count);
		}

		@Override
		public void run() 
		{
			//always do 
			for (int i = 0; i < 5; i++) 
			{
				show();
			}
		}

	}
	
	public class Depositor implements Runnable
	{
		private int id;
		private int count = 0;

		Random random = new Random();

		Depositor(int id) 
		{
			//input the data
			this.id = id;
			count = 0;

		}

		public void show() 
		{
			//to change the money in the Bank and print the data 
			int deposit = random.nextInt(10) * 100;
			count++;
			System.out.println("NO." + (id - 2) + " depositor deposit " + deposit
					+ " Current Balance " + bank.withDrawSync(id, -deposit)
					+ " count " + count);
		}

		@Override
		public void run() 
		{
			//always do 
			for (int i = 0; i < 5; i++) 
			{
				show();
			}
		}
	}

	public class Bank 
	{
		private int money = 0;
		private int person = 0;// if 0 then no person

		Bank() 
		{
			money = 10000;
		}

		public synchronized int withDrawSync(int id, int amount) 
		{
			while (person != 0) 
			{
				try 
				{
					//stop the thread except this id 
					for (int i = 0; i < 3; i++) 
					{
						if (i != id) 
						{
							thread[i].wait();
						}
					}
				} 
				
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
			}

			try 
			{
				person = 1;
				thread[id].sleep(1000);//simulate the process
			} 
			
			catch (InterruptedException e) 
			{
				System.out.println("Out2");
			}
			
			person = 0;
			for (int i = 0; i < 3; i++) 
			{
				if (i != id) 
				{
					notify();
				}
			}
			
			//withdraw
			money = money - amount;
			if (money < 0) 
			{
				money = 0;
			}
			return money;
		}
	}

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		A13 a13 = new A13();
	}
}