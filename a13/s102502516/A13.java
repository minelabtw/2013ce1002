package ce1002.a13.s102502516;

public class A13 {

	public static void main(String[] args) {

		Bank bank = new Bank();

		Consumer consumer[] = new Consumer[6]; // 三個用戶
		Thread thread[] = new Thread[6]; // 三個執行緒
		for (int i = 0; i < consumer.length; i++) {
			consumer[i] = new Consumer((i < 3 ? i : i - 3 )+ 1, bank);
			thread[i] = new Thread(consumer[i < 3 ? i : i - 3]);
			thread[i].start();
		}

	}
}
