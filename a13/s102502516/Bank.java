package ce1002.a13.s102502516;

public class Bank {
	int totalMoney = 20000; // 總金額
	boolean busy = false; // 忙碌中?

	public synchronized int withDrawSync(int amount) {
		// if busy ....
		while (busy) {
			try {
				wait(); // 等待
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// do process, use Thread.sleep() to simulate processing
		try {
			Thread.sleep(5); // sleep
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		totalMoney -= amount;
		if (totalMoney < 0)
			totalMoney = 0;
		busy = false;
		// notify() and return
		notify();
		return totalMoney;
	}

	public synchronized int deposit(int amount) {
		// if busy ....
		while (busy) {
			try {
				wait(); // 等待
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// do process, use Thread.sleep() to simulate processing
		try {
			Thread.sleep(5); // sleep
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		totalMoney += amount;
		busy = false;
		// notify() and return
		notify();
		return totalMoney;
	}
}
