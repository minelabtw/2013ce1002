package ce1002.a13.s102502016;

public class Consumer implements Runnable {
	private int num;
	private Bank bank;
	private int count = 0;

	public Consumer(int a, Bank b) {
		bank = b;
		num = a;
	}

	// please override this method when executed by thread
	public void run() {
		// withdraw money from bank
		for (int i = 0; i < 5; i++) {
			try {
				int a = (int) (Math.random() * 10+1)*100;
				// Thread.sleep(10);
				bank.withDrawSync(a, ++count, num);// ����
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}