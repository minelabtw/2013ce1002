package ce1002.a13.s102502016;

public class Bank {
	private int money = 10000;
	private boolean busy = false;

	// please fill the blank
	public synchronized int withDrawSync(int amount, int count, int num)
			throws InterruptedException {
		// if busy ....
		// wait()
		if (busy) {
			wait();
		}
		busy = true;
		if (money - amount < 0)// 不會小於0
			wait();
		money = money - amount;
		System.out.println("No." + num + " withdraw " + amount
				+ " Current Balance " + money + " count " + count);// 這裡輸出才不會有順序問題
		busy = false;
		notify();
		return money;
		// do process, use Thread.sleep() to simulate processing
		// notify() and return
	}

	public synchronized int deposit(int amount, int count, int name)
			throws InterruptedException {
		money = money + amount;
		System.out.println("No." + name + " deposit " + amount
				+ " Current Balance  " + money + " count " + count);
		return money;
	}
}