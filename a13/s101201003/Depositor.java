package ce1002.a13.s101201003;

import java.util.Random;

public class Depositor implements Runnable {
	private int number = 0;
	private Bank bank = new Bank();

	Depositor(Bank bank, int number) {
		this.bank = bank;
		this.number = number;
	}

	public void show() {
		Random ran = new Random();
		int withdraw = ran.nextInt(10) * 100;
		number++;
		System.out.println("NO." + number + " depositor deposit " + withdraw
				+ " Current Balance " + bank.withDrawSync(number) + " count "
				+ number);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 5; i++) {
			show();

		}

	}
}
