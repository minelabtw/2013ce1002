package ce1002.a13.s102502554;

import java.util.Random;

public class Consumer implements Runnable{
	
	int ID;
	static Bank bank = new Bank();
	Random random = new Random();
	
	
	public Consumer(int i , Bank bank){
		this.ID = i;
		this.bank = bank;
	}
	
	
	public void run(){
	    // withdraw money from bank
		
		for (int i = 1 ; i <= 5 ; i++){
			int amount = 100*(random.nextInt(10));
			int balance = bank.withDrawSync(amount);
			System.out.println("No."+ID+" consumer withdraw " +amount  +" Current Balance " +balance + " count " +i);
		} 
		
	  }

}