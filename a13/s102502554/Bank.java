package ce1002.a13.s102502554;

public class Bank {
	  // please fill the blank
	private int money = 10000;
	private int balance = 0;
	private boolean wait;
	
	public Bank(){
		wait = false;
	}
	
	  public synchronized int withDrawSync(int amount) {
	    // if busy ....
		  if(wait){
			  try{
				  wait();
				  wait = false;
			  }catch(InterruptedException e){
				  e.printStackTrace();
			  }
		  }
	    // wait()
		  
	    // do process, use Thread.sleep() to simulate processing
	    try{
	    	if(money - amount < 0){
	    		money = 0;
	    	}else{
	    		money = money - amount;
	    	}
	    	Thread.sleep(1000);
	    }
	    catch(InterruptedException e){
	    	e.printStackTrace();
	    }
	    
	    balance = money;
	    notify();
	    wait = false;
	    // notify() and return
	    
	    return balance;
	  }
	  
	  public synchronized int deposite(int amount) {
		    // if busy ....
			  if(wait){
				  try{
					  wait();
					  wait = false;
				  }catch(InterruptedException e){
					  e.printStackTrace();
				  }
			  }
		    // wait()
			  
		    // do process, use Thread.sleep() to simulate processing
		    try{
		    	
		    	money = money + amount;
		    	
		    	Thread.sleep(1000);
		    }
		    catch(InterruptedException e){
		    	e.printStackTrace();
		    }
		    
		    balance = money;
		    notify();
		    wait = false;
		    // notify() and return
		    
		    return balance;
		  }
	}
