package ce1002.a13.s102502509;

public class Bank 
{
	 // please fill the blank
	  int money = 10000;
	  boolean consumer = false;
	  
	  public synchronized int Consumer(int amount) 
	  {
		//someone consume and wait
		if(consumer)
		{
			try 
			{
				wait();
			} 
			catch (InterruptedException e) 
			{
				
				e.printStackTrace();
			}
		} 
		else 
		{
			consumer = true;
			
			try 
			{
				Thread.sleep(1000);
				money -= amount;
			} 
			catch (InterruptedException e)
			{
				
				e.printStackTrace();
			}
		    consumer = false; 
		    notify();
		}
		if(money <= 0)// return the remain
		{
	    	return 0;
	    }
		else 
		{
	    	return money;
	    }
	  }
	  
		public synchronized int Depositor(int amount) {
			//someone depositor and wait
			if(consumer)
			{
				try 
				{
					wait();
				} 
				catch (InterruptedException e) 
				{
					
					e.printStackTrace();
				}
			} 
			else 
			{
				consumer = true;
				
				try 
				{
					Thread.sleep(1000);
					money += amount;// add some money
				} 
				catch (InterruptedException e)
				{
					
					e.printStackTrace();
				}
			    consumer = false; 
			    notify();
			    
			}
			 if(money <= 0)
				{
			    	return 0;
			    }
				else 
				{
			    	return money;
			    }
		}
}
		
		
