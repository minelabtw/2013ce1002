package ce1002.a13.s102502509;

import java.util.Random;

public class Consumer implements Runnable 
{
	Bank bank = new Bank();
	int number = 0;
	//random
	Random ran = new Random();
	int amount = ran.nextInt(10) * 100; 
	
	Consumer(Bank bank, int number)
	{
		this.number = number;
		this.bank = bank;
	}
	
	@Override
	public void run() 
	{
		
		//consume
		for(int i = 1 ; i < 6 ; i++)
		{
			System.out.println("No." + number + " Consumer " + amount 
					         + " Current Balance " + bank.Consumer(amount) + " count " + i);
		}
	}

}
