package ce1002.a13.s102502009;

public class A13 {
	public static void main(String[] args) {
		// initialize a bank
		Bank a = new Bank();
		// initialize 3 consumer and wrap with java thread
		Thread thread = new Thread(new Consumer(1, a));
		Thread thread1 = new Thread(new Consumer(2, a));
		Thread thread2 = new Thread(new Consumer(3, a));
		// initialize 3 depositor and wrap with java thread
		Thread thread3 = new Thread(new Depositor(1, a));
		Thread thread4 = new Thread(new Depositor(2, a));
		Thread thread5 = new Thread(new Depositor(3, a));
		// start each thread to perform process
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}
}