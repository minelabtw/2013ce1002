package ce1002.a13.s102502009;

public class Bank {
	private int money = 10000;
	private boolean busy = false;

	// please fill the blank
	public synchronized int withDrawSync(int amount, int count, int num)
			throws InterruptedException {
		// if busy ....
		// wait()
		if (busy) {
			wait();
		}
		busy = true;
		if (money - amount < 0)
			wait();
		money = money - amount;
		System.out.println("No." + num + " consumer withdraw " + amount
				+ " Current Balance " + money + " count " + count);
		busy = false;
		notify();
		return money;
		// do process, use Thread.sleep() to simulate processing
		// notify() and return
	}

	public synchronized int withINSync(int amount, int count, int num)
			throws InterruptedException {
		// if busy ....
		// wait()
		if (busy) {
			wait();
		}
		busy = true;
		money = money + amount;
		System.out.println("No." + num + " depositor deposit " + amount
				+ " Current Balance " + money + " count " + count);// 這裡輸出才不會有順序問題
		busy = false;
		notify();
		return money;
		// do process, use Thread.sleep() to simulate processing
		// notify() and return
	}
}
