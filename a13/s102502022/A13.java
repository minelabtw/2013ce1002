package ce1002.a13.s102502022;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bank bank = new Bank();//new a bank
		 
		Consumer[] consumers = new Consumer[3];// initialize 3 consumer
		Thread[] consumerThreads = new Thread[3];//initialize 3 thread
		for(int i = 0;i < 3;i++)
		{
			consumers[i] = new Consumer(i);
			consumers[i].bank = bank;
			consumerThreads[i] = new Thread(consumers[i]);
			consumerThreads[i].start();// start each thread 
		}

		Depositor[] depositors = new Depositor[3];
		Thread[] depositThreads = new Thread[3];
		for(int i = 0; i < 3;i++)
		{
			depositors[i] = new Depositor(i);
			depositors[i].bank = bank;
			depositThreads[i] = new Thread(depositors[i]);
			depositThreads[i].start();// start each thread 
		}
	}
	

}
