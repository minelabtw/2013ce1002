package ce1002.a13.s102502022;

public class Bank {

	private int balance = 10000;//initialize a value of balance
	
	
	private boolean isDrawing;
	private boolean isDepositing;

	Bank() {
		isDrawing = false;
	}

	public synchronized int withDrawSync(int amount)throws InterruptedException {
        if (isDrawing) {
			wait();
		}
        Thread.sleep(200);//sleep wait
		isDrawing = true;
		
		balance = balance - amount;
		if(balance < 0){
			balance = 0;
		}
		isDrawing = false;
		notify();
		return balance;
	}
	public synchronized int depositSync(int amount)throws InterruptedException {
        if (isDepositing) {
			wait();
		}
		isDepositing = true;
		Thread.sleep(200);//sleep wait
		balance = balance + amount;
		isDepositing = false;
		notify();
		return balance;
	}
}
