package ce1002.a13.s102502022;

import java.util.Random;

public class Consumer implements Runnable{

	private int ID;
	public Bank bank;
    private int amount;
	private int balance;
	private Random rand;
	
	
	public Consumer(int id)
	{
		rand = new Random();
		ID = id;
		
	}
	
	@Override
	public void run() {
		for(int i = 0; i < 5; i++){
			amount = rand.nextInt(800)+1;//update the amount
			try {
				balance = bank.withDrawSync(amount);
				System.out.println("No."+ID+" consumer withdraw :" +amount +" Current Balance " + balance + " count "+(i+1));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
