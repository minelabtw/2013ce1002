package ce1002.a13.s102502508;

public class Bank {


    public int surplus =10000 ;

 public synchronized int withDrawSync(int x) {
	 
	 try {
			Thread.sleep(1);//控制java的處理延遲時間
		 } 
	 catch (InterruptedException e) {
			e.printStackTrace();
		}

	    this.surplus= this.surplus-x;	//隨著使用者的提款或存款而更動
		if(this.surplus<0){	//餘額的下限是0
			return 0;
		}
		else
		return this.surplus;	//迴傳餘額值
	}
	
}
