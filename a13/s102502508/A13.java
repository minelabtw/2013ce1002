package ce1002.a13.s102502508;

public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//建立bank 物件
		Bank b= new Bank();
		// 產生Thread物件並加入其相關參數
		Thread t1= new Thread(new Consumer(b,1));
		Thread t2= new Thread(new Consumer(b, 2));
		Thread t3= new Thread(new Consumer(b, 3));
		Thread t4= new Thread(new Depositor(b,1));
		Thread t5= new Thread(new Depositor(b,2));
		Thread t6= new Thread(new Depositor(b,3));
		// 開始執行t.run()
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		
}
	}


