package ce1002.a13.s102502508;

import java.util.Random;


public class Consumer implements Runnable {
	
	Random rand;
	Bank bank;
	private int id;

	public Consumer(Bank a, int id) {
		this.bank = a;
		this.id = id;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		//利用迴圈進行五次的提款
		for (int j = 1; j <=5; j=j+1) {
			rand = new Random();
			int x = rand.nextInt(10) * 100;
			System.out.println("No." + id + " withdraw " + x+ " Current Balance " + bank.withDrawSync(x) + " count "+ j);
		}
	}

}
