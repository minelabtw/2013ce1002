package ce1002.a13.s102502561;

public class Bank {
	private int balance = 100000; 
    private boolean busy = false; 
    public synchronized int withDrawSync(int amount) {   //check bank busy static?
    	if( busy ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	busy = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 2000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance -= amount;
    	
    	busy = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
    
    public synchronized int depositSync(int amount) {   //save
    	if( busy ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	busy = true;
    	
    	try { 
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
    	
    	balance += amount;
    	
    	busy = false;
    	notify();
    	if( balance <= 0 )
    		return 0;
    	return balance;
    	
    } 
}