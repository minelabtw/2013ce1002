package ce1002.a13.s102502561;
import java.util.Random;

public class Consumer implements Runnable{
	private Bank bank; //bank
	private int guest; //guests
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.guest = number;
	}
 
	@Override
	public void run() {
		
		Random r = new Random(); 
        for(int i = 1; i <= 5; i++) {//withdraws money
            int amount =  r.nextInt(10) * 1000;//prints result
            int balanceRemain = bank.withDrawSync( amount );
            System.out.println("No."+ guest +" consumer withdraw " + amount + " Current Balance "
            + balanceRemain + " count " + i);

        }       
		
	}
 
}