package ce1002.a13.s102502518;

public class Bank {
	private int balance = 10000;
	private boolean state = false;
	public synchronized int withDrawSync(int amount)
	{
		if(state)
		{
			try {
				wait();
			} 
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		state = true;
		
		try {
			Thread.sleep(100);
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		balance -= amount;
		state = false;
		notify();
		if(balance<=0)
			return 0;
		return balance;
	}
	public synchronized int depositSync(int amount)
	{
		if(state)
		{
			try {
				wait();
			} 
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		state = true;
		try {
			Thread.sleep(100);
		} 
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		balance += amount;
		state = false;
		notify();
		if(balance<=0)
			return 0;
		return balance;
	}

}
