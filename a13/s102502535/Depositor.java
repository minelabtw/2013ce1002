package ce1002.a13.s102502535;

import java.util.Random;

public class Depositor implements Runnable {

	private Bank bank; // a member object of bank to deposit their money
	private int number; // the number of consumer

	public Depositor(Bank bank, int number) {
		this.bank = bank;
		this.number = number;
	}

	// this is the method template for java thread
	// once you override this method and put it into thread and you're good to
	// go.
	@Override
	public void run() {
		Random r = new Random();
		// we assume each consumer will deposit money for 5 times
		for (int i = 1; i <= 5; i++) {
			// give a certain amount
			int amount = r.nextInt(10) * 100;
			// go to bank to save his/her money
			int balanceRemain = bank.SaveSync(amount);
			// print out the result
			System.out.println("No." + number + " deposit " + amount
					+ " Current Balance " + balanceRemain + " count " + i);
		}

	}

}
