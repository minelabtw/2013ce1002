package ce1002.a13.s995002014;

import java.util.Random;

public class A13 {
	public static void main(String[] args) {
		// initialize a bank
		Bank bank = new Bank();
		
		// initialize 3 consumer and wrap with java thread
		Thread thread = new Thread(new Consumer(bank, 1));
		Thread thread1 = new Thread(new Consumer(bank, 2));
		Thread thread2 = new Thread(new Consumer(bank, 3));
		
		// initialize 3 depositor and wrap with java thread
		Thread thread3 = new Thread(new Depositor(bank, 1));
		Thread thread4 = new Thread(new Depositor(bank, 2));
		Thread thread5 = new Thread(new Depositor(bank, 3));
		
		// start each thread to perform process
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}
}

class Depositor implements Runnable{
	int id;
	int count;
	Bank bank;
	Depositor(Bank b,int id){
		bank=b;
		this.id=id;
	}
	public void run(){
		Random r=new Random();
		int amount=r.nextInt(10)*100;
		for(int i=0;i<10;i++) {
		count++;
		System.out.println("No."+id+" depositor deposit "+amount+" Current Balance "+bank.depositSync(amount)+" count "+count);
		}
	}
}

class Consumer implements Runnable{
	int id;
	int count;
	Bank bank;
	Consumer(Bank b,int id){
		bank=b;
		this.id=id;
	}
	public void run(){
		Random r=new Random();
		int amount=r.nextInt(10)*100;
		for(int i=0;i<10;i++) {
		count++;
		System.out.println("No."+id+" consumer withdraw "+amount+" Current Balance "+bank.withDrawSync(amount)+" count "+count);
		}
	}
}
	 
class Bank {
  // please fill the blank
	int money=10000;
	boolean available=true;
	public synchronized int withDrawSync(int amount)  {
	    if(available==false){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	    available=false;
		money-=amount;
		if(money<0)money=0;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		available=true;
	    notify(); 
	    return money;
	}
	public synchronized int depositSync(int amount) {
		 if(available==false){
				try {
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    }
		    available=false;
			money+=amount;
			if(money<0)money=0;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			available=true;
		    notify(); 
		    return money;
	}
}
