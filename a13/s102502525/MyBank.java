package ce1002.a13.s102502525;

import java.util.Random;

public class MyBank {

    int totalmoney;
    int money;
    boolean lock;

    MyBank(int total) {
        this.totalmoney = totalmoney;
        lock = false;
    }

    synchronized boolean deposit(int amount, int no, int i) {
    	
        while(lock) {
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
            
        }
        // 鎖
        lock = true;
        totalmoney += amount;
        
        System.out.println("No."+no+" depositor deposit "+amount+" Current Balance "+totalmoney+" count "+i);
        //取亂數
        Random rand = new Random();
            try {
                Thread.sleep(rand.nextInt(10)*10+10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
       //回傳
        lock = false;
        notify();
        return true;
    }

    public synchronized boolean withDrawSync(int amount, int no, int i) {

        while(lock) {
            try {
                wait();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        //鎖
        lock = true;
        if(totalmoney == 0) {
            money = -2147483647;
        } else if(totalmoney >= amount) {
            totalmoney -= amount;
            money = totalmoney;
        } else {
            money = -totalmoney;
        }
        if(money < 0) {
            if(money == -2147483647) {
                System.out.println("No."+no+" withdraw fail, Bank has no money count "+i);
                lock = false;
                notify();
                return false;
            } else {
                money = -money;
                System.out.println("No."+no+" withdraw fail Current Balance "+money+" count "+i);
            }
        } else {
            System.out.println("No."+no+" withdraw "+amount+" Current Balance "+money+" count "+i);
        }
        //取亂數
        Random rand = new Random();
            try {
                Thread.sleep(rand.nextInt(10)*10+10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        
        // 回傳
        lock = false;
        notify();
        return true;
    }
}