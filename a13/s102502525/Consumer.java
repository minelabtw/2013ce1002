package ce1002.a13.s102502525;

import java.util.Random;

public class Consumer implements Runnable{

    MyBank bank;
    int no;
    int wd;

    Consumer(int no, MyBank bank) {
        this.no = no;
        this.bank = bank;
    }

    public void run(){
        Random rand = new Random();
        for(int i=1 ; i<=5 ; i++) {
            wd = rand.nextInt(9) * 100 + 100;
            //�s��
            if(!bank.withDrawSync(wd, no, i)) {
                break;
            }
            
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}