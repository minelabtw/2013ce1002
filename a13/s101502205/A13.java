package ce1002.a13.s101502205;

public class A13 {
	
	private Bank bank;
	
	public A13(){
		// the bank!
		bank = new Bank();
		
		// Create threads
		Thread thread = new Thread(new Consumer(bank, 1));
		Thread thread1 = new Thread(new Consumer(bank, 2));
		Thread thread2 = new Thread(new Consumer(bank, 3));
		Thread thread3 = new Thread(new Depositor(bank, 1));
		Thread thread4 = new Thread(new Depositor(bank, 2));
		Thread thread5 = new Thread(new Depositor(bank, 3));
		
		// start all threads
		thread.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}
	
	public class Consumer implements Runnable{
		
		private int id;
		private Bank bank;
		
		public Consumer(Bank bank, int id){
			this.bank = bank;
			this.id = id;
		}
		
		public void run(){
			int balance;
			int withdraw;
			
			for (int i=1; i<=5; i++){
				// random withdraw money
				withdraw = (int)(Math.random()*9+1)*100;
				// withdraw money from bank
				balance = bank.withDrawSync(withdraw);
				// output
				System.out.println("No." + id + " consumer withdraw " + withdraw + " Current Balance " + balance + " count " + i);
			}
		}
	}

	public class Depositor implements Runnable{

		private int id;
		private Bank bank;

		public Depositor (Bank bank, int id){
			this.bank = bank;
			this.id = id;
		}
		
		public void run(){
			int balance;
			int money;
			
			for (int i=1; i<=5; i++){
				// random
				money = (int)(Math.random()*9+1)*100;
				// saving money
				balance = bank.depositSync (money);
				// output
				System.out.println("No." + id + " depositor deposit " + money + " Current Balance " + balance + " count " + i);
			}
		}
	}

	public class Bank {
		private boolean processing = false;
		private int money = 30000;
		
		public synchronized int withDrawSync (int amount) {
			// busy and wait
			while(processing) {
				try{
					wait();
				}catch(InterruptedException ie){
					ie.printStackTrace();
					System.out.println(ie.getMessage());
				}
			}
			
			// do process
			money -= amount;
			if (money<0) {
				money=0;
			}
			
			// simulating processing
			try{
				Thread.sleep((int)(Math.random()*1000));
			}catch(InterruptedException ie){
				ie.printStackTrace();
				System.out.println(ie.getMessage());
			}
			
			// notify and return
			notify();
			return money;
		}
		
		public synchronized int depositSync (int amount) {
			// busy and wait
			while(processing) {
				try{
					wait();
				}catch(InterruptedException ie){
					ie.printStackTrace();
					System.out.println(ie.getMessage());
				}
			}
			
			// do process
			money += amount;
			
			// simulating processing
			try{
				Thread.sleep((int)(Math.random()*1000));
			}catch(InterruptedException ie){
				ie.printStackTrace();
				System.out.println(ie.getMessage());
			}
			
			// notify and return
			notify();
			return money;
		}
	}
	
	public static void main(String[] args) {
		A13 goGoPowerRangers = new A13();
	}
}