package ce1002.a13.s102502021;

public class Bank {
	private int money = 10000;
	private boolean busy = false;

	public synchronized int withDrawSync(int amount, int count, int num)
			throws InterruptedException {
		if (busy) {
			wait();
		}
		busy = true;
		if (money - amount < 0)
			wait();
		money = money - amount;
		System.out.println("No." + num + " consumer withdraw " + amount
				+ "  Current Balance " + money + "  count " + count);
		busy = false;
		notify();
		return money;
	}

	public synchronized int withINSync(int amount, int count, int num)
			throws InterruptedException {
		if (busy) {
			wait();
		}
		money = money + amount;
		System.out.println("No." + num + " consumer withdraw " + amount
				+ " Current Balance " + money + " count " + count);
		busy = false;
		notify();
		return money;

	}

}
