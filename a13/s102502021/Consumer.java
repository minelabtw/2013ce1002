package ce1002.a13.s102502021;

public class Consumer implements Runnable {
	private int num;
	private Bank bank = new Bank();
	private int count = 0;

	public Consumer(int a, Bank b) {
		bank = b;
		num = a;

	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				int a = (int) (Math.random() * 1000);
				bank.withDrawSync(a, ++count, num);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
