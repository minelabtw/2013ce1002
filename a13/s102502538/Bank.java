package ce1002.a13.s102502538;

public class Bank {

	private int money = 10000; // bank's total money
    private boolean busying = false; 

	public synchronized int withDrawSync(int amount) {
	  	if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	// set the busying flag to true
    	busying = true;
    	
    	try { 
            // sleep with random interval
    		// simulate process
            Thread.sleep((int) (Math.random() * 3000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
		money -= amount;
		
	   	busying = false;
    	notify();

		if (money <= 0)
			return 0;
		return money;
	}
	public synchronized int deposit(int amount) {
		if( busying ){
    		try {
    			wait();
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    	}
    	
    	// set the busying flag to true
    	busying = true;
    	
    	try { 
            // sleep with random interval
    		// simulate process
            Thread.sleep((int) (Math.random() * 1000)); 
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
		
		money += amount;
	   	busying = false;
    	notify();

		return money;
	}
	

}
