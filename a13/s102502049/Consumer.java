package ce1002.a13.s102502049;

public class Consumer implements Runnable {
	// please override this method when executed by thread
	private int id = 0;
	private Bank bank;
	private int count = 0;
	
	public Consumer(Bank bank,int id) {	//	set id and bank
		// TODO Auto-generated constructor stub
		this.bank = bank;
		this.id = id;
	}

	public void run() {
		// withdraw money from bank
		
		for(int i=0; i<5; i++){
			count++;
			int amount = (int)(Math.random()*500);	//	random amount
			System.out.println("No."+ id +" Consumer withdraw "+ amount +" Current Balance "+ bank.withDrawSync(amount) +" count "+ count);	//	show message 
		}
	}
}
