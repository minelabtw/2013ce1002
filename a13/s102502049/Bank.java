package ce1002.a13.s102502049;

public class Bank {
	// please fill the blank
	private int balance = 9500;	//	default balance
	private boolean lock = false;	//	default lock is false
	Bank(){
		
	}

	public synchronized int withDrawSync(int amount) {
		// if busy ....
		// wait()
		if(lock==true){
			 try 
	            {
	                wait(); 
	                //withDraw = true;
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            }             
		}
		// do process, use Thread.sleep() to simulate processing
		lock = true;	//	lock on
		try
		{		
			if(balance - amount < 0 )
				balance = 0;
			else
				balance = balance-amount;
			Thread.sleep(100);
		}
		catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 

		// notify() and return
		lock = false;	//	lock off
		notify();
		return balance;
	}
	
	public synchronized int deposit(int amount) {
		// if busy ....
		// wait()
		if(lock==true){
			 try 
	            {
	                wait(); 
	                //withDraw = true;
	            } 
	            catch(InterruptedException e) { 
	                e.printStackTrace(); 
	            }             
		}
		// do process, use Thread.sleep() to simulate processing
		lock = true;	//	lock on
		try
		{		
			balance = balance+amount;
			Thread.sleep(100);
		}
		catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 

		// notify() and return
		lock = false;	//	lock off
		notify();
		return balance;
	}
}
