package ce1002.a13.s102502049;

public class Depositor implements Runnable{
	private int id = 0;
	private Bank bank;
	private int count = 0;
	
	public Depositor(Bank bank,int id) {	//	set id and bank
		// TODO Auto-generated constructor stub
		this.bank = bank;
		this.id = id;
	}

	public void run() {
		// deposit money from bank
		
		for(int i=0; i<5; i++){
			count++;
			int amount = (int)(Math.random()*500);	//	random amount
			System.out.println("No."+ id +" Depositor deposit "+ amount +" Current Balance "+ bank.deposit(amount) +" count "+ count);	//	show message 
		}
	}
}
