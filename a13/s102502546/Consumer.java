package ce1002.a13.s102502546;

import java.util.Random;

public class Consumer implements Runnable {
	int no = 0;
	Bank bank = new Bank();
	Random rand = new Random();
	int amount = rand.nextInt(901);

	Consumer(Bank bank, int no) {
		this.no = no;
		this.bank = bank;
	}

	@Override
	public void run() {
		// depose
		for (int i = 1; i <= 5; i++) {
			System.out.println("No." + no + " consumer withdraw " + amount
					+ " Current Balance " + bank.withDrawSync(amount)
					+ " count " + i);
		}
	}

}
