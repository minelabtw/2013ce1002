package ce1002.a13.s102502546;

public class Bank {
	int money = 10000;
	boolean people = false;

	// 領
	public synchronized int withDrawSync(int amount) {
		if (people) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			people = true;

			try {
				Thread.sleep(3000);
				money = money - amount;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			people = false;
			notify();
		}
		if (money > 0) {
			return money;
		} else {
			return 0;
		}
	}

	// 存
	public synchronized int depositor(int amount) {
		if (people) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			people = true;

			try {
				Thread.sleep(2000);
				money = money + amount;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			people = false;
			notify();

		}

		if (money > 0) {
			return money;
		} else {
			return 0;
		}

	}

}