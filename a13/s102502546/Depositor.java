package ce1002.a13.s102502546;

import java.util.Random;

public class Depositor implements Runnable {
	int no = 0;
	Bank bank = new Bank();
	Random ran = new Random();
	int amount = ran.nextInt(901);

	Depositor(Bank bank, int no) {
		this.no = no;
		this.bank = bank;
	}

	@Override
	public void run() {
		// save
		for (int i = 1; i <= 5; i++) {
			System.out.println("No." + no + " depositor deposit " + amount
					+ " Current Balance " + bank.depositor(amount) + " count "
					+ i);
		}
	}
}