package ce1002.a13.s102502559;


public class A13 {

	public static void main(String[] args) {
		// initialize a bank
		Bank bank = new Bank();
		 
		// initialize 3 consumer and wrap with java thread
				
		Consumer[] consumers = new Consumer[3];
		Thread[] cThreads = new Thread[3];
				
		for(int i = 0;i < 3;i++)
		{
			consumers[i] = new Consumer(i);
			consumers[i].bank = bank;
			cThreads[i] = new Thread(consumers[i]);
			cThreads[i].start();// start each thread to perform process
		}

		Depositor[] depositors = new Depositor[3];
		Thread[] dThreads = new Thread[3];
		// initialize 3 depositor and wrap with java thread
		for(int i = 0; i < 3;i++)
		{
			depositors[i] = new Depositor(i);
			depositors[i].bank = bank;
			dThreads[i] = new Thread(depositors[i]);
			dThreads[i].start();// start each thread to perform process
		}
	}

}
