package ce1002.a13.s102502559;

import java.util.Random;

public class Consumer implements Runnable{
	private int ID;
	public Bank bank;
	private Random ran;
	private int amount;
	private int balance;
	
	public Consumer(int id)
	{
		ID = id;
		ran = new Random();
	}
	
	@Override
	public void run() {
		for(int i = 0; i < 5; i++)
		{
			amount = ran.nextInt(800)+1;
			try {
				balance = bank.withDrawSync(amount);
				System.out.println("No."+ID+" consumer withdraw :" +amount +" Current Balance " + balance + " count "+(i+1));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
