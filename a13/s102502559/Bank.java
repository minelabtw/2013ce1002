package ce1002.a13.s102502559;

public class Bank {
	private int balance = 10000;
	private boolean isDrawing;
	private boolean isDepositing;

	Bank() {
		isDrawing = false;
	}

	public synchronized int withDrawSync(int amount)
			throws InterruptedException {

		if (isDrawing) {
			wait();
		}
		isDrawing = true;
		// do process, use Thread.sleep() to simulate processing
		Thread.sleep(200);
		balance = balance - amount;
		if(balance < 0)
		{
			balance = 0;
		}
		isDrawing = false;
		notify();
		return balance;
	}
	public synchronized int depositSync(int amount)
			throws InterruptedException {

		if (isDepositing) {
			wait();
		}
		isDepositing = true;
		// do process, use Thread.sleep() to simulate processing
		Thread.sleep(200);
		balance = balance + amount;
		isDepositing = false;
		notify();
		return balance;
	}
	
	
	
	
	
	
	
}
