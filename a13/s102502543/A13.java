package ce1002.a13.s102502543;

public class A13 {
	public static void main(String[] args) {
		Bank bank = new Bank();
		Thread thread1 = new Thread(new Consumer(bank, 1));
		Thread thread2 = new Thread(new Consumer(bank, 2));
		Thread thread3 = new Thread(new Consumer(bank, 3));
		Thread thread4 = new Thread(new Depositor(bank, 1));
		Thread thread5 = new Thread(new Depositor(bank, 2));
		Thread thread6 = new Thread(new Depositor(bank, 3));
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();
	}
}
