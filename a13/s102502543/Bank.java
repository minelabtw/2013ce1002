package ce1002.a13.s102502543;

public class Bank 
{
	int total = 10000;
	public synchronized int withDrawSync(int amount)
	{
		try 
		{
			Thread.sleep(1500);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		total = total - amount;
		return total;
	}
}
