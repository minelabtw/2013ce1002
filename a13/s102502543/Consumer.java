package ce1002.a13.s102502543;

import java.util.Random;

public class Consumer implements Runnable 
{
	Bank bank;
	int id;
	Consumer(Bank bank, int id)
	{
		this.bank = bank;
		this.id = id;
	}
	public void run() 
	{
		Random ran = new Random();
		for (int t = 1; t <= 5; t++) 
		{
			int a = 100 * ran.nextInt(10);
			System.out.println("No." + id + " witdraw " + a
					+ " Current Balance " + bank.withDrawSync(a) + " count "
					+ t);
		}
	}
}
