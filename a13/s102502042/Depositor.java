package ce1002.a13.s102502042;

import java.util.Random;

public class Depositor implements Runnable{
	int id;
	int count;
	Bank bk;
	Depositor(int id,Bank bk)
	{
		this.id = id;
		this.count = 0;
		this.bk = bk;
	}
	
	//override method
	public void run(){
		// deposit money from bank
		Random rand = new Random();
		for(int i=0;i<10;++i)
		{
			int amount = rand.nextInt(1000);
			try {
				count++;
				bk.deposit(amount,id,count);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
