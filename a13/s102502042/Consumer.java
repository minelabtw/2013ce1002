package ce1002.a13.s102502042;

import java.util.Random;

public class Consumer implements Runnable{
		int id;
		int count;
		Bank bk;
		Consumer(int id,Bank bk)
		{
			this.id = id;
			this.count = 0;
			this.bk = bk;
		}
		
		// override
		public void run(){
			// withdraw money from bank
			Random rand = new Random();
			for(int i=0;i<10;++i)
			{
				//random withdraw money
				int amount = rand.nextInt(1000);
				try {
					count++;
					bk.withDrawSync(amount,id,count);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
}
