package ce1002.a13.s102502042;

public class Bank {
	  // please fill the blank
		int money;
		int drawmoney;
		int depositmoney;
		boolean lock;
		//constructor
		Bank()
		{
			money = 10000;
			drawmoney = 0;
			depositmoney = 0;
			lock = false;
		}
		
		public int currentMoney()
		{
			return money;
		}
		
		public int getDrawMoney()
		{
			return drawmoney;
		}
		
		public int getDepositMoney()
		{
			return depositmoney;
		}
		public synchronized void withDrawSync(int amount,int id,int count) throws InterruptedException {
			//if lock then wait
			while(lock)
			{
				wait();
			}
			lock = true;
			//delay 0.1 sec
			Thread.sleep(100);
			if(money <= amount)
			{
				drawmoney = 0;
				System.out.println("No."+id+" consumer withdraw " + getDrawMoney() + " Current Balance " + currentMoney() + " count " + count);
				lock = false;
				notify();
			}
			else
			{ 
				drawmoney = amount;
				money -= drawmoney;
				System.out.println("No."+id+" consumer withdraw " + getDrawMoney() + " Current Balance " + currentMoney() + " count " + count);
				lock = false;
				notify();
			}
			
		}
		
		public synchronized void deposit(int amount,int id,int count) throws InterruptedException {
			while(lock)
			{
				wait();
			}
			lock = true;
			//delay 0.1 sec
			Thread.sleep(100);
			money += amount;
			depositmoney = amount;
			System.out.println("No."+id+" depositor deposit " + amount + " Current Balance " + currentMoney() + " count " + count);
			lock = false;
			notify();
		}
}