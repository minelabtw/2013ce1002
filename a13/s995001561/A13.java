package ce1002.a13.s995001561;

public class A13 {
	public static void main(String[] args) {
		Bank bank = new Bank();		
		// Create tasks
		Runnable consumer1 = new Consumer("consumer1", bank);
		Runnable consumer2 = new Consumer("consumer2", bank);
		Runnable consumer3 = new Consumer("consumer3", bank);
		
		Runnable depositor1 = new Depositor("depositor1", bank);
		Runnable depositor2 = new Depositor("depositor2", bank);
		Runnable depositor3 = new Depositor("depositor3", bank);
		
		// Create threads
		Thread thread1 = new Thread(consumer1);
		Thread thread2 = new Thread(consumer2);
		Thread thread3 = new Thread(consumer3);
		
		Thread thread4 = new Thread(depositor1);
		Thread thread5 = new Thread(depositor2);
		Thread thread6 = new Thread(depositor3);
		
		// Star threads
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
		thread6.start();
	}
}
