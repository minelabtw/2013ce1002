package ce1002.a13.s995001561;

public class Bank {
	  int money = 100000;

	  
	  Bank() {
	  
	  }
	  // please fill the blank
	  public synchronized int withDrawSync(int amount, String number, int count) {
	    // if busy ....
	    // wait()
  
		money = money - amount;
		
		System.out.println(number + " withdraw " + amount + " Current Balance " + money + " count " + count);
		
		return money;
			   
	    // do process, use Thread.sleep() to simulate processing
	    
	    // notify() and return 
	  }
	  
	  public synchronized int deposite(int amount, String number, int count) {
		    // if busy ....
		    // wait()
	  
			money = money + amount;
			
			System.out.println(number + " deposit " + amount + " Current Balance " + money + " count " + count);
			
			return money;
				   
		    // do process, use Thread.sleep() to simulate processing
		    
		    // notify() and return 
		  }
	  
	  
}
