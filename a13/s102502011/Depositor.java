package ce1002.a13.s102502011;

import java.util.Random;

public class Depositor implements Runnable{
	Bank bank = new Bank(); // 使用者所對應的銀行
	Random rand = new Random();
	int id = 0; // 消費者編號
	int counter = 0; // 已領錢次數

	// 在constructor中，利用傳入的值設定id和bank
	Depositor(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			int amount = rand.nextInt(6) * 100;
			counter++;
			bank.DepositeSync(id, amount, counter);
		}
	}
}
