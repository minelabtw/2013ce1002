package ce1002.a13.s102502011;

public class Bank {
	private int cash = 10000; //銀行餘額
	boolean go = false; //銀行當前狀態

	public synchronized void withDrawSync(int ID, int amount, int counter) {

		//當狀態為true的時候，代表還有人在使用，則將之丟到wait裡面
		while (go == true) {
			try {
				wait();
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		} 
		go = true; //當有人開始領錢的時候，就將狀態設為true
			try {
				Thread.sleep(500); //暫停
			}
			catch (Exception ee) {
				ee.printStackTrace();
			}
			if (cash - amount <= 0) {
				cash = 0;
			} 
			else {
				cash -= amount;
			}
			System.out.println("No." + (ID + 1) + " withdraw " + amount
					+ " Current Balance " + cash + " count " + counter);
			go = false; //領完錢之後，就讓狀態回到false
			notify(); //讓wait裡面的下一個動作開始執行
	}
	
	public synchronized void DepositeSync(int ID, int amount, int counter) {
		if (go == true)  //有人在存錢的時候要等一下
			try {
				wait() ;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		
		go = true ;  //正在存錢
		try {
			Thread.sleep(500); //暫停500ns
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		cash += amount ;//正在存錢
		System.out.println("No." + (ID + 1) + " deposite " + amount
				+ " Current Balance " + cash + " count " + counter);
		                       
		go = false ; //存錢結束
		notify() ;  //讓wait裡面的下一個動作開始執行
	}
}
