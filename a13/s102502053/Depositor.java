package ce1002.a13.s102502053;

import java.util.Random;

public class Depositor implements Runnable{
	private Bank bank; // a member object of bank to deposit their money
	private int number;
	
	public Depositor(Bank bank,int number)
	{
		this.bank = bank;
		this.number = number;
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random r = new Random(); // RNG
		for(int i = 1; i <= 5; i++) 
		{
			int amount = r.nextInt(1000);
			int balanceRemain = bank.depositSync( amount );
			// print out the result
			System.out.println("No."+ number + " depositor deposits " + amount + " Current Balance "+ balanceRemain + " count " + i);
		}
	}

}
