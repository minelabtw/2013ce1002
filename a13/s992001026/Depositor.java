package ce1002.a13.s992001026;

import java.util.Random;
public class Depositor implements Runnable{
	
	private Bank bank; // a member object of bank 
	private int number; // the number of depositor
	public Depositor(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
	public void run() {
		Random r = new Random(); // RNG 
		// we assume each depositor will deposit money for 10 times
        for(int i = 1; i <= 5; i++) {
        	// give a certain amount
            int amount =  r.nextInt(10) * 100;
            // go to bank to deposit his/her money
            int balanceRemain = bank.withDrawSync( amount , 2);
            // print out the result
            System.out.println("No."+ number + " depositor deposit " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}

}
