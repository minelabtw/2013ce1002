package ce1002.a13.s992001026;

import java.util.Random;

public class Consumer implements Runnable{
	private Bank bank; // a member object of bank to withdraw their money
	private int number; // the number of consumer
	public Consumer(Bank bank,int number){
		 this.bank = bank; 
		 this.number = number;
	}
 
	// this is the method template for java thread 
	// once you override this method and put it into thread and you're good to go.
	// ** Remember to call thread.start() to execute this method **
	@Override
	public void run() {
		
		Random r = new Random(); // RNG 
		// we assume each consumer will withdraw money for 10 times
        for(int i = 1; i <= 10; i++) {
        	// give a certain amount
            int amount =  r.nextInt(10) * 100;
            // go to bank to get his/her money
            int balanceRemain = bank.withDrawSync( amount , 1 );
            // print out the result
            System.out.println("No."+ number + " consumer  withdraw " + amount + " Current Balance "+ balanceRemain + " count " + i);
        }       
		
	}

}
