package ce1002.a13.s102502032;

import java.util.Random;

public class Consumer implements Runnable
{
	private int		ID;
	private Bank	bank;
	private int		counter	= 1;

	// please override this method when executed by thread
	public Consumer(int i, Bank b)
	{
		this.ID = i;
		this.bank = b;
	}

	public void run()
	{
		// withdraw money from bank
		Random rnd = new Random(System.currentTimeMillis());
		for (int i = 0; i < 5; i ++)
		{
			bank.withDrawSync(this.ID, rnd.nextInt(10) * 100, counter, true);
			counter ++;
		}
	}
}
