package ce1002.a13.s102502032;

public class A13
{
	public static void main(String[] args)
	{
		Bank bank = new Bank();
		Thread t1 = new Thread(new Consumer(0, bank));
		t1.start();
		Thread t2 = new Thread(new Consumer(1, bank));
		t2.start();
		Thread t3 = new Thread(new Consumer(2, bank));
		t3.start();
		Thread t4 = new Thread(new Depositor(0, bank));
		t4.start();
		Thread t5 = new Thread(new Depositor(1, bank));
		t5.start();
		Thread t6 = new Thread(new Depositor(2, bank));
		t6.start();
	}
}
