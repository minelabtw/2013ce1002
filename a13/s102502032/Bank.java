package ce1002.a13.s102502032;

public class Bank
{
	private int	money	= 10000;
	private int	working	= -1;

	// Boolean mode: T: Comsumer F: Depositor
	public synchronized void withDrawSync(int id, int amount, int counter,
			Boolean mode)
	{
		// System.out.println(working);
		// if busy ....
		// wait()
		while (this.working != id && this.working != -1)
		{
			try
			{
				wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		// set up the new balance
		this.setWorking(id);
		if (mode == true)
		{
			this.money -= amount;
			if (this.money <= 0)
			{
				this.money = 0;
			}
			System.out.println("No." + id + " consumer withdraw " + amount
					+ " Current Balance " + this.money + " count " + counter);
		}
		else
		{
			this.money += amount;
			if (this.money <= 0)
			{
				this.money = 0;
			}
			System.out.println("No." + id + " depositor deposit " + amount
					+ " Current Balance " + this.money + " count " + counter);
		}
		// notify() and reset work state
		this.notify();
		this.working = -1;
	}

	public void setWorking(int w)
	{
		this.working = w;
	}

	public int getWorking()
	{
		return this.working;
	}
}
