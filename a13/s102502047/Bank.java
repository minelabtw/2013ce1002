package ce1002.a13.s102502047;

public class Bank {
	private int t=10000;//總共10000
	public synchronized int withDrawSync(int a) { //領錢
		t=t-a;			  
		if(t<=0)
			  return 0;
		else
			  return t;  
	}
	public synchronized int depositSync(int d){//存錢
		t=t+d;
		return t;
	}
}
