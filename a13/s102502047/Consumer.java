package ce1002.a13.s102502047;

import ce1002.a13.s102502047.Bank;



public class Consumer implements Runnable{
	private int n=0;
	private Bank b;
	public Consumer(Bank b,int n) {
		this.b=b;
		this.n=n;
	}
	public void run(){
		for(int i=0;i<5;i++)
		    {
		    	int a=(int)(Math.random()*9+1)*100;
		    	int j=i+1;
		    	synchronized (b){
					int remain=b.withDrawSync(a);
					System.out.println("No."+n+" withdraw "+a+" Current Balance "+remain+" count "+j);
				}

		    }
		  }
	}


