package ce1002.a13.s102502047;

import ce1002.a13.s102502047.Bank;
import ce1002.a13.s102502047.Consumer;

public class A13 {

	public static void main(String[] args) {
		Bank b=new Bank();
		Thread t1=new Thread(new Consumer(b,1));//建立6條執行續
		Thread t2=new Thread(new Consumer(b,2));
		Thread t3=new Thread(new Consumer(b,3));
		Thread d1=new Thread(new Depositor(b,1));
		Thread d2=new Thread(new Depositor(b,2));
		Thread d3=new Thread(new Depositor(b,3));
		t1.start();
		t2.start();
		t3.start();
		d1.start();
		d2.start();
		d3.start();
	}

}
