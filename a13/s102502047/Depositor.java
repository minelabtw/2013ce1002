package ce1002.a13.s102502047;

public class Depositor implements Runnable{
	private int n=0;
	private Bank b;
	public Depositor(Bank b,int n) {
		this.b=b;
		this.n=n;
	}
	public void run(){
		for(int i=0;i<5;i++)
		    {
		    	int d=(int)(Math.random()*9+1)*100;
		    	int j=i+1;
		    	synchronized (b){
					System.out.println("No."+n+" depositor "+d+" Current Balance "+b.depositSync(d)+" count "+j);
				}

		    }
		  }
	}
