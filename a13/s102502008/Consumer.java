package ce1002.a13.s102502008;

public class Consumer implements Runnable
{
	private int count=1 ;
	private int id ;
	private int amount ;
	Bank bank ;
	public Consumer(Bank bank, int id)
	{
		this.bank= bank ;
		this.id= id ;
	}
	// please override this method when executed by thread
	  public synchronized void run()
	  {
		  for(int i=0; i<5 ;i++)
		  {
			  amount=(int)(Math.random()*10)*100 ;
			  try {
				System.out.println("No."+id+" consumer withdraw "+amount+" Current Balance "+bank.withDrawSync(amount)+" count "+count) ;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  count++ ;
		  }
	  }
	  
}
