package ce1002.a13.s102502008;

public class Bank 
{
		private int money=10000 ;
		public Bank()
		{
			
		}
		public synchronized int withDrawSync(int amount) throws InterruptedException 
		{
		    // if busy ....
		    // wait()
			Thread.sleep(500);
			if(money<=amount) 
				  amount=money ;
			  if(money==0)
				  Thread.yield() ;
			  money-=amount ;
		    // do process, use Thread.sleep() to simulate processing
		    return money ;
		    // notify() and return 
		  }
		public synchronized int DepositorSync(int amount) throws InterruptedException {
		    // if busy ....
		    // wait()
			Thread.sleep(500);
			money+=amount ;
		    // do process, use Thread.sleep() to simulate processing
		    return money ;
		    // notify() and return 
		  }
}
