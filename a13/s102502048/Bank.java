package ce1002.a13.s102502048;

public class Bank 
{
	private int balance = 10000; 
	private int _m = 0;
	private boolean busying ;
	public Bank()
	{
		busying  = false;
	}
	public synchronized void withDrawSync(int amount)
	{
		if(busying)
		{
            try 
            {
                wait(); 
            } 
            catch(InterruptedException e) { 
                e.printStackTrace(); 
            }             
        }
		// set the busying flag to true
    	busying = true;
		try{
			Thread.sleep(100);
		}
		catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
		if(balance - amount < 0 )
				balance = 0;
			else
				balance = balance-amount;
		_m = balance;
		busying = false;
		notify(); 
	}
	public int getmoney()
	{
		return _m;
	}
}
