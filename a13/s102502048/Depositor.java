package ce1002.a13.s102502048;

import java.util.Random;

public class Depositor implements Runnable
{
	private Bank bank;
	private String name;
	Random ran = new Random();
	Depositor(Bank bank, String name)
    {  
        this.bank = bank; 
        this.name = name;
    }  
    public void run()  
    {  
        for(int x = 1;x<=3;x++)  
        {  
        	int mon = (ran.nextInt(9)+1)*100;
        	int _mon = (-1)*mon;
            bank.withDrawSync(_mon);// go to bank to depositor his/her money
            // print out the result
            System.out.println("No."+name+" depositor deposit "+mon+" Current Balance "+bank.getmoney()+" count "+x);
        }  
    }  
}
