package ce1002.a13.s102502048;

import java.util.Random;


public class Consumer implements Runnable
{
	private Bank bank;
	private String name;
	Random ran = new Random();
	Consumer(Bank bank, String name)  
    {  
        this.bank = bank; 
        this.name = name;
    }  
    public void run()  
    {  
        for(int x = 1;x<=3;x++)  
        {  
        	int mon = (ran.nextInt(9)+1)*100;
            bank.withDrawSync(mon);// go to bank to get his/her money
            // print out the result
            System.out.println("No."+name+" consumer withdraw "+mon+" Current Balance "+bank.getmoney()+" count "+x);
        }  
    }  
}
