package ce1002.a13.s101201506;

public class Bank {
	private int money = 0; //錢
	private int person = 0; //人

	Bank() {
		money = 10000; //一開始的金額
	}

	public synchronized int withDrawSync(int id, int amount) {
		while (person != 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			person = 1;
			Thread.sleep(1000);// simulate the process
		} catch (InterruptedException e) {
			System.out.println("Out2");
		}
		person = 0;
		notify();

		// withdraw
		money = money - amount;
		if (money < 0) {
			money = 0;
		}
		return money;
	}

	public synchronized int depositSync(int id, int amount) { //計算存款內的錢	
		while (person != 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			person = 1;
			Thread.sleep(500);
		} catch (InterruptedException e) {
			System.out.println("Out2");
		}
		person = 0;
		notify();

		money = money + amount;
		//如果錢為負的 輸出=0
		if (money < 0) { 
			money = 0;
		}
		return money;
	}
}