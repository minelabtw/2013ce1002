package ce1002.a13.s102502529;

public class Bank {

	private int balance = 10000;
	private boolean isDrawing , isDepositing;

	Bank() {
		isDrawing = false;
		isDepositing = false;
	}

	public synchronized int withDrawSync(int amount)								//�����lock object
			throws InterruptedException {

		if (isDrawing) {
			wait();
		}
		isDrawing = true;
		// do process, use Thread.sleep() to simulate processing
		Thread.sleep(500);
		balance = balance - amount;
		if(balance < 0)
			balance = 0;
		isDrawing = false;															
		notify();																	//unlock
		return balance;
	}
	public synchronized int withdeposit(int amount)									//�s�� and lock object 
			throws InterruptedException {

		if (isDepositing) {
			wait();
		}
		isDepositing = true;
		// do process, use Thread.sleep() to simulate processing
		Thread.sleep(200);
		balance = balance + amount;
		if(balance < 0)
			balance = 0;
		isDepositing = false;
		notify();																	//unlock
		return balance;
	}
}
