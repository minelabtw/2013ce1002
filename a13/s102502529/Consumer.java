package ce1002.a13.s102502529;

import java.util.Random;

public class Consumer implements Runnable {

	private int ID;
	public Bank bank;
	private Random ran;
	private int draw;
	private int balance;

	public Consumer(int id) {
		ID = id;
		ran = new Random();
	}

	@Override
	public void run() {
		for (int i = 1; i < 6; i++) {
			draw = ran.nextInt(10) * 100; // 隨機提出直
			try {
				balance = bank.withDrawSync(draw); // 對銀行進行提款
				System.out.println("No." + ID + " withdraw:" + draw
						+ " Current Balance " + balance + " count " + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
