package ce1002.a13.s102502529;

import java.util.Random;

public class Depositor implements Runnable {

	private int ID;
	public Bank bank;
	private Random ran;
	private int deposit;
	private int balance;

	public Depositor(int id) {
		ID = id;
		ran = new Random();
	}

	@Override
	public void run() {
		for (int i = 1; i < 6; i++) {
			deposit = ran.nextInt(10) * 100; // 隨機存入直
			try {
				balance = bank.withdeposit(deposit); // 對銀行作存錢動作
				System.out.println("No." + ID + " deposit " + deposit // 輸出結果
						+ " Current Balance " + balance + " count " + i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
