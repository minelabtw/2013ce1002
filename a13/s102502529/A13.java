package ce1002.a13.s102502529;



public class A13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Consumer[] guest = new Consumer[3];									//物件話題款者
		guest[0] = new Consumer(1);
		guest[1] = new Consumer(2);
		guest[2] = new Consumer(3);
		Depositor [] daddy = new Depositor[3];								//物件畫存款者
		daddy [0] = new Depositor(1);
		daddy [1] = new Depositor(2);
		daddy [2] = new Depositor(3);
		Bank bankTotal = new Bank();										//新增銀行物件
		guest[0].bank = bankTotal;											//針對每個物件對銀行做連結
		guest[1].bank = bankTotal;
		guest[2].bank = bankTotal;
		daddy [0].bank = bankTotal;
		daddy [1].bank = bankTotal;
		daddy [2].bank = bankTotal;

		Thread thread1 = new Thread(guest[0]);								//進行執行續
		thread1.start();
		Thread thread2 = new Thread(guest[1]);
		thread2.start();
		Thread thread3 = new Thread(guest[2]);
		thread3.start();
		Thread thread4 = new Thread(daddy [0]);
		thread4.start();
		Thread thread5 = new Thread(daddy [1]);
		thread5.start();
		Thread thread6 = new Thread(daddy [2]);
		thread6.start();
	}

}
