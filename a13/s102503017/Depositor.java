package ce1002.a13.s102503017;

public class Depositor implements Runnable{

	private Bank bank;
	int deposit, depositorNumber;
	static int[] counter = new int[3];
	
	//parameter constructor of Consumer class.
	Depositor(Bank bank)
	{
		this.bank = bank;
	}
	
	
	@Override
	public void run() 
	{

		for(int i = 0; i < 5;i++)
		{
			//producing the depositing amount and the depositor number with random.
			deposit = (int) (Math.random() * 1000);
			depositorNumber = (int) (Math.random() *3);


			//print out the result.
			System.out.println("No." + (depositorNumber + 1) + " depositor deposits " + deposit  + " Current Balance "  + bank.BalancingSync(deposit) + " count " + bank.depositorFrequency(depositorNumber));
			Thread.yield();
		}
		
	}

}