package ce1002.a13.s102503017;

public class A13 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Bank bank= new Bank();
		
		Thread thread0 = new Thread( new Consumer(bank) );
		Thread thread1 = new Thread( new Consumer(bank) );
		Thread thread2 = new Thread( new Consumer(bank) );
		
		Thread thread3 = new Thread( new Depositor(bank) );
		Thread thread4 = new Thread( new Depositor(bank) );
		Thread thread5 = new Thread( new Depositor(bank) );
		
		// ** this is how you start a thread **
		thread0.start(); 
		thread1.start(); 
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();

	}

}
