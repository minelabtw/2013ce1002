package ce1002.a13.s102503017;

public class Consumer implements Runnable {

	private Bank bank;
	int withDraw, consumerNumber;

	
	//parameter constructor of Consumer class.
	Consumer(Bank bank)
	{
		this.bank = bank;
	}
	
	
	@Override
	public void run() 
	{

		for(int i = 0; i < 5;i++)
		{
			//producing the withdrawing amount and the consumer number with random.
			withDraw = (int) (Math.random() * (-1000));
			consumerNumber = (int) (Math.random() *3);

			//print out the result.
			System.out.println("No." + (consumerNumber + 1) + " consumer withdraws " + (withDraw *(-1)) + " Current Balance "  + bank.BalancingSync(withDraw) + " count " + bank.consumerFrequency(consumerNumber));
			Thread.yield();
		}
		
	}

}
