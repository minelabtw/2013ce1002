package ce1002.a13.s102503017;

public class Bank {

		  int cash = 10000;
		  boolean proceeding = false;
		  static int[] consumerCounter = new int[3];
		  static int[] depositorCounter = new int[3];

		  public synchronized int BalancingSync(int amount) {
			//proceeding the withdraw/deposit thread.
		    if(proceeding == false)
		    {
		    	proceeding = true;
		    	if(cash > 0)
		    	{
		    	    if(amount < 0 && cash + amount > 0)
		    		{
		    	    	cash += amount;
		    		
		    	    	try {
		    	    		Thread.sleep(500);
		    	    	} catch (InterruptedException e) {
		    	    		// TODO Auto-generated catch block
		    	    		e.printStackTrace();
		    	    	}
		    	    	notify();
		    	    	proceeding = false;
		    	    	return cash;
		    		}
		    	    else if(amount > 0)
		    	    {	cash += amount;
		    		
		    	    	try {
		    	    		Thread.sleep(500);
		    	    	} catch (InterruptedException e) {
		    	    		// TODO Auto-generated catch block
		    	    		e.printStackTrace();
		    	    	}
		    	    	notify();
		    	    	proceeding = false;
		    	    	return cash;
		    	    }
			    	else
			    	{
			    		notify();
			    		proceeding = false;
			    		return -1;
			    	}
		    	}//end if(cash > 0)	
		    	else
		    	{
		    		notify();
		    		proceeding = false;
		    		return -1;
		    	}
		    }//end if(proceeding == false)
		    
		    else//when proceeding is true, wait until the thread notify.
		    {
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//processing of the withdraw and deposit thread.
			    if(proceeding == false)
			    {
			    	proceeding = true;
			    	if(cash > 0)
			    	{
			    	    if(amount < 0 && cash + amount > 0)
			    		{
			    	    	cash += amount;
			    		
			    	    	try {
			    	    		Thread.sleep(500);
			    	    	} catch (InterruptedException e) {
			    	    		// TODO Auto-generated catch block
			    	    		e.printStackTrace();
			    	    	}
			    	    	notify();
			    	    	proceeding = false;
			    	    	return cash;
			    		}
			    	    else if(amount > 0)
			    	    {	cash += amount;
			    		
			    	    	try {
			    	    		Thread.sleep(500);
			    	    	} catch (InterruptedException e) {
			    	    		// TODO Auto-generated catch block
			    	    		e.printStackTrace();
			    	    	}
			    	    	notify();
			    	    	proceeding = false;
			    	    	return cash;
			    	    }
				    	else
				    	{
				    		notify();
				    		proceeding = false;
				    		return -1;
				    	}
			    	}//end if(cash > 0)	
			    	else
			    	{
			    		notify();
			    		proceeding = false;
			    		return -1;
			    	}
			    }//end if(proceeding == false)
		  }//end else.
		  return -1;//return -1 when error exists.
		  
		  }//end BalancingSync
		  
		  //calculating the frequency of the specific consumer with synchronization. 
		  public synchronized int consumerFrequency(int consumerNumber)
		  {
			  //when no thread is working then proceed.
			  if(proceeding == false)
			  { 
				switch(consumerNumber)
				{
				case 0:
					consumerCounter[0]++;
					break;
				case 1:
					consumerCounter[1]++;
					break;
				case 2:
					consumerCounter[2]++;
					break;
				}
				return consumerCounter[consumerNumber];
			  	}
			  else
			  {
				  //wait when a thread is proceeding.
				  try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  //then proceed.
				  if(proceeding == false)
				  { 
					switch(consumerNumber)
					{
					case 0:
						consumerCounter[0]++;
						break;
					case 1:
						consumerCounter[1]++;
						break;
					case 2:
						consumerCounter[2]++;
						break;
					}
					return consumerCounter[consumerNumber];
				  	}
			  }//end else.
			  return -1;//return -1 when error occurred.
		  }//end consumerFrequency.

		  
		  public synchronized int depositorFrequency(int depositorNumber)
		  {
			  //same as consumerFrequency.
			  if(proceeding == false)
			  {  
				  	switch(depositorNumber)
				  	{
				  	case 0:
				  		depositorCounter[0]++;
					  	break;
				  	case 1:
				  		depositorCounter[1]++;
				  		break;
				  	case 2:
					  	depositorCounter[2]++;
					  	break;
				  	}
				  	return depositorCounter[depositorNumber];
			  }
			  else
			  {
				  try {
					wait();
				  } catch (InterruptedException e) {
				  	// TODO Auto-generated catch block
				  	e.printStackTrace();
				    }
				  if(proceeding == false)
				  {  
					switch(depositorNumber)
					{
					case 0:
						depositorCounter[0]++;
						break;
					case 1:
						depositorCounter[1]++;
						break;
					case 2:
						depositorCounter[2]++;
						break;
					}
					return depositorCounter[depositorNumber];
				  }
			  }//end if.
			  return -1;
		  }//end depositorFrequency.
		  
}
