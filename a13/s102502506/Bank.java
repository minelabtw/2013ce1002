package ce1002.a13.s102502506;
public class Bank {
private int money = 10000;
private boolean busy = false;

public synchronized int withDrawSync(int amount, int count, int num)
throws InterruptedException {
	if (busy) {
		wait();
	}
	busy = true;
	if (money - amount < 0)  //前不會不會小於0
		wait();
	money = money - amount;
	System.out.println("No." + num + " consumer withdraw " + amount+ " Current Balance " + money + " count " + count);  //這裡輸出才不會有順序問題
	busy = false;
	notify();
	return money;
	}
public synchronized int withINSync(int amount, int count, int num)  //存錢
		throws InterruptedException {
	if (busy){
		wait();
	}
	busy = true;
	money = money + amount;
	System.out.println("No." + num + " depositor deposit " + amount+ " Current Balance " + money + " count " + count);// 這裡輸出才不會有順序問題
	busy = false;
	notify();
	return money;
	}
}