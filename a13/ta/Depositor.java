package ce1002.a13;

import java.util.Random;

public class Depositor implements Runnable {
	private Bank bank; // a member object of bank to withdraw their money
	private int number; // the number of Depositor

	public Depositor(Bank bank, int number) {
		this.bank = bank;
		this.number = number;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Random r = new Random(); // RNG
		// we assume each depositor will deposit money for 10 times
		for (int i = 1; i <= 5; i++) {
			try {
				Thread.sleep(r.nextInt(3000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bank.getinCS();
			// give a certain amount
			int amount = r.nextInt(10) * 100;
			int remain = bank.criticalSecton(amount);
			System.out.println("No." + number + " depositor deposit " + amount
					+ "current balance : " + remain);
			bank.callNextThread();
		}
	}

}
