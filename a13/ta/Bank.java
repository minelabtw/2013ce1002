package ce1002.a13;

public class Bank {

	private int balance = 10000; // bank's total money
	private boolean busying = false; // indicate that the counter is busy or not

	// this method always should be synchronized, otherwise it will cause
	// unknown phenomenon happen

	// get in CS
	public synchronized void getinCS() {
		while (busying) {
			try {
				wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		busying = true;
	}

	// next thread call CS
	public synchronized void callNextThread() {
		notify();
		busying = false;
	}

	// withdraw or deposit
	public synchronized int criticalSecton(int amount) {
		balance += amount;
		return balance;
	}

}