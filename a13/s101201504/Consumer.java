package ce1002.a13.s101201504;

import java.util.Random;

public class Consumer implements Runnable {
	private int id;
	private int count = 0;
	private Bank bank;
	Random random = new Random();

	Consumer(int id, Bank bank) { // input the data
		this.id = id;
		this.bank = bank;
		count = 0;

	}

	public void show() {
		int withdraw = random.nextInt(10) * 100;
		count++;
		System.out.println("NO." + id + " withdraw " + withdraw
				+ " Current Balance " + bank.withDrawSync(id, withdraw) + " count "
				+ count);
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			show();

		}
	}
}
