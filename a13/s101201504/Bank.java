package ce1002.a13.s101201504;

public class Bank {
	private int money = 0;
	private int person = 0;

	Bank() {
		money = 10000;
	}

	public synchronized int withDrawSync(int id, int amount) {
		while (person != 0) {
			try {// stop the thread except this id
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			person = 1;
			Thread.sleep(1000);// simulate the process
		} catch (InterruptedException e) {
			System.out.println("Out2");
		}
		person = 0;
		notify();

		// withdraw
		money = money - amount;
		if (money < 0) {
			money = 0;
		}
		return money;
	}

	public synchronized int depositSync(int id, int amount) { //calculate amount
		while (person != 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		try {
			person = 1;
			Thread.sleep(500);
		} catch (InterruptedException e) {
			System.out.println("Out2");
		}
		person = 0;
		notify();


		money = money + amount;
		if (money < 0) {   //if money < 0
			money = 0;
		}
		return money;
	}
}