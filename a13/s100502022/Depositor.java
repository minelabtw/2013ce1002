package ce1002.a13.s100502022;

import java.util.Random;

public class Depositor implements Runnable{
	private int id;
	private Bank bank;

	public Depositor( Bank b,int i) {
		this.id = i;
		this.bank = b;
	}

	// this is the method template for java thread
	// once you override this method and put it into thread and you're good to
	// go.
	// ** Remember to call thread.start() to execute this method **
	@Override
	public void run() {

		Random r = new Random(); // RNG
		// we assume each Depositor will Deposit money for 5 times
		for (int i = 1; i <= 5; i++) {
			// give a certain amount
			int amount = r.nextInt(10) * 100;
			// go to bank to store his/her money
			int balanceRemain = bank.withDrawSync(amount,0);
			// print out the result
			System.out.println("No." + id + " depositor deposit " + amount
					+ " Current Balance " + balanceRemain + " count " + i);
		}
	}
}
