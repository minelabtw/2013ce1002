package ce1002.a13.s984008030;

public class A13 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
 
		// initialize a bank
		Bank bank = new Bank();
 
		// initialize 3 consumer and wrap with java thread
		Thread thread0 = new Thread(new Consumer(1, bank));
		Thread thread1 = new Thread(new Consumer(2, bank));
		Thread thread2 = new Thread(new Consumer(3, bank));
		
		// initialize 3 depositor and wrap with java thread
		Thread thread3 = new Thread(new Depositor(1, bank));
		Thread thread4 = new Thread(new Depositor(2, bank));
		Thread thread5 = new Thread(new Depositor(3, bank));
 
		// start each thread to perform process
		thread0.start();
		thread1.start();
		thread2.start();
		thread3.start();
		thread4.start();
		thread5.start();
	}
}
