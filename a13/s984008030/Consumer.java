package ce1002.a13.s984008030;

import java.util.Random;

public class Consumer implements Runnable {

	private int id;
	private Bank bank;
	
	public Consumer(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		// Withdraw money from bank
		Random r = new Random();
		for (int i = 0; i < 5; i++) {
			int amount = r.nextInt(2000);
			int oldbalance = bank.getBalance();
			int balance = bank.withDrawSync(amount);
			// If amount is bigger then balance, we cannot withdraw any money!!
			if (oldbalance == balance) {
				amount = 0;
			}
			System.out.println("No." + this.id + " consumer withdraw " + amount + " Current Balance " + balance + " count " + (i + 1));
		}
	}

}
