package ce1002.a13.s984008030;

public class Bank {
	
	private int balance;
	
	public Bank() {
		this.balance = 10000;
	}
	
	public int getBalance() {
		return this.balance;
	}
	
	public synchronized int withDrawSync(int amount) {
		// Withdraw money
		// First use Thread.sleep() to simulate processing
		try {
			Thread.sleep(50);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if(this.balance - amount < 0){ // There is no money
			return 0;
		}
		else { // Money is enough to let the consumer withdraw it
			this.balance -= amount;
			return this.balance;
		}
	}
	
	public synchronized int withDepositSync(int amount) {
		// Deposit money
		// first use Thread.sleep() to simulate processing
		try {
			Thread.sleep(50);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		// deposit
		this.balance += amount;
		return this.balance;
	}
}
