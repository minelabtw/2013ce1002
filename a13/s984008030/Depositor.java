package ce1002.a13.s984008030;

import java.util.Random;

public class Depositor implements Runnable {
	
	private int id;
	private Bank bank;
	
	public Depositor(int id, Bank bank) {
		this.id = id;
		this.bank = bank;
	}

	@Override
	public void run() {
		// deposit money to bank
		Random r = new Random();
		for (int i = 0; i < 5; i++) {
			int amount = r.nextInt(2000);
			int balance = bank.withDepositSync(amount);
			System.out.println("No." + this.id + " depositor deposit " + amount + " Current Balance " + balance + " count " + (i + 1));
		}
	}

}
