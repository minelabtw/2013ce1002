package ce1002.a13.s102502552;

import java.util.Random;

public class Consumer implements Runnable{

	int number;
	Bank bank = new Bank();
	Random ran = new Random();
	
	Consumer(Bank bank,int number)
	{
		this.number = number;
		this.bank = bank;
	}
	
	@Override
	public void run() {
		
		for(int i = 1;i < 11;i++){
			int money = (ran.nextInt(10)+1)*100;//要取的錢
			
			System.out.println("No."+ number +" withdraw "+ money + " Current Balance " +
			Bank.withDrawSync(money)+" count "+i);//輸出
		}
	}

}
