package ce1002.a13.s102502013;

public class Bank {
	private int balance = 10000;
	public synchronized int withDrawSync(int cost){
		balance -= cost;
		return balance;
	}
	
	public synchronized int depositSync(int money){
		balance += money;
		return balance;
	}
}
