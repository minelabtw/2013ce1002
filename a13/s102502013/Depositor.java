package ce1002.a13.s102502013;

public class Depositor implements Runnable{
	private int name = 0;//account's number
	private Bank B;
	Depositor(Bank b, int number){
		this.B = b;
		name = number;
	}

	private int depositorcounter = 1;

	double  money = Math.random()*1000;
	
	public void run(){
		for (int i = 0; i<5 ; i++){
			money = Math.random()*1000;//deposit money

			//wait 1s
			try{
				  Thread.sleep(1000);
				  
			  }catch(Exception e){
				  e.printStackTrace();  
			  }
			
			System.out.println("No." + name + "deposit " + (int)money + " Current Balance " + B.depositSync((int)money) + " count " + depositorcounter);
			depositorcounter++;
		}
	}
}
