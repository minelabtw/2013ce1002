package ce1002.a13.s102502034;

import java.util.Random;

public class Consumer implements Runnable {
	Random ran = new Random();
	private int consumer_no;
	private int withdraw;
	private int[] count = new int[3];
	Bank k;

	public Consumer(Bank bank, int no) {
		consumer_no = no;
		for (int i = 0; i < 3; i++) {
			count[i] = 1;
		}
		k = bank;

	}

	public void run() {

		for (int i = 0; i < 5; i++) {
			withdraw = ran.nextInt(50) * 10;
			k.withDrawSync(withdraw, consumer_no, count[consumer_no - 1]++);
		}
		//set bankmoney and consumer_no

	}
}
