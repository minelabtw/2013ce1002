package ce1002.a13.s102502034;

import java.util.Random;

public class Bank {
	static volatile int bankmoney = 10000;
	static Random ran = new Random();
	static volatile boolean busy = false;

	private synchronized boolean checkAndSetBusy(boolean bCheck, boolean bSet) {
		if (busy == bCheck) {
			busy = bSet;
			return true;
		}
		return false;
	}
  //check and set busy , in case withDrawSync and depositSync run simultaneously
	public synchronized int withDrawSync(int amount, int no, int count) {
		while (!checkAndSetBusy(false, true)) {
			try {
				wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		// check if withDraw runnable

		bankmoney = bankmoney - amount;
		// calculate bankmoney

		if (bankmoney < 0) {
			System.out.println("Not enough money");
			System.exit(1);
		}
		// if bankmoney < 0 , output Not enough money

		try {
			Thread.sleep(ran.nextInt(3) + 1 * 100);
			// Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("No." + no + " withdraw " + amount
				+ " Current Balance " + bankmoney + " count " + count);

		//print result
		busy = false;
		// reset busy to false 
		notify();
		return bankmoney;
	}

	public synchronized int depositSync(int amount, int no, int count) {
		while (!checkAndSetBusy(false, true)) {
			try {
				wait();
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}

		bankmoney = bankmoney + amount;

		try {
			Thread.sleep(ran.nextInt(3) + 1 * 100);
			// Thread.sleep(0);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("No." + no + " deposit " + amount
				+ " Current Balance " + bankmoney + " count " + count);

		busy = false;
		notify();
		return bankmoney;
		//same as above
	}
}