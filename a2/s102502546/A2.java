package ce1002.a2.s102502546;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int[][] a;
		int number = 0;
		System.out.println("Please input a number (1~10):");
		int input1 = cin.nextInt();
		while (input1 < 1 || input1 > 10) {//若不合則重新輸入
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10):");
			input1 = cin.nextInt();
		}
		a = new int[10][10];//設陣列為10,10
		a[0][0] = 1;//直接初始最上面為1

		for (int i = 0; i < input1; i++) {
			for (int j = 0; j <= number; j++) {
				if (i != 0) {//避免進入0-1的狀況
					if (j == 0 || j == number) {
						a[i][j] = 1;

					} else {
						a[i][j] = a[i - 1][j - 1] + a[i - 1][j];
					}
				}

			}
			for (int j = 0; j <= number; j++) {//輸出三角形
				System.out.print(a[i][j] + " ");
			}
			System.out.print("\n");
			number++;
		}

	}
}
