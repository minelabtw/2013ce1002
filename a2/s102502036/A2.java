package ce1002.a2.s10250236;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n;
		int array[][] = new int[11][11];//宣告陣列
		System.out.println("Please input a number (1~10):");
		n = input.nextInt();
		while (n < 1 || n > 10) {
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10):");//依題意輸出
			n = input.nextInt();
		}
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				
				array[i][i] = array[i][0] = 1;
				
				if ( i >= 2&&j!=0&&j!=i) {
					
					array[i][j] = array[i - 1][j - 1] + array[i - 1][j];//進行運算
				}
				System.out.print(array[i][j] + " ");
			}

		if(i<n-1){
			System.out.println();//換行,最後一行不做空行
		}
		}

	}

}
