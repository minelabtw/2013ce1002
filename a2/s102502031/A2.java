package ce1002.a2.s102502031;

import java.util.Scanner;

public class A2 {
	public static void main(String[] args) {
		final int size = 10;
		Scanner getData = new Scanner(System.in);
		int[][] storingPlace = new int[size][size];
		int number;

		// prompt for number
		do {
			System.out.print("Please input a number (1~" + size + "): ");
			number = getData.nextInt();
			if (number < 1 || number > size)
				System.out.println("Out of range!");
		} while (number < 1 || number > size);
		getData.close();

		// initialazation of array
		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				storingPlace[i][j] = 0; // if the value store in array is 0, this value will not be print
			}
		}

		// calculate the value of Pascal's triangle
		storingPlace[0][0] = 1;
		for (int i = 1; i < number; i++) {
			storingPlace[i][0] = 1;
			for (int j = 1; j < i + 1; j++) {
				storingPlace[i][j] = storingPlace[i - 1][j - 1] + storingPlace[i - 1][j];
			}
		}

		// print the result
		for (int i = 0; i < number; i++) {
			for (int j = 0; j < number; j++) {
				if (storingPlace[i][j] != 0)
					System.out.print(storingPlace[i][j] + " ");
				else
					j = number;
			}
			System.out.println();
		}
	}
}