package ce1002.a2.s102502526;
import java.util.Scanner;
public class A2 
{
    private static Scanner scanner;
    
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		scanner = new Scanner(System.in);
		int num;
		System.out.print("Please input a number (1~10): ");
		num = scanner.nextInt();
		while (num < 1 || num > 10)
		{
			System.out.print("out of range!\nPlease input a number (1~10): ");
			num = scanner.nextInt();
		}
		
    int array[][] = new int[num][num]; //宣告陣列
	for(int i=0;i<num;i++)
	{ 
		array[i][i]=1;//設定每行最後一個字 = 1
		array[i][0]=1;//設定每行第一個字 = 1
		for(int j=1;j<i;j++)
			{
			if(i>1) array[i][j]=array[i-1][j-1]+array[i-1][j];	
			}
	}
	
	for(int i=0;i<num;i++)  //輸出
	{ 
		for(int j=0;j<=i;j++)
		{
		System.out.print(array[i][j]);
		System.out.print(" ");
	    }
		System.out.print("\n");
	}
    }
}
