package ce1002.a2.s102502006;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int [][] Basca = new int [20][20];
		int num = 0;
		while(num<1 || num>10){ // 判斷書入
			System.out.println("Please input a number (1~10):");
			num = scanner.nextInt();
			if(num<1 || num>10)System.out.println("Out of range!");
		}
		
		for(int i=0;i<num;i++){
			for(int j=0;j<=i;j++){
				if(j==0 || j==i)Basca[i][j] =1; // 設定1
				else Basca[i][j]=Basca[i-1][j-1] + Basca[i-1][j]; // 加
				System.out.printf("%d ",Basca[i][j]);
			}
			System.out.println();
		}
	}

}
