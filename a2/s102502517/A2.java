package ce1002.a2.s102502517;
import java.util.Scanner; //輸入用
public class A2 {

	public static void main(String[] args) {
		int input = 0;
		Scanner scanner = new Scanner(System.in);
		int[][] array = new int[11][11]; //宣告二維陣列
		array[1][0] = 0; //宣告數值
		array[1][1] = 1; //宣告數值
		
		do
		{
			System.out.println("Please input a number (1~10): ");
			input = scanner.nextInt(); //輸入行數
			if(input<1 || input>10)
				System.out.println("Out of range!");
		}
		while(input<1 || input>10);
		
		for(int i=1; i<=input; i++)
		{
			for(int j=1; j<=i; j++) //輸出
			{
				for(int k=1; k<=i; k++) //取得數值
				{
					if(i>=2)
					array[i][k] = array[i-1][k-1] + array[i-1][k];
				}
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
			
	}

}
