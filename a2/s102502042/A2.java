package ce1002.a2.s102502042;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);		
		System.out.println("Please input a number  (1~10): ");
		int number  = input.nextInt();	//input a number
		while(number>10||number<1)	//out of range
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}
		int [][] pascal = new int [11][11];	
		for(int i=0; i<number; ++i)
		{
			for(int j=0; j<=i; ++j)
			{
				if(j==i||j==0) pascal[j][i] = 1;
				else pascal[j][i] = pascal[j-1][i-1] + pascal[j][i-1];
				System.out.print(pascal[j][i] + " ");	//output
			}
			System.out.println();	//end of line
		}
		input.close();
	}

}
