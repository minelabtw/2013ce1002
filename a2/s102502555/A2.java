package ce1002.a2.s102502555;

import java.util.Scanner;


public class A2 {

	public static void main(String[] args) {
		
		int n = 0;
		int[][] array = new int[10][10];
		Scanner input = new Scanner(System.in);
		
		//輸入數字並檢查有無超出範圍
		
		do{  
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
			if(n < 1 || n > 10){
				System.out.println("Out of range!");
			}
		}while(n < 1 || n > 10);
		
		//列印出巴斯卡三角形
		for(int i = 1 ; i <= n ; i++){
			for(int j = i ; j > 0 ; j--){
				if(j == 1 || j == i){  //如果等於頭或尾值為一
					array[i - 1][j - 1] = 1;
					System.out.print(array[i - 1][j - 1] + " ");
				}else {  //如果不等於頭或尾值為上一列的第K項加第K-1項
					array[i - 1][j - 1] = array[i - 2][j - 1] + array[i - 2][j - 2];
					System.out.print(array[i - 1][j - 1] + " ");
				}
			}
			System.out.println();
		}
		
	}

}
