package ce1002.a2.s102502539;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num;
		int array[][];
		array = new int [10][10];
		Scanner scanner = new Scanner(System.in);
		
	do
	{
		System.out.println("Please input a number (1~10): ");
		num = scanner.nextInt();
		if ( num < 1 || num > 10 )	//判別輸入範圍
			System.out.println("Out of range!");
	} while ( num < 1 || num > 10 );
	
	for ( int j = 0 ; j < num ; j++ )
	{
		for ( int i = 0 ; i <= j ; i++ )
		{
			if ( j > 0 && i > 0 )	//巴斯卡三角型運算
				array[j][i] = array [j-1][i-1] + array[j-1][i];
			else
				array[j][i]++;
			System.out.print(array[j][i] + " ");
		}
		System.out.println("");
	}
	
	}

}
