package ce1002.a2.s101201522;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i,j,N;
		int[][] pascal;
		do{
			System.out.println("Please input a number (1~10): ");
			N = input.nextInt();
			if(N<1 || N>10)
				System.out.println("Out of range!");
		}while(N<1 || N>10);//input number
		pascal = new int[N][N];
		for(i=0;i<N;i++){
			for(j=0;j<=i;j++){
					System.out.print((pascal[i][j] = ((j==0 || j==i) ? 1 : pascal[i-1][j-1]+pascal[i-1][j]))+" ");
			}
			System.out.println();
		}//output pascal
	}

}
