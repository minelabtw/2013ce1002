package ce1002.a2.s102502013;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		final int w = 10, l = 10;//declare two variable for array's wide and length
		int p[][] = new int[w][l];//declare an array
		Scanner input = new Scanner(System.in);
		int n = 0;//declare a variable for the layer about Pascal's Triangle
		System.out.println("Please input a number (1~10): ");
		n = input.nextInt();//input a integer
		while(n<0||n>10)//when the integer is not in the range, output "Out of range!"
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
		}
		p[0][0]=1;//set array's value
		p[1][0]=1;//set array's value
		p[1][1]=1;//set array's value
		for(int j=2;j<n;j++)
		{
			p[j][0]=1;//set array's value
			for(int i=1;i<=j;i++)
			{
				p[j][i]=p[j-1][i-1] + p[j-1][i];//calculate the value for Pascal's Triangle and store in array
			}
			p[j][j]=1;//set array's value
		}
		for(int j=0;j<n;j++)//output array
		{
			for(int i=0;i<j+1;i++)
			{
				if(p[j][i]==0)
				{
				break;	
				}
				System.out.print(p[j][i] + " ");
			}
			System.out.println("");
		}
	}
}
