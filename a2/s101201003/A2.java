package ce1002.a2.s101201003;

import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner number=new Scanner(System.in);//宣告Scanner類別的物件
		int n=0;				
		do{
			System.out.print("Please input a number (1~10): ");
			n=number.nextInt();
			if(n<1||n>10)
				System.out.println("Out of range!");
		}while(n<1||n>10);//問欲顯示的階層數,如果輸入範圍錯誤就重新輸入
		
		int[][] pascal;//宣告一個二微陣列
		pascal=new int[10][10];//大小是10x10
				
		for(int i=0;i<n;i++){
			for(int j=0;j<=i;j++){
				if(j==0){
					pascal[i][j]=1;
				}
				else if(i==j){
					pascal[i][j]=1;
				}
				else{
					pascal[i][j]=pascal[i-1][j-1]+pascal[i-1][j];
				}
			}
		}//帕斯卡三角形，頭尾一定是1，其他是由上一列相鄰的兩個數字相加
		for(int i=0;i<n;i++){
			for(int j=0;j<=i;j++){
				System.out.print(pascal[i][j]+" ");				
			}
			System.out.print("\n");
		}//輸出帕斯卡三角形	
		number.close();
	}

}
