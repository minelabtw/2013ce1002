package ce1002.a2.s102502528;
import java.util.Scanner;

public class A2 {
	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
		int n = 0;
		do
		{
			System.out.println("Please input a number (1~10): ");
			n = scanner.nextInt();
			if(n<0 || n>10)
				System.out.println("Out of range!");
		}while(n<0||n>10);
		int[][] arr = new int[n][n];  
        for(int i = 0; i < n ;i++){  
        	arr[i][0] = 1;  
        	arr[i][i] = 1;  
            if(i > 1){  
                for(int j = 1; j < i ;j++)
                	arr[i][j] = arr[i-1][j-1] + arr[i-1][j];  
            }  
        }  
		for(int k=0; k<n; k++)
		{
			for(int m=0; m<=k; m++)
			{
				System.out.print(arr[k][m] + " ");
			}
			System.out.println();
		}
	}
}
