package ce1002.a1.s995002046;
import java.util.Scanner;
class A2{
	
	public static void main(String[] args) { 
		int num=0;
		Scanner scanner;
		System.out.print("Please input a number (1~10):\n");
		while(5>3){//重複迴圈直到輸入範圍的值
			scanner = new Scanner(System.in);
			num=scanner.nextInt();
			if(1 <= num && num <= 10){
				
				break;
			}
			else
				System.out.print("Out of range !\nPlease input again(1~10):\n");
			
		}
		for(int i=0; i<num; i++){//印出巴斯卡三角
			for(int j=0; j<=i; j++){
				System.out.print(cal(i,j)+" ");
			}
			System.out.print("\n");
		}
		scanner.close();
	}

	private static int cal(int n, int r) {//計算巴斯卡三角形
		// TODO Auto-generated method stub
		int p = 1;
		for(int i=1;i<=r;i++){
			p = p * (n-i+1) / i;
		}
		return p;
	}
}