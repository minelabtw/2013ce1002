package ce1002.a2.s102502554;

import java.util.Scanner;//scanner is in the java.util package

public class A2 {
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);//creat a scanner object
		System.out.println("Please input a number (1~10): ");//prompt the user to enter a number
		int [][] arrayN;//宣告陣列
		int number = input.nextInt();
		while (number < 1 || number >10){
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}//if the number out of range , the user can enter again
		arrayN = new int [number][number];//設定陣列大小
		int y , x = 1;
		for(int i = 0; i < number; i++ )
	    {
			System.out.print( x );
			System.out.print( " " );
	        for(int j = 1; j <= i; j++ )
	        {
	            y = x;
	            x = y * ( i - j + 1 ) / j;
	            arrayN[i][j] = x;
	            System.out.print( arrayN[i][j] );
	            System.out.print( " " );
	        }
	        System.out.print("\n");
	    }//計算巴斯卡數字塔，並將之存於陣列中
	}

}
