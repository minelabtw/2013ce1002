package ce1002.a2.s102502022;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (1~10):");
		int a=0;
		a=input.nextInt();
		while(a<1 || a>10)             //迴圈
		{
			System.out.println("Out of range!");
			System.out.print("Please input a number (1~10):");
			a=input.nextInt();
		}
		int[][] x=new int [a+1][a+1];     //宣告二維陣列
		for(int m=1;m<=a;m++)
		{
			for(int n=1;n<=a;n++)
			{
				if(n==1 || m==n)         //對角線和第一航都是1
					
						x[m][n]=1;        
					
				else
					x[m][n]=x[m-1][n-1]+x[m-1][n];    //其餘由上面兩者相加
			}
		}
		for(int i=1;i<=a;i++)
		{
			for(int j=1;j<=a;j++)
			{
				if(x[i][j]!=0)
				      System.out.print(x[i][j]+" ");
			}
			System.out.println("");
		}
		
	}

}
