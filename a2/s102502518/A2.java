package ce1002.a2.s102502518;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		A2 a = new A2();
		Scanner scanner = new Scanner(System.in);	
		System.out.println("Please input a number (1~10): ");
		int num = scanner.nextInt();
		while (num > 10 || num < 1) {//判斷為範圍之內
			System.out.println("Out of range!\n" + "Please input a number (1~10): ");
			num = scanner.nextInt();
		}
		a.pascal(num);
	}
	
	public void pascal(int num) {
		int[][] p = new int[num][num];
		for(int i = 0; i < num ;i++) {
			p[i][0] = 1;//最左邊為一
			p[i][i] = 1;//最右邊為一
			if(i > 1) {
				for(int j = 1; j < i ;j++) {
					p[i][j] = p[i-1][j-1] + p[i-1][j];//中間的則為左上加右上
				}
			}
		}
		for(int i = 0; i < num ;i++) {	
			for(int j = 0; j<= i ;j++) {
				System.out.print(p[i][j] + " ");//由上往下由左至右
			}
			System.out.println();
		}
	}
}
