package ce1002.a2.s102502048;
import java.util.Scanner;
public class A2 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int[] pascal = new int[100];//宣告
	    int i=2,z=0,a=4,II=4,III=2,n=3,I=1;//宣告
	    
	    for(int _pascal=0 ; _pascal<100; _pascal++)//初始化
	    	pascal[_pascal]=1;
	    
	    do
		{
			System.out.println("Please input a number (1~10): ");//顯示
			z = input.nextInt();//輸入		
			if(z <1 || z >10)
				System.out.println("Out of range!");//顯示
		}while(z <1 || z >10);

	    for(int q=0,O=1; q<2; q++, O++)//前兩列
	    {	        
	        for(int o=0; o<O; o++)
	        	System.out.print("1 ");
	        System.out.print("\n");
	    }
	    for(int Z=3; Z<=z; Z++,n++)//後幾列
	    {
	        i=i+1;
	        for(int j=1; j<=i; j++,a++)//計算顯示
	        {
	            if(a==II)
	            {
	                pascal[a]=1;
	                if(I%2==1)
	                {
	                    II=II+III;
	                    III++;
	                }
	                else	                
	                    II=II+1;	                
	                I++;
	            }
	            else	            
	            	pascal[a]=pascal[a-n]+pascal[a-n+1];	            
	            System.out.print(pascal[a]+" ");//顯示
	        }
	        System.out.print("\n");//換行
	    }
	}
}
