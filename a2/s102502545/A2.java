package ce1002.a2.s102502545;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int number = 0;
		do {/*限定變數的範圍*/
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
			if (number < 1 || number > 10)
				System.out.println("Out of range!");
		} while (number < 1 || number > 10);

		int array[ ][ ] = new int[number][number];/*宣告一二維陣列*/
		for (int i = 0; i < number; i++) /*利用迴圈輸入二維振烈的值*/
		{
			array[i][0] = 1;
			array[i][i] = 1;
			if (i > 1) 
			{
				for (int j = 1; j < i; j++) 
				{
					array[i][j] = array[i - 1][j - 1] + array[i - 1][j];
				}
			}

		}
		for (int i = 0; i < number; i++) /*將二維陣列輸出*/
		{			
			for (int j = 0; j <= i; j++) 
			{
				System.out.print(array[i][j]+" ");
			}
			System.out.println();
		}

	}

}
