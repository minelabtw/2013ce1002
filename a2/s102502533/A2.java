package ce1002.a2.s102502533;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);//limit the range of input
		System.out.println("Please input a number (1~10): ");
		int number = input.nextInt();
		while (number < 1 || number > 10) {
			System.out.print("Out of range!");
			System.out.println("\nPlease input again (1~10): ");
			number = input.nextInt();
		}
		int[][] value = new int[number][number];//make an array to conculate triangle
		for (int i = 0; i < number; i++) {
			value[i][0] = 1;
			value[i][i] = 1;
			if (i > 1) {
				for (int j = 1; j < i; j++) {
					value[i][j] = value[i - 1][j - 1] + value[i - 1][j];
				}
			}
		}
		for (int i = 0; i < number; i++) {//output the array
			for (int j = 0; j < number; j++) {
				if (value[i][j] == 0)
					System.out.print(" ");
				else
					System.out.print(value[i][j] + " ");
			}
			System.out.println();
		}
	}
}
