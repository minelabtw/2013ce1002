package ce1002.a2.s102502538;

import java.util.Scanner;

public class A2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner  = new Scanner(System.in);
		int number;
			
		System.out.print("Please input a number (1~10): ");
		number=scanner.nextInt();
		while(number<1||number>10){//檢查是否在範圍內
			System.out.println("Out of range!");
			System.out.print("Please input a number (1~10): ");
			number=scanner.nextInt();
		}
		int[][] arr = new int[10][10];
		
		for(int i=0;i<number;i++){//定義此二微陣列
			for(int j=0;j<=i;j++){
				if(j==0){
					arr[i][j]=1;
				}
				else if(j==i){
					arr[i][j]=1;
				}
				else{
				arr[i][j]=arr[i-1][j-1] + arr[i-1][j];	
				}
          
			}
		}
		for(int i=0;i<number;i++){//輸出此二微陣列
			for(int j=0;j<=i;j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}

}
