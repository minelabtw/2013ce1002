package ce1002.a2.s102502536;

import java.util.Scanner;

public class A2 {
	
	private static Scanner input;

	public static void main(String[] args) {
		
		//宣告變數
		int num;
		
		input = new Scanner(System.in);
		
		//輸出字串
		System.out.println("Please input a number (1~10):");
		
		//設定範圍
		do
		{
			num = input.nextInt();
			
			if (num < 1 || num > 10)
				System.out.println("Out of range!\nPlease input a number (1~10):");
			
		} while (num < 1 || num > 10);
		
		//宣告陣列
		int pascal[][] = new int [num][num];
		
		//把數存進陣列
		for (int i = 0; i < num; i++)
		{
			pascal[i][0] = 1;
			pascal[i][i] = 1;
			
			if (i > 1)
			{  
                for (int j = 1; j < i; j++)
                {  
                    pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];  
                }
            }  
			
		}
		
	    //輸出陣列
		for (int i = 0; i < num; i++)
	    {  
	        for(int j = 0; j<= i; j++)
	        {  
	            System.out.print(pascal[i][j] + " ");  
	        }
	        
	        System.out.println();
	    }
	    
	    input.close();
	} 

}
