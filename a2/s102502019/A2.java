package ce1002.a2.s102502019;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    System.out.print("Please input a number (1~10): ");        //輸出
    Scanner input = new Scanner(System.in);
    int number = input.nextInt();                              //宣告變數並輸入
    while(number<1 || number>10)                               //輸入條件
    {
    	System.out.print("Out of range !\n"+"Please input a number (1~10): ");
    	number = input.nextInt();
    }
    int [][]p = new int [10][10];                              //宣告陣列
    for(int k=0; k<number;k++)                                 //巴斯卡三角形
    {
    	p[0][k]=1;
    	p[k][k]=1;
    }
    for(int y=1; y<number; y++)
    {
    	for(int x=1; x<number-1; x++)
    	{
    		p[x][y]=p[x-1][y-1]+p[x][y-1];
    	}
    }
    int r=1;
    for(int y=0; y<number; y++)                               //輸出巴斯卡三角形
    	
    {  
    	for(int x=0; x<r; x++)
    	{
    		System.out.print(p[x][y]+" ");
    	}
    	r++;
    	System.out.print("\n");
    }
	}

}
