package ce1002.a2.s102502543;

import java.util.Scanner;
public class A2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		scanner = new Scanner(System.in); 
		int a[][]; //宣告陣列
		a = new int [10][10];  //陣列大小
		int n;
		do {
			System.out.println("Please input a number (1~10): "); //輸出
			n = scanner.nextInt(); //輸入
			if (n < 1 || n > 10)
				System.out.println("Out of range!");
		} while (n < 1 || n > 10);
		for (int x=0;x<n;x++) //輸出及運算
		{
			for (int y=0;y<=x;y++)
			{
				if( y==0 || x==y)
					a[x][y]=1;
				else
					a[x][y]=a[x-1][y-1]+a[x-1][y];
				System.out.print(a[x][y]+" ");
			}
			System.out.print("\n");
		}
	}
}
