package ce1002.a2.s102502556;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //宣告一個Scanner。
		System.out.println("Please input a number (1~10): ");
		int num = input.nextInt();//讓使用者輸入數字。
		while ( num < 1 || num > 10 ) //檢查輸入是否合乎標準，否則要求其重新輸入。
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			num = input.nextInt();
		}
		int[][] array = new int[10][10]; //宣告一個名為array的int二維陣列。
		int counter = 0;
		for ( int i = 0 ; i < num ; i++ )
		{
			for ( int j = 0 ; j <= counter ; j++ )
			{
				if ( j == 0 || j == counter )//如果是那一橫列的頭或尾，則預設為1。
				{
					array[i][j] = 1;
				}
				else//計算那一橫列每一個非頭或尾的元素的值。
				{
					array[i][j] = array[i-1][j-1] + array[i-1][j];			
				}
			}
			for ( int k = 0 ; k <= counter ; k++ )//輸出當前那一橫列
			{
				System.out.print(array[i][k]+" ");
			}
			System.out.println("");
			counter++;
		}
		input.close();//關閉Scanner
	}

}
