package ce1002.a2.s100203020;

import java.util.Scanner;

public class A2 {
	
	private static Scanner scanner;
	
	public static void main(String[] args) {

		
		int num;
		scanner = new Scanner(System.in);
		System.out.print("Please input a number (1~10): \n");
		
		do{
			num = scanner.nextInt();
			if (num < 1 || num > 10)
				System.out.println("Out of range!\nPlease input a number (1~10): ");
		} while (num < 1 || num > 10);

		//declare array
		int[][] arrayII;
		
		arrayII = new int [num][num];
		
		//put 1 in edges  
		for(int i=0;i<num;i++){
			arrayII[i][i]=1;
			arrayII[i][0]=1;
		}
		
		for(int i=0;i<num;i++)
			for(int j=1;j<i;j++)
				arrayII[i][j]=arrayII[i-1][j-1]+arrayII[i-1][j];
		
		// print answer
		for (int i=0; i<num ;i++){
			for(int j=0; j<i+1;j++){
				System.out.print(arrayII[i][j]+" ");
			}
			System.out.println();
		}
		
	}

}
