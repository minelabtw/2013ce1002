package ce1002.a2.s102502557;

import java.util.Scanner;
public class A2 {
	public static void main(String[] args)
	{
    int N=0;
    int[][] array;//第一列人為設定爲(0,1,0)
    array=new int [11][13];//陣列的最左邊跟最右邊其實都有０ 為了讓每一行的１都能夠用巴斯卡三角形的原理
    array[0][0]=0;array[0][1]=1;array[0][2]=0;//初始化要在new之後
    
    
    Scanner input = new Scanner(System.in);//scanner初始化 scanner是“import java.util.Scanner
    System.out.println("Please input a number (1~10): ");
    N = input.nextInt();
    while(N>10||N<1)
    {
    	System.out.println("Out of range!");
    	System.out.println("Please input a number (1~10): ");
    	N = input.nextInt();
    }
    
    for(int width = 1 ; width <= N ; width ++ )
    {
    	for(int height = 1 ; height < width + 1 ; height++ )
    	{
    		array[width][height]=array[width-1][height-1]+array[width-1][height];
    		System.out.print(array[width][height]);
    		System.out.print(" ");
    	}
    	
    	System.out.println("");
    }
    
	}
}
