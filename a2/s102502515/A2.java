package ce1002.a2.s102502515;
import java.util.Scanner;//匯入"輸入"

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//宣告input為匯入
		int num ;
		int form[][] = new int [10][10];
		
		do{//用do while確認輸入值在範圍內
		System.out.println("Please input a number (1~10): ");
		num = input.nextInt();
		
		if (num > 10 || num < 1)//不在範圍內則，則輸出
		{
			System.out.println("Out of range!");
		}
		}
		while( num > 10 || num < 1);
	
		for (int i=1 ; i<=num ; i++)
		{
			for (int j=1 ; j<=i ; j++)
			{
				System.out.print(form[i][j] = multi(i,j));//call multi，以及印出該值
				System.out.print(" ");
			}
			System.out.println("");//換行
		}
		input.close();//把input關掉
	}


public static int multi(int i, int j)//算第i行的第j個數字為組合數C_{j-1}^{i-1}，取自維基百科
	{
		int multiI = 1, multiJ = 1, multiK = 1, k = i-j;
		
		while ( i > 1)//算分子，i-1的階層
		{
			multiI = multiI * (i-1);
			i--;
		}
		while ( j > 1)//算分母，j-1的階層
		{
			multiJ = multiJ * (j-1);
			j--;
		}
		
		while (k > 1)//算分母，i-j的階層
		{
			multiK = multiK * k;
			k--;
		}
		return multiI/(multiJ*multiK) ;//回傳算出值
	}
}