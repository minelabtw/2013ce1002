package ce1002.a2.s102502504;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) 
	{
		int num; //使用者輸入的值
		
		do
		{
		System.out.println("Please input a number (1~10): "); //輸出Please input a number (1~10): 
		Scanner input = new Scanner(System.in); //讀取使用者輸入的值
		num = input.nextInt(); //把讀取的值丟給num
		if(num<1 || num>10) //超出範圍則顯示Out of range!
		{
			System.out.println("Out of range!");
		}
		} while(num<1 || num>10); //輸錯就一直重複上述迴圈
		
		int array[][]= new int[11][11];
		
		int k=1; //一列幾個數字
		for(int i=0;i<num;i++) //每列的第一個數字都是1
		{
			array[i][0]=1;
		}
		for(int i=1;i<=num;i++) //每列最後一個數字都是1
		{
			array[i][i]=1;
		}
		for(int i=1;i<num;i++) //列
		{
			for(int j=1;j<k;j++) //行
			{
				array[i][j]=array[i-1][j-1]+array[i-1][j]; //某個數字=他的上面+他的左上
			}
			k++;
		}
		
		k=1; //把k歸回1
		for(int i=0;i<num;i++)
		{
			for(int j=0;j<k;j++)
			{
				System.out.print(array[i][j]+" "); //輸出陣列
			}
			k++;
			System.out.println(); //換列
		}
	}
}
