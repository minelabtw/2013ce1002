package ce1002.a2.s102502028;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		int n = 0 ;
		
		System.out.println("Please input a number (1~10): ") ;  //要求輸入
		Scanner cin = new Scanner(System.in) ;
		n = cin.nextInt() ;
		
		while (n < 1 || n > 10)          //判斷輸入是否符合
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ") ;
			n = cin.nextInt() ;
		}
		
		int x = 1 ;        //宣告
		int y = 2 ;
		int array[][] = {} ;
		array = new int [n][n] ;
		
		for (int a = 0 ; a < n ; a++) //製造巴斯卡三角形
		{
			array[a][0] = 1 ;
			array[a][x-1] = 1 ;
			
			if (a > 1)
			{
				for (int b = 1 ; b < y ; b++)
			{		
					array[a][b] = array[a-1][b-1] + array[a-1][b] ;					
			}
				y++ ;
			}
			x++ ;
		}
		
		x = 1 ;

		for (int a = 0 ; a < n ; a++)  //印出巴斯卡三角
		{
			for (int b = 0 ; b < x ; b++)
			{
				System.out.print(array[a][b] + " ") ;		
			}
			System.out.println();
			x++ ;
		}
		
	}

}
