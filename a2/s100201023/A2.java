package ce1002.a2.s100201023;

import java.util.Scanner;

public class A2
{
	public static void main(String[] args)
	{
		// declare variable
		Scanner input = new Scanner(System.in);
		int number;
		
		while(true)
		{
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
			
			if(number > 0 && number < 11) // determine if input is legal
				break;
			
			System.out.println("Out of range!");
		}
		
		int[][] basca = new int[number][number];
		
		for(int i = 0 ; i < number ; ++i)
		{
			basca[i][0] = basca[i][i] = 1;
			if(i > 1)
				for(int j = 1 ; j < i ; ++j)
					basca[i][j]= basca[i - 1][j - 1] + basca[i - 1][j]; 
		}
		
		for(int i = 0 ; i < number ; ++i)
		{
			for(int j = 0 ; j <= i ; ++j)
				System.out.print(basca[i][j]+ " " );
			
			System.out.println();
		}
	}
	
}

