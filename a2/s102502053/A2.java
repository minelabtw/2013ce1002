package ce1002.a2.s102502053;

import java.util.Scanner;   

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		int number; // call variables
		
		do //data input and data validation
		{
			System.out.print("Please input a number (1~10): \n");
			number = input.nextInt();
			if(number<1 || number>10)
			{
				System.out.print("Out of range!\n");
			}
		}while(number<1 || number>10);
		
		int [][] arr = new int[number][number]; //create array
		
		for(int a = 0; a<number; a++) //input all starting 1 for each column
		{
			arr[a][a] = 1;
		}
		
		int temp = 0; //call variables for calculation
		int add = 0;
		int temp1 = 1;
		
		for(int c = temp; c<number; c++) //calculate the Pascal's triangle
		{
			for(int r = temp1; r<number; r++)
			{
				if(r != 0)
				{
					arr[c][r] = arr[c][r-1] + add;
				}
			}
			temp++;
			add++;
			temp1++;
		} 
		
		for(int row = 0; row<number; row++) //print the triangle
		{
			for(int column = 0; column<number; column++)
			{
				if(arr[column][row] == 0) // print " " if it is a 0
				{
					System.out.print(" "); 
				}else
				System.out.print(arr[column][row]);
				System.out.print(" ");
			}
			System.out.print("\n");
		}
		
		
	}

}
