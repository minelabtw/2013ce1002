package ce1002.a2.s101602016;
import java.util.Scanner;
public class A2 {
	public static void main(String[] args){
		int n=0,s=2;//巴斯卡三角形的層數，陣列的編號
		int[] x;//存放巴斯卡三角形各項值的陣列
		x=new int[55];//因層數不大於10，所以數字數量不超過55
		x[0]=1;//不考慮第一層，將第二層的第一個數字當成第一項，並設定為1
		x[1]=1;//第二項為1
		Scanner scanner=new Scanner(System.in);
		do{//請使用者輸入層數，如果超出範圍則重新輸入
		System.out.println("Please input a number (1~10): ");
		n=scanner.nextInt();
		if(n<1||10<n)
			System.out.println("Out of range!");
		}while(n<1||10<n);
		//計算巴斯卡三角形的各項值
		for(int i=2;i<n;i++){//由第三層開始計算
			for(int j=0;j<=i;j++){//第n層有n個數字，所以須計算n次
				if(j==0||j==i)
					x[s]=1;//該層的第一個數與最後一個數為1
				else
					x[s]=x[s-i]+x[s-i-1];//該數字的項數減該層數加一 與 該數字的項數減該層數，將這兩項的數字相加，則能得到該數字的值
				s++;//計算下一項
			}
		}
		s=2;//重新將項數設為2
		if(n==1)//當層數為1時，只輸出第一層
			System.out.println("1");
		else//當層數為2以上時，先輸出前兩層
			System.out.println("1\n1 1 ");
		for(int k=3;k<=n;k++){//輸出第三層以後的數
			for(int l=0;l<k;l++){
				System.out.print(x[s]+" ");
				s++;
			}
			System.out.println();//換行
		}		
	}
}
