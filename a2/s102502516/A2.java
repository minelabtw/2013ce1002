package ce1002.a2.s102502516;
import java.util.Scanner; 

public class A2 {

	public static void main(String[] args) {
		int pascal[][] = new int [11][11];
		Scanner scanner = new Scanner(System.in); 
		
		int userInput; 
		do {
			System.out.println("Please input a number (1~10): ");
			userInput = scanner.nextInt();
			if( userInput < 1 || userInput > 10 )
				System.out.println("Out of range!");	//不符合規定的輸入
		} while ( userInput < 1 || userInput > 10 );
		pascal[1][1]=1;
		for (int i = 2; i <= userInput; i++) {
			for (int j = 1; j <= i; j++) {
				pascal[i][j] = pascal[i-1][j] + pascal[i-1][j-1];
			}
		}  //做出三角形
		for (int i = 1; i <= userInput; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(pascal[i][j] + " ");
			}
			System.out.println();
		}  //印出三角形
	}
}