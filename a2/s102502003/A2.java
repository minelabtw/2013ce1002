package ce1002.a2.s102502003;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		int layer=0;
		
		do  //範圍判定
		{
			System.out.println("Please input a number (1~10): ");
			layer=scanner.nextInt();
			if(layer>10||layer<=0)
				System.out.println("Out of range!");
		}while(layer>10||layer<=0);
		
		int[][] pascal = new int[layer][layer];  //宣告二維陣列
		
		for(int i=0; i<layer; i++)  //巴斯卡三角形的數字存進陣列
		{
			pascal[i][0]=1;  //每列第一個數必為1
			pascal[i][i]=1;  //每列最後一個數字必為1
			if(i>1)
			{
				for(int j=1; j<=i; j++)
				{
					pascal[i][j]=pascal[i-1][j]+pascal[i-1][j-1];				
				}
			}
		}

		for(int i = 0; i < layer ;i++)  //顯示結果
		{  
            for(int j = 0; j<= i ;j++)
            {  
                System.out.print(pascal[i][j]+" ");  
            }  
            System.out.println();  
        }
		
		

	}

}
