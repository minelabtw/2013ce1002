package ce1002.a2.s102502534;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 0;
		Scanner input = new Scanner(System.in);
		// 檢查輸入是否在0~10之間
		while (n < 1 || n > 10) {
			System.out.println("Please input a number (1~10):");
			n = input.nextInt();
			if (n < 1 || n > 10) {
				System.out.println("Out of range!");
			}
		}
		// 宣告一個陣列存放巴斯卡三角形
		int[][] array = new int[n][n];
		// 將數值放入陣列中
		for (int i = 1; i < n; i++) {
			array[0][0] = 1;
			array[i][0] = 1;
			array[i][i] = 1;
			for (int k = 1; k < i; k++) {
				array[i][k] = array[i - 1][k - 1] + array[i - 1][k];
			}
		}
		// 將巴斯卡三角形印出來
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < i + 1; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println("");
		}
	}
}