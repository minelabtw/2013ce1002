package ce1002.a2.s102502535;

import java.util.Scanner ;

public class A2 {

	private static Scanner input;

	public static void main(String[] args) {
		
		int num = 0 ;
		input = new Scanner (System.in) ;
		
		System.out.println("Please input a number (1~10): ") ;
		num = input.nextInt() ;
		while( num < 1 || num > 10 )
		{
			System.out.println("Out of range!") ;
			System.out.println("Please input a number (1~10): ") ;
			num = input.nextInt() ;
		}  //使輸入數字，並判斷其是否在範圍內，若無重新輸入直到符合為止。
		
		int arr[][] = new int [11][11] ;  //宣告一個11*11的二維陣列。
		
		for ( int i = 0 ; i < 11 ; i ++ )
		{
			for ( int j = 0 ; j < 11 ; j ++ )
			{
				arr[i][j] = 0 ;
			}
		}  //將陣列中的所有元素都初始為零。
		
		for ( int i = 0 ; i <= num ; i ++ )
		{
		    for ( int j = 0 ; j <= i ; j ++ )
			{
				if ( i == j || j == 0 )
					arr[i][j] = 1 ;
				else
				    arr[i][j] = arr[i-1][j] + arr[i-1][j-1] ;
			}
		}  //進行巴斯卡三角形之運算，並分別放入陣列中。 
		
		for ( int i = 0 ; i <= num-1 ; i ++ )
		{
			for ( int j = 0 ; j <= i ; j ++ ) 
			{
				System.out.print(arr[i][j] + " " ) ;
			}
		    System.out.println() ;
		}  //輸出巴斯卡三角形。
        
		input.close() ;
	}

}
