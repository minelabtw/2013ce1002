package ce1002.a2.s102502527;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number,k,l;
		
		do{
			System.out.println("Please input a number (1~10): ");//輸出字彙及輸入數值,並判斷是否在範圍內
			number = input.nextInt();
			if ( number < 1 || number > 10 )
				System.out.println("Out of range!");
		}while ( number < 1 || number > 10 );
		
		int array[][] = new int [number][number];
		
		if ( number == 1 )//輸入1時只顯示1
		{
			array[0][0] = 1;
		}
		
		if ( number == 2 )//輸入2時將最上層跟邊邊輸出1
		{
			for ( int m = 0 ; m < number ; m++ )
			{
				array[m][0] = 1;
				array[m][m] = 1;
			}
		}
		
		else if ( number > 2 )
		{
			for ( int a = 0 ; a < number ; a++ )//輸入超過2時將最上層及邊邊帶入1
			{
				array[a][0] = 1;
				array[a][a] = 1;
			}
			
			for( int i = 2 ; i < number ; i++ )//將左上部加上部為值
			{
				for(int j = 1 ; j < i ; j++ )
				{
					array[i][j]=array[i-1][j]+array[i-1][j-1];
				}
			}
		}
		
		
		for ( int c = 0 ; c < number ; c++ )//依序輸出
		{
			for ( int d = 0 ; d <= c ; d++ )
			{
				System.out.print(array[c][d]+" ");
			}
			System.out.println();
		}

	}

}
