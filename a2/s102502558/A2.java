package ce1002.a2.s102502558;
import java.util.Scanner;
public class A2 {
	public static void main(String[] args) {
		
		// 處理重複輸入
		int n;
		Scanner scanner = new Scanner(System.in);
		while (true)
		{
			
			System.out.println("Please input a number (1~10): ");
			n = scanner.nextInt();
			if (n >= 1 && n <= 10)
				break;
			System.out.println("Out of range!");
		}
		
		int[][] arr = new int[11][13]; // 記錄已經算過的數值
		arr[0][1] = 1;
		for (int i=1;i<=n;i++)
		{
			for (int j=1;j<i+1;j++)
				System.out.print((arr[i][j] = arr[i-1][j-1] + arr[i-1][j]) + " ");
			System.out.print("\n");
		}
		scanner.close();
	}
}
