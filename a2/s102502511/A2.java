package ce1002.a2.s102502511;

import java.util.Scanner;

public class A2 
{
	public static void main(String[] args)
	{
		int number;
		A2 triangle = new A2(); //較函式時，前面要用Class的名字 
		Scanner input = new Scanner (System.in); //存入輸入的數字
		System.out.print("Please input a number (1~10): \n");
		do
		{
			number = input.nextInt(); //將number變成輸入的數字
			if (number < 1 || number > 10)
			{
				System.out.print("Out of range!\n1Please input a number (1~10): \n");
			}
		}while (number < 1 || number > 10);
		triangle.PT(number); //叫出所使用的函式
		
		input.close(); //最後用完記得關閉
		// TODO Auto-generated method stub
	}
	public void PT(int size) //講解函式內容
	{
		int [][] Aarray = new int [size][size]; //創造一個二為陣列，陣列名字在[][]之後
		for(int i = 0; i < size; i++) 
		{
			Aarray[i][0] = 1; //由巴斯卡三角形規律可得每列的首相和末項都是1
			Aarray[i][i] = 1;
			if (i > 1) //當列數大於一時，開始有第三個數
		    {
			   for(int j = 1; j < i; j++)
			   {
				Aarray[i][j] = Aarray[i-1][j-1] + Aarray[i-1][j]; //除了首末項數以外，其餘的數皆等於它前一列的數和其前一列的前一個數相加
			   }
		    }	
		}
		for(int i = 0; i < size; i++) //將陣列依序輸出
		{
			for(int j = 0; j <= i ; j++)
			{
				System.out.print(Aarray[i][j]);
				System.out.print(" ");
			}
			System.out.print("\n");
		}
	}
}
