package ce1002.a2.s102502014;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a;
		int Pascal[][]= new int [10][10];
		Scanner scanner = new Scanner(System.in); // new一個scanner物件
		System.out.println("Please input a number (1~10): ");
		a = scanner.nextInt(); // scanner物件使用nextInt()方法
		while (a > 10 || a < 1) {
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			a = scanner.nextInt();
		}
		for (int i = 0; i < a; i++) {
			Pascal[i][0] = 1;                     // 第一點:每列最左邊是1
			for (int j = 1; j <= i; j++) {
				Pascal[i][j] = Pascal[i - 1][j] + Pascal[i - 1][j - 1]; // 第二點
			}
		}

		for (int i = 0; i < a; i++) {
			for (int j = 0; j < i + 1; j++) { // j<i+1判斷，不輸出陣列中的0
				System.out.print(Pascal[i][j] + " ");
			}
			System.out.println("");
		}
		scanner.close();
	}
}
