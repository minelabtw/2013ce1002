package ce1002.a2.s102502001;

import java.util.Scanner;

public class A2 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner= new Scanner(System.in);
		
		
		System.out.println("Please input a number (1~10): ");
		int num = scanner.nextInt();     //輸入變數

		do{
		if(num < 1 || num >10){
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			num = scanner.nextInt();
		}
		}while(num < 1 || num >10);     //判斷範圍
		
		
		int[][] value = new int[num][num];  
        for(int i = 0; i < num ;i++)
        {  
            value[i][0] = 1;           //第0列皆為1
            value[i][i] = 1;           //第i列皆為1
            if(i > 1)
            {  
                for(int j = 1; j < i ;j++)
                {  
                    value[i][j] = value[i-1][j-1] + value[i-1][j]; //[i][j]值為前一行前一列加上前一行同一列之值 
                }  
            }  
        }    
        for(int i=0;i<num;i++)       //輸出三角形
        {
        	for(int j=0;j<num;j++)
        	{
        		if(value[i][j]!=0)
        		{System.out.print(value[i][j]);
        		System.out.print(" ");}
        	}
        	System.out.println();
        }
        
        
     } 
   }