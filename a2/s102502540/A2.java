package ce1002.a2.s102502540;

import java.util.Scanner;

public class A2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		scanner = new Scanner(System.in); 
		int N;
		do {
			System.out.print("Please input a number (1~10): "); //輸出
			System.out.print("\n");	
			N = scanner.nextInt(); //輸入整數N
			if (N < 1 || N > 10)
				System.out.print("Out of range!");
				System.out.print("\n");		
		} while (N < 1 || N > 10);
		
		int arr[][]; //宣告陣列
		arr = new int [10][10];  //陣列範圍
		for (int x = 0;x < N;x++) //輸出及運算
		{
			for (int y = 0;y <= x;y++)
			{
				if( y == 0 || x == y)
					arr[x][y]=1;
				else
					arr[x][y]=arr[x-1][y]+arr[x-1][y-1];
				System.out.print(arr[x][y]+" ");
			}
			System.out.print("\n");	
			}
	}
}
