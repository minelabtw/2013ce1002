package ce1002.a2.s102502503;
import java.util.Scanner;
public class A2 
{
	private static Scanner scanner;
	public static void main(String[] args) 
	{
		int num;
		scanner = new Scanner(System.in);
		
		do{  //檢查輸入的值是否在範圍內
			System.out.println("Please input a number (1~10): ");
			num = scanner.nextInt();
			if (num>10 || num<1)
			{
				System.out.println("Out of range!");
			}
		}while (num>10 || num<1);
		
		int tri[][] = new int[num][];  //宣告二維陣列
		for (int i=0; i<num; i++)
			tri[i]= new int [i+1];
		
		for (int i=0; i<num; i++)  //輸出巴斯卡三角形
		{
			for (int j=0; j<tri[i].length; j++)
			{
				if (j==0 || j==(tri[i].length-1))  //每一列的第一個和最後一個數都為1
				{
					tri[i][j]=1;  //將數儲存至陣列
				    System.out.print("1 ");
				}
				else
				{
					tri[i][j]=tri[i-1][j-1]+tri[i-1][j];  //將數儲存至陣列
					System.out.print(tri[i-1][j-1]+tri[i-1][j]+" ");
				}
			}
			System.out.println();
		}
	
	}

}