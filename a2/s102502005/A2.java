package ce1002.a2.s102502005;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n;//用來儲存使用者欲印出的行數。
		int[][] pascal;//宣告一個二維陣列的位址。
		pascal = new int[10][10];//用剛才的位址儲存未來要操作的二維陣列位址。
		
		Scanner input = new Scanner(System.in);
		
		for(int i=0;i<9;i++)//初始化陣列元素的值。
		{
			for(int j=0;j<=i;j++)
			{
				pascal[i][j] = 0;
			}
		}
		
		for(int i=0;i<9;i++)//將巴斯卡三角形周圍的一先填進去。
		{
			pascal[i][0] = 1;
			pascal[i][i] = 1;
		}
		
		System.out.println("Please input a number (1~10): ");// 要求使用者輸入欲顯示的行數。
		n = input.nextInt();
		
		while(n<1 || n>10)//若輸入不符範圍，則要求使用者重新輸入制至符合範圍為止。
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
		}
		
		for(int i=2;i<n;i++)//計算欲顯示的資料
		{
			for(int j=1;j<=i;j++)
			{
				pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];
			}
		}
		
		for(int i=0;i<n;i++)//印出欲顯示的資料。
		{
			for(int j=0;j<=i;j++)
			{
				System.out.print(pascal[i][j]+" ");
			}
			System.out.print("\n");
		}
	}

}