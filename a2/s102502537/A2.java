package ce1002.a2.s102502537;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int a; //宣告一個整數
		int[][] b = new int[10][10] ;		//宣告一個二微陣列
		b[0][0]=1;	
		
		for(int i=1 ; i<=9 ; i++)
		{
			for(int j=0 ; j<i+1 ; j++)
			{
				int n=1;
				int m=1;
				int p=1;
				for(int x=1 ; x<=i ; x++)
					n *= x;
				for(int x=1 ; x<=j ; x++)
					m *= x;
				for(int x=1 ; x<=i-j ; x++)
					p *= x;
				b[i][j] = n/(m*p);		//用二項式定理來算出每個位置的數值
			}
		}
		System.out.println("Please input a number (1~10): "); //輸出一串字
		Scanner in = new Scanner (System.in); //輸入
		a = in.nextInt(); 
		while(a<1||a>10)		//判斷是否在範圍內
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			a = in.nextInt(); 
		}
		for (int i=0 ; i<a ; i++) //印出結果
		{
			for (int j=0 ; j<i+1 ; j++)
				System.out.print(b[i][j]+" ");
			System.out.println("");
		}
	}
}