package ce1002.a2.s984008030;

import java.util.Scanner;

public class A2{

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);//宣告Scanner形態的scanner物件 並初始化
		int number = 5;//宣告int形態的整數變數 並初始化為5
		int[][] pascalTriArray = new int[10][10];//宣告10*10 int形態的巴斯卡三角形陣列
		while(true){//取得輸入
			System.out.println("Please input a number (1~10): ");
			number = scanner.nextInt();
			if(number < 1 || number > 10){
				System.out.println("Out of range!");
			}
			else{
				break;
			}
		}
		scanner.close();
		for(int i = 0; i < number; i++){
			for(int j = 0; j <= i; j++){//算到對角線就好
				if(j == 0 || i == j){//對角線和第一個column都是1
					pascalTriArray[i][j] = 1;
				}
				else{//其他的就是上左加上方的值
					pascalTriArray[i][j] = pascalTriArray[i - 1][j - 1] + pascalTriArray[i - 1][j];
				}
			}
		}
		for(int i = 0; i < number; i++){
			for(int j = 0; j <= i; j++){
				System.out.print(pascalTriArray[i][j] + " ");
			}
			System.out.print('\n');
		}
	}

}
