package ce1002.a2.s102502011;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in) ;
		
		int number ;
		int[][] pascal = new int[10][10] ;  //宣告變數和陣列
		
		do
		{
			System.out.println("Please input a number (1~10):") ;  //輸入和判斷輸入是否正確
			number = input.nextInt() ;
			if ( number > 10 || number < 1 )
				System.out.println("Out of range!") ;
		}
		while ( number > 10 || number < 1 ) ;
		
		for(int x=0;x<number;x++) {   
			
			for(int y=0;y<x+1;y++) { 
				if (y==0 || y==x)  //位子在兩邊的情況
					pascal[x][y] = 1 ;
				else
				pascal[x][y] = pascal[x-1][y-1] + pascal[x-1][y] ; //在中間的情況
			}
		}
		
		
		for(int x=0;x<number;x++) {
			
			for(int y=0;y<x+1;y++) {
				System.out.print(pascal[x][y] + " ") ; //輸出結果
			}
			System.out.println() ;
		}
		
		input.close() ;

	}

}
