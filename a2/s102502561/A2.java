package ce1002.a2.s102502561;

import java.util.Scanner;

public class A2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (1~10):");
		int n = input.nextInt();
		int[][] arr = new int[12][12];
		while (n < 1 || n > 10) {
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10)");
		} // 題意輸入一個1~10的數字
			// [i][j] i是層數(垂直) j是橫列
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				if (j == 1 || j == i) {// 代表該層的第一個數字與最後一個數字會是1
					arr[i][j] = 1;
				} else {
					arr[i][j] = arr[i - 1][j - 1] + arr[i - 1][j];
					// 該層的第j的數字會是上一層的第(j-1)和第 j 數字的和
				}
			}
		}

		for (int i = 1; i <= n; i++) { // 輸出結果
			for (int j = 1; j <= i; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}

	}
}
