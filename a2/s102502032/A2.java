package ce1002.a2.s102502032;

import java.util.Scanner;

public class A2
{
	public static void main(String[] args)
	{
		int size;
		int Pascaltriangle[][] = new int[1][1];
		size = getSize(1, 10, 0);
		Pascaltriangle = setArray(size);
		Print2DArray(Pascaltriangle, size);
	}

	// get size; will change line
	public static int getSize(int min, int Max, int printline)
	{
		// declaration
		int size = 0;
		Scanner jin = new Scanner(System.in);
		do
		{
			System.out.println("Please input a number (" + min + "~" + Max
					+ "): ");
			size = jin.nextInt();
			if (size < min || size > Max)
				System.out.println("Out of range!");
		}
		while (size < min || size > Max);
		jin.close();
		return size;
	}

	// print 2-dimension array
	public static void Print2DArray(int arr[][], int d1)
	{
		for (int i = 0; i < d1; i ++)
		{
			for (int j = 0; j < i + 1; j ++)
			{
				System.out.print(arr[i][j] + " ");
			}
			// change line
			System.out.println();
		}
	}

	// setting array
	public static int[][] setArray(int size)
	{
		int arr[][] = new int[size][size + 1];
		// binomial thm
		for (int i = 0; i < size; i ++)
		{
			for (int j = 0; j < i + 1; j ++)
			{
				arr[i][j] = rCn(i, j);
			}
		}
		return arr;
	}

	// caculating combinations
	public static int rCn(int r, int n)
	{
		int temp = 1;
		for (int i = r - n + 1; i <= r; i ++)
		{
			temp *= i;
		}
		for (int j = 1; j <= n; j ++)
		{
			temp /= j;
		}
		return temp;
	}
}
