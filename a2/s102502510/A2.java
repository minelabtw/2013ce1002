package ce1002.a2.s102502510;
import java.util.Scanner;
public class A2 {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		System.out.print("Please input a number (1~10): \n");
		int number=input.nextInt();
		while(number<1||number>10)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number=input.nextInt();
		}
		int [][] pascal;
		pascal=new int [10][10];
		pascal[0][0]=1;
		for(int i=1;i<10;++i)
		{
			pascal[i][0]=1;
			for(int j=1;j<10;++j)
			{
				pascal[i][j]=pascal[i-1][j-1]+pascal[i-1][j];
			}
		}
		
		for(int i=0;i<number;++i)
		{
			System.out.print(pascal[i][0]);
			for(int j=1;j<number;++j)
			{
				if(pascal[i][j]!=0)
					System.out.print(" "+pascal[i][j]);
			}
			System.out.println();
		}
}

}