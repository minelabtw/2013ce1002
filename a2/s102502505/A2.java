package ce1002.a2.s102502505;

import java.util.Scanner;//匯入套件

public class A2 {

	private static Scanner scanner;//宣告scanner套件
	public static void main(String[] args) {
		// TODO Auto-generated method stub//
		
		int n;//定義巴斯卡三角形階層數
		int[][] array1;//定義陣列存取巴斯卡三角形
		scanner = new Scanner(System.in);//將scanner變為物件
		array1 = new int[10][10];//將陣列變為物件
		
		System.out.println("Please input a number (1~10): ");//輸出下列字串
		n = scanner.nextInt();//將輸入的數存入字串中
		while( n<1||n>10 )//利用迴圈限定n值
		{
			System.out.println("Out of range!");//輸出字串
			System.out.println("Please input a number (1~10): ");
			n = scanner.nextInt();//再一次存n值
		}
		
		for (int i=0;i<n+1;i++)//利用迴圈來存巴斯卡三角形的陣列
		{
			for (int j=0;j<i;j++)
			{
				if (j==0)//如果是首項，則直接設為1
					array1[i][j]=1;
				else if(j==i)//如果是末項，也直接設為1
					array1[i][j]=1;
				else//其他則為上列的兩數和相加
					array1[i][j]=array1[i-1][j-1]+array1[i-1][j];
			}
		}
		
		for (int i=0;i<n+1;i++)//輸出所存的巴斯卡三角形
		{
			for (int j=0;j<i;j++)
				{
					System.out.print(array1[i][j]+" ");//並在輸出的數中輸入空格
				}
			System.out.println();//下一行
		}
	}

}
