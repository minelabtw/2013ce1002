package ce1002.a2.s101201521;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[][] pascal = new int[10][10]; // store pascal list
		int n;
		// request pascal rank
		do{
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
			if(n<1 || n>10)
				System.out.println("Out of range!");
		} while (n<1 || n>10);
		input.close();
		//costruct pascal table
		for(int i=0; i<10; i++) {
			for(int j=0; j<10; j++) {
				if(j == 0 || j == i)
					pascal[i][j] = 1;
				else if(j > 0 && j < i)
					pascal[i][j] = pascal[i-1][j-1] + pascal[i-1][j];
			}
		}
		//print pascal
		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++) {
				if(j > i)
					continue;
				System.out.print(pascal[i][j] + " ");
			}
			System.out.println();
		}
		
	}
}
