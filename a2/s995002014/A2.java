package ce1002.a2.s995002014;

import java.util.*;

public class A2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		
		//User input
		int l=0;
		scanner = new Scanner(System.in);
		System.out.print("Please input a number (1~10):\n");
		do 
		{
			l = scanner.nextInt();
			if (l < 1 || l > 10)
				System.out.print("Out of range!\nPlease input a number (1~10):\n");
		} while (l < 1 || l > 10);
		
		//Define Pascal array
		Integer[] Pascal = new Integer[10];
		Arrays.fill(Pascal, 0);
		Pascal[0]=1;
		
		//Print out
		int i,j;
		for(i=0;i<l;i++) {
			for(j=i;j>0;j--) {
				Pascal[j] = Pascal[j] +Pascal[j-1];
			}
			for(j=0;j<=i;j++){
				System.out.print(Pascal[j]+" ");
			}
			System.out.print("\n");
		}
	}
}
