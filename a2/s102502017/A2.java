package ce1002.a2.s102502017;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int[][] x= new int[10][10];
		int _input;
		
		while(true){
			System.out.println("Please input a number (1~10): ");
			_input=input.nextInt();
			if(_input>=1 && _input<=10)break;
			else System.out.println("Out of range!");
		}
		
		x[0][0]=1;
		
		for(int i=0,k=1;i<_input;i++,k++){
			for(int j=0;j<k;j++){
				if(j==k-1){
					if(j==0)x[i][j]=1;		//for n=1
					else x[i][j]+=x[i-1][j-1]+x[i-1][j];		
					System.out.println(x[i][j]+" ");
				}
				else {
					if(j==0)x[i][j]=1;
					else x[i][j]+=x[i-1][j-1]+x[i-1][j];
					System.out.print(x[i][j]+" ");
				}
			}
		}
		input.close();		//Close object"input"
	}
}
