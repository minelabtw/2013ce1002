package ce1002.a2.s102502040;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//建立輸入器供使用者輸入三角形高度
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (1~10): ");
		int hight= input.nextInt();
		while(hight <1 || hight > 10 ){
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			hight = input.nextInt();
		}
		//設定陣列,及巴斯卡三角形的值
		int arr[][];
		arr= new int[hight][hight];
		for(int a=0 ;a<=hight-1 ; a++){
			arr[a][0]=1;
			arr[a][a]=1;
			System.out.print(arr[a][a] + " ");
			if(a>1){
				for(int b=1; b<a;b++){
					arr[a][b] = arr[a-1][b-1] + arr[a-1][b] ;
					System.out.print(arr[a][b] + " ");
				}			
			}
			if(a>0)
			{
				System.out.print(arr[a][a] + " ");
			}
			System.out.println();
		}
	}
}
