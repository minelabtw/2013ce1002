package ce1002.a2.s102502549;

import java.util.Scanner;

public class A2 
{
	public static void main(String[] args) 
	{
		int n;
		Scanner inputScanner=new Scanner(System.in);
		
		do //輸入並檢查
		{
			System.out.println("Please input a number (1~10): ");
			
			n=inputScanner.nextInt();
			
			if(n<1||n>10)
				System.out.println("Out of range!");
			
		} while(n<1||n>10);

		for(int i=0;i<n;i++)//印結果
		{
			for(int j=0;j<=i;j++)
			{
				System.out.print(Combination(i,j)+" ");
			}
			
			if(i!=n)
				System.out.println();
		}
	}

	public static int Combination(int a,int b)//這大概是比較需要說明的部分，如函式名稱，他是數學的組合
	{
		int numerator=1,denominator=1;
		
		if(a==0||b==0)
			return 1;
		else
		{
			//組合算法為[a*(a-1)*....*(a-b+1)]/[1*2*...*b]
			for(int n=1;n<=b;n++)
			{
				numerator*=a;
				a--;
			}
				
			for(int n=1;n<=b;n++)
			{
				denominator*=n;
			}
			
			return numerator/denominator;
		}
	}
}
