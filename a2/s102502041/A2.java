package ce1002.a2.s102502041;

import java.util.Scanner;

public class A2 {
	public static int comb(int r, int n)
	{
		/*The combination method*/
		int p=1,i;
		for(i=1;i<=n;i++) p=p*(r-i+1)/i;
		return p;
	}

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int num,r;
		System.out.println("Please input a number (1~10): ");
		do
		{
			/*確認階層數在給定範圍內，若否則要求重新輸入*/
			num = cin.nextInt();
			if(num<1||num>10) System.out.println("Out of range!\nPlease input a number (1~10): ");
		}while(num<1||num>10);
		
		for(r=0;r<num;r++)
		{
			/*使用兩層迴圈和前面所定義的組合函數/方法來輸出巴斯卡三角形*/
			int n;
			for(n=0;n<=r;n++){
				System.out.print(comb(r, n));
				System.out.print(" ");
			}
			System.out.print("\n");
		}
			
		
	}
	

}
