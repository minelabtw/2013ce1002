package ce1002.a2.s995001561;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {

		int n;
		int[][] matrix = new int[10][10];
		
		System.out.println( "Please input a number (1~10): " );
		Scanner input = new Scanner(System.in);	
		n = input.nextInt();
		
		while(n<1 || n>10) {
		System.out.println( "Out of range!\nPlease input a number (1~10): " );
		n = input.nextInt();
		}
		
		input.close();
		
		for(int r = 0; r<= n-1; r++)
		{
			matrix[r][0] = 1;
			
			for(int c = 1; c <= r; c++)
			{
				matrix[r][c] = matrix[r][c-1] * (r - c + 1) / c;
				
			}
		}
		
		for(int r = 0; r<= n-1; r++){
			for(int c = 0; c <= r; c++){
				System.out.print( matrix[r][c] );
				System.out.print( " " );
			}
			System.out.print( "\n" );
		}
		

	}

}
