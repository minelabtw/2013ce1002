package ce1002.a2.s102502514;
import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (1~10): ");
		int number = input.nextInt();
		while (number<1||number>10)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}
		int [][] arr = new int [10][10];  //宣告一個至多為10*10的二維陣列
		int upperbound=1;  //宣告一個橫列的上界
		for (int i=0; i<number; i++)
		{
			for (int j=0; j<upperbound; j++)
			{
				if (j==0||j==i)  //如果為每列的第一個或最後一個那就=0
				{
					arr[i][j]=1;
					System.out.print(" ");
				}
				else 
				{
					arr[i][j]=arr[i-1][j-1]+arr[i-1][j];  //巴斯卡三角形的基本定義
					System.out.print(" ");
				}
				System.out.print(arr[i][j]);  //印出結果
			}
			upperbound++;  //列的上界+1，繼續跑迴圈
			System.out.println("");  //換行
		}
	}
}
