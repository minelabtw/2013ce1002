package ce1002.a2.s102502035;

import java.util.Scanner;//導入輸入函式庫

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Please input a number (1~10): ");//提示輸入
		Scanner input = new Scanner(System.in);//建立 scanner class input object
		int inputnum = input.nextInt();// 輸入
		int[][] outputnum = new int[10][10];//建立array
		while (inputnum < 1 || inputnum > 10) {
			System.out.println("Out of range!");//提示錯誤
			System.out.println("Please input a number (1~10): ");//再提示輸入
			inputnum = input.nextInt();//再輸入
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				outputnum[i][j] = 0;//歸零
			}
		}
		for (int i = 0; i < 10; i++) {
			outputnum[i][0] = 1;//帕斯卡兩旁為1
			outputnum[i][i] = 1;
		}
		for (int i = 1; i < inputnum; i++) {
			for (int j = 1; j < i; j++) {
				outputnum[i][j] = outputnum[i - 1][j - 1] + outputnum[i - 1][j];//相加
			}
		}
		for (int i = 0; i < inputnum; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(outputnum[i][j] + " ");//輸出值
			}
			System.out.print(outputnum[i][i] + " ");//輸出邊緣
			if (i < inputnum - 1)
				System.out.println();//不為尾行換行
		}
	}
}
