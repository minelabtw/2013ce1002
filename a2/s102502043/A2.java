package ce1002.a2.s102502043;
import java.util.Scanner;
public class A2 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (1~10): ");
		int num = input.nextInt();
		while(num<1||num>10)                                        //限定輸入大小
		{
			System.out.println("Out of range!");
			System.out.print("Please input a number (1~10): ");
			num = input.nextInt();
		}
		System.out.println(num);
		int Array[][] = new int[11][11];                            //宣告二維陣列
		for(int i=1;i<=num;i++)
		{
			for(int j=1;j<=num;j++)                                
			{
			    if(j>i) 										     //現在橫列的個數
			    {
			    	break;
			    }
				else if(i==j)                                        //每列最後1的位置
				{
					Array[i][j] = 1;
					System.out.println(Array[i][j]+" ");
				}
				else if(j==1)                                        //第一行為1
				{
					Array[i][j] = 1;
					System.out.print(Array[i][j]+" ");
				}
				else                                                   //期他位置算式
				{
					Array[i][j] = Array[i-1][j-1] + Array[i-1][j];
					System.out.print(Array[i][j]+" ");
				}
			}
		}
		
	}

}
