package ce1002.a2.s101201023;

import java.util.Scanner;

public class A2 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		int array[][]=new int[10][10];
		int N=0;
		int i=3;
		Scanner input = new Scanner(System.in);
		
		//輸入整數並判斷範圍
		System.out.println("Please input a number (1~10): ");
		N = input.nextInt();
		while(N<1 || N>10)
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			N = input.nextInt();
		}
		
		//輸出巴斯卡三角形
		array[0][0]=1;                                     //第一層
		System.out.println(array[0][0]);
		
		if(N>=2)
		{	
			array[1][0]=1;                                //第二層
			array[1][1]=1;
			System.out.print(array[1][0]+" ");
			System.out.println(array[1][1]);
			
			for(int j=2 ; j < N ; j++)                    //三層以上
			{
				array[j][0]=1;
				System.out.print(array[j][0]+" ");
				for(int k=1 ; k < i-1 ; k++)
				{
					array[j][i-1]=array[j][0];						
					array[j][k]=array[j-1][k-1]+array[j-1][k];
					System.out.print(array[j][k]+" ");
				}
				System.out.println(array[j][i-1]);
				i++;	
			}
		}
		
	}

}
