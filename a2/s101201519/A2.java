package ce1002.a2.s101201519;
import java.util.Scanner;
public class A2 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int [][]x = new int[10][10];//先設定陣列大小
		int N=0;//帕斯卡三角形的大小
		do
		{
			System.out.println("Please input a number (1~10): ");
			N=input.nextInt();
			if(N<1 || N>10)
				System.out.println("Out of range");
		}while(N<1 || N>10);
		input.close();
		
		for(int i=0; i<N; i++)//將帕斯卡三角型寫入陣列
		{
			x[i][0]=1;
			x[i][i]=1;//頭尾都為1
			for(int j=1; j<i; j++)//j從1開始
			{
				x[i][j] = x[i-1][j-1] + x[i-1][j];
			}
		}
		for(int h=0; h<N; h++)//印出三角形
		{
			for(int k=0; k<=h; k++)
			{
				System.out.print(x[h][k]+" ");
			}
			System.out.println("");//換行
		}
	}
}
