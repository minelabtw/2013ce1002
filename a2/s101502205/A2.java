package ce1002.a2.s101502205;

import java.util.Scanner;

public class A2 {

	private static Scanner scanner;
	
	public static void main(String[] args) {

		//Create a scanner object
		scanner = new Scanner(System.in);
		
		int n, array[], i, j;
		
		//User input
		System.out.println("Please input a number (1~10): ");
		n = scanner.nextInt();
		while(n<1 || n>10){
			//error
			System.out.println("Out of range!");
			//User input
			System.out.println("Please input a number (1~10): ");
			n = scanner.nextInt();
		}
		
		array = new int[n];
		array[0] = 1;	//The first element of first row is 1
		//Print the first row
		System.out.println(array[0]);
		
		//Output the remaining rows
		for(i=1; i<n; i++){
			
			//Rules of Pascal's triangle:
			//(i, j) = (i-1, j) + (i-1, j-1);
			for(j=i; j>0; j--){
				array[j] += array[j-1];
			}
			
			//Output ith row
			for(j=0; j<=i; j++){
				System.out.print(array[j]);
				System.out.print(" ");
			}
			System.out.println();
		}
		
	}

}
