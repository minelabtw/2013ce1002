package ce1002.a2.s102502506;
import java.util.Scanner;
public class A2 {
	public static void main(String[] args){ 
		Scanner input = new Scanner(System.in);
		int num;                            //設變數
		int[][]arr = new int[10][10];       //設2維陣列
		do
		{
			System.out.println("Please input a number (1~10): ");  //輸出輸入
			num = input.nextInt();
			if(num<1||num>10)                                      //判斷輸入的市府超出範圍 
			System.out.println("Out of range!");
		}while(num<1||num>10);                                     //入超出範圍在執行一遍
		for(int i=0;i<num;i++){                                    //把數字丟入陣列每行第一個皆為0
			for(int j=0;j<num;j++){ 
				if(j==0){
					arr[i][j]=1;
				}
				if(i>0&&j!=0){                                     //除了每行第一個數其他數都是自己上面的數+左上方的數
					arr[i][j]=arr[i-1][j]+arr[i-1][j-1];
				}
			}
		}
		for(int j=0;j<num;j++){                                    //輸出帕斯卡三角形0不輸出
			for(int i=0;i<num;i++){
				if(arr[j][i]!=0){
				System.out.print(arr[j][i]);
				System.out.print(" ");
				}
				}
			if(j<num-1)System.out.println();                        //換行但最後一次不要換  
			}
		}
	}

