package ce1002.a2.s102502560;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		
		Scanner STDIN=new Scanner(System.in);
		
		int n;
		int[][] layer;

		while(true){
			System.out.println("Please input a number (1~10): ");
			n=STDIN.nextInt();
			if(n>=1&&n<=10)break;
			System.out.println("Out of range!");
		}
		
		STDIN.close();
		
		layer=new int[n][];
		
		for(int i=0;i<layer.length;i++){								//SET 1 at the beginning and the end of the layer
			layer[i]=new int[i+1];
			layer[i][0]=layer[i][layer[i].length-1]=1;
		}
		
		for(int i=2;i<layer.length;i++){								//calculate the number in the middle by former layer
			for(int j=1;j<=layer[i].length-2;j++){
				layer[i][j]=layer[i-1][j-1]+layer[i-1][j];
			}
		}
		
		for(int i=0;i<layer.length;i++){								//print out
			for(int j=0;j<layer[i].length;j++){
				System.out.print(layer[i][j]+" ");
			}
			System.out.println();
		}
		
	}
	
}
