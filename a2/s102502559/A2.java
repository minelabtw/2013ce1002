package ce1002.a2.s102502559;
import java.util.Scanner;
public class A2 {

	public static void main(String[] args) {
		System.out.println("Please input a number (1~10): ");
		Scanner input = new Scanner(System.in);//建立一個輸入器
		int input_number = input.nextInt();//從使用者輸入的整數值指定給變數
	/*判斷使用者輸入的值是否超出範圍,若超出,跳出錯誤訊息,並要求使用者再輸入一次*/
		if (input_number < 1 || input_number > 10) {
			System.out.print("Out of range!\n");
			System.out.println("Please input a number (1~10): ");
			input_number = input.nextInt();
		}
		/*宣告一個二維陣列*/
		int pascal[][];
		pascal = new int[10][10];
		int L = 0;
		int W = 0;
		pascal[0][0] = 1;
		/*建立一個迴圈讓巴斯卡三角形左右兩側的數字皆為1*/
		for (L = 0; L < 10; ++L) {
			for(W = 0 ; W < L ; ++W){
				if(W == 0 || W == L)
				{
					pascal[L][W]=1;
				}
			}
		}
		/*使用迴圈逐一把計算後的值指定給二維陣列中各項*/
		for (L = 2; L < 10; ++L) {
			for (W = 1; W < L; ++W) {
				pascal[L][W] = pascal[L - 1][W - 1] + pascal[L - 1][W];
			}
		}
		//印出巴斯卡三角形中的各項
		for (L = 0; L <= input_number; ++L) {
			for(W = 0 ; W < L ; ++W){
				System.out.print(pascal[L][W]+" ");
				if(W == L-1)
				{
					System.out.print("\n");
				}
			}
		}

	}

}
