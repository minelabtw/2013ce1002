package ce1002.a2.s102502552;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);//掃描機以輸入
		
		int num = 0;//宣告輸入的變數
		int[][] pascal = new int[10][10];//宣告帕斯卡陣列
		
		do{
			System.out.print("Please input a number (1~10):");
			num = sc.nextInt();
			if(num > 10 || num < 1)
				System.out.println("Out of range!");
		}while(num > 10 || num < 1);//輸入變數并檢查
		
		pascal[0][0] = 1;//給予帕斯卡頂端的值"1"
			
	for(int i = 1;i < num;i++)//帕斯卡三角形行的迴圈
	{
		for(int j = 0;j < num;j++)//帕斯卡三角形列的迴圈
		{
			if(j == 0 || j == i)
				{
				pascal[i][j] = 1;
				}//每行的首與末都是1
			else
				{
				pascal[i][j] = pascal[i - 1][j - 1] + pascal[i - 1][j];
				}//其他則是上一行與自己同列與前一列兩個數的和
		}
	}
	
	for(int i = 0;i < num;i++)//行迴圈
	{
		for(int j = 0;j < i + 1;j++)//列迴圈
		{
			System.out.print(pascal[i][j] + " ");//輸出帕斯卡三角形并將數與數之間隔開
		}System.out.println();//每行結束即換行
	}
	}
	

}
