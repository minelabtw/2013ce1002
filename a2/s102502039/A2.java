package ce1002.a2.s102502039;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (1~10): ");// 輸出題目
		int number = input.nextInt();// 輸入數字
		while (number < 1 || number > 10) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			number = input.nextInt();
		}

		int pascal[][] = new int[number][number];// 進行輸出
		for (int i = 0; i < number; i++) {
			for (int j = 0; j <= i; j++) {
				if (j == 0 || i == j)
					pascal[i][j] = 1;
				else
					pascal[i][j] = pascal[i - 1][j - 1] + pascal[i - 1][j];
			}
		}
		for (int i = 0; i < number; i++) {
			for (int j = 0; j <= i; j++) {
				System.out.print(pascal[i][j] + " ");
			}
			System.out.println();
		}
	}

}
