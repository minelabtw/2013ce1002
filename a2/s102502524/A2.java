package ce1002.a2.s102502524;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int a = 0;													// 設定輸入變數
		
		Scanner input = new Scanner(System.in);						//使用者輸入
		
		System.out.println("Please input a number (1~10): ");		//輸入巴斯卡三角形的層數
		a = input.nextInt();
		while(a<1 || a>10)											//判斷範圍是否符合
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			a = input.nextInt();
		}
		
		int[][] b = new int [10][10];								//設定巴斯卡三角形的儲存陣列
		
		for(int i=0;i<a;i++)										//完成巴斯卡三角形
		{
			for(int k=0;k<=i;k++)
			{
					if(k==0 || k==i)								//若為該行首數或尾數，其值為1
						b[i][k] = 1;
					else
						b[i][k] = b[i-1][k-1] +b[i-1][k];
					
					System.out.print(b[i][k] + " ");
			}
			System.out.println();
		}
	}

}
