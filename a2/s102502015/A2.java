package ce1002.a1.s102502015;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		int [][]arr= new int[10][10];
		int a;
		int b = 1;
		int c = 1;
		int d = 1;
		Scanner scanner = new Scanner(System.in); // 新的SCANNER
		System.out.println("Please input a number (1~10): ");
		a = scanner.nextInt(); // 讀整數
		while (a > 10 || a < 1) { // 超出範圍則重新輸入
			System.out.println("Out of range!");
			System.out.println("Please input a number (1~10): ");
			a = scanner.nextInt();
		}
		for (int i = 0; i <= a-1; i++) {         //算三角形
			if (i == 0) {
				arr[i][0]=1;
			} else {
				for (int j = 1; j <= i+1; j++) {
					for (int k = 1; k <= i; k++) {
						b = b * k;
					}
					for (int l = 1; l <= j - 1; l++) {
						c = c * l;
					}
					for (int m = 1; m <= i - j + 1; m++) {
						d = d * m;
					}
					arr[i][j-1]=b/c/d;         //存入陣列
					b=1;
					c=1;
					d=1;
				}
			}
		}
		for(int n=0;n<=a-1;n++)
		{
			for(int o=0;o<=n;o++){							//印出
				System.out.print(arr[n][o]+" ");
				}
			System.out.println("");
		}
		scanner.close(); // 關SCANNER
	}
}
