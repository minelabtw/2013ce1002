package ce1002.a2.s102502508;
import java.util.Scanner ;
public class A2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input= new Scanner(System.in);    //進入程式點
		
		int N=0 ;
		int i=0 ;
		int j=0 ;
		
		do{  //建立迴圈加以判斷使用者可輸入範圍   
			
			System.out.print("Please input a number (1~10): " );
			N=input.nextInt() ;
			
			if(N>=1 && N<=10)
			{
				break ;    //如果符合範圍要求就跳出迴圈
			}
			else
			{
				System.out.println("Out of range!");
			}
		}while(N<1 || N>10);
		
		int[][] Pasca = new int[11][11] ;   //建立一陣列
		
		Pasca[0][0]=1 ;    //定義某特定陣列的初始值為1
		
	    for(i=1 ; i<N+1; i=i+1)    //利用迴圈進行Pasca三角形的規則運算
	    {
	    	for(j=1 ; j<N+1 ; j=j+1)
	    	{   
	    		Pasca[i][j]=Pasca[i-1][j-1]+Pasca[i-1][j] ;
	    		
	    		if(Pasca[i][j]!=0)
	    		{
	    			
	    			System.out.print(Pasca[i][j]+ " ");
	    		
	    		}
	    	}
	    	System.out.println(" ");
	    }

	}

}
