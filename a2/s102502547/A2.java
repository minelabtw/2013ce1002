package ce1002.a2.s102502547;

import java.util.Scanner;

public class A2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		int a=0;
		do
		{
		System.out.println("Please input a number (1~10): "); //輸入數字，範圍不合的話重新輸入
		a = scanner.nextInt();
		if(a<1 || a>10)
			System.out.println("Out of range!");
		}while(a<1 || a>10);
		
		int[][] x;
		x=new int[a][a]; //宣告陣列，長寬為a
		for(int i=0;i<a;i++)
		{
			for(int j=0;j<a;j++) //歸0
			{
				x[i][j]=0;
			}
		}
		x[0][0]=1;
		for(int i=0;i<a-1;i++) //帕斯卡三角形
		{
			for(int j=0;j<=i;j++)
			{
				x[i+1][j]=x[i+1][j]+x[i][j];
				x[i+1][j+1]=x[i+1][j+1]+x[i][j];
			}
		}
		
		for(int i=0;i<a;i++) //輸出
		{
			for(int j=0;j<=i;j++)
			{
				System.out.print(x[i][j]+" ");
			}
			System.out.println();
		}
	}
}
