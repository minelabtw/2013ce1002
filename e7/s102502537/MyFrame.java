package c1002.e7.s102502537;

import javax.swing.*;
public class MyFrame extends JFrame{
	MyPanel panel = new MyPanel();
	public MyFrame()
	{
		setSize(600,600);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		add(panel);
	}
}
