package ce1002.e7.s102502043;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
public class MyPanel extends JPanel 
{
	MyPanel()
	{
		setLayout(null);
		setBounds(0, 0, 500, 500);
	}
	@Override public void paintComponent(Graphics g)
	{
		int i=0,j=0;
		for(i=0;i<10;i++)
		{
			for(j=0;j<10;j++)
			{
				if((i+j)%2==0)
				{
					g.setColor(Color.white);
					g.fillRect(i*50,j*50,50,50);
				}
				else
				{
					g.setColor(Color.black);
					g.fillRect(i*50,j*50,50,50);
				}
				
			}
		}
	}
	
}
