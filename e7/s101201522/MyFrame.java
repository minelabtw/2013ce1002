package ce1002.e7.s101201522;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame	{
	MyFrame() {
		setLayout(null); 
		setSize(530,550);// set window size
		add(new MyPanel()); //add board
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
	}
}
