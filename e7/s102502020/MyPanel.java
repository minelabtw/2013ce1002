package ce1002.e7.s102502020;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	private static int x = 0;
	private static int y = 0;
	public MyPanel(){
		setBounds(0, 0, 500, 500);    //建立一個(500,500)的Panel
	}
	@Override
	protected void paintComponent(Graphics g){    //畫上圖案
		super.paintComponent(g);
		for(int j=0;j<10;j++){
			System.out.println(" "+y);
			for(int i=0;i<10;i++){
				if((i+j)%2==0){
					g.setColor(Color.BLACK);
					g.fillRect(x, y, 50, 50);
					x=x+50;
					}
				else{
					g.setColor(Color.WHITE);
					g.fillRect(x, y, 50, 50);
					x=x+50;
				}
			}
			x=0;
			y=y+50;
		}
		x=0;
		y=0;
	}

}
