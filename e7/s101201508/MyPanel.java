package ce1002.e7.s101201508;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	MyPanel()
	{
		setBounds(0,0,500,500);//set the Size of MyPanel 
	}
	@Override
	protected void paintComponent(Graphics g) //use the graphics to print the graph
	{
		super.paintComponent(g);
		for (int i=0;i<10;i++)
		{
			for (int j=0;j<10;j++)
			{
				if ((i+j)%2==0)
				{
					g.setColor(Color.WHITE);//decide the color
				}
				else
				{
					g.setColor(Color.BLACK);//decide the color
				}
				g.fillRect(i*50,j*50,50,50);//print the filled rectangle
			}
		}
	}
}
