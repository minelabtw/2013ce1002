package ce1002.e7.s102502039;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {//設定輸出
	public MyPanel() {
		setBounds(0, 0, 500, 500);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int k;
		int j;
		for (k = 0; k < 10; k++) {
			for (j = 0; j < 10; j++) {
				if ((k + j) % 2 == 0) {
					g.setColor(Color.WHITE);
					g.fillRect(50 * k, 50 * j, 50, 50);
				} else {
					g.setColor(Color.BLACK);
					g.fillRect(50 * k, 50 * j, 50, 50);
				}
			}
		}
	}
}
