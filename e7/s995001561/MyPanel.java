package ce1002.e7.s995001561;


import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;


@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	MyPanel(){
		this.setLayout(null);//set Layout
	}
	
	@Override public void paintComponent(Graphics g) {//paint fill rectangle
        //super.paintComponent(g);  // Ask parent to paint background.
        int x=0;
        int y=0;
		for(int i=0;i<=9;i++){//paint fill rectangle white and black with loop
        	for(int j=0;j<=4;j++){//merge two as a group and draw a group(white and black) at one time
        		if(i%2==0){//check white one first or black one 
        			g.setColor(Color.white);
            		g.fillRect(x, y, 50, 50);
            		g.setColor(Color.black);
            		g.fillRect(x+50, y, 50, 50);
        		}
        		else{
        			g.setColor(Color.black);
            		g.fillRect(x, y, 50, 50);
            		g.setColor(Color.white);
            		g.fillRect(x+50, y, 50, 50);
        		}
        		
        		x=x+100;
        	}
        	y=y+50;
        	x=0;
        }
        
    }
}