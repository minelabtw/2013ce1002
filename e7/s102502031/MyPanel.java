package ce1002.e7.s102502031;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel {
	public MyPanel() {
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int color;
		for (int i = 0; i < 500; i = i + 50) { // i is x and j is y to java coordinate
			for (int j = 0; j < 500; j = j + 50) {
				color = ((i + j) % 100 == 0) ? 0 : 1;
				switch (color) {
				case 0:
					g.setColor(Color.WHITE);
					g.fillRect(i, j, 50, 50);
					break;
				case 1:
					g.setColor(Color.BLACK);
					g.fillRect(i, j, 50, 50);
					break;
				}
			}
		}
	}
}
