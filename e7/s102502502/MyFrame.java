package ce1002.e7.s102502502;
import javax. swing.*;
@SuppressWarnings("serial")
public class MyFrame extends JFrame {

	public MyFrame() {
         // there is no other panel to be arranged, so no need of layout.
		 //setlayout(null) let it be default
		setLocationRelativeTo(null); // let the window show up in center
		 setSize(516,538);
		 setVisible(true);
		 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   //enable you to close the window
		 MyPanel p = new MyPanel();                        //create a panel with specified layout manager
		 add(p);                                           //add the component to the panel
	}

}
