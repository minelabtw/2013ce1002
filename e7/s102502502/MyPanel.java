package ce1002.e7.s102502502;
import java.awt.*;

import javax. swing.*;

@SuppressWarnings("serial")
public class MyPanel extends JPanel{

	public MyPanel() {
		setBounds(0, 0, 500, 500);                //set points from (0,0) to (500, 500)
	}
	public void paintComponent(Graphics g) {      
		for(int i = 0; i < 10; i++){
			for(int j = 0; j < 10; j++){
				if ((i+j)%2 == 1)
					g.setColor(Color.black);      //set black color for g 
				else
					g.setColor(Color.white);      //set black color for g      
				g.fillRect(j*50, i*50, 50, 50);  
				 // fill color in rectangle whose initial point is (j*50, i*50) and w = h = 50 
			}
		}

	}
}
