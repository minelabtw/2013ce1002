package ce1002.e7.s995002014;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	MyPanel[] box=new MyPanel[100]; //100格Panel
	int i=0,j=0,k=0,l=0;
	boolean flag=true; //旗標
	
	public MyFrame() {
		setLayout(new GridLayout(10, 10)); //用本身就是棋盤的GridLayout
		for(i=0;i<10;i++) {
			for(j=0;j<10;j++){
				box[i] = new MyPanel(flag,k,l); //創造panel物件
				add(box[i]);
				flag=!flag;
			}
			flag=!flag;
		}
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}

class MyPanel extends JPanel {
	boolean fill;
	int posX;
	int posY;
	
	MyPanel(boolean fill,int x,int y) {
		this.fill=fill;
		posX=x;
		posY=y;
	}
	
	protected void paintComponent(Graphics g) {
		int width=50;
		int height=50;
		
		//判斷是白色還是黑色
		if(fill==true){
			g.setColor(Color.black);
		}
		else {
			g.setColor(Color.white);
		}
		g.fillRect(posX, posY, width, height);
	}
}