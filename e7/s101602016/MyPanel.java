package ce1002.e7.s101602016;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
public class MyPanel extends JPanel{
	MyPanel()
	{
		setBounds(0,0,500,500);//設定Panel大小為500*500
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				if((i+j)%2==0)//當x座標+y座標可以整除二時
					g.setColor(Color.WHITE);//將顏色設定為白色
				else//不能整除二時
					g.setColor(Color.BLACK);//將顏色設定為黑色
				g.fillRect(i*50,j*50,50,50);//將設定好的顏色填入到指定的格子裡
			}
		}
	}
}
