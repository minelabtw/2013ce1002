package ce1002.e7.s101602016;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	MyFrame()
	{
		add(new MyPanel());//新增一個MyPanel至MyFrame裡
		setSize(550,550);//設定視窗大小比我要畫的圖再大一點
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//當關閉視窗時，關閉此程式
		setVisible(true);//顯示MyFrame的內容
	}
}
