package ce1002.e7.s102502051;

import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	int color = 0; // 設定變數

	@Override
	public void paint(Graphics g) { // 畫圖
		super.paintComponent(g);
		for (int i = 0; i < 500; i += 50) {
			for (int q = 0; q < 500; q += 50) {
				color = (i + q)/50;
				if (color % 2 == 0) {
					g.setColor(Color.white);
					g.fillRect(i, q, 50, 50);
				} else if (color % 2 == 1) {
					g.setColor(Color.black);
					g.fillRect(i, q, 50, 50);
				}
			}
		}
	}
}
