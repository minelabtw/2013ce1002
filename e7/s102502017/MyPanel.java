package ce1002.e7.s102502017;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel{

	MyPanel(){
		setBounds(0,0,500,500);
	}
	
	
	public void paintComponent(Graphics g){
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				
				//Odd Block is white
				if((i+j)%2==0)g.setColor(Color.WHITE);
				else g.setColor(Color.BLACK);
				
				//draw rectangle
				g.fillRect(j*50, i*50, 50, 50);
			}
		}
	}
}
