package ce1002.e7.s102502030;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel panel = new MyPanel();
	
	public MyFrame() {
		//設定FRAME
		setLayout( null );
		setBounds( 0, 0, 600, 600 );
		setVisible( true );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		newrect( panel );
	}
	
	public void newrect( MyPanel p ) {
		//加入panel
		add( p );
	}
}
