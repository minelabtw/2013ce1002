package ce1002.e7.s102502030;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
    
    public MyPanel() {   
        //setLayout(null); 
        setBounds( 0, 0, 500, 500 );
        this.setVisible(true);
    }
    
    @Override
    public void paintComponent( Graphics g ) {
    	super.paintComponents( g );
    	
    	//設定格子
    	for( int i=0; i<10; i++ ) {
    		for( int j=0; j<10; j++ ) {
    			if( (i+j)%2==1 ) {
    				g.setColor( Color.black );
    				g.fillRect( i*50, j*50, 50, 50 );
    			}
    			else {
    				g.setColor( Color.white );
    				g.fillRect( i*50, j*50, 50, 50 );
    			}
    		}
    	}
    }
}
