package ce1002.e7.s100201023;

import java.awt.Graphics;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	//properties
	private MyPanel panel;
	
	//constructure
	public MyFrame()
	{
		panel = new MyPanel();
		setSize(550, 550);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
	}
	
	//method
	public void drawboard()
	{
		panel.setBounds(0, 0, 500, 500);
		add(panel);
	}
}
