package ce1002.e7.s100201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	@Override
	public void paint(Graphics g)
	{
		int count = 0;
		super.paint(g);
		
		for(int i = 0 ; i < 10 ; ++i)
		{
			for(int j = 0 ; j < 10 ; ++j)
			{
				if(count %2 == 0)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);
				g.fillRect(j * 50, i * 50, 50, 50);
				
				++count;
			}
			++count;
		}
	}	
	
}
