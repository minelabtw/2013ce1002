package ce1002.e7.s101201046;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	
	MyPanel () {
		setBounds(0, 0, 500, 500); //set board size
	}
	
	@Override
	protected void paintComponent (Graphics g) {
		super.paintComponent(g);
		
		//draw board
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				if ((i + j) % 2 == 0) {
					g.setColor(Color.white);
					g.fillRect(50*i, 50*j, 50, 50);
				}
				else {
					g.setColor(Color.black);
					g.fillRect(50*i, 50*j, 50, 50);
				}
	}
}