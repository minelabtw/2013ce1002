package ce1002.e7.s102502508;

import javax.swing.JFrame;

public class E7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		MyFrame mf = new MyFrame() ;//建立MyFrame的物件
		mf.setBounds(0, 0, 550, 550);//設定視窗大小
		mf.add(new MyPanel());//將Panel加到Frame中
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//可以使視窗關閉
		mf.setVisible(true);//讓使用者可以看見視窗

	}

}
