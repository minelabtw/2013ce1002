package ce1002.e7.s102502508;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
public class MyPanel extends JPanel {
	
	JPanel SQ[][]=new JPanel [10][10];  //建立陣列
	//利用迴圈畫圖
	public void paintComponent(Graphics g)
	{
		for(int i=0 ; i<10 ;i=i+1)
		{
			for(int j=0 ; j<10 ;j=j+1)
			{
				if((i+j)%2==0)
				{
					g.setColor(Color.WHITE);
					
				}
				else
				{
					g.setColor(Color.BLACK);
				}
				g.fillRect(i*50, j*50, 50, 50);
			}
			
		}
	}

}
