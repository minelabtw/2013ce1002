package ce1002.e7.s101201524;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame(){
		//create a chess board
		MyPanel panel = new MyPanel();
		panel.setBounds(0, 0, 500, 500);
		//add board to MyFrame
		add(panel);
		//set status
		setLayout(null);
		setBounds(300, 50, 550, 550);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
