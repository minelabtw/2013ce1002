package ce1002.e7.s102502532;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel {

	public MyPanel() {

		setBounds(0, 0, 500, 500); // 座標 + 大小
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); // 改寫 畫圖形

		for (int y = 0; y <= 500; y = y + 50) {
			if (y % 100 == 0) {
				for (int x = 0; x <= 450; x = x + 50) {
					if (x % 100 == 0) {
						g.setColor(Color.WHITE);
						g.fillRect(x, y, 50, 50);
					} else {
						g.setColor(Color.BLACK);
						g.fillRect(x, y, 50, 50);
					}
				}
			} else {
				for (int x = 0; x <= 450; x = x + 50) {
					if (x % 100 == 0) {
						g.setColor(Color.BLACK);
						g.fillRect(x, y, 50, 50);
					} else {
						g.setColor(Color.WHITE);
						g.fillRect(x, y, 50, 50);
					}
				}
			}
		}
	}
}
