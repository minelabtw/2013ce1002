package ce1002.e7.s102502532;
import javax.swing.JFrame;

public class E7 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyFrame frame = new MyFrame(); // 建立板子
		MyPanel panel = new MyPanel();
		
		frame.setSize(550, 550); // frame寫在 main 裡
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.addpanel(panel);
		
		frame.setVisible(true); // 擺最後
	}

}
