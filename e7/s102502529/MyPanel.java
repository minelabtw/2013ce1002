package ce1002.e7.s102502529;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{

	MyPanel(){
		setBounds(0, 0, 500, 500);
	}
	
	public void paintComponent(Graphics grap){
		
		for(int h=0;h<10;h++){
			for(int w=0;w<10;w++){
			if((w+h)%2==0){
				grap.setColor(Color.WHITE);
				grap.fillRect(w*50,h*50,50,50);
			}
			else{
				grap.setColor(Color.BLACK);
				grap.fillRect(w*50,h*50,50,50);
			}
				
			}
		}
	}
}
