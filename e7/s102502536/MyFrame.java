package ce1002.e7.s102502536;

import javax.swing.*;

public class MyFrame extends JFrame {
	
	
	
	public MyFrame()
	{
		MyPanel panel = new MyPanel();
		//set frame's size , layout...
		setLayout(null);
		setBounds(0 , 0 , 500 , 500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
		
	}

}
