package ce1002.e7.s102502536;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	
	public MyPanel() {
				
		setBounds(0, 0, 500, 500);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < 10; i++)
		{
			for (int j = 0; j < 10; j++)
			{
				g.fillRect(j * 50,i * 50,50,50);
			    while(i == 0 || i == 2 || i == 4 || i == 8)
			    {
			    	if (j == 0 || j == 2 || j == 4 || j == 8)
			    		g.setColor(Color.WHITE);
			    	else
			    		g.setColor(Color.BLACK);
			    }
			    while(i == 1 || i == 3 || i == 5 || i == 7 || i == 9)
			    {
			    	if (j == 0 || j == 2 || j == 4 || j == 8)
			    		g.setColor(Color.BLACK);
			    	else
			    		g.setColor(Color.WHITE);
			    }
			}
		}

		
	}

}
