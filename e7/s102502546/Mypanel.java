package ce1002.e7.s102502546;

import java.awt.*;

import javax.swing.*;

public class Mypanel extends JPanel {

	Mypanel() {
		setBounds(0, 0, 500, 500);//設定視窗大小
		setLayout(new GridLayout(10, 10));//設定棋盤式排版
	}

	protected void paintComponent(Graphics g) {
		int x = getWidth();// 取得寬
		int y = getHeight();// 取得高
		super.paintComponent(g);
		for (int j = 0; j < 10; j++) {// 利用for迴圈 判斷該填白色還是黑色
			for (int i = 0; i < 10; i++) {
				if ((i + j) % 2 == 0) {
					g.setColor(Color.WHITE);
					g.fillRect(i * 50, j * 50, x / 10, y / 10);
				} else if ((i + j) % 2 == 1) {
					g.setColor(Color.BLACK);
					g.fillRect(i * 50, j * 50, x / 10, y / 10);
				}
			}
		}

	}
}
