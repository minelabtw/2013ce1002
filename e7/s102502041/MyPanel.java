package ce1002.e7.s102502041;
import java.awt.*;
import javax.swing.*;
public class MyPanel extends JPanel{
	MyPanel()
	{
		setBounds(0,0,500,500);	//set panel size
	}
	

	public void paintComponent(Graphics g)
	{
		for(int x=0;x<10;++x)
		{
			for(int y=0;y<10;++y)
			{
				//set color change between black and white depends on the condition
				if((x+y)%2==1)g.setColor(Color.black);
				else g.setColor(Color.white);
				g.fillRect(x*50, y*50, 50, 50);
			}
		}
	}
}
