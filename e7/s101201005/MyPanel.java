package ce1002.e7.s101201005;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	MyPanel()
	{
		setLayout(null);
		setBounds(0,0,500,500);
	}
	
	public void paintComponent (Graphics g)
	{
		super.paintComponent(g);
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10; j++)
			{
				if ( (i+j)%2 ==1)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				g.fillRect(i*50,j*50,50,50);
			}
		}		
		
	}
}
