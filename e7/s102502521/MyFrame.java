package ce1002.e7.s102502521;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame {
	protected MyPanel p1 = new MyPanel();

	MyFrame() {
		setLayout(null);
		setSize(600, 600);
		add(p1);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

}
