package ce1002.e7.s102502028;
import javax.swing.* ;
import java.awt.* ;
public class MyPanel extends JPanel{
	MyPanel() //設定擺放位置及大小
	{
		setBounds(0,0,500,500) ;
	}
	
	protected void paintComponent(Graphics g)  //畫出棋盤格
		{
			super.paintComponent(g) ;
			for(int y = 3 ; y <= 12 ; y++)
			{
				for(int x = 3 ; x <= 12 ; x++)
				{
					if(x%2 == 1 && y%2 == 1 || x%2 == 0 && y%2 == 0)
					{
						g.setColor(Color.WHITE) ;
						g.drawRect(50*(x-3),50*(y-3),50,50) ;		
					}
					
					if (x%2 == 1 && y%2 == 0 || x%2 == 0 && y%2 == 1)
					{
						g.setColor(Color.BLACK) ;
						g.fillRect(50*(x-3), 50*(y-3), 50, 50) ;
					}				
				}
			}
		}
}
