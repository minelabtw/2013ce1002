package ce1002.e7.s102502542;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel 
{
	MyPanel() 
	{
		
	}

	public void paintComponent(Graphics g) //the method how the graphics draw
	{
		for (int i = 0; i < 10; i++) 
		{
			for (int j = 0; j < 10; j++) 
			{
				if ((i + j) % 2 == 0) 
				{
					g.setColor(Color.WHITE);
					g.fillRect(50*i, 50*j, 50, 50);
				} 
				else 
				{
					g.setColor(Color.BLACK);
					g.fillRect(50*i, 50*j, 50, 50);
				}

			}
		}
		//this.add(panel);

	}

}
