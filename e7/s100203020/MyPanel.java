package ce1002.e7.s100203020;

import java.awt.*;
import javax.swing.JPanel;


public class MyPanel extends JPanel{
	
	private int length;
	MyPanel (){
		setLength(50);//every rectangle is a square which length 
		setBounds(0, 0, 500, 500);

	}
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

	public void paintComponent(Graphics g){
		
		for(int i = 0; i<10 ; i++){
			for(int j=0 ; j<10; j++)
			{
				//if i+j are odd, color will be black.
				if((i+j)%2==1){
					g.setColor(Color.black);
					}
				//if i+j are even, color will be black.
				else{
					g.setColor(Color.white);
					}
				//draw a filled rectangle 
				g.fillRect(length*i, length*j, length, length);
				
			}
		}
	}

}