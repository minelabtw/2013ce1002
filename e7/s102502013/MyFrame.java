package ce1002.a6.s102502013;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class MyFrame extends JFrame{
	private MyPanel myPanels[];
	MyFrame(){
		myPanels = new MyPanel[3];//declare 3 variable for panel
		Hero heros[] = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		JPanel contentOfRole = new JPanel();
		//set picture's frame
		for(int i = 0; i < 3; i++){
			myPanels[i] = new MyPanel();
			newRolePos(heros[i], myPanels[i], 5, 5 + 225 * i);
			contentOfRole.add(myPanels[i]);
			contentOfRole.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			contentOfRole.setLayout(new BorderLayout());
			setContentPane(contentOfRole);
		}
	}
	 public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	 {
		 	panel.setRoleState(hero);
			panel.setLayout(null);
			panel.setBounds(x, y, 265, 220);
			panel.setBorder(BorderFactory.createLineBorder(Color.black, 2));
			panel.add(panel.getRoleSituation());
			panel.add(panel.getRolePicture());
	 }
}
