package ce1002.a6.s102502013;

public class Knight extends Hero{
	private final double hpCoefficient = 0.8;//declare a variable for hpCoefficient
	private final double mpCoefficient = 0.1;//declare a variable for mpCoefficient
    private final double ppCoefficient = 0.1;//declare a variable for ppCoefficient
	Knight(){
		super.setName("Knight");
	}
	public double getHP(){//overwrite hero' getHP function
		return hpCoefficient * super.getHP();
	}
	public double getMP(){//overwrite hero' getMP function
		return mpCoefficient * super.getMP();
	}
	public double getPP(){//overwrite hero' getPP function
		return ppCoefficient * super.getPP();
	}
}
