package ce1002.e7.s102502046;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	MyPanel()
	{
		setBounds(0, 0, 500, 500);
	}
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        for (int i=0;i<10;i++)
        	for (int j=0;j<10;j++)
			{
        		// set color
        		if ((i+j) % 2 == 1)
        			g.setColor(Color.black);
        		else
        			g.setColor(Color.white);
        		// draw rect
        		g.fillRect(j*50, i*50, 50, 50);
			}
        
    }
}
