package ce1002.e7.s102502514;

import javax.swing.JFrame;

public class E7 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		
		frame.setLayout(null);
		frame.setSize(540, 545);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
