package ce1002.e7.s102502014;

import java.awt.*;
import javax.swing.*;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	JLabel[][] lb = new JLabel[10][10];

	MyPanel() {
		setLayout(null);
		setBounds(0, 0, 500, 500);
		setSize(500, 500);
		for (int i = 0; i < 10; i++)   //初始化物件陣列
			for (int j = 0; j < 10; j++)
				lb[i][j] = new JLabel();
		for (int i = 0; i < 10; i++) { //上色
			for (int j = 0; j < 10; j++) {
				lb[i][j].setSize(50, 50);
				if (i % 2 == 0) {
					if (j % 2 == 0) {
						lb[i][j].setBackground(Color.WHITE);
					} else {
						lb[i][j].setBackground(Color.BLACK);
					}
				} else {
					if (j % 2 == 0) {
						lb[i][j].setBackground(Color.BLACK);
					} else {
						lb[i][j].setBackground(Color.WHITE);
					}
				}
				lb[i][j].setOpaque(true);
			}
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				lb[i][j].setLocation(i * 50, j * 50);
				add(lb[i][j]);
			}
		}
	}
}
