package ce1002.e7.s102502014;

import java.awt.*;
import javax.swing.*;
import javax.swing.JFrame;

import ce1002.e7.s102502014.MyPanel;

public class MyFrame extends JFrame {
	// public MyPanel p1;
	MyFrame() {
		MyPanel p1 = new MyPanel();
		setLayout(null);    //非預設
		setSize(500, 500);
		add(p1); //加入FRAME
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);   
	}
}
