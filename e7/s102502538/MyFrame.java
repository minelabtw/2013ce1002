package ce1002.e7.s102502538;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame(){
		
		setLayout(null);
		setBounds(0, 0, 500, 500);
		MyPanel panel = new MyPanel();
		add(panel);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
	}
	
}
