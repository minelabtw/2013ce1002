package ce1002.e7.s102502538;

	import java.awt.Color;
	import java.awt.Graphics;

	import javax.swing.JPanel;

	public class MyPanel extends JPanel{

		MyPanel()
		{
			setBounds(0,0,500,500);
		}
		
		protected void paintComponent(Graphics g) {
			 	 
				for(int i=0;i<500;i+=50){
					for(int j=0;j<500;j+=50){
						if(i%20==j%20){
							g.setColor(Color.black);
							g.fillRect(i, j, 50 ,50);
						}
						else {
							g.setColor(Color.white);
							g.fillRect(i, j, 50, 50);
						}
					}
				}
		 }
	}
