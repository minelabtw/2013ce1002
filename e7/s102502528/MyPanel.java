package ce1002.e7.s102502528;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	MyPanel() {
		setBounds(0, 0, 500, 500);
	}

	public void paintComponent(Graphics g) {    //override to draw rectangle 
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 1)
					g.setColor(Color.black);    //set it black when it is odd
				else
					g.setColor(Color.white);    //else, white
				g.fillRect(j * 50, i * 50, 50, 50);
			}
		}
	}

}
