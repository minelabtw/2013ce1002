package ce1002.e7.s102502528;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	MyFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE); //......
		setLayout(null);
		setSize(520, 540);
		setLocationRelativeTo(null);

		JPanel Panel = new MyPanel(); // add panel
		add(Panel);

		setVisible(true);  // make it visible
	}
}
