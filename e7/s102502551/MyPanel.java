package ce1002.e7.s102502551;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	MyPanel()
	{
		setBounds(0,0,500,500); //設定Panel大小和預設位置
		setLayout(new GridLayout(10,10)); //設定panel內部元素的排版方式
	}
	
	protected void paintComponent( Graphics g )
	{
		super.paintComponent(g); 
		
		int width = getWidth(); //儲存panel寬度
		int height = getHeight(); //儲存panel高度
		
		for( int y = 0 ; y < 10 ; y++ ) //用for迴圈判斷該畫空心矩形或實心矩形，分別用drawRect和fillRect函數
		{
			for ( int x = 0 ; x < 10 ; x++ )
			{
				if( y % 2 == 0 )
				{
					if ( x % 2 == 0 )
					{
						g.setColor(Color.white);
						g.fillRect(x * 50, y * 50, width / 10, height / 10);
					}
					else
					{
						g.setColor(Color.black);
						g.fillRect(x * 50, y * 50, width / 10, height / 10);
					}
				}
				else
				{
					if ( x % 2 == 0 )
					{
						g.setColor(Color.black);
						g.fillRect(x * 50, y * 50, width / 10, height / 10);
					}
					else
					{
						g.setColor(Color.white);
						g.fillRect(x * 50, y * 50, width / 10, height / 10);
					}
				}
			}
		}	
	}
}
