package ce1002.e7.s102502551;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyFrame ()
	{
		this.setSize(516,538); //設定視窗大小
		this.setLocationRelativeTo(null); //設定視窗位置
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //設定按"X"會完整關閉程式
		this.setLayout(null); //設定物件的排版方式
		MyPanel panel = new MyPanel(); //建立一個名為panel的MyPanel型態的Object
		this.add(panel); //把設定好的panel增加到視窗裡
		this.setVisible(true); //使視窗會顯示在螢幕上
	}
}
