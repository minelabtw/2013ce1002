package ce1002.e7.s102502511;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	MyPanel(){
		setSize(500,500); //設panel大小
	}
	
	public void paintComponent(Graphics g){ //graphics重要 作圖與上色用
		
		for(int i = 0 ; i < 10 ; i++){ 
			for(int j = 0 ; j < 10 ; j++){
				if( (i+j) % 2 == 0){ //當兩數相加為偶數時 白色
						g.setColor(Color.WHITE); //要先寫顏色 在寫要見的方格，不然會上色到前一個
						g.fillRect(i * 50 ,j * 50 , 50, 50); //格子大小 和坐標					
				}
				else{
					g.setColor(Color.BLACK); //當兩數相加為基數時 黑色
					g.fillRect(i * 50 ,j * 50 , 50, 50);
				}
			}
		}
	}
}

