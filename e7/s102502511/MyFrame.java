package ce1002.e7.s102502511;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	protected MyPanel panel = new MyPanel(); //建立panel建構值
	
	MyFrame(){
		setLayout(null);
		setBounds(300,50,550,550); //位置和大小
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); //關閉
		add(panel);
	}
}
