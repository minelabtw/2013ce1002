package ce1002.e7.s102502023;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame {
  public MyFrame() {
	  
	  MyPanel panel = new MyPanel(); // initialize MyPanel panel
	  setBounds(0, 0, 500, 500); // set a frame with dimension(500, 500)
	  add(panel); // add the panel
  }
}
