package ce1002.e7.s102502023;

import javax.swing.*;

public class E7 {
  public static void main(String[] args) {
	  MyFrame frame = new MyFrame();
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setVisible(true);
	  frame.setLocationRelativeTo(null); // center the frame
	  frame.setTitle(null);
  }
}
