package ce1002.e7.s102502023;

import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel {
	
	public MyPanel() {
        // Consturctor
	} 
  @Override
  protected void paintComponent(Graphics g) {
	  super.paintComponent(g);
	  //g.setColor(Color.BLACK);
	  //g.fillRect(0, 0, 50, 50);
	  for(int i = 0; i < 10; i++) 
		  for(int j = 0; j < 10; j++) {
              if ((i % 2 == 0) && (j % 2 != 0))  {
            	  g.setColor(Color.BLACK);
            	  g.fillRect(i * 50, j * 50, 50, 50);
            	  }
              else if ((i % 2 != 0) && (j % 2 == 0)) {
            	  g.setColor(Color.BLACK);
            	  g.fillRect(i * 50, j * 50, 50, 50);
              }
              else {
            	  g.setColor(Color.WHITE);
            	  g.fillRect(i * 50, j * 50, 50, 50);
              }
			  
		  }
  } // draw some rectangles
}

