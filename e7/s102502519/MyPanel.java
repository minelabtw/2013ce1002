package ce1002.e7.s102502519;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JPanel b = new JPanel();

	MyPanel() {
		setBounds(0, 0, 500, 500);
	}

	public void paint(Graphics g) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 0)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);

				g.fillRect(i * 50, j * 50, 50, 50);
			}
		}
	}

}
