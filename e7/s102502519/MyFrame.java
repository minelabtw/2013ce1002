package ce1002.e7.s102502519;

import javax.swing.*;

public class MyFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	MyFrame() {
		setSize(500, 500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		MyPanel panel = new MyPanel();
		add(panel);
	}

}
