package ce1002.e7.s101201504;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame {
private MyPanel panel;
	
	public MyFrame()    //constructure
	{	
		panel = new MyPanel();
		setSize(515,535) ;              //set size
		setLayout(null) ;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		setVisible(true);
	}
	public void draw(){
		panel.setBounds(0, 0, 500, 500);
		add(panel);
	}
	
}
