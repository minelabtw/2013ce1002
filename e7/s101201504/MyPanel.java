package ce1002.e7.s101201504;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyPanel extends JPanel {
  public void paint(Graphics g)
	{
		int block = 0;
		super.paint(g);            //draw
		for(int i = 0 ; i < 10 ; ++i)
		{
			for(int j = 0 ; j < 10 ; ++j)
			{
				if(block %2 == 0)
					g.setColor(Color.white);      //white
				else
					g.setColor(Color.black);       //black
				g.fillRect(j * 50, i * 50, 50, 50);
				
				++block;
			}
			++block;
		}
	}	
  }