package ce1002.e7.s102502543;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	MyPanel() {
		setBounds(0, 0, 500, 500);
		setBackground(Color.BLACK);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		for (int b = 0; b <= 450; b = b + 50)
			for (int a = (b % 100); a <= 400 + (b % 100); a = a + 100)
				g.fillRect(a, b, 50, 50);

	}

}
