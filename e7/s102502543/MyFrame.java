package ce1002.e7.s102502543;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyFrame() {
		setLayout(null);
		MyPanel panel = new MyPanel();
		add(panel);
		setBounds(0, 0, 520, 540);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
