package ce1002.e7.s102502001;
import javax.swing.*;
import java.awt.*;
	
public class MyPanel extends JPanel{
	
	 MyPanel(){
		 setLayout(null);
		 setBounds(0, 0, 500, 500);  
	 }
	 
	 protected void paintComponent(Graphics p){
		 super.paintComponent(p);
		 for(int i=0;i<10;i++){
				for(int j=0;j<10;j++){
					if((i+j)%2==0){
						p.drawRect((50*i),(50*j),50,50);
					}
					if((i+j)%2==1){
						p.setColor(Color.BLACK);
						p.fillRect((50*i),(50*j),50,50);
					}
				}
				
			}
	 }
}