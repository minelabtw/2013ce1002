package ce1002.e7.s102502555;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyPanel mypanel = new MyPanel();
	
	MyFrame(){
		setLayout(null);  //設排版
		setSize(519 , 540);  //設大小
		add(mypanel);  //把panel加進去
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

}
