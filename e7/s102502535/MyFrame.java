package ce1002.e7.s102502535;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {

	MyFrame() {
		setLayout(null);
		setBounds(0, 0, 500, 500);
		setVisible(true);
		chess();
	}

	public void chess() {
		MyPanel panel = new MyPanel();
		panel.setBounds(0, 0, 500, 500);
		panel.setVisible(true);
		add(panel);
	}
}
