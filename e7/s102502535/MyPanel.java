package ce1002.e7.s102502535;

import java.awt.*;

import javax.swing.*;

public class MyPanel extends JPanel {

	MyPanel() {
		setLayout(null);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int i = 0; i < 11; i++) {
			for (int j = 0; j < 11; j++) {

				if ((i + j) % 2 == 0) {
					g.setColor(Color.white);
					g.fillRect(i * 50, j * 50, 50, 50);
				}// 白色矩形
				else if ((i + j) % 2 == 1) {
					g.setColor(Color.black);
					g.fillRect(i * 50, j * 50, 50, 50);
				}// 黑色矩形
			}
		}
	}

}
