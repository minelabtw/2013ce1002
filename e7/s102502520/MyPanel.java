package ce1002.e7.s102502520;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
public class MyPanel extends JPanel{
	MyPanel()
	{
		setLayout(null);
		setBounds(0,0,500,500);
	}	
	public void paint(Graphics g)
	{
		for (int i = 0 ; i < 500; i += 50)
			for(int j = 0 ; j < 500 ; j += 50)
			{
				//set Color
				if (i % 20 == j % 20)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);
				//Bounds
				g.fillRect(i, j, 50, 50);
			}
	}
}