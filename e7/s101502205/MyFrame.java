package ce1002.e7.s101502205;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	public MyFrame() {
		// Set size, layout, ......
		setSize(550, 550);
		setLayout(null);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE); 
		setLocationRelativeTo(null);
		setVisible(true);
		
		// Create a new panel
		MyPanel panel = new MyPanel();
		panel.setBounds(0, 0, 500, 500);	// Set location and size
		add(panel);		// add to frame
	}
}
