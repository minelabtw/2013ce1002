package ce1002.e7.s101502205;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel{
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for(int i=0; i<10; i++){
			for(int j=0; j<10; j++){
				// Decide which color to fill with
				if((i+j)%2==0){
					g.setColor(Color.white);	// set color as white
				}else{
					g.setColor(Color.black);	// set color as black
				}
				// Draw a filled Rectangle at location (x, y) = (i*50, j*50)
				// with size 50x50
				g.fillRect(i*50, j*50, 50, 50);				
			}
		}
	}
}
