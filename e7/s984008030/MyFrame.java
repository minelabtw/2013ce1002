package ce1002.e7.s984008030;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	private MyPanel chessboard;//欲畫出棋盤之panel
	
	public MyFrame() {
		chessboard = new MyPanel();//初始化chessboard
		setLayout(null);
		setBounds(0, 0, 550, 550);//設定MyFrame位置在(0, 0) 大小為550*550
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(chessboard);//將chessboard加入MyFrame
	}
	
}
