package ce1002.e7.s984008030;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	public MyPanel() {
		setBounds(0, 0, 500, 500);//設定MyPanel位置在(0, 0) 大小為500*500
	}
	
	public void paintComponent(Graphics g) {
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 0) {//設定顏色
					g.setColor(Color.white);
				}
				else {
					g.setColor(Color.black);
				}
			    g.fillRect(i * 50, j * 50, 50, 50);//畫出正方形
			}
		}
	  }
	
}
