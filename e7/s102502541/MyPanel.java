package ce1002.e7.s102502541;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
public class MyPanel extends JPanel{
	public MyPanel()
	{
		setBounds(0, 0, 500, 500);
		setForeground(new Color(0,0,0) );
		setBackground(new Color(255,255,255));
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for(int i = 0; i<5; i++)
		{
			for(int j = 0; j<5; j++)
			{
				g.fillRect(100*i,50+100*j,50,50);//奇數排
				g.fillRect(50+100*i,100*j,50,50);//偶數排
			}
			
		}
	}
}
