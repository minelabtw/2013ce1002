package ce1002.e7.s101201506;

import java.awt.*;
import javax.swing.*;


public class MyPanel extends JPanel {

	 public void paint(Graphics g)
		{
			int block = 0;
			//畫圖
			super.paint(g);    
			
			for(int i = 0 ; i < 10 ; ++i)
			{
				for(int j = 0 ; j < 10 ; ++j)
				{
					//白色區
					if(block %2 == 0)
						g.setColor(Color.white);      
					//黑色區
					else
						g.setColor(Color.black);       
					g.fillRect(j * 50, i * 50, 50, 50);
					
					++block;
				}
				++block;
			}
		}	
	  }
	
