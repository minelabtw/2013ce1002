package ce1002.e7.s102502033;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel
{
	MyPanel()
	{
		setBounds(0, 0, 500, 500);//size
		setBackground(Color.white);//backgroundcolor so that  we can only draw balck rec
	}

	protected void paintComponent(Graphics g)
	{
		int x = 50;
		int y = 0;
		super.paintComponent(g);
		g.setColor(Color.black);
		for (int i = 1; i <= 5; i++)//white first
		{
			for (int j = 1; j <= 5; j++)
			{
				g.fillRect(x, y, 50, 50);
				x = x + 100;
			}
			y = y + 100;
			x = 50;
		}
		x = 0;
		y = 50;
		for (int i = 1; i <= 5; i++) //black first
		{
			for (int j = 1; j <= 5; j++)
			{
				g.fillRect(x, y, 50, 50);
				x = x + 100;
			}
			y = y + 100;
			x = 0;
		}

	}

}
