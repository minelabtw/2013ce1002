package ce1002.e7.s102502035;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	public MyPanel() {
		setBounds(0, 0, 500, 500);
		setVisible(true);
	}

	public void paint(Graphics g) {// ��X��
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				g.setColor(Color.white);
				g.fillRect(j * 100, i * 100, 50, 50);
			}
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				g.setColor(Color.white);
				g.fillRect(j * 100 + 50, i * 100 + 50, 50, 50);
			}
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				g.setColor(Color.black);
				g.fillRect(j * 100 + 50, i * 100, 50, 50);
			}
		}
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				g.setColor(Color.black);
				g.fillRect(j * 100, i * 100 + 50, 50, 50);
			}
		}
	}
}
