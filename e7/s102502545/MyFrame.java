package ce1002.e7.s102502545;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame{
	
	
	public MyFrame() {//建構子
		setSize(550,550);//利用frame設定外框大小
		setLocationRelativeTo(null);
        
		MyPanel P = new MyPanel();//設定圖片
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
		add(P);//增加圖片
	
        setVisible(true);

	}

}
