package ce1002.e7.s102502545;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel {

	public MyPanel() {
          setBounds(0,0,500,500);
	}
	public void paintComponent(Graphics g) {//圖片組成
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				g.drawRect(0, 0, 50, 50);
				if((i+j)%2==0){
					g.setColor(Color.white);
					g.fillRect(i*50, j*50, 50, 50);
				}
				else
				{
					g.setColor(Color.black);
					g.fillRect(i*50, j*50, 50, 50);
				}
			}
		}// 排版

	}

}
