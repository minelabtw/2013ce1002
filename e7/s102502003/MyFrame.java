package ce1002.e7.s102502003;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame {
	
	MyPanel panel = new MyPanel();
	
	public MyFrame(){
		
		setVisible(true);
		setSize(500,500);
		panel.setBounds(0, 0, 500, 500);
	
	}
	
	public void paint(Graphics g){
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 500, 500);
		g.setColor(Color.BLACK);
		for(int i=1; i<=10; i++)
		{
			if(i%2!=0)
			{				
				for(int j=1; j<=10; j++)
				{
					if(j%2==0)
					{
						g.fillRect((j-1)*50,(i-1)*50,50,50);
					}
				}
			}
			else
			{
				for(int j=1; j<=10; j++)
				{
					if(j%2!=0)
					{
						g.fillRect((j-1)*50,(i-1)*50,50,50);
					}
				}
			}
					

		}
	}

}
