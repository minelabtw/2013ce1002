package ce1002.e7.s102502549;

import javax.swing.JFrame;

public class E7 {
	public static void main(String[] args) {
		MyFrame frame = new MyFrame();

		frame.setSize(550, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
