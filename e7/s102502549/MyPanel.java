package ce1002.e7.s102502549;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {

	public MyPanel() {
		setBounds(0, 0, 500, 500);// 設定panel大小
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				// 判斷i+j的奇偶來決定顏色
				if ((i + j) % 2 == 0) {
					g.setColor(Color.WHITE);
					g.fillRect(50 * j, 50 * i, 50, 50);
				} else {
					g.setColor(Color.BLACK);
					g.fillRect(50 * j, 50 * i, 50, 50);
				}
			}
		}
	}
}
