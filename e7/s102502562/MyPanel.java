package ce1002.e7.s102502562;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	MyPanel()
	{
		setLayout(null);
	}
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.WHITE);//先做一個大的白色矩形
		g.fillRect(0,0,500,500);
		for(int i=0;i<10;i++)//填上黑色矩形
		{
			for(int j=0;j<10;j++)
			{
				if((i+j)%2==1)
				{
					g.setColor(Color.BLACK);
					g.fillRect(i*50,j*50,50,50);
				}
			}
		}
	}
}
