package ce1002.e7.s102502032;

import java.awt.*;

import javax.swing.*;

public class MyPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	// constructor
	public MyPanel()
	{
		this.setBounds(0, 0, 500, 500);
		this.setVisible(true);
		this.setBackground(Color.white);
	}

	// method
	@Override		//draw grids
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.black);
		for (int i = 0; i < 500; i += 50)
		{
			for (int j = 0; j < 500; j += 50)
			{
				if (1 == ((i + j) / 50) % 2)
					g.fillRect(i, j, 50, 50);
			}
		}
	}
}
