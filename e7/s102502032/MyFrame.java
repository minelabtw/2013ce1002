package ce1002.e7.s102502032;

import javax.swing.*;

public class MyFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private MyPanel				grid				= new MyPanel();

	// constructor
	public MyFrame()
	{
		this.setSize(550, 550);
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
		this.add(grid);
	}
}
