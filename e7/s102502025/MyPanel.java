package ce1002.e7.s102502025;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	public MyPanel() {
		setLayout(null);//面板
		setBounds(0, 0, 500, 500);//setting panel size
		setVisible(true);//可以看得到
	}

	public void paintComponent(Graphics g) {//開啟一個圖形

		int color = 0;//設一變數

		for (int k = 0; k < 10; k++) {//畫西洋棋版
			for (int l = 0; l < 10; l++) {
				if ((color %=2)==0) {
					g.setColor(Color.white);
					g.fillRect(l * 50, k * 50, 50, 50);
				}

				else if((color %=2)!=0){
					g.setColor(Color.black);
					g.fillRect(l * 50, k * 50, 50, 50);
				}

				color++;
			}
			color ++;
		}
	}
}
