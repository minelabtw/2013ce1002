package ce1002.e7.s102502038;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
    @Override public void paintComponent(Graphics g) {//override paintComponent function
    	super.paintComponent(g);
		setBounds(0,0,500,500);
		setBackground(Color.BLACK);
    	g.setColor(Color.WHITE);
    	for(int i = 0;i<10;i++){//fill lots of rect
    		for(int j = 0;j<10;j++){
    			if((i+j)%2 == 0){
    				g.fillRect(i*50,j*50,50,50); //fill a rect
    			}
    		}
    	}
    }
}
