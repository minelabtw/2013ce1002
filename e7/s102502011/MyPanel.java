package ce1002.e7.s102502011;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	
	MyPanel()
	{
		setBounds(0,0,500,500) ;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		for(int x=0;x<10;x++) {
			for(int y=0;y<10;y++) {
				if ( (x+y)%2==0 )
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				
				g.fillRect(x*50, y*50, 50, 50);
			}
		}
	}

	
	
	
}
