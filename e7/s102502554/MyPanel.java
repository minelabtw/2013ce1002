package ce1002.e7.s102502554;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	
	MyPanel(){
		setBounds(0, 0, 500, 500);//set the bounds of the panel
		setLayout(new GridLayout(10,10));//use  "GridLayout" to draw the chess board
	}
	
	    @Override 
	    public void paintComponent (Graphics g){
		
		super.paintComponent(g);
		g.setColor(Color.black);//let the color of "filldraw" be black
		
		int width = getWidth();
		int height = getHeight();//get the width and length from the bounds
		
		for (int i = 0 ; i < 10 ; i++){
			if (i % 2 == 0){
			for (int j = 0 ; j < 10 ; j ++){
				if (j % 2 == 0)
					g.drawRect(i*50, j*50, width/10, height/10);//draw the rectangle which is not  filled
				if (j % 2 == 1)
					g.fillRect(i*50, j*50, width/10, height/10);////draw the rectangle which is filled
			}
			}
			if (i % 2 == 1){
				for (int j = 0 ; j < 10 ; j ++){
					if (j % 2 == 0)
						g.fillRect(i*50, j*50, width/10, height/10);
					if (j % 2 == 1)
						g.drawRect(i*50, j*50, width/10, height/10);
				}
			}
		
		};//draw the chess board
		
	}
}


