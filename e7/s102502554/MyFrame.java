package ce1002.e7.s102502554;

import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame()
	{
		MyPanel Panel = new MyPanel ();//crate a new object of MyPanel
		
		setLayout(null);//use the default layout
		setBounds(0 , 0 , 550 , 550);//set bounds
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(Panel);//plus the Panel in the frame
		setVisible(true);//let the picture be visible 
	}

}
