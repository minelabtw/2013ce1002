package ce1002.e7.s102502501;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class MyPanel extends JPanel
{
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				if((i+j)%2==0)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				g.fillRect(50*i , 50*j , 50, 50);
			}
		}
	}

}