package ce1002.e7.s102502034;

import javax.swing.JFrame;
import java.awt.event.*; 

public class MyFrame extends JFrame{
	MyFrame(){
		//set frame and add frame
		setSize(600,600);
		MyPanel panel = new MyPanel();
		add(panel);
		setVisible(true);
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
		     System.exit(0);
		    }
		       });
	}

}
