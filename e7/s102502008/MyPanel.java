package ce1002.e7.s102502008;
import java.awt.Graphics;
import java.awt.Color ;
import javax.swing.JPanel ;
public class MyPanel extends JPanel
{

	public MyPanel()
	{
		setBounds(0,0,500,500) ;
	}
	@Override
	  protected void paintComponent(Graphics g) 
	  {
	    super.paintComponent(g);
	    for(int i= 0; i<10 ; i++)
	    {
	    	for(int j=0; j<10 ;j++)
	    	{
	    		if((i+j)%2==0)
	    		{	
	    			g.setColor(Color.WHITE);
	    			g.fillRect(i*50, j*50, 50, 50) ;
	    		}
	    		else
	    		{
	    			g.setColor(Color.BLACK);
	    			g.fillRect(i*50, j*50, 50, 50) ;
	    		}
	    		
	    	}
	    }

	  }
}
