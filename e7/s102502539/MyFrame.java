package ce1002.e7.s102502539;

import javax.swing.*;

public class MyFrame extends JFrame
{
	protected MyPanel panel = new MyPanel();
	
	MyFrame()
	{
		setLayout(null);
		setBounds(100,100,520,540);	//設定視窗邊界
		setVisible(true);	//顯示物件
		setDefaultCloseOperation(EXIT_ON_CLOSE);	//一般關閉
		add(panel);	//面板
	}
}
