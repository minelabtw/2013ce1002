package ce1002.e7.s102502539;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel
{	
	MyPanel()
	{
		setLayout(null);
		setBounds(0,0,500,500);
	}
		
	public void paintComponent(Graphics g)
	{
		for ( int i = 0 ; i < 500 ; i += 50 )
			for ( int j = 0 ; j < 500 ; j += 50 )
			{
				//填入顏色
				if ( i % 20 == j % 20 )
					g.setColor(Color.white);
				else	
					g.setColor(Color.black);
				//畫出線段
				g.fillRect(j, i, 50, 50);	
			}
	}
}
