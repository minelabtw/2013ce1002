package ce1002.e7.s101201521;
import javax.swing.*;
public class MyFrame extends JFrame{
	public MyFrame(){
		//create panel
		MyPanel panel = new MyPanel();
		//add panel to frame
		add(panel);
		//setting frame status
		setSize( 550, 550);
		setLocation( 0, 0);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
