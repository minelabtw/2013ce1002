package ce1002.e7.s101201521;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{
	public MyPanel(){
		//setting panel status;
		setBounds( 0, 0, 500, 500);
	}
	//painting on the panel
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		//painting chess board
		for(int i = 0; i<10; i++){
			for(int j = 0; j < 10; j++){
				if(( i + j ) % 2 == 0)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				g.fillRect( i * 50, j * 50, 50, 50);
			
			}
		}
		
	}
	
}
