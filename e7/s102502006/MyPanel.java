package ce1002.e7.s102502006;

import javax.swing.JPanel;
import java.awt.*;

public class MyPanel extends JPanel{
	
	public MyPanel(){
		setLayout(null);
		setBounds(0, 0, 500, 500);		
	}
	
	protected void paintComponent(Graphics p){
		super.paintComponent(p);
		for(int i=0; i<10; i++){
			for(int j=0; j<10;j++){
				if((i+j)%2==0)
					p.setColor(Color.WHITE);
				else 
					p.setColor(Color.BLACK);
				    p.fillRect(i*50, j*50, 50, 50); // full rectangle
			}
		}
	}
}
