package ce1002.e7.s102502006;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	public MyFrame(){
		setLayout(null);
		setBounds(0 , 0 , 550 , 570);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		add(new MyPanel());
	}
}
