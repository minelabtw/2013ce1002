package ce1002.e7.s102502049;

import javax.swing.JFrame;

public class E7 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		frame.setSize(550, 550);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); // set frame and show picture
	}

}
