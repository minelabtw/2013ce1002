package ce1002.e7.s102502049;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	public MyPanel(){
		
	}
	@Override
	protected void paintComponent(Graphics g){ // draw picture
		super.paintComponent(g);	
		
		for(int i=0, ph = 0 ; i<10; i++,ph += 50){
			for(int pw = 0, j=0; j<10; j++,pw += 50){
				if(((i+j)%2)==0){
					g.setColor(Color.white);
					g.fillRect(pw, ph, 50, 50); // draw white
				}
				else{
					g.setColor(Color.black);
					g.fillRect(pw, ph, 50, 50); // fraw black
				}
			}
		}
	}
}
