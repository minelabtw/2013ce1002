package ce1002.e7.s102502544;

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class MyPanel extends JPanel{
	MyPanel(){
		setLayout(null); //設定排版
		setBounds(0,0,500,500); //設定邊界
		
	}
	public void paint(Graphics g){ //設定圖形
		for(int i=0 ; i<10 ; i++){
			for(int j=0 ; j<10 ; j++){
				if((i+j)%2==0){
					g.setColor(Color.WHITE);
					g.fillRect(i*50,j*50,50,50);
				}
				else{
					g.setColor(Color.BLACK);
					g.fillRect(i*50, j*50, 50, 50);
				}
			}
		}
	}
}
