package ce1002.e7.s102502544;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyPanel panel=new MyPanel();
	
	MyFrame(){
		setLayout(null); //設定排版
		setSize(550,550); //設定大小
		add(panel); //把panel加進frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

}
