package ce1002.e7.s102502517;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	public MyPanel() {
		setBounds(0, 0, 500, 500);
	}

	public void paint(Graphics g) {
		g.setColor(Color.BLACK); //設定顏色為黑色
		g.fillRect(0, 0, 500, 500);//填滿

		g.setColor(Color.WHITE);//設定顏色為白色
		for (int i = 0; i <= 9; i++) {
			for (int j = 0; j <= 9; j++) {
				if ((i + j) % 2 == 0)
					g.fillRect(50 * j, 50 * i, 50, 50); //填色
			}
		}
	}
}
