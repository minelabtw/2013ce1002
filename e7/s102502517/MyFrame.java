package ce1002.e7.s102502517;

import java.awt.Graphics;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel panel = new MyPanel();
	
	public MyFrame()
	{
		setLayout(null);
		setBounds(300,50,600,600);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		add(panel);
	}
}
