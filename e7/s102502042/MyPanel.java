package ce1002.e7.s102502042;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{
	MyPanel()
	{
		setBounds(0,0,500,500);	//set panel size
	}
	
	//paint the rectangle
	@Override public void paintComponent(Graphics g)
	{
		for(int i=0;i<10;++i)
		{
			for(int j=0;j<10;++j)
			{
				//set color
				if((i+j)%2==1)g.setColor(Color.black);
				else g.setColor(Color.white);
				g.fillRect(i*50, j*50, 50, 50);
			}
		}
	}
}
