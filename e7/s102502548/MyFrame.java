package ce1002.e7.s102502548;

import java.awt.*;
import javax.swing.* ;

public class MyFrame extends JFrame {
	MyFrame(){
		MyPanel panel=new MyPanel() ;
		
		getContentPane().setLayout(null) ;//把預設用掉
		
		setSize(600,600) ;
		
		add(panel) ;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;//案叉叉時關閉
		
		setVisible(true) ;//設定為看的見

	}
}
