package ce1002.e7.s102502548;

import java.awt.*;//引入awt的package

import javax.swing.* ;

public class MyPanel extends JPanel {//繼承JPanel
	MyPanel(){
		this.setBounds(0,0,500,500) ;
		
		setBackground(Color.WHITE) ;
	}
	@Override public void paintComponent(Graphics g){//複寫調JPanel的paintComponent
		super.paintComponent(g);
		
		g.setColor(Color.BLACK);
		
		for (int x=0;x<10;x++){
			for (int y=0;y<10;y++){
				if ((x+y)%2!=0){
					g.fillRect(x*50,y*50,50,50);//劃出長方形
				}
			}
		}
	}
	
	



}
