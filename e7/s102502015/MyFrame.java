package ce1002.e7.s102502015;

import java.awt.*;
import javax.swing.*;
import ce1002.e7.s102502015.MyPanel;
public class MyFrame extends JFrame {
	public MyPanel p1;

	MyFrame() {
		p1=new MyPanel();
		setLayout(null);
		setSize(500,500);
		add(p1);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

}
