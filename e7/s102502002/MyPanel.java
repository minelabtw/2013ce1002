package ce1002.e7.s102502002;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class MyPanel extends JPanel {

	MyPanel(){
		setBounds(0, 0, 500, 500); // set the bound
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		for(int i=0; i<10; i++){ // paint certain rectangles black
			for(int j=0;j<10;j++){
				g.setColor(Color.BLACK);
				g.fillRect(50*(i*2), 50*(j*2-1), 50, 50);
			}
		}
		
		for(int k=0; k<10; k++){
			for(int h=0;h<10;h++){
				g.setColor(Color.BLACK);
				g.fillRect(50*(k*2+1), 50*(h*2), 50, 50);
			}
		}
		
	}
}
