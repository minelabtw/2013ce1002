package ce1002.e7.s102502002;
import javax.swing.JFrame;

public class MyFrame extends JFrame {

	MyFrame(){
		MyPanel panel = new MyPanel(); // new a panel
		setSize( 520, 540);  // set size of the frame
		setLayout(null);
		setVisible(true); // set visibility
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel); //add the panel to frame
		
	}	
}
