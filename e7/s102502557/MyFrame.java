package ce1002.e7.s102502557;

import javax.swing.*;//引入swing的所有類別

public class MyFrame extends JFrame 
{
	MyFrame()
	{
		this.setLayout(null);//不用null的話會有一個預設的位置  對panel做排版
		setSize(526,548);
		setLocationRelativeTo(null);//視窗彈出位置 預設在正中間
		MyPanel panel = new MyPanel();
		add(panel);
        setVisible(true);//不要再忘記了
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     }
}
