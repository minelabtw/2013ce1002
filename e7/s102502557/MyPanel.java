package ce1002.e7.s102502557;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Graphics.*;

public class MyPanel extends JPanel
{
     MyPanel()
     {
    	 setBounds(0, 0, 500, 500);//前面代表起始點 後面代表大小
    	 setLayout(new GridLayout(10,10));//設定排版
    	 
     }
	
     protected void paintComponent(Graphics g)//為何要protected????
     {
    	 super.paintComponent(g);
    	 
    	 for(int h=0; h<10; h++)
    	 {
    		 for(int w=0; w<10; w++)
    		 {
    			 if( (w+h)%2==1 )
    			 {
    				 g.setColor(Color.BLACK);
    				 g.fillRect(0 + w*50 , 0 + h*50 , 50 , 50);//fill會有預設是黑色的
    			 }
    			 else
    			 {
    				 g.setColor(Color.WHITE);//要設定顏色要記得import awt
    				 g.fillRect(0 + w*50 , 0 + h*50 , 50 , 50);
    			 }
    	     }
    	 }
    	 
     }
}
