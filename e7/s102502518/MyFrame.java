package ce1002.e7.s102502518;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	protected MyPanel panel = new MyPanel();

	MyFrame() {
		
		setBounds(0, 0, 600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
		setVisible(true);
	}
}