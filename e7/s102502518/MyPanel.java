package ce1002.e7.s102502518;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;

public class MyPanel extends JPanel {
	public MyPanel() {
		setBounds(0, 0, 500, 500);
	}

	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 500, 500);
		g.setColor(Color.WHITE);
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 0)
					g.fillRect(50 * i, 50 * j, 50, 50);

			}
		}
	}
}
