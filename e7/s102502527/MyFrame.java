package ce1002.e7.s102502527;

import javax.swing.*;

class MyFrame extends JFrame{

    MyFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//將叉叉加入函示跳出
        setLayout(null);//將排版的方式以座標方式顯示
		setLocationRelativeTo(null);//置中之類的排版
       
        MyPanel panel = new MyPanel();
        add(panel);
        
        setSize(500, 540);//顯示視窗大小
        setVisible(true);//顯示視窗
    }
}
