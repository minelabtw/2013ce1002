package ce1002.e7.s102502527;

import java.awt.*;
import javax.swing.*;

class MyPanel extends JPanel {

    MyPanel () {
        setBounds(0, 0, 500, 500);//要顯示的起始點以及末點
    }
    
    public void paintComponent(Graphics g) { 
        for (int i=0 ; i<10 ; i++) {
            for (int j=0 ; j<10 ; j++) { 
                if ((i+j) % 2 == 1)//若相加是偶數則為白色,奇數則為黑色 
                    g.setColor(Color.black); //設定顏色
                else 
                    g.setColor(Color.white); 
                g.fillRect(j*50, i*50, 50, 50);//開頭2數為起始位置,j為橫向座標,i為直向座標 ;後兩樹為末點要填滿的座標
            } 
        }
    }
}
