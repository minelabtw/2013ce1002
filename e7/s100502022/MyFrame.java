package ce1002.e7.s100502022;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	private static final long serialVersionUID = 1L;
	//
	protected MyPanel myPanel = new MyPanel();
	public MyFrame(){
		setSize(600,600);
		add(myPanel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
