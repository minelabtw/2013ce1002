package ce1002.e7.s100502022;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	public MyPanel(){
		setBounds(0,0,500,500);
	}
	public void paintComponent(Graphics g){
		for(int x=0;x<10;x++){
			for(int y=0;y<10;y++){
				if((x+y)%2==0){
					g.setColor(Color.white);
					g.fillRect(x*50, y*50, 80, 80);
				}
				else{
					g.setColor(Color.black);
					g.fillRect(x*50, y*50, 50, 50);
				}
			}
		}
	}
     
}
