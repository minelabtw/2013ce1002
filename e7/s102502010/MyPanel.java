package ce1002.e7.s102502010;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel{
	MyPanel()
	{
		setLayout(null);
		setBounds(0,0,500,500);
	}
	public void paintComponent(Graphics g)
	{
		for(int i=0; i<500; i+=50){
			for(int j=0; j<500; j+=50){
			   if(i%100==0){
				   if(j%100==0)
				      g.setColor(Color.white);
				   else
					  g.setColor(Color.black);
			   }
			   else{
				   if(j%100==0)
				     g.setColor(Color.black);
				   else
					 g.setColor(Color.white);
			   }
			   g.fillRect(i, j, 50, 50);  //填入
			}
		}
	}
}
