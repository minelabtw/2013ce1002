package ce1002.e7.s102502515;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel {
	MyPanel(){
		setBounds(0, 0, 500, 500);//set panel size & location
	}
	
	public void paint(Graphics Rec){//draw RECs

		for (int i = 0 ; i < 10 ; i++)
		{
			for (int j = 0 ; j < 10 ; j++)
			{
				if ((i+j)%2 == 0)//this makes each row different color
				{
					Rec.setColor(Color.WHITE);//set rectangle WHITE color 
					Rec.fillRect(j*50,i*50,50,50);//create a rec with filled color
				}
				else
				{
					Rec.setColor(Color.BLACK);//set rec BLACK color
					Rec.fillRect(j*50,i*50,50,50);//create a rec with filled color
				}
			}
		}
		
		
	}
}
