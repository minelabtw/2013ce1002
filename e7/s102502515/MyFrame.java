package ce1002.e7.s102502515;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	MyFrame(){
		setLayout(null);//Layout
		setBounds(300, 50, 515, 538 );//frame size & location
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//close
		MyPanel panel = new MyPanel();//establish a MyPanel object
		add(panel);//add panel
	}
	
	
}
