package ce1002.e7.s102502524;

import javax.swing.*;

public class MyFrame extends JFrame {
	
	MyFrame()
	{
		setVisible(true);							//框架可見
		setSize(500, 500);							//框架大小為500*500
		setDefaultCloseOperation(EXIT_ON_CLOSE);	//關閉後離開
		
		MyPanel panel = new MyPanel();				//把圖形放入面板裡
		add(panel);									//加入的東西是MyPanel裡全部的東西

	}

}
