package ce1002.e7.s100204006;

import javax.swing.JFrame;

public class E7 
{
	public static void main(String[] args )
	{
		MyFrame frame = new MyFrame();
		
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle(null);
		frame.setVisible(true);
	}
}
