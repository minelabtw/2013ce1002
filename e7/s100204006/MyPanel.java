package ce1002.e7.s100204006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	public MyPanel()
	{
		this.setLayout(null);
		setBounds(0,0,500,500);
	}
	public void paintComponent(Graphics g)       //need call from main
	{                                        
	    super.paintComponent(g);
	   
	    for(int i=0;i<10;i++)
	    {
	    	for(int j=0;j<10;j++)
	    	{
	    		if( ((i+1)*9+(j+1)) %2==1)
	    		{
	    			 g.setColor(Color.black);
	    		}
	    		else 
	    		{
	    			g.setColor(Color.white);
	    		}
	    		g.fillRect(i*50, j*50, 50, 50);
	    	}
	    	
	    	
	    }
		
	}
}
