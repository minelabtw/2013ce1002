package ce1002.e7.s101303504;
import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel{
	
	public MyPanel(){
		setSize(500,500);		
	}
	public void paintComponent(Graphics w){
		for(int i = 0; i<10 ; i++){
			for(int j=0 ; j<10; j++)
			{
				if((i+j)%2==1){
					w.setColor(Color.BLACK);
					w.fillRect(50*i, 50*j, 50, 50);
				}
				else{
					w.setColor(Color.WHITE);
					w.fillRect(50*i, 50*j, 50, 50);
				}
			}
		}
		
	}
	
}
