package ce1002.e7.s102502048;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame
{
	protected MyPanel p0 = new MyPanel();//Create a new panel
	
	public MyFrame()
	{		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(0, 0, 550, 550);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		add(p0);//Add a panel
	}
}
