package ce1002.e7.s992001026;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	public void paint(Graphics g){
		super.paint(g);
		
		// overwrite
		for ( int i = 0 ; i < 10; i++){
			for (int  j = 0 ; j < 10 ; j++){
				if ((i+j)%2 == 0 ) g.setColor(Color.white);
				else g.setColor(Color.black);
				g.fillRect(i*50, j*50, 50, 50);
			}
		}
		
	}
	
}
