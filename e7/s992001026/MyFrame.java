package ce1002.e7.s992001026;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	
	public MyFrame(){
		// set a new panel
		MyPanel p = new MyPanel();
		//set the panel's position and bounds
		p.setBounds(0,0,500,500);
		// add the p (panel) into frame
		add(p);
		
		// set frame size and let it be visible
		setSize(530,550);
		setVisible(true);
	}

}
