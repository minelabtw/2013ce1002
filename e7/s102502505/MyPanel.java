package ce1002.e7.s102502505;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{

	protected JPanel mainpanel = new JPanel();
	
	/**
	 * @param args
	 */
	
	MyPanel()//panel建構子
	{
		mainpanel.setBounds(0,0,500,500);//設立邊界
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.WHITE);//設立一開始的顏色
		
		int i = 0;
		for(int j=0;j<10;j++)//用迴圈畫圖
		{
			while(i<10)
			{
				if((i+j)%2==0)
				{
					g.setColor(Color.WHITE);
					g.fillRect(i*50,j*50,50,50);
					i++;
				}
				else
				{
					g.setColor(Color.BLACK);
					g.fillRect(i*50,j*50,50,50);
					i++;
				}	
			}
			i=0;//歸零i，再跑環圈
		}
			
	}
}
	