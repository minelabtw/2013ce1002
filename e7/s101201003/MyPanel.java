package ce1002.e7.s101201003;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
public class MyPanel extends JPanel{
	
	public MyPanel(){
		setBounds(0,0,500,500);		
	}

	public void paintComponent(Graphics g){

		super.paintComponent(g);
		int times=0;
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				if(times%2==0)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);			
				g.fillRect(i*50,j*50,50,50);				
				times++;
			}
			times++;
		}
		
	}
}

