package ce1002.e7.s101201003;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	private MyPanel panel;
	
	public MyFrame(){
		
			panel = new MyPanel();
			setSize(550,550);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLayout(null);
			setVisible(true);
		
	}
	
	public void drawboard()
	{
		panel.setBounds(0, 0, 500, 500);
		add(panel);
	}
	

}
