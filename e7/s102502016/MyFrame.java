package ce1002.e7.s102502016;

import java.awt.*;
import javax.swing.*;
import ce1002.e7.s102502016.MyPanel;

public class MyFrame extends JFrame {
	public MyPanel mp;

	MyFrame() {//放在建構子內,則main function 宣告時可直接用
		mp = new MyPanel();
		setLayout(null);
		setSize(515, 540);
		add(mp);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}