package ce1002.e7.s102502016;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	JLabel[][] jlb = new JLabel[10][10];

	MyPanel() {
		setLayout(null);//不要預設
		setBounds(0, 0, 500, 500);
		setSize(500, 500);
		for (int i = 0; i < 10; i++)
			for (int j = 0; j < 10; j++)
				jlb[i][j] = new JLabel();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				jlb[i][j].setSize(50, 50);
				if (i % 2 == 0) {
					if (j % 2 == 0) {
						jlb[i][j].setBackground(Color.WHITE);//白色
					} else {
						jlb[i][j].setBackground(Color.BLACK);
					}
				} else {
					if (j % 2 == 0) {
						jlb[i][j].setBackground(Color.BLACK);
					} else {
						jlb[i][j].setBackground(Color.WHITE);
					}
				}
				jlb[i][j].setOpaque(true);//*穿透,才能印上顏色!!*
			}
		}
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				jlb[i][j].setLocation(i * 50, j * 50);//位置
				add(jlb[i][j]);//放上panel
			}
		}
	}
}