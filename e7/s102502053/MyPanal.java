package ce1002.e7.s102502053;

import javax.swing.*;
import java.awt.*;

public class MyPanal extends JPanel{
	
	protected JPanel panel1 = new JPanel();
	
	MyPanal()
	{
		setBounds(0, 0, 500, 500);
		setLayout(null);
		
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		for(int a = 0; a<10; a++)
		{
			for(int b = 0; b<10; b++)
			{
				if(a % 2 ==0)
				{	
					if(b % 2 == 0)
					{
						g.setColor(Color.WHITE);
						g.fillRect(a*50, b*50, 50, 50);
					}
					if(b % 2 != 0)
					{
						g.setColor(Color.BLACK);
						g.fillRect(a*50, b*50, 50, 50);
					}
				}
				if(a % 2 != 0)
				{
					if(b % 2 != 0)
					{
						g.setColor(Color.WHITE);
						g.fillRect(a*50, b*50, 50, 50);
					}
					if(b % 2 == 0)
					{
						g.setColor(Color.BLACK);
						g.fillRect(a*50, b*50, 50, 50);
					}
				}
			}
		}
	}
	

}
