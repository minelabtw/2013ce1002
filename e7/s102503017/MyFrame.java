package ce1002.e7.s102503017;

import javax.swing.*;

public class MyFrame extends JFrame
{
	protected MyPanel panel = new MyPanel();
	
	MyFrame()
	{
		//set up the frame basic parameters.
		setLayout(null);
		setBounds(0, 0, 600, 600);
		add(panel);
		setVisible(true);
	}
}
