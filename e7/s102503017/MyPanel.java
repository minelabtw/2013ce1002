package ce1002.e7.s102503017;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel
{
	
	JPanel board = new JPanel();
	//GridLayout grid = new GridLayout(10,10);
	
	MyPanel()
	{
		//setup the panel parameters.
		setLayout(null);
		setBounds(0,0,500,500);
		setBackground(Color.white);
		//setLayout(grid);
		/*for(int i = 0; i < 100 ; i++)
		{
			JPanel block = new JPanel();
			block.setBounds(0, 0, 50, 50);
			add(block);
			
			int row = (i / 10) %2;
			if(row == 0)
			{
				block.setBackground(i % 2 == 0 ? Color.white : Color.black);
			}
			else
			{
				block.setBackground(i % 2 == 0 ? Color.black : Color.white);
			}
			
			
		}	*/
	}
	
	//using paintComponent to print the black blocks.
	protected void paintComponent(Graphics g)
	{
		//determinate where should print the blocks.
		for(int i = 0; i < 10 ; i++)
		{
			for(int j = 0; j < 10; j++)
			{				
				if(i % 2 == 0 && j % 2 == 1)
				{
					g.fillRect(j * 50, i * 50, 50, 50);
				}
				if(i % 2 == 1 && j % 2 == 0)
				{
					g.fillRect(j * 50, i * 50, 50, 50);
				}
			
			}
			
		}
	}
}
