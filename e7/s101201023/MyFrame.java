package ce1002.e7.s101201023;

import javax.swing.JFrame;
import java.awt.Graphics;

class MyFrame extends JFrame
{
	//set frame
	public MyFrame()   
	{
		setSize(600 , 600);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}	
	
	//add MyPanel to frame
	public void paintshow()
	{
		add(new MyPanel());
	}
}