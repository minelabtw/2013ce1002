package ce1002.e7.s101201023;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Graphics;

public class MyPanel extends JPanel
{
	//set bound of panel
	public MyPanel()
	{
		setBounds(0,0,500,500);
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		for(int j=0 ; j < 10 ; j++)
		{
			for(int i=0 ; i < 10 ; i++)
			{
				//decide color
				if((i+j)%2==0)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				
				//distribute shape
				g.fillRect(50*i , 50*j , 50 , 50);
			}
		}
		
		
	}
}
