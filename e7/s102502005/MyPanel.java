package ce1002.e7.s102502005;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	MyPanel() {
		this.setLayout(null);
		this.setBackground(Color.WHITE);//將背景色設為白色的話，要做出棋盤只要填上黑格子就好了。
		this.setBounds(0, 0, 500, 500);

		for (int i=50,k=0; i<500 && k<500 ; i=i+100,k=k+100) {//畫上黑格子。
			for (int j=0,l=50; j<500 && l<500; j=j+100,l=l+100) {
				this.add(new block(i, j));
				this.add(new block(k,l));
			}
		}

	}
}

class block extends JPanel {//要用JPanel當作畫布，畫出一個填滿畫布的黑色正方形，再把一個一個畫布貼在MyPanel上。

	public block(int x, int y) {
		this.setBounds(x, y, 50, 50);
	}

	protected void paintComponent(Graphics g) {//在每一個JComponent要放進JFrame或Jpanel時，
		super.paintComponent(g);			   //程式都會呼叫JComponet的paintComponent畫出這個JComponent
		g.setColor(Color.BLACK);			   //現在我們覆寫 paintComponent，讓paintComponent畫我們指定的圖形(在這裡是g)。
		g.fillRect(0, 0, 50, 50);
	}
}
