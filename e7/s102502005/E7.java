package ce1002.e7.s102502005;

import javax.swing.JFrame;

public class E7 {

	public static void main(String[] args) {
		
		MyFrame frame = new MyFrame();
		
		frame.setLayout(null);
		frame.add(new MyPanel());//這次直接在MyPanel的建構子把Panel裡的東西畫好。
		
		frame.setSize(516, 538);//frame的size要從最外框起算(包含最小化、最大化、叉叉)。
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
