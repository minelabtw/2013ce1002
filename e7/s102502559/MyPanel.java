package ce1002.e7.s102502559;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyPanel extends JPanel {
	MyPanel() {
		setLayout(null);
		setBounds(0, 0, 500, 500);
	}
	//由奇數偶數行和烈,填入黑色或白色
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int height = 0; height < 10; height++) {
			for (int width = 0; width < 10; width++) {
				if (height % 2 == 0) {
					if (width % 2 == 1) {
						g.setColor(Color.BLACK);
						g.fillRect(width * 50, height * 50, 50, 50);
					} else {
						g.setColor(Color.WHITE);
						g.fillRect(width * 50, height * 50, 50, 50);
					}
				} else {
					if (width % 2 == 0) {
						g.setColor(Color.BLACK);
						g.fillRect(width * 50, height * 50, 50, 50);
					} else {
						g.setColor(Color.WHITE);
						g.fillRect(width * 50, height * 50, 50, 50);
					}
				}
			}
		}

	}
}
