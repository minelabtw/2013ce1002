package ce1002.e7.s102502559;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyPanel panel = new MyPanel();

	MyFrame() {
		JFrame frame = new JFrame("Exercise");
		frame.setSize(519, 540);//設定frame大小
		frame.setLocationRelativeTo(null);//設定視窗起始位置
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定關閉執行的動作
		frame.setVisible(true);//設定可見
		frame.add(panel);//將panel加入frame

	}
}
