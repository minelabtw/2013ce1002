package ce1002.e7.s102502024;
import java.awt.*;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
	public  int x=0;
	public  int y=0;
	public MyPanel(){
		
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		while(y<=10)
		{
			while(x<=10)
			{
				if(y%2!=0)
				{
					if(x%2!=0)
					{
						g.setColor(Color.white);
						g.fillRect(x*50, y*50, 50, 50);
					}
					else
					{
						g.setColor(Color.black);
						g.fillRect(x*50, y*50, 50, 50);
					}
				}
				else
				{
					if(x%2==0)
					{
						g.setColor(Color.white);
						g.fillRect(x*50, y*50, 50, 50);
					}
					else
					{
						g.setColor(Color.black);
						g.fillRect(x*50, y*50, 50, 50);
					}
				}
				x++;
			}
			x=0;
			y++;
		}
	}
}
