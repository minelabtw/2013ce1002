package ce1002.e7.s102502513;

import javax.swing.*;

public class MyFrame  extends JFrame 
{
	MyFrame() 
	{
		setLayout(null);
		JPanel panel = new MyPanel();
		add(panel);
	        
		setSize(500, 540);
		setVisible(true);  
	}
}
