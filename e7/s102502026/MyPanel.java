package ce1002.e7.s102502026;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	public MyPanel() {
		setBounds(0, 0, 500, 500);
		JPanel s[][] = new JPanel[10][10];	//for 10*10 array
	}

	public void paintComponent(Graphics a) { // for the chess board
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 0) {
					a.setColor(Color.WHITE);
				} else {
					a.setColor(Color.BLACK);
				}
				a.fillRect(i * 50, j * 50, 50, 50);
			}
		}
	}
}