package ce1002.e7.s102502026;

import javax.swing.JFrame;

public class E7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyFrame frame= new MyFrame();
		frame.setBounds(0,0, 550, 550);
		frame.add(new MyPanel());	//draw chess board
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);	//visible frame
	}
}
