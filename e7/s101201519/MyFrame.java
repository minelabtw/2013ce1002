package ce1002.e7.s101201519;
import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame{
	
	public MyFrame()
	{
		MyPanel panel = new MyPanel();
		setLayout(null);
		add(panel);
		setBounds(0,0,550,550);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
