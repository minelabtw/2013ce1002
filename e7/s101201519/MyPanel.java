package ce1002.e7.s101201519;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{

	public MyPanel()
	{
		setLayout(null);
		setBounds(0,0,500,500);
	}
	
	public void paint(Graphics g)
	{
		for(int i=0; i<10; i++)
		{
			for(int j=0; j<10; j++)
			{
				if((i+j)%2 == 0)
					g.setColor(Color.WHITE);
				else
					g.setColor(Color.BLACK);
				g.fillRect(i*50, j*50, 50, 50);
			}
		}
	}
}
