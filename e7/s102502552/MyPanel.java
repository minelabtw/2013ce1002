package ce1002.e7.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	public MyPanel()
	{
		setBounds(0,0,500,500);//給定邊界
	}

	public void paint(Graphics G)//覆寫函式
	{
		for(int i = 0;i < 10;i++)
		{
			for(int j = 0;j < 10;j++)
			{
				if((i + j) % 2 == 0)
				{
					G.setColor(Color.WHITE);
					G.fillRect(i * 50,j * 50,50,50);//行加列整除2畫白色方形
				}
				else
				{
					G.setColor(Color.BLACK);
					G.fillRect(i * 50,j * 50,50,50);//其他則為黑色
				}
			}
		}
		
	}
	
}
