package ce1002.e7.s102502503;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
public class MyPanel extends JPanel{
	public MyPanel(){
		setBounds(0,0,500,500);
	}
	public void paintComponent(Graphics g) {
		   super.paintComponent(g);
		   for(int j=0; j<10; j++){
			   for(int i=0; i<10; i++){
				   
				   if((i+j)%2==0){
					   g.setColor(Color.white);  //白色
				   }
				   else{
					   g.setColor(Color.black);  //黑色
				   }
				   g.fillRect(50*i, 50*j, 50, 50);  //畫矩形
		   }
		   
		   }
		  }
}
