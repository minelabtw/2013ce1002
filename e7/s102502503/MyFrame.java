package ce1002.e7.s102502503;
import javax.swing.*;
public class MyFrame extends JFrame{
	protected MyPanel mypanel = new MyPanel();
	public MyFrame(){
		setLayout(null);
		setBounds(0 , 0 , 530 , 550);
		setVisible(true);  //將視窗設為可見
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		show(mypanel);  //呼叫show函式
	}
	public void show(MyPanel panel)
	{
		add(panel);
	}
}
