package ce1002.e7.s102502550;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	MyPanel(){
		setLayout(null);
		setBounds(0,0,500,500);
		
		
	}
	
	protected void paintComponent(Graphics g){                     
		super.paintComponent(g);
		
		
		
		for(int i=0;i<10;i++){                                     //黑白相間的格子
			for(int j=0;j<10;j++){
				if((i+j)%2 == 0){
					g.setColor(new Color(255,255,255));
					g.fillRect(i*50, j*50, 50, 50);
				}
				else{
					g.setColor(new Color(0,0,0));
					g.fillRect(i*50, j*50, 50, 50);
			
				}
		    }
		
	    }
		
	}
}
