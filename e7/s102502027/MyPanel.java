package ce1002.e7.s102502027;

import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel {

	public void paint(Graphics g){
		super.paint(g);
		setBounds(0, 0, 500, 500);
		for (int i = 0; i < 10; i++) {
			for (int k = 0; k < 10; k++) {
				if ((i + k) % 2 == 0)
					{
					g.setColor(new Color(255,255,255)); //white
				    g.fillRect(45*k, 45*i, 45, 45);
				    }
				else
				{
					g.setColor(new Color(0,0,0));  //black
					g.fillRect(45*k, 45*i, 45, 45);
					}
			}
		}
	}

}
