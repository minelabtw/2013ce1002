package ce1002.e7.s102502534;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class MyPanel extends JPanel {
	MyPanel() {
		setLayout(null);
	    setBounds(0, 0, 500, 500);//設定panel大小
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		//印出黑色部分
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if ((i + j) % 2 == 1) {
					
					g.setColor(Color.BLACK);
					g.fillRect(i*50, j*50, 50, 50);
				}
				else
				{
					
					g.setColor(Color.WHITE);
					g.fillRect(i*50, j*50, 50, 50);
				}
				System.out.println("");
			}
		}
	}
}