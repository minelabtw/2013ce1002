package ce1002.e7.s102502509;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class MyPanel extends JPanel
{
	MyPanel()
	{
		
	}
	public void paint(Graphics g) // 繪圖方程式
	{
		super.paint(g); // 召喚函式
		setBounds(0, 0, 500, 500); // 標籤的位置及大小
		
		for(int u = 0; u < 10; u++) // 創造正方形
		{	
			for(int i = 0; i < 10; i++)
			{
				if( (u + i) % 2 == 0) // 找規律
				{
					g.setColor(Color.white); // 先設立顏色才不會錯
					g.fillRect(50*i, 50*u, 50, 50);
				}
				else
				{
					g.setColor(Color.black);
					g.fillRect(50*i, 50*u, 50, 50);
				}
			}
			
		}
	}
}
