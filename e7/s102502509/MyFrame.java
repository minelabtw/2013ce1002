package ce1002.e7.s102502509;

import javax.swing.*;

public class MyFrame extends JFrame
{
	MyFrame()
	{
		setSize(600, 550); // 建立大小
		
		MyPanel panel = new MyPanel(); // 召喚標籤
		add(panel); // 將標籤加入視窗
		
		setVisible(true); // 看見視窗
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 關掉視窗
	}
}
