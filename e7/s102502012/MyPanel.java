package ce1002.e7.s102502012;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private final int width = 10;
	private final int height = 10;
	private final int edge = 50;
	
	MyPanel(){
		setBounds(0, 0, 500, 500);
	}
	
	public void paintComponent(Graphics g){
		for(int row = 0; row < width; row++){
			for(int col = 0; col < height; col++){
				// set color
				if((row + col) % 2 == 0)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);
				
				// draw rect filled with color
				g.fillRect(col * edge, row * edge, edge, edge);
			}
		}
	}
	
}
