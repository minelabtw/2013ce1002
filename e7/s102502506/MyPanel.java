package ce1002.e7.s102502506;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
	MyPanel(){
	setBounds(0,0,500,500);  //大小 從x=0,y=0的地方設定一個500*500大小的Panel
	}
	public void paint(Graphics g){
		//make rectangles
		for(int x=0;x<10;x++){
			for(int y=0;y<10;y++){
				if ((x+y)%2==0){  
					g.setColor(Color.WHITE);
					g.fillRect(50*x, 50*y, 50, 50);
				}
				else{
					g.setColor(Color.BLACK);
					g.fillRect(50*x, 50*y, 50, 50);
				}
			}
		}
	}
}
