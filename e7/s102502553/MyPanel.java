package ce1002.e7.s102502553;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.JPanel;

import java.awt.Color;

public class MyPanel extends JPanel
{
	MyPanel()//邊界
	 {
		setBounds(0, 0, 500, 500);
	 }
	 
	 public void paint(Graphics g)//填長方形
	 {
		
		 for(int i = 0;i < 10;i++)
		 {
			 for(int j = 0;j < 10;j++)
			 {
				 if((i + j) % 2 == 1) 
				 {
					  g.setColor(Color.BLACK); 
		              g.fillRect(j * 50 , i * 50 , 50 , 50);
				 } 
				 else
				 {
					 g.setColor(Color.WHITE);
					 g.fillRect(j * 50 , i * 50 , 50 , 50);
				 }
			 } 
		 }	 
	 }
		 
}
    
