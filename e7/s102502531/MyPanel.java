package ce1002.e7.s102502531;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class MyPanel extends JPanel{

	MyPanel() 
	{
		setBounds(0,0,500,500);
		setLayout(null);
	}
	protected void paintComponent(Graphics g){ //畫交錯的正方形
		super.paintComponent(g);
		
		g.setColor(Color.BLACK);
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				if((j+i)%2==0)
				{
					g.setColor(Color.WHITE);
					 g.fillRect(j*50, i*50, 50, 50); 
				}
				else
				{
					g.setColor(Color.BLACK);
					g.fillRect(j*50, i*50, 50, 50);
				}
			}
		}
	}
}
