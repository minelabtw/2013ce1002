package ce1002.a1.s102502517;
import java.util.Scanner; //輸入用
public class A1 {

	public static void main(String[] args) {
		int input = 0;
		System.out.print("Please input a number (5~30): ");
		Scanner scanner = new Scanner(System.in);
		input = scanner.nextInt(); //輸入數字
		
	    while(input<5 || input>30) //條件式
	    {      
	    	System.out.println("Out of range!");
	    	System.out.print("Please input again (5~30): ");
	    	input = scanner.nextInt();
	    }
	    
	    int x = 1;
	    int y = 1;
	    
	    for(y=1; y<=input; y++) //輸出圖案
	    {
	    	if(y%2==1)
	    	{
	    		for(x=1; x<=input; x++)
	    		{
	    			if(x%2==1)
	    				System.out.print("X");
	    			else
	    				System.out.print("O");
	    		}
	    	}
	    	else
	    	{
	    		for(x=1; x<=input; x++)
	    		{
	    			if(x%2==1)
	    				System.out.print("O");
	    			else
	    				System.out.print("X");
	    		}
	    	}
	    	System.out.println();
	    }
	}
}
