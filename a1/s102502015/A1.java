package ce1002.a1.s102502015;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		int a;
		Scanner scanner = new Scanner(System.in);				//新的SCANNER
		System.out.print("Please input a number (5~30):");
		a = scanner.nextInt();									//讀整數
		while (a > 30 || a < 5) {								//超出範圍則重新輸入
			System.out.println("Out of range!");
			System.out.print("Please input again(5~30):");
			a = scanner.nextInt();
		}
		for (int i = 1; i <= a; i++) {							//畫棋盤
			for (int j = 1; j <= a; j++) {
				if (i % 2 == 1) {
					if (j % 2 == 0)
						System.out.print("O");
					if (j % 2 == 1)
						System.out.print("X");
				}
				if (i % 2 == 0) {
					if (j % 2 == 0)
						System.out.print("X");
					if (j % 2 == 1)
						System.out.print("O");
				}
			}
			System.out.println("");								//換行
		}
		scanner.close();										//關SCANNER
	}
}