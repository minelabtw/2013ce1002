package ce1002.a1.s101602016;
import java.util.Scanner; //使用java.util下的Scanner這個類別
public class A1{
	public static void main(String[] args){
		Scanner input=new Scanner(System.in); //新增一個需要使用System.in的Scanner物件
		int length=0; //棋盤長度
		do{ //請使用者輸入長度，如果超出範圍，則重新輸入
			System.out.print("Please input a number (5~30): ");
			length=input.nextInt();
			if(length<5||30<length)
				System.out.print("Out of range !\n");
		}while(length<5||30<length);
		for(int i=0;i<length;i++){ //畫棋盤
			if(i%2==0){ //偶數行第一個字母為X
				for(int j=0;j<length;j++){
					if(j%2==0)
						System.out.print("X");
					else
						System.out.print("O");
				}
			}
			else{ //奇數行第一個字母為O
				for(int k=0;k<length;k++){
					if(k%2==0)
						System.out.print("O");
					else
						System.out.print("X");
				}
			}
			System.out.print("\n"); //換行
		}
	}
}
