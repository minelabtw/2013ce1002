package ce1002.a1.s102502035;
	import java.util.Scanner;//導入輸入函式庫

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print ("Please input a number (5~30): ");//提示輸入
		Scanner input = new Scanner(System.in);
		int inputnum =input.nextInt();//輸入
		while (inputnum >30 || inputnum <5)
		{
			System.out.println ("Out of range!");//提示錯誤輸入
			System.out.print ("Please input again (5~30): ");//提示輸入
			inputnum =input.nextInt();//再輸入
		}
		for (int i =0; i <inputnum; i++)//依題目做輸出
		{
			for (int j =0; i %2 ==0 &&j <inputnum; j++)
			{
				if (j %2 ==0)
					System.out.print ("X");
				else 
					System.out.print ("O");
			}
			for (int j =0; i %2 ==1 &&j <inputnum; j++)
			{
				if (j %2 ==1)
					System.out.print ("X");
				else 
					System.out.print ("O");
			}
			if (i !=inputnum -1)//最後一行不必換行
			System.out.println ();
		}
	}
}
