package ce1002.a1.s995001561;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		
		int n;
		
		System.out.print( "Please input a number (5~30): " );
		Scanner input = new Scanner(System.in);	
		n = input.nextInt();
		
		while(n<5 || n>30) {
		System.out.print( "Out of range!\n Please input again (5~30): " );
		Scanner scan = new Scanner(System.in);	
		n = scan.nextInt();
		}    

		for(int i=0; i<n; i++) {
			for(int j=0; j<n; j++){
				
				if((i+j)%2==0) System.out.print( "X" );
				else System.out.print( "O" );
				
				if(j==(n-1)) System.out.print( "\n" );
				}
				
			}

	}

}