package ce1002.a1.s101201522;

import java.util.Scanner;

public class A1{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int num,i,j;
		System.out.print("Please input a number (5~30): ");
		num = input.nextInt();//input size
		while(num>30 || num<5){//If range is wrong, ask again
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			num = input.nextInt();
		}
		for(i=0;i<num;i++){//output chessboard
			for(j=0;j<num;j++){
				if((i+j)%2==1)
					System.out.print("O");
				else
					System.out.print("X");
			}
			System.out.println();
		}
		
	}
}
