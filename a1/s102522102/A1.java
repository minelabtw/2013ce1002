package ce1002.a1.s102522102;

import java.util.Scanner;

public class A1 
{

	private static Scanner scanner;

	public static void main(String[] args) 
	{
		//User input
		int num;
		scanner = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");
		//Set range between 5 to 30
		do 
		{
			num = scanner.nextInt();
			if (num < 5 || num > 30)
				System.out.print("Out of range!\nPlease input again (5~30): ");
		} while (num < 5 || num > 30);
		
		//Use two loop for display
		for (int i = 0; i < num; i++) 
		{
			for (int j = 0; j < num; j++) 
			{
				if ((i + j) % 2 == 0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.println();
		}

	}

}
