package ce1002.A1.s102502013;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n = 0;
		System.out.print("Please input a number (5~30):");
		int i1=0, j1=0;
		n = input.nextInt();
		if (n>30||n<5){
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30):");
			n = input.nextInt();
		}
		else{
			i1 = n;
			j1 = n;
			for(int i=1;i<=i1;i++){
				for(int j=1;j<=j1;j++){
					if((i+j)%2==0){
						System.out.print("X");
					}
					else{
						System.out.print("O");
					}
				}
				System.out.println("");
			}
		}
	}

}
