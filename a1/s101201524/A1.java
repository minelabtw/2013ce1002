package ce1002.a1.s101201524;
import java.util.Scanner;

public class A1 {
	public static void main(String args[]){
		Scanner jin = new Scanner(System.in);
		int number = 0;
		System.out.print("Please input a number (5~30): ");
		number = jin.nextInt();
		while(number<5 || number>30){
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			number = jin.nextInt();
		}
		for(int y = 0; y < number; y++){
			for(int x = 0; x < number; x++){
				if((x+y)%2 == 0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.println("");
		}
	}
}
