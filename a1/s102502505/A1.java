package ce1002.a1.s102502505;

import java.util.Scanner;

public class A1 {
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Please input a number (5~30): ");
		Scanner scanner = new Scanner(System.in);
		int x;
		x=scanner.nextInt();
		while( x>30 | x<5)
		{ 
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			x=scanner.nextInt();
		}
		for(int i=0;i<x;i++)
		{
			if(i%2==0)
			{
				for(int j=0;j<x;j++)
				{
					if(j%2==0)
						System.out.print("X");
					if(j%2==1)
						System.out.print("O");
				}
			}
			if(i%2==1)
			{
				for(int j=0;j<x;j++)
				{
					if(j%2==0)
						System.out.print("O");
					if(j%2==1)
						System.out.print("X");
				}
			}
			System.out.println("");
		}		
	}

}
