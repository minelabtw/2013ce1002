package ce1002.a1.s102502005;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n;
		Scanner input = new Scanner(System.in);
		
		System.out.print("Please input a number (5~30): ");//要求使用者輸入測資。
		n = input.nextInt();
		
		while(n<5 || n>30)//若測資不符條件，則要求使用者重新輸入，直到測資符合條件為止。
		{
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt();
		}
		
		for(int i=1;i<n;i++)//利用兩個for迴圈加上if判斷式印出結果。
		{
			if(i%2 == 1)
			{
				for(int j=0;j<n;j++)
				{
					if(j%2 == 1)
					{
					System.out.print("X");
					}
					else if (j%2 == 0)
					{
					System.out.print("O");
					}	
				}
				System.out.print("\n");
			}
			
			else if (i%2 == 0)
			{
				for(int j=0;j<n;j++)
				{
					if(j%2 == 1)
					{
					System.out.print("O");
					}
					else if (j%2 == 0)
					{
					System.out.print("X");
					}
				}
				System.out.print("\n");
			}
		}
		
	}

}
