package ce1002.a1.s102502023;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number = 0;
		Scanner input = new Scanner(System.in);
		do
		{
			System.out.print("Please input a number (5~30):");
			number = input.nextInt();
			if( number < 5 || number > 30 )
				System.out.println("Out of range!");
		}
        while( number < 5 || number > 30);
		
		for( int i = 1; i <= number; )
		{
			if( i % 2 != 0 )
			{
				for( int j = 1; j <= number; j++ )
				{
					if( j % 2 != 0 )
						System.out.print("o");
					else if( j % 2 == 0 )
						System.out.print("x");
				}
				
				System.out.println("");
				i++;
			}
			
			if( i % 2 == 0 )
			{
				for( int j = 1; j <= number; j++ )
				{
					if( j % 2 != 0 )
						System.out.print("x");
					else if( j % 2 == 0 )
						System.out.print("o");
				}
				
				System.out.println("");
				i++;
			}
		}
	}

}
