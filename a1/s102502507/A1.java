package ce1002.a1.s102502507;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		int p;
		do
		{
			System.out.print("Please input a number (5~30)"); //問數字多少
			p=input.nextInt();//輸入數字
			if(p>30 || p<5)
			{
				System.out.print("Out of range!\n");//顯示超出範圍假如它不符合條件
			}
		}while(p>30 || p<5);
		for(int r=1;r<=p;r++)//限制圖形排列方式
		{
			for(int s=1;s<=p;s++)
			{
				if(r%2==0)
				{
					if(s%2==0)
						System.out.print("X");
					else
						System.out.print("O");
				}
				else
				{
					if(s%2==0)
						System.out.print("O");
					else
						System.out.print("X");
								}
				}
		System.out.print("\n");
		}

	}

}
