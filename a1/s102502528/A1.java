package ce1002.e1.s102502528;

import java.util.Scanner;

public class A1 {
	
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please input a number (5~30): ");
		int width = scanner.nextInt();
		while (width < 5 || width > 30)              //»èAü
		{
			System.out.println("Out of range!");
			System.out.println("Please input a number (5~30): ");
			width = scanner.nextInt();
		}
		for(int i=0; i<width; i++){                  //Aoûw
			for(int j=0; j<width; j++)
			{
				if((i+j)%2 == 0)                     //sñ×ôÉAoXC×ïÉAoO
				{
					System.out.print("X");
				}
			    else 
			    {
			    	System.out.print("O");
				}
			}
		System.out.println();
		}
	}
}
