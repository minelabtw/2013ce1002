package ce1002.A1.s102502011;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in) ;
		
		int number ;
	
	    do
	    {
	    	System.out.print("Please input a number(5~30): ") ;
	    	 number = input.nextInt() ;
	    	if (number < 5 || number > 30 )
	    		System.out.println("Out of range!") ;
	    }
	    while (number < 5 || number > 30 ) ;
	    
	    for(int i=0;i<number;i++)
	    {
	    	for(int j=0;j<number;j++)
	    	{
	    		if ( (i+j) % 2 == 1 )
	    			System.out.print("O") ;
	    		else 
	    			System.out.print("X") ;
	    	}
	    	System.out.println() ;
	    }
			
		
		

	}

}
