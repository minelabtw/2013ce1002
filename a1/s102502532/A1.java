package ce1002.a1.s102502532;

import java.util.Scanner; //匯入Scanner類別

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); // 建立Scanner物件
		System.out.print("Please input a number (5~30): ");

		int n = input.nextInt(); // 讀取輸入資訊
		while (n < 5 || n > 30) { // or
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt(); // 讀取輸入資訊
		}

		for (int y = 1; y <= n; y++) {
			if (y % 2 == 1) {
				for (int x = 1; x <= n; x++) {
					if (x % 2 == 1) {
						System.out.print("X");
					} else
						System.out.print("O");
				}
			} else {
				for (int x = 1; x <= n; x++) {
					if (x % 2 == 1) {
						System.out.print("O");
					} else
						System.out.print("X");
				}
			}
			System.out.println(""); // 換行
		}
	}
}