package ce1002.a1.s102502524;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int a = 0;												// 設定輸入變數
		
		Scanner input = new Scanner(System.in);					//使用者輸入
		
		System.out.print("Please input a number (5~30): ");		//輸入正方形大小
		a = input.nextInt();
		while(a<5 || a>30)										//判斷範圍是否符合
		{
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30): ");
			a = input.nextInt();
		}
		
		for(int i=0;i<a;i++)									//繪出正方形
		{
			for(int k=0;k<a;k++)
			{
				if(i%2==1)										//偶數行由O開始
				{
					if(k%2==0)
						System.out.print("O");
					else
						System.out.print("X");
				}
				if(i%2==0)										//奇數行由X開始
				{
					if(k%2==0)
						System.out.print("X");
					else
						System.out.print("O");
				}
			}
			System.out.println();
		}
		

	}

}
