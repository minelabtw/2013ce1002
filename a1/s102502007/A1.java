
package ce1002.A1.s102502007;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);//輸入處理物件
		int length = 0;//set up the length as 0
		System.out.print("Please input a number (5~30): ");
        do
		{
		length = input.nextInt();
		if(length<5 || length >30)
		{
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
		}		
		}while(length<5 || length>30 );//loop till the correct number is input
        for(int i=0;i<length;i++)//there are total "length" lines
        {
        	for(int j=0;j<length;j++)//every line includes "length" dots
        	{
        		if(i%2==0)//單數行的輸出順序和偶數行顛倒
        		{
        			if(j%2==0)
        				System.out.print("X");
        			else
        				System.out.print("O");
        		}

        		else
        		{
        			if(j%2==0)
        				System.out.print("O");
        			else
        				System.out.print("X");
        		}
        	}
    		System.out.println();
        }
        	input.close();
	}

}
