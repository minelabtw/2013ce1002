package ce1002.a1.s102502520;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner (System.in);
		System.out.print("Please input a number (5~30): ");//輸出
		int number = cin.nextInt();//定義整數，初始為零
		while (number<5 || number>30)//條件
		{
			System.out.println("Out of range!");//輸出
			System.out.print("Please input a number (5~30): ");//輸出
			number = cin.nextInt();//輸入
		}
		for (int x= 0; x<number; x++)//顯示圖型
		{
			for (int y=number; y>0 ;y--)
			{
				if (x % 2 == 1 && y% 2 == 0)//條件x為奇數且y為偶數輸出X
				{
					System.out.print("X");
				}
				if (x % 2 == 0 && y% 2 == 1)//類推
				{
					System.out.print("X");
				}
				if (x % 2 == 0 && y% 2 == 0)
				{
					System.out.print("O");
				}
				if (x % 2 == 1 && y% 2 == 1)
				{
					System.out.print("O");
				}
			}
			System.out.println();//換行
		}
	}
}
