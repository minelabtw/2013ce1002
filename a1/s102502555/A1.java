package ce1002.a1.s102502555;

import java.util.Scanner;

public class A1 {
	public static void main(String[] args) {
		int number = 0; // 輸入的數字
		Scanner input = new Scanner(System.in);

		System.out.print("Please input a number (5~30): "); // 提醒使用者輸入數字
		number = input.nextInt();

		while (number > 30 || number < 5) {  //超出範圍重新輸入
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			number = input.nextInt();
		}

		Boolean column = new Boolean(false);

		for (int i = number; i > 0; i--) {  //印出棋盤
			Boolean row = column;
			for (int j = number; j > 0; j--) {
				if (row == true) {
					System.out.print('O');
				} else {
					System.out.print('X');
				}
				row = !row;
			}
			System.out.println();
			column = !column;
		}
	}
}
