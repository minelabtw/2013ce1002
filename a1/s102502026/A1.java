package ce1002.a1.s102502026;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n;
		do
		{
			System.out.print("Please input a number (5~30):");	//ask number
			n= input.nextInt();		//input number
			if(n<5 || n>30)
			{
				System.out.print("Out of range!\n");		//out of range if is out of 5~30
			}
		} while (n<5 || n>30);

		for(int a=1;a<=n;a++)	//for the drawing of the table
		{
			for(int b=1; b<=n; b++)
			{
				if(a%2==0)
				{
					if(b%2==0)
						System.out.print("X");
					else
						System.out.print("O");
				}
				else
					{
					if(b%2==0)
						System.out.print("O");
					else
						System.out.print("X");
					}
			}
			System.out.print("\n");
		}
			}
		}

