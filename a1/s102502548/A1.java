package ce1002.a1.s102502548;

import java.util.Scanner ;

public class A1 {

	public static void main(String[] args) {
		int number=0 ;
		
		while (true){
			System.out.print("Please input a number (5~30): ") ;
			
			Scanner sc=new Scanner(System.in) ;
			
			number=sc.nextInt();
			
			if (number<5||number>30){
				System.out.println("Out of range!");
			}
			else break ;
		}
		
		for (int y=0;y<number;y++){
			for (int x=0;x<number;x++){
				if ((x+y)%2==0){
					System.out.print("X") ;
				}
				else System.out.print("O") ;
			}
			System.out.println(" ") ;
		}

	}

}
