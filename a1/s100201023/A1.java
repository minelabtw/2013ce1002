package ce1002.a1.s100201023;

import java.util.Scanner;

public class A1 
{

	public static void main(String[] args) 
	{
		int number , symbol = 0;
		Scanner input = new Scanner(System.in);
		
		//input number and determine if number between 5 ~ 30
		System.out.print("Please input a number (5~30): ");
		number = input.nextInt();
		while(number < 5 || number > 30)
		{
			System.out.println("Out of range!");
			System.out.println("Please input again (5~30): ");
			number = input.nextInt();
		}
		
		for(int i = 0 ; i < number ; ++i)
		{
			for(int j = 0 ; j < number ; ++j)
			{
				if((symbol + j) % 2 == 1)
					System.out.print("O");
				else 
					System.out.print("X");
			}
			System.out.println();
			++symbol;
		}
	}

}
