package ce1002.a1.s102502031;

import java.util.Scanner;

public class A1 {
	public static void main(String[] args) {
		int size = 0;
		int a = 0;
		Scanner askSize = new Scanner(System.in);
		while (size < 5 || size > 30) {
			if (a != 0)
				System.out.println("Out of range!");
			System.out.print("Please input a number (5~30): ");
			size = askSize.nextInt();
			a = 1;
		}
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if ((i + j) % 2 == 0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.println();
		}
	}
}