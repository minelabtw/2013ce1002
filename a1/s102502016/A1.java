package ce1002.a1.s102502016;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		int counter;//��X��O
		int number;//��J
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please input a number (5~30):");
		number = scanner.nextInt();
		while (number < 5 || number > 30) {
			System.out.println("Out of range !");
			System.out.print("Please input a number (5~30):");
			number = scanner.nextInt();
		}
		for (int i = 1; i <= number; i++) {
			if (i % 2 == 1)
				counter = 0;
			else
				counter = 1;
			for (int j = 1; j <= number; j++) {
				if (counter == 0) {
					System.out.print("X");
					counter += 1;
				} else {
					System.out.print("O");
					counter -= 1;
				}
				if (j == number)
					System.out.println();
			}
		}
	}

}
