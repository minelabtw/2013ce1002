package s102502552;

import java.util.Scanner;

public class A1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);//掃描機以輸入
		
		int side;//宣告邊長變數
		
		System.out.print("Please input a number (5~30):");
		
		do{
			side = sc.nextInt();
			if(side < 5 || side > 30)//對輸入數進行檢查
				System.out.print("Out of range ! Please input again(5~30):");//不合要求請求重輸
		}while(side < 5 || side > 30);//迴圈檢查數是否符合要求
		
		for(int i = 1;i <= side;i++)//行的迴圈
		{
			for(int j = 1;j <= side;j++)//列的迴圈
			{
				if(j % 2 != 0 && i % 2 !=0)
					System.out.print("X");//奇行奇列放X
				else if(j % 2 == 0 && i % 2 == 0)
					System.out.print("X");//偶行偶列放X
				else System.out.print("O");//其他放O
			}System.out.println();//一行完換行
		}
		

	}

}
