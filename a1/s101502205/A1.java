package ce1002.a1.s101502205;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		
		// create a Scanner object
		Scanner input = new Scanner(System.in);
		
		int num;
		
		while(true){
			// prompt the user to enter
			System.out.print("Please input a number (5~30): ");
			num = input.nextInt();
			
			if(num>=5 && num<=30)
				// accepted
				break;
			
			// error message
			System.out.println("Out of range!");
		}
		
		// display result
		for(int i=0; i<num; i++){
			// ith row
			for(int j=0; j<num; j++){
				if((i+j)%2 == 0){
					System.out.print("X");
				}else{
					System.out.print("O");
				}
			}
			// print '\n'
			System.out.println();	
		}
	}
}
