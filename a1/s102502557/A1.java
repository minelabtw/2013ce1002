package ce1002.A1.s102502557;
import java.util.Scanner ;
public class A1 {
public static void main(String args[])
{
  Scanner input = new Scanner(System.in);//scanner初始化 scanner是“import java.util.Scanner ”出來的 功用為解析char或是int
  System.out.print("Please input a number (5~30): ");
  int num = input.nextInt();
  while( num<5||num>30 ) 
  {	  
  System.out.println("Out of range!");
  System.out.print("Please input a number (5~30): ");
  num = input.nextInt();
  }
  
 
	  for(int i=0 ; i<num ;i++)
	  {
		  for(int j=0 ;j<num;j++ )
		  {
			  if(  (i+j)%2==0  )//這題的關鍵在於 斜的都會是同一個符號
				  System.out.print("X");//而斜的橫坐標加縱坐標都是同為偶數或同為奇數
			  else
				  System.out.print("O");
		  }
	  System.out.println("");
	  }	  
   }

}
