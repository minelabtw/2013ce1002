package ce1002.A1.s102502559;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Please input a number (5~30): ");
		Scanner scanner = new Scanner(System.in);//建立一個Scanner類別的變數scanner(建立一個輸入器)
		int number_input = scanner.nextInt();//讀取int型態的數據,將值指定給變數
		//判斷使用者輸入的數字是否超過規定範圍
		while (number_input < 5 || number_input > 30) {
			System.out.print("Out of range!\nPlease input a number (5~30): ");
			number_input = scanner.nextInt();
		}
		//顯示OX
		for (int i = 1; i <= number_input; i++) {
			for (int j = 1; j <= number_input; j++) {
				int row_detection = i % 2;//利用變數row_detection判斷為基數或偶數行
				if (j % 2 == 1) {
					switch (row_detection) {
					case 1:
						System.out.print("X");
						break;
					case 0:
						System.out.print("O");
						break;
					}
				}
				if (j % 2 == 0) {
					switch (row_detection) {
					case 1:
						System.out.print("O");break;
					case 0:
						System.out.print("X");break;
					}
				}
				if (j == number_input)
					System.out.print("\n");//一橫列顯示完後換行繼續
			}
		}
	}
}
