package ce1002.a1.s102502003;

import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in); //取得使用者輸入字元
		int number;  //宣告變數
				
		System.out.println("Please input a number(5~30): ");
		
		do  //判定範圍
		{
			number=scanner.nextInt();
			if(number<5||number>30)
				System.out.println("Out of range !\nPlease input again(5~30): ");
		}while(number<5||number>30);
		
		int i=1, j=1;  //棋盤格繪製
		for (i = 1; i <= number; i++){
            for (j = 1; j <= number; j++) {
            	if(j==2*i-1)
            		System.out.print("X");            	
            	else
            		System.out.print("O");
            	
            }
            System.out.println();
        }
		
			
		
	}
		

}
