package ce1002.a1.s102502562;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number;//宣告一個整數
		do
		{
	        System.out.print("Please input a number (5~30): ");
	        number = input.nextInt();//讓number等於輸入的數
	        if(number<5 || number>30)
	        {
	        	System.out.println("Out of range!");
	        }
		} while (number<5 || number>30);//讓使用者輸入數字並判斷是否符合範圍
		for(int i=1;i<=number;i++)//輸出結果
		{
			for(int j=1;j<=number;j++)
			{
				if((i+j)%2==0)
				{
					System.out.print("X");
				}
				else
				{
					System.out.print("O");
				}
			}
			System.out.println();
		}
	}

}
