package ce1002.a1.s102502048;
import java.util.Scanner;
public class A1 {
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int num=0;
		do
		{
			System.out.print("Please input a number (5~30): ");//顯示
			num = input.nextInt();//輸入		
			if(num <5 || num >30)
				System.out.println("Out of range!");//顯示
		}while(num <5 || num >30);		
		for(int h = 0; h < num ; h++ )//排棋盤
		{
			if(h%2 == 0)//交錯
			{
				for(int w = 0; w < num ; w++ )
				{
					if(w%2==0)//交錯
						System.out.print("O");
					else
						System.out.print("X");
				}
			}
			else
			{
				for(int w = 0; w < num ; w++ )
				{
					if(w%2==0)//交錯
						System.out.print("X");
					else
						System.out.print("O");
				}
			}
			System.out.print("\n");
		}
	}	
}
