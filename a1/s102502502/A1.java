package ce102502502.a1.s102502502;
import java.util.Scanner;
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int n;
		System.out.println("Please input a number (5~30): ");
		n = input.nextInt();
		while (n < 5 || n > 30)
		{
			System.out.println("Out of range! ");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt();
		}		
		for (int j = 0; j < n; j++)
		{
			for (int i = 0; i < n; i++)
			{
				if ((i+j) % 2 ==0 )
				{
					System.out.println("O");
				}
				else 
					System.out.println("X");
			}
			System.out.println("");
		}
	}
}
