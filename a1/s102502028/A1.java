package ce1002.a1.s102502028;
import java.util.Scanner ;
public class A1 {

	public static void main(String[] args) {
        
	    System.out.print("Please input a number (5~30): ") ; //輸出要求
	    Scanner cin = new Scanner(System.in) ;               //輸入
	    int input = cin.nextInt() ;
	    
	    while (input < 5 || input > 30)                      //判斷輸入是否符合要求
	    {
	    	System.out.println("Out of range !") ;
	    	System.out.print("Please input a number (5~30): ") ; 
		    input = cin.nextInt() ;
	    }

	    for (int y = 1 ; y <= input ; y++)                   //印出棋盤
	    {
	        for (int x = 1 ; x <= input ; x++)
	        {
	            if (y%2 != 0 && x%2 != 0)
	            	System.out.print ("X") ;
	            if (y%2 != 0 && x%2 == 0)
	            	System.out.print ("O") ;
	            if (y%2 == 0 && x%2 != 0)
	                System.out.print ("O") ;
	            if (y%2 == 0 && x%2 == 0)
	            	System.out.print ("X") ;
	        }
	        System.out.println();                           //換行
	    }
	}

}
