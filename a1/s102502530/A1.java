package ce1002.a1.s100502530;
import java.util.Scanner;

public class A1
{
   public static void main(String[] args)
   {
      int num=0;
      Scanner scan=new Scanner(System.in);
      System.out.print("Please input a number (5~30): ");
      do
      {
         num=scan.nextInt();
         if(num<5||num>30)
         {
            System.out.println("Out of range!");
            System.out.print("Please input again (5~30): ");
         }
      }while(num<5||num>30);
      for(int i=0;i!=num;i++)
      {
         for(int j=0;j!=num;j++)
            System.out.print(((i+j)&1)==1?'O':'X');
         System.out.println("");
      }
   }
}
