package ce1002.A1.s102502010;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		int number,i,j;
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number(5~30):");
		number=input.nextInt();
		while(number<5 || number>30)
		{
			System.out.println("Out of range!");
			System.out.print("Please input a number(5~30):");
			number=input.nextInt();
		}
		for(i=0;i<number;i++)
		{
			if(i%2==0)
			{
				for(j=0;j<number;j++)
			    {
				if(j%2==0)
					System.out.print("X");
				else
					System.out.print("O");
			    }
			}
			else
			{
				for(j=0;j<number;j++)
				{
					if(j%2==0)
						System.out.print("O");
					else
						System.out.print("X");
				}
			}
			System.out.println("");
		}
	}
}
