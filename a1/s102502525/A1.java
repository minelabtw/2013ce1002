package ce1002.a1.s102502525;

import java.util.Scanner;

public class A1 {
	public static void main(String[] args){
		int number;
		Scanner input =new Scanner(System.in);
			System.out.print(" Please input a number (5~30): ");
			number=input.nextInt();
			while(number<5 || number>30){
			System.out.print("Out of range ! Please input again(5~30): ");
			number=input.nextInt();
			}
		for(int j=1;j<=number;j++)
		{
				if(j%2!=0)
				{
					for(int i=1;i<=number;i++)
					{
						if(i%2!=0)
							System.out.print("X");
						else
							System.out.print("O");
					}
				}
				if(j%2==0){
					for(int i=1;i<=number;i++)
					{
						if(i%2!=0)
							System.out.print("O");
						else
							System.out.print("X");
					}
				}
				System.out.println(); 
			}
		}
}

