package ce1002.a1.s102502554;

import java.util.Scanner;//scanner is in the java.util package

public class A1 {
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);//creat a scanner object
		System.out.print("Please input a number (5~30): ");//prompt the user to enter a number
		int number = input.nextInt();
		while (number < 5 || number >30){
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30): ");
			number = input.nextInt();
		}//if the number out of range , the user can enter again
		for (int i = 0 ; i < number ; i++){
			if (i % 2 == 0){
			for (int j = 0 ; j < number ; j ++){
				if (j % 2 == 0)
					System.out.print("X");
				if (j % 2 == 1)
					System.out.print("O");
			}
			}
			if (i % 2 == 1){
				for (int j = 0 ; j < number ; j ++){
					if (j % 2 == 0)
						System.out.print("O");
					if (j % 2 == 1)
						System.out.print("X");
				}
			}
			System.out.print("\n");
		}//to draw the  chessboard with O and X
	}
}
