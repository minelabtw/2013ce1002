package ce1002.a1.s102502024;
import java.util.Scanner;
public class a1 {

	public static void main(String[] args) {
		Scanner scn=new Scanner(System.in);  //建立輸入物件
		int num;  //宣告變數
		int x=1;
		int y=1;
		System.out.print("Please input a number (5~30):");  //輸出題目
		num=scn.nextInt();  //輸入數字
		while(num<5 || num>30)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30):");
			num=scn.nextInt();
		}
		while(y<=num)  //產生圖形
		{
			while(x<=num)
			{
				if(x%2!=0 && y%2!=0)
				{
					System.out.print("X");
					x++;
				}
				if(x%2==0 && y%2!=0)
				{
					System.out.print("O");
					x++;
				}
				if(x%2!=0 && y%2==0)
				{
					System.out.print("O");
					x++;
				}
				if(x%2==0 && y%2==0)
				{
					System.out.print("X");
					x++;
				}
			}
			System.out.println();
			y++;
			x=1;
		}
		// TODO Auto-generated method stub
	}

}
