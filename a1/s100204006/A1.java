package ce1002.a1.s100204006;

import java.util.Scanner;


public class A1 {
	public static void main(String[] args) {
		while(true)
		{
		int inputNum;        //input number
        Scanner scanner = new Scanner(System.in);        //System.in用來取得使用者的輸入
		System.out.print("please input a number(5~30):");
		inputNum = scanner.nextInt();

		if(inputNum>=5&&inputNum<=30)
			
		{		
			for(int x=0;x<inputNum;x++)      //x為column
		    {
				if(x%2==1)     //奇數行
				{
			     for(int i=0;i<inputNum;i++)     //i為row 
		    	 {
				  if(i%2==0)
					  System.out.print("O");
				  else if(i%2==1)
					  System.out.print("X");
		    	  }
			     }
			     else if(x%2==0)      //偶數行
			     {	 
			    	 for(int i=0;i<inputNum;i++)
			    	 {
			    	 if(i%2==0)
						  System.out.print("X");
					  else if(i%2==1)
						  System.out.print("O");
			         }	
			     }
				System.out.print("\n");
				
		    }
			
		}
		else
			System.out.println("Out of range!");
	}
}
}