package ce1002.A1.s102502501;
import java.util.Scanner;	//#include<stdio.h>的宣告
public class A1 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int n = 0;	//宣告變數
		System.out.print("Please input a number (5~30): ");    //輸出一個Please input a number (5~30): 相等於cout
		n = input.nextInt();								   //輸入等於(c++的cin >> n;)
		while ( n < 5 || n > 30 )							   //設定一個迴圈
		{
		System.out.println("Out of range ! ");				  //當超出迴圈值，螢幕上顯示Out of range ! 並換行(ln)
		
		System.out.print("Please input again(5~30): ");		 //螢幕上顯示Please input again(5~30): 
		n = input.nextInt();							    //輸入等於(c++的cin >> n;)
		} 
		for(int i = 1; i <=n ; i++)						//在for迴圈內宣告一個變數等於1
		{   
			System.out.println();					   //輸出
			for(int j = 1; j <=n; j++)
			{ 
				if((i+j) % 2 == 0)	//i+j 的餘數如果是0將會輸出於X  i+j的餘數如果不是零將輸出漁O
					
				    System.out.print("X");
				else
				    System.out.print("O"); 
				
			}
			} 
	
	}
	
}
