package ce1002.A1.s102502506;
import java.util.Scanner;
public class A1 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int N =0;
		do // input
		{
			System.out.print("Please input a number (5~30): ");
			N = input.nextInt();
			if(N<5 || N>30)
				System.out.println("Out of range!");
		}while(N<5 ||N>30);
		
		for(int i=0;i<N;i++) //output
		{
			for(int j=0;j<N;j++)
			{	
				
				 if((i+j)%2==0)
					System.out.print("X");
				else if((i+j)%2==1)
					System.out.print("O");
			}
			System.out.println("");
		}
		
	}

}

