package ce1002.a1.s102502509;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    int number = 0;
    Scanner enter = new Scanner(System.in);
    
    System.out.print("Please input a number (5~30): ");
    number = enter.nextInt();
    
    while(number > 30 || number < 5)
    {
    	System.out.println("Out of range!");
    	System.out.print("Please input again (5~30): ");
    	number = enter.nextInt();
    }
    
    Boolean y = new Boolean(false);
    
    for(int i = number ; i > 0 ; i--)
    {
    	Boolean x = y;
    	for(int j = number ; j > 0 ; j--)
    	{
    		if(x == true)
    		{
    			System.out.print('~');
    		}
    		else 
    		{
    			System.out.print('-');
    		}
    		x = !x;
    	}
    	System.out.println();
    	y = !y;
    }
	}
}
