package ce1002.a1.s102502022;
import java.util.Scanner;
public class a1
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
        Scanner input = new Scanner(System.in);          
		int a=0;              //宣告a為變數
		System.out.print("Please input a number(5~30):");    
        a=input.nextInt();
        while(a<5 || a>30)     //迴圈
        {
        	System.out.println("Out of range!");
        	System.out.print("Please input a number(5~30):");
        	a=input.nextInt();
        }
        for(int m=0;m<a;m++)         //迴圈
        {
        	for(int n=0;n<a;n++)
        	{
        		if(m%2==0)           //如果
        		{
        			if(n%2==0)
            			System.out.print("X");
            		else
            			System.out.print("O");
        		}
        		else
        		{
        			if(n%2==0)
        				System.out.print("O");
            		else
            			System.out.print("X");
        		}
        		
        		
        	}
        	System.out.println();         //換行
        	
        }
        
    
	}

}
