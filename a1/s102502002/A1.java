package ce1002.a1.s102502002;
import java.util.Scanner;

public class A1 {

	public A1() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int size=0;
		System.out.println("Please input a number (5~30): "); 
		size = in.nextInt();
		while(size>30 || size<5)
	    	{
			    System.out.println("Out of Range!");
			    System.out.println("Please input again (5~30): ");
			    size = in.nextInt();
		    }
		for(int j=0; j<size; j++)
		{ 
			if(j%2==0){
				for(int k=0; k<size; k++)
				{
					if(k%2==0)
						System.out.print("X");
					else
						System.out.print("O");
				}
			}
			if(j%2==1){
				for(int k=0; k<size; k++)
				{
					if(k%2==0)
						System.out.print("O");
					else
						System.out.print("X");
				}
			}
			System.out.println();
		}
	}

}
