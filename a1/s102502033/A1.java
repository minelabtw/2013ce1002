package ce1002.a1.s102502033;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int x=1;//列
		int y=1;//行
		
		System.out.print("Please input a number (5~30): ");//輸出
		Scanner input = new Scanner(System.in);//輸入物件
		int n= input.nextInt();
		while(n<5 || n>30)//判別式
		{
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			n= input.nextInt();		
		}
		while(y<=n)//處理列再處理行
		{
			while(x<=n)
			{
				if(y%2==1)//奇偶的分別
				{
					if(x%2==1)
					{
						System.out.print("X");
					}
					else
					{
						System.out.print("O");
					}
					x++;
				}
				else
				{
					if(x%2==1)
					{
						System.out.print("O");
					}
					else
					{
						System.out.print("X");
					}
					x++;
				}
					
			}
			System.out.println();
			y++;
			x=1;
		}
		
	}

}
