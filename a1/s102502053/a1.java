package ce1002.a1.s102502053;

import java.util.Scanner; //scanner is in the java.util package

public class a1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int number;//call variable
		do{
			System.out.print("Please input a number (5~30): ");
			number = input.nextInt(); //input data
			if(number < 5 || number > 30) //data validation
			{
				System.out.print("Out of range!\n");
			}
		  }while(number < 5 || number > 30);
		
		for(int a = 1 ; a<=number ; a++) //display output for columns
		{
			if(a % 2 != 0) //odd number row
			{
			for(int b = 1 ; b <= number ; b++) //display output for each rows
			{
				if(b %2 != 0) // odd number column
				{
					System.out.print("X");
				}else if(b % 2== 0) //even number column
				{
					System.out.print("O");
				}
			}
			System.out.print("\n");
			}else if(a % 2 == 0) // even number row
			{
				for(int b = 1 ; b <= number ; b++)
				{
					if(b%2!=0)
					{
						System.out.print("O");
					}else if(b % 2 == 0)
					{
						System.out.print("X");
					}
				}
				System.out.print("\n");
			}
		}
	}
}
