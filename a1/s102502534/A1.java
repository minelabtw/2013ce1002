package ce1002.a1.s102502534;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");
		int n = input.nextInt();
		while (n < 5 || n > 30) {// 判斷n是否在5~30之間
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt();
		}
		// 畫棋盤
		for (int i = 1; i <= n; i++) {
			for (int s = 1; s <= n; s++) {
				if ((i + s) % 2 == 0) {
					System.out.print("o");
				} else {
					System.out.print("x");
				}
			}
			System.out.println();
		}
	}
}
