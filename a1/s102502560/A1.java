package ce1002.A1.s102502560;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		
		Scanner STDIN=new Scanner(System.in);					//scanner to read from standard input
		System.out.print("Please input a number (5~30):");
		int n=0;
		while(STDIN.hasNextInt()){								//wait for next integer
			n=STDIN.nextInt();									//input
			if(n>=5&&n<=30)break;
			else System.out.println("Out of range!\nPlease input again(5~30):");
		}
		
		for(int x=0;x<n;x++){
			for(int y=0;y<n;y++){
				System.out.print((x+y)%2==0 ? "X" : "O");		//print "X" if x and y are both even or both odd, print "O" otherwise
			}
			System.out.println();
		}

	}

}
