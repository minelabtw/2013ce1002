package ce1002.a1.s100203020;
import java.util.*;
public class A1 {

	public static void main(String[] args) {
		
		int number; 
		
		/*To determine input number. */
		while(true){
			System.out.print("Please input a number(5~30):");
			Scanner scanner = new Scanner(System.in);
			number = scanner.nextInt();

			
			/*Determining */
			if (number<=30 && number>=5)	
				break;
			else
				System.out.println("Out of range!");//loop again			
		}
		
		/* Print out board. */
		for(int i=0;i<number;i++){
			for(int j=0;j<number;j++){
				if((i+j)%2==0)  
					System.out.print("X");
				else
					System.out.print("O");				
			}
			
			System.out.println(); //change line
		
		}	
		
	}

}
