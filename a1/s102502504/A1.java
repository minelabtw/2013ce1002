package ce1002.A1.s102502504;

import java.util.Scanner; 

public class A1 
{

	public static void main(String[] args) 
	{
		int num; //宣告變數(使用者輸入的值)
		System.out.print("Please input a number (5~30): "); //輸出字串
		Scanner scn = new Scanner(System.in); //讀取使用者輸入的值
		num=scn.nextInt(); //將讀取到的值丟給num
		
		while( num>30 || num<5 ) //當num大於30小於5時執行下列程式(判斷是否超出範圍)
		{
			System.out.println("Out of range!"); //輸出字串
			System.out.print("Please input a number (5~30): "); //輸出字串
			Scanner a = new Scanner(System.in); //讀取使用者輸入的值
			num=a.nextInt(); //將讀取到的值丟給a
		}
		
		int x=1,y=1; //宣告變數
		while( x<=num && y<=num ) //當x和y小於num時執行下列程式
		{
			if(x%2==1 && y%2==1) //奇數排 奇數列 輸出O
			{
				System.out.print("O");
			}
			if(x%2==0 && y%2==1) //奇數排 偶數列 輸出X
			{
				System.out.print("X");
			}
			if(x%2==1 && y%2==0) //偶數排 奇數列 輸出O
			{
				System.out.print("X");
			}
			if(x%2==0 && y%2==0) //偶數排 偶數列 輸出X
			{
				System.out.print("O");
			}
			x++; //往右推移一格
			
			if(x==num+1) //因為到最後x會比num多一(上方的x++造成)
			{
				System.out.println(); //此時要換行
				y++; //把y+1(用來計列)
				x=1; //把x歸回1
			}	
		}
	}
}
