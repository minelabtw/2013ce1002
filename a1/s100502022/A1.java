package ce1002.a1.s100502022;

import java.util.Scanner;

public class A1 {
	public static void main(String args[]){
			Scanner s = new Scanner(System.in);
			int number;
			boolean option =false;
			System.out.print("Please input a number(5~��0):");
			number = s.nextInt();
			do{
				
				if(number<5||number>30){
					System.out.print("Out of range!\nPlease input again(5~��0):");
					number = s.nextInt();
				}
				else{
					for(int i = 0;i<number;i++){
						for(int j = 0;j<number;j++){
							if((i+j)%2==0){
								System.out.print("X");
							}
							else{
								System.out.print("O");
							}
						}
						System.out.println();
					}
					option = true;
				}
			}while(option==false);
	}
}
