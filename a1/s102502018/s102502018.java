package ce1002.A1.s102502018;

import java.util.Scanner;

public class s102502018 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		Scanner scan = new Scanner (System.in);		
		System.out.print("Please input a number (5~30): ");
		int number = scan.nextInt();		
		while(number>30||number<5)
		{
			System.out.println("Out of range!");
			System.out.print("Please input again(5~30): ");
			number = scan.nextInt();
		}
		for(int a=0;a<number;a++)
		{
			if(a%2==0)
			{
				for(int b=0;b<number;b++)
				{
					if(b%2==0)
					{
						System.out.print("X");
					}
					else
						System.out.print("O");
				}
			}
			else
			{
				for(int c=0;c<number;c++)
				{
					if(c%2==0)
					{
						System.out.print("O");
					}
					else
						System.out.print("X");
				}
			}
			System.out.println("");
		}
	}
}
