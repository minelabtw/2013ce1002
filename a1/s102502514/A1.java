package ce1002.a1.s102502514;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");
		int number = input.nextInt();
		while (number>30 || number<5) {
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			number = input.nextInt();
		}
		for(int y=1; y<=number; y++) {
			for (int x=1; x<=number; x++) {
				if (y%2==1) {
				if (x%2==1)
					System.out.print("X");
				else 
					System.out.print("O");
				}
				else if (y%2==0) {
					if (x%2==1)
						System.out.print("O");
					else
						System.out.print("X");
				}
			}
			System.out.println("");
		}
	}
}
