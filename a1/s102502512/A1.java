package ce1002.a1.s102502512;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		int n;
		System.out.print("Please input a number(5~30): ");
		do{
		n=scanner.nextInt();
		if(n<5||n>30)
			{
			System.out.println("Out of range!");
		    System.out.print("Please input again(5~30): ");
		    }
		}while(n<5||n>30);
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				if(i%2==0&&j%2==0)
					System.out.print("X");
				else if(i%2==0&&j%2==1)
					System.out.print("O");
				else if(i%2==1&&j%2==0)
					System.out.print("O");
				else if(i%2==1&&j%2==1)
					System.out.print("X");
			}
			System.out.println();
		}
	}

}
