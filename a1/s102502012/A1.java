package ce1002.A1.s102502012;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n;
		
		System.out.print("Please input a number (5~30): ");
		n = input.nextInt();
		while(n > 30 || n < 5){
			System.out.println("Out of range !");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt();
		}
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				if((i + j) % 2 == 0)
					System.out.print('O');
				else
					System.out.print('X');
			}
			System.out.println("");
		}
	}

}
