package ce1002.a1.s101201023;

import java.util.Scanner;

public class A1 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		int integer , n=0 ;
		Scanner enter = new Scanner(System.in);
		
		System.out.print("Please input a number (5~30): ");
		integer = enter.nextInt();
		while(integer < 5 || integer > 30)
		{
			System.out.println("Out of range!");
			System.out.println("Please input again (5~30): ");
			integer = enter.nextInt();
		}
		
		for(int i=0 ; i < integer ; i++)
		{
			for(int j=0 ; j <integer ; j++)
			{
				if((n+j) % 2==0)
					System.out.print("X");
				
				else
					System.out.print("O");
			}
			System.out.println();
			n++;
		}
	}

}
