package ce1002.a1.s102502025;
import java.util.Scanner;		//驅動指令
public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		//允許輸入
		int n = 0;		//create a variable
		while(n<5 || n>30)		//假如條件不符合，將進行迴圈。
		{
			System.out.print("Please input a number (5~30): ");		//顯示輸入條件
			n = input.nextInt();		//輸入
			if(n<5 || n>30)		//假如符合條件，將進行一下條件。
			{
				System.out.print("Out of range!\n");
			}
		}
		
		for(int x=0 ; x<n ; x++)		//進行畫圖
		{
			for(int y=0 ; y<n ; y++)
			{
				if(y%2==0)
				{
					if(x%2==0)
					{
						System.out.print("X");
					}
					else
					{
						System.out.print("O");
					}
				}
				else
				{
					if(x%2==0)
					{
						System.out.print("O");
					}
					else
					{
						System.out.print("X");
					}
				}
			}
			System.out.print("\n");
		}

	}
}
