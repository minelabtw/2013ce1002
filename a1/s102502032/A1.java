package ce1002.a1.s102502032;

import java.util.Scanner;

public class A1
{
	public static void main(String[] args)
	{
		int size;
		size = getsize();
		printtable(size);
	}

	// ask for the size of table
	public static int getsize()
	{
		// declaration
		int size = 0;
		Scanner jin = new Scanner(System.in);
		// range=[5,30]
		do
		{
			System.out.print("Please input a number (5~30): ");
			size = jin.nextInt();
			if (size < 5 || size > 30)
				System.out.println("Out of range!");
		}
		while (size < 5 || size > 30);
		return size;
	}

	// print the table
	public static void printtable(int size)
	{
		for (int i = 0; i < size; i ++)
		{
			for (int j = 0; j < size; j ++)
			{
				// if i + j is odd, print father o
				// if it is even, print father x
				if ((i + j) % 2 != 0)
					System.out.print("O");
				else
					System.out.print("X");
			}
			// change line
			System.out.println();
		}
	}
}
