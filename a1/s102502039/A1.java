package ce1002.a1.s102502039;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");// 輸出問題
		int number = input.nextInt();// 輸入數字
		while (number < 5 || number > 30) {
			System.out.print("Out of range!\n");
			System.out.print("Please input again (5~30): ");
			number = input.nextInt();
		}
		int a = 0;// 進行輸出
		for (int i = 0; i < number; i++) {
			for (int k = a; k < (number + a); k++) {
				if (k % 2 == 0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.print("\n");
			a++;
		}

	}

}
