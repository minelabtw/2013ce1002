package ce1002.a1.s102502535;

import java.util.Scanner ;  //

public class A1 {

	public static void main(String[] args) {

	    Scanner input = new Scanner (System.in) ;

	    System.out.println ("Please input a number (5~30): " ) ;  //輸出字串。
	    int length = input.nextInt() ;  //輸入數字。
	    
	    while ( length < 5 || length > 30 )
	    {
	    	System.out.println("Out of range!") ;
	    	System.out.println("Please input again (5~30): " ) ;
	 	    length = input.nextInt() ;
	    }  //判斷是否在範圍內，若無則使再輸入一次。
       
        int x = 0 ;
        int y = 0 ;
        
        while ( y < length )
        {
        	while ( x < length )
        	{
        	if ( (x + y)%2 == 0 )
        		System.out.print ("X") ;
        	else if ( (x + y)%2 == 1 )
        		System.out.print("O") ;
        	x ++ ;
        	}
        	System.out.println() ;
        	x = 0 ;
            y ++ ;
        }  //輸出X和O鄉間隔的棋盤。
	    
	    input.close() ;
	}

}
