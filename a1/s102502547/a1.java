package ce1002.a1.s102502547;
import java.util.Scanner;
public class a1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		int a = 0;
		do
		{
		System.out.print("Please input a number (5~30): "); //輸入數字，範圍不合的話重新輸入
		a = scanner.nextInt();
		if(a<5 || a>30)
			System.out.println("Out of range!");
		}while(a<5 || a>30);
		
		for(int i=1;i<=a;i++) //輸出棋盤
		{
			for(int j=1;j<=a;j++)
			{
			if((i+j)%2==0)
				System.out.print("X");
			if((i+j)%2==1)
				System.out.print("O");
			}	
			System.out.println("");
		}
	}
}
