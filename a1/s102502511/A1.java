package ce1002.a1.s102502511;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 0;
	    Scanner enter = new Scanner(System.in);
	    
	    System.out.print("Please input a number (5~30): ");
	    num = enter.nextInt();
	    
	    while(num > 30 || num < 5)
	    {
	    	System.out.println("Out of range!");
	    	System.out.print("Please input again (5~30): ");
	    	num = enter.nextInt();
	    }
	    
	    Boolean y = new Boolean(false);
	    
	    for(int i = num ; i > 0 ; i--)
	    {
	    	Boolean x = y;
	    	for(int j = num ; j > 0 ; j--)
	    	{
	    		if(x == true)
	    		{
	    			System.out.print('O');
	    		}
	    		else 
	    		{
	    			System.out.print('X');
	    		}
	    		x = !x;
	    	}
	    	System.out.println();
	    	y = !y;
	    }
		}
	}


