package ce1002.a1.s102502561;

import java.util.Scanner;

public class A1 {
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a number (5~30): ");
		
		int n = input.nextInt();
		while(n>30||n<5)
		{
					System.out.println("Out of range!");
					System.out.println("Please input again (5~30): ");
					n = input.nextInt();
					
		}
					for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				if((i+j)%2==0)
				{
				System.out.print("X");
				}
				else if((i+j)%2==1)
						{
							System.out.print("O");
						}
			}
				System.out.println();
		}
	}
}
