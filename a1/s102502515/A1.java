package ce1002.a1.s102502515;
import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//建立輸入處理物件
		int a;
		
		do{//利用do迴圈確認input在範圍內
		System.out.print("Please input a number (5~30): ");
		 a = input.nextInt();
		if (a>30 || a<5)
			{
			System.out.println("Out of range!");
			}
		}
		while(a > 30 || a < 5);

		for (int x=1 ; x<=a ; x++)//x當作橫軸位置，由1開始
		{
			for (int y=1 ; y<=a ; y++)//y當作縱軸位置，由1開始
			{
				if ((x+y)%2==0)//當x+y相加為偶數時，為X
				{
					System.out.print("X");
				}
				else//奇數為O
					System.out.print("O");
			}
			System.out.println();//換行
		}
	}
}
