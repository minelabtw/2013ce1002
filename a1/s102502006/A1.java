package ce1002.a1.s102502006;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Please input a number (5~30): ");
		Scanner scanner = new Scanner(System.in);
		
		int input = 0;
		while(input<5 || input>30){ // �P�_��J
			input = scanner.nextInt();
			if(input<5 || input>30){
				System.out.println("Out of Range!");
				System.out.print("Please input again (5~30): ");
			}
		}
		
		int output = 0;
		for(int i=0; i<input*input; i++){
			if(output%2==0){
				System.out.print("X");
				output++;
			}
			else {
				System.out.print("O");
				output--;
			}
			if((i+1)%input==0){
				System.out.println();
				output = ((input%2==0) ? output+1 : output); // �P�_��J���_��
				// if(input%2==0)output++;
			}
		}
	}
}
