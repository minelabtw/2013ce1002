package ce1002.a1.s102502556;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		System.out.print("Please input a number (5~30): "); 
		int num = input.nextInt(); //將下一個輸入的整數存進變數  num 裡
		while( num < 5 || num > 30 ) //判斷是否合乎標準，否則要求重新輸入
		{
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			num = input.nextInt();
		}
		//用雙層for迴圈做出對應的棋盤
		for ( int i = 0 ; i < num ; i++ ) 
		{
			for ( int j = 0 ; j < num ; j++)
			{
				if ( i % 2 == 0 )
				{
					if ( j % 2 == 0 )
					{
						System.out.print("X");
					}
					else 
					{
						System.out.print("O");
					}	
				}
				else
				{
					if ( j % 2 == 0 )
					{
						System.out.print("O");
					}
					else 
					{
						System.out.print("X");
					}	
				}
			}
			System.out.println("");
		}
	}
}
