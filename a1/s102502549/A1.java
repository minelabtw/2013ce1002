package ce1002.a1.s102502549;

import java.util.Scanner; // import Scanner類別

public class A1 
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);

		System.out.print("Please input a number (5~30): ");

		int n;

		char[] c={'X','O'};

		do
		{
			n=input.nextInt(); // 輸入n

			if(n<5||n>30)
			{
				System.out.println("Out of range !");
		        System.out.print("Please input again(5~30): ");
			}
			
		}while(n<5||n>30);

		for(int i=0;i<n;i++)
		{
			for(int j=0;j<n;j++)
			{
				if((i+j)%2==0) // 用i+j是奇數或偶數來決定印出X或O
					System.out.print(c[0]);
				else
					System.out.print(c[1]);
			}

			System.out.println();
		}
	}
}