package ce1002.a1.s102502040;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		Scanner input = new Scanner(System.in);//建立一個輸入器
		System.out.print("Please input a number (5~30): ");
		int square = input.nextInt();//輸入數字讓輸入器讀取
		//設定輸入範圍
		while (square >30 || square < 5 ){
			System.out.println("Out of range!");
			System.out.print("Please input a number (5~30): ");
			square = input.nextInt();
		}
		//設計坐標軸,然後判斷輸出X或是O,然後圖形為邊長為輸入值的正方形
		for(int y=1 ; y<= square ; y++)	{
			for(int x=1; x<=square ; x++){
				int Axis = x+y;
				if(Axis % 2 == 0){
					System.out.print("X");
				}
				if(Axis % 2 !=0){
					System.out.print("O");
				}
			}	
			System.out.print("\n");
		}				
	}

}