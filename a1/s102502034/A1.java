package ce1002.a1.s102502034;

import java.util.Scanner;

public class A1 {
	public static void main(String[] args) {
		Scanner a = new Scanner(System.in);
		int num1;
		do {
			System.out.print("Please input a number (5~30): ");
			num1 = a.nextInt();
			if (num1 < 5 || num1 > 30)
				System.out.println("Out of range!");
		} while (num1 < 5 || num1 > 30);
		for (int i = 1; i <= num1; i++) {
			for (int c = 1; c <= num1; c++) {
				if ((i + c) % 2 == 0)
					System.out.print("X");
				else
					System.out.print("O");
			}
			System.out.print("\n");
		}
		// TODO Auto-generated method stub
	}

}
