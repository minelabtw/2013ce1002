package ce1002.a1.s102502544;

import java.util.Scanner;

public class A1 {
	public static void main(String args[]){
		int n = 0; //數字
		Scanner input = new Scanner(System.in);

		System.out.print("Please input a number (5~30): "); //輸入數字
		n = input.nextInt();

		while (n > 30 || n < 5) {  //重新輸入
			System.out.println("Out of range!");
			System.out.print("Please input again (5~30): ");
			n = input.nextInt();
		}

		int ox1 = 0;

		for (int i = n; i > 0; i--) {  //輸出棋盤
			int ox2 = ox1;
			for (int j = n; j > 0; j--) {
				if (ox2 == 1) {
					System.out.print('O');
					ox2 = 0;
				} else {
					System.out.print('X');
					ox2 = 1;
				}
				if(j == 1){
					System.out.println();
				}
			}
			if(ox1 == 1){
				ox1 = 0;
			}else {
				ox1 = 1;
			}
		}
	}
}
