package ce1002.A1.s102502049;

import java.util.Scanner;

public class A1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 5;

		do // set number
		{
			if (number < 5 || number > 30) {
				System.out.print("Out of range!\n");
			}
			System.out.print("Please input a number (5~30): ");
			number = input.nextInt();
		} while (number < 5 || number > 30);

		for (int i = 1; i <= number; i++) // draw figure
		{
			if (i % 2 == 1) {
				for (int j = 1; j <= number; j++) {
					if (j % 2 == 1) {
						System.out.print("O");
					} else {
						System.out.print("X");
					}
				}
				System.out.print("\n");
			} else {
				for (int j = 1; j <= number; j++) {
					if (j % 2 == 1) {
						System.out.print("X");
					} else {
						System.out.print("O");
					}
				}
				System.out.print("\n");
			}
		}

	}

}
