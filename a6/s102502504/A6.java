package ce1002.a6.s102502504;

import java.awt.*;
import javax.swing.*;

public class A6 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		// Initialized hero array with length of 3
		Hero[] heros = new Hero[3];
		
		/*
		 * Initialized different hero with different index
		 */
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		/*
		 * Use polymorphism to print out the result of different 
		 * hero's name,hp,mp,pp
		 */
		for(Hero hero : heros)
		{
			System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
		}
		
		JFrame myframe = new JFrame();
		myframe.setSize(300,750);
		myframe.setLocationRelativeTo(null); //把元件置中於螢幕
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel mypanel1 = new JPanel(); //建立容器
		
		mypanel1.add(new JLabel("Wizard HP:6.0 MP:21.0 PP:3.0"));
		ImageIcon image1 = new ImageIcon("Wizard.jpg");
		mypanel1.add(new JLabel(image1));
		
		mypanel1.add(new JLabel("Swordsman HP:3.0 MP:3.0 PP:24.0"));
		ImageIcon image2 = new ImageIcon("Swordsman.jpg");
		mypanel1.add(new JLabel(image2));
		
		mypanel1.add(new JLabel("Knight HP:24.0 MP:3.0 PP:3.0"));
		ImageIcon image3 = new ImageIcon("Knight.jpg");
		mypanel1.add(new JLabel(image3));
		
		myframe.add(mypanel1);
		myframe.setVisible(true);
	}

}
