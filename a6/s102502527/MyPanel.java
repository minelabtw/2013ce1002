package ce1002.a6.s102502527;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel
{
	JLabel info;
	JLabel img;
	ImageIcon icon;
	
	MyPanel()
	{
		this.setSize(280, 230);
		this.setBorder(new LineBorder(Color.black, 5));
	}
	
	public void setRoleState(Hero hero)
	{
		icon = new ImageIcon(hero.getId() + ".jpg");
		info = new JLabel(hero.getId() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		img = new JLabel(icon);
		this.add(info);
		this.add(img);
	}
}

