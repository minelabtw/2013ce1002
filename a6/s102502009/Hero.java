package ce1002.a6.s102502009;

public class Hero {

	double hp = 30.0;
	double mp = 30.0;
	double pp = 30.0;
	String name = "";

	public void nameSetter(String name) {
		this.name = name;
	}

	public void hpSetter(double hp) {
		this.hp = hp;
	}

	public void mpSetter(double mp) {
		this.mp = mp;
	}

	public void ppSetter(double pp) {
		this.pp = pp;
	}

	public String nameGetter() {
		return name;
	}

	public double hpGetter() {
		return hp;
	}

	public double mpGetter() {
		return mp;
	}

	public double ppGetter() {
		return pp;
	}
}
