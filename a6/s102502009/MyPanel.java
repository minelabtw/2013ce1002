package ce1002.a6.s102502009;
import javax.swing.JPanel ;
import javax.swing.JLabel ;
import javax.swing.ImageIcon ;
import java.awt.GridLayout ;
import javax.swing.JFrame ;
public  class MyPanel extends JPanel
{
		protected JLabel text ;
		protected ImageIcon image ;
		public MyPanel(Hero hero,String str)
		{
			
			image= new ImageIcon(str) ;
			text= new JLabel(hero.nameGetter()+" HP: "+hero.hpGetter()+" MP: "+hero.mpGetter()+" PP: "+hero.ppGetter()) ;
		}
		
		
}
