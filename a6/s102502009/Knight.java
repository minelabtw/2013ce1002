package ce1002.a6.s102502009;

public class Knight extends Hero {
	public Knight() {
		name = "Knight";
	}

	public String nameGetter() {
		return name;
	}

	public double hpGetter() {
		return hp * 0.8;
	}

	public double mpGetter() {
		return mp * 0.1;
	}

	public double ppGetter() {
		return pp * 0.1;
	}
}
