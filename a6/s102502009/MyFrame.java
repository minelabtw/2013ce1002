package ce1002.a6.s102502009;
import java.awt.*;
import javax.swing.JFrame; 
import javax.swing.JPanel; 
import java.awt.GridLayout ;
import javax.swing.JLabel ;
public class MyFrame extends JFrame
{
	public MyFrame(MyPanel p1 ,MyPanel p2 ,MyPanel p3 )
	{
		setLayout(new GridLayout(6,1)) ;
		setSize(250,190) ;
		add(p1.text) ;
		add(new JLabel(p1.image)) ;
		add(p2.text) ;
		add(new JLabel(p2.image)) ;
		add(p3.text) ;
		add(new JLabel(p3.image)) ;
		
	}
	
}
