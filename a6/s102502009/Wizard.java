package ce1002.a6.s102502009;

public class Wizard extends Hero {
	public Wizard() {
		name = "Wizard";
	}

	public String nameGetter() {
		return name;
	}

	public double hpGetter() {
		return hp * 0.2;
	}

	public double mpGetter() {
		return mp * 0.7;
	}

	public double ppGetter() {
		return pp * 0.1;
	}
}
