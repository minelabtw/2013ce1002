package ce1002.a6.s102502009;

public class Swordsman extends Hero {
	public Swordsman() {
		name = "Swordsman";
	}

	public String nameGetter() {
		return name;
	}

	public double hpGetter() {
		return hp * 0.1;
	}

	public double mpGetter() {
		return mp * 0.1;
	}

	public double ppGetter() {
		return pp * 0.8;
	}
}
