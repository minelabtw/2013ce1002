package ce1002.a6.s102502507;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	String a;
	String b;

	public MyPanel() {
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}

	public void setRoleState(Hero hero) {
		a = hero.getname() + " HP: " + hero.getHP() + " MP: " + hero.getMP()
				+ " PP: " + hero.getPP();
		b = hero.getname() + ".jpg";
	}

	public void realadd() {
		add(new JLabel(a));
		ImageIcon image = new ImageIcon(b);
		add(new JLabel(image));
	}
}
