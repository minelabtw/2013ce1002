package ce1002.a6.s102502507;

public class Wizard extends Hero {
	String name = "Wizard";

	public Wizard() {
		super();
	}

	public String getname() {
		// 得name
		return name;
	}

	public double getHP() {
		// 得HP
		return HP * 0.2;
	}

	public double getMP() {
		// 得MP
		return MP * 0.7;
	}

	public double getPP() {
		// 得PP
		return PP * 0.1;
	}
}
