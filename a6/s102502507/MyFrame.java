package ce1002.a6.s102502507;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	public MyFrame(Hero hero1, Hero hero2, Hero hero3) {
		MyPanel panel1 = new MyPanel();
		MyPanel panel2 = new MyPanel();
		MyPanel panel3 = new MyPanel();
		newRolePos(hero1, panel1, 5, 0);
		newRolePos(hero2, panel2, 5, 240);
		newRolePos(hero3, panel3, 5, 480);
		panel1.realadd();
		panel2.realadd();
		panel3.realadd();
		add(panel1);
		add(panel2);
		add(panel3);
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		setLayout(null);
		panel.setRoleState(hero);
		panel.setBounds(x, y, 260, 230);
	}
}
