package ce1002.a6.s102502507;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();// use construct
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame(hero[0], hero[1], hero[2]);
		frame.setSize(300, 800);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
