package ce1002.a6.s102502507;

public class Swordsman extends Hero {
	String name = "Swordsman";

	public Swordsman() {
		super();
	}

	public String getname() {
		// 得name
		return name;
	}

	public double getHP() {
		// 得HP
		return HP * 0.1;
	}

	public double getMP() {
		// 得MP
		return MP * 0.1;
	}

	public double getPP() {
		// 得PP
		return PP * 0.8;
	}
}
