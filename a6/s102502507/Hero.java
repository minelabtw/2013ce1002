package ce1002.a6.s102502507;

public class Hero {
	// 宣告變數
	public String name;
	public double HP = 30;
	public double MP = 30;
	public double PP = 30;

	public Hero() {
	}

	public void setHP() {
		HP = 30;
	}

	public void setMP() {
		MP = 30;
	}

	public void setPP() {
		PP = 30;
	}

	public String getname() {
		// 得name
		return name;
	}

	public double getHP() {
		// 得HP
		return HP;
	}

	public double getMP() {
		// 得MP
		return MP;
	}

	public double getPP() {
		// 得PP
		return PP;
	}
}
