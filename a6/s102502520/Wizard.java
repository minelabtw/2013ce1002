package ce1002.a6.s102502520;

public class Wizard extends Hero{
	public Wizard(float HP, float MP, float PP) {//constructor
		super("Wizard",HP*0.2f, MP*0.7f, PP*0.1f);
		// TODO Auto-generated constructor stub
		OutputWizard();
	}
	public void OutputWizard()
	{
		System.out.println(getName()+" HP: "+getHP()+" MP: "+getMP()+" PP: "+getPP());
	}
}
