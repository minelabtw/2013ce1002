package ce1002.a6.s102502520;

public class Hero {
	private String name;
	private float HP;
	private float MP;
	private float PP;
	
	public Hero(String name,float HP,float MP,float PP)//constructor
	{
		setName(name);
		setHP(HP);
		setMP(MP);
		setPP(PP);
	}
	public void setName(String name) //setter for name
	{
		this.name = name;
	}
	public void setHP(float HP) //setter for HP
	{
		this.HP=HP;
	}
	public void setMP(float MP) //setter for MP
	{
		this.MP=MP;
	}
	public void setPP(float PP) //setter for PP
	{
		this.PP=PP;
	}
	public String getName() //getter for name
	{
		return name;
	}
	public float getHP() //getter for HP
	{
		return HP;
	}
	public float getMP() //getter for MP
	{
		return MP;
	}
	public float getPP() //getter for PP
	{
		return PP;
	}
}
