package ce1002.a6.s102502551;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	JLabel[] label = new JLabel[2];
	
	public void setRoleState(Hero hero){
		ImageIcon image = new ImageIcon(hero.getname()+".jpg");
		
		label[0]=new JLabel();
		label[1]=new JLabel();
		
		setLayout(null);
		
		label[0].setBounds(5,5,300,10);
		label[1].setBounds(5,20,300,180);
		
		add(label[0]);
		add(label[1]);
		
		label[0].setText(hero.getname()+" HP:"+hero.getHP()+" MP:"+hero.getMP()+" PP:"+hero.getPP());
		label[1].setIcon(image);
	}

}
