package ce1002.a6.s102502551;

public class Hero {
	private double HP=30;
	private double MP=30;
	private double PP=30;
	private String name;
	
	public Hero(){
	}
	public void setname(String a){
		name=a;
	}
	public void setHP(double a){
		HP=a;
	}
	public void setMP(double a){
		MP=a;
	}	
	public void setPP(double a){
		PP=a;
	}	
	public String getname(){
		return name;
	}
	public double getHP(){
		return HP;
	}	
	public double getMP(){
		return MP;
	}	
	public double getPP(){
		return PP;
	}	
}
