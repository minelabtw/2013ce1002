package ce1002.a6.s102502551;

public class Knight extends Hero {
	public Knight(){
		setname("Knight");
	}
	public double getHP(){
		return super.getHP()*0.8;
	}	
	public double getMP(){
		return super.getMP()*0.1;
	}	
	public double getPP(){
		return super.getPP()*0.1;
	}
}
