package ce1002.a6.s102502551;

public class Wizard extends Hero {
	public Wizard(){
		setname("Wizard");
	}
	public double getHP(){
		return super.getHP()*0.2;
	}	
	public double getMP(){
		return super.getMP()*0.7;
	}
	public double getPP(){
		return super.getPP()*0.1;
	}
}
