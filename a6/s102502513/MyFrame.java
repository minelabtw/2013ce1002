package ce1002.a6.s102502513;

import javax.swing.*;
import java.awt.*;
 
public class MyFrame extends JFrame{
	public MyFrame() {
		setSize(270, 770); //set the size of frame
		setLayout(new GridLayout(3, 1, 5, 5)); //split the frame into 3x1 blocks
		setVisible(true);
	}
	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setSize(x,y); //set frame
		panel.setRoleState(hero); //put the information of hero into panel
		add(panel); //put panel into frame
	}
}