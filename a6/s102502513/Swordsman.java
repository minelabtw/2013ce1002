package ce1002.a6.s102502513;

public class Swordsman extends Hero {
	public Swordsman() { //construct Swordsman class
		Name = "Swordsman";
	}
	
	public float HP_getter() { //set HP
		return HP * 0.1f;
	}
	
	public float MP_getter() { //set MP
		return MP * 0.1f;
	}
	
	public float PP_getter() { //set PP
		return PP * 0.8f;
	}
}