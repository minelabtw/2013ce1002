package ce1002.a6.s102502513;

public class Wizard extends Hero {
	public Wizard() { //construct Wizard class
		Name = "Wizard";
	}
	
	public float HP_getter() { //set HP
		return HP * 0.2f;
	}
	
	public float MP_getter() { //set MP
		return MP * 0.7f;
	}
	
	public float PP_getter() { //set PP
		return PP * 0.1f;
	}
}