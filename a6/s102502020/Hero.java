package ce1002.a6.s102502020;

public class Hero {
	
	private String name;            //英雄名稱
	private double HP = 30;         //生命點數
	private double MP = 30;         //魔法點數
	private double PP = 30;         //能力點數
	
	public Hero(){
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHP() {
		return HP;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public double getMP() {
		return MP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public double getPP() {
		return PP;
	}
	public void setPP(double pP) {
		PP = pP;
	}

}
