package ce1002.a6.s102502544;

public class Hero {
	private String name;
	protected float HP=30;
	protected float MP=30;
	protected float PP=30;
	
	Hero(){//建構子
		
	}

	public String getName() {//輸出名稱
		return name;
	}

	public void setName(String name) {//設定名稱的函式
		this.name = name;
	}

	public float getHP() {
		return HP;
	}

	public void setHP(float hP) {
		HP = hP;
	}

	public float getMP() {
		return MP;
	}

	public void setMP(float mP) {
		MP = mP;
	}

	public float getPP() {
		return PP;
	}

	public void setPP(float pP) {
		PP = pP;
	}

}
