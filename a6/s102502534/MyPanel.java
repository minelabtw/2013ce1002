package ce1002.a6.s102502534;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	//宣告2個 JLabel 使用，一個用來顯示角色的各項屬性狀態
	JLabel state = new JLabel() ;
	JLabel image = new JLabel() ;
	
	MyPanel()
	{ 
		setSize(280,235) ;
		setBorder(new LineBorder(Color.black, 5)) ;
	}
	public void setRoleState(Hero hero)
	{
		state = new JLabel(hero.getHeroName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP()) ;
		ImageIcon image2 = new ImageIcon(hero.getHeroName()+".jpg") ;
		image = new JLabel(image2) ;
		//加到版面中
		add(state) ; 
		add(image) ;
	}
}
