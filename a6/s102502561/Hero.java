package ce1002.a6.s102502561;

public class Hero {//英雄

	private float HP = 30.0f;//初始值
	private float MP = 30.0f;
	private float PP = 30.0f;
	private String job;

	public Hero() {//建構子

	}

	public String getjob() {//職業
		return job;
	}

	public float getHP() {
		return HP;
	}

	public float getMP() {
		return MP;
	}

	public float getPP() {
		return PP;
	}

	public void setHP(float HP) {
		this.HP = HP;
	}

	public void setMP(float MP) {
		this.MP = MP;
	}

	public void setPP(float PP) {
		this.PP = PP;

	}

	public void setjob(String job) {
		this.job = job;
	}

}
