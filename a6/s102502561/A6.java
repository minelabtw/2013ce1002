package ce1002.a6.s102502561;

import javax.swing.JFrame;

public class A6 {
	public static void main(String[] args) {

		MyFrame frame = new MyFrame();//這是視窗
		MyPanel[] panel = new MyPanel[3];//這是圖的框框
		Hero hero[] = new Hero[3];//三個英雄陣列
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		frame.setSize(320,860);//設定視窗大小
		frame.setLocationRelativeTo(null);//讓圖框置中
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//點擊關閉按鈕,執行隱藏窗口
		for (int i=0;i<3;i++)
		{
			panel[i] = new MyPanel();//新圖框
			frame.newRolePos(hero[i], panel[i], 10, 10 + 250 * i);//英雄職業,這支職業的框框預設-->MyPanel()-->this.setSize(280, 240);
		}
		frame.setVisible(true);

		}
	}


