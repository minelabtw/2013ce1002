package ce1002.a6.s102502039;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.border.*;

public class MyPanel extends JPanel {//set output standard
	public MyPanel(Hero hero) {
		setLayout(new BorderLayout());
		add(new JLabel(hero.getHeroName() + " HP:"
				+ String.valueOf(hero.getHP()) + " MP:"
				+ String.valueOf(hero.getMP()) + " PP:"
				+ String.valueOf(hero.getPP())), BorderLayout.NORTH);
		ImageIcon image = new ImageIcon(hero.getHeroName() + ".jpg");
		add(new JLabel(image), BorderLayout.SOUTH);
		Border lineborder = new LineBorder(Color.BLACK, 3);
		setBorder(lineborder);
	}
}
