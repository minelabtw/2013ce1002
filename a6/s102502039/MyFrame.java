package ce1002.a6.s102502039;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JFrame;
public class MyFrame extends JFrame {

	public MyFrame() {
		
		setLayout(new FlowLayout(FlowLayout.LEFT,3,3));//output
		add(new MyPanel(new Wizard()));
		
		add(new MyPanel(new Swordsman()));
		
		add(new MyPanel(new Knight()));
		}
}
