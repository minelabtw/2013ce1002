package ce1002.a6.s102502039;

public class Hero {
	private String heroname;// declare variable
	private double HP=30;
	private double MP=30;
	private double PP=30;

	public Hero() {// constructor

	}

	public void set(String heroname, double HP, double MP, double PP) {// set
																		// variable
		setHeroname(heroname);
		setHp(HP);
		setMP(MP);
		setPP(PP);
	}

	public void setHeroname(String heroname) {// set variable
		this.heroname = heroname;
	}

	public void setHp(double HP) {// set variable
		this.HP = HP;
	}

	public void setMP(double MP) {// set variable
		this.MP = MP;
	}

	public void setPP(double PP) {// set variable
		this.PP = PP;
	}

	public String getHeroName() {// function
		return heroname;
	}

	public double getHP() {// function
		return HP;
	}

	public double getMP() {// function
		return MP;
	}

	public double getPP() {// function
		return PP;
	}

}
