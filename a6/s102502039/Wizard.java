package ce1002.a6.s102502039;

public class Wizard extends Hero {

	public Wizard() {
		super();
		super.setHeroname("Wizard");
		}

	public double getHP() {// function
		return super.getHP() * 0.2;

	}

	public double getMP() {// function
		return super.getMP() * 0.7;
	}

	public double getPP() {// function
		return super.getPP() * 0.1;
	}

}
