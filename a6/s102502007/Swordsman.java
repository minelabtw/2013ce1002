package ce1002.a6.s102502007;

public class Swordsman extends Hero {
	public Swordsman()
	{
		setHeroName("Swordsman");
	}//constructor
	public float getHP()
	{
		return HP*0.1f;
	}//overriding
	public float getMP()
	{
		return MP*0.1f;
	}//overriding
	public float getPP()
	{
		return PP*0.8f;
	}//overriding

}
