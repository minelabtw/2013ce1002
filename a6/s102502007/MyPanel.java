package ce1002.a6.s102502007;
import javax.swing.*;

public class MyPanel extends JPanel{
	public MyPanel()
	{
		
	}
	protected ImageIcon image;
	protected JLabel text;
	
	public void setRoleState(Hero hero)
	{
		text = new JLabel(hero.getHeroName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		add(text);
	}

}
