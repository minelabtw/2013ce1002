package ce1002.a6.s102502007;

public class Hero {
	public Hero()
	{
		
	}//constructor
	private String heroName;
	protected float HP = 30;
	protected float MP = 30;
	protected float PP = 30;
	//Polymorphism 
	public void setHeroName(String heroName)
	{
		this.heroName = heroName;
	}//setter
	public void setHP(float HP)
	{
		this.HP = HP;
	}//setter
	public void setMP(float MP)
	{
		this.MP = MP;
	}//setter
	public void setPP(float PP)
	{
		this.PP = PP;
	}//setter
	public String getHeroName()
	{
		return heroName;
	}//getter
	public float getHP()
	{
		return HP;
	}//getter
	public float getMP()
	{
		return MP;
	}//getter
	public float getPP()
	{
		return PP;
	}//getter
}
