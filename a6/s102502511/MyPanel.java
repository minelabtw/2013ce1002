package ce1002.a6.s102502511;

import javax.swing.*;

public class MyPanel extends JPanel{
	JLabel lab1 , lab2 ;
	
	MyPanel(){
		setLayout(null);
		setSize(250 , 200); //label的大小
		setBounds(0, 0, 250, 200); //label的範圍 (0,0)~(250,200)
	}
	
	public void setRoleState(Hero hero){
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg"); //輸出圖片為職業+jpg
		lab1 = new JLabel(hero.getName() + " HP: " + hero.getHP()
							+ " MP: " + hero.getMP() + " PP:" + hero.getPP()); //輸出文字
		lab1.setSize(250,10); //lab1的大小
		lab1.setLocation(0, 0);//lab1的起始位置
		lab2 = new JLabel(icon);//lab2中叫出圖片
		lab2.setSize(250,190);//lab2的大小
		lab2.setLocation(0, 10);//lab2的起始位置
		add(lab1);//增lab至panel
		add(lab2);
	}
}

