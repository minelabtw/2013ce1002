package ce1002.a6.s102502511;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.border.*;

public class MyFrame extends JFrame{
	MyPanel pan1 = new MyPanel(); //pan1為MyPanel其中1種
	MyPanel pan2 = new MyPanel();
	MyPanel pan3 = new MyPanel();
	
	MyFrame(){
		setSize(250,600); //Frame大小
		setLayout(null);
		Hero [] hero = new Hero [3]; 
		
		hero[0] = new Wizard(); //每個陣列各為一個職業
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		newRolePos(hero [0] , pan1 , 0 , 0 ); //使用newRolePos 來 定義職業 + pan 即位置
		newRolePos(hero [1] , pan2 , 0 , 200 );
		newRolePos(hero [2] , pan3 , 0 , 400 );
		
		add(pan1); //增加pan到frame
		add(pan2);
		add(pan3);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setBorder(new LineBorder(Color.BLACK , 2)); //邊框
		panel.setRoleState(hero);
		panel.setLocation(x,y);
	}
}