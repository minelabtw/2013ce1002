package ce1002.a6.s102502511;

public class Hero 
{
	private String name;
	protected double HP;//使用protected 讓父類別和子類別可以用同一物件的參數
	protected double MP;
	protected double PP;
	
	Hero()
	{
		HP = 30.0;
		MP = 30.0;
		PP = 30.0;
	}
	
	public String getName() 
	{
		return name;
	}
	public double getHP() 
	{
		return HP;
	}
	public double getMP() 
	{
		return MP;
	}
	public double getPP() 
	{
		return PP;
	}
	
	
	public void setName(String heroname) 
	{
		name = heroname;
	}
	public void setHP(double hP) 
	{
		HP = hP;
	}
	public void setMP(double mP) 
	{
		MP = mP;
	}
	public void setPP(double pP) 
	{
		PP = pP;
	}
	
}