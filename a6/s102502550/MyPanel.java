package ce1002.a6.s102502550;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private JLabel state,picture;
	private int index = 0;
	
	public MyPanel(){
		
		
	}
	public void setRoleState(Hero hero){             //設定圖片與角色狀態
		
		state = new JLabel(hero.getName() +" HP:"+ hero.getHP() + " MP:"+ hero.getMP()+" PP:"+ hero.getPP());
		picture = new JLabel( new ImageIcon(hero.getName()+".jpg"));
		add(state);
		add(picture);
		setBounds(0, 0+260*index, 210, 240);
		
	}
	
}
