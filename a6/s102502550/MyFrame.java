package ce1002.a6.s102502550;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;

public class MyFrame extends JFrame{
	private MyPanel[] p = new MyPanel[3];
	private int index = 0 ; 
	
	public MyFrame(int w, int h){
		setSize(w,h);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(3,1));
		
	}
	public void newRolePos(Hero hero , int x , int y){                 //設定panel位置大小
		p[index] = new MyPanel();
		p[index].setRoleState(hero);
		p[index].setBorder(BorderFactory.createLineBorder(Color.black));
		add(p[index]);
		index++;
	}
	
}