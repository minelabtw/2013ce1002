package ce1002.a6.s102502508;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import java.awt.Color;
public class MyPanel extends JPanel 
{
	public MyPanel()
	{
		setSize(263,230);
		setBorder(new LineBorder(Color.BLACK,3));
	}
	
	   public void setRoleState(Hero hero)
	   {
		   
		   
		   JLabel text = new JLabel(hero.getHerosName()+" HP:"+hero.getHP()+" MP:"+hero.getMP()+" PP:"+hero.getPP());
		   add(text); 
		   JLabel image = new JLabel(new ImageIcon("C:/Users/user/Desktop/Java/102502508/src/ce1002/a6/s102502508/image/"+hero.getHerosName()+".jpg"));
		   
		   add(image);
	   }
  
}
