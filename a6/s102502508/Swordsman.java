package ce1002.a6.s102502508;

public class Swordsman extends Hero {
	
	public Swordsman()
	{
		setHerosName("Swordsman");
		setHP(HP);
		setMP(MP);
		setPP(PP);
	}

	
	public void setHP(float hP) {
		HP = (float) (hP*0.1);
	}

	

	public void setMP(float mP) {
		
		MP = (float) (mP*0.1);
	}

	

	public void setPP(float pP) {
		PP = (float)(pP*0.8);
	}

}
