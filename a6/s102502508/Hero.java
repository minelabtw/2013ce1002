package ce1002.a6.s102502508;

public class Hero {

	private String HerosName  ;
	protected float  HP=(float)30.0   ;
	protected float  MP=(float)30.0   ;
	protected float  PP=(float)30.0   ;
	public String getHerosName() {
		return HerosName;
	}
	public void setHerosName(String herosName) {
		HerosName = herosName;
	}
	public float getHP() {
		return HP;
	}
	public void setHP(float hP) {
		HP = hP;
	}
	public float getMP() {
		return MP;
	}
	public void setMP(float mP) {
		MP = mP;
	}
	public float getPP() {
		return PP;
	}
	public void setPP(float pP) {
		PP = pP;
	}
	
}
