package ce1002.a6.s102502024;

public class Swordsman extends Hero{
	String name="Swordsman";
	public Swordsman()
	{
		
	}
	public String getname()
	{
		return name;
	}
	public double gethp()  //設定HP
	{
		return HP*0.1;
	}
	public double getmp()  //設定MP
	{
		return MP*0.1;
	}
	public double getpp()  //設定PP
	{
		return PP*0.8;
	}
}
