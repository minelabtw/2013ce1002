package ce1002.a6.s102502024;

public class Hero {
	private String Heroname;  //宣告變數
    protected int HP=30;
    protected int MP=30;
    protected int PP=30;
    public void setname(String name)  //設定名子
	{
		Heroname=name;
	}
	public void sethp(int HP)  //設定HP
	{
		this.HP=HP;
	}
	public void setmp(int MP)  //設定MP
	{
		this.MP=MP;
	}
	public void setpp(int PP)  //設定PP
	{
		this.PP=PP;
	}
	public String getname()  //回傳名子
	{
		return Heroname;
	}
	public double gethp()  //回傳HP
	{
		return HP;
	}
	public double getmp()  //回傳MP
	{
		return MP;
	}
	public double getpp()  //回傳PP
	{
		return PP;
	}
}
