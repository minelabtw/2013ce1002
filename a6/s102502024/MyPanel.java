package ce1002.a6.s102502024;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JLabel title;
	JLabel image;
	ImageIcon con;
	public MyPanel()
	{
		setSize(250,230);
		setBorder(new LineBorder(Color.black, 5));
	}
	public void setRoleState(Hero hero)
	{
		con=new ImageIcon(hero.getname()+".jpg");
		image=new JLabel(con);
		title=new JLabel(hero.getname()+"HP:"+hero.gethp()+"MP:"+hero.getmp()+"PP:"+hero.getpp());
		add(title);
		add(image);
	}
}