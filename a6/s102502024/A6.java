package ce1002.a6.s102502024;
import javax.swing.*;
public class A6 {

	public static void main(String[] args) {
		Hero Hero[]=new Hero[3];  //宣告物件
		Hero[0]=new Wizard();
		Hero[1]=new Swordsman();
		Hero[2]=new Knight();
		MyFrame frm=new MyFrame();
		MyPanel[] MyPanel = new MyPanel[3];
		frm.setSize(350,800);
		frm.setLocationRelativeTo(null);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for(int i=0;i<3;i++)
		{
			MyPanel[i]=new MyPanel();
			frm.newRolePos(Hero[i], MyPanel[i], 10, 10 + 240 * i);
		}
		frm.setVisible(true);
	}
}
