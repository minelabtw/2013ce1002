package ce1002.a6.s102502024;

public class Wizard extends Hero{
	String name="Wizard";
	public Wizard()
	{
		
	}
	public String getname()
	{
		return name;
	}
	public double gethp()  //設定HP
	{
		return HP*0.2;
	}
	public double getmp()  //設定MP
	{
		return MP*0.7;
	}
	public double getpp()  //設定PP
	{
		return PP*0.1;
	}
}
