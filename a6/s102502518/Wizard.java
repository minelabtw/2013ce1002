package ce1002.a6.s102502518;

public class Wizard extends Hero {
	
	public Wizard() {//constructor
		
	}
    @Override
    public String GetName() {//overriding
		return "Wizard";	
	}
	public double GetHP() {//overriding
		return super.GetHP()*0.2;
	}
	public double GetMP() {//overriding
		return super.GetMP()*0.7;
	}
	public double GetPP() {//overriding
		return super.GetPP()*0.1;
	}	
}
