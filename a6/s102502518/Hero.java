package ce1002.a6.s102502518;

public class Hero {
	
	private String Name;
	private float HP;
	private float MP;
	private float PP;
	public Hero() {//constructor
		
	}
	public void SetHP() {//Set HP
		HP = 30;
	}
	public void SetMP() {//Set MP
		MP = 30;
	}
	public void SetPP() {//Set PP
		PP = 30;
	}
	public String GetName() {//Get Name
		return Name;
	}
	public double GetHP() {//Get HP
		return HP;
	}
	public double GetMP() {//Get MP
		return MP;
	}
	public double GetPP() {//Get PP
		return PP;
	}
}
