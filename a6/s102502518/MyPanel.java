package ce1002.a6.s102502518;
import javax.swing.JPanel ;
import javax.swing.JLabel ;
import javax.swing.ImageIcon ;
import java.awt.GridLayout ;
import javax.swing.JFrame ;
public  class MyPanel extends JPanel
{
		protected JLabel text ;
		protected ImageIcon image ;
		public MyPanel()
		{
			
			
		}
		public void setRoleState(Hero hero,String str)
		{
			image= new ImageIcon(str) ;
			text= new JLabel(hero.GetName()+" HP: "+hero.GetHP()+" MP: "+hero.GetMP()+" PP: "+hero.GetPP()) ;
			add(text) ;
			add(new JLabel(image)) ;
		}
		
}
