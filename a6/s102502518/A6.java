package ce1002.a6.s102502518;
import java.awt.FlowLayout ;
import java.awt.BorderLayout;
import javax.swing.* ;
public class A6 {

	public static void main(String[] args) {	
		Hero [] heros = new Hero[3];//array
		
		heros[0] = new Wizard(); 
		heros[0].SetHP();
		heros[0].SetMP();
		heros[0].SetPP();
		heros[1] = new Swordsman(); 
		heros[1].SetHP();
		heros[1].SetMP();
		heros[1].SetPP();
		heros[2] = new Knight(); 
		heros[2].SetHP();
		heros[2].SetMP();
		heros[2].SetPP();
		
		MyPanel p1 =new MyPanel() ;
		MyPanel p2 =new MyPanel() ;
		MyPanel p3 =new MyPanel() ;
		
		p1.setRoleState(new Knight(),"D:/Programming Language/Java/ce1002.a6.s102502518/src/ce1002/a6/s102502518/Knight.jpg"); 
		p2.setRoleState(new Swordsman(),"D:/Programming Language/Java/ce1002.a6.s102502518/src/ce1002/a6/s102502518/Swordsman.jpg" ) ;
		p3.setRoleState(new Wizard(),"D:/Programming Language/Java/ce1002.a6.s102502518/src/ce1002/a6/s102502518/Wizard.jpg") ;
		
		
		MyFrame frame= new MyFrame(p1,p2,p3) ;
		
		frame.setSize(270,700) ;
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true);
		}

}
