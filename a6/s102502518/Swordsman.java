package ce1002.a6.s102502518;

public class Swordsman extends Hero {
	
	public Swordsman() {//constructor
		
	}
	@Override
    public String GetName() {//overriding
		return "Swordsman";
	}
	public double GetHP() {//overriding
		return super.GetHP()*0.1;
	}
	public double GetMP() {//overriding
		return super.GetMP()*0.1;
	}
	public double GetPP() {//overriding
		return super.GetPP()*0.8;
	}
}
