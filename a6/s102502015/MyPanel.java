package ce1002.a6.s102502015;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {

	public JLabel lb1;
	public JLabel lb2;

	public MyPanel() {
		this.setLayout(null);    //非預設排版
		this.setBounds(0, 0, 250, 200);  //邊界
		this.setSize(250, 200);			//大小
	}

	public void setRoleState(Hero hero) {
		ImageIcon icon = new ImageIcon(hero.getname() + ".jpg");  //圖片物件
		lb1 = new JLabel(icon);	//LABEL 1 讀圖片 設置大小 位置
		lb1.setSize(250, 190);
		lb1.setLocation(0, 10);
		this.add(lb1);			//加到Panel�堶�
		lb2 = new JLabel();		//LABEL 2 放文字 設置大小位置
		lb2.setText(hero.getname() + " HP:" + hero.gethp() + " MP:"
				+ hero.getmp() + " PP:" + hero.getpp());
		lb2.setSize(250, 10);
		lb2.setLocation(0, 0);
		this.add(lb2);			//加到Panel�堶�
	}

}