package ce1002.a6.s102502015;

public class Swordsman extends Hero {

	public float gethp() {
		return hp / 10;
	}

	public float getmp() {
		return mp / 10;
	}

	public float getpp() {
		return pp * 8 / 10;
	}
}
