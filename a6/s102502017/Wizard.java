package ce1002.a6.s102502017;

import javax.swing.ImageIcon;

public class Wizard extends Hero{
	Wizard(){
		super();
		setName("Wizard");
		setImg( new ImageIcon(getName() + ".jpg"));		//set image
	}
	 double getHp(){
		return super.getHp() * 0.2;
	}
	
	 double getMp(){
		return super.getMp() * 0.7;
	}
	
	 double getPP(){
		return super.getPP() * 0.1;
	}
}