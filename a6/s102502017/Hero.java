//Hello World
package ce1002.a6.s102502017;

import javax.swing.ImageIcon;

public class Hero {
	
	private String name;
	private double hp=30;
	private double mp=30;
	private double pp=30;
	private ImageIcon image;
	
	//set varibles
	 void setName(String name){
		this.name = name;
	}
	
	 void setHp(double hp){
		this.hp = hp;
	}
	
	 void setMp(double mp){
		this.mp = mp;
	}
	
	 void setPP(double pp){
		this.pp = pp;
	}
	 
	void setImg(ImageIcon image){
		this.image = image; 
	}
	//get varibles
	 ImageIcon getImg(){
		 return image;
	 }
	 
	 String getName(){
		return name;
	}
	
	 double getHp(){
		return this.hp;
	}
	
	 double getMp(){
		return mp;
	}
	
	 double getPP(){
		return pp;
	}
	
}
