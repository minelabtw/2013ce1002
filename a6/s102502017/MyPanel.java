//Thank you,Seal
package ce1002.a6.s102502017;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

public class MyPanel extends JPanel{
	
	JLabel labeltext;
	JLabel labelimg;	
	
	MyPanel(Hero hero, int x, int y, int width, int height) {
        setBounds(x, y, width, height);
        Border border = new LineBorder(Color.black, 5);
        setBorder(border); 

        setRoleState(hero);
        add(labeltext);
        add(labelimg);
    }
	
	
	public void setRoleState(Hero hero){
		String state = hero.getName();
		state += " HP: " + hero.getHp();
		state += " MP: " + hero.getMp();
		state += " PP: " + hero.getPP();
		
		labeltext = new JLabel(state);
		labeltext.setHorizontalAlignment(JLabel.RIGHT);
		
		labelimg = new JLabel(hero.getImg());
	}

}
