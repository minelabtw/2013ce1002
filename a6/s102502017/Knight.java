package ce1002.a6.s102502017;

import javax.swing.ImageIcon;

public class Knight extends Hero{
	Knight(){
		super();
		setName("Knight");
		setImg( new ImageIcon(getName() + ".jpg"));		//set image
	}
	public double getHp(){
		return super.getHp() * 0.8;
	}
	
	public double getMp(){
		return super.getMp() * 0.1;
	}
	
	public double getPP(){
		return super.getPP() * 0.1;
	}
}