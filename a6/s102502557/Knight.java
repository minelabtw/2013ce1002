package ce1002.a6.s102502557;

public class Knight extends Hero{
	
	public Knight()
	{
		set_id("Knight");
	}
	
	public float get_hp()
    {
       return super.get_hp() * 0.8f;
    }
	public float get_mp()
	{
	    return super.get_mp() * 0.1f;
	}
	public float get_pp()
	{
		return super.get_pp() * 0.1f;
	}
	
}
