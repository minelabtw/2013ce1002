package ce1002.a6.s102502557;

import javax.swing.JFrame;//要import才能繼承JFrame

public class MyFrame extends JFrame{
	MyFrame()
	{
		this.setLayout(null);//變成預設狀態
	}
    
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		this.add(panel);//把panel加進frame裡面 
		panel.setState(hero);
		panel.setLocation(x,y);//繼承自Jpanel
	}
}
