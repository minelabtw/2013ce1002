package ce1002.a6.s102502557;

import java.util.Scanner;
import javax.swing.JFrame;

public class A6 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		Hero[] hero;//宣告一個Hero形態的hero陣列
		hero=new Hero[3];//設定大小   一行解：Hero[] hero = new Hero[3]
		hero[0]= new Wizard();//hero[0]變成wizard形態而不再是hero
		hero[1]= new Swordsman();
		hero[2]= new Knight();
		
		MyPanel[] panel = new MyPanel[3];
		MyFrame frame = new MyFrame();
		frame.setSize(280,750);//視窗一跳出來的大小
		frame.setLocationRelativeTo(null);//他會出現在哪個位置
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//讓x有功能
		
		for(int n=0; n<3; n++)
		{
			panel[n] = new MyPanel();
			frame.newRolePos(hero[n], panel[n], 10, 10 + 240 * n);
		}
		frame.setVisible(true);
	
	}
	

}
