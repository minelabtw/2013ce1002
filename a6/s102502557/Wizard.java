package ce1002.a6.s102502557;

public class Wizard extends Hero{
	public Wizard()//建構元 id設好 而且不能用 void 修飾字只能有public or private
	{
		set_id("Wizard");
	}
	@Override//＠這是標記 只能標記關鍵字
	public float get_hp()
    {
       return super.get_hp() * 0.2f;//super.的意義就是呼叫父類別
    }	
	
	public float get_mp()
	{
		return super.get_mp() * 0.7f;//而且一定要super 呼叫自己
	}
	
	public float get_pp()
	{
		return super.get_pp() *0.1f;
	}


}
