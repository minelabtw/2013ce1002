package ce1002.a6.s102502557;

import javax.swing.JLabel;
import javax.swing.JPanel;//要import才能繼承
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;//邊線的
import java.awt.Color;//顏色在awt裡面
public class MyPanel extends JPanel{
JLabel info;
JLabel img;
ImageIcon icon;//就是一個存圖黨的
MyPanel()//constructor
{
	this.setSize(280,230);//用this指向物件，免得名子相同的時候就有問題了
	LineBorder border = new LineBorder(Color.black, 5);//宣告一個lineborder類別
	this.setBorder(border);//setborder函數 可以放顏色 寬度等引數	
}

public void setState(Hero hero)//()內要有一個引數 代表Hero類別
{
	info = new JLabel(hero.get_id() + " HP: " + hero.get_hp() + " MP: " + hero.get_mp() + " PP: " + hero.get_pp() );//在()中打什麼 就會把什麼放進 info 這個標籤
    icon = new ImageIcon(hero.get_id() + ".jpg");//()內的引數是圖片的名稱 而且要副黨名
    img = new JLabel(icon);//把icon放入 img 這個標籤    
    
    this.add(info);//把label加到panel上面去
    this.add(img);
}

}
