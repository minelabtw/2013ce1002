package ce1002.a6.s102502501;
public class Hero {
	
	private String id;
	private float hp;
	private float mp;
	private float pp;
	
	public Hero(){
		hp = 30;
		mp = 30;
		pp = 30;
	}
	
	public void setId(String id){
		this.id = id;
	}
	public void setHP(float hp){
		this.hp = hp;
	}
	public void setMp(float mp){
		this.mp = mp;
	}
	public void setPp(float pp){
		this.pp = pp;
	}
	
	public String getId()
	{
	  return id;
	}
	public float getHp()
	{
	  return hp;
	}
	public float getMp()
	{
	  return mp;
	}
	public float getPp()
	{
	  return pp;
	}

}
