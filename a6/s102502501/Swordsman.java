package ce1002.a6.s102502501;
public class Swordsman extends Hero{
	
	public Swordsman(){
		setId("Swordsman");
	}
	
	public String getId()
	{
	  return super.getId();
	}
	public float getHp()
	{
	  return super.getHp()*0.1f;
	}
	public float getMp()
	{
	  return super.getMp()*0.1f;
	}
	public float getPp()
	{
	  return super.getPp()*0.8f;
	}
}

