package ce1002.a6.s102502501;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	public MyFrame(){
		setLayout(null); // 板子顯現
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		this.add(panel); // 加上畫布
		panel.setRoleState(hero); // 畫布樣式
		panel.setLocation(x, y); // 畫布位置
	}
}
