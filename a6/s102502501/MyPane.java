package ce1002.a6.s102502501;
import java.awt.*;
import javax.swing.*;
/*import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;*/
import javax.swing.border.LineBorder; // 邊框


public class MyPanel extends JPanel{
	
	
	MyPanel()
	{
		setSize(275, 230); // 畫布大小
		setBorder(new LineBorder(Color.black, 5)); // 畫布邊框
	}

	JLabel name; // 標籤資訊
	JLabel picture; // 標籤圖片
	ImageIcon photo; // 圖片
	
	public void setRoleState(Hero hero)
	{
		photo = new ImageIcon(hero.getId() + ".jpg"); // 放入圖片
		name = new JLabel(hero.getId() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp()); // 放入資訊
		picture = new JLabel(photo); // 圖片放入標籤
		this.add(name); // 畫布加上資訊
		this.add(picture); // 畫布加上圖片
	}
}
