package ce1002.a6.s101201003;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	
	private JLabel type;//properties
	private JLabel image;//properties
	
	
	public MyPanel()
	{
		type=new JLabel();
		setLayout(null);
	}//constructures
	
	
	public void setRoleState(Hero hero)
	{
		
		type.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		type.setBounds(6, 6, 250, 20);
		add(type);
		
		
		ImageIcon _image;
		if(hero.getName()=="Wizard")
			_image=new ImageIcon("Wizard.jpg");
		else if(hero.getName()=="Swordsman")
			_image=new ImageIcon("Swordsman.jpg");
		else
			_image=new ImageIcon("Knight.jpg");
		
		image=new JLabel(_image);
		image.setBounds(6, 27,_image.getIconWidth(),_image.getIconHeight());
		add(image);
	}
}
