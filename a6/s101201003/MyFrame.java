package ce1002.a6.s101201003;

import java.awt.Color;

import javax.swing.BorderFactory;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	
	private MyPanel knight;//properties
	private MyPanel swordsman;//properties
	private MyPanel wizard;//properties
	
	
	public MyFrame()
	{
		knight=new MyPanel();
		swordsman=new MyPanel();
		wizard=new MyPanel();
		
		setSize(283, 800);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}//constructures
	
	
	public void newRolePos(Hero hero,MyPanel panel,int x,int y)
	{
		 panel.setRoleState(hero);
		 panel.setBorder(BorderFactory.createLineBorder(Color.black,5));
		 panel.setBounds(x, y, 263, 225);
		 add(panel);
		 
	}//method
	
	public MyPanel getpanel(Hero hero)
	{
		if(hero.getName()=="Wizard")
			return wizard;
		else if(hero.getName()=="Swordsman")
			return swordsman;
		else
			return knight;
	}
}
