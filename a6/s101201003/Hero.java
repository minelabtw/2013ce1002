package ce1002.a6.s101201003;

public class Hero {
	private String Name;
	private double HP;
	private double MP;
	private double PP;
	

	//setter
	public void setName(String name){
		Name=name;
	}
	public void setHP(double hp){
		HP=hp;
	}
	public void setMP(double mp){
		MP=mp;
	}
	public void setPP(double pp){
		PP=pp;
	}
	
	//getter
	public String getName(){
		return Name;
	}
	public double getHP(){
		return HP;
	}
	public double getMP(){
		return MP;
	}
	public double getPP(){
		return PP;
	}

}
