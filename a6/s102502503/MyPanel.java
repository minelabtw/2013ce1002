package ce1002.a6.s102502503;
import java.awt.Color;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	
	JLabel labtext = new JLabel(); //屬性
	JLabel labicon = new JLabel();  //圖片
	
	MyPanel()
	{ 
		setSize(280,230);
		setBorder(new LineBorder(Color.black, 3));  //邊框
	}
	public void setRoleState(Hero hero)
	{
		labtext = new JLabel(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP: "+hero.getPp());
		ImageIcon pic = new ImageIcon(hero.getName()+".jpg");  //載入圖片
		labicon = new JLabel(pic);
		
		add(labtext); 
		add(labicon);
	}
}
