package ce1002.a6.s102502503;
import javax.swing.*;

public class A6 {

	public static void main(String[] args) {
		Hero [] hero = new Hero [3];  //宣告陣列
		hero[0]=new Wizard();
		hero[1]=new Swordsman();
		hero[2]=new Knight();
		hero[0].setName("Wizard");  //設定名字
		hero[1].setName("Swordsman");
		hero[2].setName("Knight");
		
		MyFrame frame = new MyFrame();
		frame.setSize(310, 770);  //設定視窗大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		
		MyPanel[] panel = new MyPanel[3];  //宣告陣列
				
		for(int i=0; i<3; i++){
			panel[i] = new MyPanel() ;
			frame.newRolePos(hero[i], panel[i], 5, 10+240*i);
		}
		frame.setVisible(true);  //顯示視窗
	}
}
