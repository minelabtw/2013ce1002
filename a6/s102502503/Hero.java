package ce1002.a6.s102502503;

public class Hero {
	private String name;
	protected double hp = 30.0;  //宣告為保護成員
	protected double mp = 30.0;
	protected double pp = 30.0;
	
	public Hero(){
	
	}
	public String getName() {  //取得
		return name;
	}
	public void setName(String name) {   //設定
		this.name = name;
	}
	public double getHp() {  //取得
		return hp;
	}
	public void setHp(double hp) { //設定
		this.hp = hp;
	}
	public double getMp() {  //取得
		return mp;
	}
	public void setMp(double mp) {  //設定
		this.mp = mp;
	} 
	public double getPp() {  //取得
		return pp;
	}
	public void setPp(double pp) {  //設定
		this.pp = pp;
	}
	
}
