package ce1002.a6.s102502034;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyFrame(Hero[] hero) {
		MyPanel myPanel = new MyPanel("Wizard.jpg");
		MyPanel myPanel_1 = new MyPanel("Swordsman.jpg");
		MyPanel myPanel_2 = new MyPanel("Knight.jpg");
		// create panel
		newRolePos(hero[0],myPanel,0,0);
		newRolePos(hero[1], myPanel_1,0, 230);
		newRolePos(hero[2],myPanel_2,0,460);
		//call newRolePos
		setTitle(" ");
		//set title
		setVisible(true);
		// let the frame visible
		
		setSize(280,750);
		// set frame size 
		 addWindowListener(new WindowAdapter() {
		        public void windowClosing(WindowEvent e) {
		          System.exit(0);
		        }
		      });
		 //Close

	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		setLayout(null);
		//not using auto layout
		panel.setBounds(x,y,260,230);
		//set position and area of panel
		panel.setRoleState(hero);
		//set panel
		add(panel);
		//add panel to frame
		

	}
	

}
