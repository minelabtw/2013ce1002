package ce1002.a6.s102502034;

public class Hero {
	public float Hp=30;
	public float Mp=30;
	public float Pp=30;
	public String HeroName;

	// 設定變數

	public void setMp() {
		this.Mp = 30;
	}

	// 將MP設定為30
	public void setHp() {
		this.Hp = 30;
	}

	// 將Hp設定為30
	public void setPp() {
		this.Pp = 30;
	}

	// 將Pp設定為30
	public void setHeroName(String HeroName) {
		HeroName = " ";
		this.HeroName = HeroName;
	}

	// 設定名稱
	public double getMp() {
		return Mp;
	}

	// 回傳Mp
	public double getHp() {
		return Hp;
	}

	// 回傳Hp
	public double getPp() {
		return Pp;
	}

	// 回傳Pp
	public String getHeroName() {
		return HeroName;
	}

}
