package ce1002.a6.s102502034;

import ce1002.a6.s102502034.Hero;

public class Knight extends Hero {
	public double getMp() {

		return Mp * 0.1;
	}

	public double getHp() {
		return Hp * 0.8;
	}

	public double getPp() {
		return Pp * 0.1;
	}

	public String getHeroName() {
		return this.HeroName = "Knight";
	}
	// 回傳乘上騎士比例後的數值

}
