package ce1002.a6.s100502022;

import javax.swing.*;

public class MyPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel picture;
	private JLabel status;
	public void setRoleState(Hero hero){
		//sum of the data
		String str = hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP();
		ImageIcon image=new ImageIcon(hero.getName()+".jpg");
		//new object
		picture= new JLabel(image);
		status= new JLabel(new String(str));
		add(status);
		add(picture);
	}
}
