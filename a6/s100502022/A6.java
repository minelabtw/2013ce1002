package ce1002.a6.s100502022;

import javax.swing.JFrame;

public class A6 {
	public static void main(String argsp[]){
		//initial data
		Hero[] h= new Hero[3];
		h[0]=new Wizard();
		h[0].setName("Wizard");
		h[1]=new Swordsman();
		h[1].setName("Swordsman");
		h[2]=new Knight();
		h[2].setName("Knight");
		
		
		MyFrame myFrame = new MyFrame();
		myFrame.setSize(320, 750);
		int x=100,y=250;
		myFrame.newRolePos(h[0], new MyPanel(), x, y);
		myFrame.newRolePos(h[1], new MyPanel(), x, y);
		myFrame.newRolePos(h[2], new MyPanel(), x, y);
		myFrame.setVisible(true);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
