package ce1002.a6.s100502022;
//base class
public class Hero {
	//protected for derived class
	protected double hp=30.0;
	protected double pp=30.0;
	protected double mp=30.0;
	private String name;
	//set,get
	public void setHP(double hp){
		this.hp=hp;
	}
	public void setMP(double mp){
		this.mp=mp;
	}
	public void setPP(double pp){
		this.pp=pp;
	}
	public double getHP(){
		return hp;
	}
	public double getMP(){
		return mp;
	}
	public double getPP(){
		return pp;
	}
	public void setName(String name){
		this.name=name;
	}
	public String getName(){
		return name;
	}
}
