package ce1002.a6.s100502022;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;


public class MyFrame extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6224794790016567370L;
	private MyPanel knight;
	private MyPanel swordsman;
	private MyPanel wizard;
	//constructor
	public MyFrame(){
		setLayout(new GridLayout(3,1,5,5));
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		//set mypanel's data
		panel.setRoleState(hero);
		
		panel.setSize(x, y);
		panel.setBorder(new LineBorder(Color.black,5));
		//copy
		if(hero.getName()=="Knight"){
			knight=panel;
			add(knight);
		}
		else if(hero.getName()=="Swordsman"){
			swordsman=panel;
			add(swordsman);
		}
		else{
			wizard=panel;
			add(wizard);
		}
		
	}
	
}
