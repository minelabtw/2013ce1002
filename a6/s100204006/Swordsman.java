package ce1002.a6.s100204006;

public class Swordsman extends Hero
{
	public Swordsman(String Name, float HP, float MP, float PP) 
	{
		super("Swordsman", HP, MP, PP);
		// TODO Auto-generated constructor stub
	}
	public float getHP()
	{
		return (float) (super.getHP()*0.1) ;
	}
	public float getMP()
	{
		return (float) (super.getMP()*0.1);
	}
	public float getPP()
	{
		return (float) (super.getPP()*0.8);
	}
}
