package ce1002.a6.s100204006;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel 
{
	JLabel lb1, lb2;
	public MyPanel(Hero hero) 
	{
		setRoleState(hero);
		setLayout(null);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
	}
 
	public void setRoleState(Hero hero) 
	{
		ImageIcon icon = new ImageIcon(hero.getName()+".jpg");
		lb1 = new JLabel(icon, JLabel.CENTER);
		lb2 = new JLabel(hero.getName() +" HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP(), JLabel.CENTER);
		lb2.setBounds(0, 0, 200, 30);
		lb1.setBounds(3, 20, 200, 200);
		this.add(lb2);
		this.add(lb1);
	}
}
 
