package ce1002.a6.s101201504;
import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyPanel hero[]=new MyPanel [3];
		for (int i=0;i<3;i++)
		{
			hero[i]=new MyPanel();
		}
		Hero H[]=new Hero[3];
		H[0]=new Wizard();
		H[1]=new Swordsman();
		H[2]=new Knight();
		H[0].setid("Wizard");
		H[1].setid("Swordsman");
		H[2].setid("Knight");
		for ( int i=0;i<3;i++)      //setup number of start
		{
			H[i].sethp(30);
			H[i].setmp(30);
			H[i].setpp(30);
		}
		MyFrame F=new MyFrame();
		for (int i=0;i<3;i++)
		{
			F.newRolePos(H[i],hero[i],i,0);
		}
		F.setSize(270,700) ;              //set size
		F.setLocationRelativeTo(null) ;
		F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		F.setVisible(true);
	}

}
