package ce1002.a6.s101201504;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	MyFrame()
	{
		setLayout(new GridLayout(3,1));
	}
	
	public void newRolePos(Hero hero , MyPanel P , int x , int y)
	{
		P.setRoleState(hero);
		GridBagConstraints c = new GridBagConstraints();
        c.gridx = x;       //fill way
        c.gridy = y;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.WEST;
		add(P,c);
		
		 
	}
}
