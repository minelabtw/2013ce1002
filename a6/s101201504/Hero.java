package ce1002.a6.s101201504;

public class Hero {
	private String id;
	protected double hp=0;
	protected double mp=0;
	protected double pp=0;
	public void setid(String id){    //setup name
		this.id=id;
	}
	public void sethp(double hp){              //setup hp
		this.hp=hp;
	}
	public void setmp(double mp){     //setup mp
		this.mp=mp;
		}
	public void setpp(double pp){      //setup pp
		this.pp=pp;
	}
	public String getid(){
		return id ;
	}
	public double gethp(){
		return hp;
	}
	public double getmp(){
		return mp;
	}
	public double getpp(){
		return pp;
	}
}
