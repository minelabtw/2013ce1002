package ce1002.a6.s102502539;

import ce1002.a6.s102502539.Hero;

public class Wizard extends Hero
{
	public Wizard(float hp, float mp, float pp)
	{
		super("Wizard",hp*0.2f,mp*0.7f,pp*0.1f);
	}
}
