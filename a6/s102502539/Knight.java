package ce1002.a6.s102502539;

import ce1002.a6.s102502539.Hero;

public class Knight extends Hero
{
	public Knight(float hp, float mp, float pp)
	{
		super("Knight",hp*0.8f,mp*0.1f,pp*0.1f);
	}
}
