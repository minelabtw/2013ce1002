package ce1002.a6.s102502539;

public class Hero {

	private String name;
	private float hp;
	private float mp;
	private float pp;
	
	public Hero(String name ,float h , float m , float p)
	{
		//�U�س]�w
		setName(name);
		setHp(h);
		setMp(m);
		setPp(p);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setHp(float hp)
	{
		this.hp = hp;
	}
	
	public float getHp()
	{
		return hp;
	}
	
	public void setMp(float mp)
	{
		this.mp = mp;
	}
	
	public float getMp()
	{
		return mp;
	}
	
	public void setPp(float pp)
	{
		this.pp = pp;
	}
	
	public float getPp()
	{
		return pp;
	}
	
}
