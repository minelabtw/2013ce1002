package ce1002.a6.s102502547;

public class Knight extends Hero {

	Knight() {
		name = "Knight"; //命名
	}

	public double getHP() {
		return HP * 0.8; //加權
	}

	public double getMP() {
		return MP * 0.1; //加權
	}

	public double getPP() {
		return PP * 0.1; //加權
	}
}
