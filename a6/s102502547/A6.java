package ce1002.a6.s102502547;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		Hero[] character = new Hero[3];
		character[0] = new Wizard();
		character[1] = new Swordsman();
		character[2] = new Knight();
		
		MyFrame frame = new MyFrame(); //宣告一個名為frame的MyFrame型態的變數	
		frame.setSize(295, 750); //設定視窗大小
		frame.setLocationRelativeTo(null); //設定視窗位置
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //按視窗右上角的"X"會關閉程序
		
		MyPanel[] panel = new MyPanel[3]; //宣告一個名為panel的MyPanel型態陣列
		for ( int i = 0 ; i < 3 ; i++ ) //設定panel陣列裡的元素
		{
			panel[i] = new MyPanel();
			frame.newRolePos(character[i], panel[i], 5, 5 + 235 * i);
		}
		frame.setVisible(true); //使視窗能顯示在螢幕上
	}
}
