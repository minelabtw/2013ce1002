package ce1002.a6.s102502547;

public class Hero {

	String name;
	double HP=30;
	double MP=30;
	double PP=30;
	
	//setter
	public void setName(String name){
		this.name=name;
	}
	
	public void setHP(float hp){
		HP=hp;
	}
	
	public void setMP(float mp){
		MP=mp;
	}
	
	public void setPP(float pp){
		PP=pp;
	}
	
	//getter
	public String getName(){
		return name;
	}
	
	public double getHP(){
		return HP;
	}
	
	public double getMP(){
		return MP;
	}
	
	public double getPP(){
		return PP;
	}
}
