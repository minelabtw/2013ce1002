package ce1002.a6.s102502547;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	MyFrame () {
		this.setLayout(null); //設定各物件在視窗中的布局方式
	}
	
	public void newRolePos (Hero hero,MyPanel panel,int x,int y) {
		panel.setRoleState(hero); //設定panel的資料、圖片
		panel.setLocation(x, y); //設定panel的位置
		this.add(panel); //把設定好的panel添加到視窗裡
	}
}
