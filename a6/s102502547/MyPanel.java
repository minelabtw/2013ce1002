package ce1002.a6.s102502547;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	JLabel state; //英雄狀態
	JLabel image; //圖片資料
	ImageIcon icon; //儲存圖片路徑
	MyPanel () {
		this.setSize(270, 230); //設定大小
		this.setBorder(new LineBorder(Color.black, 3));	//設定邊界屬性
	}
	public void setRoleState(Hero hero) {
		icon = new ImageIcon(hero.getName()+".jpg"); //設定icon為圖片路徑
		state = new JLabel(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		//設定state屬性
		image = new JLabel(icon); //設定image屬性
		this.add(state); //添加到Panel
		this.add(image);
	}
}
