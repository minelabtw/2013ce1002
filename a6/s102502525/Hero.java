package ce1002.a6.s102502525;

public class Hero {
	private String id;
	private float hp;
	private float mp;
	private float pp;
	Hero()
	{
		hp = 30.0f;
		mp = 30.0f;
		pp = 30.0f;
	}
	//getter
	String getId()
	{
	  return id;
	}
	float getHp()
	{
	  return hp;
	}
	float getMp()
	{
	  return mp;
	}
	float getPp()
	{
	  return pp;
	}

	//setter
	void setId(String id)
	{
	  this.id = id;
	}
	void setHp(float hp)
	{
	  this.hp = hp;
	}
	void setMp(float mp)
	{
	  this.mp = mp;
	}
	void setPp(float pp)
	{
	  this.pp = pp;
	}

}

