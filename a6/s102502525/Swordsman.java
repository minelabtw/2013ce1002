package ce1002.a6.s102502525;
public class Swordsman extends Hero {
	Swordsman(){
		super();
		setId("Swordsman");
	}
	@Override
	float getHp(){
	  return super.getHp() * 0.1f;
	}
	@Override
	float getMp(){
		return super.getMp() * 0.1f;
	}
	@Override
	float getPp(){
		return super.getPp() * 0.8f;
	}
}
