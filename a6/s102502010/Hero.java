package ce1002.a6.s102502010;

public class Hero {
	private double HP;
	private double MP;
	private double PP;
	private String name;
	Hero()  //constructor hero
	{
		HP=30;
		MP=30;
		PP=30;
		name="";
	}
	public void setHP(double HP)
	{
		this.HP=HP;
	}
	public void setMP(double MP)
	{
		this.MP=MP;
	}
	public void setPP(double PP)
	{
		this.PP=PP;
	}
	public void setname(String name)
	{
		this.name=name;
	}
	public double getHP()
	{
		return HP;
	}
	public double getMP()
	{
		return MP;
	}
	public double getPP()
	{
		return PP;
	}
	public String getname()
	{
		return name;
	}
}
