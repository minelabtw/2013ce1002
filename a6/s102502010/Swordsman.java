package ce1002.a6.s102502010;

public class Swordsman extends Hero{
	private final double hpFix = 0.1;
	private final double mpFix = 0.1;
	private final double ppFix = 0.8;
	Swordsman()  //constructor swordsman
	{
		super.setname("Swordsman");
	}
	public double getHP()
	{
		return super.getHP()*hpFix;
	}
	public double getMP()
	{
		return super.getMP()*mpFix;
	}
	public double getPP()
	{
		return super.getPP()*ppFix;
	}
}
