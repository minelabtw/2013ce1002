package ce1002.a6.s102502010;

public class Wizard extends Hero{
	private final double hpFix = 0.2;
	private final double mpFix = 0.7;
	private final double ppFix = 0.1;
	Wizard()  //constructor wizard
	{
		super.setname("Wizard");
	}
	public double getHP()
	{
		return super.getHP()*hpFix;
	}
	public double getMP()
	{
		return super.getMP()*mpFix;
	}
	public double getPP()
	{
		return super.getPP()*ppFix;
	}
}
