package ce1002.a6.s102502010;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame{
	private MyPanel myPanel[];
	
	MyFrame(){
		JPanel jpanel = new JPanel();
		myPanel = new MyPanel[3];
		Hero heros[] = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		for(int i = 0; i < 3; i++){
			myPanel[i] = new MyPanel();
			newRolePos(heros[i], myPanel[i], 5, 5 + 225 * i);
			add(myPanel[i]);
			add(jpanel);
		}
	}
	
	public void newRolePos(Hero hero, MyPanel panel, int x, int y){
		panel.setRoleState(hero);
		panel.setLayout(null);
		panel.setBounds(x, y, 230, 215);
		panel.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panel.add(panel.getStatus());
		panel.add(panel.getImage());
	}
}
