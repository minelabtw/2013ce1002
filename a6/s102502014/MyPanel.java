package ce1002.a6.s102502014;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	public JLabel L1;
	public JLabel L2;

	public MyPanel() {
		this.setLayout(null);        //非預設排版
		this.setBounds(0,0,250,200); //邊界
		this.setSize(250,200);       //大小
	}
	public void setRoleState(Hero hero) {
		ImageIcon icon = new ImageIcon(hero.getNAME() + ".jpg"); //圖片物件
		L1 = new JLabel(icon); //LABEL 1 讀圖片 設置大小 位置
		L1.setSize(250, 190);
		L1.setLocation(0, 10);
		this.add(L1); //加到Panel�堶�
		L2 = new JLabel(); //LABEL 2 放文字 設置大小位置
		L2.setText(hero.getNAME() + " HP:" + hero.getHP() + " MP:"
		+ hero.getMP() + " PP:" + hero.getPP());
		L2.setSize(250, 10);
		L2.setLocation(0, 0);
		this.add(L2); //加到Panel�堶�
	}
}
