package ce1002.a6.s102502001;

public class Wizard extends Hero {
	//get the value and modify them
	public String getName(){
		setName("Wizard");
		return super.getName();
	}	
	
	public double getHP(){
		return super.getHP()*0.2;
	}
	
	public double getMP(){
		return super.getMP()*0.7;
	}
	
	public double getPP(){
		return super.getMP()*0.1;
	}
}

