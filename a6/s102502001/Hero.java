package ce1002.a6.s102502001;

public class Hero {
	//set variable
		private String name;
		private double HP=30.0;
		private double MP=30.0;
		private double PP=30.0;
		
		//declare setName,setHP,setMP,setPP
		public void setName(String name){
			this.name=name;
		}
		public void setHP(double HP){
			this.HP=HP;
		}
		public void setMP(double MP){
			this.MP=MP;
		}
		public void setPP(double PP){
			this.PP=PP;
		}
		
		//get setName,setHP,setMP,setPP
		public String getName(){
			return name;
		}
		public double getHP(){
			return HP;
		}
		
		public double getMP(){
			return MP;
		}
		
		public double getPP(){
			return PP;
		}
}
