package ce1002.a6.s102502001;

public class Swordsman extends Hero {
	//get the value and modify them
	public String getName(){
		setName("Swordsman");
		return super.getName();
	}	
	
	public double getHP(){
		return super.getHP()*0.1;
	}
	
	public double getMP(){
		return super.getMP()*0.1;
	}
	
	public double getPP(){
		return super.getMP()*0.8;
	}
}
