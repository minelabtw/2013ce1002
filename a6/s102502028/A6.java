package ce1002.a6.s102502028;
import javax.swing.JFrame;
import java.awt.BorderLayout ;
public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3] ; //宣告物件陣列
        hero[0] = new Wizard() ;  //初始化物件陣列
        hero[1] = new Swordsman() ;
        hero[2] = new Knight() ;
        
        MyFrame frame = new MyFrame() ;//設定框架
        frame.setSize(320,770) ;
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;	
		frame.setLayout(null);
        MyPanel[] panel = new MyPanel[3] ; //3個 MyPanel 變數存放3個角色用
        
        for (int a = 0 ; a < 3 ; a++)  //印出結果
        {
        	panel[a] =  new MyPanel();
        	frame.newRolePos(hero[a],panel[a],10,10+240*a) ;  
        	frame.add(panel[a]) ;
        	//System.out.println(hero[a].getName()+" HP: "+hero[a].getHP()+" MP: "+hero[a].getMP()+" PP: "+hero[a].getPP()) ;
        }
        frame.setVisible(true);
	}

}
