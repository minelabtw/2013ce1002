package ce1002.a6.s102502028;

public class Wizard extends Hero {
	Wizard()  //建構子
	{
		name = "Wizard" ;
	}
	
	String getName()  //回傳英雄名稱，生命點數(HP)，魔法點數(MP)以及能力點數(PP)
	{
		return name ;
	}
	
	double getHP()
	{
		setHP(30.0) ;
		return hp*0.2 ;
	}
	
	double getMP()
	{
		setMP(30.0) ;
		return mp*0.7 ;
	}
	
	double getPP()
	{
		setPP(30.0) ;
		return pp*0.1 ;
	}
}
