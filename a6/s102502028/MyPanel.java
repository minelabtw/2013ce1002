package ce1002.a6.s102502028;
import javax.swing.* ;
import java.awt.Color ;

import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JLabel state = new JLabel() ;
	JLabel image = new JLabel() ;
	
	MyPanel()  //設定版面
	{
		setSize(280,235) ;
		setBorder(new LineBorder(Color.black, 5)) ;
	}
	
	public void setRoleState(Hero hero){ //設定屬性還有圖片
		//new MyPanel();
		state = new JLabel(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP()) ;
		ImageIcon image2 = new ImageIcon(hero.getName()+".jpg") ;
		image = new JLabel(image2) ;
		add(state) ; //加到版面中
		add(image) ;
	}
}
