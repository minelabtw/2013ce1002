package ce1002.a6.s102502028;

public class Knight extends Hero {
	Knight()  //建構子
	{
		name = "Knight" ;
	}
	
	String getName()  //回傳英雄名稱，生命點數(HP)，魔法點數(MP)以及能力點數(PP)
	{
		return name ;
	}
	
	double getHP()
	{
		setHP(30.0) ;
		return hp*0.8 ;
	}
	
	double getMP()
	{
		setMP(30.0) ;
		return mp*0.1 ;
	}
	
	double getPP()
	{
		setPP(30.0) ;
		return pp*0.1 ;
	}
}
