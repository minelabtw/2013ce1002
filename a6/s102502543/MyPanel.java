package ce1002.a6.s102502543;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
public class MyPanel extends JPanel {
	public void setRoleState(Hero hero){
		setBorder(new LineBorder(Color.black, 5));
		JLabel ch = new JLabel(hero.getName() + " HP: " + hero.getHp()
				+ " MP: " + hero.getMp() + " PP: " + hero.getPp());	
		ImageIcon image = new ImageIcon(hero.getName()+".jpg");
		JLabel pic = new JLabel(image);
		add(ch);
		add(pic);
	}
}
