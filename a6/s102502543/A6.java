package ce1002.a6.s102502543;
import java.awt.*; 
import java.awt.event.*; 

public class A6 {
	public static void main(String[] args) {
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame();
		MyPanel panel = new MyPanel();
		frame.setSize(280,750);
		panel.setBounds(0, 0, 100, 100);
		frame.newRolePos(hero[0], frame.panel1, 300, 300);
		frame.newRolePos(hero[1], frame.panel2, 300, 300);
		frame.newRolePos(hero[2], frame.panel3, 300, 300);
		frame.setLayout(new GridLayout(3,1,0,10));
		frame.setVisible(true);
		frame.add(frame.panel1);
		frame.add(frame.panel2);
		frame.add(frame.panel3);
	}

}
