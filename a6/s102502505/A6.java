package ce1002.a6.s102502505;

import javax.swing.*;
import java.awt.*;

public class A6 {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			// Initialized hero array with length of 3
			Hero[] heros = new Hero[3];
			
			heros[0] = new Wizard();
			heros[1] = new Swordsman();
			heros[2] = new Knight();
			
			for(Hero hero : heros)
			{
				System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
			}
			
			JFrame mainframe = new JFrame();//創建物件
			mainframe.setSize(300,700);
			mainframe.setLocationRelativeTo(null);//把元件置中於螢幕
			mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉frame時關閉城市
			mainframe.setLayout(new FlowLayout(FlowLayout.LEFT,5,5));//將frame設為循序佈局
			
			JPanel mypanela = new JPanel();//設panela元件
			JPanel mypanelb = new JPanel();//設panelb元件
			JPanel mypanelc = new JPanel();//設panelc元件
			mypanela.setLayout(new GridLayout(2,1));//將panel設圍網格佈局
			mypanelb.setLayout(new GridLayout(2,1));
			mypanelc.setLayout(new GridLayout(2,1));
			
			mypanela.add(new JLabel("Wizard HP: 6.0 MP: 21.0 PP: 3.0"));//加入文字至panel中
			ImageIcon image1 = new ImageIcon("Wizard.jpg");//讀取圖片到程式
			mypanela.add(new JLabel(image1));//把圖片加到label，再把label加到panel裡
			
			mypanelb.add(new JLabel("Swordsman HP:3.0 MP:3.0 PP:24.0"));
			ImageIcon image2 = new ImageIcon("Swordsman.jpg");
			mypanelb.add(new JLabel(image2));
			
			mypanelc.add(new JLabel("Knight HP:24.0 MP:3.0 PP:3.0"));
			ImageIcon image3 = new ImageIcon("Knight.jpg");
			mypanelc.add(new JLabel(image3));
			
			mainframe.add(mypanela);//將panela放到frame中
			mainframe.add(mypanelb);
			mainframe.add(mypanelc);
			mainframe.setVisible(true);//顯示出frame
		}
	}

