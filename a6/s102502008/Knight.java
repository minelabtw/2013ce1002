package ce1002.a6.s102502008;

public class Knight extends Hero
{
	
	public Knight()
	{
		name= "Knight" ; 
	}
	
	public String nameGetter()
	{
		return name ;
	}
	public float hpGetter()
	{
		return hp*0.8f ;
	}
	public float mpGetter()
	{
		return mp*0.1f ;
	}
	public float ppGetter()
	{
		return pp*0.1f ;
	}
}
