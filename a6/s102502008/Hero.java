package ce1002.a6.s102502008;

public class Hero 
{
	public Hero()
	{
		hp=30.0f;
		mp=30.0f ;
		pp=30.0f ;
	}
	protected String name ="" ;
	protected float hp ;
	protected float mp;
	protected float pp ;
	
	public void nameSetter(String name)
	{
		this.name= name ;
	}
	public void hpSetter(float hp)
	{
		this.hp= hp ;
	}
	public void mpSetter(float mp)
	{
		this.mp= mp ;
	}
	public void ppSetter(float pp)
	{
		this.pp= pp ;
	}
	
	public String nameGetter()
	{
		return name;
	}
	public float hpGetter()
	{
		return hp ;
	}
	public float mpGetter()
	{
		return  mp ;
	}
	public float ppGetter()
	{
		return pp ;
	}
}
