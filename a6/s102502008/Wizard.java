package ce1002.a6.s102502008;

public class Wizard extends Hero
{
	public Wizard()
	{
		name= "Wizard" ; 
	}
	public String nameGetter()
	{
		return name ;
	}
	public float hpGetter()
	{
		return hp*0.2f ;
	}
	public float mpGetter()
	{
		return mp*0.7f ;
	}
	public float ppGetter()
	{
		return pp*0.1f ;
	}
}
