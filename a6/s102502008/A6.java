package ce1002.a6.s102502008;
import java.awt.FlowLayout ;
import java.awt.BorderLayout;
import javax.swing.* ;
public class A6 
{
	public static void main(String args[])
	{
		MyPanel p1 =new MyPanel() ;
		MyPanel p2 =new MyPanel() ;
		MyPanel p3 =new MyPanel() ;
		
		p1.setRoleState(new Knight(),"src/ce1002/a6/s102502008/Knight.jpg"); 
		p2.setRoleState(new Swordsman(),"src/ce1002/a6/s102502008/Swordsman.jpg" ) ;
		p3.setRoleState(new Wizard(),"src/ce1002/a6/s102502008/Wizard.jpg") ;
		
		
		MyFrame frame= new MyFrame(p1,p2,p3) ;
		
		frame.setSize(270,700) ;
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true);
	}
}