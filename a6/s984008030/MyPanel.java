package ce1002.a6.s984008030;
import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	private JLabel status;
	private JLabel picture;
	
	public MyPanel() {
		status = new JLabel("", 2);
		picture = new JLabel();
		//setLayout(new GridLayout(2, 1));
		add(status);
		add(picture);
	}
	
	public void setRoleState(Hero hero) {
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");
		status.setText(hero.getName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP());
		picture.setIcon(image);
	}
	
}
