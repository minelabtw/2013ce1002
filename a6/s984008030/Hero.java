package ce1002.a6.s984008030;

public class Hero {
	
	private String name;//英雄名稱
	private float HP;//生命點數(HP)
	private float MP;//魔法點數(MP)
	private float PP;//能力點數(PP)
	
	public Hero(){
		this.name = "Hero";
		this.HP = (float)30.0;
		this.MP = (float)30.0;
		this.PP = (float)30.0;
	}
	
	public String getName() {//取得英雄名稱
		return this.name;
	}
	
	public void setName(String _name) {//設定英雄名稱
		this.name = _name;
	}
	
	public float getHP() {//取得生命點數(HP)
		return this.HP;
	}
	
	public void setHP(float _HP) {//設定生命點數(HP)
		this.HP = _HP;
	}
	
	public float getMP() {//取得魔法點數(MP)
		return this.MP;
	}
	
	public void setMP(float _MP) {//設定魔法點數(MP)
		this.MP = _MP;
	}
	
	public float getPP() {//取得能力點數(PP)
		return this.PP;
	}
	
	public void setPP(float _PP) {//設定能力點數(PP)
		this.PP = _PP;
	}
	
}
