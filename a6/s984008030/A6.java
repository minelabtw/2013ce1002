package ce1002.a6.s984008030;


public class A6 {

	public static void main(String[] args) {
		int width = 260, height = 630;
		Wizard wizard = new Wizard();//第一個初始化為Wizard
		Swordsman swordsman = new Swordsman();//第二個初始化為Swordsman
		Knight knight = new Knight();//第三個初始化為Knight
		MyFrame myFrame = new MyFrame();
		myFrame.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		myFrame.newRolePos(wizard, myFrame.wizardPanel, 0, 0);
		myFrame.newRolePos(swordsman, myFrame.swordsmanPanel, 0, 210);
		myFrame.newRolePos(knight, myFrame.knightPanel, 0, 420);
		myFrame.setSize(width, height);
		myFrame.setVisible(true);
	}

}
