package ce1002.a6.s984008030;
import java.awt.GridLayout;

import ce1002.a6.s984008030.Hero;
import ce1002.a6.s984008030.MyPanel;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	MyPanel wizardPanel;
	MyPanel swordsmanPanel;
	MyPanel knightPanel;
	
	public MyFrame() {
		wizardPanel = new MyPanel();
		swordsmanPanel = new MyPanel();
		knightPanel = new MyPanel();
		setLayout(new GridLayout(3, 1));
		add(wizardPanel);
		add(swordsmanPanel);
		add(knightPanel);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y) {
		panel.setRoleState(hero);
		panel.setLocation(x, y);
	}
	
}
