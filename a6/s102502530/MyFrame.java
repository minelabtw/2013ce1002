package ce1002.a6.s102502530 ;
import javax.swing.JFrame ;
import javax.swing.BorderFactory ;
import java.awt.Dimension ;
import java.awt.FlowLayout ;
import java.awt.BorderLayout ;
import java.awt.Color ;

public class MyFrame extends JFrame
{
   public void newRolePos(Hero hero, MyPanel panel, int x, int y)
   {
      panel.setLayout(new BorderLayout()) ;
      panel.setRoleState(hero) ;
      panel.setPreferredSize(new Dimension(x, y)) ; 
      panel.setBorder(BorderFactory.createLineBorder(Color.black, 5)) ;
      this.add(panel) ;
   }
}
