package ce1002.a6.s102502530 ;

public class Wizard extends Hero
{
   public Wizard()   //constructor
   {
      super.setName("Wizard") ;
   }

   public double getHp()   //get hp
   {
      return super.getHp() * 0.2 ;
   }

   public double getMp()   //get mp
   {
      return super.getMp() * 0.7 ;
   }

   public double getPp()   //get pp
   {
      return super.getPp() * 0.1 ;
   }
}

