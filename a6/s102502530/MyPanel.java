package ce1002.a6.s102502530 ;
import javax.swing.JPanel ;
import javax.swing.JLabel ;
import javax.swing.ImageIcon ;
import java.awt.BorderLayout ;

public class MyPanel extends JPanel
{
   public void setRoleState(Hero hero)
   {
      add(new JLabel(hero.getName() + " HP: " + hero.getHp() + " MP: " + hero.getMp() + " PP: " + hero.getPp()), BorderLayout.NORTH) ;
      add(new JLabel(new ImageIcon(hero.getName() + ".jpg")), BorderLayout.SOUTH) ;
   }
}
