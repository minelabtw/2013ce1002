package ce1002.a6.s102502530 ;
import javax.swing.JFrame ;
import java.awt.FlowLayout ;

public class A6
{
   public static void main(String[] args)
   {
      MyFrame myframe = new MyFrame() ;
      myframe.setSize(290, 705) ;
      myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
      myframe.setLayout(new FlowLayout(FlowLayout.CENTER)) ;
      myframe.newRolePos(new Wizard(), new MyPanel(), 260, 215) ;
      myframe.newRolePos(new Swordsman(), new MyPanel(), 260, 215) ;
      myframe.newRolePos(new Knight(), new MyPanel(), 260, 215) ;
      myframe.setVisible(true) ;
   }
}
