package ce1002.a6.s102502523;

public class Knight  extends Hero{
	
	public Knight()
	{	
		// setup the title for this character's name
		super.setName("Knight");
	}
	
	/*
	 * This is the key part of polymorphism.
	 * We overrided the each point's getter
	 * and multiply it with weight from the table.
	 * */
	public double getHp()
	{
		return super.hp*0.8;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}