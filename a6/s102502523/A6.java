package ce1002.a6.s102502523;

import ce1002.a6.s102502523.Hero;
import ce1002.a6.s102502523.Knight;
import ce1002.a6.s102502523.Swordsman;
import ce1002.a6.s102502523.Wizard;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] heros = new Hero[3];
		
		/*
		 * Initialized different hero with different index
		 */
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		/*
		 * Use polymorphism to print out the result of different 
		 * hero's name,hp,mp,pp
		 */
		for(Hero hero : heros){
			System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
		}
		Myframe myFrame = new Myframe();
	}

}
