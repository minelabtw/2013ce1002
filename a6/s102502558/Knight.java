package ce1002.a6.s102502558;

import ce1002.a6.s102502558.Hero;

public class Knight extends Hero {
	Knight(){
		super();
		setId("Knight");
	}
	@Override
	float getHp(){
	  return super.getHp() * 0.8f;
	}
	@Override
	float getMp(){
		return super.getMp() * 0.1f;
	}
	@Override
	float getPp(){
		return super.getPp() * 0.1f;
	}
}
