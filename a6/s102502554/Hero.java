package ce1002.a6.s102502554;

import javax.swing.*;
import java.awt.*;

public class Hero {
public Hero (){
		
	}
	
	private String HeroName;
	private double HP = 30.0;
	private double MP = 30.0;
	private double PP = 30.0;//the standard state of the heroes
	ImageIcon icon;
	
	public void setHeroName(String Name){
		this.HeroName = Name;
	}
	
	public void setHP(double HP){
		this.HP = HP;
	}
	
	public void setMP(double MP){
		this.MP = MP;
	}
	
	public void setPP(double PP){
		this.PP = PP;
	}
	//these are setters
	public String getHeroName(){
		return this.HeroName;
	}
	
	public double getHP(){
		return this.HP ;
	}
	
	public double getMP(){
		return this.MP;
	}
	
	public double getPP(){
		return this.PP;
	}
	//these are getters
  

}
