package ce1002.a6.s102502554;

import java.awt.*;
import javax.swing.*;

public class Knight extends Hero{
	
	public Knight (){
		setHeroName("Knight");
		icon = new ImageIcon("Knight.jpg");
	}//set hero name and icon
	
	public double getHP (){
		return super.getHP() * 0.8d;
	}
	
	public double getMP (){
		return super.getMP() * 0.1d;
	}
	
	public double getPP (){
		return super.getPP() * 0.1d;
	}
	//set the state of Knight
	

}
