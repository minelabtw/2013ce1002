package ce1002.a6.s102502554;

import javax.swing.ImageIcon;

public class Wizard extends Hero{
	public Wizard (){
		setHeroName("Wizard");
		icon = new ImageIcon("Wizard.jpg");
	}//set hero name and icon
	
	public double getHP (){
		return super.getHP() * 0.2d;
	}
	
	public double getMP (){
		return super.getMP() * 0.7d;
	}
	
	public double getPP (){
		return super.getPP() * 0.1d;
	}
	//set the state of Wizard


}
