package ce1002.a6.s102502554;

import javax.swing.*;

public class A6 {
public static void main(String [] args){
	
	    MyFrame frame = new MyFrame();
	    frame.setSize(300, 700);
	    frame.setLocationRelativeTo(null);
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setTitle(null);
	    frame.setVisible(true);//set frame state
		
		Hero [] hero;
		hero = new Hero [3];//set 3 hero arrays to represent hero
		
		hero [2] = new Knight();
		hero [1] = new Swordsman();
		hero [0] = new Wizard();
		
		MyPanel P = new MyPanel();
		
		for ( int i = 0 ; i < 3 ; i++){
			P.setRoleState(hero[i]);
			frame.newRolePos(hero[i] , P , 100 , 200);
		}//output 3 hero state and picture on the frame
	}

}
