package ce1002.a6.s102502554;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel{
	
	public MyPanel(){
		
	}
	
	JLabel state ;
	JLabel icon ;
	public void setRoleState(Hero hero){
		state = new JLabel(hero.getHeroName()+" HP: " + hero.getHP() + " MP: " + hero.getMP() +"  PP: " + hero.getPP());
		icon = new JLabel(hero.icon);
		add(state);
		add(icon);
	}//set the state and the picture of the hero	
}
