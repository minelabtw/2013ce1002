package ce1002.a6.s102502554;

import javax.swing.ImageIcon;

public class Swordsman extends Hero {
	public Swordsman (){
		setHeroName("Swordsman");
		icon = new ImageIcon("Swordsman.jpg");
	}//set hero name and icon
	
	public double getHP (){
		return super.getHP() * 0.1d;
	}
	
	public double getMP (){
		return super.getMP() * 0.1d;
	}
	
	public double getPP (){
		return super.getPP() * 0.8d;
	}
	//set the state of Swordsman

}
