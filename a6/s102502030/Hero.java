package ce1002.a6.s102502030;

public class Hero {

	private String name;
	private double HP;
	private double MP;
	private double PP;
	
	public Hero() {
		HP = 30.0;
		MP = 30.0;
		PP = 30.0;
	}

	//SetGetName
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//SetGet HP
	public double getHP() {
		return HP;
	}

	public void setHP(double hP) {
		HP = hP;
	}

	//SetGetMP
	public double getMP() {
		return MP;
	}

	public void setMP(double mP) {
		MP = mP;
	}

	//SetGetPP
	public double getPP() {
		return PP;
	}

	public void setPP(double pP) {
		PP = pP;
	}
	
}
