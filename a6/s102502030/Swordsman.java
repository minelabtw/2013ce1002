package ce1002.a6.s102502030;

public class Swordsman extends Hero {

	public Swordsman() {
		super.setName( "Swordsman" );
	}
	
	public double getHP() {
		return super.getHP()*0.1;
	}
	
	public double getMP() {
		return super.getMP()*0.1;
	}
	
	public double getPP() {
		return super.getPP()*0.8;
	}
}
