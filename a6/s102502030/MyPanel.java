package ce1002.a6.s102502030;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	static JLabel data;
	static JLabel img;
	
	public MyPanel () {
		
	}
	
	public void setRoleState( Hero hero ) {
		String name = hero.getName() + ".jpg";
		ImageIcon image = new ImageIcon( name ); 
		data = new JLabel( hero.getName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP() );
		img = new JLabel( image );
	}
	
	public JLabel getRoleState()
	{
		return data;
	}
	
	public JLabel getRoleState2()
	{
		return img;
	}
}
