package ce1002.a6.s102502553;

public class Hero {

	private String name;//名子
	private float HP;//血量
	private float MP;//魔力
	private float PP;//能力點
	
	public void setname(String s){//傳入各個值
		name = s;
	}
	public void setHP(float hp){
		HP = hp;
	}
	public void setMP(float mp){
		MP = mp;
	}
	public void setPP(float pp){
		PP = pp;
	}
	
	public String getnames(){//輸出各個值
		return name;
	}
	public float getHP(){
		return HP;
	}
	public float getMP(){
		return MP;
	}
	public float getPP(){
		return PP;
	}
}

