package ce1002.a6.s102502553;

public class Swordsman extends Hero{
	
	Swordsman(){//constructor
		super.setname("Swordsman");
	}

	public float getHP(){//回傳Swordsman的值
		return super.getHP() * (float) 0.1;
	}
	public float getMP(){
		return super.getMP() * (float) 0.1;
	}
	public float getPP(){
		return super.getPP() * (float) 0.8;
	}
}
