package ce1002.a6.s102502553;

public class Knight extends Hero{
	
	Knight(){//constructor
		super.setname("Knight");
	}
	
	public float getHP(){//回傳Knight的值
		return super.getHP() * (float) 0.8;
	}
	public float getMP(){
		return super.getMP() * (float) 0.1;
	}
	public float getPP(){
		return super.getPP() * (float) 0.1;
	}
}