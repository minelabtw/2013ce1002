package ce1002.a6.s102503017;

public class Wizard extends Heros
{
	
	Wizard()
	{
		
	}
	
	double getHp()
	{
		return this.hp * 0.8;
	}
	
	double getMp()
	{
		return this.mp * 0.1;
	}
	
	double getPp()
	{
		return this.pp * 0.1;
	}
	
	public String getHeroName() {
		return this.HeroName = "Wizard";
	}
	
}
