package ce1002.a6.s102502023;

import javax.swing.*;


public class A6 {
  public static void main(String[] args) {
	  MyFrame frame = new MyFrame(); // initialize MyFrame frame
	  frame.setSize(350, 800);
	  frame.setLocationRelativeTo(null);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setTitle(null);
	  frame.setVisible(true);
  }
}
