package ce1002.a6.s102502023;


import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

class MyFrame extends JFrame  {
  public MyFrame() {
	  Wizard w = new Wizard();// initialize Wizard w
	  Swordsman s = new Swordsman(); // initialize Swordsman s
	  Knight k = new Knight(); // initialize Knight k
	  setLayout(null);
	  MyPanel panel1 = new MyPanel(w);
	  MyPanel panel2 = new MyPanel(s);
	  MyPanel panel3 = new MyPanel(k);
	  
	  newRolePos(w, panel1, 25, 10);
	  newRolePos(s, panel2, 25, 250);
	  newRolePos(k, panel3, 25, 500); 
  } // set MyFrame
  
  public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
	  Border lineBorder = new LineBorder(Color.BLACK, 2);
	  panel.setBounds(x, y, 275, 225);
	  panel.setBorder(lineBorder);
	  this.add(panel);  
  } 
}
