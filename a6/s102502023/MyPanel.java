package ce1002.a6.s102502023;

import java.awt.*;

import javax.swing.*;


public class MyPanel extends JPanel {
  JLabel lb1, lb2; // initialize JLabel lb1, lb2
  Hero hero = new Hero(); // initialize Hero hero
  public MyPanel(Hero hero) {
	 setRoleState(hero);
	 //lb2.setHorizontalTextPosition(JLabel.RIGHT);
	 setLayout(null);
  } // set MyPanel
  
  public void setRoleState(Hero hero) {
	  String a = hero.getHero();
	  double b = hero.getHP();
	  double c = hero.getMP();
	  double d = hero.getPP();
	  String e = hero.getPic();
	  ImageIcon icon = new ImageIcon(e);
	  lb1 = new JLabel(icon, JLabel.CENTER);
	  lb2 = new JLabel(a +" HP:" + b + " MP:" + c + " PP:" + d, JLabel.LEFT);
	  lb2.setBounds(5, 0, 200, 20);
	  lb1.setBounds(5, 20, 250, 190);
	  //lb2.setVerticalTextPosition(JLabel.TOP);
	  //
	  
	  this.add(lb2);
	  this.add(lb1);
  }
}
