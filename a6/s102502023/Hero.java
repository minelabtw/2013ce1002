package ce1002.a6.s102502023;

public class Hero {
  private double HP = 30; // initialize double HP to 30
  private double MP = 30; // initialize double MP to 30
  private double PP = 30; // initialize double PP to 30
  private String type = "Hero"; // initialize String type to Hero
  
  public Hero(){
	  // Hero constructor
  }
  
  public void setHero(String type) {
	  this.type = type;
  }
  
  public String getHero() {
	  return type;
  }
  
  public void setHP(double HP) { 
	  this.HP = HP;
  }
  
  public void setMP(double MP) {
	  this.MP = MP;
  }
  
  public void setPP(double PP) {
	  this.PP = PP;
  }
  
  public double getHP() {
	  return HP;
  }
  
  public double getMP() {
	  return MP;
  }
  
  public double getPP() {
	  return PP;
  }
  
  public String getPic() {
	  return (type + ".jpg");
  } 
  }
