package ce1002.a6.s102502023;

public class Wizard extends Hero {
  public Wizard() {
	  setHero("Wizard");
	  setHP();
	  setMP();
	  setPP();
  } // Wizard constructor
  
  public void setHP() {
	  double hp = getHP();
	  hp = hp * 0.2;
	  setHP(hp);
  }
  
  public void setMP() {
	  double mp = getMP();
	  mp = mp * 0.7;
	  setMP(mp);
  }
  
  public void setPP() {
	  double pp = getPP();
	  pp = pp * 0.1;
	  setPP(pp);
  }
  
  public String getHero() {
	 return super.getHero();
  }
  
  public double getHP() {
	return super.getHP();  
  }
  
  public double getMP() {
	  return super.getMP();
  }
  
  public double getPP() {
	  return super.getPP();
  }
  
  public String getPic() {
	  return super.getPic();
  }
}
