package ce1002.a6.s102502023;

public class Swordsman extends Hero {
  public Swordsman() {
	  setHero("Swordsman");
	  setHP();
	  setMP();
	  setPP();
  }	// Swordsman constructor
  public void setHP() {
	  double hp = getHP();
	  hp = hp * 0.1;
	  setHP(hp);
  }
  
  public void setMP() {
	  double mp = getMP();
	  mp = mp * 0.1;
	  setMP(mp);
  }
  
  public void setPP() {
	  double pp = getPP();
	  pp = pp * 0.8;
	  setPP(pp);
  }
  
  @Override
  public String getHero() {
	  return super.getHero();
  }
  
  public double getHP() {
	  return super.getHP();
  }
  
  public double getMP() {
	  return super.getMP();
  }
  
  public double getPP() {
	  return super.getPP();
  }
  
  public String getPic() {
	  return super.getPic();
  }
}
