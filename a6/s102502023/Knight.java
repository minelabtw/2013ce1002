package ce1002.a6.s102502023;

public class Knight extends Hero {
  public Knight() {
	  setHero("Knight");
	  setHP();
	  setMP();
	  setPP();
  }	// Knight constructor
  public void setHP() {
	  double hp = getHP();
	  hp = hp * 0.8;
	  setHP(hp);
  }
  
  public void setMP() {
	  double mp = getMP();
	  mp = mp * 0.1;
	  setMP(mp);
  }
  
  public void setPP() {
	  double pp = getPP();
	  pp = pp * 0.1;
	  setPP(pp);
  }
  @Override
  public String getHero() {
	  return super.getHero();
  }
  @Override
  public double getHP() {
	  return super.getHP();
  }
  @Override
  public double getMP() {
	  return super.getMP();
  }
  @Override
  public double getPP() {
	  return super.getPP();
  }
  public String getPic() {
	  return super.getPic();
  }
}
