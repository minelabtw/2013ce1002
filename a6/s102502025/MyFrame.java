package ce1002.a6.s102502025;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame() {
		setLayout(null);
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setRoleState(hero);
		panel.setLocation(x, y);
		add(panel);
	}
}
