package ce1002.a6.s102502025;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Initialized hero array with length of 3
		Hero[] heroes = new Hero[3];
		/*
		 * Initialized different hero with different index
		 */
		heroes[0] = new Wizard();
		heroes[1] = new Swordsman();
		heroes[2] = new Knight();
		/*
		 * Use polymorphism to print out the result of different hero's
		 * name,hp,mp,pp
		 */
		MyFrame frame = new MyFrame();
		MyPanel[] panel = new MyPanel[3]; // put 3 array in MyPanel
		frame.setSize(298, 775); // size of the frame
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for (int x = 0; x < 3; x++) {
			// output answer
			panel[x] = new MyPanel();
			frame.newRolePos(heroes[x], panel[x], 10, 10 + 240 * x);
		}
		frame.setVisible(true); // visible frame
	}

}
