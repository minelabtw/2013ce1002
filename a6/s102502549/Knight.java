package ce1002.a6.s102502549;

public class Knight extends Hero{
	
	//初始名稱為Knight
	public Knight()
	{
		super.setName("Knight");
	}

	//覆寫getter
	public float getHP() {
		return super.getHP()*0.8f;
	}

	public float getMP() {
		return super.getMP()*0.1f;
	}

	public float getPP() {
		return super.getPP()*0.1f;
	}
}
