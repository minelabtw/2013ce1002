package ce1002.a6.s102502549;

import java.awt.Frame;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	// 宣告三個hero的panel
	private MyPanel panel1 = new MyPanel();
	private MyPanel panel2 = new MyPanel();
	private MyPanel panel3 = new MyPanel();

	public MyFrame() {
		setLayout(null);
		// 建立三個panel
		newRolePos(new Wizard(), panel1, 10, 10);
		newRolePos(new Swordsman(), panel2, 10, 232);
		newRolePos(new Knight(), panel3, 10, 454);
		// 加入panel
		add(panel1);
		add(panel2);
		add(panel3);
	}

	// 建panel的函式
	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setBounds(x, y, 260, 212);
		panel.setRoleState(hero);
	}
}
