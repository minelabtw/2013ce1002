package ce1002.a6.s102502035;

public class Swordsman extends Hero {
	String name;
	double HPrate = 0.1;
	double MPrate = 0.1;
	double PPrate = 0.8;

	public Swordsman() {
		name = "Swordsman";
	}

	public String getname() {
		return name;
	}

	public double getHP() {// return and calculate
		return HP *= HPrate;
	}

	public double getMP() {// return and calculate
		return MP *= MPrate;
	}

	public double getPP() {// return and calculate
		return PP *= PPrate;
	}
}
