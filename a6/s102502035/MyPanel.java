package ce1002.a6.s102502035;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	String labelcontent;
	String picture;
	ImageIcon image;

	public MyPanel(String picture) {
		this.picture = picture;
		image = new ImageIcon(picture);
	}

	public void setRoleState(Hero hero) {
		labelcontent = hero.getname() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP: " + hero.getPP();// save content
		// setVisible(true);
		finaladd();// add label

		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}

	public void finaladd() {
		add(new JLabel(labelcontent));
		add(new JLabel(image));
	}
}