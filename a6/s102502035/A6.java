package ce1002.a6.s102502035;

import java.awt.Panel;

import ce1002.a6.s102502035.Hero;
import ce1002.a6.s102502035.Knight;
import ce1002.a6.s102502035.Swordsman;
import ce1002.a6.s102502035.Wizard;

public class A6 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();// use constructor
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame(hero);// MyFrame class frame object
		frame.setSize(300, 750);// frame size
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);// to see
	}
}
