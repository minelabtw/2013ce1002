package ce1002.a6.s102502035;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyPanel panel1;// MyPanel class panel object
	MyPanel panel2;
	MyPanel panel3;

	public MyFrame(Hero[] hero) {
		panel1 = new MyPanel("Wizard.jpg");// constructor
		panel2 = new MyPanel("Swordsman.jpg");
		panel3 = new MyPanel("Knight.jpg");
		newRolePos(hero[0], panel1, 10, 10);// call function
		newRolePos(hero[1], panel2, 10, 235);
		newRolePos(hero[2], panel3, 10, 460);
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		setLayout(null);
		panel.setRoleState(hero);// pass hero object to MyPanel
		panel.setBounds(x, y, 270, 230);
		finaladd();// add panel
	}

	public void finaladd() {
		add(panel1);
		add(panel2);
		add(panel3);
	}
}
