package ce1002.a6.s995002046;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
@SuppressWarnings("serial")
class MyPanel extends JPanel{
	JLabel status=new JLabel();//status
	JLabel picture=new JLabel();//picture
	MyPanel(){
		this.add(status);
		this.add(picture);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));//set border
	}
	public void setRoleState(Hero hero){
		status.setText(hero.getname()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());//show status in label
		picture.setIcon(hero.getImage());
	}
}