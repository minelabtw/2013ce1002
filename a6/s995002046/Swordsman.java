package ce1002.a6.s995002046;

import javax.swing.ImageIcon;

class Swordsman extends Hero{
	Swordsman(){
		setname("Swordsman");
		icon = new ImageIcon("Swordsman.jpg");//read picture
	}
	@Override
	double getHP(){
		return HP*0.1;
	}
	@Override
	double getMP(){
		return MP*0.1;
	}
	@Override
	double getPP(){
		return PP*0.8;
	}
}