package ce1002.a6.s995002046;

import javax.swing.ImageIcon;

class Hero{
	protected String name="Hero";
	protected ImageIcon icon;
	protected double HP=30.0;
	protected double MP=30.0;
	protected double PP=30.0;
	Hero(){
		
	}
	String getname(){//getter
		return name;
	}
	ImageIcon getImage(){
		return icon;
	}
	double getHP(){
		return HP;
	}
	double getMP(){
		return MP;
	}
	double getPP(){
		return PP;
	}
	void setname(String s){//setter
		name=s;
	}
	void setHP(double i){
		HP=i;
	}
	void setMP(double i){
		MP=i;
	}
	void setPP(double i){
		PP=i;
	}
	
}