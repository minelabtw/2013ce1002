package ce1002.a6.s995002046;
import javax.swing.ImageIcon;
class Knight extends Hero{
	Knight(){
		setname("Knight");
		icon = new ImageIcon("Knight.jpg");//read picture
	}
	@Override
	double getHP(){
		return HP*0.8;
	}
	@Override
	double getMP(){
		return MP*0.1;
	}
	@Override
	double getPP(){
		return PP*0.1;
	}
}