package ce1002.a6.s102502524;

public class Swordsman extends Hero {
	
	public void setName()			//setting abilities to swordsman
	{
		this.HeroName = "Swordsman";
	}
	
	public double getHP()
	{
		return this.HP*0.1;
	}

	public double getMP()
	{
		return this.MP*0.1;
	}
	
	public double getPP()
	{
		return this.PP*0.8;
	}

}
