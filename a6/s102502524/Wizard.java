package ce1002.a6.s102502524;

public class Wizard extends Hero {
	
	public void setName()			//setting abilities to wizard
	{
		this.HeroName = "Wizard";
	}
	
	public double getHP()
	{
		return this.HP*0.2;
	}

	public double getMP()
	{
		return this.MP*0.7;
	}
	
	public double getPP()
	{
		return this.PP*0.1;
	}
	
}
