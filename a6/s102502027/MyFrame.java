package ce1002.a6.s102502027;

import javax.swing.*;

public class MyFrame extends JFrame {

	MyPanel pnl1 = new MyPanel(); //設變數
	MyPanel pnl2 = new MyPanel();
	MyPanel pnl3 = new MyPanel();
	
	MyFrame() {
		setLayout(null);
		newRolePos(new Wizard((float)30.0,(float)30.0,(float)30.0,"Wizard"),pnl1,5,0); //呼叫
		newRolePos(new Swordsman((float)30.0,(float)30.0,(float)30.0,"Swordsman"),pnl2,5,230);
		newRolePos(new Knight((float)30.0,(float)30.0,(float)30.0,"Knight"),pnl3,5,460);
		add(pnl1); //存入
		add(pnl2);
		add(pnl3);
		setSize(300,800);
		setDefaultCloseOperation(EXIT_ON_CLOSE);//關
		setVisible(true);//顯示
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {

		panel.setRoleState(hero); //呼叫
		panel.setLocation(x,y); //設位置
	}

}
