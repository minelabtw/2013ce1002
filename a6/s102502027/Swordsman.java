package ce1002.a6.s102502027;

public class Swordsman extends Hero {

	Swordsman(float HP, float MP, float PP, String name) {
		super(HP, MP, PP, name);
	}

	public String name() {
		return name;
	}

	public float hp() { // 做加權計算，並回傳加權後的數值
		return (float) (HP * 0.1);
	}

	public float mp() {
		return (float) (MP * 0.1);
	}

	public float pp() {
		return (float) (PP * 0.8);
	}
}
