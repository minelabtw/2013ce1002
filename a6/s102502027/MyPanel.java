package ce1002.a6.s102502027;
import javax.swing.*;
import java.awt.*;


public class MyPanel extends JPanel {

	public void setRoleState(Hero hero){
		
		JLabel label1 = new JLabel(); //設變數
		JLabel label2 =new JLabel();
		label1.setText(hero.name()+" HP:"+hero.hp()+" MP:"+hero.mp()+" PP:"+hero.pp());
		label1.setPreferredSize(new Dimension(200,10)); //調位置
		add(BorderLayout.NORTH , label1); //存入
		ImageIcon icon = new ImageIcon(hero.name()+".jpg"); //讀取圖片
		label2.setIcon(icon); 
		label2.setPreferredSize(new Dimension(240,188));
		add(label2);
		setSize(230,220); //設圖片大小
		setVisible(true);
		
	}
	public void paint(Graphics g){ //黑框
		super.paint(g);
		g.setColor(Color.BLACK);
		g.drawRect(0,0,228,210);
		g.drawRect(1,1,228,210);
		g.drawRect(2,2,228,210);
	}
}
