package ce1002.a6.s102502026;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	public MyPanel(){	//panel
		setSize(263, 230);
		setBorder(new LineBorder(Color.black, 3));
	}
	public void setRoleState(Hero hero){
		String str= "src/ce1002/a6/s102502026/" + hero.getHero() +".jpg" ;	//location of the picture
		ImageIcon icon= new ImageIcon(str);
		JLabel state= new JLabel(hero.getHero() + " HP:" + hero.getHp()
				+ " MP:" + hero.getMp() + " PP:" + hero.getPp());
		add(state);	//output state
		add(new JLabel(icon));	//output picture
	}
}
