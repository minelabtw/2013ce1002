package ce1002.a6.s102502026;

class Wizard extends Hero {

	public Wizard() {
		setHero("Wizard"); // set name
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {
		Hp = (float) (.2 * 30); // get hp of Wizard
	}

	public void setMp() {
		Mp = (float) (.7 * 30); // get mp of Wizard
	}

	public void setPp() {
		Pp = (float) (.1 * 30); // get pp of Wizard
	}
}
