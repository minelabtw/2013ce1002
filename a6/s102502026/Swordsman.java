package ce1002.a6.s102502026;

class Swordsman extends Hero {

	public Swordsman() {
		setHero("Swordsman"); // set name
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {
		Hp = (float) (.1 * 30); // get hp of Swordsman
	}

	public void setMp() {
		Mp = (float) (.1 * 30); // get mp of Swordsman
	}

	public void setPp() {
		Pp = (float) (.8 * 30); // get pp of Swordsman
	}
}
