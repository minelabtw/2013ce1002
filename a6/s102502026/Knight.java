package ce1002.a6.s102502026;

class Knight extends Hero {

	public Knight() {
		setHero("Knight"); // set the name
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {
		Hp = (float) (.8 * 30); // get the hp of a knight
	}

	public void setMp() {
		Mp = (float) (.1 * 30); // get mp of a knight
	}

	public void setPp() {
		Pp = (float) (.1 * 30); // get pp of a knight

	}

}
