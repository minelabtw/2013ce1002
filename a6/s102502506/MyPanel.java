package ce1002.a6.s102502506;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	JLabel info;  //設一個window
	JLabel img;
	ImageIcon icon;  
	MyPanel(){
		this.setSize(280, 230);
		this.setBorder(new LineBorder(Color.black, 5));
	}
	
	public void setRoleState(Hero hero){
		icon = new ImageIcon(hero.getHeroname() + ".jpg");  //召喚圖片
		info = new JLabel(hero.getHeroname() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		img = new JLabel(icon);
		this.add(info);
		this.add(img);
	}
}
