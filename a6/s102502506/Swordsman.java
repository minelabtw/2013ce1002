package ce1002.a6.s102502506;

public class Swordsman extends Hero{
	@Override   
	public float getHP(){
		return super.getHP() * 0.1f;
	}
	@Override
	public float getMP(){
		return super.getMP() * 0.1f;
	}
	@Override
	public float getPP(){
		return super.getPP() * 0.8f;
	}
}
