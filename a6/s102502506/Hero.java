package ce1002.a6.s102502506;

public class Hero {
	private String heroname;
	private float HP;
	private float MP;
	private float PP;

	Hero(){     //建構子
		HP = 30;
		MP = 30;
		PP = 30;
	}

	public void setHeroname(String heroname){
		this.heroname = heroname;
	}
	public void setHP(float HP){
		this.HP = HP;
	}
	public void setMP(float MP){
		this.MP = MP;
	}
	public void setPP(float PP){
		this.PP = PP;
	}

	public String getHeroname(){
		return this.heroname;
	}
	public float getHP(){
		return this.HP;
	}
	public float getMP(){
		return this.MP;
	}
	public float getPP(){
		return this.PP;
	}
	
}