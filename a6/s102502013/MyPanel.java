package ce1002.a6.s102502013;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private JLabel situation;
	private JLabel picture;
	MyPanel(){
		
	}
	public void setRoleState(Hero hero)
	{
		situation = new JLabel(hero.getName() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		picture = new JLabel(new ImageIcon(hero.getName() + ".jpg"));
		situation.setSize(225, 20);//show situationlabel's size
		picture.setSize(250,200);//show picturelabel's size
		situation.setLocation(5, 0);//set situationlabel's location
		picture.setLocation(5, 20);//set picturelabel's location
	}
	public JLabel getRoleSituation(){
		return situation;
	}
	public JLabel getRolePicture(){
		return picture;
	}
}
