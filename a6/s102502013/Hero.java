package ce1002.a6.s102502013;

public class Hero {
	private String Name;//declare a variable for hero's name
	private double HP;//declare a variable for hero's hp
	private double MP;//declare a variable for hero's mp
	private double PP;//declare a variable for hero's pp
	Hero(){
		Name = "";
		HP = 30.0;
		MP = 30.0;
		PP = 30.0;
	}
	public void setName(String N){//setName
		Name = N;
	}
	public void setHP(double hp){//setHP
		HP = hp;
	}
	public void setMP(double mp){//setMP
		MP = mp;
	}
	public void setPP(double pp){//setPP
		PP = pp;
	}
	public String getName(){//getName
		return Name;
	}
	public double getHP(){//getHP
		return HP;
	}
	public double getMP(){//getMP
		return MP;
	}
	public double getPP(){//getPP
		return PP;
	}
}
