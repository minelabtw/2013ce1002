package ce1002.a6.s102502514;

public class Swordsman extends Hero{
	//覆寫父類別Hero的getter為各職業的加權計算，並呼叫父類別Hero的setter
	Swordsman(){
		setHeroName("Swordsman");
	}
	public double getHP() {
		return super.getHP()*0.1;
	}
	public double getMP() {
		return super.getMP()*0.1;
	}
	public double getPP() {
		return super.getPP()*0.8;
	}
}
