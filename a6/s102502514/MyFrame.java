package ce1002.a6.s102502514;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame(){
		this.setLayout(null);  //初始化使用者介面的格局
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setRoleState(hero);  //設定設定圖片與角色狀態
		panel.setLocation(x, y);  //設定面板的位置
		this.add(panel);  //將panel加到使用者介面內
	}
}
