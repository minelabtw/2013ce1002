package ce1002.a6.s102502514;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	MyPanel(){
		this.setSize(280 , 230);  //設定面板的版面大小
		this.setBorder(new LineBorder(Color.black, 5));  //設定面板外的邊框顏色與粗細
	}
	public void setRoleState(Hero hero)  //設定設定圖片與角色狀態
	{
		ImageIcon icon = new ImageIcon(hero.getHeroName()+".jpg");
		JLabel state = new JLabel(hero.getHeroName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		JLabel image = new JLabel(icon);
		this.add(state);
		this.add(image);
	}
}
