package ce1002.a6.s102502514;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		MyFrame frame = new MyFrame();
		frame.setSize(320 , 770);  //設定使用者介面的版面大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		//建立一個MyPanel的物件陣列，並將他指向panel址參變數
		MyPanel[] panel = new MyPanel[3];
		//初始化
		panel[0] = new MyPanel();
		panel[1] = new MyPanel();
		panel[2] = new MyPanel();
		
		//呼叫newRolePos方法
		frame.newRolePos(hero[0] , panel[0] , 10 , 10);
		frame.newRolePos(hero[1] , panel[1] , 10 , 250);
		frame.newRolePos(hero[2] , panel[2] , 10 , 490);
	}

}
