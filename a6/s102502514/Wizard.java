package ce1002.a6.s102502514;

public class Wizard extends Hero{
	//覆寫父類別Hero的getter為各職業的加權計算，並呼叫父類別Hero的setter
	Wizard(){
		setHeroName("Wizard");
	}
	public double getHP() {
		return super.getHP()*0.2;
	}
	public double getMP() {
		return super.getMP()*0.7;
	}
	public double getPP() {
		return super.getPP()*0.1;
	}
}
