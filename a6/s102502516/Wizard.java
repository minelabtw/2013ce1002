package ce1002.a6.s102502516;

public class Wizard extends Hero {
	public String getName() { // 名字寫死
		return "Wizard";
	}

	// 利用override，回傳加權後的屬性值
	@Override
	public float getHP() {
		return (float) (super.getHP() * 0.2);
	}

	public float getMP() {
		return (float) (super.getMP() * 0.7);
	}

	public float getPP() {
		return (float) (super.getPP() * 0.1);
	}
}
