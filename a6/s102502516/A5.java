package ce1002.a6.s102502516;

public class A5 {

	public static void main(String[] args) {
		Hero[] heros = new Hero[3]; // 宣告物件陣列
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight(); // 初始化

		for (int i = 0; i < 3; i++)
			System.out.println(heros[i].getName() + " HP:" + heros[i].getHP()
					+ " MP:" + heros[i].getMP() + " PP:" + heros[i].getPP());
	} // 印出屬性值
}
