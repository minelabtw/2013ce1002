package ce1002.a6.s102502516;

public class Hero {
	private String Name;
	private float HP, MP, PP;

	public Hero() {
		HP = MP = PP = (float) 30; // 預設皆為30
	}

	// 以下為各種setter, getter
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public float getHP() {
		return HP;
	}

	public void setHP(float hP) {
		HP = hP;
	}

	public float getMP() {
		return MP;
	}

	public void setMP(float mP) {
		MP = mP;
	}

	public float getPP() {
		return PP;
	}

	public void setPP(float pP) {
		PP = pP;
	}

}
