package ce1002.a6.s101201506;

public class Hero {
	private String heroname;// 英雄名子
	protected double HP = 0;// 血量
	protected double MP = 0;// 魔力
	protected double PP = 0;// 能力點

	public void setName(String heroname) {
		this.heroname = heroname;
	}

	public String getName() {
		return heroname;
	}

	// set HP
	public void setHP(double HP) {
		this.HP = HP;
	}

	public double getHP() {
		return HP;
	}

	public void setMP(double MP) {
		this.MP = MP;
	}

	public double getMP() {
		return MP;
	}

	public void setPP(double PP) {
		this.PP = PP;
	}

	public double getPP() {
		return PP;
	}

}
