package ce1002.a6.s101201506;

public class Swordsman extends Hero {
	public double getHP() {
		return HP * 0.1;
	}

	public double getMP() {
		return MP * 0.1;
	}

	public double getPP() {
		return PP * 0.8;
	}

}
