package ce1002.a6.s101201506;

public class Wizard extends Hero {
	public double getHP() {
		return HP * 0.2;
	}

	public double getMP() {
		return MP * 0.7;
	}

	public double getPP() {
		return PP * 0.1;
	}

}
