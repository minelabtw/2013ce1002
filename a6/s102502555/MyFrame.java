package ce1002.a6.s102502555;

import java.awt.*;
import javax.swing.border.*;
import javax.swing.JFrame;


public class MyFrame extends JFrame{
	MyPanel p1 = new MyPanel();  //建立新的panel
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();
	
	MyFrame(){	
		setSize(300, 710);  //設frame的大小
		setLayout(null);  //設定排版
		
		
		//初始化三個英雄
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard(30 , 30 , 30);
		hero[1] = new Swordsman(30 , 30 , 30);
		hero[2] = new Knight(30 , 30 , 30);
		
		//設定panel
		newRolePos(hero[0] , p1 , 10 , 10);
		newRolePos(hero[1] , p2 , 10 , 230);
		newRolePos(hero[2] , p3 , 10 , 450);
		
		//把panel放到frame上
		add(p1);
		add(p2);
		add(p3);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setBorder(new LineBorder(Color.BLACK, 3));  //設定邊線
		panel.setRoleState(hero);  //設定panel裡的東西
		panel.setLocation(x, y);  //設定panel在frame裡的位置
	}
}
