package ce1002.a6.s102502555;

public class Wizard extends Hero{
	Wizard(double h , double m , double p){
		super(h , m , p);  //初始化生命,魔法,能力
		setName("Wizard");  //設定名稱
	}
	
	//取得加權後的HP
	public double getHp() {
		return super.getHp() * 0.2;
	}
	
	//取得加權後的MP
	public double getMp() {
		return super.getMp() * 0.7;
	}
	
	//取得加權後的PP
	public double getPp() {
		return super.getPp() * 0.1;
	}
}
