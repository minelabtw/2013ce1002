package ce1002.a6.s101201005;

public class Wizard extends Hero{
	public void setHP(double HP)//set HP
	{
		this.HP=HP;
	}
	public void setMP(double MP)//set MP
	{
		this.MP=MP;
	}
	public void setPP(double PP)//set PP
	{
		this.PP=PP;
	}
	public double  getHP()
	{
		return HP*0.2;
	}
	public double  getMP()
	{
		return MP*0.7;
	}
	public double  getPP()
	{
		return PP*0.1;
	}
}
