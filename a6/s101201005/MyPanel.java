package ce1002.a6.s101201005;
import javax.swing.*;
public class MyPanel extends JPanel {
	private String picture;
	MyPanel()
	{
	}
	public void setRoleState(Hero Hero)
	{
		JLabel label1= new JLabel(Hero.getName()+" HP: "+Hero.getHP()+" MP: "+Hero.getMP()+" PP: "+Hero.getPP());
		add(label1);
		if (Hero.getName() == "Wizard")
			picture="src\\ce1002\\a6\\s101201005\\Wizard.jpg";
		else if (Hero.getName() == "Knight")
			picture="src\\ce1002\\a6\\s101201005\\Knight.jpg";
		else if (Hero.getName() == "Swordsman")
			picture="src\\ce1002\\a6\\s101201005\\Swordsman.jpg";
		ImageIcon image = new ImageIcon(picture);
		JLabel label2=new JLabel(image);
		add(label2);
	}
}

