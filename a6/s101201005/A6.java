package ce1002.a6.s101201005;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
public class A6 {

	public static void main(String[] args) {
		MyPanel hero[]=new MyPanel [3]; //define hero array
		for (int i=0;i<3;i++)
		{
			hero[i]=new MyPanel();
		}
		Hero H[]=new Hero[3];
		H[0]=new Wizard();
		H[1]=new Swordsman();
		H[2]=new Knight();
		H[0].setName("Wizard");
		H[1].setName("Swordsman");
		H[2].setName("Knight");
		for ( int i=0;i<3;i++)
		{
			H[i].setHP(30);
			H[i].setMP(30);
			H[i].setPP(30);
		}
		
		hero[0].setRoleState(H[0]);
		hero[1].setRoleState(H[1]);
		hero[2].setRoleState(H[2]);
		
		MyFrame F=new MyFrame();
		for (int i=0;i<3;i++)
		{
			F.newRolePos(hero[i]);
		}
		
		F.setSize(270,700) ;
		F.setLocationRelativeTo(null) ;
		F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		F.setVisible(true);
	}

}
