package ce1002.a6.s101201005;

public class Hero {
	private String name;
	protected double HP=0;
	protected double MP=0;
	protected double PP=0;
	public void setName(String name)//set name
	{
		this.name=name;
	}
	public String getName()//use get name
	{
		return name;
	}
	public void setHP(double HP)//set HP
	{
		this.HP=HP;
	}
	public double getHP()//return HP
	{
		return HP;
	}
	public void setMP(double MP)//set MP
	{
		this.MP=MP;
	}
	public double getMP()//return MP
	{
		return MP;
	}
	public void setPP(double PP)//set PP
	{
		this.PP=PP;
	}
	public double  getPP()//return PP
	{
		return PP;
	}
}
