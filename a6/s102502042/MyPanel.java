package ce1002.a6.s102502042;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	
	public void setRoleState(Hero hero) {
		this.setLayout(new BorderLayout());	//設定layout
		this.setBorder(BorderFactory.createLineBorder(Color.black, 3));	//線邊框
		label1.setText(hero.getName()+" HP:"+hero.getHP()+" MP:"+hero.getMP()+" PP:"+hero.getPP());
		label2.setIcon(new ImageIcon(hero.getPath()));
		//放進panel
		this.add(label1,BorderLayout.NORTH);
		this.add(label2,BorderLayout.CENTER);
	}
}
