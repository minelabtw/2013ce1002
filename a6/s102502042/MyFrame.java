package ce1002.a6.s102502042;

import javax.swing.*;

public class MyFrame extends JFrame {
	//三個panel
	MyPanel p1 = new MyPanel();
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();

	MyFrame()
	{
		/*三個英雄*/
		Hero knight = new Knight();
		Hero swordsman = new Swordsman();
		Hero wizard = new Wizard();
		//設定大小、英雄、位置
		this.newRolePos(wizard,this.p3,250,200);
		this.p3.setLocation(10,10);
		this.newRolePos(swordsman, this.p2, 250, 200);
		this.p2.setLocation(10, 220);
		this.newRolePos(knight,this.p1,250,200);
		this.p1.setLocation(10,430);
	}
	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setRoleState(hero);
		panel.setSize(x,y);	//設定大小
		this.add(panel);
	}
}
