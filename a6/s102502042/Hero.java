package ce1002.a6.s102502042;

public class Hero {
	protected String name;
	protected float HP;
	protected float MP;
	protected float PP;
	
	Hero()	//constructor
	{
		HP = MP = PP = (float)30.0;
	}
	//setter
	void setName(String name)
	{
		this.name = name;
	}
	void setHP(float HP)
	{
		this.HP = HP;
	}
	void setMP(float MP)
	{
		this.MP = MP;
	}
	void setPP(float PP)
	{
		this.PP = PP;
	}
	//getter
	String getName()
	{
		return this.name;
	}
	float getHP()
	{
		return this.HP;
	}
	float getMP()
	{
		return this.MP;
	}
	float getPP()
	{
		return this.PP;
	}
	String getPath()
	{
		return this.name+".jpg";
	}
}
