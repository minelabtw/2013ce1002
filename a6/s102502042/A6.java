package ce1002.a6.s102502042;
import java.awt.*;
import javax.swing.*;
public class A6 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		frame.setSize(276,670);	//設定frame的大小
		frame.setResizable(false);	//固定frame大小
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);	//取消預設Layout，防止panel自適應大小
		frame.setVisible(true);
	}

}
