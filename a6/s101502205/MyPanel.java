package ce1002.a6.s101502205;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Graphics;

public class MyPanel extends JPanel{

	private JLabel tlbl, ilbl;	// assignment request
	
	public MyPanel(){
		setLayout(null);	// no layout
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawRect(0, 0, 270, 230);	// Draw a rectangle at (0, 0) with width 270 and height 230
	}
	
	public void setRoleState(Hero hero){
		tlbl = new JLabel(hero.getName() + " HP: " + hero.getHp() + " MP: " + hero.getMp() + " PP: " + hero.getPp());
		tlbl.setBounds(10, 10, 250, 15);	// Set text location and size
		ilbl = new JLabel(new ImageIcon(hero.getName() + ".jpg"));	// image
		ilbl.setBounds(10, 30, 250, 190);	// Set image location and size
		add(tlbl);	// add to panel
		add(ilbl);
	}
	
}
