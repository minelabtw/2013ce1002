package ce1002.a6.s101502205;

public class Wizard extends Hero {

	public Wizard() {
		// Set the name
		name = "Wizard";
	}
	
	// Overridings
	public float getHp() {
		return (float)0.2*hp;
	}
	public float getMp() {
		return (float)0.7*mp;
	}
	public float getPp() {
		return (float)0.1*pp;
	}
	
}
