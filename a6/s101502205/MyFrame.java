package ce1002.a6.s101502205;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	private MyPanel p1,p2,p3;	// assignment request
	
	public MyFrame(){
		setLayout(null);	// no layout
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setBounds(x, y, 280, 250);	// Set panel location and size
		panel.setRoleState(hero);			// Set hero state
		if(hero.getName() == "Wizard"){
			p1 = panel;
			add(p1);	// Add panel to frame
		}if(hero.getName() == "Swordsman"){
			p2 = panel;
			add(p2);	// Add panel to frame
		}if(hero.getName() == "Knight"){
			p3 = panel;
			add(p3);	// Add panel to frame
		}
	}
	
}
