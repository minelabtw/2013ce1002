package ce1002.a6.s101502205;

public class Hero {
	
	protected String name;
	protected float hp, mp, pp;
	
	public Hero() {
		// Initialize
		hp = 30;
		mp = 30;
		pp = 30;
	}
	
	// Getters and Setters
	// Name
	public void setName (String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	// HP
	public void setHp (float hp) {
		this.hp = hp;
	}
	public float getHp() {
		return hp;
	}
	
	// MP
	public void setMp(float mp) {
		this.mp = mp;
	}
	public float getMp() {
		return mp;
	}
	
	// PP
	public void setPp(float pp) {
		this.pp = pp;
	}
	public float getPp() {
		return pp;
	}
}
