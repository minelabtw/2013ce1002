package ce1002.a6.s101502205;

public class Swordsman extends Hero {
	
	public Swordsman() {
		// Set the name
		name = "Swordsman";
	}
	
	// Overridings
	public float getHp() {
		return (float)0.1*hp;
	}
	public float getMp() {
		return (float)0.1*mp;
	}
	public float getPp() {
		return (float)0.8*pp;
	}
	
}
