package ce1002.a6.s101502205;

public class Knight extends Hero {

	public Knight() {
		// Set the name
		name = "Knight";
	}
	
	// Overridings
	public float getHp() {
		return (float)0.8*hp;
	}
	public float getMp() {
		return (float)0.1*mp;
	}
	public float getPp() {
		return (float)0.1*pp;
	}
	
}
