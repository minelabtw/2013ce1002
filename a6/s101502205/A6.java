package ce1002.a6.s101502205;

public class A6 {

	public static void main(String[] args) {
		
		MyFrame frame = new MyFrame();		// Create a frame
		
		frame.newRolePos(new Wizard(), new MyPanel(), 15, 15);		// Add Wizard
		frame.newRolePos(new Swordsman(), new MyPanel(), 15, 265);	// Add Swordsman
		frame.newRolePos(new Knight(), new MyPanel(), 15, 515);		// Add Knight
		
		frame.setSize(320, 810);	// Set frame size
		frame.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE); 
		frame.setLocationRelativeTo(null);	// Center
		frame.setVisible(true);		// Display
		
	}

}
