package ce1002.a6.s102502536;

public class Wizard extends Hero {
	
	public Wizard() {  // constructor
		
	}
	
	public double getHP() {  // setters and getters
		return super.getHP() * 0.2;
	}
	public double getMP() {
		return super.getMP() * 0.7;
	}
	public double getPP() {
		return super.getPP() * 0.1;
	}
	public String getName() {
		super.setName("Wizard");
		return super.getName();
	}

}

