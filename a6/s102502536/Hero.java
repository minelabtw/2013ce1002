package ce1002.a6.s102502536;

public class Hero {
	
	private double HP = 30.0;  // declare variable
	private double MP = 30.0;
	private double PP = 30.0;
	private String Name;
	
	public Hero() {  // constructor
		
	}
	
	public double getHP() {  // setters and getters
		return HP;
	}
	public void setHP(Double HP) {
		this.HP = HP;
	}
	public double getMP() {
		return MP;
	}
	public void setMP(Double MP) {
		this.MP = MP;
	}
	public double getPP() {
		return PP;
	}
	public void setPP(Double PP) {
		this.PP = PP;
	}
	public String getName() {
		return Name;
	}
	public void setName(String Name) {
		this.Name = Name;
	}


}
