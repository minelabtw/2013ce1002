package ce1002.a6.s102502536;

public class A6 {

	public static void main(String[] args) {
		
		 Hero [] Hero = new Hero[3];  // declare the class array
		
		 Hero[0] = new Wizard();  // initialize
		 Hero[0].setHP(30.0);
		 Hero[0].setMP(30.0);
		 Hero[0].setPP(30.0);
		 Hero[1] = new Swordsman();
		 Hero[1].setHP(30.0);
		 Hero[1].setMP(30.0);
		 Hero[1].setPP(30.0);
		 Hero[2] = new Knight();
		 Hero[2].setHP(30.0);
		 Hero[2].setMP(30.0);
		 Hero[2].setPP(30.0);
		 
		 for (int i = 0; i < 3; i++)  // output the line
		 {
			 System.out.println(Hero[i].getName() + " HP: " + Hero[i].getHP() + " MP: " + Hero[i].getMP() + " PP: " + Hero[i].getPP());
		 }
		 	 
	}

}
