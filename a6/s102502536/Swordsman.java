package ce1002.a6.s102502536;

public class Swordsman extends Hero {
	
	public Swordsman() {  // constructor
		
	}
	
	public double getHP() {  // setters and getters
		return super.getHP() * 0.1;
	}
	public double getMP() {
		return super.getMP() * 0.1;
	}
	public double getPP() {
		return super.getPP() * 0.8;
	}
	public String getName() {
		super.setName("Swordsman");
		return super.getName();
	}

}

