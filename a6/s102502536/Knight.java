package ce1002.a6.s102502536;

public class Knight extends Hero {
	
	public Knight() {  // constructor
		
	}
	
	public double getHP() {  // setters and getters
		return super.getHP() * 0.8;
	}
	public double getMP() {
		return super.getMP() * 0.1;
	}
	public double getPP() {
		return super.getPP() * 0.1;
	}
	public String getName() {
		super.setName("Knight");
		return super.getName();
	}

}
