/**
 *  @name Knight.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 3/29/2014
 *  This class is to build class Knight which has weight point of Hp, Mp, Pp and inherit class Hero.
 *  And override getHP, getMp, getPP.
 */
package ce1002.a6.s100203020;

public class Knight extends Hero{
	/* weight point of Hp, Mp, Pp for Knight */
	private float weightOfHP= (float) 0.8;
	private float weightOfMP= (float) 0.1;
	private float weightOfPP= (float) 0.1;
	
	/** Knight constructor*/
	public Knight(){
		super.setName("Knight");
		super.setPic(super.getName()+".jpg");
	}
	
	/** Overriding getHP, getMP, getPP */
	@Override
	public float getHP(){
		return super.getHP()*weightOfHP;
	}
	@Override
	public float getMP(){
		return super.getMP()*weightOfMP;
	}
	@Override
	public float getPP(){
		return super.getPP()*weightOfPP;		
	}
}
	
	
