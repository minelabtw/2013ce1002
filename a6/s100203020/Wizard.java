/**
 *  @name Wizard.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 3/29/2014
 *  This class is to build class Wizard which has weight point of Hp, Mp, Pp, and inherit class Hero.
 *  And override getHP, getMp, getPP.
 */
package ce1002.a6.s100203020;

public class Wizard extends Hero{
	/* weight point of Hp,Mp,Pp for Wizard */
	private float weightOfHP= (float) 0.2;
	private float weightOfMP= (float) 0.7;
	private float weightOfPP= (float) 0.1;
	
	/** Wizard constructor*/
	public Wizard(){
		super.setName("Wizard");
		super.setPic(getName()+".jpg");
	}

	/** Overriding getHP, getMP, getPP */
	@Override
	public float getHP(){
		return super.getHP()*weightOfHP;
	}
	@Override
	public float getMP(){
		return super.getMP()*weightOfMP;
	}
	@Override
	public float getPP(){
		return super.getPP()*weightOfPP;		
	}
}
