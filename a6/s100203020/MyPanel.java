package ce1002.a6.s100203020;

import javax.swing.*;
import javax.swing.border.Border;
public class MyPanel extends JPanel{
	public MyPanel(Hero hero){
		//call setRoleState
		setRoleState(hero);
	}
	
	Hero hero = new Hero();
	JLabel lableA;
	JLabel lableB;
	public void setRoleState(Hero hero){
		//icon
		ImageIcon a = new ImageIcon(hero.getName()+".jpg");
		lableA = new JLabel(a, JLabel.CENTER);
		lableA.setIcon(a);
		lableB = new JLabel(hero.getName() +" HP: " + hero.getHP()
				+ " MP: " + hero.getMP() + " PP: " + hero.getPP(), JLabel.LEFT);
		lableB.setVerticalTextPosition(JLabel.TOP);
		lableB.setHorizontalTextPosition(JLabel.LEFT);
		Border blackline;

		add(lableB);
		add(lableA);
		
	}
	
	
}
