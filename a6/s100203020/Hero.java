/**
 *  @name Hero.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 3/29/2014
 *  This class is to build class Hero which has name, HP, MP,and PP.
 *  
 */

package ce1002.a6.s100203020;

public class Hero {
	
	private String name; // name of hero
	/* initial HP, MP, PP are 30.0 */
	private float HP = (float) 30.0;
	private float MP = (float) 30.0;
	private float PP = (float) 30.0;
	private String pic ;
	/** Setter and Getter   */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getHP() {
		return HP;
	}
	public void setHP(float hP) {
		HP = hP;
	}
	public float getMP() {
		return MP;
	}
	public void setMP(float mP) {
		MP = mP;
	}
	public float getPP() {
		return PP;
	}
	public void setPP(float pP) {
		PP = pP;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String Pic) {
		this.pic = Pic;
	}
}
