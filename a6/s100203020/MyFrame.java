package ce1002.a6.s100203020;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.Border;

public class MyFrame extends JFrame{
	public MyFrame(){
		 //HEROs
		  Wizard w = new Wizard();
		  Swordsman s = new Swordsman();
		  Knight k = new Knight();
		  //panel 1 2 3
		  MyPanel panel1 = new MyPanel(w);
		  MyPanel panel2 = new MyPanel(s);
		  MyPanel panel3 = new MyPanel(k);
		  this.setLayout(new GridLayout(3, 1));
		  //new position
		  newRolePos(w, panel1, 0,0);
		  newRolePos(s, panel2, 0,0);
		  newRolePos(k, panel3, 0,0);
		 
	}

	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		this.add(panel);
		//Border
		Border blackline;               
		blackline = BorderFactory.createLineBorder(Color.black);
		panel.setBorder(blackline);
		panel.setBounds(x, y, 300, 0);
		
		
		
		
	}	
	
}
