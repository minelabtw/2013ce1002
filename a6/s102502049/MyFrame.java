package ce1002.a6.s102502049;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame() {
		setLayout(new FlowLayout(FlowLayout.LEFT, 7, 7)); // choose layout method and set gap
		add(new MyPanel(new Wizard()));
		add(new MyPanel(new Swordsman()));
		add(new MyPanel(new Knight())); // add three panel to frame
	}
}
