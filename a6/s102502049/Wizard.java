package ce1002.a6.s102502049;

public class Wizard extends Hero {
	public Wizard(){ // default constructor
		super.setName("Wizard");
	}
	
	@Override
	public float getHP(){
		return super.getHP()*2/10;
	}
	
	public float getMP(){
		return super.getMP()*7/10;
	}
	
	public float getPP(){
		return super.getPP()*1/10;
	}
}
