package ce1002.a6.s102502049;

public class Hero {
	String name;
	private float HP=30;
	private float MP=30;
	private float PP=30;
	
	public Hero(){ // default constructor
		
	}
	
	public void setName(String x) { // setter
		name = x;
	}
	
	public void setHP(float x){
		HP = x;
	}
	
	public void setMP(float x){
		MP = x;
	}
	
	public void setPP(float x){
		PP = x;
	}
	
	public String getName(){ // getter
		return name;
	}
	
	public float getHP(){
		return HP;
	}
	
	public float getMP(){
		return MP;
	}
	
	public float getPP(){
		return PP;
	}
	
}
