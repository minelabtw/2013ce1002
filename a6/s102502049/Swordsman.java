package ce1002.a6.s102502049;

public class Swordsman extends Hero {
	public Swordsman(){ // default constructor
		super.setName("Swordsman");
	}
	
	@Override
	public float getHP(){
		return super.getHP()*1/10;
	}
	
	public float getMP(){
		return super.getMP()*1/10;
	}
	
	public float getPP(){
		return super.getPP()*8/10;
	}
}
