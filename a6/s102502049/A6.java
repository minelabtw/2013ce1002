package ce1002.a6.s102502049;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame(); //make a Frame object
		frame.setSize(285,710); // size
		frame.setLocationRelativeTo(null); // show out in center
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // set close
		frame.setVisible(true); // visible
	}
}
