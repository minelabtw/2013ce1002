package ce1002.a6.s102502049;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	public MyPanel(Hero hero){
		String picture = hero.getName() + ".jpg"; // picture name
		setLayout(new BorderLayout()); // choose borderlayout
		add(new JLabel(hero.getName() + " HP:" + String.valueOf(hero.getHP()) + " MP:" + String.valueOf(hero.getMP()) + " PP:" + String.valueOf(hero.getPP())),BorderLayout.NORTH);
		add(new JLabel(new ImageIcon(picture)),BorderLayout.SOUTH); // set role's state north and picture south
		setBorder(BorderFactory.createLineBorder(Color.black, 3)); // draw line
	}
	
}

