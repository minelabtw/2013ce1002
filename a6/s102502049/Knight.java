package ce1002.a6.s102502049;

public class Knight extends Hero {
	public Knight(){ // default constructor
		super.setName("Knight");
	}
	
	@Override
	public float getHP(){
		return super.getHP()*8/10;
	}
	
	public float getMP(){
		return super.getMP()*1/10;
	}
	
	public float getPP(){
		return super.getPP()*1/10;
	}
	
}
