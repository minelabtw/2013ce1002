package ce1002.a6.s102502532;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] heros = new Hero[3];                  //物件陣列
		heros[0] = new Wizard();               //不同型態(父,子)
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		heros[0].setHeroName("Wizard");
		heros[1].setHeroName("Swordsman");
		heros[2].setHeroName("Knight");
		
		MyPanel myp0 = new MyPanel(heros[0].getHeroName() + " HP: " + heros[0].getHP() + " MP: " + heros[0].getMP() + " PP: " +  heros[0].getPP(),"Wizard.jpg");
		MyFrame frame = new MyFrame();
		
		MyPanel myp1 = new MyPanel(heros[1].getHeroName() + " HP: " + heros[1].getHP() + " MP: " + heros[1].getMP() + " PP: " +  heros[1].getPP(),"Swordsman.jpg");
		MyPanel myp2 = new MyPanel(heros[2].getHeroName() + " HP: " + heros[2].getHP() + " MP: " + heros[2].getMP() + " PP: " +  heros[2].getPP(),"Knight.jpg");
		
		frame.newRolePos(heros[0],myp0,250,190);
		frame.newRolePos(heros[1],myp1,250,190);
		frame.newRolePos(heros[2],myp2,250,190);
		
		/*System.out.println(heros[0].getHeroName() + " HP: " + heros[0].getHP() + " MP: " + heros[0].getMP() + " PP: " +  heros[0].getPP());  //印出
		System.out.println(heros[1].getHeroName() + " HP: " + heros[1].getHP() + " MP: " + heros[1].getMP() + " PP: " +  heros[1].getPP());
		System.out.println(heros[2].getHeroName() + " HP: " + heros[2].getHP() + " MP: " + heros[2].getMP() + " PP: " +  heros[2].getPP());*/
	}

}
