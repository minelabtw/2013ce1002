package ce1002.a6.s102502532;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {

	public MyPanel(String label, String imageName) {
		JPanel p = new JPanel(new BorderLayout());
		p.add(new JLabel(label), BorderLayout.NORTH);

		ImageIcon image1 = new ImageIcon(imageName);          //不用給絕對路徑讀取圖片 
		p.add(new JLabel(image1), BorderLayout.CENTER);
	}

	public void setRoleState(Hero hero) {

	}

}
