package ce1002.a6.s101201522;

import javax.swing.*;

public class MyPanel extends JPanel {
	private JLabel state;
	private JLabel image;
	public void setRoleState (Hero hero) {
		state = new JLabel(hero.getName() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() , JLabel.CENTER);
		//set state
		image = new JLabel(new ImageIcon(hero.getName() + ".jpg"),JLabel.CENTER);
		//import image
		add(state);
		add(image);
	}
}
