package ce1002.a6.s101201522;

public class Hero {
	private String name;
	private double hp;//health point
	private double mp;//mana point
	private double pp;//power point
	
	Hero () {//initailize hero's status
		hp = 30.0;
		mp = 30.0;
		pp = 30.0;
	}
	
	public void setName (String name) {//set Hero's name
		this.name = name;
	}
	
	public void setHp (double hp) {//set hp
		this.hp = hp;
	}
	
	public void setMp (double mp) {//set mp
		this.mp = mp;
	}
	
	public void setPp (double pp) {//set pp
		this.pp = pp;
	}
	
	public String getName () {//get Hero's name
		return name;
	}
	
	public double getHp () {//get hp
		return hp;
	}
	
	public double getMp () {//get mp
		return mp;
	}
	
	public double getPp () {//get pp
		return pp;
	}
	
	public void printStatus () {//print hero's status
		System.out.println(name + " HP:" + hp + " MP:" + mp + " PP:" + pp);
	}
}