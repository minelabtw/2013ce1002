package ce1002.a6.s101201522;

import javax.swing.*;

import java.awt.Color;
import java.awt.GridLayout;

public class MyFrame extends JFrame {
	private MyPanel[] heros = new MyPanel[3];
	MyFrame () {
		setLayout(null);
		setSize(315,800);//frame size
		heros[0] = new MyPanel();//add wizard panel
		newRolePos(new Wizard(),heros[0],20,20);
		heros[1] = new MyPanel();//add swordsman panel
		newRolePos(new Swordsman(),heros[1],20,265);
		heros[2] = new MyPanel();//add knight panel
		newRolePos(new Knight(),heros[2],20,510);
		setVisible(true);//visible
	}
	public void newRolePos (Hero hero, MyPanel panel, int x, int y) {
		panel.setBounds(x,y,260,225);//set panel position
		panel.setRoleState(hero);//set panel content
		panel.setBorder(BorderFactory.createLineBorder(Color.black,3));//set border
		add(panel);
	}
}
