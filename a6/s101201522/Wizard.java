package ce1002.a6.s101201522;

public class Wizard extends Hero {
	Wizard () {//construct wizard
		setName("Wizard");
		setHp(getHp()*0.2);
		setMp(getMp()*0.7);
		setPp(getPp()*0.1);
	}
}