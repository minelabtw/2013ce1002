package ce1002.a6.s102502529;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel
{
	JLabel info;
	JLabel img;
	ImageIcon icon;
	MyPanel()
	{
		this.setSize(280, 230);
		this.setBorder(new LineBorder(Color.black, 7));
	}
	
	public void setRoleState(Hero hero)						//�ɤJ���A
	{
		icon = new ImageIcon(hero.getName() + ".jpg");
		info = new JLabel(hero.getName() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		img = new JLabel(icon);
		this.add(info);
		this.add(img);
	}
}