package ce1002.a6.s102502517;

import java.awt.*;
import javax.swing.*;

public class A6 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3]; //宣告Hero陣列
		MyPanel[] panel = new MyPanel[3];
		
		hero[0] = new Wizard(); //實體化
		hero[1] = new Swordsman(); //實體化
		hero[2] = new Knight(); //實體化
		
		hero[0].setName("Wizard"); //設定名稱
		hero[1].setName("Swordsman"); //設定名稱
		hero[2].setName("Knight"); //設定名稱
		
		MyFrame frame = new MyFrame(); //輸出圖片
		frame.setVisible(true);
		frame.setSize(500,600);
		frame.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		frame.setLayout(new GridLayout(3,1));
		for(int i=0;i<=2;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i],panel[i],5,5);
		}
	}
}
