package ce1002.a6.s102502517;

import ce1002.a6.s102502517.Hero;

public class Swordsman extends Hero {
	public Swordsman()
	{
	}
	
	public float getHP() { //調整權重
		return super.getHP()/10;
	}

	public float getMP() {
		return super.getMP()/10;
	}

	public float getPP() {
		return super.getPP()*8/10;
	}
}
