package ce1002.a6.s102502517;

public class Hero {
	private String name;
	private float HP;
	private float MP;
	private float PP;
	
	public Hero() //初始化數值
	{
		HP = 30;
		MP = 30;
		PP = 30;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getHP() {
		return HP;
	}
	public void setHP(float hP) {
		HP = hP;
	}
	public float getMP() {
		return MP;
	}
	public void setMP(float mP) {
		MP = mP;
	}
	public float getPP() {
		return PP;
	}
	public void setPP(float pP) {
		PP = pP;
	}
	
	
}
