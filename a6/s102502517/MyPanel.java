package ce1002.a6.s102502517;

import javax.swing.*;

public class MyPanel extends JPanel{
	public JLabel showState;
	public JLabel showImage;
	
	public MyPanel()
	{
		
	}
	
	public void setRoleState(Hero hero)
	{
		showState = new JLabel(hero.getName() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP(),JLabel.LEFT);
		ImageIcon image = new ImageIcon(hero.getName());
		showImage = new JLabel(image);
	}
}
