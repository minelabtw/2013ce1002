package ce1002.a6.s102502517;

import ce1002.a6.s102502517.Hero;

public class Wizard extends Hero {
	public Wizard()
	{
	}
	
	public float getHP() { //調整權重
		return super.getHP()*2/10;
	}

	public float getMP() {
		return super.getMP()*7/10;
	}

	public float getPP() {
		return super.getPP()/10;
	}
}
