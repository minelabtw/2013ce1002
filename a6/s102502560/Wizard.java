package ce1002.a6.s102502560;

public class Wizard extends Hero{
	public Wizard() {
		setName("Wizard");
	}
	
	@Override
	public double getHP() {
		return super.getHP()*0.2;
	}
	
	@Override
	public double getMP() {
		return super.getMP()*0.7;
	}
	
	@Override
	public double getPP() {
		return super.getPP()*0.1;
	}
}
