package ce1002.a6.s102502560;

import java.util.ArrayList;
import javax.swing.JFrame;

public class MyFrame extends JFrame {

	ArrayList<MyPanel> panels=new ArrayList<MyPanel>();
	
	public void newRolePos(Hero hero) {
		MyPanel panel=new MyPanel();
		panels.add(panel);
		
		panel.setRoleState(hero);
		setLayout(null);
		panel.setBounds(10, 10+(panels.size()-1)*230, 270, 220);
		this.add(panel);
	}
}
