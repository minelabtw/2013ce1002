package ce1002.a6.s102502560;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	Hero hero;
	JLabel statuslabel;
	JLabel picturelabel;
	
	public MyPanel() {
		statuslabel=new JLabel();
		picturelabel=new JLabel();
	}
	
	public void setRoleState(Hero hero){
		statuslabel.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		picturelabel.setIcon(new ImageIcon(hero.getName()+".jpg"));
		this.hero=hero;
		statuslabel.setBounds(10, 0, 250, 20);
		picturelabel.setBounds(10, 20, 250, 190);
		add(statuslabel);
		add(picturelabel);
		setBorder(new LineBorder(Color.BLACK, 2));
		setLayout(null);
	}
}
