package ce1002.a6.s102502560;

public class Knight extends Hero{
	public Knight() {
		setName("Knight");
	}
	
	@Override
	public double getHP() {
		return super.getHP()*0.8;
	}
	
	@Override
	public double getMP() {
		return super.getMP()*0.1;
	}
	
	@Override
	public double getPP() {
		return super.getPP()*0.1;
	}
}
