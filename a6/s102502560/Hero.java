package ce1002.a6.s102502560;

public class Hero {
	private String name;
	public double HP;
	private double MP;
	private double PP;
	
	public Hero() {
		HP=30.0;MP=30.0;PP=30.0;
	}
	
	public String getName() {
		return name;
	}
	
	public double getHP() {
		return HP;
	}
	
	public double getMP() {
		return MP;
	}
	
	public double getPP() {
		return PP;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setHP(double hp) {
		this.HP = hp;
	}
	
	public void setMP(double mp) {
		this.MP = mp;
	}
	
	public void setPP(double pp) {
		this.PP = pp;
	}
		
}
