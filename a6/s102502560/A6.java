package ce1002.a6.s102502560;

public class A6 {
	public static void main(String[] args) {
		Hero[] heros=new Hero[]{new Wizard(),new Swordsman(),new Knight()};		//superclass type array with different subclass object
		
		MyFrame frame=new MyFrame();
		for(Hero hero: heros){
			frame.newRolePos(hero);
		}
		frame.setSize(300,40+frame.panels.size()*230);
		frame.setVisible(true);
	}
}
