package ce1002.a6.s102502541;
import javax.swing.*;
public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int type = 3;
		Hero hero[] = new Hero[type];//建立 hero陣列
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame();
		MyPanel panel = new MyPanel();
		for(int i = 0; i<type; i++)
		{
			frame.newRolePos(hero[i],panel ,300,200);
		}//設定frame
		frame.setSize(300,700);
		frame.setLocationRelativeTo(null);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
