package ce1002.a6.s102502048;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel
{
	public MyPanel()
	{
		
	}
	public MyPanel(Hero hero , ImageIcon im)
	{
		setRoleState(hero , im);
	}
	JLabel lb1;
	// Hero hero = new Hero();
	public void setRoleState(Hero hero , ImageIcon im)
	{
		
		lb1 = new JLabel(" HP:" + String.valueOf(hero.getHP()) + " MP:" + String.valueOf(hero.getMP()) + " PP:" + String.valueOf(hero.getPP()),im, JLabel.CENTER); 
		lb1.setVerticalTextPosition(JLabel.TOP);
		lb1.setHorizontalTextPosition(JLabel.LEFT);
		this.add(lb1);
	}
}
