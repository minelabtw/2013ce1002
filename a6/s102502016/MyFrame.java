package ce1002.a6.s102502016;

import javax.swing.*;

public class MyFrame extends JFrame {
	public MyPanel panel1;
	public MyPanel panel2;
	public MyPanel panel3;

	public MyFrame() {
		this.setLayout(null); // 非預設排版
		this.setSize(250, 600); // 設置大小
		this.setVisible(true); // 可看見
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel = new MyPanel(); // 初始化
		panel.setRoleState(hero); // 設置圖片 角色資訊
		this.add(panel); // 加到FRAME�堶�
		panel.setLocation(x, y); // 設置PANEL位置
	}
}