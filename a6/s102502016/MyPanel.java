package ce1002.a6.s102502016;

import javax.swing.*;

public class MyPanel extends JPanel {

	public JLabel lb1;
	public JLabel lb2;

	public MyPanel() {
		this.setLayout(null); // 非預設排版
		this.setBounds(0, 0, 250, 200); // 邊界
		this.setSize(250, 200); // 大小
	}

	public void setRoleState(Hero hero) {
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg"); // 圖片物件
		lb1 = new JLabel(icon); // LABEL 1 讀圖片 設置大小 位置
		lb1.setSize(260, 200);
		lb1.setLocation(0, 15);
		this.add(lb1); // 加到Panel�堶�
		lb2 = new JLabel(); // LABEL 2 放文字 設置大小位置
		lb2.setText(hero.getName() + " HP:" + hero.getHP() + " MP:"
				+ hero.getMP() + " PP:" + hero.getPP());
		lb2.setSize(250, 10);
		lb2.setLocation(0, 0);
		this.add(lb2); // 加到Panel�堶�
	}
}