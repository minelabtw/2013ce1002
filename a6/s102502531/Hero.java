package ce1002.a6.s102502531;

public class Hero { 

	 String name;
	private float hp = 30; //Initial
	private float mp = 30;
	private float pp = 30;

	public void setName(String name) {  //method
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setHp(float hp) {
		this.hp = hp;
	}

	public float getHp() {
		return hp;
	}

	public void setMp(float mp) {
		this.mp = mp;

	}

	public float getMp() {
		return mp;
	}

	public void setPp(float pp) {
		this.pp = pp;

	}

	public float getPp() {
		return pp;
	}

}
