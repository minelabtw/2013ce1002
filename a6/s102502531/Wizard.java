package ce1002.a6.s102502531;

public class Wizard extends Hero {//Wizard hp mp pp

	public String getName() {
		return "Wizard";
	}

	public float getHp() {
		return super.getHp() * 0.2f;
	}

	public float getMp() {
		return super.getMp() * 0.7f;
	}

	public float getPp() {
		return super.getPp() * 0.1f;
	}

}
