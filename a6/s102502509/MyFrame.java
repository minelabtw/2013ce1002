package ce1002.a6.s102502509;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class MyFrame extends JFrame
{
	MyPanel p1 = new MyPanel(); // 讀取 MyPanel class 的東西!!!!!!!!
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();
	
	MyFrame() // 視窗建構子
	{
		setSize(250, 600); // 視窗大小
		setLayout(null); // manager
		setLocationRelativeTo(null); // 視窗置中
		
		Hero hero[] = new Hero[3]; // 利用陣列 要轉換!!!!!!!
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		// 將容器存入陣列
		newRolePos(hero[0], p1, 0, 0);
		newRolePos(hero[1], p2, 0, 200);
		newRolePos(hero[2], p3, 0, 400);
		
		// 將容器陣列存入視窗
		add(p1);
		add(p2);
		add(p3);
	}
	
	// 拿去panel class 的函式
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{

		panel.setBorder(new LineBorder(Color.BLACK, 2));
		panel.setRoleState(hero); 
		panel.setLocation(x, y); //容器在視窗的位置
	}
	
}
