package ce1002.a6.s102502509;

import javax.swing.*;

public class MyPanel extends JPanel 
{
	MyPanel()//panel(容器)建構子 建立範圍
	{
		setLayout(null); // manage
		setSize(250, 200); // 容器大小
		setBounds(0, 0, 250, 200); //容器邊界
	}
	
	public void setRoleState(Hero hero)
	{
		ImageIcon image = new ImageIcon(hero.getHeroname() + ".jpg"); // 利用函式，減少複雜度 建立路徑
		
		JLabel label1 = new JLabel(hero.getHeroname() 
							   	 + " HP: " + hero.getHP()
								 + " MP: " + hero.getMP()
								 + " PP: " + hero.getPP()); // 給定字串，利用hero。
		
		JLabel label2 = new JLabel(image); // 讀取圖片
		
		 // 設定標籤大小及位置
		label1.setSize(250, 10);
		label1.setLocation(0, 0);

		label2.setSize(250, 190);
		label2.setLocation(0, 10);
		
		 // 將標籤加入容器中
		
		add(label2);
		add(label1);
	}


}
