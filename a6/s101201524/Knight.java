package ce1002.a6.s101201524;

public class Knight extends Hero{
	//change hero's data with knight's coefficient
		Knight(){
			setName("Knight");
			setImage("Knight.jpg");
			setHp(getHp()*0.8);
			setMp(getMp()*0.1);
			setPp(getPp()*0.1);
		}
}
