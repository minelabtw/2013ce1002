package ce1002.a6.s101201524;

import javax.swing.ImageIcon;

public class Hero {
	private String name;
	private ImageIcon image;
	private double hp;
	private double mp;
	private double pp;
	
	Hero(){
		hp = mp = pp = 30;  //initialize hp, mp, pp to 30
	}
	public void setName(String name){  //name's setter
		this.name = name;
	}
	public void setImage(String image){  //image's setter
		this.image = new ImageIcon(image);
	}
	public void setHp(double hp){  //hp's setter
		this.hp = hp;
	}
	public void setMp(double mp){  //mp's setter
		this.mp = mp;
	}
	public void setPp(double pp){  //pp's setter
		this.pp = pp;
	}
	public String getName(){  //name's getter
		return name;
	}
	public ImageIcon getImage(){  //image's getter
		return image;
	}
	public double getHp(){  //hp's getter
		return hp;
	}
	public double getMp(){  //mp's getter
		return mp;
	}
	public double getPp(){  //pp's getter
		return pp;
	}
	public String getData(){  //output hero's name, hp, mp, pp
		return getName() + " HP: " + getHp() + " MP: " + getMp() + " PP: " + getPp();
	}
}
