package ce1002.a6.s101201524;

public class Wizard extends Hero{
	//change hero's data with wizard's coefficient
		Wizard(){
			setName("Wizard");
			setImage("Wizard.jpg");
			setHp(getHp()*0.2);
			setMp(getMp()*0.7);
			setPp(getPp()*0.1);
		}
}
