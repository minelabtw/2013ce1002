package ce1002.a6.s101201524;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel{
	private JLabel data; //label to show hero's data
	private JLabel image; //label to show hero's image
	
	MyPanel(){
	}
	
	public void setRoleState(Hero hero){  //set hero's data and image, and then add to panel
		data = new JLabel(hero.getData()); 
		image = new JLabel(hero.getImage());
		add(data);
		add(image);
	}
}
