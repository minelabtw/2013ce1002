package ce1002.a6.s102502040;

import ce1002.a6.s102502040.Hero;

public class Wizard extends Hero {
	Wizard(){
		super();
		setId("Wizard");
	}
	@Override
	float getHp(){
	  return super.getHp() * 0.2f;
	}
	@Override
	float getMp(){
		return super.getMp() * 0.7f;
	}
	@Override
	float getPp(){
		return super.getPp() * 0.1f;
	}
}
