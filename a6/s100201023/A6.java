package ce1002.a6.s100201023;

import ce1002.a6.s100201023.Hero;
import ce1002.a6.s100201023.Knight;
import ce1002.a6.s100201023.Swordsman;
import ce1002.a6.s100201023.Wizard;
import ce1002.a6.s100201023.MyFrame;
import ce1002.a6.s100201023.MyPanel;

public class A6
{

	public static void main(String[] args)
	{
		Hero[] hero = new Hero[3];
		MyFrame frame = new MyFrame();
		
		
		//set Wizard
		hero[0] = new Wizard();
		hero[0].setname("Wizard");
		
		//set Swordsman
		hero[1] = new Swordsman();
		hero[1].setname("Swordsman");
		
		//set Knight
		hero[2] = new Knight();
		hero[2].setname("Knight");
		
		//output
		frame.newRolePos(hero[0], frame.getpanel(hero[0]), 10, 10);
		frame.newRolePos(hero[1], frame.getpanel(hero[1]), 10, 245);
		frame.newRolePos(hero[2], frame.getpanel(hero[2]), 10, 480);
	}

}
