package ce1002.a6.s102502032;

import java.awt.Color;

import javax.swing.*;

public class MyPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private JLabel				properity			= new JLabel();
	private JLabel				img					= new JLabel();

	// constructor
	public MyPanel()
	{
		this.setVisible(true);
		this.add(properity);
		this.add(img);
		properity.setVisible(true);
		img.setVisible(true);
	}

	// method
	// show the propities, icon, and the border
	public void setRoleState(Hero hero)
	{
		properity.setText(hero.getName() + " HP:" + hero.getHP() + " MP:"
				+ hero.getMP() + " PP:" + hero.getPP());
		img.setIcon(hero.getIcon());
		this.setBorder(BorderFactory.createLineBorder(Color.black, 2, true));
	}
}
