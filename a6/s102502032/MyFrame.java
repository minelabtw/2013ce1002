package ce1002.a6.s102502032;

import javax.swing.*;

public class MyFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private MyPanel				panelList[]			= new MyPanel[3];
	private Hero				heroList[]			= new Hero[3];

	// constructor
	public MyFrame()
	{
		for (int i = 0; i < 3; i ++)
		{
			panelList[i] = new MyPanel();
			heroList[i] = new Hero();
		}
		this.setLayout(null);
		this.setVisible(true);
		this.setTitle(null);
		this.setSize(315, 790);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// set
	public void setHero(Hero[] hero)
	{
		heroList = hero;
	}

	public void setPanel()
	{
		for (int i = 0; i < 3; i ++)
		{
			this.newRolePos(heroList[i], i, 10, 20 + 240 * i);
		}
	}

	// method
	public void newRolePos(Hero hero, int i, int x, int y)
	{
		panelList[i].setRoleState(hero);
		panelList[i].setBounds(x, y, 275, 225);
		this.add(panelList[i]);
	}
}
