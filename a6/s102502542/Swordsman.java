package ce1002.a6.s102502542;

public class Swordsman extends Hero {
	public String getname() {
		return "Swordsman";
	}

	public double getHP()// 回傳加權值
	{
		return HP * 0.1;
	}

	public double getMP() {
		return MP * 0.1;
	}

	public double getPP() {
		return PP * 0.8;
	}
}
