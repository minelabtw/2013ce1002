package ce1002.a6.s102502542;

import java.util.Scanner;

import javax.swing.*;

public class A6 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Hero[] hero = new Hero[3];// 創建Hero物件陣列並取名hero
		hero[0] = new Wizard();// 將Wizard class儲存在hero陣列的第0個位置
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame myframe = new MyFrame();
		myframe.setSize(320,800);
		myframe.setLocationRelativeTo(null);
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setVisible(true);
		MyPanel mypanel = new MyPanel();
		MyPanel mypanel1 = new MyPanel();
		MyPanel mypanel2 = new MyPanel();
		myframe.newRolePos(hero[0],mypanel,10,10);
		myframe.newRolePos(hero[1],mypanel1,10,250);
		myframe.newRolePos(hero[2],mypanel2,10,490);
		
		

		// 輸出題目所需
		// System.out.println(hero[0].getname() + " HP: " + hero[0].getHP()
		// + " MP: " + hero[0].getMP() + " PP: " + hero[0].getPP());
		// System.out.println(hero[1].getname() + " HP: " + hero[1].getHP()
		// + " MP: " + hero[1].getMP() + " PP: " + hero[1].getPP());
		// System.out.println(hero[2].getname() + " HP: " + hero[2].getHP()
		// + " MP: " + hero[2].getMP() + " PP: " + hero[2].getPP());

	}

}
