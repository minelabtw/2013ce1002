package ce1002.a6.s102502556;

public class Wizard extends Hero {
	Wizard () //constructor，並重新設定英雄名稱
	{
		setId("Wizard");
	}
	float getHp () //回傳加權後的生命點數
	{	
		return super.getHp() * 0.2f;
	}
	float getMp () //回傳加權後的魔法點數
	{
		return super.getHp() * 0.7f;
	}
	float getPp () //回傳加權後的能力點數
	{
		return super.getHp() * 0.1f;
	}
}
