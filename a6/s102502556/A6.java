package ce1002.a6.s102502556;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
		Hero[] character = new Hero[3]; //宣告一個名為character的Hero型態陣列
		character[0] = new Wizard(); //宣告character的第一個元素為Wizard型態的Object
		character[1] = new Swordsman(); //宣告character的第二個元素為Swordsman型態的Object
		character[2] = new Knight(); //宣告character的第三個元素為Knight型態的Object
		MyPanel[] panel = new MyPanel[3]; //宣告一個名為panel的MyPanel型態陣列
		MyFrame frame = new MyFrame(); //宣告一個名為frame的MyFrame型態的變數	
		frame.setSize(320, 770); //設定視窗大小
		frame.setLocationRelativeTo(null); //設定視窗位置
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //設定按視窗右上角的"X"會自動關閉程序
		for ( int i = 0 ; i < 3 ; i++ ) //用for迴圈設定panel陣列裡的元素
		{
			panel[i] = new MyPanel();
			frame.newRolePos(character[i], panel[i], 10, 10 + 240 * i);
		}
		frame.setVisible(true); //使視窗能顯示在螢幕上
	}
}
