package ce1002.a6.s102502556;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	JLabel state; //英雄狀態
	JLabel image; //圖片資料
	ImageIcon icon; //儲存圖片路徑
	MyPanel () {
		this.setSize(280, 230); //設定panel的大小
		this.setBorder(new LineBorder(Color.black, 5));	//設定panel的邊界屬性
	}
	public void setRoleState(Hero hero) {
		icon = new ImageIcon(hero.getId()+".jpg"); //設定icon為圖片路徑
		state = new JLabel(hero.getId()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP: "+hero.getPp());
		//設定state的屬性
		image = new JLabel(icon); //設定image的屬性
		this.add(state); //把設定好的JLable添加到Panel裡
		this.add(image);
	}
}
