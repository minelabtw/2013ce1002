package ce1002.a6.s102502556;

public class Knight extends Hero {
	Knight () //constructor，並重新設定英雄名稱
	{
		setId("Knight");
	}
	float getHp () //回傳加權後的生命點數
	{	
		return super.getHp() * 0.8f;
	}
	float getMp () //回傳加權後的魔法點數
	{
		return super.getMp() * 0.1f;
	}
	float getPp () //回傳加權後的能力點數
	{
		return super.getPp() * 0.1f;
	}
}
