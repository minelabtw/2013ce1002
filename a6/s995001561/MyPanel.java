package ce1002.a6.s995001561;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	
	// create status and picture
	JLabel status = new JLabel();
	JLabel picture = new JLabel();
	
	// set border
	public MyPanel() {
		this.add(status);
		this.add(picture);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
		}
	
	
	public void setRoleState(Hero hero){
		
		//show status in label
		status.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		picture.setIcon(hero.getIcon());
		
	}
}
