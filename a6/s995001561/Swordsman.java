package ce1002.a6.s995001561;

import javax.swing.ImageIcon;


public class Swordsman extends Hero{
	
	protected double HP = 30.0;
	protected double MP = 30.0;
	protected double PP = 30.0;
	
	//read picture
	Swordsman(){
		setName("Swordsman");
		icon = new ImageIcon("Swordsman.jpg");
	}
	
	// modify getHP method
	public double getHP() {
		return HP*0.1;
	}
	
	// modify getMP method
	public double getMP() {
		return MP*0.1;
	}
	
	// modify getPP method
	public double getPP() {
		return PP*0.8;
	}
	

}
