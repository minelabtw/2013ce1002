package ce1002.a6.s995001561;

import javax.swing.ImageIcon;

public class Hero {

	protected String name="Hero";
	protected ImageIcon icon;
	protected double HP = 30.0;
	protected double MP = 30.0;
	protected double PP = 30.0;
	
	Hero(){
		
	}
	
	// create HP getter and setter
	public double getHP() {
		return HP;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	
	// create MP getter and setter
	public double getMP() {
		return MP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	
	// create PP getter and setter
	public double getPP() {
		return PP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ImageIcon getIcon() {
		return icon;
	}
	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}
	
}