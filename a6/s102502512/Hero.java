package ce1002.a6.s102502512;

public class Hero {
	private String name;
	private double hp;
	private double mp;
	private double pp;
	
	public String getName() {				//make some function to set and get heros' abilities
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHp() {
		return hp;
	}
	public void setHp(double hp) {
		this.hp = hp;
	}
	public double getMp() {
		return mp;
	}
	public void setMp(double mp) {
		this.mp = mp;
	}
	public double getPp() {
		return pp;
	}
	public void setPp(double pp) {
		this.pp = pp;
	}

}
