package ce1002.a6.s101201023;

import javax.swing.*;

public class MyPanel extends JPanel
{
	private JLabel rolekind;
	private JLabel rolepic;
	
	public void setRoleState(Hero hero)
	{
		rolekind = new JLabel(hero.getHeroName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + "PP: " + hero.getPP());
		if (hero.getHeroName() == "Wizard")
		{
			rolepic = new JLabel(new ImageIcon("C:\\Users\\smile\\workspace\\A6\\src\\ce1002\\Wizard.jpg"));
		}
		else if(hero.getHeroName() == "Swordsman")
		{
			rolepic = new JLabel(new ImageIcon("C:\\Users\\smile\\workspace\\A6\\src\\ce1002\\Swordsman.jpg"));
		}
		else if (hero.getHeroName() == "Knight")
		{
			rolepic = new JLabel(new ImageIcon("C:\\Users\\smile\\workspace\\A6\\src\\ce1002\\Knight.jpg"));
		}
		add(rolekind);
		add(rolepic);
	}
}
