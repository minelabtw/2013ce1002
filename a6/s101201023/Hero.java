package ce1002.a6.s101201023;

public class Hero 
{
	String name;
	private double hp=30;
	private double mp=30;
	private double pp=30;
	
	public void setHeroName(String n)            //傳入名字
	{
		if(n!="")
			name = n;
	}
	
	public String getHeroName()                 //回傳名字
	{
		return name;
	}
	
	public void setHP(double h)
	{
		if(h>=0)
			hp = h;
	}
	
	public double getHP()
	{
		return hp;
	}
	
	public void setMP(double m)
	{
		if(m>=0)
			mp = m;
	}
	
	public double getMP()
	{
		return mp;
	}
	
	public void setPP(double p)
	{
		if(p>=0)
			pp = p;
	}
	
	public double getPP()
	{
		return pp;
	}
}
