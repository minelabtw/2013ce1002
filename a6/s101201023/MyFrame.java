package ce1002.a6.s101201023;

import java.awt.GridLayout;

import javax.swing.*;

public class MyFrame extends JFrame
{
	private MyPanel wizard;
	private MyPanel swordsman;
	private MyPanel knight;
	
	public void newRolePos(Hero heroW , Hero heroS ,Hero heroK)
	{
		setLayout(new GridLayout(3,1));
		wizard.setRoleState(heroW);
		swordsman.setRoleState(heroS);
		knight.setRoleState(heroK);
		add(wizard);
		add(swordsman);
		add(knight);
	}
}
