package ce1002.a6.s101201023;

import javax.swing.JFrame;

public class A6 
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub

			Hero hero[] = new Hero[3];                   //創建一個Hero型態的陣列
			
			hero[0] = new Wizard();                      //創建新物件並丟進陣列裡
			hero[1] = new Swordsman();
			hero[2] = new Knight();
			
			hero[0].setHeroName("Wizard");               //將name傳入
			hero[1].setHeroName("Swordsman");
			hero[2].setHeroName("Knight");
			for(int i=0 ; i < 3 ; i++)                   
			{
				hero[i].setHP(30);
				hero[i].setMP(30);
				hero[i].setPP(30);
			}
			
			//輸出
			MyFrame frame= new MyFrame();
			frame.newRolePos(hero[0],hero[1],hero[2]);
			frame.setSize(270,700);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
	}

}
