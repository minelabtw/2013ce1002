package ce1002.a6.s102502006;

public class Wizard extends Hero{
	
	public Wizard()
	{
		setId("Wizard");
	}
	public String getId()
	{
	  return super.getId();
	}
	public float getHp()
	{
	  return super.getHp()*0.2f;
	}
	public float getMp()
	{
	  return super.getMp()*0.7f;
	}
	public float getPp()
	{
	  return super.getPp()*0.1f;
	}

}
