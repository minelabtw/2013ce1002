package ce1002.a6.s101201521;

public class Swordsman extends Hero{
	//create swordsman with hero's name and initialize status
	public Swordsman(String name){
		super(name);
		super.setHp(super.getHp() * 0.1);
		super.setMp(super.getMp() * 0.1);
		super.setPp(super.getPp() * 0.8);
	}
}
