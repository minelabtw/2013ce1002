package ce1002.a6.s101201521;

public class Hero {
	private String name;
	private double hp;
	private double mp;
	private double pp;
	//create with hero's name and initialize status
	public Hero(String name){
		setName(name);
		setHp(30.0);
		setMp(30.0);
		setPp(30.0);
	}
	//set name of this hero
	public void setName(String name){
		this.name = name;
	}
	//set hp of this hero
	public void setHp(double hp){
		this.hp = hp;
	}
	//set mp of this hero
	public void setMp(double mp){
		this.mp = mp;
	}
	//set pp of this hero
	public void setPp(double pp){
		this.pp = pp;
	}
	//return name of this hero
	public String getName(){
		return name;
	}
	//return hp of this hero
	public double getHp(){
		return hp;
	}
	//return mp of this hero
	public double getMp(){
		return mp;
	}
	//return pp of this hero
	public double getPp(){
		return pp;
	}
	public String toString(){
		return this.getName();
	}
}
