package ce1002.a6.s101201521;

public class Wizard extends Hero{
	//create wizard with hero's name and initialize status
	public Wizard(String name){
		super(name);
		super.setHp(super.getHp() * 0.2);
		super.setMp(super.getMp() * 0.7);
		super.setPp(super.getPp() * 0.1);
	}
}
