package ce1002.a6.s101201521;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{
	private JLabel jlblState;
	private JLabel jlblGraph;
	public MyPanel(){
		setLayout(null);
	}
	//set panel with two JLabel
	public void setRoleState(Hero hero){
		jlblState = new JLabel(hero.getName() + 
								" HP: " + hero.getHp() + 
								" MP: " + hero.getMp() +
								" PP: " + hero.getPp());
		jlblGraph = new JLabel(new ImageIcon(hero.getName() + ".jpg"));
		add(jlblState);
		add(jlblGraph);
		//set jlabels position and size
		jlblState.setBounds(5, 5, 300, 10);
		jlblGraph.setBounds(-20, -35, 300, 300);
	}
}
