package ce1002.a6.s101201521;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
public class MyFrame extends JFrame{
	public MyFrame(){
		setLayout(null);
		//declare panels
		MyPanel pWizard = new MyPanel();
		MyPanel pSwordsman = new MyPanel();
		MyPanel pKnight = new MyPanel();
		//declare heros
		Wizard wizard = new Wizard("Wizard");
		Swordsman swordsman = new Swordsman("Swordsman");
		Knight knight = new Knight("Knight");
		//handling panels
		newRolePos(wizard, pWizard, 5, 5);
		newRolePos(swordsman, pSwordsman, 5, 225);
		newRolePos(knight, pKnight, 5, 445);
	}
	public void newRolePos(Hero hero, MyPanel panel, int x, int y){
		panel.setRoleState(hero);
		add(panel);
		//set panels position and size
		panel.setBounds(x, y , 260, 215);
		panel.setBorder(new LineBorder(Color.BLACK, 2));
	}
}
