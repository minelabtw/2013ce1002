package ce1002.a6.s101201521;

public class Knight extends Hero{
	//create knight with hero's name and initialize status
	public Knight(String name){
		super(name);
		super.setHp(super.getHp() * 0.8);
		super.setMp(super.getMp() * 0.1);
		super.setPp(super.getPp() * 0.1);
	}
}
