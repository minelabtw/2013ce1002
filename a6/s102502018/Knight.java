package ce1002.a6.s102502018;

public class Knight extends Hero {
	public Knight(float hp,float mp,float pp)
	{
		super("Knight",hp*0.8f,mp*0.1f,pp*0.1f);
		knight();
	}
	public void knight()
	{
		System.out.println(getname()+" HP: "+gethp()+" MP: "+getmp()+" PP: "+getpp());
	}
}
