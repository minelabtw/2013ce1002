package ce1002.a6.s102502018;

public class Wizard extends Hero {
	public Wizard(float hp,float mp,float pp)
	{
		super("Wizard",hp*0.2f,mp*0.7f,pp*0.1f);
		wizard();
	}
	public void wizard()
	{
		System.out.println(getname()+" HP: "+gethp()+" MP: "+getmp()+" PP: "+getpp());
	}
}
