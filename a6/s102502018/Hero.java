package ce1002.a6.s102502018;

public class Hero {
	private String name;
	private float hp;
	private float mp;
	private float pp;

	public Hero()
	{
		
	}
	public Hero(String n,float hp,float mp,float pp)
	{
		setname(n);
		sethp(hp);
		setmp(mp);
		setpp(pp);
	}
	public void setname(String n)
	{
		this.name = n;
	}
	public void sethp(float hp)
	{
		this.hp = hp;
	}
	public void setmp(float mp)
	{
		this.mp = mp;
	}
	public void setpp(float pp)
	{
		this.pp = pp;
	}
	public String getname()
	{
		return name;
	}
	public float gethp()
	{
		return hp;
	}
	public float getmp()
	{
		return mp;
	}
	public float getpp()
	{
		return pp;
	}

}
