package ce1002.a6.s102502018;

public class Swordsman extends Hero {
	public Swordsman(float hp,float mp,float pp)
	{
		super("Swordsman",hp*0.1f,mp*0.1f,pp*0.8f);
		swordsman();
	}
	public void swordsman()
	{
		System.out.println(getname()+" HP: "+gethp()+" MP: "+getmp()+" PP: "+getpp());
	}
}
