package ce1002.a6.s102502546;
import javax.swing.ImageIcon;
public class Wizard extends Hero {
	Wizard() {
		setName("Wizard");
	}// 建構子鎖死自己的Name
		// 下面即是多型 讓每項回傳屬於自己的加權數值

	public double getHP() {
		return HP * 0.2;
	}

	public double getMP() {
		return MP * 0.7;
	}

	public double getPP() {
		return PP * 0.1;
	}
	public ImageIcon getImage(){
		ImageIcon image = new ImageIcon("Wizard.jpg");
		return image;
	}
}
