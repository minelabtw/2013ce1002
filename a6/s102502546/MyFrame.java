package ce1002.a6.s102502546;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame{
	public MyFrame()
	{
		Hero hero[] = new Hero[3];//開始建立英雄物件
		MyPanel mypanel[] = new MyPanel[3];//建立面板物件
		
		setLayout(null);//排版
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
 		for(int i = 0;i < 3;i++)
		{
			mypanel[i] = new MyPanel();
	
			newRolePos(hero[i],mypanel[i],0,i * 220);//把對應參數與圖片放入面板
			add(mypanel[i]);//把面板放入方框
		}
	}
	
	public void newRolePos(Hero hero,MyPanel panel,int x,int y)
	{
		panel.setLocation(x, y);//面板定位
		panel.setRoleState(hero);//英雄屬性
	}

}
