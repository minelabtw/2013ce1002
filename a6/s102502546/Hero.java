package ce1002.a6.s102502546;
import javax.swing.ImageIcon;
public class Hero {
	private String name;// protected 型態 可以讓繼承的使用
	protected double HP = 30;
	protected double MP = 30;
	protected double PP = 30;
	protected ImageIcon image;
//set+get Name HP MP PP 
	public void setName(String name) {
		this.name = name;
	}

	public void setHP(double HP) {
		this.HP = 30.0;
	}

	public void setMP(double MP) {
		this.MP = 30.0;
	}

	public void setPP(double PP) {
		this.PP = 30.0;
	}

	public String getName() {
		return name;
	}

	public double getHP() {
		return HP;
	}

	public double getMP() {
		return MP;
	}

	public double getPP() {
		return PP;
	}
	public ImageIcon getImage(){
		return image;
	}

}
