package ce1002.a6.s102502546;
import javax.swing.ImageIcon;
public class Swordsman extends Hero {
	Swordsman() {
		setName("Swordsman");
	}// 建構子鎖死自己的Name
		// 下面即是多型 讓每項回傳屬於自己的加權數值

	public double getHP() {
		return HP * 0.1;
	}

	public double getMP() {
		return MP * 0.1;
	}

	public double getPP() {
		return PP * 0.8;
	}
	public ImageIcon getImage(){
		ImageIcon image = new ImageIcon("Swordsman.jpg");
		return image;
	}

}
