package ce1002.a6.s102502546;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private String name ;//名稱
	private double hp = 30;//血量
	private double mp = 30;//魔力
	private double pp = 30;//能力
	private ImageIcon image;//圖片
	
	public MyPanel()
	{
		setSize(250,220);//面板大小
	}
	
	public void setRoleState(Hero hero)
	{
		name = hero.getName() ;
		hp = hero.getHP() ;
		mp = hero.getMP() ;
		pp = hero.getPP() ;
		image = hero.getImage();
		
		JLabel labela = new JLabel(name + " HP: " + hp + " MP: " + mp + " PP: " + pp) ;
		labela.setSize(250,30);//標籤大小
	    labela.setLocation(0, 0);//標籤位置
		add(labela);//將標籤加入面板
		JLabel labelb = new JLabel(image) ;
		labelb.setSize(250,190);
		labelb.setLocation(0,30);
		add(labelb);
		
	}
}