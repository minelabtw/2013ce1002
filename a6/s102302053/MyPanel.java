package ce1002.a6.s102302053;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends  JPanel{
	JLabel[] label = new JLabel[2];
	MyPanel(){
	}
	public void setRoleState(Hero hero){
		String n = hero.getPic();
		ImageIcon image = new ImageIcon(n);
		label[0] = new JLabel(hero.getHeroName() + " HP: " + hero.getHp() +" MP: " + hero.getMp() + " PP: " + hero.getPp());
		label[1] = new JLabel(image, JLabel.CENTER);
		label[0].setHorizontalTextPosition(JLabel.CENTER);
		label[0].setVerticalTextPosition(JLabel.TOP);
		this.add(label[0]);
		this.add(label[1]);

	}

}
