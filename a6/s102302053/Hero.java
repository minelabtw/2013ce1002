package ce1002.a6.s102302053;

public class Hero {
	protected String heroName;
	protected String heroPic;
	protected double hp;
	protected double mp;
	protected double pp;
	public void setHeroName(String name){//設置英雄名稱
		heroName = name;
	}
	public void setHp(){//設置HP
		hp = 30.0;
	}
	public void setMp(){//設置MP
		mp = 30.0;
	}
	public void setPp(){//設置PP
		pp = 30.0;
	}
	public String getHeroName(){//回傳英雄名稱
		return heroName;
	}
	public double getHp(){//回傳HP
		return hp;
	}
	public double getMp(){//回傳MP
		return mp;		
	}
	public double getPp(){//回傳PP
		return pp;
	}
	public void setPic(String name){
		heroPic = name;
	}
	public String getPic(){
		return heroPic;
	}
	
	
}
