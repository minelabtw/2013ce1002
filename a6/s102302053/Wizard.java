package ce1002.a6.s102302053;

public class Wizard extends Hero {
	Wizard(){
		setHeroName("Wizard");
		setHp();
		setMp();
		setPp();
		setPic("Wizard.jpg");
	}
	public double getHp(){//回傳加權後的HP
		return hp*0.2;
	}
	public double getMp(){//回傳加權後的MP
		return mp*0.7;
	}
	public double getPp(){//回傳加權後的PP
		return pp*0.1;
	}


}
