package ce1002.a6.s102502002;

public class Swordsman extends Hero{
	
	public Swordsman()
	{
		super.setName("Swordsman");
	}
	public double getHp()
	{
		return super.hp*0.1;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.8;
	}
}
