package ce1002.a6.s102502002;

public class Hero{
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	
	public Hero()
	{
		setHp(30);
		setMp(30);
		setPp(30);
	}
	
	public void setName(String Name)
	{
		this.name=Name;
	}
	public String getName()
	{
		return this.name;
	}
	public double getHp()
	{
		return this.hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return this.pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
}