package ce1002.a6.s102502002;

public class Knight extends Hero {
	
	public Knight()
	{	
		super.setName("Knight");
	}
	public double getHp()
	{
		return super.hp*0.8;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}
