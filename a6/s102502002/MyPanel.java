package ce1002.a6.s102502002;
import javax.swing.*;

public class MyPanel extends JPanel {
	
	public void setRoleState(Hero hero){
		setVisible(true);
		setLayout(null);
        Hero[] heros = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		ImageIcon k = new ImageIcon("Knight.jpg");
		ImageIcon s = new ImageIcon("Swordsman.jpg");
		ImageIcon w = new ImageIcon("Wizard.jpg");
		add(new JLabel(w));
		add(new JLabel(s));
		add(new JLabel(k));
		for(int i=0;i<3;i++){
		add(new JLabel(hero.getName()+" HP:"+hero.getHp()+" MP:"+hero.getMp()+" PP:"+hero.getPp()));
		}	
	}
}
