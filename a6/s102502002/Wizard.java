package ce1002.a6.s102502002;

public class Wizard  extends Hero{
	 
	public Wizard()
	{
		super.setName("Wizard");
	}
	public double getHp()
	{
		return super.hp*0.2;
	}
	public double getMp()
	{
		return super.mp*0.7;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}
