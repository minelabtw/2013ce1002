package ce1002.a6.s102502502;

public class Knight extends Hero {

	public Knight() {
		super();              // the constructor of Knight from Hero
		setHeroName("Knight");
		setHP(30.0*0.8);
		setMP(30.0*0.1);
		setPP(30.0*0.1);
	    getHP();
		getMP();
		getPP();
	}
}
