package ce1002.a6.s102502502;

public class Hero {
	private String HeroName;
	private double HP = 30.0;          //Initialize the three points = 30.0
	private double MP = 30.0;
	private double PP = 30.0;

	public void setHeroName(String heroName) {
		HeroName = heroName;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	public String getHeroName() {
		return HeroName;
	}
	public double getHP() {
		return HP;
	}
	public double getMP() {
		return MP;
	}
	public double getPP() {
		return PP;
	}
}


