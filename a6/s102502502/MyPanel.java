package ce1002.a6.s102502502;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel {
	JLabel jlb1,jlb2;
  Hero hero = new Hero();
  JPanel jpanel = new JPanel();
  public MyPanel(Hero hero) {
	 setRoleState(hero);
  }
  public void setRoleState(Hero hero) {
	  String a = hero.getHeroName();
	  double b = hero.getHP();
	  double c = hero.getMP();
	  double d = hero.getPP();
	  String e = ((Object) jpanel).getPic();
	  ImageIcon icon = new ImageIcon(e);
	 jlb1 = new JLabel(icon, JLabel.CENTER);
	 jlb2 = new JLabel(a +" HP:" + b + " MP:" + c + " PP:" + d, JLabel.CENTER);
	 jlb2.setVerticalTextPosition(JLabel.TOP);
	 jlb2.setHorizontalTextPosition(JLabel.CENTER);
	  this.add(jlb2);
	  this.add(jlb1);
  }
}