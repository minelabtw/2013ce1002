package ce1002.a6.s102502502;
import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame  {
  public MyFrame() {

	  Wizard w = new Wizard();
	  Swordsman s = new Swordsman();
	  Knight k = new Knight();
 
	  //JPanel panel = new JPanel();
	  MyPanel panel1 = new MyPanel(w);
	  MyPanel panel2 = new MyPanel(s);
	  MyPanel panel3 = new MyPanel(k);
	  this.setLayout(new GridLayout(3, 1));

	  newRolePos(w, panel1, 0, 0);
	  newRolePos(s, panel2, 0, 500);
	  newRolePos(k, panel3, 0, 1000); 

  }
 
  public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
	  setLayout(null);
	  setBounds(x, y, 300, 450);
	  this.add(panel);  
  } 
}
