package ce1002.a6.s102502559;

public class Hero {
	public String hero_Name;
	private double hero_HP;
	private double hero_MP;
	private double hero_PP;

	public Hero() //建構子,初始化HP,MP,PP的初始值為30
	{
		this.hero_HP = 30;
		this.hero_MP = 30;
		this.hero_PP = 30;
	}
	//get變數回傳屬性值
	public double getHP()
	{
		return this.hero_HP;
	}
	public double getMP()
	{
		return this.hero_MP;
	}
	public double getPP()
	{
		return this.hero_PP;
	}
	public String getName()
	{
		return hero_Name;
	}
}
