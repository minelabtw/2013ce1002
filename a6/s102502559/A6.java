package ce1002.a6.s102502559;

import ce1002.a6.s102502559.Hero;
import ce1002.a6.s102502559.Knight;
import ce1002.a6.s102502559.Swordsman;
import ce1002.a6.s102502559.Wizard;
import javax.swing.JFrame;
public class A6 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3]; //宣告型別為Hero的陣列,陣列長度為3
		//宣告陣列的三項分別為Knight,Swordsman,Wizard類別的物件
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		MyPanel[] panel = new MyPanel[3];
		MyFrame frame = new MyFrame();
		frame.setSize(300,900);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		for(int i = 0;i<3;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i],panel[i],10,10+240*i);
		}
		frame.setVisible(true);
	}

}
