package ce1002.a6.s102502559;

public class Swordsman extends Hero{
	public Swordsman()
	{
		this.setName("Swordsman");
	}
	public void setName(String Name)
	{
		this.hero_Name = Name;
	}
	public double getHP()
	{
		return super.getHP() * 0.1;
	}
	public double getMP()
	{
		return super.getMP() * 0.1;
	}
	public double getPP()
	{
		return super.getPP() * 0.8;
	}
}
