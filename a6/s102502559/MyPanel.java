package ce1002.a6.s102502559;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MyPanel extends JPanel{
	ImageIcon icon;
	JLabel info;
	JLabel image;
	public MyPanel()
	{
		this.setSize(260,240);
		LineBorder LB = new LineBorder(Color.black,5);
		this.setBorder(LB);
	}
	public void setRoleState(Hero hero)
	{
		icon = new ImageIcon(hero.getName()+".jpg");
		info = new JLabel(hero.getName() + " HP:" + hero.getHP() +" MP:" + hero.getMP() + " PP:"+ hero.getPP());
		image = new JLabel(icon);
		this.add(info);
		this.add(image);
	}

}
