package ce1002.a6.s102502559;

public class Knight extends Hero{
	public Knight()
	{
		this.setName("Knight");
	}
	public void setName(String Name)
	{
		this.hero_Name = Name;
	}
	public double getHP()
	{
		return super.getHP() * 0.8; //繼承父屬性的getHP函式*0.8後回傳
	}
	public double getMP()
	{
		return super.getMP() * 0.1;
	}
	public double getPP()
	{
		return super.getPP() * 0.1;
	}
}
