package ce1002.a6.s102502559;

import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame{
	public MyFrame()
	{
		this.setLayout(null);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		this.add(panel);
		panel.setRoleState(hero);
		panel.setLocation(x,y);
	}
}
