package ce1002.a6.s102502031;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.FlowLayout;

public class MyPanel extends JPanel {
	private JLabel property;
	private JLabel figure;

	public MyPanel() {
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
	}

	public void setRoleState(Hero hero) {
		// set figure
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg");
		this.figure = new JLabel(icon);

		// set property
		this.property = new JLabel(getHeroProperty(hero));

		// add to panel
		this.add(property);
		this.add(figure);
	}

	private String getHeroProperty(Hero hero) { // get the string in label property
		String a = new String();
		a = hero.getName() + " ";
		a = a + "HP: " + hero.getHP() + " ";
		a = a + "MP: " + hero.getMP() + " ";
		a = a + "PP: " + hero.getPP();
		return a;
	}
}
