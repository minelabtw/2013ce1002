package ce1002.a6.s102502031;

import javax.swing.JFrame;
import java.awt.GridLayout;

public class MyFrame extends JFrame {
	private MyPanel knight = new MyPanel();
	private MyPanel swordsman = new MyPanel();
	private MyPanel wizard = new MyPanel();

	private Hero a = new Knight();
	private Hero b = new Swordsman();
	private Hero c = new Wizard();

	public MyFrame() {
		this.setLayout(new GridLayout(3, 1)); // use gridLayout
		this.newRolePos(c, wizard, 0, 0);
		this.newRolePos(b, swordsman, 0, 0);
		this.newRolePos(a, knight, 0, 0);
	}

	private void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setRoleState(hero); // get property and figure
		this.add(panel); // add panel to frame
	}
}
