package ce1002.a6.s102502031;

public class Hero {
	private String name;
	private double HP;
	private double MP;
	private double PP;

	public Hero() { // default constructor gives value 30 to HP, MP, and PP
		setHP(30);
		setMP(30);
		setPP(30);
		setName("Hero");
	}

	public Hero(String name) { // constructor with string will also gives name
		this(); // give value 30 to HP, MP, and PP
		setName(name);
	}

	public Hero(double HP, double MP, double PP) { // constructor with three value will gives to HP, MP, and PP
		setHP(HP);
		setMP(MP);
		setPP(PP);
		setName("Hero");
	}

	public Hero(String name, double HP, double MP, double PP) {
		this(HP, MP, PP);
		setName(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setHP(double HP) {
		this.HP = HP;
	}

	public void setMP(double MP) {
		this.MP = MP;
	}

	public void setPP(double PP) {
		this.PP = PP;
	}

	public String getName() {
		return name;
	}

	public double getHP() {
		return HP;
	}

	public double getMP() {
		return MP;
	}

	public double getPP() {
		return PP;
	}
}
