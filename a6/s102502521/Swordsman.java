package ce1002.a6.s102502521;

public class Swordsman extends Hero{
	public Swordsman(){
		super.setHeroName("Swordsman");
	}
	
	public float getHP(){
		return super.getHP()*0.1F;
	}
	
	public float getMP(){
		return super.getMP()*0.1F;
	}
	
	public float getPP(){
		return super.getPP()*0.8F;
	}
}
