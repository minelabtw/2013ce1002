package ce1002.a6.s102502521;

public class Knight extends Hero{
	public Knight(){
		super.setHeroName("Knight");
	}
	
	public float getHP(){
		return super.getHP()*0.8F;
	}
	
	public float getMP(){
		return super.getMP()*0.1F;
	}
	
	public float getPP(){
		return super.getPP()*0.1F;
	}
}
