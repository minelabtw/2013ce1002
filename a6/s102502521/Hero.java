package ce1002.a6.s102502521;

public class Hero {
	private String heroName;
	private float HP=30.0F;
	private float MP=30.0F;
	private float PP=30.0F;
	
	public void setHeroName(String heroName){
		this.heroName=heroName;    //設定職業
	}
	
	public String getHeroName(){
		return heroName;                 //回傳職業
	}
	
	public void setHP(float HP){
		this.HP=HP;                          //設定HP
	}
	
	public float getHP(){
		return HP;                            //回傳HP       
	}
	
	public void setMP(float MP){
		this.MP=MP;                        //設定MP   
	}
	
	public float getMP(){
		return MP;                           //回傳MP
	}
	
	public void setPP(float PP){
		this.PP=PP;                          //設定PP
	}
	
	public float getPP(){
		return PP;                            //回傳PP
	}
}
