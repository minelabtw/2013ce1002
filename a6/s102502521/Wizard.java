package ce1002.a6.s102502521;

public class Wizard extends Hero{
	public Wizard(){
		super.setHeroName("Wizard");
	}
	
	public float getHP(){
		return super.getHP()*0.2F;
	}
	
	public float getMP(){
		return super.getMP()*0.7F;
	}
	
	public float getPP(){
		return super.getPP()*0.1F;
	}
}
