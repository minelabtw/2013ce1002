package ce1002.a6.s101201046;

public class Knight extends Hero {
	public Knight () { //construct Knight class
		Name = "Knight";
	}
	
	public float HP_getter() { //set HP
		return HP * 0.8f;
	}
	
	public float MP_getter() { //set MP
		return MP * 0.1f;
	}
	
	public float PP_getter() { //set PP
		return PP * 0.1f;
	}
}
