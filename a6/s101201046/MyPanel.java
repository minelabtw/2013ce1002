package ce1002.a6.s101201046;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;

public class MyPanel extends JPanel {
	private JLabel states;
	private JLabel img;
	
	public MyPanel() {
		setBorder(new LineBorder(Color.BLACK, 2));
	}
	
	public void setRoleState(Hero hero) {
		ImageIcon _img = new ImageIcon("/home/firejox/image/" + hero.Name_getter() + ".jpg"); //get the image
		states = new JLabel(hero.Name_getter() + " HP: " + hero.HP_getter() + " MP: " + hero.MP_getter() + "PP: " + hero.PP_getter()); //set state
		img = new JLabel(_img); //set the image
		add(states); //put the information of state into panel
		add(img); //put the image into panel
	}
	
}
