package ce1002.a6.s101201046;


public class A6 {

	public static void main(String[] args) {
		Hero[] heros;
		MyPanel[] panels = new MyPanel[3];
		MyFrame frame = new MyFrame();
		heros = new Hero[3];
		
		
		heros[0] = new Wizard(); //set Wizard
		((Wizard)heros[0]).HP_setter(30);
		((Wizard)heros[0]).MP_setter(30);
		((Wizard)heros[0]).PP_setter(30);
		panels[0] = new MyPanel();
		frame.newRolePos(heros[0], panels[0], 250, 250);
		
		heros[1] = new Swordsman(); //set Swordsman
		((Swordsman)heros[1]).HP_setter(30);
		((Swordsman)heros[1]).MP_setter(30);
		((Swordsman)heros[1]).PP_setter(30);
		panels[1] = new MyPanel();
		frame.newRolePos(heros[1], panels[1], 250, 250);
		
		heros[2] = new Knight(); //set Knight
		((Knight)heros[2]).HP_setter(30);
		((Knight)heros[2]).MP_setter(30);
		((Knight)heros[2]).PP_setter(30);
		panels[2] = new MyPanel();
		frame.newRolePos(heros[2], panels[2], 250, 250);

	}

}
