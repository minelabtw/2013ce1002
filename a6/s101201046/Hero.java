package ce1002.a6.s101201046;

public class Hero {
	protected String Name; //Hero's name
	protected float HP; //Hero's health point
	protected float MP; //Hero's mana point
	protected float PP; //Hero's power point
	
	public void Name_setter(String _Name) { //set name
		Name = _Name;
	}
	
	public void HP_setter(float hpp) { //set health point
		HP = hpp;
	}
	
	public void MP_setter(float mpp) { //set mana point
		MP = mpp;
	}
	
	public void PP_setter(float ppp) { //set power point
		PP = ppp;
	}
	
	public String Name_getter() { //get name
		return Name;
	}
	
	public float HP_getter() { //get health point
		return HP;
	}
	
	public float MP_getter() { //get mana point
		return MP;
	}
	
	public float PP_getter() { //get power point
		return PP;
	}
	
}
