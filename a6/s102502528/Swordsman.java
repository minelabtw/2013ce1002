package ce1002.a6.s102502528;

public class Swordsman extends Hero {
	//override
	Swordsman() {
		name = "Swordsman";
	}

	public float getHP() {
		return super.getHP() * 0.1f;
	}

	public float getMP() {
		return super.getMP() * 0.1f;
	}

	public float getPP() {
		return super.getPP() * 0.8f;
	}
}
