package ce1002.a6.s102502528;

import java.awt.Frame;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	Hero heros[] = new Hero[3];

	MyFrame() {
		heros[0] = new Wizard();        //new heros
		MyPanel panel = new MyPanel(heros[0], 5, 5, 270, 230);  
		this.add(panel);          //add them to frame
		heros[1] = new Swordsman();
		panel = new MyPanel(heros[1], 5, 240, 270, 230);
		this.add(panel);
		heros[2] = new Knight();
		panel = new MyPanel(heros[2], 5, 475, 270, 230);
		this.add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
		setSize(300, 760);          //set size
		setLayout(null);            //set layout
		setLocationRelativeTo(null);//set location
		setVisible(true);           //set visible
	}

}
