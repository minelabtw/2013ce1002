package ce1002.a6.s102502011;

public class Hero {
	private float hp ;
    private float mp ;
    private float pp ;
    private String name ;
    
    Hero() {
    	hp = 30 ;
    	mp = 30 ;
    	pp = 30 ;
    	name = "" ;
    }
    
    public void setHP(float hp ) { //setter
    	this.hp = hp ;
    }
    
    public void setMP(float mp ) {
    	this.mp = mp ;
    }
    
    public void setPP(float pp) {
    	this.pp = pp ;
    }
    
    public void setName(String name) {
    	this.name = name ;
    }
    
    public float getHP() { //getter
    	return hp ;
    }
    
    public float getMP() {
    	return mp ;
    }
    
    public float getPP() {
    	return pp ;
    }
    
    public String getName() {
    	return name ;
    }
}
