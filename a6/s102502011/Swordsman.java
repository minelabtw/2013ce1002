package ce1002.a6.s102502011;

public class Swordsman extends Hero {
	
	public float getHP() {
		return super.getHP() * (float)0.1 ;
	}
	
	public float getMP() {
		return super.getMP() * (float)0.1 ;
	}
	
	public float getPP() {
		return super.getPP() * (float)0.8 ;
	}
	
	public String getName() {
    	return "Swordsman" ;
    }

}