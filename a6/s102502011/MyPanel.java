package ce1002.a6.s102502011;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private JLabel status;
	private JLabel image;
	
	public void setRoleState(Hero hero){
		status = new JLabel(hero.getName() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		image = new JLabel(new ImageIcon(hero.getName() + ".jpg"));
		
		// configure display effect
		status.setSize(220, 20);
		image.setSize(220,230);
		status.setLocation(5, 0);
		image.setLocation(5, 0);
	}
	
	public JLabel getStatus(){
		return status;
	}
	
	public JLabel getImage(){
		return image;
	}
}