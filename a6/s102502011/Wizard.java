package ce1002.a6.s102502011;

public class Wizard extends Hero {
	
	public float getHP() {
		return super.getHP() * (float)0.2 ;
	}
	
	public float getMP() {
		return super.getMP() * (float)0.7 ;
	}
	
	public float getPP() {
		return super.getPP() * (float)0.1 ;
	}
	
	public String getName() {
    	return "Wizard" ;
    }

}