package ce1002.a6.s102502005;

public class Swordsman extends Hero{

	Swordsman() {
		this.setHeroname("Swordsman");
		this.setIconPath("Swordsman.jpg");
	}
	
	String getIconPath(){
		return "Swordsman.jpg";
	}
	
	double getHP()  //getter
	{
		return this.HP * 0.1;
	}
	
	double getMP()
	{
		return this.MP * 0.1;
	}
	
	double getPP()
	{
		return this.PP * 0.8;
	}
}
