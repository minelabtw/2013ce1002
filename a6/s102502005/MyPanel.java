package ce1002.a6.s102502005;

import ce1002.a6.s102502005.Hero;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	
	String IconPath;
	String HeroStatus;
	
	MyPanel() {
	}
	
	public void setRoleState(Hero hero){
		
		this.IconPath = hero.getIconPath();
		this.HeroStatus = hero.getHeroname() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP();
		
		ImageIcon HeroIcon = new ImageIcon(IconPath); 
		JLabel Status = new JLabel(HeroStatus);
		JLabel Icon = new JLabel(HeroIcon);
		
		this.setLayout(null);
		Status.setBounds(0,0,250,15);
		Icon.setBounds(1,16,250,190);
		
		this.add(Status);
		this.add(Icon);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}

}
