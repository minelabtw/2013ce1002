package ce1002.a6.s102502005;

import javax.swing.JFrame;

public class A6 {

	public static void main(String[] args) {
	
		Wizard wizard = new Wizard();
		Swordsman swordsman = new Swordsman();
		Knight knight = new Knight();
		
		MyFrame frame = new MyFrame();
		frame.setLayout(null);
		frame.newRolePos(wizard,frame.WizardPanel,15,15);
		frame.newRolePos(swordsman,frame.SwordsManPanel,15,235);
		frame.newRolePos(knight,frame.KnightPanel,15,455);
		
		frame.add(frame.WizardPanel);
		frame.add(frame.SwordsManPanel);
		frame.add(frame.KnightPanel);
		
		frame.setSize(297, 715);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
