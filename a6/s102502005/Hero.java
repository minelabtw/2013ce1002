package ce1002.a6.s102502005;

public class Hero {

	public String Heroname;
	public String IconPath;
	public double HP=30.0;
	public double MP=30.0;
	public double PP=30.0;
	
	String getHeroname(){ //getter
		return this.Heroname;
	}
	
	String getIconPath(){
		return this.IconPath;
	}
	
	double getHP()
	{
		return this.HP;
	}
	
	double getMP()
	{
		return this.MP;
	}
	
	double getPP()
	{
		return this.PP;
	}
	
	void setHeroname(String name){ //setter
		this.Heroname = name;
	}

	void setIconPath(String path){ 
		this.IconPath = path;
	}
	
	void setHP(double HP){
		this.HP = HP ;
	}
	
	void setMP(double MP){
		this.MP = MP;
	}
	
	void setPP(double PP){
		this.PP = PP;
	}
}
