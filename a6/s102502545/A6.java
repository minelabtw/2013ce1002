package ce1002.a6.s102502545;

import javax.swing.*;
import java.awt.*;

public class A6 {

	public static void main(String[] args) {
		

		MyFrame frame = new MyFrame();//宣告
		Hero knight = new Knight();
		Hero swordsman = new Swordsman();
		Hero wizard = new Wizard();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉視窗後結束程式
		frame.setLayout(null);

		frame.newRolePos(wizard,frame.A,250,200);
		frame.A.setLocation(15,15);
		frame.newRolePos(swordsman, frame.B,250,200);
		frame.B.setLocation(15, 225);
		frame.newRolePos(knight,frame.C,250,200);
		frame.C.setLocation(15, 435);
		
		frame.setSize(295,690);//利用frame設定外框大小
		frame.setVisible(true);
	}
}
