package ce1002.a6.s102502545;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	JLabel jlb1= new JLabel();
	JLabel jlb2 = new JLabel();

	public void setRoleState(Hero hero) {
		
		setLayout(new BorderLayout()); //�ƪ�
		setBorder(BorderFactory.createLineBorder( Color.black, 4) ); //�®�
		
		jlb1.setText(hero.getName() + " HP:" + hero.getHP() + " MP:"
				+ hero.getMP() + " PP:" + hero.getPP());
		add(jlb1, BorderLayout.NORTH);
		
		jlb2.setIcon(new ImageIcon(hero.getPicture()));
		add(jlb2, BorderLayout.CENTER);
		
	}
}
