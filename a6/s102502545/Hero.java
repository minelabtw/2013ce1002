package ce1002.a6.s102502545;

import java.util.Scanner;

public class Hero {

	protected String name;
	protected double HP=30;//宣告變數
	protected double MP=30;//宣告變數
	protected double PP=30;//宣告變數
	
	public Hero(){
		
	}
	
	public String getName() {
		return name;//回傳
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHP() {
		return HP;//回傳
	}
	public void setHP() {
		this.HP = 30.0;
	}
	public double getMP() {
		return MP;//回傳
	}
	public void setMP() {
		this.MP = 30.0;
	}
	public double getPP() {
		return PP;
	}
	public void setPP() {
		this.PP = 30.0;
	}
	String getPicture()
	{
		return this.name+".jpg";
	}
}
