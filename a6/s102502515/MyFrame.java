package ce1002.a6.s102502515;
import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;

public class MyFrame extends JFrame {

	MyPanel WIpanel = new MyPanel();//宣告Wizard的面板
	MyPanel SWpanel = new MyPanel();//宣告Swordsman的面板
	MyPanel KNpanel = new MyPanel();//宣告Knight的面板
	
	public MyFrame()
	{
		setSize(280,700);//設定frame的大小
		setLayout(new GridLayout(3,1,10,10));//frame裡面有幾格，以及間距
		Hero hero[] = new Hero[3];//宣告Hero物件陣列
		
		hero[0] = new Wizard();//將陣列第一項改為Wizard物件
		hero[1] = new Swordsman();//將陣列第二項改為Swordsman物件
		hero[2] = new Knight();//將陣列第三項改為Knight物件
	
		newRolePos(hero[0], WIpanel, 0, 0);//傳入到newRolePos function
		newRolePos(hero[1], SWpanel, 0, 250);
		newRolePos(hero[2], KNpanel, 0, 450);
		add(WIpanel);//加WIpanel
		add(SWpanel);
		add(KNpanel);
		
	}

	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		
		panel.setLocation(x, y);//起始位置
		panel.setRoleState(hero);
		panel.setBorder(new LineBorder(Color.BLACK, 5));//邊框
		}	
	}

