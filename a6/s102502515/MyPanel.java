package ce1002.a6.s102502515;
import javax.swing.*;
public class MyPanel extends JPanel {
	public MyPanel(){

	}
	
	public void setRoleState(Hero hero){
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");//建立圖片
		JLabel lbl = new JLabel(hero.getName() + " HP: " //增加一文字標籤
				+ hero.getHP() + " MP: " + hero.getMP() +
				" PP: " + hero.getPP());
		add(lbl);
		add(new JLabel(image));
		
	}
}
