package ce1002.a6.s102502515;

public class Wizard extends Hero {//Wizard的子類別繼承Hero
	public Wizard(){
		super.setName("Wizard");
	}
	public String getName() {//回傳Wizard
		return name;
	}
	public void setName(String name) {//傳入名稱
		this.name = name;
	}
	public double getHP() {//推翻父類別，回傳
		return HP*0.2;
	}
	public void setHP(double hP) {//初始值傳入
		HP = hP;
	}
	public double getMP() {//推翻父類別，回傳
		return MP*0.7;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public double getPP() {//推翻父類別，回傳
		return PP*0.1;
	}
	public void setPP(double pP) {
		PP = pP;
	}
}
