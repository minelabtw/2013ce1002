package ce1002.a6.s102502515;

public class Hero {//父類別，以下的setter和getter會被推翻
	protected String name;//protected使其他子類別亦可使用
	protected double HP;
	protected double MP;
	protected double PP;
	public Hero(){
		setHP(30.0);
		setMP(30.0);
		setPP(30.0);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHP() {
		return HP;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public double getMP() {
		return MP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public double getPP() {
		return PP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	
	

}
