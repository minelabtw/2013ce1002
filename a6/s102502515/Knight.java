package ce1002.a6.s102502515;

public class Knight extends Hero {//Knight子類別繼承Hero
	public Knight(){
		super.setName("Knight");
	}
	public String getName() {//推翻父類別，回傳
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHP() {//推翻父類別，回傳
		return HP*0.8;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public double getMP() {//推翻父類別，回傳
		return MP*0.1;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public double getPP() {//推翻父類別，回傳
		return PP*0.1;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	
	
}
