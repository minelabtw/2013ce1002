package ce1002.a6.s102502519;

import ce1002.a6.s102502519.Hero;
import ce1002.a6.s102502519.Knight;
import ce1002.a6.s102502519.Swordsman;
import ce1002.a6.s102502519.Wizard;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Hero hero[] = new Hero[3];    //宣告建構子
		
		double HP= 0;
		double MP= 0;
		double PP= 0;
		
			Wizard wizard = new Wizard();
			HP=(double) (wizard.getHp() * 0.2);
			wizard.setHp(HP);
			MP=(double) (wizard.getMp() * 0.7);
			wizard.setMp(MP);
			PP=(double) (wizard.getPp() * 0.1);
			wizard.setPp(PP);
			hero[0] = wizard;    //將值存入陣列裡
		   System.out.print(wizard.getName() + " ")	;
		   System.out.print("HP: " + wizard.getHp()+ " ")	;
		   System.out.print("MP: " + wizard.getMp()+ " ")	;
		   System.out.println("PP: " + wizard.getPp()+ " ")	;

			Swordsman swordsman = new Swordsman();
			HP=(double) (swordsman.getHp() * 0.1);
			swordsman.setHp(HP);
			MP=(double) (swordsman.getMp() * 0.1);
			swordsman.setMp(MP);
			PP=(double) (swordsman.getPp() * 0.8);
			swordsman.setPp(PP);
			hero[1] = swordsman;
		   System.out.print(swordsman.getName() + " ")	;
		   System.out.print("HP: " + swordsman.getHp()+ " ")	;
		   System.out.print("MP: " + swordsman.getMp()+ " ")	;
		   System.out.println("PP: " + swordsman.getPp()+ " ")	;

			Knight knight = new Knight();
			HP=(double) (knight.getHp() * 0.8);
			knight.setHp(HP);
			MP=(double) (knight.getMp() * 0.1);
			knight.setMp(MP);
			PP=(double) (knight.getPp() * 0.1);
			knight.setPp(PP);
			hero[2] = knight;
		   System.out.print(knight.getName() + " ")	;
		   System.out.print("HP: " + knight.getHp()+ " ")	;
		   System.out.print("MP: " + knight.getMp()+ " ")	;
		   System.out.println("PP: " + knight.getPp()+ " ")	;
		   
		   MyFrame f = new MyFrame();
		   
	}

}
