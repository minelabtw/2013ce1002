package ce1002.a6.s102502526;

public class Knight extends Hero{  //複製A5
	@Override           //覆寫Hero class裡HP函式
	public float getHP(){
		return super.getHP() * 0.8f;  //f是給0.8一個float值
	}
	@Override
	public float getMP(){
		return super.getMP() * 0.1f;
	}
	@Override
	public float getPP(){
		return super.getPP() * 0.1f;
	}
}
