package ce1002.a6.s102502526;
import java.util.Scanner;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner a = new Scanner(System.in);
        MyFrame frame = new MyFrame();
        MyPanel[] panel = new MyPanel[3];

		Hero[] hero = new Hero[3];
		hero[0] = new Wizzard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		hero[0].setHeroname("Wizzard");
		hero[1].setHeroname("Swordsman");
		hero[2].setHeroname("Knight");
		
		for(int i = 0 ; i<3 ; i++){
			System.out.println(hero[i].getHeroname()+hero[i].getHP()+" MP: "+hero[i].getMP()+" PP: "+hero[i].getPP());
		}
		
		for (int i=0;i<3;i++){
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i], panel[i], 10, 10 + 240 * i);
		}
		frame.setVisible(true);
	}

}
