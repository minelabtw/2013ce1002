package ce1002.a6.s995002014;

public class Swordsman extends Hero {
	
	Swordsman() {
		setName("Swordsman");
	}
	
	//Override
	public float getHp() {
		return (float) (this.hp*0.1);
	}
	public float getMp() {
		return (float) (mp*0.1);
	}
	public float getPp() {
		return (float) (pp*0.8);
	}

}
