package ce1002.a6.s995002014;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame() {
		//設定JFrame基本參數
	   setTitle("A6");
	   //setSize(300, 200);
	   setLocationRelativeTo(null);
	   //Contentpane.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
	   setDefaultCloseOperation(EXIT_ON_CLOSE);        
	}
	
	//依照作業要求之method
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setRoleState(hero);
		panel.setLocation(x, y);
	}
}
