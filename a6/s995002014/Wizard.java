package ce1002.a6.s995002014;

public class Wizard extends Hero {
	
	Wizard() {
		setName("Wizard");
	}
	
	//Override
	public float getHp() {
		return (float) (this.hp*0.2);
	}
	public float getMp() {
		return (float) (mp*0.7);
	}
	public float getPp() {
		return (float) (pp*0.1);
	}
	
}
