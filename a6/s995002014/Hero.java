package ce1002.a6.s995002014;

public class Hero {
	
	protected String name;
	protected float hp=30;
	protected float mp=30;
	protected float pp=30;
	
	//setters
	public void setName(String name) {
		this.name=name;
	}
	public void setHp(float hp) {
		this.hp=hp;
	}
	public void setMp(float mp) {
		this.mp=mp;
	}
	public void setPp(float pp) {
		this.pp=pp;
	}
	
	//getters
	public String getName() {
		return name;
	}
	public float getHp() {
		return hp;
	}
	public float getMp() {
		return mp;
	}
	public float getPp() {
		return pp;
	}
	
}
