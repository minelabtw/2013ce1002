package ce1002.a6.s995002014;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel  {
	private JLabel herotext;
	MyPanel(){
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); //上下BoxLayout
		setBorder(BorderFactory.createLineBorder(Color.black)); //邊界黑線
	}
	
	//設定hero文字圖片
	public void setRoleState(Hero hero){
		herotext=new JLabel(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP: "+hero.getPp());
		add(herotext);
		JLabel picLabel = new JLabel(new ImageIcon(hero.getName()+".jpg"));
		add(picLabel);
	}
}
