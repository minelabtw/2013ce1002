package ce1002.a6.s102502021;

import ce1002.a5.s102502021.Hero;
import ce1002.a5.s102502021.Knight;
import ce1002.a5.s102502021.Swordsman;
import ce1002.a5.s102502021.Wizard;

public class A6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero hero[] = new Hero[3]; //宣告陣列
		hero[2] = new Knight();   //多形
		hero[1] = new Swordsman();
		hero[0] = new Wizard();
		
		hero[2].setname("Knight");        //設定名子
		hero[1].setname("Swordman");
		hero[0].setname("Wizard");
		
		for(int i=0;i<3;i++)        //設定hp mp pp
		{
			hero[i].sethp(30);
			hero[i].setmp(30);
			hero[i].setpp(30);
		}
		for (int k=0;k<3;k++)   //輸出
		{
			System.out.println(hero[k].getname()+" HP: "+hero[k].gethp()+" MP: "+hero[k].getmp()+" PP: "+hero[k].getpp());
		}

	}

}
