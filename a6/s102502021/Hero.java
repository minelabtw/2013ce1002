package ce1002.a6.s102502021;

public class Hero {
	public String _name;
	public float _hp;
	public float _mp;
	public float _pp;
	
	Hero()
	{
		
	}
	
	public void setname(String name)
	{
		_name = name;
	}
	public void sethp(float hp)
	{
		_hp = hp;
	}
	public void setmp(float mp)
	{
		_mp = mp;
	}
	public void setpp(float pp)
	{
		_pp = pp;
	}
	
	public String getname()
	{
		return _name;
	}
	public float gethp()
    {
		return _hp; 
	}
	public float getmp()
	{
		return _mp;
	}
	public float getpp()
	{
		return _pp;
	}
}
