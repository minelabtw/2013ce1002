package ce1002.a6.s101201508;

import java.awt.GridLayout;

import javax.swing.*;

public class MyPanel extends JPanel{
	MyPanel()
	{
		
	}
	public void setRoleState(hero in_hero)
	{
		String picture = null;
		//input name,HP,MP,PP into label
		JLabel label1= new JLabel(in_hero.get_name()+" HP: "+in_hero.get_HP()+" MP: "+in_hero.get_MP()+" PP: "+in_hero.get_PP(),JLabel.LEFT);
		//add into JPanel
		add(label1);
		//input the image into label
		if (in_hero.get_name()=="Wizard")
		{
			picture="src\\ce1002\\a6\\s101201508\\Wizard.jpg";
		}
		else if (in_hero.get_name()=="Swordsman")
		{
			picture="src\\ce1002\\a6\\s101201508\\Swordsman.jpg";
		}
		else if (in_hero.get_name()=="Knight")
		{
			picture="src\\ce1002\\a6\\s101201508\\Knight.jpg";
		}
		ImageIcon image = new ImageIcon(picture);
		JLabel label2=new JLabel(image);
		//add into the JPanel
		add(label2);
	}
}
