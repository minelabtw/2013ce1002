package ce1002.a6.s101201508;

import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame{
	MyFrame()
	{
		setLayout(new GridLayout(3,1));
	}
	
	public void newRolePos(hero H , MyPanel P , int x , int y)
	{
		P.setRoleState(H);
		//set the Layout
		GridBagConstraints c = new GridBagConstraints();
        c.gridx = x;
        c.gridy = y;
        c.gridwidth = 1;
        c.gridheight = 1;
        c.weightx = 0;
        c.weighty = 0;
        c.fill = GridBagConstraints.NONE;
        c.anchor = GridBagConstraints.WEST;
        //add into MyFrame
		add(P,c);
	}
}
