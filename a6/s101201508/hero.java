package ce1002.a6.s101201508;

public class hero {
	private String name;
	protected double HP=0;
	protected double MP=0;
	protected double PP=0;
	public void set_name(String in_name)//use input name
	{
		name=in_name;
	}
	public String get_name()//use get name
	{
		return name;
	}
	public void set_HP(double in_HP)//input HP
	{
		HP=in_HP;
	}
	public double get_HP()//get HP
	{
		return HP;
	}
	public void set_MP(double in_MP)//input MP
	{
		MP=in_MP;
	}
	public double get_MP()//get MP
	{
		return MP;
	}
	public void set_PP(double in_PP)//input PP
	{
		PP=in_PP;
	}
	public double  get_PP()//get PP
	{
		return PP;
	}
}
