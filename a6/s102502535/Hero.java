package ce1002.a6.s102502535;

public class Hero {

	private String Name;
	private double HP = 30;
	private double MP = 30;
	private double PP = 30; // declare variables
	private String Pic;

	// getters and setters
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getHP() {
		return HP;
	}

	public void setHP(float hp) {
		HP = hp;
	}

	public double getMP() {
		return MP;
	}

	public void setMP(float mp) {
		MP = mp;
	}

	public double getPP() {
		return PP;
	}

	public void setPP(float pp) {
		PP = pp;
	}

	public String getPic() {
		return Pic;
	}

	public void setPic(String picture) {
		Pic = picture;
	}
}
