package ce1002.a6.s102502535;

public class Swordsman extends Hero {

	public String getName() {
		setName("Swordsman");
		return super.getName();
	} // set the name and then return it

	public double getHP() {
		return super.getHP() * 0.1;
	} // set HP and then return it

	public double getMP() {
		return super.getMP() * 0.1;
	} // set MP and then return it

	public double getPP() {
		return super.getPP() * 0.8;
	} // set PP and then return it
}
