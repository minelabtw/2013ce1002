package ce1002.a6.s102502535;

public class A6 {

	public static void main(String[] args) {
		Hero[] heroes;
		heroes = new Hero[3]; // declare a class array

		heroes[0] = new Wizard();
		heroes[0].setHP(30);
		heroes[0].setMP(30);
		heroes[0].setPP(30); // read data of the wizard

		heroes[1] = new Swordsman();
		heroes[1].setHP(30);
		heroes[1].setMP(30);
		heroes[1].setPP(30); // read data of the swordsman

		heroes[2] = new Knight();
		heroes[2].setHP(30);
		heroes[2].setMP(30);
		heroes[2].setPP(30); // read data of the knight

		for (int i = 0; i < 3; i++) {
			System.out.println(heroes[i].getName() + " HP: "
					+ heroes[i].getHP() + " MP: " + heroes[i].getMP() + " PP: "
					+ heroes[i].getPP());
		} // output the data
	}

}
