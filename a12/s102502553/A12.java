package ce1002.a12.s102502553;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Set;
import java.util.Timer;
import javax.swing.JFrame;
public class A12 implements KeyListener
{
	static JFrame jf; 
	public static void main(String[] args)//設定frame
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, 500, 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton btn = new MovingButton();
		Thread t1 = new Thread(btn);//會跑
		t1.start();//開始跑
		jf.add(btn);
		
	}
	@Override
	public void keyPressed(KeyEvent e) {//案任意建會觸發事件
		
		MovingButton btn2 = new MovingButton();
		Thread t2 = new Thread(btn2);
		t2.start();
		jf.add(btn2);
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
