package ce1002.a12.s102502553;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable{

	private double dx,dy;
	private int x = 197,y = 109;
	private int counter = 0;
	
	public MovingButton()//設定button基本值
	{
		setLayout(null);
		setBounds(x, y, 50, 50);
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
	}
	@Override
	public void run() {
		Timer timer = new Timer(20,new TimerListener());//每隔0.02秒執行一次
		timer.start();	
	}
	
	class TimerListener implements ActionListener{//碰壁的情況和跑的位置

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			if(x < 0 || x > 430)
			{
				dx = dx * (-1);
				setBounds((int)(x + dx),(int)(y + dy),50,50);
				counter++;
			}
			if(y < 0 || y > 410)
			{
				dy = dy * (-1);
				setBounds((int)(x + dx),(int)(y + dy),50,50);
				counter++;
			}
			else{
				setBounds((int)(x + dx),(int)(y + dy),50,50);
			}
			x = (int) (x + dx);
			y = (int) (y + dy);
			setText("" + counter);
			repaint();
			
		}
	}
}
		
	