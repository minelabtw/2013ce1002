package ce1002.a12.s102502027;

import javax.swing.*;

import java.util.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class A12 implements KeyListener {

	static JFrame jf;
	static JButton b;
	static JButton bb;
	static int lx = 500;
	static int ly = 500;
	
	public static void main(String[] args) {
		
		bb = new MovingButton(lx, ly);
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.setLayout(null);
		jf.setSize(500, 500);
		jf.add(bb);
		Thread thread = new Thread((Runnable) bb);  // declare
		thread.start(); // operate thread
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void keyPressed(KeyEvent event) { 
		b =new MovingButton(lx, ly); // new one button
		jf.add(b);
		Thread thread = new Thread((Runnable) b);  // declare
		thread.start(); // operate thread

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}