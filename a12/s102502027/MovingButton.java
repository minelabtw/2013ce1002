package ce1002.a12.s102502027;

import javax.swing.*;

import java.util.*;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	double x, y;
	int lx, ly;
	private int bounceCounter;
	
	
	public MovingButton(int lx, int ly) { 
		this.lx = lx;
		this.ly = ly;
		x = lx / 2 - 40;
		y = ly / 2 - 50;
		setBounds((int) x, (int) y, 50, 50);
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);

	}

	public void run() { // move

		while (true) {
			if (!(x + 50 >= lx || y + 50 >= ly || x <= 0 || y <= 0)) {
				x += dx;
				y += dy;
			} else {  //bounce
				
				if (x + 50 > lx || x < 0) {
					y += dy;
					dx = -dx;
					x += dx;
				}
				if (y + 50 > ly || y < 0) {
					x += dx;
					dy = -dy;
					y += dy;
				}
				bounceCounter++;
			}

			setBounds((int) x, (int) y, 50, 50); // lacation
			setText(bounceCounter + "");

			try {  // slow down
				Thread.sleep(15);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

}
