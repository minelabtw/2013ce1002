package ce1002.a12.s102502516;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double x, y, dx, dy;
	private int bounceCounter;

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r); // 設定方向
		x = 250;
		y = 0;
		setBounds((int) x, (int) y, 50, 50); // 設定初始位置

	}

	public void run() {
		for (;;) {
			x += dx;
			y += dy; // 位移

			if (x < 0) {
				x = Math.abs(x);
				dx = Math.abs(dx);
				bounceCounter++;
			} else if (y < 0) {
				y = Math.abs(y);
				dy = Math.abs(dy);
				bounceCounter++;

			} else if (x > 500 - 50) {
				x -= 2 * dx;
				dx *= -1;
				bounceCounter++;
			} else if (y > 500 - 50) {
				y -= 2 * dy;
				dy *= -1;
				bounceCounter++;
			} // 碰壁時

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} // 速度
			setLocation((int) x, (int) y);
			setText(Integer.toString(bounceCounter)); // 顯示次數

		}
	}
}
