package ce1002.a12.s102502005;

import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;


import javax.swing.JFrame;
import javax.swing.JPanel;

public class A12 implements KeyListener {//整個A12 class就是一個Listener。
	static JFrame jf;
	static JPanel jp;
	static ArrayList<MovingButton> buttonList;//把所有button放到一個ArrayList裡面，以便修改Button的碰撞範圍。
											//其實可以用一個thread執行ArrayList裡面所有MovingButton的run()。
	public static void main(String[] args) {

		buttonList = new ArrayList<MovingButton>();

		A12 listener = new A12();

		jf = new JFrame("A12");
		// jf.setLayout(new FlowLayout());//不知為何如果在這邊設定layout為其他layout的話，按鈕和panel都會顯示不出來
		jf.setSize(jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);//設定JFrame的大小為外框寬度+500px
		jf.setLocationRelativeTo(null);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		jp = new JPanel();//在frame裡面放一個JPanel比較好處理。
		jp.setLayout(null);
		jp.addComponentListener(new ComponentListener() {//這個Listener在這裡主要是監聽JPanel改變大小的事件

			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void componentResized(ComponentEvent e) {//一旦改變大小，就要把每個MovingButton的碰撞判定範圍改掉
				for (int i = 0; i < buttonList.size(); i++) {
					buttonList.get(i).setpanelHeight(jp.getHeight());
					buttonList.get(i).setpanelWidth(jp.getWidth());
				}

			}

			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub

			}
		});
		jf.add(jp);
		
		MovingButton temp = new MovingButton(500, 500);//一開始要有一個MovingButton在中間，不知為何位置填入jp.getHeight(),jp.getWidth會導致MovingButton被卡在左上角。
		jp.add(temp);
		buttonList.add(temp);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		MovingButton temp = new MovingButton(jp.getHeight(), jp.getWidth());//一但按下鍵盤就新增一個MovingButton到jp上。
		jp.add(temp);
		buttonList.add(temp);//記得要把MovingButton加到buttonlist裡面。

	}

	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}
}