package ce1002.a12.s102502005;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;// MovingBtton移動的方向。
	private int panelHeight, panelwidth;// 裝MovingButton容器的大小。
	private int x = 225, y = 225;// MovingButton在容器裡的位置。
	private int bounceCounter = 0;// 反彈了幾次。
	Thread thread = new Thread(this);// 執行該MovingButton的run()的thread。

	public MovingButton(int panelHeigght, int panelWidth) {
		this.panelHeight = panelHeigght;
		this.panelwidth = panelWidth;

		this.setBounds(x, y, 50, 50);
		this.setText(String.valueOf(bounceCounter));

		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		thread.start();// 開始執行run()的內容。

	}

	public void setpanelHeight(int panelHeight) {// 設定容器大小。
		this.panelHeight = panelHeight;
	}

	public void setpanelWidth(int panelWidth) {
		this.panelwidth = panelWidth;
	}

	public void run() {

		long beforeTime;// 該次移動開始的時間戳章。
		long timeDiff;// 執行完移動運算所花的時間。
		long sleep;// 預定移動一次之後要延遲多久再移動一次。

		beforeTime = System.currentTimeMillis();// 開始移動的運算之前先把時間記下來。

		while (true) {
			x += (int) dx;// 移動。
			y += (int) dy;

			if (x <= 0) {// 碰撞判定。
				dx *= -1;
				x = 0;
				bounceCounter++;
			} else if (x >= panelwidth - 50) {
				dx *= -1;
				x = panelwidth - 50;
				bounceCounter++;
			}

			if (y <= 0) {
				dy *= -1;
				y = 0;
				bounceCounter++;
			} else if (y >= panelHeight - 50) {
				dy *= -1;
				y = panelHeight - 50;
				bounceCounter++;
			}
			this.setLocation(x, y);
			this.setText(String.valueOf(bounceCounter));

			timeDiff = System.currentTimeMillis() - beforeTime;// 算出"移動運算"花了多少時間。
			sleep = 25 - timeDiff;// 把"預定要延遲的時間"減掉因"移動運算"消耗的時間，就是最後真正要延遲的時間。

			if (sleep > 0) {//假如"移動運算"太久的話，真正的延遲時間可能為負(代表應該要算下一次移動了電腦還來不及算)，就不需要再sleep了。
				try {
					Thread.sleep(sleep);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

			beforeTime = System.currentTimeMillis();//記下下一次移動運算開始時的時間。
		}

	}
}