package ce1002.a12.s102502513;

import javax.swing.*;
import java.util.Timer;
import java.util.TimerTask;

public class MovingButton extends JButton implements Runnable
{
  private double dx,dy;
  int dnx=180 , dny=250; //每個按鈕的初位置
  private int bounceCounter =0;  
	
  public MovingButton()
  {
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r); // x方向上的速度
		dy=3*Math.sin(r); // y方向上的速度
		setBounds(dnx,dny,50,50);
		this.setText("" + bounceCounter);
  }
  public void run() 
  {
    Timer timer = new Timer();
    timer.schedule(new ButtonRun(), 0, 20); // 0為起步的延時，20為每個動作的延時
  }
  class ButtonRun extends TimerTask
  {
	  @Override
	  public void run() 
	  {
		 if(dnx<=0 || dnx>=440)  //讓按鈕觸及邊框時，有反彈的效果
		 {
			 dx = -dx;
			 bounceCounter++;
			 setText("" + bounceCounter);
		 }
		 if(dny<=0 || dny>=415)
		 {
			 dy = -dy;
			 bounceCounter++;
			 setText("" + bounceCounter);
		 }
		 
		 dnx += dx;
		 dny += dy;
		 setLocation(dnx,dny);
	  }
  }
}