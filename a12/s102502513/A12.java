package ce1002.a12.s102502513;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 100, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton Btn = new MovingButton(); //用MovingButton類別,建立一個Btn物件
		Thread Td1 = new Thread(Btn); //將Btn放入Thread中做動作
		Td1.start();
		jf.add(Btn);
	}
	
	@Override
	public void keyPressed(KeyEvent e)  //建立一個由鍵盤控制的事件處理者
	{
		MovingButton Btn2 = new MovingButton();
		Thread Td2 = new Thread(Btn2);
		Td2.start();
		jf.add(Btn2);
	}
	
	@Override
	public void keyReleased(KeyEvent e)  //因實作介面的關西，需將未用到的方法寫入，但內容為空
	{}

	@Override
	public void keyTyped(KeyEvent e)
	{}
	
	}
	
	
 
  

