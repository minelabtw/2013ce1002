package ce1002.a12.s102502535;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

import javax.swing.*;

public class MyFrame extends JFrame {

	List<JButton> buttons = new ArrayList<JButton>();
	JFrame frame = new JFrame("A12-102502535");

	MyFrame() {
		setSize(500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLayout(null);
		setTitle("A12-102502535");
		KeyListener listener = new Key_listener();
		addKeyListener(listener);
		setVisible(true);
	} // set the frame's data

	class Key_listener implements KeyListener {

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
		}

		public void keyTyped(KeyEvent e) {
			JButton button = new Move(getHeight(), getWidth());
			buttons.add(button);
			add(button);

			Thread thread = new Thread((Runnable) button);
			thread.start();
		}
	} // key listener

}
