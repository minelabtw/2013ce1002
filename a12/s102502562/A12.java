package ce1002.a12.s102502562;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		jf=new JFrame("A12-102502562");
		A12 listener=new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300,300,jf.getInsets().left+jf.getInsets().right+500,jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton firstbutton=new MovingButton();//讓一開始有一個按鈕在畫面中
		jf.add(firstbutton);
	}
	@Override
	public void keyPressed(KeyEvent arg0)//當按按鍵時新增一個按鈕出來
	{
		MovingButton newbutton=new MovingButton();
		jf.add(newbutton);
	}
	@Override
	public void keyReleased(KeyEvent arg0)
	{
		
	}
	@Override
	public void keyTyped(KeyEvent arg0)
	{
		
	}
}
