package ce1002.a12.s102502562;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	private double dx,dy;
	private int bounceCounter=0;
	Thread movingbutton=new Thread(this);
	public MovingButton()
	{
		double r=Math.random() * 3.14159;
		dx=3 * Math.cos(r);
		dy=3 * Math.sin(r);
		setText(String.valueOf(bounceCounter));
		setBounds(220,220,60,60);
		movingbutton.start();//呼叫Thread的star函式
	}
    public void run() 
    {
    	while(true)
    	{
    		try
    		{
    			Thread.sleep(15);//設定進入下一次的間隔
    			double x=getLocation().x;//取得按鈕的x,y位置
				double y=getLocation().y;
				if(x+75+dx <= 500 && x+dx >= 0 && y+95+dy <= 500 && y+dy >= 0)//當按鈕還沒碰到邊界時
				{
					x+=dx;//移動按鈕
					y+=dy;
					setLocation((int)x,(int)y);//重新設定按鈕位置
				}
				else//當按鈕碰到邊界時
				{
					bounceCounter++;//計算撞擊次數
					setText(String.valueOf(bounceCounter));//重新寫上新的撞擊次數
					if(x+dx < 0 || x+dx+75 > 500)//當按鈕撞到x邊界讓dx反向
					{
						dx*=Double.valueOf(-1);
					}
					else if(y+dy < 0 || y+dy+95 > 500)//當按鈕撞到y邊界讓dy反向
					{
						dy*=Double.valueOf(-1);
					}
				}
			}
    		catch(Exception e)
    		{
    			e.printStackTrace();
			}
    	}
    }
}
