package ce1002.a12.s100203020;
import java.awt.Dimension;
import java.util.Random;

import javax.swing.*;
public class MovingButton extends JButton implements Runnable
{
  private double dx,dy;
	private int bounceCounter = 0;
	
	public MovingButton()
	{		
		double r = Math.random()*Math.PI*2;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);		
	}
	public void run()
	{
		Random random = new Random();
		int speed = random.nextInt(30)+10;
		int directionsX=-1;
		int directionsY=-1;
		int nextX = this.getLocation().x;
		int nextY = this.getLocation().y;
		this.setText(Integer.toString(bounceCounter));

		while(true){
			
			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			nextX += (int)(dx*directionsX);
			nextY += (int)(dy*directionsY);
			boolean outOfBorder =false;
			if(nextX<0||nextX > this.getParent().getSize().width-this.getSize().width){
				directionsX = -directionsX;
				bounceCounter ++;
				this.setText(Integer.toString(bounceCounter));
				outOfBorder =true;
							
			}
			if(nextY<0|| nextY > this.getParent().getSize().height-this.getSize().height){
				directionsY = -directionsY;
				bounceCounter ++;
				this.setText(Integer.toString(bounceCounter));
				outOfBorder =true;

			}
			if(outOfBorder){
				continue;
			}
			
			this.setLocation(nextX, nextY);
			repaint();
		}
  }
}