package ce1002.a12.s100203020;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import java.util.Random;
public class A12 implements KeyListener
{
	static JFrame jf;
	
	public static void main(String[] args) 
	{
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(200, 100, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addMovingButton();//add MovingButton
	}
	
	//add MovingButton to JFrame
	public static void addMovingButton(){
		Random random = new Random();
		int jbtSize = 55;//set MovingButton length
		MovingButton button = new MovingButton();
		//set initial location of MovingButton random 
		button.setBounds(random.nextInt(jf.getSize().width-jbtSize- jf.getInsets().left-jf.getInsets().right),random.nextInt(jf.getSize().height-jf.getInsets().top-jf.getInsets().bottom-jbtSize), jbtSize, jbtSize);
		Thread thread = new Thread(button);
		thread.start();
		jf.add(button);//add MovingButton
	
	}
	
	
	//if press key , add MovingButton
	@Override
	public void keyPressed(KeyEvent arg0) {
		addMovingButton();
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
	}
 
}