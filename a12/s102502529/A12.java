package ce1002.a12.s102502529;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

public class A12 implements KeyListener {

	static JFrame jf;

	public static void main(String[] args) { // 助教給Der
		// TODO Auto-generated method stub

		jf = new JFrame();
		A12 listener = new A12();
		jf.setTitle("A12-102502529");
		jf.getContentPane().setLayout(null);
		jf.setBounds(0, 0, 500, 500);
		jf.addKeyListener(listener); // 對於A12做keylistener
		jf.setVisible(true);
		jf.setFocusable(true); // 針對override的物件??
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent e) { // 按下按鍵執行
		// TODO Auto-generated method stub
		MovingButton e04 = new MovingButton();
		jf.add(e04);
		Thread thread = new Thread(e04);
		thread.start();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
