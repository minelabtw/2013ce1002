package ce1002.a12.s102502529;

import java.util.Random;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double dx, dy;
	private double x, y;
	private int bounceCounter;
	Random random = new Random();

	public MovingButton() { // 設置button的基本資料

		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		x = random.nextInt(437);
		y = random.nextInt(412);
		setSize(50, 50);
		setLocation((int) x, (int) y);
		setText("" + bounceCounter);
	}

	public void run() {
		try {
			while (true) { // button 移動
				x = (x + dx);
				y = (y + dy);
				setLocation((int) x, (int) y);
				Thread.sleep(18); // lock
				if (x < 0 || x > 436) { // 反彈判斷
					dx = (-dx); // 改向量
					bounceCounter++;
					setText("" + bounceCounter);
				}
				if (y < 0 || y > 411) { // 反彈判斷
					dy = (-dy);// 改向量
					bounceCounter++;
					setText("" + bounceCounter);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}