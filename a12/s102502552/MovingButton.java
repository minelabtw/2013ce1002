package ce1002.a12.s102502552;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable{

	private double dx,dy;
	private int x = 197,y = 109;
	private int counter = 0;
	
	public MovingButton()
	{
		setLayout(null);
		setBounds(x, y, 50, 50);
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);

	}
	@Override
	public void run() {
		Timer timer = new Timer(20,new TimerListener());
		
		timer.start();	//計時器每20毫秒計一次
	}
	
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			if(x == 0 || x >= 384)
			{
				dx = dx * (-1);
				x += dx;
				y += dy;
				setBounds((int)x,(int)y,50,50);
				counter++;
			}//撞到左右彈回
			if(y <= 0 || y >= 211)
			{
				dy = dy * (-1);
				y += dy;
				x += dx;
				setBounds((int)x,(int)y,50,50);
				counter++;
			}//撞到上下彈回
			else{
				y += dy;
				x += dx;
				setBounds((int)x,(int)y,50,50);
			}//正常移動
			
			setText("" + counter);
			repaint();
			
		}
		
	}
}
