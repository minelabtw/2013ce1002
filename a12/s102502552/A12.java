package ce1002.a12.s102502552;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener{
	private static JFrame frame;

	public static void main(String[] args){
		
		A12 listener = new A12();
		
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setVisible(true);//建立方框
		
		MovingButton btn = new MovingButton();
		Thread thread = new Thread(btn);
		thread.start();
		frame.add(btn);//先建一個按鍵
		
		frame.addKeyListener(listener);//根據按下指令製造按鍵
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		MovingButton btn2 = new MovingButton();
		Thread thread2 = new Thread(btn2);
		thread2.start();
		frame.add(btn2);//按任意鍵即做一個按鍵
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
