package ce1002.a12.s102502050;
import javax.swing.*;

import java.util.Random;
public class MovingButton extends JButton implements Runnable{
	double vx,vy;
	private int bounce=0;
	MovingButton nextButton;
	public MovingButton()
	{
		this.setText(""+0);
		nextButton=null;
		Random random = new Random();
		double angle = random.nextDouble()*3.14;
		vx = Math.cos(angle)*3;
		vy = Math.sin(angle)*3;
	}
	
	//linklist
	public void Setnextbutton(MovingButton next)
	{
		nextButton=next;
	}
	public MovingButton Getnextbutton()
	{
		return nextButton;
	}
	
	public void run()
	{
		
		int x =this.getX();
		int y=this.getY();
		x+=(int)vx;
		y+=(int)vy;
		//加上(int)就不會有button卡在牆邊
		//但我不知道為什麼
		
		if(x<=0 &&(int)  vx !=0)
		{
			vx = -1*vx;
			bounce++;
			this.setText(""+bounce);
		}
		else if(x+50>=485 &&(int) vx!=0)
		{
			vx= -1*vx;
			bounce++;
			this.setText(""+bounce);
		}
		if(y<=0 &&(int)  vy !=0)
		{
			vy= -1*vy;
			bounce++;
			this.setText(""+bounce);
		}
		else if(y+50>=465 &&(int)  vy!=0)
		{
			vy= -1*vy;
			bounce++;
			this.setText(""+bounce);
		}
		setBounds(x,y,50,50);
		
	}
}
