package ce1002.a12.s995002014;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	private static Vector<MovingButton> buttonList=new Vector<MovingButton>();
	private static JFrame jf= new JFrame("A12");
	public static void main(String[] args) {
		A12 listener = new A12();
		//jf.getContentPane().add(new JButton("1234252"));
		MovingButton a =new MovingButton(jf);
		jf.add(a);
		Thread thread = new Thread(a);
		thread.start();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		buttonList.add(new MovingButton(jf));
		jf.add(buttonList.lastElement()); 
		Thread thread = new Thread(buttonList.lastElement());
		thread.start();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
//���ʫ��s
class MovingButton extends JButton implements Runnable {
	private double dx,dy;
	private double x,y;
	private int bounceCounter=0;
	private JFrame a;
	public MovingButton(JFrame a) {
		super();
		this.a=a;
		setBounds(225, 255, 50, 50);
		setText(Integer.toString(bounceCounter));
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
	}
	public void run() {
		while(true){
			x+=dx;
			y+=dy;
			if(x>425||x<0){
				dx*=-1;
				bounceCounter++;
				setText(Integer.toString(bounceCounter));
			}
			if(y>425||y<0){
				dy*=-1;
				bounceCounter++;
				setText(Integer.toString(bounceCounter));
			}
			setLocation((int)x,(int)y);
			a.repaint();
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}