package ce1002.a12.s101201521;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
public class A12 extends KeyAdapter
{
	public static JFrame frame;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		frame = new JFrame("A12-101201521");
		A12 listener = new A12();
		//add first button
		MovingButton bt = new MovingButton();
		Thread t = new Thread(bt);
		frame.add(bt);
		t.start();
		//setting of frame
		frame.getContentPane().setLayout(null);
		frame.setSize(17+frame.getInsets().left+frame.getInsets().right+500, 38+frame.getInsets().top+frame.getInsets().bottom+500);
		frame.setLocationRelativeTo(null);
		frame.addKeyListener(listener);
		frame.setVisible(true);
		frame.setFocusable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	@Override
	public void keyPressed(KeyEvent e){
		//add new button
		MovingButton bt = new MovingButton();
		frame.add(bt);
		new Thread(bt).start();
	}
}