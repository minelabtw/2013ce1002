package ce1002.a12.s101201521;
import javax.swing.*;
public class MovingButton extends JButton implements Runnable
{
	//moving vector
	private double dx,dy;
	//bounce counter
	private int bounceCounter=0;
	//origin point of button
	public int x =225;
	public int y =225;
	public MovingButton()
	{
		//setting button
		setText(bounceCounter+"");
		setSize(50,50);
		setLocation(x,y);
		//calculate moving vector
		double r = ( Math.random()*2 -1)*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		
	}

	@Override
	public void run() {
		try {
			while(true){
				//check whether the button touched the edge
				if(x + 50 >= 500 || x <= 0){
					dx = -dx;
					bounceCounter++;
					setText(bounceCounter+"");
				}
				if(y + 50 >= 500 || y <=0){
					dy = -dy;
					bounceCounter++;
					setText(bounceCounter+"");
				}
				x = x + (int)(dx);
				y = y + (int)(dy);
				this.setLocation(x,y);
				Thread.sleep(10);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}