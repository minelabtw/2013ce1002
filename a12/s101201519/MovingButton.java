package ce1002.a12.s101201519;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	private double location[]=new double[2];//存現在的位置用
	private double d[]=new double[2];//位移
	private int s[]=new int[2];//視窗長寬
	private int bounceCounter;//計算彈幾次
	
	public MovingButton(int s1,int s2)//改用陣列形式存 比較方便
	{
		double r=Math.random()*3.14159;
		d[0]=Math.cos(r)/5;
		d[1]=Math.sin(r)/5;
		s[0]=s1;//存入陣列
		s[1]=s2;
		bounceCounter=0;
		String label=Integer.toString(bounceCounter);
		super.setLabel(label);
		super.setBounds(s1/2-30,s2/2-20,60,40);
		location[0]=s1/2-30;
		location[1]=s2/2-20;
	}
	@Override
	
	public void run()
	{
		//判斷反彈方向
		for (int i=0;i<2;i++)
		{
			location[i]=location[i]+d[i];
			if (location[i]<0)
			{
				location[i]=location[i]*(-1);
				d[i]=d[i]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
			else if (i==0 && location[i]>s[0]-60)
			{
				location[0]=(location[0]-s[0]+60)*(-1)+s[0]-60;
				d[0]=d[0]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
			else if (i==1 && location[i]>s[1]-40)
			{
				location[1]=(location[1]-s[1]+40)*(-1)+s[1]-40;
				d[1]=d[1]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
		}
		
		super.setLocation((int)location[0], (int)location[1]);
	}
	
	

}
