package ce1002.a12.s101201519;

import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.awt.event.KeyListener;
import java.util.concurrent.Executors;


public class A12 implements KeyListener{
	
	static JFrame jf;
	
	public static void main(String[] args) {//做面板

		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,jf.getInsets().left + jf.getInsets().right + 500,jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Button();

	}

	public static void Button() {//按鈕
		MovingButton m = new MovingButton(500 , 500 );
		jf.add(m);
		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(m, 0, 1, TimeUnit.MILLISECONDS);
	}

	@Override
	public void keyPressed(KeyEvent e)//鍵盤案下去就執行
	{
		
	}
	@Override
	public void keyReleased(KeyEvent e)//鍵盤起來就執行
	{
		
	}
	@Override
	public void keyTyped(KeyEvent e) //鍵盤下去又起來
	{
		Button();
	}

}
