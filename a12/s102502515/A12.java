package ce1002.a12.s102502515;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		// frame settings
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.setLayout(null);
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 100, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton Btn = new MovingButton();//the first button
		 Thread t1 = new Thread(Btn);//thread to run
		 t1.start();//start
		 jf.add(Btn);
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
	}
	//if pressed, new a button
	@Override
	public void keyPressed(KeyEvent e) {
		 MovingButton Btn = new MovingButton();
		 Thread t1 = new Thread(Btn);
		 t1.start();
		 jf.add(Btn);
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

 

}
