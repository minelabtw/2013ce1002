package ce1002.a12.s102502515;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

	public class MovingButton extends JButton implements Runnable
	{
	  private double dx,dy;
	  private int bounceCounter = 0;
	  private int x=220,y=220;

	  public MovingButton()//button settings
		{
			double r = Math.random()*3.14159;
			dx=3*Math.cos(r);//the direction
			dy=3*Math.sin(r);
			setLayout(null);
			setBounds(x,y,50,50);//first location
			setText("" + bounceCounter);
		}
	  public void run() 
	  {
		  Timer timer = new Timer(10, new TimerListener());//new a timer to control the moving button
		  timer.start();//start
	  }
	  
	  private class TimerListener implements ActionListener{
		  @Override
		  public void actionPerformed(ActionEvent e){
			  if ( x < 0 || x > 450-getInsets().right ){//if touch the left or right then bounce
				  dx = dx * (-1);
				  bounceCounter++;
				  setText(""+bounceCounter);
			  }
			  if ( y < 0 || y > 420-getInsets().bottom){//if touch the top or bottom then bounce
				  dy = dy * (-1);
				  bounceCounter++;
				  setText(""+bounceCounter);
			  }
			  x = (int) (x + dx);
			  y = (int) (y + dy);
			  setLocation(x, y);//new it location
		  }
		 
	  }
	}
