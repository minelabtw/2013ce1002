package ce1002.a12.s102502555;



import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import java.util.Timer;
import java.util.TimerTask;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;  //向量
	int x = 200;  //初始值
	int y = 200;
	private int bounceCounter;  //計數器
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setText(""+bounceCounter);
		setBounds(x,y,60,60);
	}
  public void run() 
  {
	 Timer timer = new Timer();
	 timer.schedule(new Running(), 500, 15);  //隨時間移動位置
  }
  
  class Running extends TimerTask{
	  public void run(){
		 //判斷有無碰到邊界
		 if((x + dx) < 0 || (x + 80 + dx) >= 500){
			 dx = -dx;
			 bounceCounter++;
			 setText(""+bounceCounter);
		 }
		 if((y + dy) < 0 || (y + 80 + dy) >= 500){
			 dy = -dy;
			 bounceCounter++;
			 setText(""+bounceCounter);
		 }
		 //移動位置
		 x += dx;
		 y += dy;
		 setLocation(x,y);
	  }
  }
}