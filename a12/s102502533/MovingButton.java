package ce1002.a12.s102502533;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.omg.CORBA.WStringValueHelper;

public class MovingButton extends JButton implements Runnable{
	  private double dx;
	  private double dy;
	  private double x;
	  private double y;
	  private int a=60;
	  private int b=60;
	  private int bounceCounter;
	  Thread mb = new Thread(this);
	  
		public MovingButton(int x ,int y)
		{
			this.x =x;
			this.y =y;
			double r = Math.random()*3.14159;
			dx=3*Math.cos(r);
			dy=3*Math.sin(r);
			setText(""+bounceCounter);
			setBounds(((x-a)/2), ((y-b)/2), a, b);
			mb.start(); 
		}
		@Override
	  public void run() 
	  {
			while(true){
			try{
				Thread.sleep(100);
				double w = getLocation().x; 
				double h = getLocation().y;
				
				if(w + dx <= 0 || (w + dx + b + 15) > x){
						bounceCounter++; 
						setText("" + bounceCounter);
							dx *= -1.0f;
				}
				if(h + dy <= 0 || (h + dy + b + 40) > y){
					bounceCounter++; 
					setText("" + bounceCounter);
						dy *= -1.0f;
			}
				else{
					w = w+ dx;
					h =h+ dy;
					setLocation((int) w, (int) h);
				}
				/*if (x1 + dx >= 0 && x1 + dx + a + 15 <= x && y1 + dy >= 0
						&& y1 + dy + b + 40 <= y) {
					x1 = x1+ dx;
					y1 =y1+ dy;
					setLocation((int) x1, (int) y1);
				} 
				else {
					bounceCounter++; 
					setText("" + bounceCounter);
					
					if (x1 + dx < 0 || x1 + dx + b + 15 > x) {
						dx *= -1.0f;
					}
					if (y1 + dy < 0 || y1 + dy + b + 40 > y) {
						dy *= -1.0f;
					}
				}*/
			}catch(Exception e){
				e.printStackTrace();
			}
	  }
	}
}
