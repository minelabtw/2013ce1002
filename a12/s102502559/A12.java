package ce1002.a12.s102502559;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class A12 implements KeyListener{
	
	static JFrame jf;
	
	public static void main(String[] args) {
		jf = new JFrame();
		jf.setTitle("A12-102502559");
		jf.getContentPane().setLayout(null);
		jf.setBounds(400, 100, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		A12 listener = new A12();
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		MovingButton button =  new MovingButton();//When pressing any key,new an button
		Thread power = new Thread(button);//New a thread
		power.start();// to run it
		jf.add(button);
	}

	@Override
	public void keyReleased(KeyEvent e) {}
	@Override
	public void keyTyped(KeyEvent e) {}
	
}
