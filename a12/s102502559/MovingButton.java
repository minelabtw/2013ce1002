package ce1002.a12.s102502559;

import java.util.Random;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{

	private double xLocation;
	private double yLocation;
	private double dx;
	private double dy;
	private int bounceCounter;
	private Random ran;
	
	public MovingButton()
	{
		setSize(50,50);
		ran = new Random();
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		xLocation = ran.nextInt(400)+1;
		yLocation = ran.nextInt(400)+1;
		setLocation((int)xLocation,(int)yLocation);
		bounceCounter = 0; 
		
	}
	
	@Override
	public void run() {
		
		while(true)
		{
			try {
				yLocation = yLocation + dy;//let it move
				xLocation = xLocation + dx;
				if(xLocation<=0 || xLocation >= 440)//if collision happened , bouncing
				{
					dx = (-1)*dx;
					bounceCounter++;
				}
				if(yLocation<=0 || yLocation >= 430)
				{
					dy = (-1)*dy;
					bounceCounter++;
				}
				setLocation((int)xLocation,(int)yLocation);
				setText(""+bounceCounter);
				Thread.sleep(10);// set delay period
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
