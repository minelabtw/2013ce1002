package ce1002.a12.s102502514;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {

	private double dx, dy, x, y;
	private int bounceCounter = 0;

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		setText("0");
		x = Math.random() * 435; /*隨意設定初始位置*/
		y = Math.random() * 415;
		setBounds((int) x, (int) y, 50, 50);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (true) {
			x = x + dx; /*將x座標加上速度*/
			y = y + dy; /*將y座標加上速度*/
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setBounds((int) x, (int) y, 50, 50); /*改變位置*/
			if (x <= 0 || x >= 435) {
				dx = -dx;
				bounceCounter++;
				setText(bounceCounter + "");
			} else if (y <= 0 || y >= 415) {
				dy = -dy;
				bounceCounter++;
				setText(bounceCounter + "");
			}
		}
	}
}
