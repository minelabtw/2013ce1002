package ce1002.a12.s101201524;
import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	private double dx, dy;
	private int x, y, width, height;
	private int bounceCounter;
		
	MovingButton(){
		//set start point and direction
		x = (485-width)/2;
		y = (470-height)/2;
		double r = Math.random()*2*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		//set size and location
		width = height = 50;
		setBounds(x, y, width, height);
		//count bounceCounter
		bounceCounter = 0;
		setText("" + bounceCounter);
	}
	public void run(){
		while(true){
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//move the button
			x += dx;
			y += dy;
			//if touch the edge, change the direction bounceCounter+1
			if(x + width >= 485){
				x = 485 - width;
				dx = -dx;
				bounceCounter++;
			}
			else if(x <= 0){
				x = 0;
				dx = -dx;
				bounceCounter++;
			}
			if(y + height >= 470){
				y = 470 - height;
				dy = -dy;
				bounceCounter++;
			}
			else if(y <= 0){
				y = 0;
				dy = -dy;
				bounceCounter++;
			}
			//reset and repaint
			setBounds(x, y, width, height);
			setText("" + bounceCounter);
			repaint();
		}
	}
}
