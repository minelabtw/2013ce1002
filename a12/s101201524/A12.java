package ce1002.a12.s101201524;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener{
	static JFrame jf;
	static MovingButton[] button = new MovingButton[1000];
	int i = 1;
	public static void main(String[] args) 
	{
		//set frame's status and add a key listener
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 100, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		button[0] = new MovingButton();
		jf.add(button[0]);
		(new Thread(button[0])).start();
	}
	
	public void keyTyped(KeyEvent e) {
	}
	
	public void keyPressed(KeyEvent e) {
		if(true){
			//create a new button and launch it
			button[i] = new MovingButton();
			jf.add(button[i]);
			(new Thread(button[i])).start();
			i++;
		}
	}
	
	public void keyReleased(KeyEvent e) {
	}
}
