package ce1002.a12.s102502549;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter;

	public MovingButton() {
		setBounds(225, 225, 50, 50);// 要設定bounds按鈕才會出現
		setText("" + bounceCounter);// 設定按鈕上的數字
		setFocusable(false);// 萬一有人按了按鈕，監聽器就完了，所以讓按鈕不能聚焦

		double r = Math.random() * 2 * Math.PI;// 原本只有pi，這樣dy永遠是正值，所以改2pi
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);

		Thread t = new Thread(this);
		t.start();
	}

	public void run() {
		while (true) {
			try {
				setLocation(getX() + (int) dx, getY() + (int) dy);

				// x反彈
				if (getX() <= 0 || getX() >= 450) {
					dx = -dx;
					bounceCounter++;
					setText("" + bounceCounter);
				}

				// y反彈
				if (getY() <= 0 || getY() >= 450) {
					dy = -dy;
					bounceCounter++;
					setText("" + bounceCounter);
				}

				Thread.sleep(10);// 記得要延遲

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
