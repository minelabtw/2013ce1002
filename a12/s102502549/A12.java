package ce1002.a12.s102502549;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {

		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setLocation(300, 300);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setSize(jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);//實測發現getinsets必須在frame顯示後才有值，所以在這裡才設定大小
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	//以下兩個方法可以不用做
	public void keyTyped(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	// 這個就夠了
	public void keyPressed(KeyEvent e) {
		jf.add(new MovingButton());
	}
}
