package ce1002.a12.s102502038;//by 102502038

import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MovingButton extends JButton implements Runnable{
	private double dx,dy;
	public double x,y;
	private int bounceCounter;
	private JFrame frame;
	public MovingButton(){
		double r = Math.random()*Math.PI;
		bounceCounter = 0;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		x = Math.random()*300;
		y = Math.random()*300;
		this.setSize(50, 50);
	}
	public void run(){
		Timer time = new Timer(true);
		time.schedule(new java.util.TimerTask() { public void run(){
			if(x<=0||x>=300){
				bounceCounter++;
				x=(x<=0?0:300);
				dx*=-1;
			}
			if(y<=0||y>=300){
				bounceCounter++;
				y=(y<=0?0:300);
				dy*=-1;
			}
			x += dx;
			y += dy;
			System.out.println(String.valueOf(bounceCounter));
			setText(String.valueOf(bounceCounter));
			setLocation((int)x, (int)y);
			repaint();
		}}, 0, 100);
	}
}