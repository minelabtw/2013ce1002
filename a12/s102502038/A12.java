package ce1002.a12.s102502038;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener{//main class
	static JFrame jf;
	public static void main(String[] args){
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton btn = new MovingButton();
		jf.getContentPane().add(btn);
		Thread thread = new Thread(btn);
		thread.start();
	}
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
	}
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		MovingButton btn = new MovingButton();
		jf.getContentPane().add(btn);
		Thread thread = new Thread(btn);
		thread.start();
	}
}