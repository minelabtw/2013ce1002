package ce1002.a12.s102502040;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy,x,y;
	private int bounceCounter;
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		setText("0");
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		x = Math.random() * 425;//起始位置隨機
		y = Math.random() * 415;
		setBounds((int)x, (int)y, 50, 50);
	}
	public void run() 
	{
		while(true) {
			x = x +dx;//移動位置
			y = y +dy;
			try {
				Thread.sleep(100);
			}catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			setBounds(getX()+(int)dx, getY() + (int)dy, 50, 50);
			if (x <= 0 && x >= 425) {//設定撞牆的位置
				x += dx;
				bounceCounter++;
				setText(bounceCounter + "");
			}
			else if (y <= 0 && y >= 415) {
				y += dy;
				bounceCounter++;
				setText(bounceCounter + "");
			}
	  }
  }
}