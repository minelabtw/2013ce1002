package ce1002.a12.s102502040;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.* ;
import javax.swing.JFrame;
public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args)	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton mb = new MovingButton();
		Thread thread = new Thread(mb);//加入執行動作
		jf.add(mb);
		thread.start();//start to run
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		MovingButton mb = new MovingButton();
		Thread thread = new Thread(mb);
		jf.add(mb);
		thread.start();//run
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}