package ce1002.a12.s102502034;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setFocusable(true);
		MovingButton btn = new MovingButton();
		Thread b = new Thread(btn);
		b.start();
		jf.getContentPane().add(btn);
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// setting frame

	}

	@Override
	public void keyPressed(KeyEvent e) {
		MovingButton btn = new MovingButton();
		Thread b = new Thread(btn);
		b.start();
		jf.getContentPane().add(btn);
		System.out.println("ya");
		// events while key pressed

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
