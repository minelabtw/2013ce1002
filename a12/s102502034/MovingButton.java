package ce1002.a12.s102502034;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	private double vx = 150, vy = 150;

	public MovingButton() {
		setSize(50, 50);
		setText("0");
		double r = Math.random() * 3.14159 * 2;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		//setting dx dy 
		// dx dy are the directions of movement 
	}

	public void run() {

		try {
			for (;;) {

				if (vx > 440 || vx < 0) {
					dx = dx * -1;
					bounceCounter++;
					setText("" + bounceCounter);
					
				}
				if (vy > 410 || vy < 0) {
					dy = dy * -1;
					bounceCounter++;
					setText("" + bounceCounter);

				}
				// while over the range of frame , change direction
				vx = vx + dx;
				vy = vy + dy;
				// move it 
				setLocation((int) vx, (int) vy);
				Thread.sleep(14);
				//set speed

			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
