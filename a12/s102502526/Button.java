package ce1002.a12.s102502526;
import javax.swing.*;
import java.util.*;

public class Button extends JButton implements Runnable{
	private double dx, dy;
	double x, y;
	int xlocate, ylocate;
	private int bounceCounter;
	
	
	public Button(int lx, int ly) { 
		this.xlocate = lx;
		this.ylocate = ly;
		x = lx / 2 - 40;
		y = ly / 2 - 50;
		setBounds((int) x, (int) y, 10, 10);
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);

	}

	public void run() { //判斷有沒有撞牆，並且執行撞到之後的反應

		while (true) {
			if (!(x + 50 >= xlocate || y + 50 >= ylocate || x <= 0 || y <= 0)) {
				x += dx;
				y += dy;
			} else {  
				
				if (x + 50 > xlocate || x < 0) {
					y += dy;
					dx = -dx;
					x += dx;
				}
				if (y + 50 > ylocate || y < 0) {
					x += dx;
					dy = -dy;
					y += dy;
				}
				bounceCounter++;
			}

			setBounds((int) x, (int) y, 50, 50);
			setText(bounceCounter + "");

			try { 
				Thread.sleep(10);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}
}
