package ce1002.a12.s102502526;
import javax.swing.*;
import java.awt.event.*;

public class A12 implements KeyListener{
	static JFrame frame;
	static JButton button;
	static JButton bb;
	static int xlocate = 490;
	static int ylocate = 470;

	public static void main(String[] args) {

		bb = new Button(xlocate, ylocate);
		frame = new JFrame("A12-102502526");
		A12 listener = new A12();
		frame.setBounds(300, 100, 500, 500);
		frame.add(bb); 
		Thread thread = new Thread((Runnable) bb);  // 宣告新的執行序
		thread.start(); 
		frame.addKeyListener(listener);
		frame.setVisible(true);
		frame.setFocusable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void keyPressed(KeyEvent event) {
		button =new Button(xlocate, ylocate); 
		frame.add(button);
		Thread thread = new Thread((Runnable) button);  // 宣告新的執行序
		thread.start(); 

	}
	@Override
	public void keyReleased(KeyEvent e) {
	}//java強制我們覆寫這個函式
	@Override
	public void keyTyped(KeyEvent e) {
	}//同上

}
