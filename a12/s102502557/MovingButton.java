package ce1002.a12.s102502557;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private double dx, dy; //為宜量
	private int bounceCounter = 0; //碰撞次數
	private int fx = 0; //
	private int fy = 0; //
	private int bw = 50; //按鈕的寬
	private int bh = 50;
	
	Thread moving = new Thread(this); //建立一個多執行恤
	
	public MovingButton(int x, int y) 
	{
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		setText("" + bounceCounter); 
		fx = x;//把裡面的x y 傳出這個函式
		fy = y;
		x = (fx - bw) / 2;//設定按鈕的位置 以正中間為基準
		y = (fy - bh) / 2;
		setBounds(x, y, bw, bh); //
		moving.start(); //開始執行序
	}

	public void run() 
	{
		while (true) 
		{
			try 
			{
				Thread.sleep(5); //讓執行敘休息一下
				double x = getLocation().x; //
				double y = getLocation().y; //
			
				if (x + dx >= 0 && x + dx + bw + 15 <= fx && y + dy >= 0 && y + dy + bh + 40 <= fy) //沒有撞到牆壁的情況
				{
					x += dx;
					y += dy;
					setLocation((int) x, (int) y);//設定位置
					System.out.print(x +" " +y);
				} 
				//
				else 
				{
					bounceCounter++; 
					setText("" + bounceCounter);//因為是要輸出字串 所以前面要放一個空字串強致轉型
					if (x < 20 || x > 400 ) //那些數值要多試試...... 
					{
						dx *= -1.0f;  //讓位移植方向相反就會造成反彈
					}
					if (y < 20 || y > 300) //  
					{
						dy *= -1.0f;
					}
				}
			} catch (Exception e) 
			{
				e.printStackTrace();//錯誤訊息
			}
		}
	}
}
