package ce1002.a12.s102502557;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame frame;
	public static void main(String[] args) 
	{
		frame = new JFrame("A12-102502557");
		A12 Listener = new A12();//
		frame.getContentPane().setLayout(null);
		frame.setBounds(300, 300, frame.getInsets().left+frame.getInsets().right+500, frame.getInsets().top+frame.getInsets().bottom+500);
		frame.addKeyListener(Listener);
		frame.setVisible(true);
		frame.setFocusable(true);    //對frame聚焦 這樣一打開就會聚焦在frame 
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//讓x有功能
		MovingButton first = new MovingButton(frame.getWidth(), frame.getHeight()); //設定movingbutton的寬長 太小的話 就只會撞到小的
		frame.add(first); 
	}
	@Override
	public void keyTyped(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		MovingButton other = new MovingButton(frame.getWidth(),frame.getHeight());//設定其他有移動的方塊
		frame.add(other);//別忘記了!!!!!!!!! 
		
	}
	@Override
	public void keyPressed(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}

}
