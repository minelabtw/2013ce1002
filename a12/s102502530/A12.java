package ce1002.a12.s102502530;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.JFrame;

public class A12 implements KeyListener, ComponentListener {
	private static JFrame frame = new JFrame();
	public static void main(String[] args) {
		//create frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		frame.setTitle("A12-102502530");
		frame.getContentPane().setLayout(null);
		
		//OS X full screen
	    try {
	        Class util = Class.forName("com.apple.eawt.FullScreenUtilities");
	        Class params[] = new Class[]{Window.class, Boolean.TYPE};
	        Method method = util.getMethod("setWindowCanFullScreen", params);
	        method.invoke(util, frame, true);
	    } catch (Exception e) {}
	    
	    //open frame
		frame.setVisible(true);
		
		//set minimum size
		Insets insets = frame.getInsets();
		frame.setMinimumSize(new Dimension(50 + insets.left + insets.right, 50 + insets.top + insets.bottom));
		
		//add first button
		MovingButton button = new MovingButton(frame);
		MovingButton.buttons.add(button);
		frame.getContentPane().add(button);
		(new Thread(button)).start();
		
		A12 listener = new A12();
		//add a new button when key pressed
		frame.addKeyListener(listener);
		
		//tell every button when frame is just resized
		frame.addComponentListener(listener);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		//add a new button
		MovingButton button = new MovingButton(frame);
		frame.getContentPane().add(button);
		MovingButton.newButtons.add(button);
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		//reset buttons position
		MovingButton.onFrameResize();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {};
	@Override
	public void keyTyped(KeyEvent arg0) {};
	@Override
	public void componentHidden(ComponentEvent arg0) {};
	@Override
	public void componentMoved(ComponentEvent arg0) {};
	@Override
	public void componentShown(ComponentEvent arg0) {};
}