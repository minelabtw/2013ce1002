package ce1002.a12.s102502530;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MovingButton extends JButton implements Runnable {
	//declare
	public static int width, height;
	public static ArrayList<MovingButton> buttons = new ArrayList<MovingButton>();
	public static ArrayList<MovingButton> newButtons = new ArrayList<MovingButton>();
	private static Random random = new Random();
	private static JFrame frame;
	private static long lastTime;
	private double x, y, dx, dy;
	private int bounceCounter;
	
	public MovingButton(final JFrame frame)
	{
		//set frame
		MovingButton.frame = frame;
		
		//set vector
		double r = random.nextDouble() * Math.PI * 2;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		
		//get frame size
		width = frame.getContentPane().getWidth() - 50;
		height = frame.getContentPane().getHeight() - 50;
		
		//set position
		x = random.nextDouble() * width;
		y = random.nextDouble() * height;
		setBounds((int) x, (int)y, 50, 50);
		
		//set text
		bounceCounter = 0;
		setText(Integer.toString(bounceCounter));
		
		//prevent focus on this button
		addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent arg0) {};
			@Override
			public void focusGained(FocusEvent arg0) {
				frame.requestFocus();
			}
		});
	}
	
	//on frame resize
	public static void onFrameResize() {
		//get frame size
		width = frame.getContentPane().getWidth() - 50;
		height = frame.getContentPane().getHeight() - 50;
		
		//set new position
		for (MovingButton button : buttons) {
			boolean flag = false;
			if (button.x >= width) {
				button.x = random.nextDouble() * width;
				flag = true;
			}
			if (button.y >= height) {
				button.y = random.nextDouble() * height;
				flag = true;
			}
			if(flag)
				button.setBounds((int) button.x, (int) button.y, 50, 50);
		}
	}
	
	public void run() 
	{
		while (true) {
			//sleep
			long time = System.currentTimeMillis();
			try {
				if(17 - time + lastTime > 0)
					Thread.sleep(17 - time + lastTime);
			} catch (Exception e) {}
			lastTime = time;
			
			//add new buttons
			buttons.addAll(newButtons);
			newButtons.clear();
			
			for (MovingButton button : buttons) {
				//check if bounce
				boolean flag = false;
				if (button.x + button.dx >= width || button.x + button.dx < 0) {
					button.dx *= -1;
					flag = true;
				}
				if (button.y + button.dy >= height || button.y + button.dy < 0) {
					button.dy *= -1;
					flag = true;
				}
				
				//then count bounce
				if (flag) {
					++button.bounceCounter;
					button.setText(Integer.toString(button.bounceCounter));
				}
				
				//set new position
				button.setBounds((int)(button.x += button.dx), (int)(button.y += button.dy), 50, 50);
			}
		}
	}
}