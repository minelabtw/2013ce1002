package ce1002.a12.s102502053;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;
	private int bounceCounter = 0;
	Thread thread = new Thread();
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		setText(Integer.toString(bounceCounter));
		setBounds(250, 250, 40, 40);
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		thread.start();
	}
	
	public void run()
	{
		double x = 250;
		double y = 250;
		while(true)
		{
			x = x + dx;
			y = y + dy;
		
			if(x > 300)
			{
				dx = dx * -1;
				x = 300;
				bounceCounter++;
			}else if (x < 0)
			{
				dx = dx * -1;
				x = 0;
				bounceCounter++;
			}
			if(y > 300)
			{
				dy = dy*-1;
				y = 300;
				bounceCounter++;
			}else if(y < 300)
			{
				dy = dy * -1;
				y = 300;
				bounceCounter++;
			}
			
			setLocation((int) x, (int) y);
			setText(Integer.toString(bounceCounter));
			
			try {
				Thread.sleep(7);  
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
		 
		}
	}
}
