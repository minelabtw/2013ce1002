package ce1002.a12.s102502502;

import javax.swing.JButton;
import javax.swing.JFrame;

class MovingButton extends JButton implements Runnable{
    private static final long serialVersionUID = 1L;
    private double dx,dy;            // the vector of x,y
    private int x,y;                 // the position of this button
	private int bounceCounter;       // the time of the bounce of the button
    JFrame f;  
	Thread thread = new Thread(this);    // operation
	public MovingButton(JFrame f)        // use this to get the information of frame
	{
		this.f = f;
		setBounds(x,y,50,50);
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);             // the vector of x
		dy=3*Math.sin(r);             // the vector of y
		thread.start();               // start operation

	}
  public void run() 
  {
	  while (true) {
			x += (int) dx;
			y += (int) dy;
			
			if (x <= f.getInsets().left){
				dx *= -1;
		        x = f.getInsets().left + 50;
				bounceCounter++;
		    }
		   else if ( x >= 500 - f.getInsets().right -50){
				dx *= -1;
				x = 500 + f.getInsets().right - 100;
				bounceCounter++;
			}
			if (y <= f.getInsets().top) {
				dy *= -1;
				y = f.getInsets().top + 50;
				bounceCounter++;
			} else if (y == 500 - f.getInsets().bottom -50) {
				dy *= -1;
				y = 500 + f.getInsets().bottom - 100;
				bounceCounter++;
			}
			this.setLocation(x, y);
			this.setText(String.valueOf(bounceCounter));

			try {
				Thread.sleep(25);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	  }
  }
}
