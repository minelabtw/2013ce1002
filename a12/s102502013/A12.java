package ce1002.a12.s102502013;
import javax.swing.JFrame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class A12 implements KeyListener{

	static JFrame jf;
	public static void main(String[] args) {
		jf = new JFrame("A12-102502013");    
		A12 listener = new A12();
		jf.setBounds(250, 250, 500, 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setResizable(false);
		jf.setLayout(null);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	@Override
	public void keyPressed(KeyEvent e) {

		if(e.getKeyCode() != KeyEvent.KEY_PRESSED)//press any key to create a new button
		{
			MovingButton btn = new MovingButton();
			
			Thread tm = new Thread(btn);
			tm.start();
			jf.add(btn);
			btn.setVisible(true);
		}
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
