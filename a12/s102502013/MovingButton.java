package ce1002.a12.s102502013;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	
    private double dx,dy;
    private double x = 225, y = 225;
	private int bounceCounter;
	
	public MovingButton()
	{
		
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);//the amount of x axis's change
		dy=3*Math.sin(r);//the amount of y axis's change
		bounceCounter = 0;
		setLayout(null);
		setSize(50, 30);
		setLocation((int)this.x, (int)this.y);
		setText(""+bounceCounter);
		
	}
  public void run() 
  {
	  
	  while(true){
		  //delay
		  try{
			  Thread.sleep(40);
		  }catch(Exception e){
			  e.printStackTrace();  
		  }
		  double x, y;
		  x = getLocation().x;
		  y = getLocation().y;
		  //當Button尚未碰到frame之前繼續移動
	    if(x + dx >= 0 && x + dx + 50 <= 500 && y + dy >= 0 && y + dy + 30 <= 470){
	    	x = x + dx;
	    	y = y + dy;
	    	//reset button's location
	    	setLocation((int)x, (int)y);
	    }
	    //當Button碰到frame之後將其原變化量乘以"-1"
	    else{
	    	bounceCounter++;
	    	setText(""+bounceCounter);
	    	if(x + dx<0||x + dx + 50>500){
	    		dx *= -1.0;
	    	}
	    	if(y + dy<0||y + dy + 30>470){
	    		dy *= -1.0;
	    	}
	    }
	  }
  }
  
}
