package ce1002.a12.s101201046;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A12 extends KeyAdapter {
	static JFrame jf;

	static { //initial JFrame
		jf = new JFrame("A12");
		jf.getContentPane().setLayout(null);
		jf.getContentPane().setPreferredSize(new Dimension(500, 500));
		jf.setLocation(300, 300);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
		jf.requestFocusInWindow();
		jf.pack();
	}

	public static void main(String[] args) {
		jf.addKeyListener(new A12()); //set keylistener
	}

	public void keyPressed(KeyEvent e) {
		new Thread(new MovingButton()).start();	//create thread
	}
}
