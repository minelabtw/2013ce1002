package ce1002.a12.s101201046;

import javax.swing.*;
import static java.lang.Math.*;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;

	/*** initial **/
	public MovingButton() {
		double r = random() * PI;
		dx = cos(r) * 3.0;
		dy = sin(r) * 3.0;
		A12.jf.getContentPane().add(this);
		this.setBounds((int)(random()*(getParent().getWidth() - 50)),
					   (int)(random()*(getParent().getHeight() - 50)),
						50, 50);
		this.setText(String.valueOf(bounceCounter));
	}

	//thread run
	public void run() {
		try {
			while(true) {
				Thread.sleep(10);
				setLocation(getX() + (int)dx, getY() + (int)dy);
				
				//wall collision check
				if (getX() < 0 || getX() > (getParent().getWidth() - getWidth())) {
					dx = -dx;
					setText(String.valueOf(++bounceCounter));
				}

				if (getY() < 0 || getY() > (getParent().getHeight() - getHeight())) {
					dy = -dy;
					setText(String.valueOf(++bounceCounter));
				}

			}	
		} catch(InterruptedException e) {
			e.printStackTrace();	
		}	
	}
}
