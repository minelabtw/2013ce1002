package ce1002.a12.s102502018;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
  private double dx,dy;
	private int bounceCounter;
	double x=100;
	private double y=100;
	private int w,h;
	public MovingButton(int h,int w)
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		this.w=w-60;
		this.h=h-80;
		setBounds((int)x,(int)y,50,50);			//設定初始位置
	}
  public void run() 
  {
	  while(true)
	  {
		  this.setSize(50, 50);
		  x=x+dx;
		  y=y+dy;		  
		  this.setText(""+bounceCounter);	//顯示碰撞次數
		  this.setVisible(true);		  
		  if(x>w||x<0||y>h||y<0)		//若撞牆，則換方向
		  {
			  if(x < 0) {
                  x = 0;
                  dx = -dx;
              } else if (x > w) {                  
                  dx = -dx;
                  x=x+dx;
              }
			  if(y < 0) {
                  y = 0;
                  dy = -dy;
              } else if (y > h) {
                  
                  dy = -dy;
                  y = y+dy;
              }
			  bounceCounter++;			//碰撞次數
		  }
		  this.setLocation((int)x,(int)y);	//設置新的位置
		  try {
              Thread.sleep(10); 			
          } catch(InterruptedException e) {
              e.printStackTrace();
          }
	  }
  }
}