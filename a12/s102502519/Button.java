package ce1002.a12.s102502523;

import javax.swing.*;

public class Button extends JButton implements Runnable {

	int height;
	int width;
	int bounce_counter;
	double a;
	double b;
	double da;
	double db;

	Button(int height, int width) {
		this.height =  height - 80;
		this.width =  width - 50;
		a = width / 2;
		b = height / 2;

		setBounds((int) a, (int) b, 50, 50);

		double r = Math.random() * Math.PI * 2;    //angle
		da = 3 * Math.cos(r);
		db = 3 * Math.sin(r);
	}

	public void run() {                 //���|~
		while (true) {
			a += da;
			b += db;

			if (a < 0 || a > width || b < 0 || b > height) {
				if (a < 0) {
					a = -a;
					da = -da;
				} else if (a > width) {
					a = -a + 2 * width;
					da = -da;
				}

				if (b < 0) {
					b = -b;
					db = -db;
				} else if (b > height) {
					b = -b + 2 * height;
					db = -db;
				}
				bounce_counter++;
			}

			setText(bounce_counter + "");
			setLocation((int) a, (int) b);

			try {    //exception
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
