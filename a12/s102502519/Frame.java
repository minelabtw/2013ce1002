package ce1002.a12.s102502523;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;

public class Frame extends JFrame {

	List<JButton> button = new ArrayList<JButton>();

	Frame() {    //frame
		setLayout(null);
		setLocationRelativeTo(null);
		setTitle("A12-102502519");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);

		KeyListener listener = new Key_listener();
		addKeyListener(listener);

		setVisible(true);
	}

	class Key_listener implements KeyListener {

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
		}

		public void keyTyped(KeyEvent e) {    //buttom
			JButton cell = new Button(getHeight(), getWidth());
			button.add(cell);
			add(cell);

			Thread thread = new Thread((Runnable) cell);    //�����
			thread.start();
		}
	}
}
