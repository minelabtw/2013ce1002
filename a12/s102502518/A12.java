package ce1002.a12.s102502518;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import java.awt.event.KeyEvent;
public class A12 implements KeyListener {
	static JFrame jf;
	public static void main(String[] args) {
		A12 listener = new A12();
		jf = new JFrame("A12");
		jf.addKeyListener(listener);
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void keyTyped(KeyEvent arg0) {
		MovingButton b = new MovingButton(); // add button
		jf.add(b);
		Thread t = new Thread(b); // add thread
		t.start();
	}
	public void keyPressed(KeyEvent arg0) {
	}

	public void keyReleased(KeyEvent arg0) {
	}
}
