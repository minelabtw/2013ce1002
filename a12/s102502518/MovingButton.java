package ce1002.a12.s102502518;
import javax.swing.JButton;
public class MovingButton extends JButton implements Runnable {
	private int counter;
	private double x,y,a,b;
	public MovingButton() {
		double r=Math.random()*3.14159;
		x=3*Math.cos(r);
		y=3*Math.sin(r); // set direction
		a=0;
		b=0;
		setBounds((int) a,(int) b,50,50); 
	}
	public void run() {
		for (;;) {
			a+=x;
			b+=y; 
			if (a<0) {
				a=Math.abs(a);
				x=Math.abs(x);
				counter++;
			} 
			else if (b<0) {
				b=Math.abs(b);
				y=Math.abs(y);
				counter++;
			} 
			else if (a>500-50) {
				a-=2*x;
				x*=-1;
				counter++;
			} 
			else if (b>500-50) {
				b-=2*y;
				y*=-1;
				counter++;
			} 
			try {
				Thread.sleep(10);
			} 
			catch (InterruptedException e) {
				e.printStackTrace(); // speed
			}
			setLocation((int) a,(int) b);
			setText(Integer.toString(counter)); // times
		}
	}
}
