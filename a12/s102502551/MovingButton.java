package ce1002.a12.s102502551;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	private int buttonwidth = 45;
	private int buttonheight = 45;
	private int framewid;
	private int frameheight;
	Thread moving = new Thread(this);

	public MovingButton(int x, int y) {
		double r = Math.random() * Math.PI * 2;								//讓她動360度
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		
		setText("" + bounceCounter);
		framewid = x;
		frameheight = y;
		
		int a,b;
		
		a = (framewid - buttonwidth) / 2;
		b = (frameheight - buttonheight) / 2 - 30;
		setBounds(a, b, buttonwidth, buttonheight);							//從中間開始
		moving.start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(30);
				double x = getLocation().x;
				double y = getLocation().y;
				if (x + dx >= 0 && x + dx + buttonwidth + 10 <= framewid && y + dy >= 0 && y + dy + buttonheight + 40 <= frameheight) {			//還沒撞到
					x = x + dx;
					y = y + dy;
					setLocation((int) x, (int) y);
				} 
				else {										//撞到牆
					bounceCounter++;
					setText("" + bounceCounter);
					if (x + dx < 0 || x + dx + buttonwidth + 10 > framewid) {
						dx = dx * -1;
					}
					if (y + dy < 0 || y + dy + buttonheight + 40 > frameheight) {
						dy = dy * -1;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}