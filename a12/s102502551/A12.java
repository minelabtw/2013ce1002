package ce1002.a12.s102502551;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12-102502551");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+600, jf.getInsets().top+jf.getInsets().bottom+600);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton button = new MovingButton(jf.getWidth(), jf.getHeight());			//加按鈕
		jf.add(button);
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton button2 = new MovingButton(jf.getWidth(), jf.getHeight());			//加後面的按鈕
		jf.add(button2);
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}