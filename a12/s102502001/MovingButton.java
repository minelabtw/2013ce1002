package ce1002.a12.s102502001;

import javax.swing.JButton;

public class MovingButton  extends JButton implements Runnable{
	private double dx,dy;
	private int bounceCounter;                  //set the number of the button
	private float speed = (float) 5.0;          //set the speed
	boolean isgo = true;
	public MovingButton ()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setBounds(200,200,50,50);
		
	}
  public void run() 
  {
	  try{
		  int x = 220;                          //set the initial location
		  int y = 5;
		  while(isgo){
			  x += speed * dx;                  //the path of moving
			  y += speed * dy;
			  
			  
			  if(x<0 || x>420){                 //if the button hit the border, it bounces
				  dx=-dx;
				  bounceCounter++;
			  }
			  
			  if(y<0 || y>400){
				  dy=-dy;
				  bounceCounter++;
			  }  
			  setBounds(x,y,50,50);
			  setText(""+ bounceCounter);
			  Thread.sleep(100);		  
		  }
	  }
	  catch(Exception e){
		  e.printStackTrace();
	  }
  }

}
