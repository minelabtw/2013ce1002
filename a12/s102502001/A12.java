package ce1002.a12.s102502001;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;


public class A12 implements KeyListener{
	
	static JFrame jf;
	public static void main(String[] args)
	{	
    jf = new JFrame("A12");
    MovingButton b1 = new MovingButton();            //set a button
    jf.add(b1);                                      //add the button on the frame
	Thread thread = new Thread(b1);	                 //create new thread
	thread.start();
	
	A12 listener = new A12(){
		@Override
		public void keyPressed(KeyEvent e) {
			MovingButton[] balls = new MovingButton[10];
			for(int i = 0; i<10 ; i++){
				MovingButton button = new MovingButton();
				jf.add(button);
				balls[i] = MovingButton(i+1);
				Thread thread = new Thread(button);
				thread.start();
			}
		}
	};
	
	jf.getContentPane().setLayout(null);             //set the frame
	jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
	jf.addKeyListener(listener);
	jf.setVisible(true);
	jf.setFocusable(true);
	jf.setLocationRelativeTo(null);
	jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	protected static MovingButton MovingButton(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}



