package ce1002.a12.s101201506;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class A12 implements KeyListener { // by TA提供
	static JFrame jf;

	public static void main(String[] args) {

		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton MB = new MovingButton(500 - jf.getInsets().left
				- jf.getInsets().right, 500 - jf.getInsets().top
				- jf.getInsets().bottom);
		jf.add(MB);

		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(MB, 0, 1, TimeUnit.MILLISECONDS);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {

	}

	@Override
	public void keyReleased(KeyEvent arg0) {

	}

	@Override
	public void keyTyped(KeyEvent arg0) { // 設定 bound .. 等
		MovingButton movingbutton = new MovingButton(700 - jf.getInsets().left
				- jf.getInsets().right, 700 - jf.getInsets().top
				- jf.getInsets().bottom);
		jf.add(movingbutton);

		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(movingbutton, 0, 1, TimeUnit.MILLISECONDS);

	}

}