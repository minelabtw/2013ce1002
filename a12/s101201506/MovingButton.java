package ce1002.a12.s101201506;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private double x, y, width, height;
	private int bounceCounter;

	public MovingButton(int width, int height) { //設定案鳥 和 Dx Dy  的變化
		this.width = width - 70;
		this.height = height - 70;
		double r = Math.random() * 3.1415926; // 圓周率
		dx = Math.cos(r) / 5;
		dy = Math.sin(r) / 5;
		x = width / 2 - 35;
		y = height / 2 - 35;
		setBounds(width / 2 - 35, height / 2 - 35, 70, 70); 

		bounceCounter = 0;
		setLabel(Integer.toString(bounceCounter));
	}

	public void run() { //位移!!!!

		x = x + dx;
		y = y + dy;
		if (x < 0) { //當x y 為負時 改變正負
			x = (-1) * x;
			dx = (-1) * dx;
			bounceCounter++;
		} else if (x > width) { 
			x = 2 * width - x;
			dx = (-1) * dx;
			bounceCounter++;
		}
		if (y < 0) { 
			y = (-1) * y;
			dy = (-1) * dy;
			bounceCounter++;
		} else if (y > height) { 
			y = 2 * height - y;
			dy = (-1) * dy;
			bounceCounter++;
		}
		setLocation((int) x, (int) y);
		setLabel(Integer.toString(bounceCounter));
	}
}
