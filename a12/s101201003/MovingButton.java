package ce1002.a12.s101201003;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy;
    private double d []=new double [2];//dx,dy
	private int bounceCounter;
	private int s[]=new int [2];//side
	private double l[]=new double[2];//location
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		d[0]=dx=3*Math.cos(r);
		d[1]=dy=3*Math.sin(r);
		bounceCounter=0;
	}	
	public MovingButton(int a,int b)
	{
		s[0]=a;
		s[1]=b;
		String label=Integer.toString(bounceCounter);
		super.setBounds(a-60,b-40,120,80);
		l[0]=a-60;
		l[1]=b-40;
		setLabel(label);
	}
	
  public void run() 
  {
	  for (int i=0;i<2;i++)
		{
			l[i]=l[i]+d[i];
			if (l[i]<0)
			{
				l[i]=l[i]*(-1);
				d[i]=d[i]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
			}
			else if (i==0 && l[i]>s[0]-60)
			{
				l[0]=(l[0]-s[0]+60)*(-1)+s[0]-60;
				d[0]=d[0]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
			}
			else if (i==1 && l[i]>s[1]-40)
			{
				l[1]=(l[1]-s[1]+40)*(-1)+s[1]-40;
				d[1]=d[1]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);			}
			}		
		super.setLocation((int)l[0], (int)l[1]);
    
  }
}
