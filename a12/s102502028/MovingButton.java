package ce1002.a12.s102502028;

import javax.swing.*;
import java.awt.*;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter;
	double x ;
	double y ;
	Thread moving = new Thread(this); //宣告

	public MovingButton(double x, double y) {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		this.x = x;
		this.y = y;
		setText("" + bounceCounter);
		setBounds((int)x,(int) y, 50, 50); //設定按鈕位置和大小
		moving.start(); //call Thread 的 start 函數
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(10); //延遲25ms
				double x = getLocation().x; //取得x座標
				double y = getLocation().y; //取得y座標
				//設定彈跳範圍
				if (x + dx >= 0 && x + dx + 50 <= 485 && y + dy >= 0
						&& y + dy + 50  <= 465) {
					x += dx;
					y += dy;
					setLocation((int) x, (int) y);
				} 
				//如果按鈕碰到了邊界
				else {
					bounceCounter++; 
					setText("" + bounceCounter);
					if (x + dx < 0 || x + dx + 50  > 485) {
						dx *= -1.0f;
					}
					if (y + dy < 0 || y + dy + 50  > 465) {
						dy *= -1.0f;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		
	}
}
