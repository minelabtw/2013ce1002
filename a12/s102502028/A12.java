package ce1002.a12.s102502028;
import javax.swing.* ;

import java.awt.* ;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12"); //建立框架
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);		
		jf.setFocusable(true);
		jf.setLocationRelativeTo(null) ;
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton first = new MovingButton(135,135); //宣告
		jf.add(first); //把Button加到視窗裡
		
		jf.setVisible(true);
	}
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		MovingButton other = new MovingButton(135, 135); //宣告
		jf.add(other); //把Button加到視窗裡
	}
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
