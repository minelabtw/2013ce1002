package ce1002.a12.s102502024;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	private double dx,dy;
	private double x=70;
	private double y=70;
	private int bounceCounter=0;  //碰撞次數
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);  //向量
		dy=3*Math.sin(r);
		setBounds(100,100,(int)x,(int)y);
	}
	@Override
	public void run() {
		while(true)
		{
		x=x+dx;  //位置
		y=y+dy;
		if(x<0 ||x>300 ||y<0 ||y>300)
		{
			if(x<0 || x>500)
			{
				dx=-dx;  //邊界反彈
			}
			if(y<0 || y>500)
			{
				dy=-dy;  //邊界反彈
			}
			bounceCounter++;
		}
		setText(bounceCounter+"");
		setLocation((int)x,(int)y);
		try {
			                  Thread.sleep(10);  //設定延遲 
			              } catch(InterruptedException e) {
			                  e.printStackTrace();
		                  }
		}
	}
}
