package ce1002.a12.s102502024;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		jf = new JFrame("A12");
		A12 listener = new A12();  //設定聆聽者
		jf.setLayout(null);
		jf.setBounds(300,100,600,600);
		jf.addKeyListener(listener);  //設定聆聽者
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {  //設定按鍵事件
		MovingButton movingButton=new MovingButton();
	    jf.add(movingButton);
		Thread thread = new Thread(movingButton);
		thread.start();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}

}
