package ce1002.a12.s101201504;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {

	double x, y;
	private double dx, dy;
	private int width, height;
	private int bounceCounter;

	public MovingButton(int w, int h) {// construction
		double r = Math.random() * 3.14159;
		dx = Math.cos(r) / 5;
		dy = Math.sin(r) / 5;
		width = w - 60;
		height = h - 40;
		bounceCounter = 0;
		x = w / 2 - 30;
		y = h / 2 - 20;
		super.setLabel(Integer.toString(bounceCounter));
		super.setBounds((int) x, (int) y, 60, 40);
	}

	@Override
	public void run() {
		// judge the direction of rebound
		x = x + dx;
		y = y + dy;
		if (x < 0) {// exception on x
			x = x * (-1);
			dx = dx * (-1);
			bounceCounter++;
		} else if (x > width) {
			x = 2 * width - x;
			dx = dx * (-1);
			bounceCounter++;
		}
		if (y < 0) {// exception y
			y = y * (-1);
			dy = dy * (-1);
			bounceCounter++;
		} else if (y > height) {
			y = 2 * height - y;
			dy = dy * (-1);
			bounceCounter++;
		}

		// set data
		super.setLocation((int) x, (int) y);
		super.setLabel(Integer.toString(bounceCounter));
	}
}
