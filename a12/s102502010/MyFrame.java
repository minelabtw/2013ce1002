package ce1002.a12.s102502010;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;

class MyFrame extends JFrame {

    List<JButton> button = new ArrayList<JButton>();

    MyFrame()
    {
    	setTitle("A12-102502010");
		setLayout(null);//浮動預設
		setBounds(200,200,800,600);//視窗大小
		setVisible(true);//顯示
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        
        KeyListener listener = new Key_listener();
        addKeyListener(listener);
    }

  
    class Key_listener implements KeyListener //setbutton
    {
        public void keyTyped(KeyEvent e) 
        {
            JButton square = new Move(getHeight(), getWidth());
            button.add(square);
            add(square);

            Thread thread = new Thread((Runnable)square);
            thread.start();
        }
        
        public void keyPressed(KeyEvent e){}
        public void keyReleased(KeyEvent e){}
    }
}