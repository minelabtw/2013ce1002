package ce1002.a12.s102502010;

import javax.swing.*;

public class Move extends JButton implements Runnable {
    int height;
    int width;
    int time;
    int x;
    int y;
    double dx;
    double dy;
    double r;

    
    Move(int height, int width)
    {
        this.height = height-120;
        this.width = width-100;
        x = width / 2;
        y = height / 2;
        r = Math.random()*3.14*2;
        dx = Math.cos(r) * 2;
        dy = Math.sin(r) * 2;
        setBounds(x, y, 100, 100);
    }

    public void run()  //move
    {  
        while(true)
        {
            x+=dx;
            y+=dy;

            if(x<0 || x>width)
            {
                if(x < 0)
                {
                    x = -x;
                    dx = -dx;
                } 
                if (x > width)
                {
                    x = width;
                    dx = -dx;
                }
                time++;
            }
            if(y<0 || y>height)
            {
            	if(y < 0)
                {
                    y = -y;
                    dy = -dy;
                } 
                if (y > height)
                {
                    y = height;
                    dy = -dy;
                }
                time++;
            }
            
            setText(time+"");  //word
            setLocation(x, y);

            try
            {
                Thread.sleep(8); 
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}