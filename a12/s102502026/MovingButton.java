package ce1002.a12.s102502026;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	Thread thread = new Thread(this);

	public MovingButton() {
		this.setBounds(0, 0, 50, 50); // bounds of the buttons
		this.setText(String.valueOf(bounceCounter)); // text of the
														// button(counter)
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		thread.start();
	}

	public void run() {
		int a = 1;
		int b = 1;
		while (true) {
			try {
				Thread.sleep(5); // speed of the buttons
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// movement of the button
			a = a - (int) dx;
			b = b - (int) dy;
			if (a < 0) {
				dx = dx * (-1); // to move to the other side
				a = 0;
				bounceCounter = bounceCounter + 1;
			} else if (a > 440) {
				dx = dx * (-1); // to move to the other side
				a = 440;
				bounceCounter = bounceCounter + 1;
			}
			if (b < 0) {
				dy = dy * (-1); // to move to the other side
				b = 0;
				bounceCounter = bounceCounter + 1;
			} else if (b > 420) {
				dy = dy * (-1); // to move to the other side
				b = 410;	
				bounceCounter = bounceCounter + 1;
			}
			this.setLocation(a, b);		//button movement
			this.setText(String.valueOf(bounceCounter));	//add counter
		}

	}

}
