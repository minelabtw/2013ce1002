package ce1002.a12.s102502506;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	private static MovingButton a = new MovingButton();

	public static void main(String[] args) {
		jf = new JFrame("A12");
		A12 L = new A12();
		jf.getContentPane().setLayout(null);
		jf.setSize(800, 800);
		jf.addKeyListener(L);
		jf.add(a);  //第一個Button
		new Thread(a).start();  //第一個Button開始動
		jf.setLayout(null);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setResizable(false);
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(3);
	}

	@Override
	public void keyPressed(KeyEvent e) {  
		MovingButton a = new MovingButton();  //按下按鍵就會做一個新的Button
		jf.add(a);  
		new Thread(a).start();  //Button開始移動 
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}