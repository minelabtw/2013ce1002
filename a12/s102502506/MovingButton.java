package ce1002.a12.s102502506;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	private double x = 300;
	private double y = 300;

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		this.setSize(80, 80);
		this.setText(Integer.toString(bounceCounter));
	}

	public void run() {
		while (true) {  //判斷撞擊邊界
			if (y > 700 || x > 715 || x < 0 || y < 0) {  
				if (y > 700 || y < 0) {
					dy = -dy;
					bounceCounter++;
				}
				if (x > 715 || x < 0) {
					dx = -dx;
					bounceCounter++;
				}
				this.setText(Integer.toString(bounceCounter));  
			}
			x = x + dx;
			y = y + dy;
			this.setLocation((int) x, (int) y);
			try {  //每10/1000秒動一次
				Thread.sleep(10);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}