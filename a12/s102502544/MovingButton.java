package ce1002.a12.s102502544;

import java.util.TimerTask;

import javax.management.timer.Timer;
import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	private double dx,dy;  //向量
	private int bounceCounter;  //按鈕中間的數字
	private int x=200;
	private int y=200;  //位置
	
	public MovingButton(){
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setText(""+bounceCounter);
		setBounds(x,y,50,50);
		
	}
	public void run(){
		java.util.Timer timer = new java.util.Timer(true);  
		Running running = new Running();
		timer.schedule(running, 500, 15);
	}
	class Running extends TimerTask{
		public void run() {
			// TODO Auto-generated method stub
			if( (x+dx) <= 0 || (x+dx+65) >= 500 ){
				dx = -dx;
				bounceCounter++;
				setText(""+bounceCounter);
			}
			if( (y+dy) <= 0 || (y+dy+80) >= 500 ){
				dy = -dy;
				bounceCounter++;
				setText(""+bounceCounter);
			}
			x += dx; //移動
			y += dy;
			setLocation(x,y);
			}
		}
}
