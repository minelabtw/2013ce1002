package ce1002.a12.s102502544;

import javax.swing.JFrame;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, 500, 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton b1 = new MovingButton();
		Thread t1 = new Thread(b1);
		t1.start();
		jf.add(b1);
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton b2 = new MovingButton();
		Thread t2 = new Thread(b2);
		t2.start();
		jf.add(b2);
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
