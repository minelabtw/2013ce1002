package ce1002.a12.s102502534;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		jf = new JFrame("A12");
		
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		A12 listener = new A12();
		jf.addKeyListener(listener);
		
		MovingButton mb =new MovingButton(jf.getWidth(), jf.getHeight());
		jf.add(mb);//把MovingButton加到視窗
		
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		MovingButton mb1 =new MovingButton(jf.getWidth(), jf.getHeight());
		jf.add(mb1);//把MovingButton加到視窗
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
