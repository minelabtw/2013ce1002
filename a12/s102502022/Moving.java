package ce1002.a12.s102502022;

import java.awt.*;
import javax.swing.*;

public class Moving extends JButton implements Runnable{

	 int height;
	 int width;
	 int bounce_counter;
	 double x;
	 double y;
	 double distancex;
	 double distancey;
	
	 Moving(int height, int width) {
	        this.height = height - 80;
	        this.width = width - 50;
	        x = width / 2;//將傳入的width的值除以2
	        y = height / 2;
            setBounds((int)x, (int)y, 50, 50);
            double r = Math.random() * Math.PI * 2;
	        distancex = 3 * Math.cos(r);
	        distancey = 3 * Math.sin(r);
	 }

	 
	 
	 @Override
	 public void run() {
		// TODO Auto-generated method stub
		 while(true) {
	            x += distancex;
	            y += distancey;

	            if(x<0 || x>width || y<0 || y>height) {
	                if(y < 0) {
	                    y = -y;
	                    distancey = -distancey;
	                } else if (y > height) {
	                    y = -y +2* height;
	                    distancey = -distancey;
	                }
	                if(x < 0) {
	                    x = -x;
	                    distancex = -distancex;
	                } else if (x > width) {
	                    x = -x + 2* width;
	                    distancex = -distancex;
	                }
	                bounce_counter++;
	            }

	            setText(bounce_counter+"");
	            setLocation((int)x, (int)y);

	            try {
	                Thread.sleep(10); //執行緒
	            } catch(InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	 }

}
