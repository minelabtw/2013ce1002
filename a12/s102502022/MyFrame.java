package ce1002.a12.s102502022;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;


public class MyFrame extends JFrame{
	
	List<JButton> cells = new ArrayList<JButton>();

    MyFrame() {
    	setTitle("A12-102502022");//標題
        setSize(800, 600);//視窗大小
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//視窗關閉
        setLocationRelativeTo(null);
        setLayout(null);//排版設置
        KeyListener listener = new Keylistener();//呼叫函式
        addKeyListener(listener);//加入
        setVisible(true);
    }

  
    class Keylistener implements KeyListener {

        public void keyPressed(KeyEvent e) {
        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyTyped(KeyEvent e) {
            JButton cell = new Moving(getHeight(), getWidth());//加入jbutton
            cells.add(cell);
            add(cell);//加入

            Thread thread = new Thread((Runnable)cell);//執行緒
            thread.start();
        }
    }
	

}
