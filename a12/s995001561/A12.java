package ce1002.a12.s995001561;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

@SuppressWarnings("serial")
public class A12 extends JFrame {
    public JButton btn;
    public final int btnSideLen = 60;
    public double btnX, btnY, dx, dy;
    public int bounceCounter;

    public A12() {
        this.setTitle("A12");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(650, 650);
        this.setResizable(false);
        this.setLayout(null);
        this.setLocationRelativeTo(null);

        btnX = getWidth() / 2.2;
        btnY = getHeight() / 2.2;
        btn = new JButton("" + bounceCounter);
        btn.setBounds((int) btnX, (int) btnY, btnSideLen, btnSideLen);

        btn.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {           	
                double r = Math.random()*3.14159;
                dx = 3*Math.cos(r);
                dy = 3*Math.sin(r);
            }
        });

        this.add(btn);
        this.setVisible(true);
        Timer timer = new Timer();
        timer.schedule(new RunningButton(), 1000, 20);
    }

    class RunningButton extends TimerTask {
        public void run() {
            boolean crash = false;
            if (btnX + dx < 0) {
                dx = -dx;
                crash = true;
            }
            if (btnX + dx + btnSideLen >= getWidth()-getInsets().left-getInsets().right) {
                dx = -dx;
                crash = true;
            }
            if (btnY + dy < 0) {
                dy = -dy;
                crash = true;
            }
            if (btnY + dy + btnSideLen >= getHeight()-getInsets().bottom-getInsets().top) {
                dy = -dy;
                crash = true;
            }
            if (crash) {
                bounceCounter++;
                btn.setText("" + bounceCounter);
            }
            btnX += dx;
            btnY += dy;
            btn.setBounds((int) btnX, (int) btnY, btnSideLen, btnSideLen);
            // repaint();
        }
    }

    public static void main(String[] args) {
        new A12();
    }
}
