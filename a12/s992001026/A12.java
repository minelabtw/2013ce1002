package ce1002.a12.s992001026;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pushButton();
	}

	// create a thread and insert a button to JFrame
	private static void pushButton() {
		MovingButton jb = new MovingButton();
		jf.add(jb);
		int x = (int) ( (jb.getParent().getWidth() - 50) * Math.random());
		int y = (int) ( (jb.getParent().getHeight() - 50) * Math.random());
		jb.setBounds(x, y, 50, 50);
		jb.refreshPostintion();
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		pushButton();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
