package ce1002.a12.s992001026;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class MovingButton extends JButton implements Runnable {

	private double dx, dy;
	private int bounceCounter;

	private double simulateX; //simulate X at coutinuous plane
	private double simulateY; //simulate Y at coutinuous plane
	
	public Thread currentThread;

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r) ;
		dy = 3 * Math.sin(r) ;
		bounceCounter = 0;
		this.setText("" + bounceCounter);
		
		currentThread = new Thread(this);
	}
	
	public void refreshPostintion(){
		simulateX = this.getX();
		simulateY = this.getY();
		currentThread.start();
	}
	
	public void run() {
		
		while(true){
			try {
				Thread.sleep(750/60);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			double expectX = simulateX + dx;
			double expectY = simulateY + dy;
			
			boolean counterFlag = false;
			//x out of bound
			if( expectX < 0 || expectX + this.getWidth() > this.getParent().getWidth() ){
				dx = -dx;
				expectX = simulateX + dx;
				counterFlag = true;
			}
			//y out of bound
			if( expectY < 0 || expectY + this.getHeight() > this.getParent().getHeight() ){
				dy = -dy;
				expectY = simulateY + dy;
				counterFlag = true;
			}
			if( counterFlag ){
				++bounceCounter;
				this.setText("" + bounceCounter);
			}
			simulateX = expectX;
			simulateY = expectY;
			
			this.setBounds( (int)simulateX, (int)simulateY, this.getWidth(), this.getHeight() );
			
		}
	}
}
