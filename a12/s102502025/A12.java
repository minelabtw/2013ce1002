package ce1002.a12.s102502025;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;//open frame

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// add frame
		jf = new JFrame("A12");//name frame
		A12 listener = new A12();//set keyboard
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,//set size
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(new MovingButton());
	}

	// auto appears
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		jf.add(new MovingButton());
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}
}
