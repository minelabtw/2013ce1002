package ce1002.a12.s102502025;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	Thread thread = new Thread(this);

	public MovingButton() {
		this.setBounds(100, 100, 75, 75); // bounds of the buttons
		this.setText(String.valueOf(bounceCounter)); // text of the
														// button(counter)
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		thread.start();
	}

	public void run() {//run start
		int x = 100;
		int y = 100;
		while (true) {
			try {
				Thread.sleep(7); // speed of the buttons
			} catch (InterruptedException e) {
				e.printStackTrace();
			}// movement of the button
			x = x - (int) dx;
			y = y - (int) dy;
			if (x < 0) {
				dx = dx * (-1); // to move to the other side
				x = 0;
				bounceCounter = bounceCounter + 1;
			} else if (x > 410) {
				dx = dx * (-1); // to move to the other side
				x = 410;
				bounceCounter = bounceCounter + 1;
			}
			if (y < 0) {
				dy = dy * (-1); // to move to the other side
				y = 0;
				bounceCounter = bounceCounter + 1;
			} else if (y > 400) {
				dy = dy * (-1); // to move to the other side
				y = 400;
				bounceCounter = bounceCounter + 1;
			}
			this.setLocation(x, y); // button movement
			this.setText(String.valueOf(bounceCounter)); // add counter
		}

	}

}
