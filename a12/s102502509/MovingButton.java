package ce1002.a12.s102502509;

import java.awt.Graphics;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{ 
	double dx,dy; 
	int dnx = 220; 
	int dny = 190;
	int size = 60; 
	private int bounceCounter; 

	public MovingButton()
	{
		double r = Math.random()*3.14159; 
		dx=3*Math.cos(r); 
		dy=3*Math.sin(r); 
		this.setText("" + bounceCounter); 
		this.setBounds(dnx,dny,size,size); 
	}	
	 
	public void run(){
		Timer timer = new Timer(); //timer
		timer.schedule(new RunButton(), 0, 15); //timer 1.timertask 2.delay time 3.each motion delay time 
	}
	
	 class RunButton extends TimerTask{ //extends timertask

		@Override
		public void run() {
            if (dnx + dx < 0 || dnx + dx + size >= 500) 
            { //when touch the border
                dx = -dx; //turn the way
                bounceCounter++; //time of touch +1
                setText("" + bounceCounter); // display the time
            }
            if (dny + dy < 0 || dny + dy + size >= 470) { 
                dy = -dy; 
                bounceCounter++; 
                setText("" + bounceCounter); 
            }
            dnx += dx; 
            dny += dy;
            setBounds((int) dnx, (int) dny, size, size); 		
		}	
	 }
}
