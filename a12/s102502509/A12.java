package ce1002.a12.s102502509;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener// remember to realize
{
	static JFrame jf;
	
	public static void main(String[] args) 
	{
		A12 listener = new A12();
		
		jf = new JFrame("A12"); // name the frame
		jf.getContentPane().setLayout(null);// null the layout
		
		jf.setBounds(300, 300, 
				     jf.getInsets().left+jf.getInsets().right+500, 
				     jf.getInsets().top+jf.getInsets().bottom+500);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.addKeyListener(listener);
		// button action
		MovingButton mbt = new MovingButton();
		Thread th = new Thread(mbt);
		th.start();
		jf.add(mbt);
		
	}
	// pressed action
	@Override
	public void keyPressed(KeyEvent arg0) {
		MovingButton mbt = new MovingButton();
		Thread th1 = new Thread(mbt);
		th1.start();
		jf.add(mbt);
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
	
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
}
