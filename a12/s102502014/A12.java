package ce1002.a12.s102502014;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	private static MovingButton a = new MovingButton();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setSize(500, 525);
		jf.addKeyListener(listener);
		jf.add(a);
		new Thread(a).start(); // 開始移動
		jf.setLayout(null);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
    //複寫方法
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton a = new MovingButton();
		jf.add(a);
		new Thread(a).start(); // 開始移動
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}
}