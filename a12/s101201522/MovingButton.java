package ce1002.a12.s101201522;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter;//counter
	
	MovingButton () {//initial
		double r = Math.random()*Math.PI;//degree
		bounceCounter = 0;
		setText("0");
		dx = 3*Math.cos(r);
		dy = 3*Math.sin(r);
	}
	
	@Override
	public void run() {//motion of button
		try {
			while (true) {
				setLocation((int)(getX()+dx), (int)(getY()+dy));
				if (getX()<0 || getX()+getWidth()>getParent().getWidth()) {//check x bounce
					dx *= -1;
					this.setText(String.valueOf(++bounceCounter));
				}
				if(getY()<0 || getY()+getHeight()>getParent().getHeight()) {//check y bounce
					dy *= -1;
					this.setText(String.valueOf(++bounceCounter));
				}
				getParent().repaint();
				Thread.sleep(10);//speed
			}
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
