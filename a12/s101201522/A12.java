package ce1002.a12.s101201522;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;

public class A12 implements KeyListener {
	static JFrame jf;
	
	public static void main(String[] args) {
		/*set frame*/
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent e) {//motion of pressing any key
		MovingButton mb = new MovingButton();
		mb.setBounds((int)(Math.random()*(jf.getContentPane().getWidth()-50)), (int)(Math.random()*(jf.getContentPane().getHeight()-50)), 50, 50);
		jf.add(mb);
		Thread thread = new Thread(mb);
		thread.start();//start move
	}

	@Override
	public void keyReleased(KeyEvent e) {		
	}

	@Override
	public void keyTyped(KeyEvent e) {		
	}

}
