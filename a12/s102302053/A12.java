package ce1002.a12.s102302053;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args){
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addButton();//建立初始按鈕
	}

	public static void addButton(){//放入按鈕,並建立執行旭讓他不斷移動
		jf.repaint();
		MovingButton nmb = new MovingButton();
		jf.add(nmb);
		Thread t = new Thread(nmb);
		t.start();
		
	}
	@Override
	public void keyPressed(KeyEvent e) {//按下任意建會加入新的按鈕
		addButton();
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
			
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	
}