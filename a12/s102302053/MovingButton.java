package ce1002.a12.s102302053;

import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy,x = Math.random()*300,y =Math.random()*300;
    private int bounceCounter = 0;
    String bc = String.valueOf(bounceCounter); 
    public MovingButton(){
    	double r = Math.random()*3.14159;
        dx=3*Math.cos(r);
        dy=3*Math.sin(r);
    	this.setText(bc);
    	this.setBounds((int)x, (int)y, 50, 50);
       }

    public void run(){//執行緒裡藉由timer不斷移動按鈕
    		Timer timer = new Timer();
            timer.schedule(new Moving(), 50, 20);
    }
    class Moving extends TimerTask{//按鈕的移動模式:碰到邊框反彈,按鈕上數字會+1
		@Override
		public void run() {
			if(x >= 435  || x <= 0){//橫向的邊界
				dx = (dx*-1);
				dy = (dy*-1);
				bounceCounter++;
				bc = String.valueOf(bounceCounter); 
				
			}
			if(y>=420 || y <= 0){//縱向的邊界
				dx = (dx*-1);
				dy = (dy*-1);
				bounceCounter++;
				bc = String.valueOf(bounceCounter); 
			}
			x += dx;//橫向移動
    		y += dy;//縱向移動
    		setBounds((int)x, (int)y, 50, 50);
    		setText(bc);
    		
		}
    	
    }
}