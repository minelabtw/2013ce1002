package ce1002.a12.s102502512;
import java.util.*;
import javax.swing.*;
import java.awt.event.*;

class MyFrame extends JFrame {

    List<JButton> cells = new ArrayList<JButton>();

    //config this frame
    MyFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//Line 12-16: create a frame
        setLocation(0,0);
        setLayout(null);
        setTitle("A12-102502512");
        setSize(840, 600);

        KeyListener listener = new Key_listener();
        addKeyListener(listener);

        setVisible(true);
    }

  
    class Key_listener implements KeyListener {

        public void keyPressed(KeyEvent e) {
        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyTyped(KeyEvent e) {
            JButton cell = new Move(getHeight(), getWidth());		//set the cell
            cell.setSize(60,60);
            cells.add(cell);
            add(cell);

            Thread thread = new Thread((Runnable)cell);  //Let cells run
            thread.start();
        }
    }
}