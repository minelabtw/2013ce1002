package ce1002.a12.s102502512;

import javax.swing.*;

public class Move extends JButton implements Runnable {
    int height;
    int width;
    int bounce_counter;
    double x;
    double y;
    double dx;
    double dy;

    
    Move(int height, int width) {				//set the direction of the button
        this.height = height - 80;
        this.width = width - 50;
        x = width / 2;
        y = height / 2;

        setBounds((int)x, (int)y, 50, 50);

        double r = Math.random() * Math.PI * 2;
        dx = 3 * Math.cos(r);
        dy = 3 * Math.sin(r);
    }

   //To move
    public void run() {
        while(true) {
            x += dx;
            y += dy;

            if(x<0 || x>width || y<0 || y>height) {		//if the button meet the bounds turn back
                if(x < 0) {
                    x = -x;
                    dx = -dx;
                } else if (x > width) {
                    x = -x + 2* width;
                    dx = -dx;
                }

                if(y < 0) {
                    y = -y;
                    dy = -dy;
                } else if (y > height) {
                    y = -y +2* height;
                    dy = -dy;
                }
                bounce_counter++;
            }

            setText(""+bounce_counter);//let the number write on the button
            setLocation((int)x, (int)y);

            try {							//set the speed of the button
                Thread.sleep(5); 
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}