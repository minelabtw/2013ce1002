package ce1002.a12.s995002046;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;


public class A12 implements KeyListener
{
	A12(){
		
	}
	static JFrame jf;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");//set title
		A12 listener = new A12();
		//set frame
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 100, 500, 500);//set frame location and width and height
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//create a moving button and frame adds it
		MovingButton m = new MovingButton(50,50);
		jf.add(m);
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		//click any key create a random x and y button
		int x = (int) (Math.random()*400);
		int y = (int) (Math.random()*400);
		MovingButton m = new MovingButton(x,y);
		jf.add(m);
	}
 
  
}