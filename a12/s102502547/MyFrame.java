package ce1002.a12.s102502547;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;

public class MyFrame extends JFrame {

	List<JButton> a = new ArrayList<JButton>();

	MyFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setBounds(300, 300, getInsets().left + getInsets().right + 500,
				getInsets().top + getInsets().bottom + 500);

		KeyListener listener = new Key_listener();
		addKeyListener(listener);

		setVisible(true);
	}

	// ������
	class Key_listener implements KeyListener {

		public void keyPressed(KeyEvent e) {
		}

		public void keyReleased(KeyEvent e) {
		}

		public void keyTyped(KeyEvent e) {
			JButton cell = new Button(getWidth(), getHeight());
			a.add(cell);
			add(cell);

			Thread thread = new Thread((Runnable) cell);
			thread.start();
		}
	}
}