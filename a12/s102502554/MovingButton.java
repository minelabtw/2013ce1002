package ce1002.a12.s102502554;

import javax.swing.*;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy,r;
	private int bounceCounter,x,y;
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);//the way it move along
		setBounds(x,y,50,50);//set the button which appear in the middle. The button is 50*50. 
		x= 225;
		y= 225;//the location of the button
		setText(""+ bounceCounter);//display times which button bounce at the bound
		
	}
    public void run() 
    {
    	while(true){
    		x += (int)dx;
        	y += (int)dy;//the location is change
        	
        	
        	if(x > 435 || x < 0){
        		dx = - dx;
        		bounceCounter ++;
        	}else if(y > 415 || y < 0){
        		dy = -dy;
        		bounceCounter ++;
        	}//while the button touch the bound it will change the way it move and counter plus one
        	
        	setLocation(x,y);//reset the location
        	setText("" + bounceCounter);//display the counter again
        	
        	try {
                Thread.sleep(10);//that the button move more slowly 
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        	
        	repaint();//repaint the button
    	}

    }
    
}
