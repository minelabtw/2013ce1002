package ce1002.a12.s102502554;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		jf = new JFrame("A12-102502554");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener( listener);//add the motion of keyboard
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
		
		MovingButton b = new MovingButton(); 
		jf.add(b);  
		Thread thread1 = new Thread((Runnable)b);
        thread1.start();// we can have one button without press any key
		
		
	}
	


	@Override
	public void keyPressed(KeyEvent e) {//set the function of pressing any key
		
		MovingButton btn = new MovingButton(); 
		jf.add(btn);  // add button
		Thread thread = new Thread((Runnable)btn);
        thread.start();//let it run
	}


	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}
 
  
}
