package ce1002.a12.s120502517;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	static int numberofbutton = 1; //宣告按鈕數量
	static MovingButton[] button;
	static Thread[] thread;

	public static void main(String[] args) {
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		addButton(numberofbutton);
	}

	private static void addButton(int numberofbutton) {
		button = new MovingButton[numberofbutton];
		thread = new Thread[numberofbutton];
		button[numberofbutton - 1] = new MovingButton();
		thread[numberofbutton - 1] = new Thread(button[numberofbutton - 1]);
		thread[numberofbutton - 1].start();
		jf.add(button[numberofbutton - 1]);
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
		addButton(++numberofbutton); //新增按鈕
	}
}
