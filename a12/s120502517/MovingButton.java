package ce1002.a12.s120502517;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;

	public MovingButton() {
		setBounds((int) (Math.random() * 450), (int) (Math.random() * 450), 50,
				50); //隨機設定位置
		setText("" +bounceCounter); //設定顯示文字
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r); //設定向量
		dy = 3 * Math.sin(r); //設定向量
	}

	public void run() {
		Timer time = new Timer(10, new ActionListener() { //宣告Timer
			public void actionPerformed(ActionEvent e) {
				if (getX() <= 0 || getX() >= 450) //若接觸到邊界
				{
					dx *= -1;
					bounceCounter++;
				}
				if (getY() <= 0 || getY() >= 410) //若接觸到邊界
				{
					dy *= -1;
					bounceCounter++;
				}
				setBounds((int) (getX() + dx), (int) (getY() + dy), 50, 50); //移動
				setText("" +bounceCounter); //設定顯示文字
			}
		});
		time.start();
	}
}
