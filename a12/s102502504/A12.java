package ce1002.a12.s102502504;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12-102502504"); //frame的名稱
		A12 listener = new A12(); //創立a12的物件，這樣下面才能addKeyListener
		jf.getContentPane().setLayout(null); 
		jf.setBounds(300,200,500,500); //設立大小
		jf.setVisible(true); //設為可見
		jf.setFocusable(true); //要鎖定住
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton btn = new MovingButton(); //先創第一個button
		jf.add(btn);
		jf.addKeyListener(listener); //整個frame加入keyListener
		
		
		Thread thread = new Thread(btn); //括弧裡面放的是一個runnable的東西
		thread.start();
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) //按一下就可以產生一個button
	{	
		MovingButton btn = new MovingButton(); 
		Thread thread = new Thread(btn);
		jf.add(btn);
		thread.start();
	}
	
	@Override
	public void keyReleased(KeyEvent e) 
	{
		// TODO Auto-generated method stub	
	}
	@Override
	public void keyTyped(KeyEvent e) 
	{
		// TODO Auto-generated method stub
	}
 
}
	