package ce1002.a12.s102502504;

import java.util.Random;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy; //類似向量的東西，用來決定方向
	private int bounceCounter=1; //計數器
	
	public MovingButton()
	{
		double r = Math.random()*3.14159; //隨機決定方向
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
	}
	
	@Override
	public void run() 
	{
		int x =220; //一開始的位置
		int y =200;
		while(true)
		{
			try 
			{
				x = x + (int)dx; //移動後的位置取代x
				y = y + (int)dy;
				
				if(x<0||x>430) //碰到邊界的時候要改變彈跳的方向，且計數器加1
				{  
		            dx=-dx;
		            bounceCounter++;
		        }  
		        if(y<0||y>420)  
		        {  
		            dy=-dy;  
		            bounceCounter++;
		        }  
		        
				Thread.sleep(10); //彈跳的速度
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			
	        setBounds(x, y, 50, 50); 
	        setText(""+bounceCounter);
		}
		
	}

}
			 
		 
	

