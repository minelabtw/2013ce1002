package ce1002.a12.s102502039;

import java.awt.event.*;

import javax.security.auth.x500.X500Principal;
import javax.swing.*;

public class MovingButton extends JButton implements Runnable {//create button and let it run
	private double dx, dy;
	private int bounceCounter;
	private double r = Math.random() * 3.14159;
	private int Buttonx;
	private int Buttony;

	Timer timer = new Timer(12, new TimerListener());

	public MovingButton() {

		timer.start();
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		Buttonx = 250;
		Buttony = 250;
       
		setSize(50, 50);
		setLocation(Buttonx, Buttony);
		setText("" + bounceCounter);
		
	}

	public void run() {
		setLocation(Buttonx, Buttony);
		
		Buttonx += dx;
		Buttony += dy;
		setText("" + bounceCounter);
		
		if (Buttonx <= 5 || Buttonx >= 430) {
			dy = -(dy);
			dx = -(dx);
			bounceCounter++;
		} else if (Buttony <= 5 || Buttony >= 410) {
			dx = -(dx);
			dy = -(dy);
		}
	}
	
	class TimerListener implements ActionListener {//time listener
		@Override
		public void actionPerformed(ActionEvent e) {
			run();
		}
	}
}