package ce1002.a12.s102502039;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {

	static JFrame jf;

	public static void main(String[] args) {//set frame
		MovingButton jbt = new MovingButton();
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setSize(500, 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(jbt);

	}

	@Override
	public void keyTyped(KeyEvent e) {//keylistener
		MovingButton jbt = new MovingButton();
		jf.add(jbt);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
