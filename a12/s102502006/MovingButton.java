package ce1002.a12.s102502006;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	
	private double x;
	private double y;
	private int width, height;
	
	private double dx;
	private double dy;
	private int bounceCounter;
	private boolean running = false;
	
	public MovingButton(int width, int height) { 
		this.width = width;
		this.height = height;
		running = true;
		
		setBounds((width/2), (height/2), 50, 50);
		
		double r = Math.random() * Math.PI;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		bounceCounter = 0;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(running) {
			
			if (x+50 == width || y+50 == height || x == 0 || y == 0) {
				
				if (x+50 > width || x < 0) {
					y += dy;
					dx = -dx;
					x += dx;
				}
				if (y + 50 > height || y < 0) {
					x += dx;
					dy = -dy;
					y += dy;
				}
				bounceCounter++;
			}
		
			else {
				x += dx;
				y += dy;
			}	
			setBounds((int) x, (int) y, 50, 50);
			setText(bounceCounter + "");
			
			try {  // slow down
				Thread.sleep(15);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
