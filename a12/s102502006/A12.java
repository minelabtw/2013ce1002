package ce1002.a12.s102502006;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	
	static JFrame jf;
	static JButton button;
	static JButton bbutton;
	
	 static int WIDTH = 500;
	 static int HEIGHT = 500;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		jf = new JFrame("A12");
		button = new MovingButton(WIDTH, HEIGHT);
		A12 listener = new A12();
		jf.setLayout(null);
		jf.setSize(WIDTH, HEIGHT);
		jf.add(button);
		
		Thread thread = new Thread((Runnable) button); 
		thread.start(); // operate thread
		
		jf.addKeyListener(listener);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
		
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		bbutton =new MovingButton(WIDTH, HEIGHT); // new one button
		jf.add(bbutton);
		Thread thread = new Thread((Runnable) bbutton);  // declare
		thread.start(); // operate thread
	}
	
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

}
