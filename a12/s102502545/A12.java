package ce1002.a12.s102502545;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	int num = 0;
	public static int width = 800;
	public static int height = 600;
		
	public static void main(String[] args) {		

		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.setLayout(null);
		jf.setSize(width, height);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {//�H�N���U��
		num++;
		MovingButton btn = new MovingButton(width,height);
		jf.add(btn);
		Thread thread = new Thread(btn);
		thread.start();
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

}
