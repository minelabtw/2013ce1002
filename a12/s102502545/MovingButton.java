package ce1002.a12.s102502545;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double x,y;
	private double dx, dy;
	private int bounceCounter;
	private int size = 50;
	private int count = 0;

	public MovingButton(int width,int height) {
		x = (width - size)/2;      //先設定button的初始值
		y = (height - size)/2;
		setBounds((int)x,(int)y,size,size);//設定button大小
		setText(""+count);//裡面次數
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);//討厭的數學

	}

	public void run() 
	  {
	    while(true)//撞牆壁開始囉
	    {
	    	x = x + dx;
	    	y = y + dy;
	    	if( x<0 || x> A12.width - size-25)//撞到x邊界
	    	{
	    		x = x - dx;
	            dx = - dx;
	            count++;
	    	}
	    	if( y<0 || y>A12.height - size-25)//撞到y邊界
	    	{
	    		y = y - dy;
	    		dy = - dy;
	    		count++;
	    	}
	    	setLocation((int)x,(int)y);//重新給位置
	    	setText(""+count);
	    	
	    	try{
	    		Thread.sleep(10);//延遲
	    	}catch(InterruptedException e){
	    		e.printStackTrace();
	    	}
	    	
	    	
	    	
	    	
	    }
	  }
}
