package ce1002.a12.s101201508;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//set the data of the Jframe
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AddButton();

	}

	public static void AddButton() {//set the button
		MovingButton mb = new MovingButton(500 - jf.getInsets().left
				- jf.getInsets().right, 500 - jf.getInsets().top
				- jf.getInsets().bottom);
		jf.add(mb);
		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(mb, 0, 1, TimeUnit.MILLISECONDS);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {//run when the keybroad pressed
		// TODO Auto-generated method stub
		AddButton();
	}

}
