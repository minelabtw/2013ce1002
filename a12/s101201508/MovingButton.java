package ce1002.a12.s101201508;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	double location[]=new double [2];
	private double d []=new double [2];
	private int side[]=new int [2];
	private int bounceCounter;
	public MovingButton(int s1, int s2)
	{//set this button
		double r=Math.random()*3.14159;
		d[0]=Math.cos(r)/5;
		d[1]=Math.sin(r)/5;
		side[0]=s1;
		side[1]=s2;
		bounceCounter=0;
		String label=Integer.toString(bounceCounter);
		super.setLabel(label);
		super.setBounds(s1/2-30,s2/2-20,60,40);
		location[0]=s1/2-30;
		location[1]=s2/2-20;
	}
	@Override
	public void run()
	{
		/*
		double location[]=new double [2];
		location[0]=super.getX();
		location[1]=super.getY();
		*/
		//judge the direction of rebound
		for (int i=0;i<2;i++)
		{
			location[i]=location[i]+d[i];
			if (location[i]<0)
			{
				location[i]=location[i]*(-1);
				d[i]=d[i]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
			else if (i==0 && location[i]>side[0]-60)
			{
				location[0]=(location[0]-side[0]+60)*(-1)+side[0]-60;
				d[0]=d[0]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
			else if (i==1 && location[i]>side[1]-40)
			{
				location[1]=(location[1]-side[1]+40)*(-1)+side[1]-40;
				d[1]=d[1]*(-1);
				bounceCounter++;
				String label=Integer.toString(bounceCounter);
				super.setLabel(label);
			}
		}
		//set the location
		super.setLocation((int)location[0], (int)location[1]);
	}
}
