package ce1002.a12.s102502561;

import javax.swing.*;
import java.awt.Color;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	double x, y;
	int lx, ly;
	private int bounceCounter;
	int speed = 50;
		
	public MovingButton(int lx, int ly) { 
		this.lx = lx;
		this.ly = ly;
		x = lx / 2 - 40;
		y = ly / 2 - 50;
		setBounds((int) x, (int) y, 50, 50);
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);

	}

	public void run() { // move, change color while bouncecounter = 2,4,8,16,32,disappear at 64

		while (true) {
			if (!(x + 50 >= lx || y + 50 >= ly || x <= 0 || y <= 0)) {
				x += dx;//continue torwards moving direction
				y += dy;
			} else {  //while hits wall
				
				if (x + 50 > lx || x < 0) {
					y += dy;
					dx = -dx;
					x += dx;
				}
				if (y + 50 > ly || y < 0) {
					x += dx;
					dy = -dy;
					y += dy;
				}
				bounceCounter++;
				
				if(bounceCounter>=2&&bounceCounter<4){
					setBackground(Color.RED);
					speed = speed-15;
				}
				else if(bounceCounter==4){
					setBackground(Color.ORANGE);
					speed = speed-5;
				}
				else if(bounceCounter==8){
					setBackground(Color.YELLOW);
					speed = speed-5;
				}
				else if(bounceCounter==16){
					setBackground(Color.GREEN);
					speed = speed-5;
				}
				else if(bounceCounter==32){
					setBackground(Color.BLUE);
					speed = 5;
				}
				else if(bounceCounter==64){
					setVisible(false);
				}
				else{
					
				}
				}//while hits wall
			
			setBounds((int) x, (int) y, 50, 50); // location
			setText(bounceCounter + "");

			try {  // slow down
				Thread.sleep(speed);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

}