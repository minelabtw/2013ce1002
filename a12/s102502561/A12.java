package ce1002.a12.s102502561;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	
	static JFrame MyFrame = new JFrame("A12-102502561");
	static JButton firstbutton;//最一開始的button
	static JButton morebuttons;//more buttons!!!
	static int lx = 480;
	static int ly = 470;

	public static void main(String[] args) {
		morebuttons = new MovingButton(lx, ly);//new button
		A12 listener = new A12();
		Thread thread = new Thread((Runnable) morebuttons);  // declare
		thread.start(); // operate thread
		MyFrame.add(morebuttons);
		MyFrame.addKeyListener(listener);
		MyFrame.setFocusable(true);
		MyFrame.setLayout(null);
		MyFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MyFrame.setBounds(200, 50, 500, 500);
		MyFrame.setVisible(true);
		
	}	


@Override
public void keyPressed(KeyEvent event) { 
	firstbutton =new MovingButton(lx, ly); // new one button
	MyFrame.add(firstbutton);
	Thread thread = new Thread((Runnable) firstbutton);  // declare
	thread.start(); // operate thread

}

@Override
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub

}

@Override
public void keyTyped(KeyEvent e) {
	// TODO Auto-generated method stub

}

}
