package ce1002.a12.s102502532;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable, java.lang.Runnable {

	private double dx, dy;
	private int bounceCounter;
	private int jfx = 0; // 視窗x長度
	private int jfy = 0; // 視窗y長度
	private int buttonw = 50; // 按鈕x長度
	private int buttonh = 50; // 按鈕y長度

	Thread moving = new Thread(this);            // 宣告一個Thread //this

	public MovingButton(int x, int y) {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);

		setText(bounceCounter + ""); // setText(內容)
		jfx = x;
		jfy = y;
		x = (jfx - buttonw) / 2;          // 中間
		y = (jfy - buttonh) / 2;

		setBounds(x, y, buttonw, buttonh);             // 按鈕 座標 + 大小
		moving.start();    // call Thread 的 start 函數
	}

	@Override
	public void run() {

		while (true) {
			try {
				Thread.sleep(25);    // 暫停25ms     //button速度

				double x = getLocation().x;     // 按鈕當前x座標
				double y = getLocation().y;     // 按鈕當前y座標

				if (x < 0 || x +dx+50 +15 > jfx) { // 邊界
					dx *= -1; // 變化為負 反方向
					bounceCounter++;
					setText(bounceCounter + "");
				}
				if (y < 0 || y +dy+50 +35 > jfy) {
					dy *= -1;
					bounceCounter++;
					setText(bounceCounter + "");
				}

				x += dx;
				y += dy;
				setLocation((int) x, (int) y);           // 新座標

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
