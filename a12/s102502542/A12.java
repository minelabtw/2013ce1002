package ce1002.a12.s102502542;

import java.awt.event.*;

import javax.swing.*;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		jf = new JFrame("A12-102502542");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300,300,jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setLocationRelativeTo(null);//將frame視窗固定在center
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//將frame的寬度跟長度傳入movingbutton
		MovingButton button = new MovingButton(jf.getWidth(),jf.getHeight());
		jf.add(button);
	}
	
	public void keyPressed(KeyEvent e) //按下任意鍵即呼叫movingbutton
	{
		MovingButton newbutton = new MovingButton(jf.getWidth(),jf.getHeight());
		jf.add(newbutton); 
	}
	public void keyReleased(KeyEvent e) 
	{
	}
	public void keyTyped(KeyEvent e) 
	{
	}
}
