package ce1002.a12.s102502542;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy;
    private double x,y;
	private int bounceCounter;
	private int width,height;//frame的寬度和長度
	Thread move= new Thread(this); //將實作的Runnable當作參數傳給thread
	
	public MovingButton(int w,int h)//接收來自main傳入的frame的寬度和長度
	{
		width=w;//frame的寬度
		height=h;//frame的長度
		
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);//x軸移動的向量
		dy=3*Math.sin(r);//y軸移動的向量
		x=w/2;
		y=h/2;
		setText(""+bounceCounter);
		setBounds(w/2,h/2,75,75);
		move.start();//呼叫thread執行run()
	}
  public void run() 
  {
	  while (true) 
	  {
			try 
			{
				Thread.sleep(10);//延遲下一步所需時間
		
				//判斷式當button的x y座標未超出frame的框框
				if(x+dx>=0&&x+dx+80<=width&&y+dy>=0&&y+dy+105<=height)
				{
					x=x+dx;
					y=y+dy;
					setLocation((int)x,(int)y);//讓button往前移動
				}
				//判斷式當button的x y座標超出frame的框框
				else
				{
					bounceCounter++;
					setText(""+bounceCounter);
					if(x+dx<0||x+dx+80>width)
					{
						dx=-1*dx;//朝-dx向量移動
					}
					if(y+dy<=0||y+dy+105>height)
					{
						dy=-1*dy;//朝-dy向量移動
					}
				}
			} 
			catch (Exception a) 
			{
				a.printStackTrace();
			}
	  }
  }
}
