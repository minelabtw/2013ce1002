package ce1002.a12.s102502505;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A12 extends KeyAdapter implements KeyListener {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final JFrame jf = new JFrame("A12");
		MovingButton button = new MovingButton();//新增第一個按鈕
		Thread t = new Thread(button, "button");
		t.start();

		A12 listener = new A12() {//新增鍵盤轉接器
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
				MovingButton button = new MovingButton();//新增按鈕
				Thread t = new Thread(button, "button");
				jf.add(button);//新增按鈕至frame
				t.start();//開始跑run()

			}
		};
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.add(button);

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
