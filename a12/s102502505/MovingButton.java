package ce1002.a12.s102502505;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JLabel;

public class MovingButton extends JButton implements Runnable {

	public double dx, dy;
	public int count = 0;

	public MovingButton() {

		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		setBounds(215, 200, 50, 50);//設定起始位置

	}

	public void run() {

		try {

			int xpoint = 215;//新增xy座標起始值
			int ypoint = 200;

			while (true) {

				xpoint = xpoint + (int) dx;//改變XY座標，移動
				ypoint = ypoint + (int) dy;

				if (xpoint < 0 || xpoint > 430) {
					dx = -dx;//到邊界時改變方向
					count++;//計算碰撞次數
				}
				if (ypoint < 0 || ypoint > 430) {
					dy = -dy;
					count++;
				}
				Thread.sleep(10);//新增時間間隔
				setBounds(xpoint, ypoint, 50, 50);//改變位置
				setText("" + count);//顯示碰撞次數

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
