package ce1002.a12.s100502022;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.JFrame;

public class A12 extends JFrame implements KeyListener, KeyEventDispatcher {
	public static Vector<MovingButton> buttonList;
	boolean add = false;

	public static void main(String[] args) {
		new A12();
	}

	public A12() {
		this.setTitle("A12");
		this.setSize(433, 453);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setLayout(null);
		//add firsts
		buttonList = new Vector<MovingButton>();
		addDouble();
		this.setVisible(true);
		//time set
		Timer timer = new Timer();
		timer.schedule(new Process(), 100, 30);
		KeyboardFocusManager.getCurrentKeyboardFocusManager()
				.addKeyEventDispatcher(this);
	}
	//for all buttons , repaint moving path
	class Process extends TimerTask {
		public void run() {
			if (!add) {
				for (MovingButton bb : buttonList)
					bb.adjust();
				repaint();
			} 
		}
	}
	//if click any key add a new button
	public boolean dispatchKeyEvent(KeyEvent e) {
		if (e.getID() == KeyEvent.KEY_RELEASED)
			return false;
		addDouble();
		return false;
	}
	//add new button
	public void addDouble() {
		add = true;
		MovingButton temp = new MovingButton(this.getWidth() / 2,this.getHeight() / 2, 50, 50, this);
		this.add(temp);
		buttonList.add(temp);
		add = false;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}