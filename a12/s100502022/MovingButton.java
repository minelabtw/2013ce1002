package ce1002.a12.s100502022;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	double x, y;
    double vx, vy;
    int count = 0;
    A12 pi;

    public MovingButton(int x, int y, int width, int height, A12 pi) {
        super();
        
        this.setBounds(x, y, width, height);
        this.x = x;
        this.y = y;
        this.pi = pi;
        this.setText("0");
        double theta = Math.random() * 2 * Math.PI;
        vx = Math.sin(theta) * 5;
        vy = Math.cos(theta) * 5;
        //if click this button, we have a new button
        this.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                double theta = Math.random() * 2 * Math.PI;
                vx = Math.sin(theta) * 5;
                vy = Math.cos(theta) * 5;
                addDouble();
            }
        });
      //if click this button, we have a new button
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                addDouble();
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mousePressed(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                // TODO Auto-generated method stub

            }
        });
    }
    //add function
    void addDouble() {
        pi.addDouble();
    }
    //if button touch the wall, react
    void adjust() {
        boolean touch = false;
        if (x + vx < 0 || x + vx + this.getWidth() + 20 >= 450) {
            vx = -vx;
            touch = true;
        }
        if (y + vy < 0 || y + vy + this.getHeight() + 20 >= 450) {
            vy = -vy;
            touch = true;
        }
        x += vx;
        y += vy;
        if (touch == true) {
            count++;
            this.setText("" + count);
        }
        this.setBounds((int) x, (int) y, this.getWidth(), this.getHeight());
    }

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
 
}