package ce1002.a12.s102502047;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.Timer;


public class MovingButton extends JButton implements Runnable
{
  private double dx,dy;//定義變化量
	private int bounceCounter=0;
	private Timer t=new Timer(100,new TimerListener());//定義Timer
    private int x=200;
    private int y=200;
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setText("0");
		setBounds(200,200,50,25);
		t.start();
	}
  public void run() 
  {
	if(x<=0||x>=435){
		dx=-dx;
		bounceCounter++;
	}
	if(y<=0||y>=440){
	    dy=-dy;
	    bounceCounter++;
	}
	x=x+(int)dx*5;
	y=y+(int)dy*5;
	setText(""+bounceCounter);
	setLocation(x, y);
  }
  class TimerListener implements ActionListener{
	public void actionPerformed(ActionEvent e) {
		run();
	}
  }
}
