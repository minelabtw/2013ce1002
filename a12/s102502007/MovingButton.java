package ce1002.a12.s102502007;
import javax.swing.JButton;
public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;//button's vector
	private int bounceCounter;
	private double x, y;//coordinate
	private double height, width;//frame's size
	
	public MovingButton(int height, int width)
	{
		double r = Math.random()*3.14159;
		dx = 6*Math.cos(r);
		dy = 6*Math.sin(r);
		this.height = height - 50;
		this.width = width  - 50;
		//set center point
		x = width*0.5;
		y = height*0.5;
		//center the button and set the button's size
		setBounds((int)x, (int)y, 60, 75);
	}
	@Override
  public void run() 
  {
    for(;;)
    {
    	x += dx;// change x coordinate
    	y += dy;// change y coordinate
    	
    	/*
    	 * change direction when the
    	 *  button collides with the frame
    	 */
    	if(x<=0 || x>=width)
    	{
    		dx = -1*dx;
    		bounceCounter++;
    	}
    	else if(y<=0 || y>=height)
    	{
    		dy = -1*dy;
    		bounceCounter++;
    	}
    	else;
    	//change the number of button
    	this.setText("" + bounceCounter);
    	//change the position
    	this.setLocation((int)x, (int)y);
    	
    	try 
    	{
			Thread.sleep(15);
		} catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
  }
}