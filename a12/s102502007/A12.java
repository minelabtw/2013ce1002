package ce1002.a12.s102502007;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
public class A12 extends JFrame implements KeyListener
{
	private A12()
	{
		super("A12-102502007");
		getContentPane().setLayout(null);
		setSize(getInsets().left+getInsets().right+500, getInsets().top+getInsets().bottom+500);
		addKeyListener(this);
		setFocusable(true);
		setDefaultCloseOperation(A12.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	static A12 a12()
	{
		return new A12();
	}
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		A12.a12();
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
		//when the key is typed, create a JButton
		JButton button = new MovingButton(getHeight(), getWidth());
		//add the button into frame
		this.add(button);
		//start running
		Thread thread = new Thread((Runnable)button);
		thread.start();
	}

}
