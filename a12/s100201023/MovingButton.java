package ce1002.a12.s100201023;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable
{
	//properties
	private double dx , dy , x, y;
	private int bounceCounter = 0;
	private Timer timer = new Timer(10, new TimerListioner());
	
	//constructure
	public MovingButton()
	{
		Random ran = new Random();
		x = ran.nextInt(450);
		y = ran.nextInt(400);
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		timer.start();
		
	}
	
	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		boolean check;
		check = false;
		setText("" + bounceCounter);
		setBounds((int)x, (int)y, 50, 50);
		x += dx;
		y += dy;
		
		//consider bounds
		if(x < 0)
		{
			x = 0;
			check = true;
			dx *= -1;
		}
		if(y < 0)
		{
			y = 0;
			check = true;
			dy *= -1;
		}
		if(x > 450)
		{
			x = 450;
			check = true;
			dx *= -1;
		}
		if(y > 400)
		{
			y = 400;
			check = true;
			dy *= -1;
		}
		if(check)
			++bounceCounter;
	}
	
	class TimerListioner implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			// TODO Auto-generated method stub
			run();
		}
	}
}
