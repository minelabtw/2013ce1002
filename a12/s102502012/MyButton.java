package ce1002.a12.s102502012;

import javax.swing.JButton;

public class MyButton extends JButton implements Runnable {
	private int windowWidth;
	private int windowHeight;
	private int width = 50;
	private int height = 50;
	private double x, y;
	private double dx, dy;
	private int bounceCounter;
	
	public MyButton(int windowWidth, int windowHeight){
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		bounceCounter = 0;
		setText("" + bounceCounter);
		this.windowHeight = windowHeight;
		this.windowWidth = windowWidth;
		x = (windowWidth - width) / 2;
		y = (windowHeight - height) / 2;
		setBounds((int)x, (int)y, width, height);
	}
	
	public void run() {
		while(true){
			try{
				Thread.sleep(50);
			}catch(Exception e){
				e.printStackTrace();
			}
			double x = getLocation().x;
			double y = getLocation().y;
			if(x + dx >= 0 && x + dx + width <= windowWidth && y + dy >= 0 && y + dy + height + 30 <= windowHeight){
				x += dx;
				y += dy;
				setLocation((int)x, (int)y);
			}
			else{
				bounceCounter++;
				setText("" + bounceCounter);
				if(x + dx < 0 || x + dx + width > windowWidth){
					dx *= -1.0f;
					System.out.println(x + "" + y);
				}
				if(y + dy < 0 || y + dy + height + 30> windowHeight){
					System.out.println(x + "" + y);
				
					dy *= -1.0f;
				}
			}
		}
	}
	
}
