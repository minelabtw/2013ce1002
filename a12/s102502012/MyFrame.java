package ce1002.a12.s102502012;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame() {
		setLayout(null);
		addKeyListener(new pressAction());
		setTitle("A12-s102502012");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 600);
		setVisible(true);
		setResizable(false);
		
		addButton();
	}
	
	public void addButton(){
		MyButton btn = new MyButton(getWidth(), getHeight());
		Thread t = new Thread(btn);
		t.start();
		add(btn);
	}
	
	public class pressAction implements KeyListener{

		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() != KeyEvent.KEY_PRESSED){ // I don't know which key it is
				addButton();
			}
		}

		public void keyReleased(KeyEvent e) {
			
		}

		public void keyTyped(KeyEvent e) {
			
		}
		
	}
}