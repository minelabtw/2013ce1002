package ce1002.a12.s102502548;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx,dy;
	private int bounceCounter=0;
	private int locatex=0 ;
	private int locatey=0 ;
	
	public MovingButton()//建立一個預設的按鈕
	{	
		locatex=(int)(Math.random()*450) ;
		locatey=(int)(Math.random()*450) ;
		
		setBounds(locatex, locatey, 50, 50) ;
		
	}
	
	public void run() 
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r) ;//速度向量
		dy=3*Math.sin(r) ;
		
		
		
	    while(true){//讓他連續移動
	    	locatex+=dx ;
			locatey+=dy ;
			
	    	setBounds(locatex, locatey, 50, 50) ;
	    	
	    	if (getLocation().x<=0||getLocation().x>=450){//碰到邊界
	    		dx=-dx ;

	    		bounceCounter++ ;
	    	}
	    	
	    	if (getLocation().y<=0||getLocation().y>=450){
	    		dy=-dy ;
	    		
	    		bounceCounter++ ;
	    	}
	    	
	    	setText(""+bounceCounter) ;
	    	
	    	try {
				Thread.sleep(10) ;
			} 
	    	
	    	catch (InterruptedException e) {
				e.printStackTrace() ;
			}
	    }
	}
}
