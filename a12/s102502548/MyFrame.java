package ce1002.a12.s102502548;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel panel=new MyPanel() ;
	
	public MyFrame() {
		setBounds(5, 5, 515, 530) ;//設定視窗
		
		setTitle("A12") ;
		
		setLayout(null) ;
 		
		add(panel) ;
		
		addKeyListener(new KeyListener() {//加一個監聽器
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {//建立一個按鈕
				MovingButton button=new MovingButton() ;
				Thread thread=new Thread(button) ;//建立執行緒
				
				panel.add(button) ;
	
				thread.start() ;//啟動
			}
		}) ;	
		
		setFocusable(true) ;
		
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		
		setVisible(true) ;
	}
}
