package ce1002.a12.s102502008;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy;
	private int bounceCounter;
	private double x,y ;
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		this.setBounds(0,0,50,50) ;
		this.setText(""+bounceCounter) ;
	}
  public void run() 
  {
	 while(true)
	 {
		 if(x<=-1 || x>=450)
		 {
			 dx=(-dx) ;
			 bounceCounter++ ;
			 this.setText(""+bounceCounter) ;
		 }
		 if(y<=-1 || y>=425)
		 {
			 dy=(-dy) ;
			 bounceCounter++ ;
			 this.setText(""+bounceCounter) ;
		 }
		 x+=dx ;
		 y+=dy ;
		 this.setLocation((int)x, (int)y);
		 try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
  }
  
}
