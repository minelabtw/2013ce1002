package ce1002.a12.s102502537;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	private static MovingButton movingbutton = new MovingButton();

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton mb = new MovingButton(); // new movingbutton
		jf.add(movingbutton);
		new Thread(movingbutton).start();
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12"); // set frame
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.addKeyListener(listener);
		jf.add(movingbutton);
		new Thread(movingbutton).start(); // start movingbutton
	}

}