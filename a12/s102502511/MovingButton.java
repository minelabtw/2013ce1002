package ce1002.a12.s102502511;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable{ //實作Runnable
	double dx,dy; //速度和方向 
	int dnx = 220; //原始位置
	int dny = 190; //原始位置
	int size = 60; //方塊大小
	private int bounceCounter; //紀錄碰撞次數

	public MovingButton()
	{
		double r = Math.random()*3.14159; 
		dx=3*Math.cos(r); //給定x的速度方向
		dy=3*Math.sin(r); //給定y速度方向
		this.setText("" + bounceCounter); //將計次的數字丟入button
		this.setBounds(dnx,dny,size,size); //button位置和大小
	}	
	 
	public void run(){
		try {
			boolean check = true; //將check設為true
			while(check){ //因為check是true所以不停重複迴圈內的東西
				 	if (dnx + dx < 0 || dnx + dx + size >= 500) { //當碰到邊界時
		                dx = -dx; //轉向
		                bounceCounter++; //碰撞次數+1
		                setText("" + bounceCounter); //顯示碰撞次數
		            }
		            if (dny + dy < 0 || dny + dy + size >= 470) {//當碰到邊界時
		                dy = -dy; //轉向
		                bounceCounter++; //碰撞次數+1
		                setText("" + bounceCounter); //顯示碰撞次數
		            }
		            dnx += dx;  //每次的移動 都加回至原本的座標 使座標有變動 產生移動的現象
		            dny += dy;
		            setBounds((int) dnx, (int) dny, size, size); //將新的座標重設回button中
		            Thread.sleep(15); //每次動作停頓15毫秒
			}
        } 
        catch(InterruptedException e) { 
            e.printStackTrace(); 
        } 
	}
}