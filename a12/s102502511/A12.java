package ce1002.a12.s102502511;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;



public class A12 implements KeyListener{
	
	static JFrame jf;
	
	public static void main(String[] args) {
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton btn = new MovingButton(); //使用MovingButton 建立一個btn
		Thread t1 = new Thread(btn); //MovingButton實作Runnable 因此將btn丟入Thread做動作
		t1.start(); //t1開始
		jf.add(btn); //加入button
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		MovingButton btn2 = new MovingButton(); //使用MovingButton 建立一個新的按鈕 btn2
		Thread t2 = new Thread(btn2); //MovingButton實作Runnable 因此將btn2丟入Thread做動作
		t2.start(); //t2開始
		jf.add(btn2); //加入button
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		
	}
}
	