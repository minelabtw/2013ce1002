package ce1002.a12.s102502541;
import javax.swing.JButton;
import javax.swing.Timer;



import java.awt.event.*;

public class MovingButton extends JButton implements Runnable{

	private double dx,dy;
	private int bounceCounter;
	private int x,y;

		public MovingButton()
		{
			double r = Math.random()*3.14159;
			dx=3*Math.cos(r);
			dy=3*Math.sin(r);
			x = 210;
			y = 210;//初始位置
		}
	  public void run() 
	  {
		  while(true)
		  {
			  x += (int)dx;
			  y += (int)dy;
			  if(x < 0)
			  {
				  bounceCounter++;
				  x=0;
				  dx=-dx;
			  }
			  else if(x>425)
			  {
				  bounceCounter++;
				  x=425;
				  dx=-dx;
			  }
			  
			  if(y < 0)
			  {
				  bounceCounter++;
				  y=0;
				  dy=-dy;
			  }
			  else if(y>425)
			  {
				  bounceCounter++;
				  y=425;
				  dy=-dy;
			  }//碰到邊界  計數器+1  方向轉向
			  setBounds(x,y,50,50);
			  setText(""+bounceCounter);
			  repaint();
			  try
			  {
				  Thread.sleep(10);
			  }
			  catch(InterruptedException e)
			    {
			    	e.printStackTrace();
			    }
		  }
		  
	  }

}
