package ce1002.a12.s102502541;
import javax.swing.JFrame;
import java.awt.event.*;
public class A12 implements KeyListener{
	static JFrame jf;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton b = new MovingButton();
		jf.add(b);
		Thread thread1 = new Thread(b);
		thread1.start();
	}
	public void keyPressed(KeyEvent e)
	{
		if(e.getSource()!=null)
		{
			MovingButton b = new MovingButton();
			jf.add(b);
			Thread thread = new Thread(b);//執行run()
			thread.start();	
		}		
	}
	public void keyReleased(KeyEvent e)
	{
		
	}
	public void keyTyped(KeyEvent e)
	{
	
	}
	
	
	 

}
