package ce1002.a12.s984008030;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	
	static JFrame jf = new JFrame("A12");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300,
					 300,
					 jf.getInsets().left + jf.getInsets().right + 500,
					 jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addBotton();// add new button
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// create new button
		addBotton();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// Do nothing
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// Do nothing
	}

	private static void addBotton() { // Create new botton
		MovingButton newMovingButton = new MovingButton();
		newMovingButton.fw = jf.getWidth() - jf.getInsets().left - jf.getInsets().right;
		newMovingButton.fh = jf.getHeight() - jf.getInsets().top - jf.getInsets().bottom;
		// set button position at center of the frame
		newMovingButton.setBounds(jf.getWidth() / 2 - 25 , jf.getHeight() / 2 - 25, 50, 50);
		jf.add(newMovingButton);
		// start moving the button
		new Thread(newMovingButton).start();
	}
	
}