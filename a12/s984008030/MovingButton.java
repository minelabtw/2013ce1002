package ce1002.a12.s984008030;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable {
	
	private double dx,dy;
	private int bounceCounter;
	public int fw, fh;
	private long interval = 25;
	
	public MovingButton() {
		double r = Math.random() * Math.PI;
		this.dx = 3 * Math.cos(r);
		this.dy = 3 * Math.sin(r);
		this.bounceCounter = 0;
		this.setText(String.valueOf(bounceCounter));
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				// Sleep interval time and then change button's position
				Thread.sleep(interval);
			}
			catch (InterruptedException e) {
			}
			// Check if the button touches one of the frame's border
			if (this.getX() <= 0 || this.getX() + this.getWidth() >= this.fw) {
				// touch left or right border of frame
				// change the direction of dx an add one to the bounceCounter
				this.setText(String.valueOf(++bounceCounter));
				this.dx *= -1;
			}
			if (this.getY() <= 0 || this.getY() + this.getHeight() >= this.fh) {
				// touch top or bottom border of frame
				// change the direction of dy an add one to the bounceCounter
				this.setText(String.valueOf(++bounceCounter));
				this.dy *= -1;
			}
			// update the position of the button
			this.setBounds((int)(this.getX() + this.dx),
						   (int)(this.getY() + this.dy),
						   this.getWidth(),
						   this.getHeight());
		}
	}
}