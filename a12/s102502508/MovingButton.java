package ce1002.a12.s102502508;
import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	
	 private double dx,dy;
	 private int frameHeight, framewidth;
	 private int x = 225, y = 225;//設定新按鈕的初始位子
	 private int bounceCounter = 0;
	 Thread thread = new Thread(this);

	 public MovingButton()
	 {
	   //設定按鈕的移動範圍
	   this.frameHeight=500;
	   this.framewidth = 500;
	   //按鈕的初始位子和大小
	   this.setBounds(x, y, 70, 70);
	   //按鈕內的計數器
	   this.setText(String.valueOf(bounceCounter));		 
	   double r = Math.random()*3.14159;
	   dx=3*Math.cos(r);
	   dy=3*Math.sin(r);
	   thread.start();
	 }
	 public void run()
	 {   //當新增的按鈕移動超出範圍時,利用將向量方向改向,且重設起始點的方法,讓新按鈕順利地從邊框反彈,並且將其內的計數器加1
		 while (true) {
				x =x+ (int) dx;
				y =y+ (int) dy;

				if (x <= 0)
				{
					dx =dx*(-1);
					x = 0;
					bounceCounter=bounceCounter+1;
				}
				else if (x >= framewidth - 70) 
				{
					dx *= -1;
					x = framewidth - 70;
					bounceCounter=bounceCounter+1;
				}

				if (y <= 0)
				{
					dy *= -1;
					y = 0;
					bounceCounter=bounceCounter+1;
				} 
				else if (y >= frameHeight - 70) 
				{
					dy *= -1;
					y = frameHeight - 70;
					bounceCounter=bounceCounter+1;
				}
				this.setLocation(x, y);
				this.setText(String.valueOf(bounceCounter));

				try 
				{
					Thread.sleep(35);//設定休息時間
				} 
				catch (InterruptedException e) {
					e.printStackTrace();
				}

			}

		}
		 
	 }


