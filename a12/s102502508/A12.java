package ce1002.a12.s102502508;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import java.awt.event.KeyEvent;
public class A12 implements KeyListener{

	    static JFrame jf;//建立視窗
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		A12 listener = new A12();

		jf = new JFrame("A12");//視窗主題
		jf.setLayout(null);
		jf.setBounds(300,300,jf.getInsets().left + jf.getInsets().right + 500,jf.getInsets().top + jf.getInsets().bottom + 500);//設定視窗大小和位子
		jf.addKeyListener(listener);//將能產生新按鈕的物件加到視窗內
		jf.setVisible(true);//讓使用者能看到視窗
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//使視窗的主程式可以被關閉
		jf.add(new MovingButton());//將所製造出來的新按鈕加到視窗內
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		 jf.add(new MovingButton());//藉由按鍵可以不斷的新增新按鈕
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
