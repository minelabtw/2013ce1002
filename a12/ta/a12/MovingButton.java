package ce1002.a12;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// used to calculate button's next coordinate
	private double dx,dy;
	private int bounceCounter;
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		
	}
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			
			double x = getLocation().x , y = getLocation().y;
			x+=dx;
			y+=dy;
			setLocation((int)x, (int)y);
			
			if(getLocation().x>=435 || getLocation().x<=0)
			{
				dx*=-1;
				bounceCounter++;
			}
			if(getLocation().y>=410 || getLocation().y<=0)
			{
				dy*=-1;
				bounceCounter++;
			}
			setText(Integer.toString(bounceCounter));
			
			try 
			{
				Thread.sleep(15);
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
	}
}
