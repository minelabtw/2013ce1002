package ce1002.a12.s102502035;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter;
	int x;
	int y;

	public MovingButton() {
		double r = Math.random() * 2 * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		x = (int) (440 * Math.random());
		y = (int) (420 * Math.random());
		//different initial point
		setBounds(x, y, 50, 50);

		/*
		 * ActionListener actionListener = new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent arg0) { // TODO
		 * Auto-generated method stub // run(); } };
		 */

		ActionListener buttonListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println(bounceCounter);
			}
		};
		// final Timer timer = new Timer(10, actionListener);
		// timer.start();
		addActionListener(buttonListener);
		// me program can read button's number
		// cool

	}

	public void run() {

		try {
			for (;;) {
				if (x >= 440 || x <= 0) {
					dx *= -1;
					bounceCounter++;
				}
				if (y >= 420 || y <= 0) {
					dy *= -1;
					bounceCounter++;
				}
				x = x + (int) dx;
				y = y + (int) dy;
				setLocation(x, y);
				setText("" + bounceCounter);
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			// e.printStackTrace();
		}
	}
}
