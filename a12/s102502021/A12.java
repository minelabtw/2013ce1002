package ce1002.a12.s102502021;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;
	private static MovingButton z = new MovingButton();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.addKeyListener(listener);
		jf.setSize(500, 500);
		jf.getContentPane().setLayout(null);

		jf.add(z);
		new Thread(z).start(); // 開始移動
		jf.setVisible(true);
		jf.setLayout(null);
		jf.setFocusable(true);

		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton k = new MovingButton();
		jf.add(k);
		new Thread(k).start(); // 開始移動
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
