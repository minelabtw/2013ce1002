package ce1002.a12.s102502522;
import javax.swing.*;
public class MovingButton extends JButton implements Runnable 
{
private int x,y;
private double dx,dy;
private int bounceCounter=0;
private double r;
MovingButton()
{     
	  r = Math.random()*3.14159;//隨機一角度
      dx=3*Math.cos(r);//x軸向量
      dy=3*Math.sin(r);//y軸向量
      x=250;//按鈕出現起始值
      y=250;
	  setBounds(x,y,50,50);
	  setText(""+bounceCounter);
}
	
	public void run() 
	{
		while(true)
		{
			if(x==0 || x==440)//碰到上下兩道牆
			{
				dx=dx*-1;
				bounceCounter++;
			}
			if(y==0 || y==410)//碰到左右兩道牆
			{
				dy=dy*-1;
				bounceCounter++;
			}
			x=x+(int)dx;
			y=y+(int)dy;
			setBounds(x,y,50,50);
			setText(""+bounceCounter);
			try
			{
					Thread.sleep(10);//延遲程式運作
			}
			catch(InterruptedException e)
			{
				
			}
		}
		
	}
	
}