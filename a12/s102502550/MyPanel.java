package ce1002.a12.s102502550;

import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	MyPanel(){
		setBounds(0, 0, 500, 500) ;
		setLayout(null) ;
		
		MovingButton button=new MovingButton();		
		add(button) ;

		Thread thread=new Thread(button) ;
		thread.start() ;
	}
}
