package ce1002.a12.s102502550;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx,dy;
	private int locx=0 ;
	private int locy=0 ;
	private MyFrame f;
	private int bounceCounter=0;
	
	public MovingButton() 						    //create a button
	{	
		locx=(int)(Math.random()*450) ;
		locy=(int)(Math.random()*450) ;
		
		setBounds(locx, locy, 50, 50) ;
		
	}
	
	public void run() 
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r) ;							//vector
		dy=3*Math.sin(r) ;
		
		
		
	    while(true){
	    	locx+=dx ;
			locy+=dy ;
			
	    	setBounds(locx, locy, 50, 50) ;
	    	
	    	if (getLocation().x<=0||getLocation().x>=440){         //bounce back
	    		dx=-dx ;

	    		bounceCounter++ ;
	    	}
	    	
	    	if (getLocation().y<=0||getLocation().y>=420){
	    		dy=-dy ;
	    		
	    		bounceCounter++ ;
	    	}
	    	
	    	setText(bounceCounter+"") ;
	    	
	    	try {
				Thread.sleep(10) ;
			} 
	    	
	    	catch (InterruptedException e) {
				e.printStackTrace() ;
			}
	    }
	}
}
