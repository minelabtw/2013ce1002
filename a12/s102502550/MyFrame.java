package ce1002.a12.s102502550;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel panel=new MyPanel() ;
	
	public MyFrame() {
		setBounds(300, 300,getInsets().left+getInsets().right+500, getInsets().top+getInsets().bottom+500) ;//�]�w����
		setTitle("A12") ;	//GUI
		setLayout(null) ;
		add(panel) ;
		
		addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				MovingButton button=new MovingButton() ;
				Thread thread=new Thread(button) ;			 //create new thread
				
				panel.add(button) ;
	
				thread.start() ;
			}
		}) ;	
		
		setVisible(true) ;
		setFocusable(true) ;
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		
		
	}
}
