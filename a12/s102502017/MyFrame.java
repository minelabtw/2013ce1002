package ce1002.a12.s102502017;

import javax.swing.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyFrame extends JFrame{
	
	int width = 600;
	int height = 600;
	
	MyFrame(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setSize(width,height);
		this.setTitle("A12");
		
		KeyListener listener = new Listener();
		addKeyListener(listener);
		
		setVisible(true);
	}
	//add Buttons
	class Listener implements KeyListener {

		public void keyPressed(KeyEvent k) {
			// TODO Auto-generated method stub
			
		}

		public void keyReleased(KeyEvent k) {
			// TODO Auto-generated method stub
			
		}

		public void keyTyped(KeyEvent k) {
			// TODO Auto-generated method stub
			JButton b = new MovingButton(width,height);
			add(b);
			
			Thread thread = new Thread((Runnable)b);
			thread.start();
		}
		
	}

}
