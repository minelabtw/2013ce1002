package ce1002.a12.s102502017;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable{
	
	//the thick of the button
	int thickness = 50;
	int width;
	int height;
	
	double movex;
	double movey;
	
	double movexv;
	double moveyv;
	
	int counter;
	
	
	MovingButton(int width,int height){
		//cause the width is the window's width,
		//not the content's width
		this.width = width-15;
		this.height = height-40;
		
		movex = width/2;
		movey = height/2;
		//let r between 0~2�k
		//so it won't be only start with �� direction
		double r = (Math.random()*2)*Math.PI;
		movexv = 3*Math.cos(r);
		moveyv = 3*Math.sin(r);
		
		this.setBounds((int)movex,(int)movey,thickness,thickness);
		
	}
	
	
	
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			movex += movexv;
			movey += moveyv;
			
			//border
			if(willBounce()){
				if(movex + movexv >= width - thickness){
					movex = width - thickness;
					movexv *=-1;
				}
				else if(movex + movexv <= 0){
					movex = 0;
					movexv *=-1;
				}
				if(movey + moveyv >= height - thickness){
					movey = height - thickness;
					moveyv *=-1;
				}
				else if(movey + moveyv <= 0){
					movey = 0;
					moveyv*=-1;
				}
				counter++;
			}
			
			setText(String.valueOf(counter));
			setLocation((int)movex,(int)movey);
			//slow the speed
			try {
	            Thread.sleep(7); 
	        } catch(InterruptedException e) {
	            e.printStackTrace();
	        }
			
		}
		
		
	}
	
	boolean willBounce(){
		if(		movex + movexv >= width - thickness ||
				movex + movexv <=0 ||
				movey + moveyv >= height - thickness ||
				movey + moveyv <= 0)
			return true;
		else return false;
	}

}
