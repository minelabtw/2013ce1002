package ce1002.a12.s102502032;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class A12 implements KeyListener
{
	static JFrame		jf;
	static MovingButton	movingButtonList[]	= new MovingButton[1];
	static timeLisenter	lisenter			= new timeLisenter();
	static Timer		timer				= new Timer(25, lisenter);

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setResizable(false);
		
		movingButtonList[0] = new MovingButton();
		jf.add(movingButtonList[movingButtonList.length - 1]);
		lisenter.setList(movingButtonList);
		timer.start();
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		movingButtonList = arrExtend(movingButtonList);
		jf.add(movingButtonList[movingButtonList.length - 1]);
		lisenter.setList(movingButtonList);
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub
	}

	// extending array
	public MovingButton[] arrExtend(MovingButton[] origArr)
	{
		MovingButton extendedArr[] = new MovingButton[origArr.length + 1];
		System.arraycopy(origArr, 0, extendedArr, 0, origArr.length);
		extendedArr[origArr.length] = new MovingButton();
		return extendedArr;
	}
}
