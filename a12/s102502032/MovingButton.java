package ce1002.a12.s102502032;

import java.util.Random;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private int		bounceCounter	= 0;
	private double	direction[]		= new double[2];

	public MovingButton()
	{
		while (Math.abs(direction[0]) < 1 && Math.abs(direction[1]) < 1)
		{
			direction[0] = new Random().nextDouble() * 5;
			direction[1] = new Random().nextDouble() * 5;
			if (new Random().nextBoolean() == true)
			{
				direction[0] *= -1;
			}
			if (new Random().nextBoolean() == true)
			{
				direction[1] *= -1;
			}
		}
		this.setSize(50, 50);
		this.setLocation((300 + this.getWidth()) / 2,
				(300 + this.getHeight()) / 2);
		this.setText(String.valueOf(bounceCounter));
		this.setVisible(true);
	}

	@Override
	public void run()
	{
	}

	public void setNewLocation()
	{
		if (this.getLocation().x <= 5 || this.getLocation().x >= 400)
		{
			direction[0] *= -1;
			if (this.getLocation().x <= 5)
			{
				this.setLocation(7, this.getLocation().y);
			}
			else
			{
				this.setLocation(398, this.getLocation().y);
			}
			bounceCounter ++;
		}
		if (this.getLocation().y <= 4 || this.getLocation().y >= 401)
		{
			direction[1] *= -1;
			if (this.getLocation().y <= 4)
			{
				this.setLocation(this.getLocation().x, 6);
			}
			else
			{
				this.setLocation(this.getLocation().x, 399);
			}
			bounceCounter ++;
		}
		this.setLocation((int) (this.getLocation().x + direction[0]),
				(int) (this.getLocation().y + direction[1]));
		this.setText(String.valueOf(bounceCounter));
		repaint();
	}
}
