package ce1002.a12.s102502032;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class timeLisenter implements ActionListener
{
	private MovingButton	mbList[]	= new MovingButton[1];

	public void setList(MovingButton[] movingButtonList)
	{
		this.mbList = movingButtonList;
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		for (int i = 0; i < mbList.length; i ++)
		{
			mbList[i].setNewLocation();
		}
	}
}
