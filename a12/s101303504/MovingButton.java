package ce1002.a12.s101303504;
import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy;
	private int times=0;
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setBounds(50, 50, 50, 50);
		
	}

public void run() 
  {	
	int x=50,y=50;
	while(true){
		try
		{
			x=(int) (x+dx);
			y=(int) (y+dy);
			Thread.sleep(15);// the speed of moving button
		}
		catch(InterruptedException e)
		{
			e.printStackTrace();
		}
		
		if(x<0 || x> 430)// the range of x 
		{
			dx = -dx;
			times ++;
		}
		if(y<0 || y>430)//the range of y
		{
			dy = -dy;
			times ++;
		}
		
		setBounds(x, y, 50, 50);
		setText( "" + times);
	}
  }
}
