package ce1002.a12.s102502019;
import javax.swing.JButton;
public class MovingButton extends JButton implements Runnable{
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double dx,dy,x,y;
		private int bounceCounter=1;
		public MovingButton()                        //按鈕基本設置
		{
		double r = Math.random()*3.14159;
	        dx=3*Math.cos(r);
	        dy=3*Math.sin(r);
	        x = Math.random()*436;
	        y = Math.random()*412;
	        setBounds((int)x,(int)y,50,50);
	        setText(""+bounceCounter);
		}
		
	public void run()                                    //動作
	{
		try
		{
			while(true)
			{
				x = x+dx;
				y = y+dy;
				setLocation((int)x, (int)y);
			    setText(""+bounceCounter);
			    Thread.sleep(9);
			    if(x<0 || x>437)
			    {
			    	dx = -dx;
			    	bounceCounter++;
			    	setText(""+bounceCounter);
			    }
			    if(y<0 || y>412)
			    {
			    	dy = -dy;
			    	bounceCounter++;
			    	setText(""+bounceCounter);
			    }
			}
		}catch(Exception e)
		{
				
		}
		
	}
}

