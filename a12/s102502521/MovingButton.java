package ce1002.a12.s102502521;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable {
	
	private int bounceCounter;
	private double dx, dy;
	private int height;
	private int width;
	private double x, y;

	public MovingButton(int height, int width) {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		
		//設定按鈕參數
		this.height = height - 90;
        this.width = width - 50;
        
        x = Math.random()*400;
        y = Math.random()*400;
        
        setBounds((int)x, (int)y, 50, 50);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true) {
            x = x+ dx;
            y = y+ dy;

            //若撞到邊邊COUNTER+1
            if(x<0){
            	x = -x;
            	dx = -dx;
            	bounceCounter++;
            }
            
            if(x>width){
            	x = -x + 2* width;
            	dx = -dx;
            	bounceCounter++;
            }
            
            if(y<0){
            	y = -y;
            	dy = -dy;
            	bounceCounter++;
            }
            
            if(y>height){
            	y = -y +2* height;
            	dy = -dy;
            	bounceCounter++;
            }
            
            setText(bounceCounter+"");
            setLocation((int)x, (int)y);

            try {
                Thread.sleep(10); 
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
	}

}
