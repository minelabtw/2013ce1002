package ce1002.a12.s102502521;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

import javax.swing.*;

public class A12 implements KeyListener {

	static JFrame jf;
	protected JButton BTN = new JButton("test");

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//加入FRAME
		jf = new JFrame("A12-102502521");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
		//觸發加按鈕
		JButton btn = new MovingButton(500, 500);
        jf.add(btn);

        Thread thread = new Thread((Runnable)btn);
        thread.start();
	}
}
