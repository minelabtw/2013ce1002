package ce1002.a12.s102502503;
import javax.swing.*;
import java.awt.event.*;
public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(400, 100, 500, 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
 
	public void keyPressed(KeyEvent e){
		MovingButton button=new MovingButton();  //建立按鈕物件
		jf.add(button);
		Thread thread=new Thread(button);  //多執行緒
		thread.start();
	}
	public void keyReleased(KeyEvent e){
		
	}
	public void keyTyped(KeyEvent e){
		
	}
}