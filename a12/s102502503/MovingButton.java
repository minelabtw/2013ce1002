package ce1002.a12.s102502503;
import javax.swing.*;
public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;
	private int x=250,y=250;
	private int bounceCounter;
	
	
	public MovingButton()
	{	
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setText(""+bounceCounter);
		setBounds(x,y,50,50);
	}
	public void run() 
	{
		while(true){
		
			if(x==0 || x==432){  //碰到牆
				  dx=-dx;
				  bounceCounter++;
			  }
			if(y==0 || y==400){  //碰到牆
				  dy=-dy;
			      bounceCounter++;
			  }
			setBounds(x,y,50,50);
			setText(""+bounceCounter); //重設按鈕的文字
			x=x+(int)dx;  //往x向量
			y=y+(int)dy;  //往y向量
			try {
				Thread.sleep(10); //延遲時間
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
  }
}