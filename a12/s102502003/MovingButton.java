package ce1002.a12.s102502003;
import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;
	private int bounceCounter;
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		
	}
	public void run()
	{
		int i = 1;
		while(true){  // when the button move
			
			add(new JButton(""+i));  // add a new button
			
		
		if (dx > this.getSize().width){  // hit the x axis
			 dx = dx * (-1);
			 bounceCounter++;
		} 

		if (dy > this.getSize().height){  // hit the y axis
			dy = dy*(-1);
			bounceCounter++;
		}
		
		i++;
		
		}
		
			
	}
	
}
