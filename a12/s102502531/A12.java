package ce1002.a12.s102502531;


import javax.swing.*;

import java.awt.event.*;

public class A12 implements KeyListener{
	static MyFrame jf;
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new MyFrame("A12");
		A12 listener = new A12();  //題目給定的東西
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		MovingButton movingbutton = new MovingButton(jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		Thread thread = new Thread(movingbutton);
		jf.add(movingbutton);
		thread.start();
	}
	public void keyPressed(KeyEvent e) {  //用來鍵盤控制
		JButton jbutton = new MovingButton(jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		Thread thread = new Thread((Runnable)jbutton);
		jf.add(jbutton);
		thread.start();
	}
	public void keyReleased(KeyEvent e) {
	}
	public void keyTyped(KeyEvent e) {
	}
}
