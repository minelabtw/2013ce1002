package ce1002.a12.s102502531;

import javax.swing.*;
import java.util.*;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter;
	private int height;
	private int width;
	private double x ;
	private double y ;
	private double r = Math.random() * 3.14159;

	public MovingButton(int height, int width) { //用來建構一個按鈕
		this.height = height-50;
		this.width = width-50;
		x=((width-100)/2);
		y=((height-100)/2);
		setBounds((int) x, (int) y, 50, 50);
		Random random = new Random();
		switch (random.nextInt(2)) {
		case 0:
			dx = -(3 * Math.cos(r));
			break;
		case 1:
			dx = 3 * Math.cos(r);
			break;
		}
		switch (random.nextInt(2)) {
		case 0:
			dy = -(3 * Math.sin(r));
			break;

		case 1:
			dy = 3 * Math.sin(r);
			break;
		}
	}

	public void run() {
		while (true) {
			x += dx;
			y += dy;  //牆壁的設置與移動
			if (x < 0) {
				dx = -dx;
			}
			if (x > width) {
				dx = -dx;
			}
			if (y < 0) {
				dy = -dy;
			}
			if (y > height ) {
				dy = -dy;
			}
			if (y < 0 || y > height || x < 0 || x > width) {
				bounceCounter++;
			}
			try {
				setLocation((int) x, (int) y);
				setText(bounceCounter + "");
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
