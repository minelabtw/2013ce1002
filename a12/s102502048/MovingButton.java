package ce1002.a12.s102502048;
import java.awt.event.*;
import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
	public final int btnSideLen = 60;
	double r = Math.random()*3.14159;
	public double btnX, btnY, dx, dy;	
    public int bounceCounter;    
    Timer timer = new Timer(10, new TimerListener());
    
	MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
        btnX = 250;
        btnY = 250;
        setBounds((int) btnX, (int) btnY, btnSideLen, btnSideLen);
        setText("" + bounceCounter);   
        timer.start();      
	}
	public void run()
	{
		boolean crash = false;//�P�_�O�_����
        if (btnX + dx < 0) {
        	dx = -dx;
            crash = true;
        }
        if (btnX + dx + btnSideLen >= 500) {
        	dx = -dx;
            crash = true;
        }
        if (btnY + dy < 0) {
        	dy = -dy;
            crash = true;
        }
        if (btnY + dy + btnSideLen >= 470) {
        	dy = -dy;
            crash = true;
        }
        if (crash) {
        	bounceCounter++;
            setText("" + bounceCounter);
        }
        btnX = btnX + dx;
        btnY = btnY + dy;
        setBounds((int) btnX, (int) btnY, btnSideLen, btnSideLen);
        // repaint();
	}
	class TimerListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			run();
		}
	}
}
