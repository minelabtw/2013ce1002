package ce1002.a12.s102502048;
import java.awt.event.*;
import javax.swing.JFrame;

public class A12 implements KeyListener {
	static MovingButton btn;
	static JFrame jf;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
				jf = new JFrame("A12");
				A12 listener = new A12();
				jf.getContentPane().setLayout(null);
				jf.setBounds(100, 100,
						jf.getInsets().left + jf.getInsets().right + 500,
						jf.getInsets().top + jf.getInsets().bottom + 500);
				jf.addKeyListener(listener);
				jf.setVisible(true);
				jf.setFocusable(true);
				jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
				
				btn = new MovingButton();
				jf.add(btn);
				jf.setVisible(true);
	}
	public void keyTyped(KeyEvent e) {
		MovingButton tmp = new MovingButton();
		jf.add(tmp);
	}
	public void keyPressed(KeyEvent e) {
	}
	public void keyReleased(KeyEvent e) {
	}
}
