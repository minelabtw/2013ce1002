package ce1002.a12.s102502536;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable
{
    private double dx,dy;  
	private int bounceCounter;  // the number on the button
	
	public MovingButton()
	{
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setBounds(200, 200, 50, 50); // set size and location
		
	}
    public void run() 
    {
    	try 
    	{
    		int x = 200;
			int y = 200;

			while (true) 
			{   
				x = x + (int)dx;
				y = y + (int)dy;

				if (x < 0 || x > 430) 
				{
					dx = -dx;  // change the direction
					bounceCounter++;
				}
				if (y < 0 || y > 400)
				{
					dy = -dy;
					bounceCounter++;
				}
				
				Thread.sleep(10); 
				setLocation(x, y);  // change the location
				setText("" + bounceCounter);
			}

		}
    	catch (Exception e)  // catch exception
    	{
			e.printStackTrace();
		}

    }
}