package ce1002.a12.s102502536;

import javax.swing.*;
import java.awt.event.*;

public class A12 extends KeyAdapter implements KeyListener {
	
	static JFrame jf;
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12(); 
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton button = new MovingButton(); // create button
		Thread thread = new Thread(button, "button");
		thread.start();

	}
 
	@Override
	public void keyPressed(KeyEvent e) {
		
		MovingButton button = new MovingButton(); // create button
		Thread thread = new Thread(button, "button");
		jf.add(button);  // add button to frame
		thread.start();  // start run()

	}
	
	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}




}