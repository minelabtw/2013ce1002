package ce1002.a12.s102502023;

import java.awt.event.*;

import javax.swing.*;
public class A12 implements KeyListener
{
	static JFrame jf; // initialize static JFrame jf
	public static void main(String[] args) 
	{

		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton a = new MovingButton();
		jf.add(a);
		Thread thread = new Thread(a);
		thread.start();
		//System.out.println(jf.getWidth());
		//System.out.println(jf.getHeight());
		//...
	}
     
   

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		MovingButton b = new MovingButton();
		jf.add(b);
		Thread thread = new Thread(b);
		thread.start();
		//System.out.println("Hello");
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
