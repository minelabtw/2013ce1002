package ce1002.a12.s102502023;


import javax.swing.*;


public class MovingButton extends JButton implements Runnable
{
    private double dx,dy; // initialize double dx, dy
	private int bounceCounter; // initialize bounceCounter
	private int x = 225; private int y = 225; // initialize integer x to 225 and y to 225
	
	public MovingButton()
	{
		String s = Integer.toString(bounceCounter);
		setText(s);
		double r =  Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setBounds(x, y, 50, 50);
	}
	
	
  public void run() 
  {
    //...
	  for (;;) {
	  try {
		  if (x < 0 || (x + 50) > 500) {
		  dx *= -1;
		  bounceCounter++;
	  }
	  if (y < 0 || (y + 50) > 500) {
		  dy *= -1;
		  bounceCounter++;
	  }
	  
	  x += dx;
	  y += dy;
	  //setBounds(x, y, 50, 50);
	  Thread.sleep(15);
	  setLocation(x, y);
	  String s = Integer.toString(bounceCounter);
	  setText(s);
	  }
	  catch (InterruptedException e) {
		  e.printStackTrace();
	  }
  }
}
}
