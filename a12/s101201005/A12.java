package ce1002.a12.s101201005;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton MB = new MovingButton(500 - jf.getInsets().left
				- jf.getInsets().right, 500 - jf.getInsets().top
				- jf.getInsets().bottom); //x扣掉旁邊兩個邊邊，y扣掉上面下面兩個邊邊
		jf.add(MB);

		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(MB, 0, 1, TimeUnit.MILLISECONDS);
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) { //按一下執行
		// TODO Auto-generated method stub
		MovingButton MB = new MovingButton(500 - jf.getInsets().left
				- jf.getInsets().right, 500 - jf.getInsets().top
				- jf.getInsets().bottom);
		jf.add(MB);

		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
		service.scheduleAtFixedRate(MB, 0, 1, TimeUnit.MILLISECONDS);

	}

}