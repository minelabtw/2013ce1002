package ce1002.a12.s101201005;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private double x, y, width, height;
	private int bounceCounter;

	public MovingButton(int width, int height) {
		this.width = width - 50;
		this.height = height - 50;
		double r = Math.random() * 3.14159;
		dx = Math.cos(r) / 5;
		dy = Math.sin(r) / 5;
		x=width/2-25;
		y=height/2-25;
		setBounds(width / 2 - 25, height / 2 - 25, 50, 50);

		bounceCounter = 0;
		setLabel(Integer.toString(bounceCounter));
	}

	public void run() {

		x = x + dx;
		y = y + dy;
		if (x < 0) { //碰到左邊框框時
			x = (-1) * x;
			dx = (-1) * dx;
			bounceCounter++;
		} else if (x > width) { //碰到右邊框框時
			x = 2 * width - x;
			dx = (-1) * dx;
			bounceCounter++;
		}
		if (y < 0) { ///碰到上面框框時
			y = (-1) * y;
			dy = (-1) * dy;
			bounceCounter++;
		} else if (y > height) {    //碰到下面框框時
			y = 2 * height - y;
			dy = (-1) * dy;
			bounceCounter++;
		}
		setLocation((int) x, (int) y);
		setLabel(Integer.toString(bounceCounter));
	}
}
