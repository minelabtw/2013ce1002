package ce1002.a12.s102502049;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Bpanel extends JPanel{
	private int width = 500;
	private int height = 500;
	
	int number = 0;
	
	private ArrayList<MovingButton> buttons = new ArrayList<MovingButton>();	//	array list
	
	Bpanel(){
		MovingButton dbutton = new MovingButton();
		buttons.add(dbutton);	//	add default button
		Thread dthread = new Thread(dbutton);
		add(dbutton);
		number++;
		dthread.start();
	}
	
	public void addButton(){
		MovingButton button = new MovingButton();
		buttons.add(button);	//	add button
		number++;
		Thread thread = new Thread(button);
		thread.start();
		this.add(button);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		try{	//	sleep 10 m-second
			Thread.sleep(10);			
		}
		catch(InterruptedException e) { 
			e.printStackTrace(); 
		}
		
		for(int i=0; i<number; i++){	//	add to panel
			buttons.get(i).setSize(width/9, height/9);
			buttons.get(i).setLocation(buttons.get(i).getxPos(), buttons.get(i).getyPos());
			buttons.get(i).run();
		}
		
	}
}
