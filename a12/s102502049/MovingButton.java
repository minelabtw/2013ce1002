package ce1002.a12.s102502049;

import java.awt.Graphics;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx = 0, dy = 0;
	private int bounceCounter = 0;
	private int width = 500;
	private int height = 500;

	private double xPos = (int) (width / 2);
	private double yPos = (int) (height / 2);

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r); // unit vector
		dy = 3 * Math.sin(r);
		setText(""+bounceCounter);
	}

	public int getxPos() {	/*getter*/
		return (int) (xPos);
	}

	public int getyPos() {
		return (int) (yPos);
	}

	public void run() {
		
		if (xPos > width-70 || xPos < 0) { // reverse direction when collision
			bounceCounter++;
			setText(""+bounceCounter);
			dx *= -1;
		} else if (yPos > height-80 || yPos < 0) {
			bounceCounter++;
			setText(""+bounceCounter);
			dy *= -1;
		}
		xPos += dx;	//	move
		yPos -= dy;
	}
}