package ce1002.a12.s102502049;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JFrame;

public class BFrame extends JFrame{
	private int width = 500;
	private int height = 500;
	
	private Bpanel panel = new Bpanel();

	BFrame(){
		setSize(width,height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		
		add(panel);
		
		addKeyListener(new KeyAdapter() {	//	call panel add button
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				panel.addButton();
			}
		});
	}
	
	
}
