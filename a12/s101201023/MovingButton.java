package ce1002.a12.s101201023;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;
	double location[]=new double [2];
	private int bounceCounter=0;
	int width; int height;
	
	//set coordinate and title of button
	public MovingButton(int w , int h)
	{
		double r = Math.random()*3.14159;
		dx=Math.cos(r)/10;
		dy=Math.sin(r)/10;
		width = w;
		height = h;
		String number=Integer.toString(bounceCounter);
		setLabel(number);
		setBounds(width/2-30,height/2-20,60,40);
		location[0]=width/2-30;
		location[1]=height/2-20;
	}
	
	//judge rebounce
	@Override
	public void run() 
	{
		// TODO Auto-generated method stub
		for(int i=0 ; i < 2 ; i++)
		{
			location[0] = location[0] + dx;
			location[1] = location[1] + dy;
			
			if(location[0] < 0)
			{
				location[0] = -1 * location[0];
				dx = -1 * dx;
				bounceCounter++;
				String number=Integer.toString(bounceCounter);
				setLabel(number);
			}
			
			else if(location[1] < 0)
			{
				location[1] = -1 * location[1];
				dy = -1 * dy;
				bounceCounter++;
				String number=Integer.toString(bounceCounter);
				setLabel(number);
			}
			
			else if(location[0] > width-60)
			{
				location[0] = 2 * (width-60)-location[0];
				dx = -1 * dx;
				bounceCounter++;
				String number=Integer.toString(bounceCounter);
				setLabel(number);
			}
			
			else if(location[1] > height-40)
			{
				location[1] = 2 * (height-40)-location[1];
				dy = -1 * dy;
				bounceCounter++;
				String number=Integer.toString(bounceCounter);
				setLabel(number);
			}
		}
		super.setLocation((int)location[0], (int)location[1]);
	}
}