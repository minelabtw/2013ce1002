package ce1002.a12.s101201023;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	
	//set frame
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Button();
	}
	
	//set button
	public static void Button()
	{
		MovingButton Mbutton=new MovingButton(500-jf.getInsets().left-jf.getInsets().right,500-jf.getInsets().top-jf.getInsets().bottom);
		jf.add(Mbutton);
		ScheduledExecutorService service = Executors.newScheduledThreadPool(2);
        service.scheduleAtFixedRate(Mbutton, 0, 1, TimeUnit.MILLISECONDS);
	}
	
	@Override
	public void keyPressed(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
	
	//按下一個鍵時會多一個按鈕
	@Override
	public void keyTyped(KeyEvent e) 
	{
		// TODO Auto-generated method stub
		Button();
	}
}
