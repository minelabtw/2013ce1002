package ce1002.a12.s102502031;

import java.awt.Dimension;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private int width;
	private int height;

	private MyPanel() {
		this.setLayout(null);
	} // end constructor MyPanel()

	public MyPanel(int width, int height) {
		this();
		this.width = width;
		this.height = height;
	} // end constructor MyPanel(width, height)

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	} // end getPreferredSize()
} // end MyPanel