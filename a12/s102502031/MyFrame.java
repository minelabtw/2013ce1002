package ce1002.a12.s102502031;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	private MyFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	} // end constructor MyFrame()

	public MyFrame(String title) {
		this();
		this.setTitle(title);
	} // end constructor MyFrame(title)
} // end class MyFrame