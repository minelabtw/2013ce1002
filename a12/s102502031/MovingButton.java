package ce1002.a12.s102502031;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton implements Runnable {
	private int bounceCounter = 0;
	private double x;
	private double y;
	private double dx; // direction of x
	private double dy; // direction of y
	private int panelWidth;
	private int panelHeight;

	Random random = new Random();

	private MovingButton() {
		this.setSize(60, 30);
		this.setText(bounceCounter + "");
		double radian = random.nextDouble() * 2;
		dx = Math.cos(radian);
		dy = Math.sin(radian);
	} // end constructor MovingButton()

	public MovingButton(int panelWidth, int panelHeight) {
		this();
		this.panelWidth = panelWidth;
		this.panelHeight = panelHeight;
		x = random.nextInt(panelWidth - this.getWidth());
		y = random.nextInt(panelHeight - this.getHeight());
		this.setLocation((int) x, (int) y);
		Timer timer = new Timer(1, new TimerListener());
		timer.start();
	} // end constructor MovingButton(panelWidth, panelHeight)

	@Override
	public void run() {
	} // empty method

	private class TimerListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			if (x + dx < 0) {
				dx = -dx;
				bounceCounter++;
			} // end if
			else if (x + dx + getWidth() > panelWidth) {
				dx = -dx;
				bounceCounter++;
			} // end else if
			if (y + dy < 0) {
				dy = -dy;
				bounceCounter++;
			} // end if
			else if (y + dy + getHeight() > panelHeight) {
				dy = -dy;
				bounceCounter++;
			} // end else if
			x = x + dx;
			y = y + dy;
			setText(bounceCounter + "");
			setLocation((int) x, (int) y);
		} // end actionPerformed(actionEvent)
	} // end class TimerListener
} // end class MovingButton 