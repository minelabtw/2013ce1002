package ce1002.a12.s102502031;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class A12 implements KeyListener {
	final static int width = 500;
	final static int height = 500;
	static MyFrame frame = new MyFrame("A12-102502031");
	static MyPanel panel = new MyPanel(width, height);

	public static void main(String[] args) {
		A12 listener = new A12();

		frame.add(panel);
		frame.addKeyListener(listener);
		frame.pack();
		frame.setFocusable(true);

		// the first moving button
		creatMovingButton(panel);
	} // end main

	@Override
	public void keyPressed(KeyEvent keyEvent) {
		creatMovingButton(panel);
		panel.repaint();
	} // end keyPressed(keyEvent)

	@Override
	public void keyReleased(KeyEvent keyEvent) {
	} // empty method

	@Override
	public void keyTyped(KeyEvent keyEvent) {
	} // empty method

	private static void creatMovingButton(MyPanel panel) {
		MovingButton movingButton = new MovingButton(panel.getWidth(), panel.getHeight());
		panel.add(movingButton);
	} // end creatMovingButton(panel)
} // end A12