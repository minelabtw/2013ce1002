package ce1002.a12.s102502556;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy; //每次的移動量
	private int bounceCounter = 0; //撞擊次數
	private int jfx = 0; //視窗x長度
	private int jfy = 0; //視窗y長度
	private int jbw = 50; //按鈕x長度
	private int jbh = 50; //按鈕y長度
	Thread moving = new Thread(this); //宣告一個Thread

	public MovingButton(int x, int y) {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		setText("" + bounceCounter); 
		jfx = x;
		jfy = y;
		x = (jfx - jbw) / 2;
		y = (jfy - jbh) / 2;
		setBounds(x, y, jbw, jbh); //設定按鈕位置和大小
		moving.start(); //call Thread 的 start 函數
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(50); //暫停50ms
				double x = getLocation().x; //取得按鈕當前x座標
				double y = getLocation().y; //取得按鈕當前y座標
				//如果按鈕尚未碰到邊界
				if (x + dx >= 0 && x + dx + jbw + 15 <= jfx && y + dy >= 0
						&& y + dy + jbh + 40 <= jfy) {
					x += dx;
					y += dy;
					setLocation((int) x, (int) y);//設定新的位置
				} 
				//如果按鈕碰到了邊界
				else {
					bounceCounter++; //撞擊次數+1
					setText("" + bounceCounter);
					//如果是撞到x,就讓dx反向
					if (x + dx < 0 || x + dx + jbw + 15 > jfx) {
						dx *= -1.0f;
					}
					//如果是撞到y,就讓dy反向
					if (y + dy < 0 || y + dy + jbh + 40 > jfy) {
						dy *= -1.0f;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}