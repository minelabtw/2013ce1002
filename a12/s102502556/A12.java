package ce1002.a12.s102502556;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf; //宣告一個JFrame
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		jf = new JFrame("A12-102502556");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton first = new MovingButton(jf.getWidth(), jf.getHeight()); //宣告一個MovingButton
		jf.add(first); //把MovingButton加到視窗裡
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		MovingButton other = new MovingButton(jf.getWidth(), jf.getHeight()); //宣告一個MovingButton
		jf.add(other); //把MovingButton加到視窗裡
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}