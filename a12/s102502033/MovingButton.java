package ce1002.a12.s102502033;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.Timer;

public class MovingButton extends JButton
{
	private double x = 250;
	private double y = 250;
	private double dx;
	private double dy;
	private int calcular=0;
	private Timer timer = new Timer(7, new TimerListener());

	public MovingButton()
	{
		setLocation((int) x, (int) y);
		setSize(50, 30);
		setText(""+calcular);
		double r = Math.random() * Math.PI * 2;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		timer.start();//時間的變化
	}

	class TimerListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (x+dx<=0 || x+dx>=440)//改變方向
			{
				dx=-dx;
				calcular=calcular+1;
				setText(""+calcular);
			}
			if(y+dy<=0 || y+dy>=430)
			{
				dy=-dy;
				calcular=calcular+1;
				setText(""+calcular);
			}
			
			x=x+dx;
			y=y+dy;
			setLocation((int) x, (int) y);//依照向量改變位置
		}

	}

}
