package ce1002.a12.s102502033;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class A12 implements KeyListener
{
	static MyFrame myFrame = new MyFrame();
	
	public static void main(String[] args)
	{
		myFrame.setLayout(null);
		myFrame.setSize(500, 500);
		myFrame.setLocationRelativeTo(null);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		A12 listener = new A12();		
	    myFrame.setFocusable(true);//avaluable listener
		myFrame.addKeyListener(listener);
	}
	

	@Override
	public void keyPressed(KeyEvent e)
	{
		MovingButton movingButton = new MovingButton();
		myFrame.add(movingButton);//press and add button
	}

	@Override
	public void keyReleased(KeyEvent e)
	{		
		
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		
	}

	

}
