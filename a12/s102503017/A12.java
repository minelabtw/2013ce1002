package ce1002.a12.s102503017;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class A12 implements KeyListener{

	@Override
	public void keyPressed(KeyEvent e) {
		//when pressed any key, add an MovingButton object and count it as a Thread, then start the thread and add the object to the frame.
		MovingButton button = new MovingButton();
		Thread thread = new Thread(button);
		thread.start();
		jf.add(button);
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	static JFrame jf;
	public static void main(String[] args) 
	{
		MovingButton button = new MovingButton();
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this is the  default button on the frame when initialized the program.
		Thread thread = new Thread(button);
		thread.start();
		jf.add(button);
	}

	

}
