package ce1002.a12.s102503017;

import javax.swing.*;


public class MovingButton extends JButton implements Runnable{
	
	private double dx,dy;
	private int bounceCounter = 0;
	int direction, x, y;
	
	public MovingButton()
	{
		bounceCounter = 0;
		//default constructor of MovingButton.
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		//the starting x-position of the button is from 0 to (the edge of the frame - width of the button).
		x = (int)(Math.random()*(420-60-2)+2);
		//y-position is same as the calculations of x-position.
		y = (int)(Math.random()*(405-50-2)+2);
		//the vector direction of the button.
		direction = (int)(Math.random()* 4 + 1);
		setBounds(x,y,60,50);
		//set the bounceCounter value on the button.
		setText("" + bounceCounter);
	}
	
	public void run() 
	{
		while(true)//infinte loop.
		{

			//the ��v of the button refer to the value of the direction.
			switch(direction)
			{			
				case 1:
				 dx += 2;
				 dy += 2;
				 break;
				case 2:
				 dx -= 2;
				 dy += 2;
				 break;
				case 3:
				 dx += 2;
				 dy -= 2;
				 break;
				case 4:
				 dx -= 2;
				 dy -= 2;
				 break;
			}
			
			//velocity of x-axis or y-axis = x-position or y-position + ��v.
			int vecx = (int)(x+dx);
			int vecy = (int)(y+dy);
			
			//when button touches the edge of the frame, change the direction into opposite direction, whenever the buttons touches the edges,
			//bounceCounter increases.
			if(vecx <= 0 || vecx >= 420 || vecy <= 0 || vecy >= 405)
			{
				switch(direction)
				{
					case 1:
						direction = 4;
						break;
					case 2:
						direction = 3;
						break;
					case 3:
						direction = 2;
						break;
					case 4:
						direction = 1;
						break;
				}
				bounceCounter++;
				setText("" + bounceCounter);
			}
			//change the current position of the button.
			setBounds(vecx,vecy,60,50);
			try {
				//delay of the printing.
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//refresh the screen via repaint all elements.
			repaint();	
		}
			
	}

}
