package ce1002.a12.s102502543;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	private int fx = 0;
	private int fy = 0;
	private int bw = 50;
	private int bh = 50;
	Thread moving = new Thread(this);

	public MovingButton(int x, int y) {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		setText("" + bounceCounter);
		fx = x;
		fy = y;
		x = (fx - bw) / 2;
		y = (fy - bh) / 2;
		setBounds(x, y, bw, bh);
		moving.start();
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(30);
				double x = getLocation().x;
				double y = getLocation().y;
				if (x + dx >= 0 && x + dx + bw + 15 <= fx && y + dy >= 0
						&& y + dy + bh + 40 <= fy) {
					x = x + dx;
					y = y + dy;
					setLocation((int) x, (int) y);
				} else {
					bounceCounter++;
					setText("" + bounceCounter);
					if (x + dx < 0 || x + dx + bw + 15 > fx) {
						dx = dx * (-1.0f);
					}
					if (y + dy < 0 || y + dy + bh + 40 > fy) {
						dy = dy * (-1.0f);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}