package ce1002.a12.s102502543;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener {
	static JFrame jf;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		jf = new JFrame("A12-102502543");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300,
				jf.getInsets().left + jf.getInsets().right + 500,
				jf.getInsets().top + jf.getInsets().bottom + 500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MovingButton first = new MovingButton(jf.getWidth(), jf.getHeight());
		jf.add(first);
	}

	public void keyPressed(KeyEvent e) {
		MovingButton other = new MovingButton(jf.getWidth(), jf.getHeight());
		jf.add(other);
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}
}