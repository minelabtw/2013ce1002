package ce1002.a12.s102502520;

import javax.swing.*;


public class Moving extends JButton implements Runnable {
    int h;
    int w;
    int bounce_counter;
    double x;
    double y;
    double dx;
    double dy;

    
    Moving(int h, int w) {
        this.h = h - 80;
        this.w = w - 50;
        x = w / 2;
        y = h / 2;

        setBounds((int)x, (int)y, 50, 50);

        double r = Math.random() * Math.PI * 2;
        dx = 3 * Math.cos(r);
        dy = 3 * Math.sin(r);
    }

   //����
    public void run() {
        while(true) {
            x += dx;
            y += dy;

            if(x<0 || x>w || y<0 || y>h) {
                if(x < 0) {
                    x = -x;
                    dx = -dx;
                } else if (x > w) {
                    x = -x + 2* w;
                    dx = -dx;
                }

                if(y < 0) {
                    y = -y;
                    dy = -dy;
                } else if (y > h) {
                    y = -y +2* h;
                    dy = -dy;
                }
                bounce_counter++;
            }

            setText(bounce_counter+"");//�r��
            setLocation((int)x, (int)y);

            try {
                Thread.sleep(10); 
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}