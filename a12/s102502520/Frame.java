package ce1002.a12.s102502520;

import java.util.*;
import javax.swing.*;
import java.awt.event.*;

class Frame extends JFrame {

    List<JButton> cells = new ArrayList<JButton>();

    //config this frame
    Frame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//視窗關閉
        setLocationRelativeTo(null);//視窗位置
        setLayout(null);//排版
        setTitle("A12-102502520");//標題
        setSize(800, 600);

        KeyListener listener = new Key_listener();
        addKeyListener(listener);

        setVisible(true);
    }

  
    class Key_listener implements KeyListener {

        public void keyPressed(KeyEvent e) {
        }

        public void keyReleased(KeyEvent e) {
        }

        public void keyTyped(KeyEvent e) {
            JButton cell = new Moving(getHeight(), getWidth());
            cells.add(cell);
            add(cell);

            Thread thread = new Thread((Runnable)cell);
            thread.start();
        }
    }
}