package ce1002.a12.s102502546;

import javax.swing.*;
import java.awt.*;

public class MovingButton extends JButton implements Runnable {
	private double XD, YD;
	double X, Y;
	int XL, YL;
	private int bounceCounter;
	int speed = 50;
		
	public MovingButton(int XL, int YL) { 
		this.XL = XL;
		this.YL = YL;
		X = XL / 2 - 40;
		Y = YL / 2 - 50;
		setBounds((int) X, (int) Y, 50, 50);
		double r = Math.random() * 3.14159;
		XD = 3 * Math.cos(r);
		YD = 3 * Math.sin(r);

	}

	public void run() { 

		while (true) {
			if (!(X + 50 >= XL || Y + 50 >= YL || X <= 0 || Y <= 0)) {
				X += XD;//continue torwards moving direction
				Y += YD;
			} else {  //�������
				
				if (X + 50 > XL|| X < 0) {
					Y += YD;
					XD = -XD;
					X += XD;
				}
				if (Y + 50 > YL || Y < 0) {
					X += XD;
					YD = -YD;
					Y += YD;
				}
				bounceCounter++;
			}
				
			
			setBounds((int) X, (int) Y, 50, 50); //�]�w��m
			setText(bounceCounter + "");

			try {
				Thread.sleep(speed);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

}