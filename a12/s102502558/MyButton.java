package ce1002.a12.s102502558;

import javax.swing.JButton;

public class MyButton extends JButton implements Runnable
{
	private int count = 0;
	private double x, y;
	private double dx, dy;
	private static final int size = 50;
	MyButton(int w, int h)
	{
		x = (w - size) / 2;
		y = (h - size) / 2;
		setBounds((int)x, (int)y, size, size);
		setText("" + count);
		double r = Math.random() * Math.PI * 2;
        dx = 3 * Math.cos(r);
        dy = 3 * Math.sin(r);
	}
	
	public void run()
	{
		while(true)
		{	
			x += dx;
			y += dy;
			if (x < 0 || x > A12.WIDTH - size)
			{
				x -= dx;
				dx = -dx;
				count++;
			}
			if (y < 0 || y > A12.HEIGHT - size)
			{
				y -= dy;
				dy = -dy;
				count++;
			}
			setLocation((int)x, (int)y);
			setText(""+count);
			try {
                Thread.sleep(10); 
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
		}
	}
}
