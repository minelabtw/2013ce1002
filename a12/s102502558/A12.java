package ce1002.a12.s102502558;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	public static final int WIDTH = 800;
	public static final int HEIGHT = 600;
	static JFrame jf;
	public static void main(String[] args)
	{
		jf = new JFrame("A12-102502558");
		A12 listener = new A12();
		jf.setLayout(null);
		//jf.setSize(jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.setSize(WIDTH, HEIGHT);
		jf.setLocationRelativeTo(null);
		jf.addKeyListener(listener);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.setVisible(true);
	}

	@Override
	public void keyPressed(KeyEvent arg0)
	{
		MyButton btn = new MyButton(WIDTH, HEIGHT); 
		jf.add(btn);
		jf.repaint();
		Thread thread = new Thread((Runnable)btn);
        thread.start();
	}
	@Override
	public void keyReleased(KeyEvent arg0) {}
	@Override
	public void keyTyped(KeyEvent arg0) {}
}
