package ce1002.a12.s102502528;

import java.util.*;

import javax.swing.*;

import java.awt.event.*;

public class A12 extends JFrame implements KeyListener{

	A12() {//nothing special
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setTitle("A12-102502044");
		setSize(800, 600);
		setLocationRelativeTo(null);
		addKeyListener(this);

		setVisible(true);
		
	}

	public static void main(String[] args) {
		new A12();

	}

	public void keyPressed(KeyEvent arg0) {
		
	}

	public void keyReleased(KeyEvent arg0) {
		
	}

	public void keyTyped(KeyEvent arg0) {
        JButton button = new MovingButton(getHeight(), getWidth());
        add(button);

        Thread thread = new Thread((Runnable)button);//thread
        thread.start();
	}
	
}
