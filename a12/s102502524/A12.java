package ce1002.a12.s102502524;

import java.util.*;

import javax.swing.*;

import java.awt.event.*;


public class A12 extends JFrame implements KeyListener
{
	List<JButton> cells = new ArrayList<JButton>();
	
	static JFrame jf;
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub		
		jf = new JFrame("A12");
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
		JButton cell = new Moving_cell(getHeight(), getWidth());
        cells.add(cell);
        add(cell);

        Thread thread = new Thread((Runnable)cell);
        thread.start();
		
	}
 
  
}