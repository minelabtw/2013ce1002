package ce1002.a12.s102502037;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MovingButton extends JButton implements Runnable{
	private double dx, dy;
	private int Counter;
	int a,b;

	public MovingButton() {
		double r = Math.random() * 2 * 3.14159;//讓按鈕出現
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		a = (int) (440 * Math.random());
		b = (int) (420 * Math.random());
		setBounds(a, b, 50, 50);
		ActionListener buttonListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println(Counter);
			}
		};
		addActionListener(buttonListener);
	}

	public void run() {//跑的速度 跑的方向
		try {
			while(true) {
				if (a >= 440 || a <= 0) {
					dx *= -1;
					Counter++;
				}
				if (b >= 420 || b <= 0) {
					dy *= -1;
					Counter++;
				}
				a = a + (int) dx;
				b = b + (int) dy;
				setLocation(a, b);
				setText("" + Counter);
				Thread.sleep(20);
			}
		} catch (InterruptedException e) {
			 e.printStackTrace();
		}
	}
}
