package ce1002.a12.s102502042;
import javax.swing.*;
public class MovingButton extends JButton implements Runnable
{
	private double dx,dy;
	private int bounceCounter;
	private double x,y;
	private int width,height;
	
	public MovingButton(int width, int height)
	{
		//初始化
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		bounceCounter = 0;
		this.x = width/3;
		this.y = height/3;
		this.width = width;
		this.height = height;
		this.setSize(50,50);
		
	}
    public void run() 
    {
    	while(true){
	    	if(x + dx < 0 || x + dx > width)
	    	{
	    		dx = -dx;
	    		bounceCounter++;
	    	}
	
	    	if(y + dy <0 || y + dy > height)
	    	{
	    		dy = -dy;
	    		bounceCounter++;
	    	}
	    	x+=dx;
	    	y+=dy;
	    	//更新位置、文字
	    	this.setLocation((int)x, (int)y);
	    	this.setText(bounceCounter+"");
	    	
	    	//延時
	    	try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	
    }
}
