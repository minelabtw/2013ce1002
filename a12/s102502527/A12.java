package ce1002.a12.s102502527;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	
	public static final int WIDTH = 500;//視窗的彎度
	public static final int HEIGHT = 500;//視窗的高度
	
	public static void main(String[] args)
	{
		jf = new JFrame("A12-102502558");//視窗名稱
		A12 listener = new A12();
		
		jf.setSize(WIDTH, HEIGHT);//視窗大小
		jf.setLocationRelativeTo(null);//視窗置中
		jf.getContentPane().setLayout(null);//内容面板
		jf.addKeyListener(listener);//用來設定元件鍵盤按鍵
		jf.setVisible(true);//設定視窗顯現
		jf.setFocusable(true);//焦點設定到JFrame上
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//給予叉叉鍵離開的功能
	}

	@Override
	public void keyPressed(KeyEvent key)//用來設定鍵盤按鍵按住的動作
	{
		MovingButton m = new MovingButton(WIDTH, HEIGHT); 
		jf.add(m);
		jf.repaint();
		Thread t = new Thread((Runnable)m);
        t.start();
	}
	
	@Override
	public void keyReleased(KeyEvent key) {}
	
	@Override
	public void keyTyped(KeyEvent key) {}
}

