package ce1002.a12.s102502527;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private int size = 50;//設定大小
	private double dx, dy;
		private int bounceCounter = 0;//計算次數	
		private double x, y;
	
	
		public MovingButton(int width, int height)
		{
			double r = Math.random() * 3.14159;
			dx = 3 * Math.cos(r);
			dy = 3 * Math.sin(r);
			
			x = (width - size) / 2;
			y = (height - size) / 2;
			
			setBounds( (int)x , (int)y , size , size );
			setText( "" + bounceCounter );
			
		}
	
	public void run()//計算次數的函示
	{
		while( true )
		{	
			x += dx;
			y += dy;
			if ( x < 0 || x > A12.WIDTH - size )
			{
				x -= dx;
				dx = -dx;
				bounceCounter++;
			}
			if ( y < 0 || y > A12.HEIGHT - size )
			{
				y -= dy;
				dy = -dy;
				bounceCounter++;
			}
			
			setLocation( (int)x , (int)y );
			setText( "" + bounceCounter );
			
			try 
			{
                Thread.sleep(10); 
            }
			catch(InterruptedException i) {
                i.printStackTrace();
            }
		}
	}
}