package ce1002.a12.s102502016;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 extends KeyAdapter {//轉機器
	static JFrame jf;
	private static MovingButton a = new MovingButton();

	public static void main(String[] args) {
		jf = new JFrame("A12");
		jf.getContentPane().setLayout(null);
		jf.setSize(500, 500);
		jf.addKeyListener(new A12());
		jf.add(a);
		new Thread(a).start(); // 開始移動
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(3);
	}

	public void keyPressed(KeyEvent e) {
		MovingButton a = new MovingButton();
		jf.add(a);
		new Thread(a).start(); // 開始移動
	}
}