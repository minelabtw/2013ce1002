package ce1002.a12.s102502016;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx, dy;
	private int bounceCounter = 0;
	private double x = 225;
	private double y = 225;

	public MovingButton() {
		double r = Math.random() * 3.14159;
		dx = 3 * Math.cos(r);
		dy = 3 * Math.sin(r);
		this.setSize(50, 50);
		this.setLocation(250, 240);
		this.setText(Integer.toString(bounceCounter));
	}

	public void run() {
		while (true) {//一直做
			if (y > 420 || x > 440 || x < 0 || y < 0) {// 判斷撞擊牆壁
				if (y > 420 || y < 0) {
					dy = -dy;
					bounceCounter++;
				}
				if (x > 440 || x < 0) {
					dx = -dx;
					bounceCounter++;
				}
				this.setText(Integer.toString(bounceCounter));
			}
			x = x + dx;
			y = y + dy;
			this.setLocation((int) x, (int) y);
			try {
				Thread.sleep(10);// 移動間格
			} catch (InterruptedException e) {
				System.out.println("Interrupted: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
}