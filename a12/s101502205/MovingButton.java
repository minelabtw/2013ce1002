package ce1002.a12.s101502205;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable {
	private double dx,dy;
	private int bounceCounter = 0;
	private int maxWidth, maxHeight;

	public MovingButton(int w, int h) {
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		
		maxWidth = w;
		maxHeight = h;
		// set size and text & random the location
		setBounds((int)(Math.random()*maxWidth-50),(int)(Math.random()*maxHeight-50),50,50);
		setText(Integer.toString(bounceCounter));
	}
	
	public void run() {
		int x = getLocation().x;
		int y = getLocation().y;
		while(true){
			// Prevent the button stuck on the wall
			if (x != (int)Math.round(x+dx)){
				x = (int)Math.round(x+dx);
			}else{
				dx=3*Math.sin(Math.random()*3.14159);
			}
			// Prevent the button stuck on ceiling or ground
			if (y != (int)Math.round(y+dy)){
				y = (int)Math.round(y+dy);
			}else{
				dy=3*Math.sin(Math.random()*3.14159);
			}

			// let's bounce !
			if(x<=0) {
				x = 0;
				bounceCounter++;
				dx = -1*dx;
				setText(Integer.toString(bounceCounter));
			}else if(x >= maxWidth-50) {
				x = maxWidth-50;
				bounceCounter++;
				dx = -1*dx;
				setText(Integer.toString(bounceCounter));
			}
			if(y<=0) {
				y = 0;
				bounceCounter++;
				dy = -1*dy;
				setText(Integer.toString(bounceCounter));
			}else if(y >= maxWidth-50) {
				y = maxWidth-50;
				bounceCounter++;
				dy = -1*dy;
				setText(Integer.toString(bounceCounter));
			}
			
			// set location
			setLocation(x, y);
			
			// not too fast
			try{
				Thread.sleep((int)(10));
			}catch(InterruptedException ie){
				ie.printStackTrace();
				System.out.println(ie.getMessage());
			}
		}
	}
}
