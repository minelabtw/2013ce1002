package ce1002.a12.s101502205;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;

public class A12 implements KeyListener{

	static JFrame jf;
	public static void main(String[] args) 
	{
		// Create frame
		jf = new JFrame("A12-101502205");
		// same as the given code 
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(300, 300, jf.getInsets().left+jf.getInsets().right+500, jf.getInsets().top+jf.getInsets().bottom+500);
		jf.addKeyListener(listener);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void keyTyped(KeyEvent ke) {
		// key typed
		// do nothing
	}

	public void keyReleased(KeyEvent ke) {
		// key released
		// get some candies
	}
	
	public void keyPressed(KeyEvent ke) {
		// key pressed
		// Create button
		MovingButton mbtn = new MovingButton(jf.getWidth()-jf.getInsets().left-jf.getInsets().right, jf.getHeight()-jf.getInsets().top-jf.getInsets().bottom);
		
		// add button to frame
		jf.add(mbtn);
		
		// add new thread
		Thread thread = new Thread(mbtn);
		// start the thread
		thread.start();
	}
}
