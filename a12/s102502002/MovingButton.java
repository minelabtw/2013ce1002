package ce1002.a12.s102502002;

import javax.swing.JButton;

public class MovingButton extends JButton implements Runnable
{
	private double dx=0;
	private double dy=0;
	private int bounceCounter=0;
		
	public MovingButton(){ // set random position
		double r = Math.random()*3.14159;
		dx=3*Math.cos(r);
		dy=3*Math.sin(r);
		setBounds(100,100,50,50);
	}

	@Override
	public void run() 
	{
		int mx=100;
		int my=100;
		while(true)  
        {  
			try{  
            	mx = (int) (mx+dx); // make it move
            	my = (int) (my+dy);
                Thread.sleep(10);
            }catch(InterruptedException e ){  
                e.printStackTrace();  
            }  
            
            if(mx<0||mx>430){  
                dx=-dx;  // change direction
                bounceCounter++;
            }  
            if(my<0||my>410){  
                dy=-dy;  // change direction
                bounceCounter++;
            }  
            setBounds(mx,my,50,50);
            setText("" + bounceCounter); // output times pushed to the wall
        }
	}
}


