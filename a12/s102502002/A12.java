package ce1002.a12.s102502002;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class A12 implements KeyListener
{
	static JFrame jf;
	public static void main(String[] args){ 	
		// TODO Auto-generated method stub
		jf = new JFrame("A12-102502002"); // set frame
		A12 listener = new A12();
		jf.getContentPane().setLayout(null);
		jf.setBounds(500, 300, 500, 500);
		jf.setVisible(true);
		jf.setFocusable(true);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MovingButton mb = new MovingButton();
		jf.add(mb);
		jf.addKeyListener(listener);
		Thread thread = new Thread(mb);
		thread.start();// start thread
		
	}

	@Override
	public void keyTyped(KeyEvent e){
		MovingButton btn = new MovingButton();//add new button after pushing space
		Thread thread = new Thread(btn);
		jf.add(btn);
		thread.start();
	
	}
	@Override
	public void keyPressed(KeyEvent e){
		// TODO Auto-generated method stub
	
	}
	@Override
	public void keyReleased(KeyEvent e){
		// TODO Auto-generated method stub
		
	}

	

}
