package ce1002.a11.s102502556;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.nio.charset.Charset;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	JLabel word = new JLabel("key:"); 
	JTextField tf = new JTextField(); //輸入鍵值
	JButton send = new JButton("Decrypt"); //送出按鈕
	JLabel result = new JLabel("Result?"); //顯示結果
	MyFrame() {
		setTitle("A11-102502556"); //標題
		setSize(280, 150); //大小
		setLocationRelativeTo(null); //位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //關閉程序
		setLayout(null); //排版
		word.setSize(40, 30);
		word.setLocation(10, 10);
		tf.setSize(100, 30);
		tf.setLocation(40, 10);;
		send.setSize(100, 30);
		send.setLocation(150, 10);
		send.addActionListener(new Listener()); //接上ActionListener
		result.setSize(240, 30);
		result.setLocation(10, 60);
		result.setHorizontalAlignment(JLabel.CENTER); //文字置中
		add(word);
		add(tf);
		add(send);
		add(result);
		setVisible(true);
	}
	class Listener implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			result.setText(Decrypt(tf.getText())); //改變顯示結果
		}
	}
	String Decrypt ( String clue ) {		
		try {
			if ( clue.isEmpty() ) //檢查鍵值是否為空
			{
				JOptionPane.showMessageDialog(null, "Can’t be empty!!", "A11", JOptionPane.ERROR_MESSAGE);
				return "Result?";
			}
			int key = Integer.valueOf(clue); //轉成int型別方便計算
			if ( key < 1 || key > 9 ) {
				JOptionPane.showMessageDialog(null, "Out Of Range!", "A11", JOptionPane.ERROR_MESSAGE);
				return "Result?";
			}
			DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));
			int num = input.available(); //儲存需讀入的數量
			byte[] word = new byte[num]; //儲存已計算完成後的結果
			for ( int i = 0 ; i < num ; i++ )
			{
				word[i] = input.readByte(); //一個一個字寫入word陣列中
				word[i] -= key; //減掉鍵值
			}
			input.close();
			return new String(word, Charset.defaultCharset()); //回傳結果
		}
		catch ( Exception e ) {
			e.printStackTrace();
			return "Result?";
		}	
	}
}
