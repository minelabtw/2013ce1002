package ce1002.a11.s102502053;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class MyFrame extends JFrame implements ActionListener {
	
	MyPanel mypanel = new MyPanel();
	
	MyFrame()
	{
		
		getContentPane().setLayout(null); 
		setSize(320, 120);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		setTitle("A11-102502053");
		mypanel.decrypt.addActionListener(this);
		add(mypanel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if(mypanel.key.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "Can't be empty!!");
			
		}else if(Integer.parseInt(mypanel.key.getText()) < 1 || Integer.parseInt(mypanel.key.getText()) > 9)
		{
			JOptionPane.showMessageDialog(null, "Out Of Range!");
		}else
		{
		
			File file = new File("A11.txt");
			FileInputStream input = null;
			
			try {
			        
			        input = new FileInputStream("A11.txt");

				} catch (FileNotFoundException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();

				}
			
			int arraylength = (int) file.length();
			char[] contentArray = new char[arraylength];
			
			String key = mypanel.key.getText();
			int keyvalue = Integer.parseInt(key);
			int value;
			int temp = 0;
			String content;
			int inputdata = Integer.parseInt(input.toString());
			
	        
	         try {
	        	 while((value = input.read()) != -1)
	 			{
	 				value -= inputdata;
	 				contentArray[temp] = (char) value;
	 				temp++;
	 			}

			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	         String finalstring = null;
		     for(int a = 0; a < contentArray.length; a++)
		     {
		        	finalstring += keyvalue;
		     }
		     mypanel.label2.setText(finalstring);

		}
				
		
	}

}
