package ce1002.a11.s102502053;

import javax.swing.*;

import java.util.Scanner;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class MyPanel extends JPanel implements ActionListener{
	
	public JButton decrypt = new JButton("Decrypt");
	public JTextField key = new JTextField(30);
	public JLabel label1 = new JLabel();
	public JLabel label2 = new JLabel();
	
	MyPanel()
	{
		setLayout(null);
		setBounds(0, 0, 300, 120);
		decrypt.setBounds(200, 2, 78, 30);
		label1.setHorizontalAlignment(SwingConstants.RIGHT);
		label1.setBounds(2, 2, 40, 30);
		label1.setText("Key: ");
		key.setBounds(45, 2, 150, 30);
		label2.setBounds(2, 50, 300, 30);
		label2.setHorizontalAlignment(SwingConstants.CENTER);
		label2.setText("Result?");
		add(decrypt);
		add(label1);
		add(label2);
		add(key);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
