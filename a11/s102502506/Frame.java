package ce1002.a11.s102502506;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
public class Frame extends JFrame{
	  private JButton JBD = new JButton("Decrypt");
	  private JTextField JTK = new JTextField(10);
	  private JLabel JL = new JLabel("Result?");
	  
	  Frame()
	  {
	    JPanel P = new JPanel();
	    P.add(new JLabel("Key:"));
	    P.add(this.JTK);
	    P.add(this.JBD);
	    
	    JPanel p = new JPanel();
	    p.add(this.JL);
	    add(P, "North");  //BorderLayout
	    add(p, "Center");
	    
	    this.JBD.addActionListener(new BLR(this));  //new一個ActionListener給JBD
	  }
	  
	  private class BLR
	    implements ActionListener
	  {
	    JFrame frame;
	    
	    BLR(JFrame f)
	    {
	      this.frame = f;
	    }
	    
	    public void actionPerformed(ActionEvent E)
	    {
	      if (Frame.this.JTK.getText().isEmpty())
	      {
	        EmptyMessage();  //用這個function跳出Empty訊息Can't be empty!
	      }
	      else if ((Integer.valueOf(Frame.this.JTK.getText()).intValue() < 1) || (Integer.valueOf(Frame.this.JTK.getText()).intValue() > 9))
	      {
	        OutOfRangeMessage();  //用這個function判斷書入數值是否OutOfRange若是輸出Out Of Range!
	      }
	      else
	      {
	        String fileName = "A11.txt";
	        try
	        {
	          DataInputStream input = new DataInputStream(new FileInputStream(fileName));
	          
	          String str = "";
	          while (input.available() > 0)
	          {
	            byte SA = input.readByte();
	            int key = Integer.valueOf(Frame.this.JTK.getText()).intValue();
	            

	            SA = (byte)(SA - key);
	            
	            str = str + (char)SA;
	          }
	          Frame.this.JL.setText(str);
	          input.close();
	        }
	        catch (FileNotFoundException e)
	        {
	          e.printStackTrace();
	        }
	        catch (NumberFormatException e)
	        {
	          e.printStackTrace();
	        }
	        catch (IOException e)
	        {
	          e.printStackTrace();
	        }
	      }
	    }
	    
	    private void EmptyMessage()
	    {
	      JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
	    }
	    
	    private void OutOfRangeMessage()
	    {
	      JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
	    }
	  }
}
