package ce1002.a11.s102502514;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel implements ActionListener {
	JLabel key = new JLabel("Key:");
	JButton jbt = new JButton("Decrypt");
	JTextField jtf = new JTextField();
	JLabel OutputDecrypt = new JLabel("Result?");
	JLabel jlb = new JLabel();
	Scanner input;

	MyPanel() {
		setLayout(null);
		setBounds(0, 0, 265, 110);

		key.setBounds(5, 15, 30, 12);

		jtf.setBounds(35, 10, 120, 25);
		jtf.setText(null);

		jbt.setBounds(160, 5, 85, 30);

		add(key);
		add(jtf);
		add(jbt);

		OutputDecrypt.setBounds(0, 50, 245, 12);
		OutputDecrypt.setHorizontalAlignment(JTextField.CENTER);
		add(OutputDecrypt);

		jbt.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (jtf.getText().isEmpty()) {
			JOptionPane.showMessageDialog(jlb, "Can't be empty!", "訊息",
					JOptionPane.INFORMATION_MESSAGE);
		}
		int intValue = Integer.valueOf(jtf.getText()); /* 將jtf的內容轉為整數型別 */
		if (intValue > 9 || intValue < 1) {
			JOptionPane.showMessageDialog(jlb, "Out Of Range!", "訊息",
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			try {

				/* 開始輸入檔案 */
				java.io.File file = new java.io.File("A11.txt");
				input = new Scanner(file);
				/* 結束輸入檔案 */

			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
			String EncryptString = input.next(); /* 存放輸入檔案的內容 */
			char[] chars = EncryptString.toCharArray(); /* 將字串轉成字元陣列 */

			/* 解密開始 */
			for (int i = 0; i < chars.length; i++) {
				chars[i] = (char) ((int) chars[i] - intValue);
			}
			/* 解密結束 */

			String DecryptString = new String(chars); /* 將解密字元陣列轉換為字串 */
			OutputDecrypt.setText(DecryptString); /* 將字串add到label上 */

			input.close();
		}
	}

}
