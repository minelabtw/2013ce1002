package ce1002.a11.s102502514;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyFrame() {
		super("A11-s102502514");

		MyPanel panel = new MyPanel();

		setLayout(null);
		setSize(265, 110);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		add(panel);

		setVisible(true);

	}

}
