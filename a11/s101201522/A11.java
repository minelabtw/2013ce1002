package ce1002.a11.s101201522;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import javax.swing.*;
import java.io.IOException;

public class A11 extends JFrame {
	private JLabel keyLabel = new JLabel("Key:");
	private JLabel resultLabel = new JLabel("Result?");
	private JButton decrypt = new JButton("Decrypt");
	private JTextField ketTest = new JTextField(10);
	private JPanel panel = new JPanel();
	private int key = 0;
	
	public A11 (String title){//initial
		/*set frame*/
		setTitle(title);
		setLayout(new GridLayout(2,1));
		
		/*add component*/
		add(panel);
		panel.add(keyLabel);
		panel.add(ketTest);
		panel.add(decrypt);
		decrypt.addActionListener(new ActionListener () {//motion of decrypt

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String result = "";
				
				if(ketTest.getText().length() == 0){//check empty input
					JOptionPane.showMessageDialog(null, "Can't be empty!");
				}
				else if(Integer.parseInt(ketTest.getText()) < 1  || Integer.parseInt(ketTest.getText()) > 9){//check range
					JOptionPane.showMessageDialog(null, "Out Of Range!");
				}
				else{//decrypt
					key = Integer.parseInt(ketTest.getText());
					try {	
						FileInputStream input = new FileInputStream("A11.txt");
						while(input.available()!=0){
							result = result + (char)(input.read() - key);
						}
						input.close();
						resultLabel.setText(result);
					} 
					catch (IOException e) {
						e.printStackTrace();
					}	
				}
				
			}
			
		});
		add(resultLabel);
		resultLabel.setHorizontalAlignment(JLabel.CENTER);
		pack();
		
	}
	
	public static void main(String[] args) throws IOException {
		A11 frame = new A11("A11-101201522");
		
		/*set frame*/
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
}