package ce1002.a11.s102502559;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame {

	JPanel panel;
	JTextField textField;
	JButton decoder;
	JSplitPane splitPane;
	JLabel label;
	String message;

	public MyFrame() {
		setTitle("A11-102502559");// set frame's title
		setSize(300, 100);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panel = new JPanel();
		panel.setLayout(new BorderLayout(3, 3));
		panel.setBorder(new EmptyBorder(3, 3, 3, 3));

		setContentPane(panel);

		textField = new JTextField();
		textField.setColumns(16);

		decoder = new JButton("decode");
		decoder.addActionListener(new DecorderButtonActionListener());

		splitPane = new JSplitPane();
		panel.add(splitPane, "North");
		splitPane.setRightComponent(decoder);
		splitPane.setLeftComponent(textField);

		label = new JLabel();
		LineBorder lineBorder = new LineBorder(Color.black, 1);
		label.setBorder(lineBorder);
		label.setSize(290, 70);
		panel.add(label, "Center");

	}

	class DecorderButtonActionListener implements ActionListener {

		int mode; // to store the number which user inputed
		String check; // to get the number which user inputed in String for checking 

		@Override
		public void actionPerformed(ActionEvent arg0) {

			check = textField.getText();// to get the number which user inputed in String for checking 
			if(check.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"Can’t be empty!!","A11",JOptionPane.ERROR_MESSAGE); // show message dialog
			}
			
			String temp = textField.getText();
			mode = Integer.valueOf(temp); // get the key
			try {
				if(mode < 1 || mode > 9)
				{
					JOptionPane.showMessageDialog(null,"Out of range !","A11",JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					Scanner input = new Scanner(new File("A11.txt"));
					String string = input.nextLine(); // read the file's content into variable

					char[] unit = string.toCharArray();

					for (int i = 0; i < unit.length; i++) 
					{
						unit[i] = (char) (unit[i] - mode); // decode
					}
					message = String.valueOf(unit);
					label.setText("" + message);
				}// for else
			} // for try
			catch (Exception e) 
			{
				e.printStackTrace();
			}

		}// for actionPerformed
	}//for DecorderButtonActionListener
}
