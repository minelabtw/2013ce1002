package ce1002.a11.s102502560;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	
	JLabel keyLabel=new JLabel("Key:");
	JLabel resultLabel=new JLabel("Result?");
	JButton jbt=new JButton("Decrypt");
	JTextField jtf=new JTextField(10);
	File file=new File("A11.txt");
	
	public MyFrame() {
		setTitle("A11-102502560");
		setLayout(new FlowLayout());
		setSize(250,100);
		
		jbt.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(jtf.getText().isEmpty()){
					JOptionPane.showMessageDialog(MyFrame.this, "Can't be empty!!");
					return;
				}
				
				int key;
				try {
					key=Integer.parseInt(jtf.getText());
				} catch (NumberFormatException e2) {
					JOptionPane.showMessageDialog(MyFrame.this, "Out Of Range!");
					return;
				}

				if(key<1||key>9){
					JOptionPane.showMessageDialog(MyFrame.this, "Out Of Range!");
					return;
				}
				
				resultLabel.setText(decryptWithKey(key));
			}
		});
		
		add(keyLabel);
		add(jtf);
		add(jbt);
		add(resultLabel);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	protected String decryptWithKey(int key) {
		
		Scanner IN=null;
		try {
			IN = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String decodedstString="";
		
		while(IN.hasNextLine()) {
			String line=IN.nextLine();
			for(char c: line.toCharArray()){
				c-=key;
				decodedstString+=c;
			}
		}		
		
		IN.close();
		
		return decodedstString;
	}
	
}
