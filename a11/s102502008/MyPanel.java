package ce1002.a11.s102502008;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;

public class MyPanel extends JPanel {
	JLabel text;
	JTextField textField;
	JButton button;
	JPanel panel;
	int key;
	String temp;

	public MyPanel() {
		panel = new JPanel();
		JLabel key = new JLabel("Key");
		textField = new JTextField(10);
		button = new JButton("Decrypt");
		button.addActionListener(new ButtonListener());
		panel.add(key);
		panel.add(textField);
		panel.add(button);
		text = new JLabel();
		setLayout(new BorderLayout());
		add(panel, BorderLayout.NORTH);
		add(text, BorderLayout.CENTER);
	}

	class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			if (textField.getText().equals("")) {
				JPanel panel = new JPanel();
				panel.add(new JLabel("Can't be empty!!"));
				// display the jpanel in a joptionpane dialog, using
				// showMessageDialog
				JFrame frame = new JFrame(
						"JOptionPane showMessageDialog component example");
				JOptionPane.showMessageDialog(frame, panel);

			} else {

				key = Integer.parseInt(textField.getText());
				if (key >= 1 && key <= 9) {
					try {
						FileInputStream input = new FileInputStream("A11.txt");
						byte data[] = new byte[input.available()];
						input.read(data);
						char answer[] = new char[data.length];
						for (int i = 0; i < data.length; i++) {
							if (data[i] == ' ')
								continue;
							answer[i] = (char) ((char) data[i] - key);

						}
						text.setText(new String(answer));
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} else {
					JPanel panel = new JPanel();
					panel.add(new JLabel("Out Of Range!"));
					// display the jpanel in a joptionpane dialog, using
					// showMessageDialog
					JFrame frame = new JFrame(
							"JOptionPane showMessageDialog component example");
					JOptionPane.showMessageDialog(frame, panel);
				}
			}
		}
	}
}
