package ce1002.a11.s102502552;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class A11 {

	private JFrame frame;
	private JLabel Key;
	private JLabel decoder;
	private JTextField key;
	private String decode = "Result?";
	private int keycode;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A11 window = new A11();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}//主程式執行

	public A11() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 287, 155);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);//方框參數
		
		key = new JTextField();
		key.setBounds(34, 0, 120, 21);
		frame.getContentPane().add(key);
		key.setColumns(10);//輸入金鑰的textfield參數
		
		Key = new JLabel("Key:");
		Key.setBounds(10, 3, 46, 15);
		frame.getContentPane().add(Key);//提示語
		
		JButton Decrypt = new JButton("Decrypt");// 按鍵
		Decrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent dec) {//動作
			try{
				empty();//檢查是否為空
				keycode = Integer.valueOf(key.getText());//把金鑰轉為整數
				
				if(keycode < 1 || keycode > 9)//檢查範圍
				{
					SmallFrame smallframe = new SmallFrame("Out of Range!");
					smallframe.setVisible(true);//超出範圍則跳出小方框
				}else{
					decodefile();//範圍內則進行解碼
					decoder.setText(decode);//解碼內容放入顯示結果的標籤
				}
			}catch(Exception e){
				SmallFrame smallframe = new SmallFrame(e.getMessage());
				smallframe.setVisible(true);//內容為空則跳出小方框
			}
		}
		});
		Decrypt.setBounds(155, 0, 111, 21);
		frame.getContentPane().add(Decrypt);//按鍵參數
		
		decoder = new JLabel(decode);
		decoder.setBounds(0, 23, 271, 93);
		frame.getContentPane().add(decoder);//顯示結果的標籤
	}
	
	public void empty() throws Exception
	{	
		if(key.getText().isEmpty() == true)
		{
			throw new Exception("Can’t be empty!!");//金鑰為空則丟出例外
		}
	}//檢查是否為空
	
	public void decodefile() throws IOException//解碼函式
	{
		
		try {
			FileReader file = new FileReader("src/ce1002/a11/s102502552/A11.txt");
			BufferedReader br = new BufferedReader(file);//讀檔
	
			do{
				if(br.equals(null))
				{
					break;//讀到結束跳出
				}else{
					String cutter = br.readLine();//一次讀一行
					
					char[] bedecoded = cutter.toCharArray();//讀到的字串存為字元陣列
					
					for(int counter = 0;counter < bedecoded.length;counter++)
					{
						bedecoded[counter] -= keycode;
						decode = String.valueOf(bedecoded);
					}//字元逐個解碼，存入新的字串
					break;
				}
			}while(true);
			
			br.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
