package ce1002.a11.s102502552;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class SmallFrame extends JFrame{
	
	public SmallFrame(String message)
	{
		setBounds(150,150,100,50);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//小方框參數
		
		JLabel wrong = new JLabel(message);
		wrong.setBounds(0,0,80,40);
		add(wrong);//顯示的內容置於標籤
	}

}
