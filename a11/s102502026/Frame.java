package ce1002.a11.s102502026;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame implements ActionListener {
	JTextField txt = new JTextField(13);	//set textfield and its large for frame
	JButton button = new JButton();			//set button	for frame
	JPanel panel = new JPanel();		//set panel	for frame
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	

	public Frame() {
		Container c = this.getContentPane();	//for container
		button.setText("Decrypt");					//type save on button
		label1.setText("Key:");
		label2.setText("Result?");
		// add to panel
		panel.add(label1);
		panel.add(txt);
		panel.add(button);	
		panel.add(label2);
		// add panel in container
		c.add(panel);
		button.addActionListener(this);	//do actionperformed when press button
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ("".equals(txt.getText())) {	//when textfield is null
			final JFrame fr = new JFrame("Message");		//open another frame
			fr.setLayout(new BorderLayout());	
			ImageIcon icon= new ImageIcon("src/ce1002/a11/s102502026/a.png");	//add picture
			JButton bt = new JButton("Accept");	  //add button
			JPanel pn = new JPanel();			//add panel
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();			//add panel2
			pn1.setSize(100, 50);
			JLabel label = new JLabel();		//add label
			fr.setSize(200, 100);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("Can't be empty!");		//label output
			//add to panel
			pn.add(new JLabel(icon));		
			pn.add(label);
			pn1.add(bt);
			fr.add(pn,BorderLayout.CENTER);
			fr.add(pn1,BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();		//close frame when press button
				}
			});
		} else if(Integer.parseInt(txt.getText())<1 || Integer.parseInt(txt.getText())>9) {	//when textfield is null
			final JFrame fr = new JFrame("Message");		//open another frame
			fr.setLayout(new BorderLayout());	
			ImageIcon icon= new ImageIcon("src/ce1002/a11/s102502026/a.png");	//add picture
			JButton bt = new JButton("Accept");	  //add button
			JPanel pn = new JPanel();			//add panel
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();			//add panel2
			pn1.setSize(100, 50);
			JLabel label = new JLabel();		//add label
			fr.setSize(200, 100);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("Out Of Range!");		//label output
			//add to panel
			pn.add(new JLabel(icon));		
			pn.add(label);
			pn1.add(bt);
			fr.add(pn,BorderLayout.CENTER);
			fr.add(pn1,BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();		//close frame when press button
				}
			});
	}else {
		    File file = new File("A11.txt");	//open file
	        StringBuilder contents = new StringBuilder();	///stringbuild
	        BufferedReader reader = null;
	      //save content
	        try {
	            reader = new BufferedReader(new FileReader(file));
	            String text = null;
	 
	            // repeat until all lines is read
	            while ((text = reader.readLine()) != null) {
	                contents.append(text).append(System.getProperty("line.separator"));	
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                if (reader != null) {
	                    reader.close();
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	            //process for decrypt
	            String str = contents.toString();	//get content of the file
	            char[] charArray = str.toCharArray();	//change to char
	            int n = Integer.parseInt(txt.getText());	//get number of textfield
	          String a= "";
	            for(char c:charArray){
	            	int s= (int) c;		//save char int to s
	            	s= s-n;				//to decrypt file char minus the number for decrypt
	            	if(s<32){
	            		System.out.print("");	//dont output thing
	            	}else{
	            	char z= (char) s;			// save the result of int to char 
	            	a= a+ String.valueOf(z);	//save char to string
	            }
	            	label2.setText(a);		//output into label2 the a(result of decrypt in string)
	            }
	        }
	         
	    }
	}

}
