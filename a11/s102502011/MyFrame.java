package ce1002.a11.s102502011;

import javax.swing.JFrame;

import java.io.File;
import java.io.FileNotFoundException; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;

public class MyFrame extends JFrame {
	
	MyFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new Decode() ) ;
	}
	
	class Decode extends JPanel implements ActionListener {
		
		private JLabel key = new JLabel("Key:") ;
		private JLabel result = new JLabel("Result?") ;
		private JButton button = new JButton("Decrypt") ;
		private JTextField textfield = new JTextField(15);

		
		Decode() {
			
			setLayout(null) ;
			setSize(250,120) ;
			
			textfield.setBounds(40,10,100,20); //set key
			add(textfield) ;
			
			key.setBounds(5,5,30,30); 
			add(key) ;
			
			result.setBounds(80,50,50,30);
			add(result) ;
			
			button.setBounds(150,5,80,30) ;
			button.addActionListener((ActionListener)this) ;
			add(button) ;
			
		}

		@Override
		public void actionPerformed(ActionEvent a) {
			// TODO Auto-generated method stub
			if(textfield.getText().isEmpty() )   //can��t be empty!
				JOptionPane.showMessageDialog(null, "File name can��t be empty!", "�T��", JOptionPane.INFORMATION_MESSAGE ) ;
			else if (textfield.getText().length() != 1)  // Out Of Range!
				JOptionPane.showMessageDialog(null, "Out of range!", "�T��", JOptionPane.INFORMATION_MESSAGE );
			else {
				try {
					   File file = new File("A11.txt");  
					   Scanner input = new Scanner(file);  //read file
					   String ans="";
					   while(input.hasNext())
					   {
						   String s = input.next();		   
						   int i;
							for(i=1; i<10; i++)
							{
								if(i == new Integer(textfield.getText()).intValue())
									break;
							}
							for ( int j = 0; j < s.length(); j++ ) 
							{
								if(s.charAt( j )!=' ')
								{
									char c = s.charAt( j ); 
									ans += (char)(c-i); 
								}
							} 
					   }
					   result.setText(ans);
					   input.close();
				    } catch (FileNotFoundException e) {
					   e.printStackTrace();
				    }
				
			}
			
		}

	}

}
