package ce1002.a11.s101201005;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.net.URL;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener {
	private JLabel label1 = new JLabel("Key: ");
	private JLabel label2 = new JLabel("Result?");
	private JTextField text = new JTextField();
	private JButton button = new JButton("Decrypt");

	MyFrame() {
		setTitle("A11-101201005");
		setBounds(0, 0, 255, 100);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		label1.setBounds(5, 10, 50, 20);
		label2.setBounds(90, 40, 50, 20);
		text.setBounds(35, 10, 115, 25);
		button.setBounds(155, 7, 80, 30);
		text.setText(null);
		button.addActionListener(this);
		add(label1);
		add(label2);
		add(text);
		add(button);

	}

	public void actionPerformed(ActionEvent e) {
		try {
			if (text.getText().equals("")) //如果沒有輸入內容
			{
				JOptionPane.showMessageDialog(this, "Can't be empty!");
			} else {
				int Ciphertext = Integer.parseInt(text.getText());
				if (Ciphertext > 0 && Ciphertext < 10) {
					int key = Integer.parseInt(text.getText());
					FileReader file = new FileReader("A11.txt");
					Scanner input = new Scanner(file);
					label2.setText("");
					while (input.hasNext()) {
						String data = input.next();
						for (int i = 0; i < data.length(); i++) {
							label2.setText(label2.getText()
									+ Decrypt(data.charAt(i), key));
						}
					}
				} else //Ciphertext<=0 或 Ciphertext>=10
				{
					throw new Exception();
				}
			}
		} catch (Exception e2) {
			JOptionPane.showMessageDialog(this, "Out Of Range!");
		}
	}

	public char Decrypt(char a, int key) {
		int number = (int) a;
		number = number - key;
		return (char) number;
	}

	/*public void test(int Ciphertext) {
		try {
		} catch (Exception e2) {
		}
	}*/
}
