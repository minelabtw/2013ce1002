package ce1002.a11.s102502016;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

public class A11 extends JFrame implements ActionListener {
	JLabel key = new JLabel("key: ");
	JTextField input = new JTextField(15);
	JButton decrypt = new JButton("Decrypt");
	JLabel result = new JLabel("Result?");

	public A11() {//整個架構
		decrypt.addActionListener(this);
		JPanel panel1 = new JPanel(new GridLayout(1, 3));
		JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		panel1.add(key);
		panel1.add(input);
		panel1.add(decrypt);
		panel2.add(result);

		add(panel1);
		add(panel2);
	}

	public static void main(String[] args) {
		JFrame frame = new A11();
		frame.setTitle("A11-102502016");
		frame.setLayout(new GridLayout(2, 1));
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {//執行動作
		file();
	}

	public void file() {//動作內容

		if ("".equals(input.getText())) {//無輸入任何東西
			JFrame a = new JFrame();
			JLabel b = new JLabel("Can’t be empty!!");
			JOptionPane.showMessageDialog(a, b);
		} else if (Integer.parseInt(input.getText()) > 9
				|| Integer.parseInt(input.getText()) < 1) {
			JFrame a = new JFrame();
			JLabel b = new JLabel("Out Of Range!");
			JOptionPane.showMessageDialog(a, b);
		} else {
			String ans = "";
			int key = Integer.parseInt(input.getText());//字串轉數字
			try {
				int i = 1;
				File file = new File("A11.txt");//創個A11的file
				Scanner output = new Scanner(file);//讀取file
				char b;
				String a = output.nextLine();
				while (a.length() > i) {
					b = (char) (int) (a.charAt(i) - key);
					ans = ans + b;
					i = i + 2;
				}
				output.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			result.setText(ans);
		}
	}
}