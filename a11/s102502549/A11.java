package ce1002.a11.s102502549;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class A11 extends JFrame {

	private JPanel flowlayoutPanel = new JPanel();// flowlayout用的panel
	private JLabel keylLabel = new JLabel("key:");// 顯示"key:"的label
	private JTextField keyField = new JTextField(10);// 輸入key的textfield
	private JButton decryptButton = new JButton("Dycrypt");// 解碼按鈕
	private JLabel result = new JLabel("Result?", JLabel.CENTER);// 顯示結果的label

	public A11() {
		// 把按鈕加個監聽器
		decryptButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {

					// 沒輸入要跳出警告
					if (keyField.getText().length() == 0) {
						JOptionPane.showMessageDialog(null, "Can’t be empty!!");
					}
					// 數入超出範圍要警告
					else if (Integer.parseInt(keyField.getText()) <= 0
							|| Integer.parseInt(keyField.getText()) >= 10) {
						JOptionPane.showMessageDialog(null, "Out Of Range!");
					} else {

						File file = new File("A11.txt");
						int key = Integer.parseInt(keyField.getText());

						Scanner input = new Scanner(file);
						String s = new String();
						StringBuilder sb = new StringBuilder();// 這裡用了StringBuilder因為String不能修改

						while (input.hasNext()) {
							s = input.nextLine();//一次讀一行

							for (int i = 0; i < s.length(); i++) {
								sb.append((char) (s.charAt(i) - key));
							}
						}
						input.close();

						result.setText(sb.toString());
					}
				} catch (Exception e1) {
					//總共可能有二種例外
				}
			}
		});

		//把keylLabel,keyField,decryptButton加入flowlayoutPanel
		flowlayoutPanel.add(keylLabel);
		flowlayoutPanel.add(keyField);
		flowlayoutPanel.add(decryptButton);

		//把flowlayoutPanel,result加入frame
		add(flowlayoutPanel, BorderLayout.NORTH);
		add(result);

		//frame的一些屬性
		setTitle("A11-102502549");
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		A11 frame = new A11();
		frame.setVisible(true);
	}
}
