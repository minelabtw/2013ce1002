package ce1002.a11.s102502553;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import ce1002.a11.s102502553.MyFrame.SaveButtonActionListener;

public class MyFrame extends JFrame{
	
	private JTextField textField_1;
	private String code;
	int key;
	int counter;
	JLabel lblResult = new JLabel("Result?");
	boolean can = true;

	public MyFrame() {//設定frame裡的東西的基本值
		setTitle("A11-s102502553");
		setBounds(100, 100, 250, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Key:");
		lblNewLabel.setBounds(4, 9, 32, 15);
		getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Decrypt");
		btnNewButton.setBounds(120, 9, 81, 20);
		getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new SaveButtonActionListener(this));
		
		textField_1 = new JTextField();
		textField_1.setBounds(30, 9, 74, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		lblResult.setBounds(52, 37, 151, 30);
		getContentPane().add(lblResult);
	}	
		
	class SaveButtonActionListener implements ActionListener {
		JFrame frame;
 
		public SaveButtonActionListener(JFrame frame) {
			this.frame = frame;
		}
		
		public void actionPerformed(ActionEvent e) {
			if (textField_1.getText().isEmpty())//判斷是否為空
				showEmptyFileNameMsg();//秀訊息
			else
			{
				int afterConvert =  Integer.parseInt(textField_1.getText());//String讀檔換成Int
				if( afterConvert > 9 ||afterConvert < 1)//判斷是否超出範圍
				{
				shownumberMsg();
				can = false;
				}
			}
		
				key =  Integer.parseInt(textField_1.getText());
				
				try {
					BufferedReader in = new BufferedReader(new FileReader("A11.txt"));//讀檔案進入
					String s="",s2="";
					 while((s=in.readLine())!=null)//判斷是否換行
					 {
					 s2+=s+"\n";//把讀到的檔案給s2
					 }
					 in.close();//關檔
					 code = s2;//把檔案給code，外面函式才能用
				}
				catch (FileNotFoundException e1)
				{
				} 
				catch (IOException e1) 
				{
				}
				
				if(can == true)//解碼
				{
					 char[] bm;
					  bm = code.toCharArray();//把字串變字元
					 for(int i = 0;i < bm.length-1;i++)
					 {
					 bm[i] = (char) (bm[i] - key);
					 }
					 code = String.valueOf(bm);//把字元變字串
				lblResult.setText(String.valueOf(code));//換上新標籤
				}
		}
 
		// dialog
		private void showEmptyFileNameMsg() {
			JOptionPane.showMessageDialog(frame, "Can't be empty!");
	    }
		
		private void shownumberMsg() {
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
	    }
}
}
