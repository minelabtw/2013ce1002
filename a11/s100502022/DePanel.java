package ce1002.a11.s100502022;

import java.awt.FlowLayout;
import java.awt.Panel;

import javax.swing.JLabel;

public class DePanel extends Panel{
	/**
	 * result panel
	 */
	private static final long serialVersionUID = 1L;
	private JLabel Content = new JLabel("result?");
	public DePanel(int x,int y,int width,int height){
		setBounds(x,y,width,height);
		FlowLayout fl = new FlowLayout(FlowLayout.CENTER);
		setLayout(fl);
		this.add(Content);
	}
	public void setContent(String de){
		Content.setText(de);
	}
	

}
