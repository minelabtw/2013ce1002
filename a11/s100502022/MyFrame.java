package ce1002.a11.s100502022;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	/**
	 * make a decrypt calaulator GUI
	 */
	private KeyPanel kp = new KeyPanel(8,8,35,30);
	private DePanel dp = new DePanel(10,35,190,50);
	private JPanel panel;
	public MyFrame() {
		setTitle("A11-100502022");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(800, 400, 255, 110);
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(panel);
		panel.setLayout(null);
		//KeyInput design
		final JTextField KeyInput = new JTextField();
		KeyInput.setBounds(35, 8, 110, 25);
		panel.add(KeyInput);
		//Button design
		JButton button = new JButton("Decrypt");
		button.addActionListener(new ActionListener() {
			//action process
			public void actionPerformed(ActionEvent e) {
					FileReader fr = null;
					BufferedReader br =null;
					String res = "";
					try {
						fr = new FileReader("A11.txt");
						br = new BufferedReader(fr);
						String gets ="";
						while((gets=br.readLine())!=null){
							res+=gets;
						}
						br.close();
						fr.close();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//if key value is empty
					if(KeyInput.getText().isEmpty()){
						JOptionPane.showMessageDialog(null, "Can't be empty!!", "�T��", JOptionPane.PLAIN_MESSAGE);
					}
					else{
						//normally execute
						int intKey = Integer.parseInt(KeyInput.getText());
						String after = "";
						if(intKey>9||intKey<1){
							JOptionPane.showMessageDialog(null, "Out Of Range!", "�T��", JOptionPane.PLAIN_MESSAGE);
						}
						else{
							//process in decrypting
							int[] data = new int[res.length()];
							for (int i = 0; i <res.length(); i++) {
								data[i] = res.getBytes()[i];
								data[i] = data[i] - intKey;
								after = after + (char) data[i];
							}
							dp.setContent(after);
						}
					}
					
			}
		});
		button.setBounds(150, 5,80, 30);
		panel.add(button);
		add(kp);
		add(dp);
		
	}
}