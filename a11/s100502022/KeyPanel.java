package ce1002.a11.s100502022;

import java.awt.Graphics;

import javax.swing.JPanel;

public class KeyPanel extends JPanel{
	/**
	 * key: textpanel
	 */
	private static final long serialVersionUID = 1L;
	private int height;
	public KeyPanel(int x,int y,int width,int height){
		this.height=height;
		setBounds(x,y,width,height);
	}
	public void paintComponent(Graphics g){
		g.drawString("Key:", 0, height/2);
	}
}
