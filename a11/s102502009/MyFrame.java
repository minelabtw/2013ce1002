package ce1002.a11.s102502009;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JButton Decrypt = new JButton("Decrypt");
	private JTextField Key = new JTextField(10);
	private JLabel Label = new JLabel("Result?");

	MyFrame() {
		JPanel pane1 = new JPanel(); // add panel
		pane1.add(new JLabel("Key:"));
		pane1.add(this.Key);
		pane1.add(this.Decrypt);

		JPanel pane2 = new JPanel();
		pane2.add(this.Label);

		add(pane1, "North");
		add(pane2, "Center");

		this.Decrypt.addActionListener(new ButtonListener(this));
	}

	private class ButtonListener implements ActionListener {
		JFrame frame;

		ButtonListener(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent e) {
			if (MyFrame.this.Key.getText().isEmpty()) { // empty
				JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
			} else if ((Integer.valueOf(MyFrame.this.Key.getText()).intValue() < 1)
					|| (Integer.valueOf(MyFrame.this.Key.getText()).intValue() > 9)) {
				JOptionPane.showMessageDialog(this.frame, "Out Of Range!"); // key
																			// must
																			// be
																			// 1~9
			} else {
				String fileName = "A11.txt";
				try {
					DataInputStream input = new DataInputStream(
							new FileInputStream(fileName));

					String str = "";
					while (input.available() > 0) { // Decrypt
						byte c = input.readByte();
						int key = Integer.valueOf(MyFrame.this.Key.getText())
								.intValue();

						c = (byte) (c - key);

						str = str + (char) c;
					}
					MyFrame.this.Label.setText(str);
					input.close();
				} catch (FileNotFoundException e1) { // error not found file
					e1.printStackTrace();
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

	}
}