package ce1002.a11.s102502502;
		import java.awt.event.ActionEvent;
		import java.awt.event.ActionListener;
		import java.io.DataInputStream;
		import java.io.FileInputStream;
		import java.io.FileNotFoundException;
		import java.io.IOException;
		import javax.swing.JButton;
		import javax.swing.JFrame;
		import javax.swing.JLabel;
		import javax.swing.JOptionPane;
		import javax.swing.JPanel;
		import javax.swing.JTextField;

		public class DecryptFrame extends JFrame{
			private static final long serialVersionUID = 1L;
		private JButton jbtDecrypt = new JButton("Decrypt");       // create a button "Decrypt"
		  private JLabel jlbKey = new JLabel("Key:");          		 // create a label to place the key
		  private JTextField Keyarea = new JTextField(10);           // create a textField to input the key
		  private JLabel jlbResult = new JLabel("Result:");           // create a label to place the result
		  
		  public DecryptFrame(){
			  setTitle("A11-102502502");
			  setSize(300,300);
			  setDefaultCloseOperation(EXIT_ON_CLOSE);
			  setVisible(true);
		    JPanel p1 = new JPanel();
		    p1.add(this.jlbKey);
		    p1.add(this.Keyarea);
		    p1.add(this.jbtDecrypt);
		    
		    JPanel p2 = new JPanel();
		    p2.add(this.jlbResult);
		    
		    add(p1, "North");
		    add(p2, "Center");
		    this.jbtDecrypt.addActionListener(new ButtonListener(this));
		  }
		  
		  private class ButtonListener implements ActionListener{
		    JFrame frame;
		    ButtonListener(JFrame frame)
		    {
		      this.frame = frame;
		    }
		    
		    public void actionPerformed(ActionEvent e)
		    {
		      if (DecryptFrame.this.Keyarea.getText().isEmpty())     // if the key is null, show "The key can't be Empty!"
		      {
		        showEmptyMsg();
		      }
		      else if ((Integer.valueOf(DecryptFrame.this.Keyarea.getText()).intValue() < 1) || (Integer.valueOf(DecryptFrame.this.Keyarea.getText()).intValue() > 9))
		      {
		        showOutOfRangeMsg();                                 // if the key is <1 or >9, show "The key is Out Of Range!"
		      }
		      else
		      {
		        String fileName = "A11.txt";
		        try
		        {
		          DataInputStream input = new DataInputStream(new FileInputStream(fileName));
		          
		          String s = "";
		          while (input.available() > 0)
		          {
		            char c = input.readChar();
		            int integer = Integer.valueOf(c).intValue();
		            int key = Integer.valueOf(DecryptFrame.this.Keyarea.getText()).intValue();
		            
		            c = (char)(integer - key);
		            s = s + c;
		          }
		          DecryptFrame.this.jlbResult.setText(s);
		          input.close();
		        }
		        catch (FileNotFoundException e1){
		          e1.printStackTrace();
		        }
		        catch (NumberFormatException e1){
		          e1.printStackTrace();
		        }
		        catch (IOException e1){
		          e1.printStackTrace();
		        }
		      }
		    }
		    
		    private void showEmptyMsg(){
		      JOptionPane.showMessageDialog(this.frame, "The key can't be empty!");
		    }
		    
		    private void showOutOfRangeMsg(){
		      JOptionPane.showMessageDialog(this.frame, "The key  is Out Of Range!");
		    }
		  }
		}

