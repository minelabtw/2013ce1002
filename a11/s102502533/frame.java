package ce1002.a11.s102502533;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;
public class frame extends JFrame {
	JLabel label1 = new JLabel("Key:");  //元件
	JLabel label2 = new JLabel("Result?");
	JPanel panel = new JPanel();
	JButton button = new JButton("Decrypt");
	JTextField textfield = new JTextField();
	String strings ="";
	frame(){
		setLayout(null);
		setTitle("A11-102502533");
		setSize(400, 150);
		setLocationRelativeTo(null);// 讓視窗置中
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		panel.setBounds(0, 0, 100, 270); //位置大小
		button.setBounds(230, 20, 100, 30);
		textfield.setBounds(70, 20, 150, 30);
		label1.setBounds(30, 20, 70, 30);
		label2.setBounds(140, 60, 70, 30);
		
		add(textfield);
		add(label1);
		add(label2);
		add(panel);
		add(button);
		button.addActionListener(new ActionListener() { //listener event
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (textfield.getText().equals("")) { //check whether file is empty??
					JOptionPane.showMessageDialog(null, "Can’t be empty!!","訊息", JOptionPane.INFORMATION_MESSAGE);
				} else if (Integer.valueOf(textfield.getText()).intValue() > 9|| Integer.valueOf(textfield.getText()).intValue() < 0) {
					JOptionPane.showMessageDialog(null, "Out Of Range!", "訊息",JOptionPane.INFORMATION_MESSAGE);
				} else {
					try {//decode
						DataInputStream data = new DataInputStream(new FileInputStream("A11.txt"));
						while(data.available()>0){
						byte b =data.readByte(); 
						b=(byte)(b-Integer.valueOf(textfield.getText()).intValue());
						strings+=(char)b;
						}
						label2.setText(strings); //output text and close file
						data.close();
					} catch (FileNotFoundException e2) { 
						e2.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
	}
}
