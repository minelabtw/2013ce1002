package ce1002.a11.s100203020;

import javax.swing.*;

import java.io.*;
import java.awt.event.*;

public class MyFrame extends JFrame{
	
	private JButton jbDecode = new JButton("Decrypt");
	private JTextField jtKey = new JTextField(12	);
	private JLabel jlResult = new JLabel();
	private JLabel jlKey = new JLabel("key");
	
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	
	MyFrame(){
		
		//panel1 have labelKey, jtKey, jbDecode
		panel1.add(this.jlKey);
		panel1.add(this.jtKey);
		panel1.add(this.jbDecode);
		
		//panel2 have label
		this.jlResult.setText("Result?");
		panel2.add(this.jlResult);
		
		this.add(panel1, "North");//add panel1 
		this.add(panel2, "Center");//add panel2 under panel1	
		this.pack();
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("A11");

		this.jbDecode.addActionListener(new ButtonListener(this));
	}
	private class ButtonListener implements ActionListener{
		
		JFrame frame;
		String file = "A11.txt";
		ButtonListener(MyFrame frame)
	    {
	      this.frame = frame;
	    }
		@Override
		public void actionPerformed(ActionEvent e0) {
			String key = MyFrame.this.jtKey.getText();
			int ceiling =9;
			int floor = 1;
			//empty
			if(key.isEmpty()){
				this.empty();
			}	
			//out of range
			else if(Integer.valueOf(key)> ceiling ||Integer.valueOf(key) < floor){
				this.outOfRange();
			}
			
			else{
				try{
					DataInputStream input = new DataInputStream(new FileInputStream(this.file));
		        
		        		String decode = "";
		        		//decoding by shift key 1~9
		        		while(input.available() >0 ){
							byte code = input.readByte();
		        			code = (byte)(code - Integer.valueOf(key));
		        			decode = decode + (char)code; 
		        		}
		        		MyFrame.this.jlResult.setText(decode);
		        		input.close();
		        	}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				catch (NumberFormatException | IOException e) {
					e.printStackTrace();
				}
		        
				 
			}
		
		}
		
		
		//empty message
		private void empty(){
		      JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
		}
		//out of range message
		private void outOfRange(){
		      JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
		}
		
	}
	
}
