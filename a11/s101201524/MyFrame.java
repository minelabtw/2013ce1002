package ce1002.a11.s101201524;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel mypanel = new MyPanel();
	
	MyFrame(){
		//create a JFrame and set it's status
		super("E11-101201524");
		setLayout(null);
		setBounds(300, 150, 300, 130);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//add a panel
		add(mypanel);
	}
}
