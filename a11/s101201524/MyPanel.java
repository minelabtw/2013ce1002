package ce1002.a11.s101201524;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel{
	//create label, button, text field
	private JLabel keyLb = new JLabel("Key:");
	private JLabel resultLb = new JLabel("Result?");
	private JButton decrypt = new JButton("Decrypt");
	private JTextField keyTf = new JTextField(10);
	private int key = 0;
	
	MyPanel(){
		//set panel's status
		setLayout(null);
		setBounds(0, 0, 500, 500);
		keyLb.setBounds(0, 5, 50, 30);
		keyTf.setBounds(50, 5, 130, 30);
		decrypt.setBounds(200, 5, 80, 30);
		resultLb.setBounds(0, 40, 300, 40);
		//add the decrypt button and give it a action listener
		decrypt.addActionListener(new DecryptListener());
		//add them on panel
		add(keyLb);
		add(keyTf);;
		add(decrypt);
		add(resultLb);
	}
	//create a button listener
	class DecryptListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String result = "";
			if(keyTf.getText().length() == 0)
				JOptionPane.showMessageDialog(null, "Can't be empty!");
			else if(Integer.parseInt(keyTf.getText()) < 1  || Integer.parseInt(keyTf.getText()) > 9)
				JOptionPane.showMessageDialog(null, "Out Of Range!");
			else{
				key = Integer.parseInt(keyTf.getText());
				try {
					FileInputStream input = new FileInputStream("A11.txt");
					while(input.available()!=0){
						result += (char)(input.read() - key);
					}
					input.close();
					resultLb.setText(result);
				} catch (IOException e1) {
					e1.printStackTrace();
				}	
			}
			
		}
	}
}