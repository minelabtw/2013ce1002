package ce1002.a11.s102502040;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Decode extends JFrame{
	JButton save;
	JLabel key;
    JTextField txt_key;
    JTextArea result;
    
    Decode()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("A11-102502040");
        setSize(400, 250);
        buildUI();//使用者介面
	}
	
	private void buildUI()
	{
		
		key = new JLabel("KEY:");
		key.setBounds(100, 5, 350, 30);
		add(key);
		

		save = new JButton("Decrypt");//知道按下去會是什麼
		save.setBounds(5, 5, 90, 30);
		save.addActionListener(new DecodeFile());
		add(save);
		
		// key
		txt_key = new JTextField();//檔名
		txt_key.setBounds(135, 5, 250, 30);
		txt_key.setColumns(10);
		add(txt_key);
		
		// result
		result = new JTextArea();//加密
		result.setBounds(5, 40, 400, 215);
		result.setLineWrap(true);
		add(result);
	}
	
	class DecodeFile implements ActionListener
	{
		DecodeFile(){}
		@Override
		public void actionPerformed(ActionEvent event)
		{
			
			if (txt_key.getText().isEmpty())//要有內容
			{
				JOptionPane.showMessageDialog(null, "Can't be empty!!", "A11", JOptionPane.ERROR_MESSAGE);
				return;
			}
			int key = Integer.valueOf(txt_key.getText());
			
			if (!(1 <= key && key <= 9))//密碼限制範圍
			{
				JOptionPane.showMessageDialog(null, "Out Of Range!", "A11", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			try//解碼
			{
				byte[] encoded = Files.readAllBytes(Paths.get("A11.txt"));
				String str = new String(encoded, Charset.defaultCharset());
				String decrypted = "";	//讀檔
				for(char c : str.toCharArray())
					decrypted += (char)(c - key);
				result.setText(decrypted);	//解碼
			}
			catch (Exception e)
			{
				// error
				System.out.println("Error: " + e.getMessage());
			}
		}
	}
}
