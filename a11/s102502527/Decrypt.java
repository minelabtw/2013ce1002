package ce1002.a11.s102502527;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Decrypt extends JFrame
{
	JButton btn_save;
	JLabel label_key;
    JTextField txt_key;
    JTextArea txt_result;
    
    Decrypt()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//基本設定
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("A11-102502558");
        setSize(450, 300);
        
        buildUI();//使用者介面
	}
	
	private void buildUI()
	{
		label_key = new JLabel("KEY:");//開頭
		label_key.setBounds(100, 5, 355, 30);
		add(label_key);
		
		btn_save = new JButton("Decrypt");//按鈕
		btn_save.setBounds(5, 5, 90, 30);
		btn_save.addActionListener(new DecryptFile());
		add(btn_save);
		
		txt_key = new JTextField();
		txt_key.setBounds(135, 5, 305, 30);
		txt_key.setColumns(10);
		add(txt_key);
	
		txt_result = new JTextArea();//結果文字檔
		txt_result.setBounds(5, 40, 435, 215);
		txt_result.setLineWrap(true);
		add(txt_result);
	}
	
	class DecryptFile implements ActionListener
	{
		DecryptFile(){}
		
		@Override
		public void actionPerformed(ActionEvent event)
		{
			if (txt_key.getText().isEmpty())//檢查是否為空
			{
				JOptionPane.showMessageDialog(null, "Can’t be empty!!", "A11", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			int key = Integer.valueOf(txt_key.getText());//檢查範圍
			if (!(1 <= key && key <= 9))
			{
				JOptionPane.showMessageDialog(null, "Out Of Range!", "A11", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			try
			{
				byte[] encoded = Files.readAllBytes(Paths.get("A11.txt"));//讀取文字
				String str = new String(encoded, Charset.defaultCharset());
				String decrypted = "";
			
				for(char c : str.toCharArray())
					decrypted += (char)(c - key);
				txt_result.setText(decrypted);
			}
			catch (Exception e)
			{
				System.out.println("Error: " + e.getMessage());
			}
		}
	}
}
