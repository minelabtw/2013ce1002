package ce1002.a11.s102502015;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener{
	private JButton decrypt = new JButton("Decrypt");
	private JTextField key = new JTextField();
	private JLabel ans = new JLabel("Result?");
	private JLabel key1 = new JLabel("Key:");
	MyFrame() {
		this.setTitle("A11-s102502015");//版面
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(280, 120);
		this.setLayout(null);
		this.add(key1);
		this.add(key);
		this.add(decrypt);
		this.add(ans);
		key1.setLocation(5, 10);
		key1.setSize(30, 30);
		key.setSize(100, 30);
		key.setLocation(40, 10);
		decrypt.setSize(100, 35);
		decrypt.setLocation(150,10);
		decrypt.addActionListener(this);
		ans.setSize(100, 30);
		ans.setLocation(40, 50);
		this.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {
		JFrame a=new JFrame();
		String file = "A11.txt";
		String ans = "";
		if ("".equals(key.getText().trim())) {//空
			JOptionPane.showMessageDialog(a, "Can't be empty!");
		} else if (Integer.valueOf(key.getText())>9 || Integer.valueOf(key.getText())<1)//判斷
				 {
			JOptionPane.showMessageDialog(a, "Out Of Range!");
		} else {
				try {
					DataInputStream input;//讀檔案
					input = new DataInputStream(new FileInputStream(file));
					while (input.available() > 0) {//有東西
						char x = input.readChar();
						int integer = (int)x;
						int key2 = Integer.valueOf(key.getText()).intValue();
						x = (char) (integer - key2);//解答
						ans = ans + x;
					}
					this.ans.setText(ans);//設定答案
					input.close();
				} catch ( NumberFormatException | IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
		}
	}


}
