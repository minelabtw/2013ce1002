package ce1002.a11.s102502523;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.io.*;

public class Panel extends JPanel implements ActionListener{

	private JButton button = new JButton("Decrypt"); 
	private JTextField textfield = new JTextField();    //做出空格
	private JLabel label1 = new JLabel("Key :"); 
	private JLabel label2 = new JLabel("Result ?");
	private JLabel label3 = new JLabel();
	
	Panel(){
		setLayout(null);
		setBounds(0,0,350,170); 
		button.setBounds(200,20,110,20); 
		textfield.setBounds(50,10,130,25);      //空格大小
		
		Font font1 = new Font(Font.DIALOG, Font.BOLD, 15);      //選擇字形
		label1.setFont(font1);
		label1.setBounds(5,10,40,25);
		
		Font font2 = new Font(Font.DIALOG, Font.BOLD, 15);
		label2.setFont(font2);
		label2.setBounds(120,50,70,25);
		
		label3.setBounds(0,0,300,120); 
		
		add(button);     //加進panel中
		add(textfield);
		add(label1);
		add(label2);
		add(label3);
		
		
		button.addActionListener(this);     //點下按鈕時做動作
	}              	
	
	public void actionPerformed(ActionEvent e) { 
		try{
			check1(textfield.getText());	    //檢查輸入內容
			check2(textfield.getText());
			FileReader fr = new FileReader("A11.txt");         //讀取A11.txt
			BufferedReader br = new BufferedReader(fr); 
			char arrary [] = br.readLine().toCharArray(); 
			for(int i = 0 ; i < arrary.length ; i++){     //進行轉換
				arrary[i] = (char)((int)arrary[i] - Integer.parseInt(textfield.getText()));
			}
			String str = new String(arrary); 
			label2.setText(str); 
			
		}catch(FileNotFoundException ex){
			JOptionPane.showMessageDialog(label3, "Can't be empty!","訊息", 
					JOptionPane.INFORMATION_MESSAGE);			
		}catch(Exception ex1){
			JOptionPane.showMessageDialog(label3, "Out Of Range!","訊息",
					JOptionPane.INFORMATION_MESSAGE);	
		}
	}
	
	public boolean check1 (String file) throws FileNotFoundException{ 
		if(textfield.getText().isEmpty()){     //輸入內容是空的 , 輸出例外
			throw new FileNotFoundException();
		}else{
			return true;
		}
	}
	public boolean check2 (String file) throws Exception{
		if(Integer.parseInt(textfield.getText()) > 10 || Integer.parseInt(textfield.getText()) < 0){ 
			throw new Exception();
		}else{
			return true;
		}
	}
}