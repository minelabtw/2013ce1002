package ce1002.a11.s102502027;

import javax.swing.*;

import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class A11 extends JFrame { 
	JLabel l1 = new JLabel("Key:");  //元件
	JLabel l2 = new JLabel("Result?");
	JPanel p = new JPanel();
	JButton button = new JButton("Decrypt");
	JTextField field = new JTextField();

	A11() {
		p.setBounds(0, 0, 100, 270); //位置大小
		button.setBounds(310, 10, 100, 30);
		field.setBounds(100, 10, 200, 30);
		l1.setBounds(70, 10, 70, 30);
		l2.setBounds(180, 50, 70, 30);

		add(l1);
		add(field);
		add(button);
		add(l2);
		add(p);
		button.addActionListener(new ActionListener() { //點擊處發事件

			@Override
			public void actionPerformed(ActionEvent e) {
				String s ="";
				if (field.getText().isEmpty()) { //判斷是否符合條件
					JOptionPane.showMessageDialog(null, "Can’t be empty!!",
							"訊息", JOptionPane.INFORMATION_MESSAGE);
				} else if (Integer.valueOf(field.getText()).intValue() > 9
						|| Integer.valueOf(field.getText()).intValue() < 0) {
					JOptionPane.showMessageDialog(null, "Out Of Range!", "訊息",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					try {
						//解碼字元
						DataInputStream data = new DataInputStream(
								new FileInputStream("A11.txt"));//讀取檔案
						while(data.available()>0){
						byte b =data.readByte(); //以位元為單位
						b=(byte)(b-Integer.valueOf(field.getText()).intValue());
						s=s+(char)b;
						}
	
						l2.setText(s); //顯示字串
						data.close();
					} catch (FileNotFoundException e2) { //例外
						e2.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				}

			}
		});

	}

	public static void main(String[] args) { //視窗
		JFrame frame = new A11();
		frame.setLayout(null);
		frame.setTitle("A11-102502027");
		frame.setSize(430, 150);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
