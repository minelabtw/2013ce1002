package ce1002.a11.s102502012;

import javax.swing.JFrame;

public class A11 {

	public static void main(String[] args) {
		MyFrame window = new MyFrame();
		window.setTitle("A11-102502012");
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocationRelativeTo(null);
		window.setSize(300, 100);
		window.setVisible(true);
	}

}
