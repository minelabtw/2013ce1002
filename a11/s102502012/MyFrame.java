package ce1002.a11.s102502012;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JLabel keyText;
	private JLabel resultText;
	private JTextField key;
	private JButton btn; 
	private String path = "A11.txt";
	
	public MyFrame(){
		JPanel content = new JPanel();
		content.setLayout(null);
		
		resultText = new JLabel();
		keyText = new JLabel();
		key = new JTextField();
		btn = new JButton();
		
		keyText.setText("Key : ");
		keyText.setBounds(10, 10, 40, 20);
		
		key.setBounds(50, 10, 100, 20);
		
		btn.setText("Decrypt");
		btn.setBounds(160, 10, 100, 20);
		btn.addActionListener(new btnPressedEvent());
		
		resultText.setText("Press button to decrypt");
		resultText.setBounds(10, 20, 280, 50);
		
		content.add(keyText);
		content.add(btn);
		content.add(key);
		content.add(resultText);
		
		add(content);
	}
	
	
	public class btnPressedEvent implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if(key.getText().isEmpty()){
				JOptionPane.showMessageDialog(new JFrame(), "Can't be empty!!");
			}
			else{
				int keyValue = Integer.parseInt(key.getText());
				if(keyValue >= 1 && keyValue <= 9){
						try {
							FileReader fr = new FileReader(path);
							BufferedReader br = new BufferedReader(fr);
							
							String text = "";
							System.out.println("Read");
							while(br.ready()){
								String tmp1 = br.readLine();
								String tmp2 = "";
								System.out.println(tmp1);
								
								boolean isCharacter = false;
								for(int i = 0; i < tmp1.length(); i++){ // the characters are splitted by a space
									if(isCharacter)
										tmp2 += (char)((int)tmp1.charAt(i) - keyValue);
									isCharacter = !isCharacter;
								}
								text += tmp2;
							}
							resultText.setText(text);
							
							br.close();
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
				}
				else{
					JOptionPane.showMessageDialog(new JFrame(),  "Out Of Range!");	
				}
			}
		}
		
	}
}
