package ce1002.a11.s102502013;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;

public class Panel extends JPanel{
	private JLabel key = new JLabel("Key:");
	private JLabel result = new JLabel("Result?");
	private JTextField Key = new JTextField(8);
	protected JButton decode;
	String ans = "";
	Panel(){
		setLayout(new FlowLayout(FlowLayout.LEFT, 5 , 5));
		setBounds(5, 5, 225, 500);
		decode = new JButton("Decrypt");
		decode.setSize(25, 10);
		decode.setVisible(true);
		decode.addActionListener(new DECODEListenerClass());
		add(key);
		add(Key);
		add(decode);
		add(result);
	}
	class DECODEListenerClass implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			java.io.File file = new java.io.File("A11.txt");
			int i = 0;
			try {
				//讀檔案
				Scanner input = new Scanner(file);
				if(Key.getText().length() == 0 ){
					//訊息視窗
					JPanel panel = new JPanel();
					JLabel message = new JLabel("Can't be empty!");
					panel.add(message);
				    panel.setMinimumSize(new Dimension(200,200));
				    JFrame frame = new JFrame("訊息");
				    JOptionPane.showMessageDialog(frame, panel);
				}
				else{ 
					//將key從string轉為integer
					int key = new Integer(Key.getText()).intValue();
					if(key>0&&key<10){
						while(input.hasNext()){
					String test = input.next();
					ans = "";
					//解密
					for (i = 0; i < test.length(); i++ ){
						char c = test.charAt( i ); 
					    ans += (char)( c - key);
						} 
					}
						//輸出答案
					result.setText(ans);
					}
					else{
						//訊息視窗
						JPanel panel = new JPanel();
						JLabel message = new JLabel("Out Of Range!");
						panel.add(message);
					    panel.setMinimumSize(new Dimension(200,200));
					    JFrame frame = new JFrame("訊息");
					    JOptionPane.showMessageDialog(frame, panel);
					}
				}	
			}
			catch (FileNotFoundException e) {
				
				e.printStackTrace();
			}
			
		}
	}
}
