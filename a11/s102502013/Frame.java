package ce1002.a11.s102502013;
import javax.swing.JFrame;

public class Frame extends JFrame{
	Frame(){
		Panel panel = new Panel();
		setSize(230, 100);
		setTitle("A11-102502013");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
		add(panel);
	}
}
