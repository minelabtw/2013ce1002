package ce1002.a11.s101201519;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.*;

public class MyFrame extends JFrame{
	public JLabel key = new JLabel("Key:");
	public JLabel result = new JLabel("Result?");
	public JButton decrypt = new JButton("Decrypt");
	private JTextField text = new JTextField();
	
	MyFrame()
	{
		
		setTitle("A11-101201519");
		setSize(600,200);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		key.setBounds(0,20,100,50);
		result.setBounds(0,100,500,50);
		text.setBounds(100,20,200,50);
		decrypt.setBounds(300,20,200,50);
		text.setText(null);
		
		Save savelistener=new Save();
		decrypt.addActionListener(savelistener);
		//add the data into the Myframe
		add(key);
		add(result);
		add(text);
		add(decrypt);
	}
	public class Save implements ActionListener{
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				if (text.getText().equals(""))
				{//the exception if the text is empty then do 
					throw new NullPointerException();
				}
				else
				{
					//judge the number of the text
					int Ciphertext=Integer.parseInt(text.getText());
					if (Ciphertext>0 && Ciphertext<10)
					{
//						
						int key=Integer.parseInt(text.getText());
						//open the file 
						FileReader file =new FileReader("A11.txt");
						Scanner input =new Scanner(file);
						result.setText("");
						//reader the file and transform the data and output to the label2
						while (input.hasNext())
						{
							String data=input.next();
							for (int i=0;i<data.length();i++)
							{
								result.setText(result.getText()+Decrypt(data.charAt(i),key));
							}
						}
					}
					else
					{//the exception happen when the number out of range
						throw new Exception();
					}
				}
				
			}
			catch (NullPointerException e1)
			{//show the message
				JOptionPane.showMessageDialog(new JFrame("�T��"),"Can't be empty!!");
			}
			catch (Exception e2)
			{//show the message
				JOptionPane.showMessageDialog(new JFrame("�T��"), "Out Of Range!");
			}
			
		}
		public char Decrypt(char a, int key)
		{//decrpt
			int number=(int)a;
			number=number-key;
			return (char)number;
		}
	}
}
