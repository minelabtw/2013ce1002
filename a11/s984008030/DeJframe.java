package ce1002.a11.s984008030;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class DeJframe extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextArea textArea;

	/**
	 * Create the frame.
	 */
	public DeJframe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("A11-984008030");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		// First panel, put a text label, a text field , and a button in it
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Key: ");
		panel.add(lblNewLabel);
		
		this.textField = new JTextField();
		panel.add(this.textField);
		this.textField.setColumns(10);
		
		JButton btnE = new JButton("Encyrpt");
		// Define click action of btnE
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					int encyrptNumber = Integer.parseInt(textField.getText());
					if (encyrptNumber < 1 || encyrptNumber > 9) {
						// encyrptNumber is out of range
						JOptionPane.showMessageDialog(null,
														"Out Of Range!",
														"訊息",
														JOptionPane.PLAIN_MESSAGE );
					}
					else {
						try {
							FileReader fio = new FileReader("A11.txt");
							int readInput = 0;
							String result = "";
							while((readInput = fio.read()) != -1){
								result += (char)(readInput - encyrptNumber);
							}
							textArea.setText(result);
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null,
									"File A11.txt is not found!",
									"訊息",
									JOptionPane.PLAIN_MESSAGE );
						}
					}
				} catch (NumberFormatException e) {
					// encyrptNumber is null
					JOptionPane.showMessageDialog(null,
													"Can’t be empty!!",
													"訊息",
													JOptionPane.PLAIN_MESSAGE );
				}
			}
		});
		panel.add(btnE);
		// Second panel, put a textArea in it
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		this.textArea = new JTextArea();
		textArea.setLineWrap(true);
		this.textArea.setColumns(35);
		this.textArea.setRows(12);
		panel_1.add(this.textArea);
	}

}
