package ce1002.a11.s102502513;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import java.util.Scanner;
import java.util.ArrayList;

import java.io.FileReader;
import java.io.PrintWriter;


public class DecryptFrame extends JFrame {
	
	private JButton DecryptBtn = new JButton("Decrypt"); 
	public JTextField textfield = new JTextField();      
	private JLabel AnsLab = new JLabel("Result?");       
	private JLabel JumpLab = new JLabel();        
	
	DecryptFrame()
	{
		//設置視窗
		setLayout(null);
		setBounds(310,310,250,140);
		setTitle("A11-102502513");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//設置'DecryptButton'
		DecryptBtn.setBounds(25,25,85,35);
		DecryptBtn.addActionListener( new DecryptButtonListener());
		add(DecryptBtn);
		
		//設置'textfield'
		textfield.setBounds(120,25,100,35);
		textfield.setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		add(textfield);
		
		//設置'AnsLab'
		AnsLab.setBounds(60,60,110,50);
		AnsLab.setHorizontalAlignment(SwingConstants.CENTER);  
		add(AnsLab);

		setVisible(true);
	}
	
	class DecryptButtonListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)
		{
			try{
				Check(textfield.getText());
				
				//讀入檔案
				java.io.File file = new java.io.File("A11.txt");
				Scanner input = new Scanner(file);
				
				char password[] = input.nextLine().toCharArray();
				for (int i = 0; i < password.length ; i++)
				{
					password[i] = (char)((int)password[i] - Integer.parseInt(textfield.getText()));
				}
				String str = new String(password);
				AnsLab.setText(str);
				} 
			catch(Exception e1)
				{
					JumpLab.setText("" + e1);
				    JumpLab.setMinimumSize(new Dimension(200,200));
					JFrame conframe = new JFrame("");
				    JOptionPane.showMessageDialog(conframe, JumpLab);
				}
		}
		
		public boolean Check(String filetext) throws Exception  //Check 輸入值是否為空or超出範圍
		{
			if(filetext.isEmpty())
			{
				throw new Exception("Can't be empty!");
			}
			else if ( Integer.parseInt(filetext) > 10 ||  Integer.parseInt(filetext) < 0)
			{
				throw new Exception("Out of range!");
			}
			else
				return true;
		}
	}
}

