package ce1002.a11.s102502047;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

public class A11 extends JFrame {

	private JPanel contentPane;
	private JTextField t;
	private JLabel r = new JLabel("Result?");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A11 frame = new A11();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A11() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 272, 149);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel k = new JLabel("Key:");
		k.setBounds(10, 0, 31, 47);
		contentPane.add(k);
		
		t = new JTextField();
		t.setBounds(39, 13, 89, 21);
		contentPane.add(t);
		t.setColumns(10);
		
		JButton b = new JButton("Decrypt");
		b.setBounds(156, 12, 87, 23);
		contentPane.add(b);
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {	
				String a=t.getText();
				if(a.isEmpty())
				JOptionPane.showMessageDialog(null,"Can't be empty!!");
				else if(Integer.parseInt(a)<1||Integer.parseInt(a)>9)
					JOptionPane.showMessageDialog(null,"Out Of Range!");
				else
				{
					try{
					DataInputStream ds=new DataInputStream(new FileInputStream("A11.txt"));//讀入
					String s="";
					for(;ds.available()>0;)
					{
						s=s+(char)((int)ds.read()-Integer.parseInt(a));//解碼
					}
					ds.close();
					r.setText(s);//改變label內容
					}
					catch(Exception ex)
					{
						
					}
				}
				
			}
		});
		
		r.setHorizontalAlignment(SwingConstants.CENTER);
		r.setBounds(74, 79, 109, 21);
		contentPane.add(r);
		
		
	}
}
