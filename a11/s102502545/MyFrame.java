package ce1002.a11.s102502545;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class MyFrame extends JFrame {

	private JTextField Keyfield = new JTextField();
	private JLabel label = new JLabel("Result?");
	private JButton decrypt = new JButton("Decrypt");
	private JLabel Keylabel = new JLabel("Key:");

	public MyFrame() {

		Keylabel.setBounds(65, 5, 40, 30);
		add(Keylabel);

		Keyfield.setBounds(105, 5, 100, 30);
		add(Keyfield);

		decrypt.setBounds(205, 5, 100, 30);
		add(decrypt);
		decrypt.addActionListener(new Listener(this));

		add(label);

	}

	private class Listener implements ActionListener {
		JFrame frame = new JFrame();

		Listener(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent e) {
			
			
			if (MyFrame.this.Keyfield.getText().isEmpty()) {
				showMessage();
			} else if ((Integer.valueOf(MyFrame.this.Keyfield.getText())
					.intValue() < 1)
					|| (Integer.valueOf(MyFrame.this.Keyfield.getText())
							.intValue() > 9)) {
				showOutOfRange();
			} else {
				String name = "A11.txt";
				try {
					DataInputStream contain = new DataInputStream(
							new FileInputStream(name));

					String string = "";
					while (contain.available() > 0) {
						byte c = contain.readByte();
						int key = Integer.valueOf(
								MyFrame.this.Keyfield.getText()).intValue();

						c = (byte) (c - key);

						string = string + (char) c;
					}
					MyFrame.this.label.setText(string);
					contain.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}

		private void showMessage() {
			JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
		}

		private void showOutOfRange() {
			JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
		}
	}

	

}
