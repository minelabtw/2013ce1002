package ce1002.a11.s102502035;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	JLabel label1 = new JLabel("key:");
	JTextField field = new JTextField();
	JButton button = new JButton("Decrypt");
	buttonListenerClass listener1 = new buttonListenerClass();

	// JTextArea area = new JTextArea();
	JLabel label2 = new JLabel();

	public MyFrame() {
		// TODO Auto-generated constructor stub
		setTitle("A11-102502035");
		setLayout(null);
		setSize(320, 135);
		setDetail();
		String key = field.getText();
		add(label1);
		add(field);
		add(button);
		add(label2);
		System.out.print(key);
	}

	public void setDetail() {
		label1.setBounds(10, 0, 50, 30);
		label1.setVisible(true);
		field.setBounds(45, 0, 170, 30);
		field.setVisible(true);
		button.setBounds(220, 0, 80, 30);
		button.setVisible(true);
		button.addActionListener(listener1);
		label2.setBounds(50, 30, 170, 50);
		label2.setVisible(true);
	}

	class buttonListenerClass implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String keyWord = field.getText();
			if (keyWord.length() == 0) {
				JOptionPane
						.showMessageDialog(null, "File name can’t be empty!");
			} else {
				int keyCode = Integer.valueOf(keyWord);
				if (keyCode < 1 || keyCode > 9) {
					JOptionPane.showMessageDialog(null, "Out Of Range!");
				} else {
					System.out.println(keyCode);
					try {
						// read from disk
						FileInputStream fileInputStream = new FileInputStream(
								"A11.txt");
						// works data
						DataInputStream dataInputStream = new DataInputStream(
								fileInputStream);
						InputStreamReader inputStreamReader = new InputStreamReader(
								dataInputStream);
						BufferedReader bufferedReader = new BufferedReader(
								inputStreamReader);
						// save as string
						String strSecretLine = bufferedReader.readLine();

						char[] charArray = strSecretLine.toCharArray();
						char c;
						for (int i = 0; i < charArray.length; i++) {
							c = charArray[i];
							c -= keyCode;
							charArray[i] = c;
						}
						String strLine = String.valueOf(charArray);
						label2.setText(strLine);
						// System.out.println(strLine);
						// System.out.println(code);

						bufferedReader.close();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
	}
}