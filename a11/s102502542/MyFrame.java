package ce1002.a11.s102502542;

import java.awt.event.*;

import javax.swing.*;

import java.io.*;

public class MyFrame extends JFrame implements ActionListener
{
	MyFrame()
	{
		setTitle("A11-102502542");
		setSize(300,100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);//將frame視窗固定在center
		setLayout(null);//設定排版
		
		final JTextField textfield = new JTextField();
		textfield.setBounds(70,5,100,20);
		
		JLabel l = new JLabel("Key:");
		l.setBounds(35,5,60,20);
	    final JLabel l1 = new JLabel("Result?");
		l1.setBounds(120,35,60,20);
		
		JButton button = new JButton("Decrypt");
		button.setBounds(180,5,90,20);
		button.addActionListener( new ActionListener()//監聽使用者輸入事件
		{
			public void actionPerformed(ActionEvent e) 
			 {			 
				if(textfield.getText().isEmpty())//如果textfield內容為empty
					 JOptionPane.showMessageDialog(null,"Can’t be empty!!");
				else if (Integer.parseInt(textfield.getText())>9||Integer.parseInt(textfield.getText())<1)
					 JOptionPane.showMessageDialog(null,"Out Of Range!");
				else
				{
					String line;
					String target ="";
					try 
					{
						FileReader fr = new FileReader("A11.txt");//讀取檔名
						BufferedReader br = new BufferedReader(fr);//讀入檔案
						while((line=br.readLine())!=null)//當讀入檔案不為空時
						{
							char[] charArray = line.toCharArray();//陣列將讀入的字串拆成字元儲存
							for(int i=0;i<line.length();i++)//將字元傳入解密函式在回傳
							{
								target=target+decode(charArray[i],Integer.parseInt(textfield.getText()));
							}
							l1.setText(target);//將解密完字串覆蓋掉原本的jlabel
						}
					} 
					catch (IOException e1) 
					{
						e1.printStackTrace();
					} 
				}
			 }
		});
		
		add(button);
		add(textfield);
		add(l);
		add(l1);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e)
	{
		
	}
	public char decode(char a,int b) //解密函式
	{
		a= (char)((int)(a)-b);
		return a;
	}
}
		
	    