package ce1002.a11.s101201003;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import javax.swing.JOptionPane;

public class Decrypt implements ActionListener
{	
	private MyFrame myframe;
	
	public Decrypt(){
		// TODO Auto-generated constructor stub
		myframe=new MyFrame();
		myframe.setaction(this);
	}
	
	
	public void actionPerformed(ActionEvent e){		
		if(myframe.getkey().isEmpty())
			JOptionPane.showMessageDialog(null, "Can't be empty!!","Warning!!",JOptionPane.WARNING_MESSAGE);
		else{
			int key=myframe.getkey().charAt(0)-48;
			if(key<1||key>9||myframe.getkey().length()>1)
				JOptionPane.showMessageDialog(null,"Out Of Range!","Warning!!",JOptionPane.WARNING_MESSAGE);
			else{
				String code,decode="";
				try{ 					
					FileReader fin = new FileReader("A11.txt");
					BufferedReader br = new BufferedReader(fin);					
					while(br.ready()){
						code = br.readLine();
						for(int i=0;i<code.length();++i)
							decode+=(char)(code.charAt(i)-key);
					}
					myframe.setcode(decode);
				}
				catch (Exception e2){					
					JOptionPane.showMessageDialog(null, "Can not open Encrypt file - A11.txt","Warning!!",JOptionPane.WARNING_MESSAGE);
				}

			}
		}
		
	}
}
