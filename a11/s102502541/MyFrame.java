package ce1002.a11.s102502541;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class MyFrame extends JFrame{
	JButton Decrypt  = new JButton("Decrypt");
	JTextField Key = new JTextField();
	JLabel j1 = new JLabel("Key:");
	JLabel j2 = new JLabel("Result?");
	int key;
	public MyFrame()
	{
		setTitle("A11-102502541");
		setVisible(true);
		setSize(400,150);
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Decrypt.setBounds(250,20,100,50);
		DecryptListener d = new DecryptListener();
		Decrypt.addActionListener(d);
		add(Decrypt);
		
		Key.setBounds(80,20,150,50);
		add(Key);
		
		j1.setBounds(30,30,50,20);
		add(j1);
		j2.setBounds(150,80,50,20);
		add(j2);
	}
	class DecryptListener implements ActionListener
	{
		public void actionPerformed(ActionEvent a)
		{
			JLabel l = new JLabel();
			JFrame f = new JFrame();
			try{
				key = Integer.valueOf(Key.getText());//把key的值轉成int
			}
			catch(NumberFormatException e1)
			{
				
			}//catch key為""時
			    if(Key.getText().isEmpty())//判斷key是否為空
			    {
			    	l.setText("Can’t be empty!!"); 
			    	JOptionPane.showMessageDialog(f, l);
			    }
					
			    else if(key<1 || key>9)//判斷key的範圍
			    {
			    	l.setText("Out Of Range!");
			    	JOptionPane.showMessageDialog(f, l);
			    }
					
				else
				{
					try{
						DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));
						String s1 = "";
						while(input.available() > 0)
						{
							byte b = input.readByte();
							int temp = (int)b - Integer.valueOf(Key.getText());//將讀入的值移位
							char c= (char)temp;//將int 轉為 ascii code							
							String s = String.valueOf(c);//將char轉為 string
							s1 = s1+s;//將字串相連
							j2.setText(s1);
						}
						
					}
					catch(Exception e)
					{
						
					}
				}	
		}
	}
}
