package ce1002.a11.s100204006;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileReader;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MyFrame  extends JFrame 
{
	private JPanel contentPane;
	private JSplitPane splitPane;
	private JTextField fileNameArea;
	private JButton btnDecrypt;
	private JLabel label1;
	private JLabel label2;
		
	public MyFrame() 
	{
		setTitle("A11-100204006");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 250, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
 
		splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.SOUTH);
				 
		fileNameArea = new JTextField();
		splitPane.setLeftComponent(fileNameArea);
		fileNameArea.setColumns(10);
 
		btnDecrypt = new JButton("Decrypt");
		btnDecrypt.addActionListener(new SaveButtonActionListener(this));
		splitPane.setRightComponent(btnDecrypt);
				
		label1 = new JLabel("Key:");
		contentPane.add(label1, BorderLayout.NORTH);
		label2 = new JLabel("Result?");
		contentPane.add(label2, BorderLayout.CENTER);
		
		
	}

	
	    // handle save action
		class SaveButtonActionListener implements ActionListener 
		{
			JFrame frame;
	 
			public SaveButtonActionListener(JFrame frame) 
			{
				this.frame = frame;
			}
	 
			// invoke when button be clicked
			@Override
			public void actionPerformed(ActionEvent e) 	
			{
				String strkey=fileNameArea.getText();
				int numkey=Integer.parseInt(strkey);
				
				if (MyFrame.this.fileNameArea.getText().isEmpty())     // file name must be non-empty
					showEmptyFileNameMsg();
				
				else if(numkey<1 || numkey>9)
				{
					showFiledMsg();
				}
				
				else 
				{
					//String mainText = mainTextArea.getText();
					String fileName = "A11.txt";
					try 
					{
						//FileReader fr = new FileReader(fileName); 
						//BufferedReader br = new BufferedReader(fr); 
						//String text = br.readLine(); 
						//fr.close();
			
						//PrintWriter out = new PrintWriter(fileName);
						//out.close();
												
						/*for(int i=0; i<text.length();i++) 
						{ 
							char a = text.charAt(i); a-=numkey;
							System.out.print( a ); 
						}*/
						 DataInputStream input = new DataInputStream(new FileInputStream(fileName));

				          String str = "";

				          while (input.available() > 0) 
				          {
				            byte c = input.readByte();
				            int key = Integer.valueOf(MyFrame.this.fileNameArea.getText()).intValue();

				            c = (byte)(c - key);

				            str = str + (char)c;
				          }
				          MyFrame.this.label1.setText(str);
				          input.close();
						
					} 
					catch (FileNotFoundException e1)
			        {
			          e1.printStackTrace();
			        }
			        catch (NumberFormatException e1)
			        {
			          e1.printStackTrace();
			        }
					catch (IOException e1) 
					{
						e1.printStackTrace();
					}
				}
				
			}
	 
			// dialog
			private void showEmptyFileNameMsg() 
			{
				JOptionPane.showMessageDialog(frame, "Can��t be empty!!");
			}
	 
			private void showFiledMsg()
			{
				JOptionPane.showMessageDialog(frame, "Out Of Range!");
			}
		}
}
