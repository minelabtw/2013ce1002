package ce1002.a11.s102502529;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	JTextField text = new JTextField();
	JLabel textA = new JLabel("Result?");
	String title;
	String content;
	MyPanel(){
		
		setBorder(new LineBorder(Color.green,5));
		setLayout(null);
		setBounds(0, 0, 350, 400);
		
		
	}
	public void bt(){																//創造一個button
		JButton bt = new JButton("Decrypt");
		bt.setBounds(20, 20, 100, 30);
		bt.addActionListener(new ActionListener() {									//對button 做 actionlistener
			
			@Override
			public void actionPerformed(ActionEvent e) {							//執行事件
				// TODO Auto-generated method stub
				title=text.getText();
				if(text.getText().isEmpty())										//空的
				JOptionPane.showMessageDialog(null, "Can’t be empty!!");	//輸出錯誤訊息
				else if(Integer.valueOf(title).intValue()<1||Integer.valueOf(title).intValue()>9)
					JOptionPane.showMessageDialog(null, "Out Of Range!");
				else {																
				try {
					
					DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));
					String str = "";
					
			          while (input.available() !=0)								//讀取直到沒讀到
			          {
			        	byte c = input.readByte();								//java 編碼不同 雇用byte而不是char byte佔一個字節
			            int key = Integer.valueOf(title).intValue();			//將string的直轉為int
			            c = (byte)(c - key);
			            
			            str = str + (char)c;
			          }
			          textA.setText(str);
			          input.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				catch (IOException e1)											
		        {
		          e1.printStackTrace();
		        }
				}
			}
		});
		add(bt);
	}
	

	public void textF (){															//textField
		text.setBounds(150, 20, 150, 30);
		setVisible(true);
		add(text);
		
}
	public void textA(){															//textArea
		textA.setBounds(120, 70, 300, 300);
		add(textA);
	}
}

