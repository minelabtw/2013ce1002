package ce1002.a11.s102502504;

import java.awt.FlowLayout;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;


public class MyFrame extends JFrame
{
	private JTextField key;
	private JButton btn;
	private JLabel result;
	
	private int num;
	
	MyFrame()
	{
		setTitle("A11-102502504"); //frame的名稱
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 300, 280, 150); //frame的大小
		
		setLayout(new FlowLayout(FlowLayout.LEFT,10,20)); //使用的排版方式
		
		add(new JLabel("key:"));
		
		key = new JTextField(); //輸入key的欄位
		key.setColumns(10);
		add(key);
		
		btn = new JButton("Decrypt");
		btn.addActionListener(new DButtonActionListener(this));
		add(btn);
		
		result = new JLabel("                            Result?");
		add(result);
		
	}
	
	public class DButtonActionListener implements ActionListener //btn的actionlistener
	{
		JFrame frame; 
 
		public DButtonActionListener(JFrame frame) 
		{
			this.frame = frame;
		}
 
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			String content; //用來讀內容
			String key = MyFrame.this.key.getText(); //用來讀key欄位
			
			if (key.isEmpty()) //key欄位如果是空的
			{
				 show_error_message("Can't be empty!!");
			}
			else if(num<1 && num >9) //key欄位如果不再範圍內
			{
				show_error_message("Out Of Range!");
			}
			else //正常的情況下
			{
				content = readfile("A11.txt"); //讀取檔案
				String str = "";
				int key_value = Integer.parseInt(key);
				for(char c : content.toCharArray()) 
				{
					str += (char)(c - key_value);
				}
				result.setText(str);
			}
		}
		
		String readfile(String filename) 
		{
			String content = "";
			try {
				FileReader file = new FileReader("A11.txt"); //讀取檔案
				BufferedReader input = new BufferedReader(file); //Scanner(以空白來區隔每一個輸入字串)
				while (input.ready()) 
				{
					content += input.readLine() + "\n";
				}
			}
			catch (IOException event) 
			{
				event.printStackTrace();
			}
			return content;
		}
		
		void show_error_message(String massage) //用來顯示錯誤視窗
		{
			JOptionPane.showMessageDialog(null, massage, "A11", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
}
