package ce1002.a11.s102502508;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Decrypt extends JPanel implements ActionListener {

	JButton jbt = new JButton();// 新增一個按鍵
	JTextField jtf = new JTextField(10);// 新增一個文字框
	JLabel jbl = new JLabel("Key:");// 新增一個標籤

	public Decrypt() {

		jbt.setText("Decrypt");

		add(jbl);
		add(jtf);
		add(jbt);

		jbt.addActionListener(this);// 讓使用者能在按鍵後得到下一步的操作

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ("".equals(jtf.getText())) { // 建造一個視窗提醒使用者文字框內不能空白
			JLabel jbl= new JLabel();
			jbl.setText(" Can’t be empty!");
			JButton jbtok = new JButton("確定");
			JPanel jpl1 = new JPanel();
			JPanel jpl2 = new JPanel();
			jpl1.add(jbl);
			jpl2.add(jbtok);
			final JFrame jf = new JFrame("訊息");
			jf.setSize(210, 120);
			jf.setLocationRelativeTo(null);
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jf.setVisible(true);
			jf.add(jpl1, "North");
			jf.add(jpl2, "Center");

			jbtok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jf.dispose();// 使視窗能被關掉
				}
			});
		}

		else if (Integer.parseInt(jtf.getText()) < 1
				|| Integer.parseInt(jtf.getText()) > 9) {

			jbl.setText(" Out Of Range!");
			JButton jbtok = new JButton("確定");
			JPanel jpl1 = new JPanel();
			JPanel jpl2 = new JPanel();
			jpl1.add(jbl);
			jpl2.add(jbtok);
			final JFrame jf = new JFrame("訊息");
			jf.setSize(210, 120);
			jf.setLocationRelativeTo(null);
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jf.setVisible(true);
			jf.add(jpl1, "North");
			jf.add(jpl2, "Center");

			jbtok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jf.dispose();// 使視窗能被關掉
				}
			});
		} else {
			File file = new File("A11.txt"); // 導入文件
			StringBuilder contents = new StringBuilder(); //讀取內容
			BufferedReader reader = null;
			//存取內容
			try {
				reader = new BufferedReader(new FileReader(file));
				String text = null;

				// repeat until all lines is read
				while ((text = reader.readLine()) != null) {
					contents.append(text).append(
							System.getProperty("line.separator"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				// 開始解碼
				String key = contents.toString(); // 獲得物件內容
				char[] charArray = key.toCharArray(); // 將所獲得的內容改變成字元
				int k = Integer.parseInt(jtf.getText()); // 獲得文字框內的數字
				String a = ""; 
				
				for (char c : charArray) {
					
					int s = (int) c; // 將獲得的字元變成數字並存入s
					s = s - k; // 進行解碼

					if (s < 32) //在ASCII中數值不能小於32
					{
						System.out.print(""); 
					} 
					else {
						char t = (char) s;//將解出的數字再轉回字元
						a = a + String.valueOf(t); // 讓所有字元組成字串
					}
					   A11.jbl2.setText(a); // 輸出結果

				}

			}

		}

	}
}
