package ce1002.a11.s102502522;
import java.io.*;
import java.awt.FlowLayout;
import java.awt.event.*;
import javax.swing.*;
public class MyFrame extends JFrame implements ActionListener{
	
	private JButton button = new JButton("Decrypt"); 
	private JTextField textfield = new JTextField(20);
	private JOptionPane o=new JOptionPane();
	private JLabel label=new JLabel();
	private char data[]=new char[500];
	private int num,key;  
	
	public MyFrame() {
		setTitle("A11-s102502522");  //設定標題
		setVisible(true);
		setBounds(500,200,350,150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout(FlowLayout.CENTER));
	    getContentPane().add(button);  //加入按鈕
	    getContentPane().add(textfield);
	    getContentPane().add(label);
	    button.addActionListener(this);
	    textfield.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{ 
		if(textfield.getText().isEmpty())  //若輸入空白
			o.showConfirmDialog(this,"Can’t be empty!!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
		else if(textfield.getText().equals("1")){  //若輸入1則將key值設為1
			this.key=1;
		}
		else if(textfield.getText().equals("2")){
			this.key=2;
		}
		else if(textfield.getText().equals("3")){
			this.key=3;
		}
		else if(textfield.getText().equals("4")){
			this.key=4;
		}
		else if(textfield.getText().equals("5")){
			this.key=5;
		}
		else if(textfield.getText().equals("6")){
			this.key=6;
		}
		else if(textfield.getText().equals("7")){
			this.key=7;
		}
		else if(textfield.getText().equals("8")){
			this.key=8;
		}
		else if(textfield.getText().equals("9")){
			this.key=9;
		}
		else
			o.showConfirmDialog(this,"Out Of Range!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
		
		try {  
	
			FileReader fr=new FileReader("A11.txt");  //將檔案讀入
			num=fr.read(data);  //將資料傳入data陣列並將字元數設定給num
		    char word[]=new char[num];
		    for(int i=0; i<num; i++){
		    	if(1<=key && key<=9)  //若key值在1~9之間才進行解碼
		    	word[i]=(char) (data[i]-key);  //將data解碼後傳入word陣列
		    }
		    String str=new String(word,0,num);  //將word陣列轉為string型態
		    label.setText(str);  //設定標籤文字
		} 
		catch (IOException e1) 
		{
			e1.printStackTrace();
		}	
	}
}
