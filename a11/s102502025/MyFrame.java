package ce1002.a11.s102502025;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener {
	JTextField txt = new JTextField(13);//create txt name
	JButton button = new JButton();//create button setting
	JPanel panel = new JPanel();//create panel setting
	JLabel label1 = new JLabel();//create label setting
	JLabel label2 = new JLabel();

	public MyFrame() {
		Container c = this.getContentPane();
		button.setText("Decrypt");//create button name
		label1.setText("Key:");
		label2.setText("Result?");
		panel.add(label1);
		panel.add(txt);
		panel.add(button);
		panel.add(label2);
		c.add(panel);
		button.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ("".equals(txt.getText())) {//if blank,it will......
			final JFrame fr = new JFrame("ӍϢ");
			fr.setLayout(new BorderLayout());
			ImageIcon icon = new ImageIcon("src/ce1002/a11/s102502025/images.jpg");
			JButton bt = new JButton("�_��");
			JPanel pn1 = new JPanel();
			pn1.setSize(100, 50);
			JPanel pn2 = new JPanel();
			pn2.setSize(100, 50);
			JLabel label = new JLabel();
			fr.setSize(250, 135);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("Can't be empty!");
			pn1.add(new JLabel(icon));
			pn1.add(label);
			pn2.add(bt);
			fr.add(pn1, BorderLayout.CENTER);
			fr.add(pn2, BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();
				}
			});
		} else if (Integer.parseInt(txt.getText()) < 1 || Integer.parseInt(txt.getText()) > 9) {//if answer on...,it will
			final JFrame fr = new JFrame("ӍϢ");
			fr.setLayout(new BorderLayout());
			ImageIcon icon = new ImageIcon("src/ce1002/a11/s102502025/images.jpg");
			JButton bt = new JButton("�_��");
			JPanel pn1 = new JPanel();
			pn1.setSize(100, 50);
			JPanel pn2 = new JPanel();
			pn2.setSize(100, 50);
			JLabel label = new JLabel();
			fr.setSize(250, 135);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("Out Of Range!");
			pn1.add(new JLabel(icon));
			pn1.add(label);
			pn2.add(bt);
			fr.add(pn1, BorderLayout.CENTER);
			fr.add(pn2, BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();
				}
			});
		} else {
			File file = new File("A11.txt");
			StringBuilder contents = new StringBuilder();//create the sentence
			BufferedReader reader = null;//delat reader
			try {
				reader = new BufferedReader(new FileReader(file));
				String text = null;

				while ((text = reader.readLine()) != null) {
					contents.append(text).append(
							System.getProperty("line.separator"));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (reader != null) {
						reader.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				String str = contents.toString();
				char[] charArray = str.toCharArray();
				int k = Integer.parseInt(txt.getText());
				String a = "";
				for (char c : charArray) {
					int s = (int) c;
					s = s - k;
					if (s < 32) {
						System.out.print("");
					} else {
						char z = (char) s;
						a = a + String.valueOf(z);
					}
					label2.setText(a);
				}
			}

		}
	}

}
