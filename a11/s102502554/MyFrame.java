package ce1002.a11.s102502554;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	 private JButton D;
	 private JTextField Key;
	 private JLabel label;
	
	 private class ButtonListener implements ActionListener{
		 
		 JFrame frame;
	     final MyFrame Frame;
		 
		 private void Empty()
	     {
	         JOptionPane.showMessageDialog(frame, "Can't be empty!");
	     }//print if can't find file

	     private void OutOfRange()
	     {
	         JOptionPane.showMessageDialog(frame, "Out Of Range!");
	     }//print if Out Of Range
		 
		 public void actionPerformed(ActionEvent e){
			 if(Key.getText().isEmpty()){//if key is empty
				 Empty();
	     }
			 else if(Integer.valueOf(Key.getText()).intValue() < 1 || Integer.valueOf(Key.getText()).intValue() > 9){
				 //use "Integer.valueOf(Key.getText()).intValue()" to find the input number
                 OutOfRange();
         } 
			else{
             String fileName = "A11.txt";//read the file
             try
             {
                 DataInputStream input = new DataInputStream(new FileInputStream(fileName));
                 String str ;
                 byte c;
                 for(str = ""; input.available() > 0; str = (new StringBuilder(String.valueOf(str))).append((char)c).toString())
                 {
                     c = input.readByte();
                     int key = Integer.valueOf(Key.getText()).intValue();
                     c -= key;
                 }//convert the string

                 label.setText(str);
                 input.close();
             }
             catch(FileNotFoundException e1)
             {
                 e1.printStackTrace();
             }
             catch(NumberFormatException e1)
             {
                 e1.printStackTrace();
             }
             catch(IOException e1)
             {
                 e1.printStackTrace();
             }
         }
     }

     

     ButtonListener(JFrame frame)
     {
         Frame = MyFrame.this;
         this.frame = frame;
     }
 }


 MyFrame()
 {
     D = new JButton("Decrypt");
     Key = new JTextField(10);
     label = new JLabel("Result?");
     JPanel kpanel = new JPanel();
     kpanel.add(new JLabel("Key:"));
     kpanel.add(Key);
     kpanel.add(D);
     JPanel panel1 = new JPanel();
     panel1.add(label);
     add(kpanel, "North");
     add(panel1, "Center");
     D.addActionListener(new ButtonListener(this));
     
     setTitle("A11-102502554");
     setDefaultCloseOperation(EXIT_ON_CLOSE);
     setBounds(0,0,300,100);
     setVisible(true);
 }//set the state of frame

}