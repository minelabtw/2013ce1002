package ce1002.a11.s102502537;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyPanel myPanel = new MyPanel();
	
	public MyFrame() {
		super("A11-102502537");
		setLayout(null);
		setSize(250, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		add(myPanel);
	}
}