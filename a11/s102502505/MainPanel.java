package ce1002.a11.s102502505;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;

public class MainPanel extends JPanel {

	String keystring = null;//解密密碼string型態
	int key = 0;//解密密碼int型態
	int keystringlength;//解密密碼長度
	int contentlength;//內容長度
	char[] content = new char[100];//內容

	public MainPanel() {

		add(new JLabel("key:"));
		final JTextField textfield = new JTextField(10);
		add(textfield);
		JButton button = new JButton("Decrypt");
		add(button);
		final JLabel label = new JLabel("Result?");
		add(label);

		button.addActionListener(new ActionListener() {//設定按下按鈕後的動作
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				keystring = textfield.getText();//取得位於textfield中的字串
				keystringlength = keystring.length();//取得字串長度
				if (keystringlength == 0) {//假如等於零(空字串)，即顯示下列對話框

					final JDialog dialog = new JDialog(new JFrame(), "訊息");
					dialog.setSize(150, 100);
					dialog.setLayout(new FlowLayout());
					JButton button = new JButton("確定");
					dialog.add(new JLabel("Can’t be empty!!"));
					dialog.add(button);
					dialog.setLocationRelativeTo(null);
					dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
					dialog.setVisible(true);

					button.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							dialog.dispose();//按下按鈕後仍可繼續跑程式
						}
					});
				}

				else {//假如不為空字串
					key = Integer.parseInt(keystring);//設定解密密碼從string變為int

					if (key > 10 || key < 0) {//判斷key條件，假如不符合，跑出下列對話框

						final JDialog dialog = new JDialog(new JFrame(), "訊息");
						dialog.setSize(150, 100);
						dialog.setLayout(new FlowLayout());
						JButton button = new JButton("確定");
						dialog.add(new JLabel("Out of range!"));
						dialog.add(button);
						dialog.setLocationRelativeTo(null);
						dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
						dialog.setVisible(true);

						button.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								dialog.dispose();
							}
						});
					}
				}

				try {//讀取加密檔案
					DataInputStream input = new DataInputStream(
							new FileInputStream("A11.txt"));
					contentlength=input.available();//取得檔案字串長度
					for (int i = 0; i < contentlength; i++) {
						content[i] = (char) input.readByte();//將字元存入char中
					}

				} catch (FileNotFoundException e1) {//抓例外
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				char[] newcontent = new char[contentlength];//解密，設新的char存解密後內容
				for (int j = 0; j < contentlength; j++) {
					newcontent[j] = (char) (content[j] - key);//一一將內容解密
				}

				String decrypt = String.copyValueOf(newcontent);//將char轉為string
				label.setText(decrypt);//將解密完後的內容輸出在label中

			}
		});
	}
}
