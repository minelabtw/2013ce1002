package ce1002.a11.s102502001;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

class Frame extends JFrame {

	JButton de = new JButton();                          //set panel
	JTextField field = new JTextField(8);
	JLabel result = new JLabel();
	ActionListener decode_listener;

 
	void Frame() {
		setTitle("A11-102502001");   
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
		setLocationRelativeTo(null);   
		setLayout(null);      
		New();    
		setSize(250, 120);   
		setVisible(true);
   }

   void New() {
      decode_listener = new Decode_listener();
      de = new JButton("Decrypt");
      de.addActionListener(decode_listener);
      add(de);
      add(field);
      result = new JLabel("");                        //create a new label to put the result 
      result.setBounds(5, 40, 200, 50);
      add(result);
  }


   class Decode_listener implements ActionListener {
	   String key;
	   String content;
       Decode_listener() {                           //constructor
       }

     public void actionPerformed(ActionEvent e) {
    	 key = Frame.this.field.getText();          //get message
    	 if(key.isEmpty()) {  
    		 show_error_message("Can't be empty!!");
          } else if(key.matches("^[1-9]$")) {            
        	  content = read_file("A11.txt");           
        	  String str = "";          
        	  int key_value = Integer.parseInt(key);
              for(char c : content.toCharArray()) {
                  str += (char)(c - key_value);
              }
             result.setText(str);

          } else {
              show_error_message("Out Of Range!");     //judge the range   
          }
     }
       String read_file(String filename) {
           String content = "";
           try {
              FileReader file = new FileReader("A11.txt");
              BufferedReader input = new BufferedReader(file);
              while (input.ready()) {
                   content += input.readLine() + "\n";
              }
           } catch (IOException event) {
               event.printStackTrace();
          }
          return content;
       }

      void show_error_message(String massage) {     //show the message
       JOptionPane.showMessageDialog(null, massage, "A10", JOptionPane.ERROR_MESSAGE);
     }
  }
}

