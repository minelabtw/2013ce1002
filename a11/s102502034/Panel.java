package ce1002.a11.s102502034;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Panel extends JPanel {
	Panel() {
		JLabel labelkey = new JLabel("Key");
		JLabel labelcontent = new JLabel("Content");
		labelcontent.setFont(new Font(getName(), Font.BOLD, 15));
		JButton DecryptBtn = new JButton("Decrypt");
		JTextField KeyText = new JTextField(14);
		JTextArea Content = new JTextArea(18, 19);
		add(DecryptBtn, BorderLayout.WEST);
	    add(labelkey,BorderLayout.WEST);
		add(KeyText, BorderLayout.NORTH);
		add(labelcontent,BorderLayout.WEST);
		add(Content, BorderLayout.SOUTH);
		DecryptBtn.addActionListener(new Decrypt(KeyText,Content));
		// add textfield , textarea,button to panel
	}
}
