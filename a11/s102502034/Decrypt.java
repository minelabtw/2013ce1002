package ce1002.a11.s102502034;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Decrypt implements ActionListener {
	private JTextField key;
	private JTextArea result;

	Decrypt(JTextField text, JTextArea area) {
		key = text;
		result = area;
		// set parameter

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int n;
		int asciinumber = 0;
		String line;
		String decryptline;
		if (key.getText().trim().equals("")) {
			JOptionPane.showMessageDialog(null, "Can't be empty!", "Info ",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		n = Integer.parseInt(key.getText());
		// convert string to int (key)
		if (n > 9 || n < 1) {
			JOptionPane.showMessageDialog(null, "Out Of Range!", "Info ",
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		File file = new File("A11.txt");
		Scanner input;
		try {

			input = new Scanner(file);
			while (input.hasNextLine()) {
				decryptline = "";
				line = input.nextLine();

				for (int i = 0; i < line.length(); i++) {
					asciinumber = line.charAt(i) - n;
					decryptline += Character.toString((char) asciinumber);
				}
				// decrypt line by line
				result.setText(decryptline);
			}

		} catch (FileNotFoundException e1) {
			JOptionPane.showMessageDialog(null, "File is not exist!", "Info ",
					JOptionPane.INFORMATION_MESSAGE);
			// show information

		}

	}
}
