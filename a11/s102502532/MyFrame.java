package ce1002.a11.s102502532;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;
import javax.swing.*;

public class MyFrame extends JFrame {

	private JButton button = new JButton("Decrypt"); // (字串)
	private JLabel label1 = new JLabel("Key:");
	private JTextField textF = new JTextField();
	private JLabel label2 = new JLabel("Result?");
	private String str;

	public MyFrame() {
		setLayout(null); // 板子顯現

		button.setBounds(170, 5, 80, 30); // Bounds 座標 + 大小
		label1.setBounds(5, 5, 80, 30);
		textF.setBounds(35, 10, 120, 20);
		label2.setBounds(5, 35, 120, 30);

		add(label1);
		add(textF);
		add(button);
		add(label2);

		button.addActionListener(new buttonListener());
	}

	private class buttonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
	
			try {

				if ( textF.getText().isEmpty() == true) {                 // 不能用textF.getText()
															// == null
					JOptionPane.showMessageDialog(null, "Can't be empty!!",
							"訊息", JOptionPane.INFORMATION_MESSAGE); // 建立對話視窗
					 
				}
				int n = Integer.valueOf(textF.getText()).intValue(); // 字串轉整數
				
				 if (n > 9 || n < 1) {
					JOptionPane.showMessageDialog(null, "Out Of Range!", "訊息",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					java.io.File file = new java.io.File("A11.txt"); // 不用ce1002.a10.s102502532/
					Scanner input = new Scanner(file);

					// file
					// DataOutputStream output = new DataOutputStream( new FileOutputStream("A11.txt") );
					// output.close();
					// DataInputStream input = new DataInputStream( new FileInputStream(file) );

					String buf = input.next();
					char[] ch1 = buf.toCharArray(); // 字串轉字元
					char[] ch2 = new char[buf.length()];

					// for( char i : ch1){ //循序讀取陣列內容
					// System.out.print( (char)( (int)i-n ) ); //1字母轉換 ASCII
					for (int i = 0; i < buf.length(); i++) {
						ch2[i] = (char) ((int) ch1[i] - n);
					}

					str = String.valueOf(ch2);
					label2.setText(str);
					// }
					input.close();
				}

			} catch (FileNotFoundException e1) {
				// TODO: handle exception

				// } catch (IOException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
			}

		}
	}
}
