package ce1002.a11.s102502031;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel {
	private int width;
	private int height;
	JLabel label = new JLabel("Key:");
	JTextField textField = new JTextField();
	JButton button = new JButton("Decrypt");
	JLabel resultlLabel = new JLabel("Result?");

	private MyPanel() {
		this.setLayout(null);
		this.setLocation(0, 0);
	} // end constructor MyPanel()

	public MyPanel(int width, int height) {
		this();
		this.setWidth(width);
		this.setHeight(height);
		this.setSize(width, height);
		this.panelDesign();
	} // end constructor MyPanel(width, height)

	private void panelDesign() {
		label.setBounds(10, 10, 25, 30);
		textField.setBounds(45, 10, this.getWidth() - 145, 30);
		button.setBounds(this.getWidth() - 90, 10, 80, 30);
		resultlLabel.setBounds(10, 50, getWidth() - 20, getHeight() - 60);

		this.add(label);
		this.add(textField);
		this.add(button);
		this.add(resultlLabel);

		DecryptListener decryptListener = new DecryptListener();
		button.addActionListener((ActionListener) decryptListener);
	} // end panalDesign

	class DecryptListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			String tempString = textField.getText();
			int key;
			switch (tempString.length()) {
				case 0:
					key = 0;
					break;
				case 1:
					if (tempString.charAt(0) > '9' || tempString.charAt(0) < '0') {
						key = -1;
					} // end if
					else {
						key = tempString.charAt(0) - '0';
					} // end else
					break;
				default:
					key = -1;
					break;
			} // end switch

			switch (key) {
				case 0:
					JOptionPane.showMessageDialog(null, "Key value can't be empty!", "Message", JOptionPane.ERROR_MESSAGE);
					break;
				case -1:
					JOptionPane.showMessageDialog(null, "Key value out of range!", "Message", JOptionPane.ERROR_MESSAGE);
					break;
				default:
					try {
						DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));
						String temp = input.readLine();
						char[] resultChar = new char[temp.length()];
						for (int i = 0; i < resultChar.length; i++) {
							resultChar[i] = (char) (temp.charAt(i) - key);
						} // end for
						String resultString = new String(resultChar);
						resultlLabel.setText(resultString);
					} // end try
					catch (EOFException eofException) {
						System.out.println("End");
					} // end catch
					catch (IOException ioException) {
						ioException.printStackTrace();
					} // end catch
					break;
			} // end switch
		} // end actionPerformed(actionEvent)
	} // end class DecryptListener

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(this.getWidth(), this.getHeight());
	} // end getPreferredSize()

	public int getWidth() {
		return width;
	} // end getWidth()

	public int getHeight() {
		return height;
	} // end getHeight()

	public void setWidth(int width) {
		this.width = width;
	} // end setWidth(width)

	public void setHeight(int height) {
		this.height = height;
	} // end setHeight(height)
}