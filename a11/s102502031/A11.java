package ce1002.a11.s102502031;

public class A11 {
	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		MyPanel panel = new MyPanel(600, 90);

		frame.add(panel);
		frame.pack();
		frame.setLocationRelativeTo(null);
	} // end main
} // end class A11