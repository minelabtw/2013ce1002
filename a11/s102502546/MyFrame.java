package ce1002.a11.s102502546;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{
	
	public MyFrame(String MESSAGE)
	{
		setBounds(150,150,100,50);
		JLabel l1 = new JLabel(MESSAGE);
		l1.setBounds(0,0,80,40);
		add(l1);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
