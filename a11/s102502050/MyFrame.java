package ce1002.a11.s102502050;
import javax.swing.*;

import java.util.Scanner;
import java.awt.event.*;

public class MyFrame extends JFrame {
	JPanel panel = new JPanel();
	JButton button = new JButton("Encrypt");
	JLabel label = new JLabel();
	JTextField field = new JTextField(1);
	
	char[] key;
	char[] code;
	public MyFrame() throws Exception
	{
		setLayout(null);
		setBounds(200,100,400,400);
		panel.setLayout(null);
		panel.add(field);
		panel.add(button);
		panel.add(label);
		
		panel.setVisible(true);
		
		panel.setBounds(0,0,200,200);
		field.setBounds(20,20,50,40);
		button.setBounds(80,20,100,40);
		label.setBounds(20,80,200,40);
		//設定大小
		
		add(panel);
		setVisible(true);
		button.addActionListener(new Buttonclick());
	}
	
	class Buttonclick implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			java.io.File file = new java.io.File("A11.txt");
			key = field.getText().toCharArray();
			if(field.getText().trim().equals(""))//textfield為空
			{
				JFrame frame = new JFrame();
				JPanel panel2 = new JPanel();
				panel2.add(new JLabel("Can't be empty!!"));
				JOptionPane.showMessageDialog(frame, panel2);
			}
			else if ( (key[0]>'9' || key[0] <'1' )|| field.getText().length()!=1  )//out of range
			{
				JFrame frame = new JFrame();
				JPanel panel2 = new JPanel();
				panel2.add(new JLabel("Out Of Range!"));
				JOptionPane.showMessageDialog(frame, panel2);
			}
			else
			{
				try//有檔案有exception
				{
					
					Scanner scanner = new Scanner(file);
					
					
						code = scanner.nextLine().toCharArray();
						System.out.println(String.valueOf(code));
						for(int j=0;j<code.length;j++)
							code[j]=(char)(code[j]-key[0]+'0');//解碼
						label.setText(String.valueOf(code));
					
				}
				catch(Exception exception)
				{
					
				}
			}
		}
	}
}
