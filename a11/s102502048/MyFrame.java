package ce1002.a11.s102502048;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame 
{
	private JButton jbtDecrypt = new JButton("Decrypt");
	private JTextField jtfKey = new JTextField(10);
	private JLabel jlabel = new JLabel("Result?");

	MyFrame() 
	{
		/*Create panel*/
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		/*add to panel*/
		p1.add(new JLabel("Key:"));
		p1.add(jtfKey);
		p1.add(jbtDecrypt);		
		p2.add(this.jlabel);
		/*add to frame*/
		add(p1, "North");
		add(p2, "Center");

		this.jbtDecrypt.addActionListener(new ButtonListener());
	}

	private class ButtonListener implements ActionListener 
	{
		ButtonListener() 
		{
			
		}
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			if (jtfKey.getText().equals("") ) 
				showEmptyMsg();	
			else if (Integer.parseInt(jtfKey.getText(),10) < 1 || Integer.parseInt(jtfKey.getText(),10) > 9 ) 			
				showOutOfRange();
			else 
			{
				String fileName = "A11.txt";
				try 
				{
					//Create an input stream for file 
					DataInputStream input = new DataInputStream(new FileInputStream(fileName));

					String str = "";
					while (input.available() > 0) //Read content from the file
					{
						byte c = input.readByte();
						int key = Integer.parseInt(jtfKey.getText(),10);
						
						c = (byte) (c - key);
						str = str + (char) c;
					}
					jlabel.setText(str);
					input.close();//Close input stream
				} 
				catch (FileNotFoundException ex) 
				{
					ex.printStackTrace();
				} 
				catch (NumberFormatException ex) 
				{
					ex.printStackTrace();
				} 
				catch (IOException ex) 
				{
					ex.printStackTrace();
				}
			}
		}
		/*setshowMessageDialog*/
		private void showEmptyMsg() 
		{
			JOptionPane.showMessageDialog(null, "Can't be empty!");
		}
		private void showOutOfRange() 
		{
			JOptionPane.showMessageDialog(null, "Out Of Range!");
		}
	}
}