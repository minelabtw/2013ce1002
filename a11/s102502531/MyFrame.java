package ce1002.a11.s102502531;

import java.io.*;

import javax.swing.*;


import java.awt.event.*;

class MyFrame extends JFrame {

	ActionListener decode_listener;

	MyFrame() {
		JButton jbutton = new JButton("Decrypt"); // 建立按鈕
		jbutton.setBounds(5, 5, 100, 40);
		jbutton.addActionListener(decode_listener);
		JTextField jtextfield = new JTextField();
		jtextfield.setBounds(110, 5, 400, 40);
		JLabel jlabel = new JLabel("Result?");  
		jlabel.setBounds(5, 50, 100, 100);
		JLabel jlabel_1 = new JLabel();
		jlabel_1.setBounds(110, 50, 100, 100);
		UseActionListener useactionlistener = new UseActionListener(jtextfield,jlabel_1,jbutton); //把資料傳入去解密
		jbutton.addActionListener(useactionlistener);
		add(jlabel_1);
		add(jbutton);
		add(jtextfield);
		add(jlabel);
	}

}
