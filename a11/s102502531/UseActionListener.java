package ce1002.a11.s102502531;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UseActionListener implements ActionListener {
	String content;
	JTextField jtextfield;
	JLabel jlabel_1;
	JButton jbutton;

	UseActionListener(JTextField jtextfield, JLabel jlabel_1, JButton jbutton) {  //解成正確輸出
		this.jtextfield = jtextfield;
		this.jlabel_1 = jlabel_1;
		this.jbutton = jbutton;
	}

	public void actionPerformed(ActionEvent e) { //監聽

		if (jtextfield.getText().trim().length() == 0) {
			JOptionPane.showMessageDialog(null, "File name can't be empty!");
		}
		if (Integer.parseInt(jtextfield.getText()) < 1
				|| Integer.parseInt(jtextfield.getText()) > 9) {
			JOptionPane.showMessageDialog(null, "Out of range!");
		} else {
			String string_1 = "";  //扣除錯誤再來做解密
			String string_2 = "";
			try {
				BufferedReader bufferedreader = new BufferedReader(
						new FileReader("A11.txt"));
				 string_1 += bufferedreader.readLine() + "\n";
				char[] temp =  string_1.toCharArray();
				for (int i = 0; i < temp.length; i++) {
					string_2 += String.valueOf((char) (temp[i] - Integer
							.parseInt(jtextfield.getText())));
				}
				bufferedreader.close();
			} catch (IOException event) {
				System.out.println("get a IOException a");
			}
			jlabel_1.setText(string_2);

		}
	}
}
