package ce1002.a11.s101502205;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.Scanner; 


public class MyFrame extends JFrame{
	private JPanel panel = new JPanel();
	private JLabel textLabel = new JLabel("key:");
	private JTextField textField= new JTextField();;
	private JButton btnDecrypt = new JButton("Decrypt");
	private JLabel codeLabel = new JLabel("Result?");
	
	public MyFrame() {
		//panel.setBounds(0, 0, getWidth(), getHeight());
		panel.setLayout(null);
		
		// Create a label for text "key:"
		textLabel.setBounds(15, 16, 50, 20);
		panel.add(textLabel);

		// Create text Field
		textField.setBounds(50, 15, 140, 25);
		panel.add(textField);

		// Create decrypt button
		btnDecrypt.setBounds(200, 12, 80, 30);
		panel.add(btnDecrypt);
		
		// Add listener
		BtnListener btnListener = new BtnListener();
		btnDecrypt.addActionListener(btnListener);
		
		// Add label for decrypted code
		codeLabel.setBounds(15, 50, 290, 20);
		panel.add(codeLabel);
		
		// Add the panel to frame
		add(panel);
		
		// set title and settings
		setTitle("A11-101502205");
		setSize(310, 130);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	private void Decrypt() {
		String encryptedCode;
		String keyStr = textField.getText();
		int key;
		String code = "";
		try{
			// Test if 1<=key<=9
			// Check length first
			if(keyStr.length() == 0) {
				throw new Exception("Can��t be empty!!");
			}else if(keyStr.length() != 1) {
				throw new Exception("Out of range!");
			}
			key = keyStr.charAt(0) - '0';
			// then check value
			if(key<1 || key>9) {
				throw new Exception("Out of range!");
			}
			
			// open file
			File file = new File("A11.txt");				// may throw IOException (ex: access denied)
			// create scanner
			Scanner input = new Scanner(file);
			// read to string
			encryptedCode = input.next();
			for(int i=0; i<encryptedCode.length(); i++) {
				// decrypting
				code += (char)(encryptedCode.charAt(i) - (char)key);
			}
			// Output result
			codeLabel.setText(code);
		}catch(IOException ioe) {
			// IOException handling
            JOptionPane.showMessageDialog(null, ioe.getMessage());
        }catch(Exception e) {
        	// Exception handling
        	JOptionPane.showMessageDialog(null, e.getMessage());
        }
	}
	
	// Listener for button
	class BtnListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			Decrypt();
		}
	}
}
