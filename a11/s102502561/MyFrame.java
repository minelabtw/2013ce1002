package ce1002.a11.s102502561;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	private JButton Dbutton = new JButton("Decrypt");// button
	private JTextField Text = new JTextField(10);// textfield
	private JLabel Label = new JLabel("Result?");// label

	MyFrame() {
		setTitle("A11-102502561");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(350, 150, 400, 300);
		setVisible(true);// frame^
		JPanel pane1 = new JPanel();// panel
		JPanel pane2 = new JPanel();
		pane1.add(new JLabel("Key: "));
		pane1.add(this.Text);
		pane1.add(this.Dbutton);
		pane2.add(this.Label);
		add(pane1, "North");
		add(pane2, "Center");// panel
		this.Dbutton.addActionListener(new ButtonListener(this));
	}

	private class ButtonListener implements ActionListener {
		JFrame frame;

		ButtonListener(JFrame frame) {
			this.frame = frame;// 在建構子中把this.frame傳到 buttonlistener中的this
		}

		public void actionPerformed(ActionEvent e) {
			if (MyFrame.this.Text.getText().isEmpty()) {// Text.getText() =>
														// 這是輸入的數字
				JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
			} else if ((Integer.valueOf(MyFrame.this.Text.getText()) < 1)
					|| (Integer.valueOf(MyFrame.this.Text.getText()) > 9)) {
				JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
			} else {
				String fileName = "A11.txt";
				try {
					DataInputStream input = new DataInputStream(
							new FileInputStream(fileName));

					String str = "";
					while (input.available() > 0) {
						byte c = input.readByte();
						int key = Integer.valueOf(MyFrame.this.Text.getText());
						c = (byte) (c - key);
						str = str + (char) c;
					}
					MyFrame.this.Label.setText(str);
					input.close();// close DataInputStream
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
