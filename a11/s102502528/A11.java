package ce1002.a11.s102502528;

import java.awt.event.*;

import javax.swing.*;

import java.io.*;

public class A11 extends JFrame {

	JButton decrypt;
	JTextField key;
	JLabel result;
	ActionListener decode_listener;

	public static void main(String[] args) {
		new A11();
	}
	
	A11() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		setTitle("A11-102502528");

		new_all_member();

		setSize(350, 200);
		setVisible(true);
	}

	void new_all_member() {
		decode_listener = new Decode_listener();

		decrypt = new JButton("Decrypt");           //gui
		decrypt.addActionListener(decode_listener);
		decrypt.setBounds(150, 5, 100, 30);
		add(decrypt);

		key = new JTextField(10);
		key.setBounds(5, 5, 140, 30);
		add(key);

		result = new JLabel("Result will be shown here.");
		result.setBounds(5, 40, 390, 30);
		add(result);
	}

	class Decode_listener implements ActionListener {

		String key;
		String content;

		Decode_listener() {
		}

		public void actionPerformed(ActionEvent e) {
			key = A11.this.key.getText();

			if (key.isEmpty()) {
				show_error_message("Can't be empty!!");
			} else if (key.matches("^[1-9]$")) {
				content = read_file("A11.txt");  //read file
				String str = "";
				int key_value = Integer.parseInt(key);
				for (char c : content.toCharArray()) { //decode
					str += (char) (c - key_value);
				}
				result.setText(str);
			} else {
				show_error_message("Out Of Range!");
			}
		}

		String read_file(String filename) {
			String content = "";
			try {
				FileReader file = new FileReader("A11.txt");
				BufferedReader input = new BufferedReader(file);
				while (input.ready()) {
					content += input.readLine() + "\n";
				}
			} catch (IOException event) {
				event.printStackTrace();
			}
			return content;
		}

		void show_error_message(String massage) { // display error message
			JOptionPane.showMessageDialog(null, massage, "A11",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
