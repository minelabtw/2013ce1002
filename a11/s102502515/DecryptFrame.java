package ce1002.a11.s102502515;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class DecryptFrame extends JFrame {
	private JButton DecryptBtn = new JButton("Decrypt");//Button
	private JTextField textfield = new JTextField();//input number
	private JLabel answerLbl = new JLabel();//for showing answer
	private JLabel popLbl = new JLabel();//for popping out text
	
	DecryptFrame(){//constructor starts
		//frame settings
		setLayout(null);
		setBounds(300,300,260,150);
		setTitle("A11-102502515");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//button settings;
		DecryptBtn.setBounds(20,20,80,30);
		add(DecryptBtn);
		DecryptBtn.addActionListener(new DecryptListener());
		
		//textfield settings
		textfield.setBounds(120,20,100,30);
		textfield.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		add(textfield);
		
		//answerLbl settings
		answerLbl.setBounds(80,60,100,50);
		answerLbl.setHorizontalAlignment(SwingConstants.CENTER);
		answerLbl.setText("Result?");
		add(answerLbl);
		
		setVisible(true);
	}//constructor ends
	
	class DecryptListener implements ActionListener{//DecryptListener starts
		@Override
		public void actionPerformed(ActionEvent e){//actionPerformed starts
			
			try {//first do
				check(textfield.getText());//check the file name isEmpty?
				
				//read the file
				java.io.File file = new java.io.File("A11.txt");
				Scanner input = new Scanner(file);

					char password [] = input.nextLine().toCharArray();//get content, into char array

					for (int i = 0 ; i < password.length ; i++){//for for decryption
						password[i] = (char)((int)password[i] - Integer.parseInt(textfield.getText()));
					}
				String str = new String(password);//put char array into string
				answerLbl.setText(str);//output string
				input.close();

			} catch (Exception e1) {
					//if file name is empty
					popLbl.setText("" + e1);
				    popLbl.setBackground(Color.BLUE);
				    popLbl.setMinimumSize(new Dimension(200,200));
					JFrame conframe = new JFrame("");//popping out frame
				    JOptionPane.showMessageDialog(conframe, popLbl);
			}
		}//actionPerformed ends

		public boolean check(String filetext) throws Exception{//check the file name isEmpty?
			if(filetext.isEmpty()){
				throw new Exception("Can't be empty!");
			}
			else if ( Integer.parseInt(filetext) > 10 ||  Integer.parseInt(filetext) < 0){
				throw new Exception("Out of range!");
			}
			else
				return true;
		}
	}//DecryptListener ends
}
