package ce1002.a11.s102502030;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener{

	JTextField code = new JTextField();
	JButton decript = new JButton( "Decript" );
	JLabel key = new JLabel( "Key:" );
	JLabel result = new JLabel( "Result?" );
	
	public MyFrame() {
		//設定frame
		setTitle( "A10-102502030" );
		setVisible( true );
		setSize( 300, 100 );
		setLocation( 300, 300 );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		
		decript.addActionListener(this);
		add( key, BorderLayout.WEST );
		add( code, BorderLayout.CENTER );
		add( decript, BorderLayout.EAST );
		add( result, BorderLayout.SOUTH );
	}
	
	public void actionPerformed( ActionEvent e ) {
		DataInputStream input = null;
		if( code.getText().isEmpty() ) {
			//if empty
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog( frame, "Can’t be empty!!" );
		}
		else if( Integer.parseInt(code.getText())<1 ||  Integer.parseInt(code.getText())>9 ) {
			//if out of range
			JFrame frame = new JFrame();
			JOptionPane.showMessageDialog( frame, "Out Of Range!" );
		}
		else {
				try {
					//get data
					int keynum = Integer.parseInt(code.getText());
					input = new DataInputStream( new FileInputStream( "A11.txt" ) );
					char[] secret = input.readLine().toCharArray();
					//change code
					for( int i=0; i<secret.length; i++ ) {
						secret[i] -=  keynum;
					}
					String answer = String.valueOf( secret );
					result.setText( answer );
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		}
	}
	
	
}
