package ce1002.a11.s101201521;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import javax.swing.*;
import java.io.IOException;

public class A11 extends JFrame{
	//label to request key
	private JLabel keyLb = new JLabel("Key:");
	//label to present result
	private JLabel resultLb = new JLabel("Result?");
	//button to decrypt
	private JButton decryptBt = new JButton("Decrypt");
	//text field to catch key
	private JTextField keyTf = new JTextField(10);
	//panel to contain keyLb, keyTf, and decryBt
	private JPanel upperPnl = new JPanel();
	//the key
	private int key = 0;
	public A11(String title){
		super(title);
		setLayout(new GridLayout(2,1));
		//layout
		add(upperPnl);
		upperPnl.add(keyLb);
		upperPnl.add(keyTf);
		upperPnl.add(decryptBt);
		decryptBt.addActionListener(new BtListener());
		add(resultLb);
		resultLb.setHorizontalAlignment(JLabel.CENTER);
		//setting
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		A11 frame = new A11("A11-101201521");
	}
	//listener for decryptBt
	class BtListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//the result
			String result = "";
			
			if(keyTf.getText().length() == 0){
				JOptionPane.showMessageDialog(null, "Can't be empty!");
			}
			else if(Integer.parseInt(keyTf.getText()) < 1  || Integer.parseInt(keyTf.getText()) > 9){
				JOptionPane.showMessageDialog(null, "Out Of Range!");
			}
			else{//decrypting
				key = Integer.parseInt(keyTf.getText());
				try {
					
					FileInputStream input = new FileInputStream("A11.txt");
					while(input.available()!=0){
						result = result + (char)(input.read() - key);
					}
					input.close();
					resultLb.setText(result);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
			
		}
		
	}
}
