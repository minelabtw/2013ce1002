package ce1002.a11.s102502539;

import java.awt.event.*;
import java.io.DataInputStream;
import java.io.FileInputStream;
import javax.swing.*;

public class Panel extends JPanel
{
	public JButton decrypt = new JButton("Decrypt");
	public JTextField key_field = new JTextField();
	public JLabel key_label = new JLabel("Key: ");
	public JLabel result_label = new JLabel("Result?");
	public int key;
	
	public Panel()
	{
		setSize(480, 360);
		setLayout(null);
		
		key_label.setBounds(35, 15, 30, 20);
		add(key_label);
		
		key_field.setBounds(70, 15, 240, 20);
		add(key_field);
		
		DecryptListener dl = new DecryptListener();
		decrypt.addActionListener(dl);
		decrypt.setBounds(320, 15, 100, 20);
		add(decrypt);
		
		result_label.setBounds(200, 30, 60, 50);
		add(result_label);
	}
	
	class DecryptListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			JLabel pop_label = new JLabel();
			JFrame pop_frame = new JFrame();
			
			try 
			{
				key = Integer.valueOf(key_field.getText());
			} 
			catch (NumberFormatException nfe) { }
			
			if (key_field.getText().isEmpty())
			{
				pop_label.setText("Can��t be empty!!");
				JOptionPane.showMessageDialog(pop_frame, pop_label);
			}
			else if (key < 1 || key > 9)
			{
				pop_label.setText("Out Of Range!");
				JOptionPane.showMessageDialog(pop_frame, pop_label);
			}
			else 
			{
				try 
				{
					/* �I�s�ɮ� */
					DataInputStream in = new DataInputStream(new FileInputStream("A11.txt"));
					String new_s = "";
					
					while (in.available() > 0)
					{
						/* �ѱK */
						byte in_letter = in.readByte();
						int out_letter = (int)in_letter - Integer.valueOf(key_field.getText());
						char c = (char)out_letter;
						String s = String.valueOf(c);
						new_s += s;
						result_label.setText(new_s);
					}
				} 
				catch (Exception e2) { }
			}
		}
	}
}
