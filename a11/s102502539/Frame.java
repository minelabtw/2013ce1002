package ce1002.a11.s102502539;

import javax.swing.JFrame;

public class Frame extends JFrame
{
	Panel panel = new Panel();
	
	public Frame(String name)
	{
		setTitle(name);
		setVisible(true);
		setSize(480, 360);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(panel);
	}
}
