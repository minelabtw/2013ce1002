package ce1002.a11.s102502511;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	MyPanel panel = new MyPanel();
	
	MyFrame(){
		super("A10-102502511"); //視窗的名字
		setLayout(null);
		setBounds(0,0,300,120); //視窗大小
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		add(panel); //加入panel
	}
}
