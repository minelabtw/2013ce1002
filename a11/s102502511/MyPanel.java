package ce1002.a11.s102502511;


import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel implements ActionListener{

	private JButton button1 = new JButton("Decrypt"); //按鈕的名字
	private JTextField textfield = new JTextField();
	private JLabel lab4 = new JLabel();
	private JLabel lab1 = new JLabel("Key :"); //lab1的輸出
	private JLabel lab2 = new JLabel("Result ?");	//lab2的輸出
	
	MyPanel(){
		setLayout(null);
		setBounds(0,0,300,120); //panel的大小跟視窗一樣大
		button1.setBounds(180,10,100,25); //按鈕的大小
		textfield.setBounds(47,10,130,25); //輸入空格的大小
		
		Font font1 = new Font(Font.DIALOG, Font.BOLD, 15); //字形和大小
		lab1.setFont(font1);       //將lab1的字形根改
		lab1.setBounds(5,10,40,25); //lab1的大小位置
		
		Font font2 = new Font(Font.DIALOG, Font.BOLD, 15);
		lab2.setFont(font2);
		lab2.setBounds(120,50,70,25);
		
		lab4.setBounds(0,0,300,120); //lab4的大小位置
		
		add(button1); //加入所有東西進panel中
		add(textfield);
		add(lab1);
		add(lab2);
		add(lab4);
		
		
		button1.addActionListener(this); //點下按鈕時做動作
	}              	
	
	public void actionPerformed(ActionEvent e) { 
		try{
			check(textfield.getText());	 //檢查輸入的內容
			FileReader fr = new FileReader("A11.txt"); //讀取A11.txt的這個檔案
			BufferedReader br = new BufferedReader(fr); //將這個檔案的內容寫入br中
			char arrary [] = br.readLine().toCharArray(); //將br(A11.txt)的內容轉成字元
			for(int i = 0 ; i < arrary.length ; i++){ //將每個字元做一次ASCII的轉換 並開始解密
				arrary[i] = (char)((int)arrary[i] - Integer.parseInt(textfield.getText()));
			}
			String str = new String(arrary); //將這些字元組起來成字串
			lab2.setText(str); //在lab2中輸出字串			
		}catch(Exception ex1){
			JOptionPane.showMessageDialog(lab4, ex1.getMessage(),"訊息", //丟出例外中的訊息
					JOptionPane.INFORMATION_MESSAGE);	
		}
	}
	
	public boolean check (String file) throws Exception{ 
		if(textfield.getText().isEmpty()){ //若輸入內容是空的 , 丟出例外
			throw new Exception("Can't be empty!");
		}else if(Integer.parseInt(textfield.getText()) >= 10 || Integer.parseInt(textfield.getText()) <= 0){ //將數字的字串轉成數字 在去做判定 若超出範圍 丟出例外
			throw new Exception("Out Of Range!");
		}else{
			return true;
		}		
	}
	
	
}
