package ce1002.a11.s992001026;
import java.util.Scanner;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.BufferedReader ;
import java.io.IOException;


public class A11 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnNewButton ;
	private JLabel lblNewLabel ;
	
	// main 
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A11 frame = new A11();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public A11()  {
		
		setTitle("A11-s992001026");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 374, 237);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		btnNewButton = new JButton("Decrypt");
		btnNewButton.setBounds(233, 55, 90, 30);
		contentPane.add(btnNewButton);
		btnNewButton.addActionListener(new ButtonListener(this));
		
		textField  = new JTextField();
		textField.setBounds(35, 55, 179, 30);
		contentPane.add(textField);
		textField.setColumns(10);
		
		lblNewLabel = new JLabel("Result?");
		lblNewLabel.setBounds(35, 95, 288, 77);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Key :");
		lblNewLabel_1.setBounds(35, 24, 46, 15);
		contentPane.add(lblNewLabel_1);
	}
	
	// inner class
	private class ButtonListener implements ActionListener {
		JFrame frame;
		String str , decostr="";
		char[] ch ;
		
		public ButtonListener(JFrame frame) {
			this.frame = frame;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if (textField.getText().isEmpty()){
				showEmptyFileNameMsg();
			}else if (Integer.parseInt(textField.getText())<1  || Integer.parseInt(textField.getText()) >9){
				showOutOfRrangeMsg();
			}else {
			
				int key = Integer.parseInt(textField.getText());
				
				// read the file 
				try {
					Scanner scn = new Scanner( new File("A11.txt"));
					str = scn.nextLine();
					ch = str.toCharArray();
					// decode 
					for (int i = 0 ; i < ch.length ; ++i){
						decostr +=  (char)((int)ch[i]-key);
					}
					
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				// change the label's text !
				lblNewLabel.setText(decostr);
			}
			
		}
		
		private void showEmptyFileNameMsg() {
			JOptionPane.showMessageDialog(frame, "Key can't be empty!");
		}
 
		private void showOutOfRrangeMsg() {
			JOptionPane.showMessageDialog(frame, "Out of range!");
		}
		
		
		
	}
}
