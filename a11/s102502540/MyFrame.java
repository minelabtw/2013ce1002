package ce1002.a11.s102502540;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class MyFrame extends JFrame {

	JButton Decrypt = new JButton("Decrypt");
	JTextField Key = new JTextField();
	JLabel K = new JLabel("Key:");
	int key;
	JLabel R = new JLabel("Result?");

	public MyFrame() { // 設定視窗格式大小
		setTitle("A11-102502540");
		setVisible(true);
		setSize(400, 150);
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Decrypt.setBounds(250, 20, 120, 50);
		DecryptListener d = new DecryptListener();
		Decrypt.addActionListener(d);
		add(Decrypt);

		Key.setBounds(80, 20, 140, 50);
		add(Key);
		K.setBounds(30, 30, 50, 20);
		add(K);
		R.setBounds(130, 80, 50, 20);
		add(R);
	}

	class DecryptListener implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			JLabel l = new JLabel();
			JFrame f = new JFrame();
			try {
				key = Integer.valueOf(Key.getText());
			} catch (NumberFormatException e1) {

			}
			if (Key.getText().isEmpty()) // 若按下按鈕時，key值為空，則跳出對話框，對話框中顯示“Can’t be
											// empty!!"若按下按鈕時，key值超出範圍，則跳出對話框，對話框中顯示“Out Of Range!"
			{
				l.setText("Can'tbe empty!!");
				JOptionPane.showMessageDialog(f, l);
			}

			else if (key < 1 || key > 9) // 若按下按鈕時，key值超出範圍，則跳出對話框，對話框中顯示“Out Of
											// Range!"
			{
				l.setText("Out Of Range!");
				JOptionPane.showMessageDialog(f, l);
			}

			else {
				try { // 讀入文字檔(A11.txt)進行解密，並顯示出解密完畢後的結果
					DataInputStream input = new DataInputStream(
							new FileInputStream("A11.txt"));
					String s1 = "";
					while (input.available() > 0) {
						byte b = input.readByte();
						int temp = (int) b - Integer.valueOf(Key.getText());
						char c = (char) temp;
						String s = String.valueOf(c);
						s1 = s1 + s;
						R.setText(s1);
					}

				} catch (Exception e) {

				}
			}
		}
	}
}
