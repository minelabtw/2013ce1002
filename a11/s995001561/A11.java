package ce1002.a11.s995001561;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;



@SuppressWarnings("serial")
public class A11 extends JFrame {
	


//	private static byte key1;




	A11() {
		
		JLabel key = new JLabel("Key:");
		JTextField textfield = new JTextField(10);
		JButton button = new JButton("Decrypt");
		JLabel Result = new JLabel("Result?");
		
		JPanel panel = new JPanel();
		
		panel.add(key);
		panel.add(textfield);
		panel.add(button);
		panel.add(Result);
		
		add(panel);
		
		buttonListenerClass listener = new buttonListenerClass(textfield.getText());
		button.addActionListener(listener);

		
		
	}
	
	class buttonListenerClass implements ActionListener {
		
		String string;
		JFrame frame;
		
		buttonListenerClass(String str) {
			this.string  = str;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			int a = Integer.parseInt(string);
			
			if(a<1||a>9) showEmptyFileNameMsg();
		
		}

		private void showEmptyFileNameMsg() {
			
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
			
		}
		
	}

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		

		
		A11 frame = new A11();
		frame.setTitle("A11-995001561");
		frame.setSize(400,80);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		ObjectInputStream input = new ObjectInputStream(new FileInputStream("A11.txt"));
		  String[] newStrings = (String[])(input.readObject());
		  for (int i = 0; i < newStrings.length; i++)
				 System.out.print(newStrings[i] + " ");
		  
		  input.close();
				

	}
	

	
	

}


