package ce1002.a11.s995002046;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

class A11{
	JFrame myf;
	JLabel l1;
	JLabel l2;
	JTextField f;
	JButton b;
	JTextArea t;
	A11(){
		//new components I need
		myf=new JFrame("A11-995002046");
		l1=new JLabel("Key: ");
		l2=new JLabel("Context: ");
		f=new JTextField();
		b= new JButton("Decrypt");
		t=new JTextArea();
		//set frame
		myf.setLayout(null);
		myf.setSize(500, 500);
		myf.setVisible(true);
		myf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		//add components and set their location
		myf.add(l1);
		l1.setBounds(50, 10, 50, 30);
		myf.add(f);
		f.setBounds(110, 10, 150, 30);
		myf.add(b);
		b.setBounds(260, 10, 80, 30);
		myf.add(l2);
		l2.setBounds(50, 50, 50, 30);
		myf.add(t);
		t.setBounds(110, 50, 230, 150);
		//add action listener
		b.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				t.setText("");//set text empty
				if(f.getText().contentEquals("")){//text field is empty then show dialog
					JOptionPane.showMessageDialog(myf, "Can��t be empty!");
				}
				else if(Integer.parseInt(f.getText())>=1 && Integer.parseInt(f.getText())<=9){
				//right range to decode
					Decrypt(Integer.parseInt(f.getText()));
				}
				else{
				//text field is out of range then show dialog
					JOptionPane.showMessageDialog(myf, "Out Of Range!");
				}
			}
			
		});
		
	}
	public void Decrypt(int key) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader("A11.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
			    //decode the file
				for(int i=0;i<line.length();i++){
					char c =(char) (line.charAt(i)-key);
					t.setText(t.getText()+c);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		new A11();
	}
}