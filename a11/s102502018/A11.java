package ce1002.a11.s102502018;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.*;



public class A11 {

	public static void main(String[] args) {
		
		Frame();
	}	
	
	
	public static void Frame()
	{
		JFrame frame = new JFrame();
		frame.setSize(260, 120);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setTitle("A11-102502018");
		JPanel panel = new JPanel();
		panel.setLayout(null);
		JLabel label = new JLabel();
		final JLabel label5 = new JLabel();
		final JLabel label1 = new JLabel();
		final JTextField tf = new JTextField();
		JButton but = new JButton();
		label.setText("Key:");
		label.setBounds(10,10,50,30);
		label1.setText("Result?");
		label1.setBounds(100,40,70, 30);
		but.setText("Decrypt");
		but.setBounds(150,10,80, 30);
		tf.setBounds(45,10,100, 30);
		panel.add(label);
		panel.add(tf);
		panel.add(but);
		panel.add(label1);
		but.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e)
			{
				
				String txt = null;	//儲存文件中的字串
				String result;		//解密後的字串
				char[] txt1;		//儲存文件內容
				char[] key;			
				int a=0,b=0;
				int asc=0;
				try {
					FileReader fr = new FileReader("A11.txt");
					BufferedReader br = new BufferedReader(fr);	//讀取文件內容
					try {
						txt=br.readLine();						//將文件內容存到txt字串
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				txt1 = txt.toCharArray();						//將字串各字元存入char陣列
				key = tf.getText().toCharArray();				//將輸入內容存入char陣列
				asc = (int) key[0];								//將char轉換成int形態
				asc = asc-48;									//ASCII 0的代碼為48
				if(asc<1||asc>9||tf.getText().length()>1)		//判斷輸入的數字為1到9
				{
					JPanel panel1 = new JPanel();
				    panel1.setMinimumSize(new Dimension(200,200));
				    JFrame frame = new JFrame();
				    label5.setText("Out of range!");
				    panel1.add(label5);
				    JOptionPane.showMessageDialog(frame, panel1);
				}
				else
				{
				while(a<txt.length()){
				txt1[a] =(char) (txt1[a]-asc);		
				a++;
				}					//存儲解密後的字串
				result = String.valueOf(txt1);		//將字串存入結果			
				label1.setText(result);
				}
			}
		});
		frame.add(label1);
		frame.add(panel);
		
		
		
	}
}
