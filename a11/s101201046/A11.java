package ce1002.a11.s101201046;

import javax.swing.*;

public class A11 extends JFrame {

	A11() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.setContentPane(new MyPanel());//set content pane
		this.setTitle("A11-101201046"); // set title
		this.setVisible(true);
		
		this.pack();
	}

	public static void main(String[] args) {
		new A11();//create a frame	
	}
}
