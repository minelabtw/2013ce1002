package ce1002.a11.s101201046;

import java.awt.*;
import java.awt.event.*;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.text.*;

import java.io.*;

public class MyPanel extends JPanel {

	private JLabel key;
	private JFormattedTextField ft;
	private JButton dec;
	private JLabel ans;

	MyPanel() {
		GridBagConstraints c = new GridBagConstraints();

		this.setLayout(new GridBagLayout());

		c.weightx = 0;
		c.weighty = 0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.FIRST_LINE_START;

		this.add(new JLabel("Key:"), c);

		c.gridx = 1;
		c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.PAGE_START;
		
		try {
			ft = new JFormattedTextField(new MaskFormatter("#")); //make sure input is number
			ft.setFocusLostBehavior(JFormattedTextField.COMMIT); //for the sisuation imput is empty
			ft.setColumns(10);
		} catch (ParseException e){
			e.printStackTrace();
		}

		this.add(ft, c);

		c.gridx = 2;
		c.weightx = 0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.FIRST_LINE_END;

		dec = new JButton("Decrypt"); //decrypt button
		dec.addActionListener(new Decrypting());

		this.add(dec, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 3;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.fill = GridBagConstraints.NONE;
		c.anchor = GridBagConstraints.PAGE_START;

		ans = new JLabel("Result?"); //result label

		this.add(ans, c);
	}

	private class Decrypting implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int key;
			if (ft.getText().equals(" ")) 
				showErrorMsg("Can't be empty!!");
			else if (ft.getText().equals("0")) //since restrict the input to one digit
				showErrorMsg("Out Of Range!"); 
			else {
				key = Integer.valueOf(ft.getText()); //get key code
				try {
					FileInputStream fi = new FileInputStream("A11.txt"); //open encrypt file
					ByteArrayOutputStream op = new ByteArrayOutputStream();
					
					while (fi.available() > 0)
						op.write(fi.read() - key);

					ans.setText(op.toString("US-ASCII")); //trans to ascii code
					pack(); // resize jframe
					fi.close(); //close file
				} catch(FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}		
		}

		private void showErrorMsg(String msg) {
			Container c =  MyPanel.this.getTopLevelAncestor(); //get the frame
			if (c != null)
				JOptionPane.showMessageDialog(c, msg);
			else
				JOptionPane.showMessageDialog(MyPanel.this, msg);
		}
	}	

	private void pack() {
		JFrame c =  (JFrame)this.getTopLevelAncestor(); //get the frame
		if (c != null) 
			c.pack();
	}
}
