package ce1002.a11.s102502509;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;

import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel implements ActionListener
{
	private JButton bt = new JButton("Decrypt");
	private JTextField tf = new JTextField();
	private JLabel l1 = new JLabel();
	private JLabel l2 = new JLabel();
	
	MyPanel()
	{
		// traditional setting 
		setLayout(null);
		setBounds(0, 0, 300, 110);
		
		l1.setBounds(5, 5, 35, 30); //key
		l1.setText("Key:");
		
		tf.setBounds(45, 5, 130, 30);
		tf.setText(null);
		
		bt.setBounds(200, 5, 80, 30);
		
		l2.setBounds(0, 60, 300, 30);
		l2.setText("Result?");
		l2.setHorizontalAlignment(JLabel.CENTER);
		
		add(tf);
		add(bt);
		add(l2);
		add(l1);
		
		bt.addActionListener(this);
	}

		public void actionPerformed(ActionEvent e) // must have to create the event action
		{
			try // exception method
			{
				
				check(tf.getText());

				FileReader fr = new FileReader("A11.txt");
				BufferedReader output = new BufferedReader(fr);
				
				/*char password[] = output.readLine().toCharArray();
				char answer[] = new char [password.length/2];
				int j = 0;
				
				for (int i = 1 ; i < password.length ; i += 2){
					answer[j] = (char)((int)password[i] - Integer.parseInt(tf.getText()));
				}
			String str = new String(password);
			l2.setText(str);
			}*/// the solution of full type of the number
				char password[] = output.readLine().toCharArray();
				for (int i = 0 ; i < password.length ; i++)
				{
					password[i] = (char)((int)password[i] - Integer.parseInt(tf.getText()));
				}
			String str = new String(password);
			l2.setText(str);
			output.close();
			}
			catch(Exception excep) // encounter the empty situation
			{
				JOptionPane.showMessageDialog(tf, excep.getMessage(), "訊息",
	                      JOptionPane.INFORMATION_MESSAGE);
			}
			
		}
		public boolean check (String file) throws Exception
		{
			// check method; check the file's name is empty or not
			if(tf.getText().isEmpty())
			{
				throw new Exception("Can't be empty!!"); // false = empty => exception
			}
			else if(Integer.parseInt(tf.getText()) >= 10 || Integer.parseInt(tf.getText()) <= 0)
			{
				throw new Exception("Out of range!!"); 
			}else
			{
				return true; // true = exist => do the "try" thing
			}
		}
		
		
		
}
