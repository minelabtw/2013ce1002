package ce1002.a11.s102502043;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class Frame extends JFrame
{
	JButton jbt = new JButton("Decrypt");
	JTextField message=new JTextField();
	JLabel labk = new JLabel("Key:");
	JPanel panel = new JPanel();
	JLabel result = new JLabel("Result?");
	int convertmessage = 0;
	Frame()
	{
		setTitle("A11-102502043");
		panel.setLayout(null);
		setBounds(0,0,300,150);
		jbt.setBounds(200,23,80,20);
		panel.add(jbt);
		panel.add(message);
		message.setBounds(45,23,130,20);
		panel.add(labk);
		panel.add(result);
		result.setBounds(5,70,60,50);
		labk.setBounds(5,7,60,50);
		jbt.addActionListener(new jbtaction());
		add(panel);
		setVisible(true);
	}
	class jbtaction implements ActionListener
	{
		//等號右邊先將JTEXTFIELD轉成STRING再轉成INT
		public void actionPerformed(ActionEvent e)
		{
			
			if(message.getText().isEmpty())
			{
				showempty();
			} 
			else
			{
				convertmessage =  Integer.valueOf(message.getText());
				if(convertmessage>9||convertmessage<1)
				{
					outofrange();
				}
				else
				{
					
				}
			}
		}
		public void showempty()
		{
			JOptionPane.showMessageDialog(null, "Can't be empty!!","A11",JOptionPane.ERROR_MESSAGE);
		}
		public void outofrange()
		{
			JOptionPane.showMessageDialog(null,"Out of range!","A11",JOptionPane.ERROR_MESSAGE);
		}
	}
}
