package ce1002.a11.s101201023;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame
{
	JTextField field = new JTextField();
	JButton button = new JButton("Decrypt");
	JLabel label1 = new JLabel("Key:");
	JLabel label2 = new JLabel("Result?");
	
	//set frame
	public MyFrame()
	{
		setSize(300 , 150);
		setLayout(null);
		setTitle("A11-101201023");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		setVisible(true);
		
		add(field);
		add(button);
		add(label1);
		add(label2);
		
		field.setBounds(30,10,125,30);
		button.setBounds(155,10,125,30);
		label1.setBounds(0,0,500,50);
		label2.setBounds(120,50,100,50);
		
		button.addActionListener(new Button());    //inspect exception
	}
	
	//inspect exception
	public class Button implements ActionListener
	{
		public void actionPerformed(ActionEvent a)
		{
			try
			{	
     			//inspect if field is empty
				if(field.getText().equals(""))
			    {
			    	throw new NullPointerException();
			    }
				
				//inspect if field is out of range
				else if(!(field.getText().equals("1") || field.getText().equals("2") || field.getText().equals("3") || field.getText().equals("4") || field.getText().equals("5") || field.getText().equals("6") || field.getText().equals("7") || field.getText().equals("8") || field.getText().equals("9")))
				{
					throw new Exception();
				}
				
				//Decrypt
				else
				{
					FileReader file = new FileReader("A11.txt");
					Scanner input = new Scanner(file);
					int key = Integer.parseInt(field.getText());
					
					label2.setText("");
					while (input.hasNext())
					{
						String data = input.next();
						for (int i = 0; i < data.length() ; i++)
						{
							label2.setText(label2.getText()+Decrypt(data.charAt(i),key));
						}
					}
				}
			}
			
			catch(NullPointerException e)
			{
				JOptionPane.showMessageDialog(new JFrame("�T��"), "Can��t be empty!!");
			}
			
			catch(Exception g)
			{	
	    		JOptionPane.showMessageDialog(new JFrame("�T��"), "Out Of Range!");
			}
		}
	}
	public char Decrypt(char word, int key)   //Decrypt
	{
		int number = (int)word;
		number = number - key;
		return (char)number;
	}
}
