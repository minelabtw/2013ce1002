package ce1002.a11.s102502042;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
public class MyPanel extends JPanel{
	JTextField text = new JTextField();
	JButton button = new JButton("Decrypt");
	JLabel key = new JLabel("Key:");
	JLabel result = new JLabel("Result?");
	JFrame frame = new JFrame();
	//constructor
	MyPanel()
	{
		setLayout(null);
		setSize(400,300);
		//key
		key.setLocation(10,10);
		key.setSize(30, 30);
		add(key);
		//text
		text.setLocation(50,10);
		text.setSize(150,30);
		add(text);
		//button
		button.setLocation(210,10);
		button.setSize(100,30);
		add(button);
		button.addActionListener(new Click());
		//result
		result.setLocation(80,60);
		result.setSize(80,60);
		add(result);
	}
	
	class Click implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			//action
			if(text.getText().isEmpty())	//empty condition
			{
				JOptionPane.showMessageDialog(null, "Can't be empty!","�T��",JOptionPane.ERROR_MESSAGE);
			}
			else if(Integer.parseInt(text.getText())<1||Integer.parseInt(text.getText())>9)	//out of range condition
			{
				JOptionPane.showMessageDialog(null, "Out of range!","�T��",JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				//read file + ascii
				try
				{
					Scanner read = new Scanner(new File("A11.txt"));
					char[] s;
					s = read.nextLine().toCharArray();
					for ( int i = 0 ; i < s.length ; i++ )
					{
						s[i] -= Integer.parseInt(text.getText());
					}
					String over = "";
					for ( int i = 0 ; i < s.length ; i++ )
					{
						over += s[i];
					}
					read.close();
					result.setText(over);
				}
				catch(IOException gg)
				{
					System.out.println("OH~NO!!");
				}
			}
		}
	}
}
