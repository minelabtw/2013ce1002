package ce1002.a11.s102502022;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class myPanel extends JPanel{
	JTextField jtf = new JTextField();
	JLabel jta = new JLabel("Result?");//new result的字串
	String title;
	String content;
	
	myPanel(){
		setBorder(new LineBorder(Color.BLACK,5));//加粗細為5的框線
		setLayout(null);
		setBounds(0,0,350,400);//設置大小
	}
	
	public void Button(){
		JButton button = new JButton("Decrypt");//加入上面寫字的按鈕
		button.setBounds(20,20,100,30);//按鈕大小
		button.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				title = jtf.getText();
				if(jtf.getText().isEmpty()){//如果是空的
					JOptionPane.showMessageDialog(null, "Can't be empty!!");
				}
				else if(Integer.valueOf(title).intValue()<1 || Integer.valueOf(title).intValue()>9){//如果超出值的限制
					JOptionPane.showMessageDialog(null, "Out of Range!");
				}
				else{
					try{
						DataInputStream input = new DataInputStream(new FileInputStream("A11.text"));//讀入A11的檔
						String str = "";
						
						while(input.available()!=0){
							byte b = input.readByte();
							int key = Integer.valueOf(title).intValue();
							b = (byte)(b-key);
							
							str = str + (char)(b);
						}
						jta.setText(str);
						input.close();
					}
					catch(FileNotFoundException e1){
						e1.printStackTrace();
					}
					catch(IOException e2){
						e2.printStackTrace();
					}
				}
			}
		});
		add(button);
	}
	
	public void textfield()
	{
		jtf.setBounds(150,20,150,30);
		setVisible(true);
		add(jtf);
	}
	
	public void textarea(){
		jta.setBounds(120,70,220,300);
		add(jta);
	}
       

}
