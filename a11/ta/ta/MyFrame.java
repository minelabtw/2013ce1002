package ce1002.a11.ta;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JButton jbtDecrypt = new JButton("Decrypt");
	private JTextField jtfKey = new JTextField(10); 
	private JLabel jlabel = new JLabel("Result?");
	
	MyFrame() {
		JPanel pane1 = new JPanel();
		pane1.add(new JLabel("Key:"));
		pane1.add(jtfKey);
		pane1.add(jbtDecrypt);
		
		JPanel pane2 = new JPanel();
		pane2.add(jlabel);
			
		add(pane1,BorderLayout.NORTH);
		add(pane2,BorderLayout.CENTER);
		
		//Register listener with button
		jbtDecrypt.addActionListener(new ButtonListener(this));
		
	}
	// handle save action
	private class ButtonListener implements ActionListener{
		JFrame frame;
		ButtonListener(JFrame frame){
			this.frame = frame;
		}

		// invoke when button be clicked
		@Override
		public void actionPerformed(ActionEvent e) {
			if (jtfKey.getText().isEmpty())	// Key and content must be non-empty
				showEmptyMsg();
			else if(Integer.valueOf(jtfKey.getText())<1 || Integer.valueOf(jtfKey.getText())>9) //Out of range
				showOutOfRange();
			else {
				String fileName = "A11.txt";
				try {
					
					DataInputStream input = new DataInputStream(new FileInputStream(fileName));
					int value;
					String str="";

					//readInput
					while(input.available()>0){ 
						byte c = input.readByte();
						int key = Integer.valueOf(jtfKey.getText());
						
						//Decrypt
						c = (byte) (c - key);
						
						str+=(char) c;
					}
					jlabel.setText(str);
					input.close();
					
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	
			}
		}

		// dialog
		private void showEmptyMsg() {
			JOptionPane.showMessageDialog(frame, "Can't be empty!");
		}
		// dialog
		private void showOutOfRange() {
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
		}
	}
	

}
