package ce1002.a11.ta_3;

import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JButton jbtEncrypt = new JButton("Encrypt");
	private JTextField jtfKey = new JTextField(10); 
	private JTextArea jtaContent = new JTextArea();
	
	MyFrame() {
		JPanel pane1 = new JPanel();
		pane1.add(new JLabel("Key:"));
		pane1.add(jtfKey);
		pane1.add(jbtEncrypt);
		
		JPanel pane2 = new JPanel();
		pane2.add(new JLabel("Content:"));
		jtaContent.setRows(5);
		jtaContent.setColumns(20);
		pane2.add(jtaContent);
			
		add(pane1,BorderLayout.NORTH);
		add(pane2,BorderLayout.CENTER);
		
		jbtEncrypt.addActionListener(new ButtonListener(this));
		
	}
	private class ButtonListener implements ActionListener{
		JFrame frame;
		ButtonListener(JFrame frame){
			this.frame = frame;
		}

		// invoke when button be clicked
		@Override
		public void actionPerformed(ActionEvent e) {
			if (jtfKey.getText().isEmpty() || jtaContent.getText().isEmpty())	// key and content must be non-empty
				showEmptyMsg();
			else if(Integer.valueOf(jtfKey.getText())<1 || Integer.valueOf(jtfKey.getText())>9)
				showOutOfRange();
			else {
				String fileName = "A11.txt";
				try {

					DataOutputStream output = new DataOutputStream(new FileOutputStream(fileName));
					int contentSize = jtaContent.getText().length();
					for (int i=0;i<contentSize;i++)
					{
						char c = jtaContent.getText().charAt(i);
						int key = Integer.valueOf(jtfKey.getText());

						output.writeByte(c + key);
						output.flush();
					}
					output.close();
					
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		}

		// dialog
		private void showEmptyMsg() {
			JOptionPane.showMessageDialog(frame, "Can't be empty!");
		}
		// dialog
		private void showOutOfRange() {
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
		}
	}
	

}
