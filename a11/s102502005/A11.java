package ce1002.a11.s102502005;

import java.awt.BorderLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.print.DocFlavor.INPUT_STREAM;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class A11 extends JFrame implements ActionListener {

	JLabel keyLabel;
	TextField keyField;
	JButton decryptButton;
	JLabel resultLabel;
	String result = "                                  Result?";

	public A11() {
		this.setLayout(null);
		this.setSize(250, 100);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		keyLabel = new JLabel("Key:");
		keyLabel.setBounds(5, 5, 30, 20);
		this.add(keyLabel);
		keyField = new TextField();
		keyField.setBounds(35, 5, 110, 20);
		this.add(keyField);

		decryptButton = new JButton("Decrypt");
		decryptButton.setBounds(150, 5, 80, 20);
		decryptButton.addActionListener(this);
		this.add(decryptButton);

		resultLabel = new JLabel(result);
		resultLabel.setBounds(0, 30, 250, 30);
		resultLabel.setHorizontalTextPosition(JLabel.CENTER);
		this.add(resultLabel);
	}

	public static void main(String[] args) {
		A11 frame = new A11();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// 輸入不合規定時。
		if (keyField.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "Can’t be empty!!");
			return;
		}

		try {
			int key = Integer.parseInt(keyField.getText());
			if (key > 9 || key < 1) {
				JOptionPane.showMessageDialog(this, "Out Of Range!");
				return;
			}

			DataInputStream input = new DataInputStream(new FileInputStream(//把一個一個字元讀進來，然後馬上處理。
					"A11.txt"));
			result = "";

			while (input.available() > 0) {
				char temp = input.readChar();
				temp = (char) ((int)temp - key);//分解動作為:先把temp轉乘int和key相減，再把結果轉成char。
				result += temp;
			}

		} catch (java.lang.NumberFormatException ex) {
			JOptionPane.showMessageDialog(this, "Please fill in an Integer!");
			return;
		} catch (FileNotFoundException e1) {
			JOptionPane.showMessageDialog(this, "File not found!");
		} catch (Exception e2) {
			JOptionPane
					.showMessageDialog(
							this,
							"Exception occured, please make sure your input is an integer "
									+ "between 1 to 9 and encrypted file is in the same directory as this program!");
			return;
		}
		resultLabel.setText(result);
		repaint();

	}
}
