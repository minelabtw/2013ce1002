package ce1002.a11.s102502501;
import javax.swing.JFrame;

import java.io.File;
import java.io.FileNotFoundException; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JOptionPane;
public class MyFrame extends JFrame{
	
	MyFrame() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new Decode() ) ;
	}
	class Decode extends JPanel implements ActionListener
	{
		
		private JLabel key = new JLabel("Key:") ;
		private JLabel result = new JLabel("Result?") ;
		private JButton button = new JButton("Decrypt") ;
		private JTextField textfield = new JTextField(15);

		
		Decode() {
			
			setLayout(null) ;
			setSize(300,100) ;
			
			textfield.setBounds(40,10,100,20); //set key
			add(textfield) ;
			
			key.setBounds(5,5,30,30); 
			add(key) ;
			
			result.setBounds(80,50,50,30);
			add(result) ;
			
			button.setBounds(150,5,80,30) ;
			button.addActionListener((ActionListener)this) ;
			add(button) ;
			
		}

	}
}
