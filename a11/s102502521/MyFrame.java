package ce1002.a11.s102502521;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	private JButton btn = new JButton("Decrypt");
	private JTextField jtf = new JTextField(10);
	private JLabel jl = new JLabel("Result?");

	MyFrame() {		
		JPanel p1 = new JPanel();
		p1.setLayout(new FlowLayout());
		p1.add(new JLabel("Key:"));
		p1.add(this.jtf);
		p1.add(this.btn);
		p1.add(this.jl);

		//為按鈕加入Listener
		this.btn.addActionListener(new ButtonListener(this));
	
		//將p1加入frame
		add(p1);
	}

	private class ButtonListener implements ActionListener {
		JFrame frame;

		ButtonListener(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent e) {
			//當TextField為空
			if (MyFrame.this.jtf.getText().isEmpty()) {
				JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
			}
			
			
			//當TextField值合理
			else if ((Integer.valueOf(MyFrame.this.jtf.getText()).intValue() < 10) && (Integer.valueOf(MyFrame.this.jtf.getText()).intValue() > 0)) {	
				try {
					String txt = "";			
					DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));		
					
					while (input.available() > 0) {
						byte b = input.readByte();
						int key = Integer.valueOf(MyFrame.this.jtf.getText())
								.intValue();

						//解碼
						b = (byte) (b - key);

						//將密碼存入txt
						txt = txt + (char) b;
					}
					MyFrame.this.jl.setText(txt);
					input.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (NumberFormatException e2) {
					e2.printStackTrace();
				} catch (IOException e3) {
					e3.printStackTrace();
				}
			} 
			
			//當TextField值合理
			else {
				JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
			}
		}
	}
}
