package ce1002.a11.s102503017;

import javax.swing.*;

import java.awt.event.*;
import java.awt.Font;
import java.io.*;

public class Frame extends JFrame{
	
	JPanel p = new JPanel();
	JTextField textfd = new JTextField();
	JLabel key = new JLabel();
	JLabel result = new JLabel();
	JButton button = new JButton();
	
	Frame()
	{
		//setting of frame.
		setTitle("A11 - 102503017");
		getContentPane().setLayout(null);
		setBounds(0,0,400,300);
		setVisible(true);
		
		//setting of panel.
		p.setLayout(null);
		p.setBounds(0,0,400,300);
		
		//setting of textfield.
		textfd.setBounds(61,6,204,40);
		p.add(textfd);
		
		//setting of the label on the left of textfield.
		key.setFont(new Font("新細明體", Font.BOLD, 15));
		key.setHorizontalAlignment(SwingConstants.CENTER);
		key.setBounds(14,0,50,50);
		key.setText("Key:");
		p.add(key);
		
		//setting of the result label.
		result.setFont(new Font("新細明體", Font.PLAIN, 18));
		result.setHorizontalAlignment(SwingConstants.CENTER);
		result.setBounds(0,70,386,156);
		result.setText("Result");
		p.add(result);
		
		//overriding actionPerformed method of ActionListener.
		ActionListener listener = new ActionListener(){
											public void actionPerformed(ActionEvent evt)
											{
												//grab the text in the textfield.
												String input;
												input = textfd.getText();
												//if textfield is empty, a pop-up message triggered. 
												if(input.equals(""))
												{
													JLabel errmsg = new JLabel("Decrypt key cannot be empty!");
													JOptionPane.showMessageDialog(null, errmsg, "Message", JOptionPane.INFORMATION_MESSAGE);
												}
												
												//change input into integer.
												int inputInInt = Integer.parseInt(input.toString());
												
												//determination of input, if not required, pop-up message triggered.
												if(inputInInt < 1 || inputInInt > 9)
												{
													JLabel errmsg = new JLabel("Out of range!");
													JOptionPane.showMessageDialog(null, errmsg, "Message", JOptionPane.INFORMATION_MESSAGE);
												}
												//decrypt part.
												else
												{
													//create a File object with opening A11.txt and a FileInputStream object with A11.txt.
													File findingLength = new File("A11.txt");
													FileInputStream encryptedFile = null;
													try {
														encryptedFile = new FileInputStream("A11.txt");
													} catch (FileNotFoundException e1) {
														e1.printStackTrace();
													}
													
													//File object is for find the length of A11.txt.
													int length = (int) findingLength.length();
													
													//create an character array to put the result into it.
													char[] decryptedCode = new char[length];
													
													int readchar, counter = 0;
													//read the A11.txt and minus the decrypt-key to decrypt the encrypted file A11.txt.Then put the 
													//decrypted character into the character array until all encrypted character is decrypted.
													try {
														while((readchar = encryptedFile.read()) != -1)
														{

															readchar -= inputInInt;
															decryptedCode[counter] = (char) readchar;
															counter++;
														}
													} catch (IOException e) {
														e.printStackTrace();
													}
													
													//put all characters together into a String object and use it to change the text in result label.
													String resultString = "";
													for(char c : decryptedCode)
														resultString += c;
													result.setText(resultString);
												}//end else.
											}//end method - actionPerformed.
									};//end initializing ActionListener with overriding.
									
		//setting of button.
	    button.setFont(new Font("新細明體", Font.BOLD, 15));
		button.setBounds(269,4,92,40);
		button.setText("Decrypt");
		button.addActionListener(listener);
	    p.add(button);
		
	    //add panel.
		getContentPane().add(p);		
		
		
	}

}
