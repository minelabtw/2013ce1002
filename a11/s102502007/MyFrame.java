package ce1002.a11.s102502007;

import javax.swing.*;

import java.awt.BorderLayout;
import java.io.*;
import java.nio.charset.Charset;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFrame extends JFrame {
	
	  private JButton decrypt;
	  private JTextField field;
	  private JLabel result;
	  private JLabel key;
	  
	  MyFrame()
	  {
		/*
		 * frame attributes
		 */
		super("A11-102502007");
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setSize(350,150);
		
		//Decrypt button
		decrypt = new JButton("Decrypt");
		decrypt.setSize(100,30);
		
		//textfield
		field = new JTextField(15);
		
		//key label
		key = new JLabel("Key:");
		key.setSize(30,10);
		
		//result label
		result = new JLabel("Result?"); 
		result.setSize(50, 30);
		
		//put label, textfield and button horizontally
		JPanel p1 = new JPanel();
		p1.setLayout(new FlowLayout());
		p1.setBounds(0, 0, 350, 75);
	    p1.add(key);
	    p1.add(field);
	    p1.add(decrypt);
	    
	    JPanel p2 = new JPanel();
	    p2.setBounds(0, 75, 350, 75);
	    p2.setLayout(new FlowLayout());
	    p2.add(result);
	    
	    
	    add(p1, BorderLayout.NORTH);
	    add(p2, BorderLayout.CENTER);
	    //set the listener
	    decrypt.addActionListener(new Listener());
	  } 
	  private class Listener implements ActionListener {
			public void actionPerformed (ActionEvent e) {
				result.setText(check(field.getText())); 
				//reset the result label with the content decrypted
			}
		}
	  private String check(String code) {		
			try 
			{
				if(code.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Can't be empty!", "Error", JOptionPane.ERROR_MESSAGE);
					return "Result?";
				}
				//the text field is empty
				
				int key = Integer.parseInt(code);
				//turn the type from string to integer
				
				if(key<1 || key>9) 
				{
					JOptionPane.showMessageDialog(null, "Out Of Range!", "Error", JOptionPane.ERROR_MESSAGE);
					return "Result?";
				}
				//check the key is on the legal range
				
				DataInputStream input = new DataInputStream(new FileInputStream("A11.txt"));
				int length = input.available();
				//the length of the content
				byte[] content = new byte[length];
				//the decrypted result
				for(int i=0; i<length; i++)
				{
					content[i] = input.readByte();
					//read
					content[i] = (byte)(content[i] - key);
					//decrypt and store
				}
				input.close();
				return new String(content, Charset.defaultCharset());
			}
			catch(Exception e) 
			{
				e.printStackTrace();
				return "Result?";
			}
			finally
			{
				
			}
		}

	}

