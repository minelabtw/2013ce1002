package ce1002.a11.s102502555;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	Panel Panel = new Panel();
	
	//基本設定
	public MyFrame() {
		super("A11-102502555");
		setLayout(null);
		setSize(300, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		add(Panel);
	}
}
