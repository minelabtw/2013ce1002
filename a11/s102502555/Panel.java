package ce1002.a11.s102502555;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Panel extends JPanel implements ActionListener
{
	private JButton bt = new JButton("Decrypt");
	private JTextField tf = new JTextField();
	private JLabel key = new JLabel();
	private JLabel result = new JLabel();
	
	Panel()
	{
		//基本設定
		setLayout(null);
		setBounds(0, 0, 300, 110);
		
		key.setBounds(5, 5, 35, 30); //key
		key.setText("Key:");
		
		tf.setBounds(45, 5, 130, 30);
		tf.setText(null);
		
		bt.setBounds(200, 5, 80, 30);
		
		result.setBounds(0, 60, 300, 30);
		result.setText("Result?");
		result.setHorizontalAlignment(JLabel.CENTER);
		
		add(tf);
		add(bt);
		add(result);
		add(key);
		
		bt.addActionListener(this);
	}

		public void actionPerformed(ActionEvent e) 
		{
			try //例外處理
			{
				
				check(tf.getText());

				FileReader fr = new FileReader("A11.txt");
				BufferedReader output = new BufferedReader(fr);
				
				char password[] = output.readLine().toCharArray();
				for (int i = 0 ; i < password.length ; i++)
				{
					password[i] = (char)((int)password[i] - Integer.parseInt(tf.getText()));
				}
			String str = new String(password);
			result.setText(str);
			output.close();
			}
			catch(Exception excep) //處理空白
			{
				JOptionPane.showMessageDialog(tf, excep.getMessage(), "訊息",
	                      JOptionPane.INFORMATION_MESSAGE);
			}
			
		}
		public boolean check (String file) throws Exception
		{
			// 檢查檔名
			if(tf.getText().isEmpty())
			{
				throw new Exception("Can't be empty!!"); 
			}
			else if(Integer.parseInt(tf.getText()) >= 10 || Integer.parseInt(tf.getText()) <= 0)
			{
				throw new Exception("Out of range!!"); 
			}else
			{
				return true; 
			}
		}
		
		
		
}
