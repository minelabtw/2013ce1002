package ce1002.a11.s102502039;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFrame extends JFrame
{
private JButton jbtDecrypt = new JButton("Decrypt");//declare variables
private JTextField jtfKey = new JTextField(1１);
private JLabel Result = new JLabel("Result?");

MyFrame()//implement
{
  JPanel pane1 = new JPanel();
  pane1.add(new JLabel("Key:"));
  pane1.add(this.jtfKey);
  pane1.add(this.jbtDecrypt);
  JPanel pane2 = new JPanel();
  pane2.add(this.Result);
  add(pane1, "North");
  add(pane2, "Center");
  this.jbtDecrypt.addActionListener(new DecryptButtonListener(this));
}

private class DecryptButtonListener implements ActionListener
{
  JFrame frame;
  
  DecryptButtonListener(JFrame frame)
  {
    this.frame = frame;
  }
  
  public void actionPerformed(ActionEvent e)
  {
    if (MyFrame.this.jtfKey.getText().isEmpty())
    {
      showEmptyMsg();
    }
    else if ((Integer.valueOf(MyFrame.this.jtfKey.getText()).intValue() < 1) || (Integer.valueOf(MyFrame.this.jtfKey.getText()).intValue() > 9))
    {
      showOutOfRange();
    }
    else
    {
      String fileName = "A11.txt";
      try
      {
        DataInputStream input = new DataInputStream(new FileInputStream(fileName));
        
        String str = "";
        while (input.available() > 0)
        {
          byte c = input.readByte();
          int key = Integer.valueOf(MyFrame.this.jtfKey.getText()).intValue();
          c = (byte)(c - key);
          str = str + (char)c;
        }
        MyFrame.this.Result.setText(str);
        input.close();
      }
      catch (FileNotFoundException e1)
      {
        e1.printStackTrace();
      }
      catch (NumberFormatException e1)
      {
        e1.printStackTrace();
      }
      catch (IOException e1)
      {
        e1.printStackTrace();
      }
    }
  }
  
  private void showEmptyMsg()//dialog
  {
    JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
  }
  
  private void showOutOfRange()
  {
    JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
  }
}
}
