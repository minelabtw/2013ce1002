package ce1002.a11.s102502512;
import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.swing.event.*;
import javax.swing.event.*;

import java.util.*;


public class Decode extends JFrame{
	private JButton save = new JButton("Decrypt");
	private JTextField title = new JTextField(20);
	private JLabel key = new JLabel("  Key:");
	private JLabel answer = new JLabel("Result?");
	private String warring ;
	private String message;
	private int k=0;
	
	Decode()
	{	
		setLayout(null);								//Line 21-29: 排版面
		save.setBounds(250,10,100,30);
		this.add(save);
		title.setBounds(50,10,200,30);
		this.add(title);
		key.setBounds(2, 10, 100, 30);
		this.add(key);
		answer.setBounds(150, 70, 300, 30);
		this.add(answer);
		
		save.addActionListener(new hello());
	}
	
	public void MessageBox(String message)				//Line 32-41: 設一個會跳出
	{	
		this.message = message;
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(200, 200));
		JFrame frame = new JFrame("訊息");
		JLabel label = new JLabel(message);
		panel.add(label);
		JOptionPane.showMessageDialog(frame, panel);
	  }
	
	class hello implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e)		
		{
			try
			{				
				if(title.getText().isEmpty())			
				{
					throw new Exception("Can't be empty!");
				}
				k = new Integer(title.getText()).intValue();
				if(k<1 || k>9)
				{
					throw new Exception("Out of range!");
				}
				File file = new File("A11.txt");       //!!!!!!!!!!!!!!  read file
				Scanner input = new Scanner(file);  
				String s = input.next();	
				String test="";
				for ( int j = 0; j < s.length(); j++ )       //!!!!!! Line 72-76: decode
				{
					char c = s.charAt( j ); 
					test += (char)(c-k); 
					}				
				answer.setText(test);
				input.close();
				}
				catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();					
				warring = e1.getMessage();
				MessageBox(warring);
			}	
		}		
	}//class savelistner's end
}