package ce1002.a11.s102502041;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;
public class MyFrame extends JFrame{
	private JPanel contentPane;
	private JTextField encodeArea;
	private JButton btnEncode;
	private JTextArea mainTextArea;
	private JLabel key,content;
	/**
	 * Create the frame.
	 */
	public MyFrame()
	{
		setTitle("A11-102502041");
		setLayout(null);
		setSize(465, 340);
		contentPane = new JPanel();
		contentPane.setLayout(null);
		contentPane.setBounds(0, 0, 450, 300);
		add(contentPane);
		
		key = new JLabel("Key:");
		content = new JLabel("Content:");
		key.setBounds(35, 40, 25, 20);
		content.setBounds(15, 150, 60, 10);
		
		encodeArea = new JTextField();
		encodeArea.setBounds(70, 40, 200, 20);
		
		btnEncode = new JButton("Encrypt");
		btnEncode.setBounds(290, 40, 100, 20);
		btnEncode.addMouseListener( new ButtonActionListener());
		
		mainTextArea = new JTextArea();
		mainTextArea.setLineWrap(true);
		mainTextArea.setBounds(70, 100, 300, 190);
		
		contentPane.add(key);
		contentPane.add(content);
		contentPane.add(encodeArea);
		contentPane.add(btnEncode);
		contentPane.add(mainTextArea);
		
	}
	// handle encrypt action
	class ButtonActionListener implements MouseListener{
		public void mouseClicked(MouseEvent e)
		{
			String encodeM = encodeArea.getText();
			String mainText = mainTextArea.getText();
			//encodeM checking
			if(encodeM==null)JOptionPane.showMessageDialog(null, "Out of Range!");
			else if(Integer.parseInt(encodeM)<1||Integer.parseInt(encodeM)>9)
				JOptionPane.showMessageDialog(null, "Out of Range!");
			else if(mainText==null)
				JOptionPane.showMessageDialog(null, "Can't be empty!!");
			else
				{
					try{
						PrintWriter out = new PrintWriter("A11-102502041.txt");
						int mainTextLen= mainText.length();
						for(int j=0; j<mainTextLen; j++)
						{
							int afterEncode;
							afterEncode = (int)mainText.charAt(j)+Integer.parseInt(encodeM);
							out.print((char)afterEncode);
						}
						out.close();
					}catch(FileNotFoundException e1){
						e1.printStackTrace();
					}
				}
			
		}
		public void mousePressed(MouseEvent e){}
		public void mouseReleased(MouseEvent e){}
		public void mouseEntered(MouseEvent e){}
		public void mouseExited(MouseEvent e){}
	}
}

