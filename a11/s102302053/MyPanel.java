package ce1002.a11.s102302053;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class MyPanel extends JPanel{
	JTextField key = new JTextField();//key值欄位
	JLabel resultArea = new JLabel();//結果顯示位置
	JButton decryptButton = new JButton();//解碼鈕
	JLabel title = new JLabel("Key:");
	private int keyNumber;
	
	MyPanel(){//加入各項元件 並且執行actionlistner
		setLayout(null);
		setTextField();
		setResultArea();
		setDecryptButton();
		setActionListner();
		title.setBounds(10, 10, 30, 30);
		add(title);
		
	}
	
    private void setResultArea(){//結果顯示的位置
    	resultArea.setText("Result?");
    	resultArea.setBounds(145,60,400,60);
		add(resultArea);
	}
 
	private void setTextField(){//key值輸入欄位
		key.setColumns(100);
		key.setBorder(BorderFactory.createLineBorder(Color.black));
		key.setBounds(40, 10, 150, 30);
		add(key);
		
	}
	
	private void setDecryptButton(){//解碼鈕設置
		decryptButton.setText("Decrypt");
		decryptButton.setBounds(200, 10, 120, 30);
		add(decryptButton);
	}
	private void setActionListner(){//當按下decrypt後 執行decrypt()
		decryptButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				decrypt();
			}
		});
	}
	private void decrypt() {//按下decrypt後執行
		 
		 if (key.getText().equals("")) {//當key欄位是空的的時候 輸出提示訊息
				JFrame decryptfail = new JFrame("訊息");
				JLabel text = new JLabel("Can't be empty!!");
				JOptionPane.showMessageDialog(decryptfail, text);//輸出解碼失敗的提示訊息
			}
		 else {
			 keyNumber = Integer.parseInt(key.getText()); 
			 if (keyNumber >9 || keyNumber < 1) {//當key超出範圍的的時候 輸出提示訊息
					JFrame decryptfail = new JFrame("訊息");
					JLabel text = new JLabel("Out Of Range!");
					JOptionPane.showMessageDialog(decryptfail, text);//輸出解碼失敗的提示訊息
				}
		
			 else if (keyNumber <=9 && keyNumber >= 1){//當key符合條件的 就輸出解碼結果
					String finalResult = null;
					try {
						BufferedReader buf = new BufferedReader(new FileReader("A11.txt"));//讀取要解碼的資料
						BufferedReader counter = new BufferedReader(new FileReader("A11.txt"));//用來計算資料長度
						int data = buf.read();
						int tmp = counter.read();
						int num = 0;
						while(tmp!=-1){//計算資料長度
							tmp = counter.read();
							num++;
						}
						char[] result = new char[num];
						num = 0;
						while(data!=-1){//解碼並儲存
							result[num] =(char)(data - keyNumber);
							data = buf.read();
							num++;
						}
						finalResult = new String(result);
						buf.close();
						counter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				    resultArea.setText("");//顯示結果
				    resultArea.setText(finalResult);
			}	   
		 }
		 
	}

}