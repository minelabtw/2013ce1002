package ce1002.a11.s102502534;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class Frame extends JFrame {
	private JLabel label1 = new JLabel("Key:");
	private JButton button = new JButton("Decrtpt");
	private JTextField textfield = new JTextField(15);
	private JLabel label2 = new JLabel("Result?");

	Frame() {
		setSize(300, 110);
		setTitle("A11-102502534");
		setLayout(new FlowLayout(FlowLayout.CENTER));
		setVisible(true);
		setLocationRelativeTo(null);// 讓視窗置中
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(label1);
		add(textfield);
		add(button);
		add(label2);
		button.addActionListener(new buttonListener());
	}

	private class buttonListener implements ActionListener {
		private int key;

		public void actionPerformed(ActionEvent e) {
			if (textfield.getText().equals(""))
				JOptionPane.showMessageDialog(null, "Can't be empty!!", "訊息",
						JOptionPane.INFORMATION_MESSAGE);
			//設定輸入的key值
			else if (textfield.getText().equals("1")) {
				this.key = 1;
			} else if (textfield.getText().equals("2")) {
				this.key = 2;
			} else if (textfield.getText().equals("3")) {
				this.key = 3;
			} else if (textfield.getText().equals("4")) {
				this.key = 4;
			} else if (textfield.getText().equals("5")) {
				this.key = 5;
			} else if (textfield.getText().equals("6")) {
				this.key = 6;
			} else if (textfield.getText().equals("7")) {
				this.key = 7;
			} else if (textfield.getText().equals("8")) {
				this.key = 8;
			} else if (textfield.getText().equals("9")) {
				this.key = 9;
			} else
				JOptionPane.showConfirmDialog(null, "Out Of Range!", "訊息",
						JOptionPane.CLOSED_OPTION);

			try {
				char data[] = new char[500];
				int num;
				FileReader fr = new FileReader("A11.txt");
				num = fr.read(data);//讀檔跟把字元數存到num
				char word[] = new char[num];
				for (int i = 0; i < num; i++) {
					if (key >= 1 && key <= 9)
						word[i] = (char) (data[i] - key);
				}
				String str = new String(word, 0, num);//將word陣列從0開始到第num個轉成字串型態 
				label2.setText(str);//把result蓋掉顯示顯解碼結果
			} catch (IOException e1) {

				e1.printStackTrace();
			}
		}

	}
}
