package ce1002.a11.s102502550;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;



public class MyFrame extends JFrame{	
	                          
  
    private JTextField keyInput;          						 //key輸入格
    private JButton btn;
 
    
	MyFrame(){
		setTitle("A11-s102502550");                              //設定GUI介面
        setDefaultCloseOperation(3);
        setLayout(new GridLayout(1, 3));
        setBounds(100, 100, 300, 100);
        
        
        keyInput = new JTextField();
        keyInput.setColumns(10);
        btn = new JButton("Decrypt");
        add(new JLabel("Key: "));
        add(keyInput);
        add(btn);
        btn.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String ss, pp = "", result = ""; 
				try {
					Scanner inFile = new Scanner(new FileReader("A11.txt"));  //開啟檔案
					
					while(inFile.hasNextLine()){                              //不斷讀取
						
						 ss = inFile.nextLine();
						 for(char c: ss.toCharArray()){
					                                                          //解密	
							 pp += (char)((int)c - (int)(keyInput.getText().toCharArray()[0]-48));
						 }
						 
						 result += pp;
					}
					  JOptionPane.showMessageDialog(null, result);            //顯示結果
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
			}
        	
        });
       
	}
	
}
