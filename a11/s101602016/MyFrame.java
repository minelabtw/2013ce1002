package ce1002.a11.s101602016;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.net.URL;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JLabel label1=new JLabel("Key: ");//文字1
	private JLabel label2=new JLabel("Result?");//文字2
	private JTextField text=new JTextField();//輸入
	private JButton button =new JButton("Decrypt");//按鍵
	MyFrame(){
		setTitle("A11-101602016");//標題
		setSize(255,105);//視窗大小
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		//文字,按鍵,輸入視窗大小
		label1.setBounds(5,5,45,25);
		label2.setBounds(90,28,45,50);
		text.setBounds(35,6,110,22);
		button.setBounds(150,3,80,28);
		text.setText(null);
		SaveListener savelistener=new SaveListener();//設定listener
		button.addActionListener(savelistener);
		//加入
		add(label1);
		add(label2);
		add(text);
		add(button);
	}
	public class SaveListener implements ActionListener{//listener
		public void actionPerformed(ActionEvent e){
			try{
				if (text.getText().equals("")){//如果沒有輸入文字
					throw new NullPointerException();
				}
				else{//正常情況
					int Ciphertext=Integer.parseInt(text.getText());
					if (0<Ciphertext && Ciphertext<10){//如果key值在1~9之間
						int key=Integer.parseInt(text.getText());//打開文件
						FileReader file =new FileReader("A11.txt");//設定文件檔名
						Scanner input=new Scanner(file);
						label2.setText("");
						while (input.hasNext()){
							String data=input.next();
							for (int i=0;i<data.length();i++){
								label2.setText(label2.getText()+Decrypt(data.charAt(i),key));
							}
						}
					}
					else{//如果key值超出範圍
						throw new Exception();
					}
				}
			}
			catch (NullPointerException e1){//如果沒有輸入文字，則顯示"Can't be empty!!"
				JOptionPane.showMessageDialog(new JFrame("訊息"),"Can't be empty!!");
			}
			catch (Exception e2){//如果超出範圍，則顯示"Out Of Range!"
				JOptionPane.showMessageDialog(new JFrame("訊息"),"Out Of Range!");
			}
		}
		public char Decrypt(char a, int key){//解密函數
			int number=(int)a;//令number等於a的ASCII碼
			number=number-key;//減掉key值
			return (char)number;//回傳該ASCII碼所代表的字元
		}
	}
}
