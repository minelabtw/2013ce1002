package ce1002.a11.s102502037;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	  private JPanel MyPanel;//設定 同時命名按鈕跟結果的地方
	  private JTextField text=new JTextField(10);
	  private JButton btn=new JButton("Decrypt");
	  private JLabel label=new JLabel("Result");
	  
	  public MyFrame()
	  {
	    setTitle("A11-102502037");//set details and add textfield button...
	    setBounds(100, 100, 450, 300);
	    JPanel MyPanel = new JPanel();
	    MyPanel.add(new JLabel("Key"));
	    MyPanel.add(this.text);
	    MyPanel.add(this.btn);
	    JPanel MyPanel2 = new JPanel();
	    MyPanel2.add(this.label);
	    add(MyPanel,"North");
	    add(MyPanel2,"Center");
	    this.btn.addActionListener(new BtnListener(this));
	  }
	  private class BtnListener implements ActionListener//write actionlistener
	  {
		  JFrame Frame;
		  BtnListener(JFrame Frame)
		  {
			  this.Frame=Frame;
		  }
		  public  void actionPerformed(ActionEvent e)
		  {
		      if (MyFrame.this.text.getText().isEmpty())//check if empty or out of range
		      {
		        JOptionPane.showMessageDialog(this.Frame, "Can't be empty!");
		      }
		      else if((Integer.valueOf(MyFrame.this.text.getText()).intValue() < 1) || (Integer.valueOf(MyFrame.this.text.getText()).intValue() > 9))
		      {
		    	  JOptionPane.showMessageDialog(this.Frame, "Out Of Range!");
		      }
		      else 
		      {
		    	  String filename = "A11.txt";
		    	  try//try to decode and print at the label
		    	  {
		    		  DataInputStream input = new DataInputStream(new FileInputStream(filename));
		    		  String str=" ";
		    		  while (input.available() > 0)
		              {
		                byte a = input.readByte();
		                int b = Integer.valueOf(MyFrame.this.text.getText()).intValue();
		                a = (byte)(a-b);
		                str = str + (char)a;
		              }
		              MyFrame.this.label.setText(str);
		              input.close();
		    	  }
		    	  catch (Exception e1)
		    	    {
		    	      e1.printStackTrace();
		    	    }
		      }
		      
		  }

	  }
}

