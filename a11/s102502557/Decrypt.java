package ce1002.a11.s102502557;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Decrypt extends JFrame
{	
	JButton btn_save;
	JLabel label_key;
    JTextField txt_key;
    JTextArea txt_result;
    
    Decrypt()
   	{
    	// basic setup
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	setLocationRelativeTo(null);
    	setLayout(null);
    	setTitle("A11-102502557");
    	setSize(450, 300);
    	// build user interface
    	buildUI();
   	}
    
    private void buildUI()
	{
		// label
    	label_key = new JLabel("KEY:");
    	label_key.setBounds(100, 5 , 355 , 30);
    	add(label_key);
    	// add a button with click event
    	btn_save = new JButton("Decrypt");
    	btn_save.setBounds(5, 5, 90 , 30);
    	btn_save.addActionListener( new DecryptFile());
    	add(btn_save);
    	// key
    	txt_key = new JTextField();
		txt_key.setBounds(135, 5, 305, 30);
		txt_key.setColumns(10);
		add(txt_key);
		// result
		txt_result = new JTextArea();
		txt_result.setBounds(5, 40, 435, 215);
		txt_result.setLineWrap(true);
		add(txt_result);

	}

    class DecryptFile implements ActionListener
	{
		DecryptFile(){}
		@Override
		public void actionPerformed(ActionEvent event)
		{
			// check empty
			if(txt_key.getText().isEmpty())
			{
				JOptionPane.showMessageDialog(null, "Can't be empty","this is Title",JOptionPane.ERROR_MESSAGE);
				return;
			}
			int key = Integer.valueOf(txt_key.getText()); //把 txt_key的字串變成數字 並存入key
			// check in range
			if(key>9||key<1)
			{
				JOptionPane.showMessageDialog(null, "key should be in the range of 1~9","Title",JOptionPane.ERROR_MESSAGE);
				return;
			}
			// do decrypt
			try
			{
				// read file
				byte[] encoded = Files.readAllBytes(Paths.get("A11.txt"));  //讀所有的byte 到一個byte[]  //Paths.get("A11.txt")就是幫你取得路徑
				String str = new String  (encoded,Charset.defaultCharset());//encoded引入一個Byte陣列      charset就是編碼
				String decrypted = "";   //decrypted 就是最後的解碼 所以一開始社空的
				// shift char
				char[] arr = str.toCharArray();//把string變成char陣列 然後存進arr裏
				for(int i=0;i<arr.length;i++)
					decrypted += (char)(arr[i]-key); //(arr[i]-key)會被當成int形態 做完運算後 再強致轉型成char
				txt_result.setText(decrypted);
					
			}
			catch (Exception e)
			{
				// error
				System.out.println("Error: " + e.getMessage());
			}
		}
	}
}