package ce1002.a11.s102502547;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {

	JButton decrypt;
	JTextField key;
	JLabel content;
	JTextArea result;
	ActionListener listener;

	MyFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		setSize(470, 300);

		listener = new DecodeListener();

		decrypt = new JButton("Decrypt");
		decrypt.addActionListener(listener);
		decrypt.setBounds(360, 5, 80, 30);
		add(decrypt);

		key = new JTextField(10);
		key.setBounds(5, 5, 350, 30);
		add(key);

		content = new JLabel("content");
		content.setBounds(5, 40, 435, 215);
		add(content);
		
		result = new JTextArea();
		result.setBounds(50, 40, 400, 215);
		add(result);

		setVisible(true);
	}

	class DecodeListener implements ActionListener {

		String key;
		String content;

		public void actionPerformed(ActionEvent e) {
			key = MyFrame.this.key.getText();

			if (key.isEmpty()) {
				showErrorMessage("Can't be empty!!");
			} else if (key.matches("^[1-9]$")) {
				content = readFile("A11.txt");
				String str = "";
				int key_value = Integer.parseInt(key);
				for (char c : content.toCharArray()) { //�ѽX
					str += (char) (c - key_value);
				}
				result.setText(str);
			} else {
				showErrorMessage("Out Of Range!");
			}
		}

		String readFile(String filename) { //Ū��
			String content = "";
			try {
				FileReader file = new FileReader("A11.txt");
				BufferedReader input = new BufferedReader(file);
				while (input.ready()) {
					content += input.readLine() + "\n";
				}
			} catch (IOException event) {
				event.printStackTrace();
			}
			return content;
		}

		void showErrorMessage(String massage) {
			JOptionPane.showMessageDialog(null, massage, "", JOptionPane.ERROR_MESSAGE);
		}
	}
}
