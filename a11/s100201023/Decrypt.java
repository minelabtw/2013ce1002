package ce1002.a11.s100201023;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Decrypt implements ActionListener
{	
	private GUI gui;
	
	public Decrypt()
	{
		// TODO Auto-generated constructor stub
		gui = new GUI();
		gui.setaction(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		if(gui.getkey().isEmpty())
			JOptionPane.showMessageDialog(null, "Can’t be empty!!", "Warning!!", JOptionPane.WARNING_MESSAGE);
		else
		{
			int key = gui.getkey().charAt(0) - 48;
			if(key < 1 || key > 9 || gui.getkey().length() > 1)
				JOptionPane.showMessageDialog(null, "Out Of Range!", "Warning!!", JOptionPane.WARNING_MESSAGE);
			else 
			{
				String code , decode = "";
				try
				{ 
					//Decrypt coed
					FileReader fin = new FileReader("A11.txt");
					BufferedReader br = new BufferedReader(fin);
					
					while(br.ready())
					{
						code = br.readLine();
						for(int i = 0 ; i < code.length() ; ++i)
							decode += (char)(code.charAt(i) - key);
					}
					gui.setcode(decode);
				}
				catch (Exception e2)
				{
					// TODO: handle exception
					JOptionPane.showMessageDialog(null, "Can not open Encrypt file - A11.txt", "Warning!!", JOptionPane.WARNING_MESSAGE);
				}

			}
		}
		
	}
}
