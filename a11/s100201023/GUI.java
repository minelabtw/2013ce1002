package ce1002.a11.s100201023;

import javax.swing.JFrame;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.GridLayout;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame
{
	private JTextField textField;
	private JLabel label;
	private JButton btnDecrypt;
	
	//setting GUI
	public GUI() 
	{
		setVisible(true);
		setTitle("A11-100201023");
		setSize(new Dimension(300, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblPleaseInputKey = new JLabel("Please input key value(1~9) :");
		getContentPane().add(lblPleaseInputKey);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		textField = new JTextField();
		panel.add(textField, BorderLayout.CENTER);
		textField.setColumns(10);
		
		btnDecrypt = new JButton("Decrypt");
		panel.add(btnDecrypt, BorderLayout.EAST);
		
		JLabel lblNewLabel = new JLabel("The Decrypt code is :");
		getContentPane().add(lblNewLabel);
		
		label = new JLabel("");
		getContentPane().add(label);
	}
	
	//getting method
	String getkey()
	{
		return textField.getText();
	}
	
	//setting method
	void setcode(String code)
	{
		label.setText(code);
	}
	
	void setaction(ActionListener in)
	{
		btnDecrypt.addActionListener(in);
	}
}
