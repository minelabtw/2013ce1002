package ce1002.a11.s102502543;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyFrame extends JFrame {
	JLabel key = new JLabel("Key:");
	JTextField code = new JTextField();
	JButton btn = new JButton("Decrypt");
	JLabel con = new JLabel("Result?");

	MyFrame() {
		setTitle("A11-102502543");
		setBounds(0, 0, 500, 300);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		key.setBounds(25, 5, 45, 30);
		code.setBounds(70, 5, 270, 30);
		btn.setBounds(350, 5, 110, 30);
		con.setBounds(220, 40, 50, 50);
		add(key);
		add(code);
		add(btn);
		add(con);
		btn.addActionListener(new ButtonActionListener(this));
	}

	class ButtonActionListener implements ActionListener {
		JFrame frame;

		public ButtonActionListener(JFrame frame) {
			this.frame = frame;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			if (code.getText().isEmpty())
				showEmptyMsg();
			else if (Integer.parseInt(code.getText()) < 1
					|| Integer.parseInt(code.getText()) > 9)
				showOOFMsg();
			else {
				String str;
				String str2 = "";
				try {
					FileReader fr = new FileReader("A11.txt");
					BufferedReader br = new BufferedReader(fr);
					while ((str = br.readLine()) != null) {
						char[] array = str.toCharArray();
						for (int a = 0; a < str.length(); a++) {
							str2 = str2
									+ (char) ((int) (array[a]) - Integer
											.parseInt(code.getText()));
						}
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				con.setText(str2);
			}
		}

		private void showOOFMsg() {
			// TODO Auto-generated method stub
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
		}

		private void showEmptyMsg() {
			// TODO Auto-generated method stub
			JOptionPane.showMessageDialog(frame, "Can’t be empty!!");
		}
	}
}
