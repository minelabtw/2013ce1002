package ce1002.a11.s102502562;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Decrypt extends JFrame{
	JButton save;
	JLabel key;
	JTextField keyNumber;
	JTextArea text;
	
	Decrypt()//設定JFrame
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("A11-102502562");
        setSize(280,300);
        UserInterface();
	}
	private void UserInterface()//設定元素與位置
	{
		key=new JLabel("Key:");
		key.setBounds(5,0,30,30);
		add(key);
		keyNumber=new JTextField();
		keyNumber.setBounds(40,0,100,30);
		add(keyNumber);
		save=new JButton("Decrypt");
		save.setBounds(145,0,100,30);
		save.addActionListener(new DecryptText());
		add(save);
		text=new JTextArea();
		text.setBounds(5,35,245,250);
		text.setLineWrap(true);
		add(text);
	}
	class DecryptText implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent event)
		{
			if(keyNumber.getText().isEmpty())//判斷key是否為空值
			{
				JOptionPane.showMessageDialog(null,"Can’t be empty!!","訊息",JOptionPane.ERROR_MESSAGE);
				return;
			}
			int keyNum=Integer.valueOf(keyNumber.getText());
			if(keyNum<1 || keyNum>9)//判斷key的範圍
			{
				JOptionPane.showMessageDialog(null,"Out Of Range!","訊息",JOptionPane.ERROR_MESSAGE);
				return;
			}
			try//解碼
			{
				byte[] code=Files.readAllBytes(Paths.get("A11.txt"));
				String str=new String(code,Charset.defaultCharset());
				String decrypted="";
				for(char c:str.toCharArray())
				{
					decrypted+=(char)(c-keyNum);
				}
				text.setText(decrypted);
			}
			catch(Exception e)
			{
				System.out.println("Error: " + e.getMessage());
			}
		}
		
	}
}
