package ce1002.a11.s102502010;

import javax.swing.JFrame;

import java.io.File;
import java.io.FileNotFoundException; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JOptionPane;

public class MyFrame  extends JFrame implements ActionListener {
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	private JLabel label = new JLabel("Key:");
	private JLabel answer = new JLabel("Result?");
	private JButton button = new JButton("Decrypt");
	private JTextField textfield = new JTextField(15);
	
	MyFrame()
	{
		setTitle("A11-102502010");
		setLayout(null);//浮動預設
		setBounds(200,200,350,150);//視窗大小
		setVisible(true);//顯示
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		paint1(p1,250,5);
		paint2(p2,40,8);
		paint3(p3,5,8);
		paint4(p4,150,50);
	}
	
	public void paint1(JPanel panel, int x, int y)   //button
	{
		panel.setBounds(x, y, 80 , 40);
		panel.add(button);
		button.addActionListener((ActionListener) this);
		add(panel);
	}
	
	public void paint2(JPanel panel, int x, int y)   //textfield
	{
		panel.setBounds(x, y, 200, 40);
		panel.add(textfield);
		add(panel);
	}
	
	public void paint3(JPanel panel, int x, int y)   //label
	{
		panel.setBounds(x, y, 50, 30);
		panel.add(label);
		add(panel);
	}
	
	public void paint4(JPanel panel, int x, int y)   //answer
	{
		panel.setBounds(x, y, 50, 30);
		panel.add(answer);
		add(panel);
	}
	
	public void actionPerformed(ActionEvent a) {
		if(textfield.getText().length() == 0) {  //can’t be empty!
		    JPanel panel = new JPanel();
		    panel.setBackground(Color.BLUE);
		    panel.setMinimumSize(new Dimension(200,200));		 
		    JFrame frame = new JFrame("訊息");
		    JOptionPane.showMessageDialog(frame, "Can’t be empty!!");
		 }
		else if(textfield.getText().length() == 1 && 0 != new Integer(textfield.getText()).intValue()) {
			
			try {
			   File file = new File("A11.txt");  
			   Scanner input = new Scanner(file);  //read file
			   String ans="";
			   while(input.hasNext())
			   {
				   String s = input.next();				   
				   int i;
					for(i=1; i<10; i++)
					{
						if(i == new Integer(textfield.getText()).intValue())
							break;
					}
					for ( int j = 0; j < s.length(); j++ ) 
					{
						if(s.charAt( j )!=' ')
						{
							char c = s.charAt( j ); 
							ans += (char)(c-i); 
						}
					} 
			   }
			   answer.setText(ans);
			   paint4(p4,150,50);
			   input.close();
		    } catch (FileNotFoundException e) {
			   e.printStackTrace();
		    }
		}
		else {  // Out Of Range!
			JPanel panel = new JPanel();
		    panel.setBackground(Color.BLUE);
		    panel.setMinimumSize(new Dimension(200,200));		 
		    JFrame frame = new JFrame("訊息");
		    JOptionPane.showMessageDialog(frame, "Out Of Range!");
		} 
	}
}
