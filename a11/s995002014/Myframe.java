package ce1002.a11.s995002014;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextArea;

import java.io.*;
import java.util.Scanner;

public class Myframe extends JFrame {
	private JTextField textField;
	int key=0;
	JTextArea textArea = new JTextArea();
	public Myframe() {
		setTitle("A11");
		getContentPane().setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 300);
		setVisible(true);
		
		JLabel lblKey = new JLabel("Key:");
		lblKey.setBounds(10, 10, 30, 21);
		getContentPane().add(lblKey);
		
		textField = new JTextField();
		textField.setBounds(38, 10, 96, 21);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnDecrypt = new JButton("Decrypt");
		//按鈕event listener
		btnDecrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				key=Integer.parseInt(textField.getText()); //取得key
				textArea.setText("");
				textArea.append(readfile(key)); //顯示
			}
		});
		btnDecrypt.setBounds(144, 9, 87, 23);
		getContentPane().add(btnDecrypt);
		
		JLabel lblResult = new JLabel("Result:");
		lblResult.setBounds(128, 67, 46, 15);
		getContentPane().add(lblResult);
		
		textArea.setLineWrap(true);
		textArea.setBounds(90, 85, 215, 122);		
		textArea.setEditable(false);
		getContentPane().add(textArea);
		
	}
	
	//讀檔以及解密
	private String readfile(int keyin) {
		char c;
		try {
			Scanner in = new Scanner(new FileReader("A11.txt")); //讀黨
			String str =in.next();
			while(in.hasNext()){
				 str+=in.next();
			}
			char[] charArray =str.toCharArray(); //將字串轉成字元陣列
		    for (int i=0;i<charArray.length;i++) {
		        char temp =(char)((int)charArray[i]-keyin); //減回key
		        charArray[i]= temp;
		    }
		    String text = String.copyValueOf(charArray); //轉回array
		    System.out.println(text);
		    return text;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
