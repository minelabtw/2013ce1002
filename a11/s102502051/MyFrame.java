package ce1002.a11.s102502051;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame
{
    private class ButtonListener
        implements ActionListener
    {

        public void actionPerformed(ActionEvent e)  //解密內容
        {
            if(Key.getText().isEmpty())
                showEmptyMsg();
            else
            if(Integer.valueOf(Key.getText()).intValue() < 1 || Integer.valueOf(Key.getText()).intValue() > 9)
            {
                showOutOfRange();
            } else
            {
                String fileName = "A11.txt";
                try
                {
                    DataInputStream input = new DataInputStream(new FileInputStream(fileName));
                    String str;
                    byte c;
                    for(str = ""; input.available() > 0; str = (new StringBuilder(String.valueOf(str))).append((char)c).toString())
                    {
                        c = input.readByte();
                        int key = Integer.valueOf(Key.getText()).intValue();
                        c -= key;
                    }

                    label.setText(str);
                    input.close();
                }
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
                catch(NumberFormatException e1)
                {
                    e1.printStackTrace();
                }
                catch(IOException e1)
                {
                    e1.printStackTrace();
                }
            }
        }

        private void showEmptyMsg()
        {
            JOptionPane.showMessageDialog(frame, "Can't be empty!");  //輸入不能為空
        }

        private void showOutOfRange()
        {
            JOptionPane.showMessageDialog(frame, "Out Of Range!"); //只能輸入1~9
        }

        JFrame frame;
        final MyFrame this$0;

        ButtonListener(JFrame frame)
        {
            this$0 = MyFrame.this;
           
            this.frame = frame;
        }
    }


    MyFrame()    //介面設定
    {
        Decrypt = new JButton("Decrypt");
        Key = new JTextField(10);
        label = new JLabel("Result");
        JPanel pane1 = new JPanel();
        pane1.add(new JLabel("Key:"));
        pane1.add(Key);
        pane1.add(Decrypt);
        JPanel pane2 = new JPanel();
        pane2.add(label);
        add(pane1, "North");
        add(pane2, "Center");
        Decrypt.addActionListener(new ButtonListener(this));
    }

    private JButton Decrypt;  //加入BUTTON 以及解密內容
    private JTextField Key;
    private JLabel label;


}


