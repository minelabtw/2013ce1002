package ce1002.a11.s102502032;

import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class A11 extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 5740824758879882341L;
	private JPanel				contentPane;
	private JTextField			textField;
	private JLabel				lblResult			= new JLabel("Result?");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					A11 frame = new A11();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public A11()
	{
		// Frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("A11-102502032");
		setBounds(100, 100, 268, 115);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		// label
		JLabel lblKey = new JLabel("Key");
		lblKey.setBounds(10, 10, 46, 15);
		contentPane.add(lblKey);
		// textField
		textField = new JTextField();
		textField.setBounds(52, 7, 96, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		// result label
		lblResult.setBounds(52, 53, 46, 15);
		contentPane.add(lblResult);
		// button
		JButton btnNewButton = new JButton("Decrypte");
		btnNewButton.setBounds(158, 6, 87, 23);
		contentPane.add(btnNewButton);
		btnListener listener = new btnListener();
		btnNewButton.addActionListener(listener);
	}

	public class btnListener implements ActionListener
	{
		int		key		= 0;
		byte	temp[]	= new byte[1];

		@Override
		public void actionPerformed(ActionEvent e)
		{
			if (loadKey() == true)
			{
				decrypte();
				printResult();
			}
			else if (textField.getText().length() == 0)
			{
				JOptionPane.showMessageDialog(null, "Can��t be empty!!",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}

		private void printResult()
		{
			char c[] = new char[temp.length];
			for (int i = 0; i < temp.length; i ++)
			{
				c[i] = (char) temp[i];
			}
			lblResult.setText(new String(c));
		}

		public boolean loadKey()
		{
			try
			{
				key = Integer.parseInt(textField.getText());
				if (checkSize(key, 1, 9) == true)
				{
					return true;
				}
				else
				{
					key = 0;
					return false;
				}
			}
			catch (NumberFormatException e)
			{
				// TODO: handle exception
			}
			return false;
		}

		public void decrypte()
		{
			try
			{
				FileInputStream in = new FileInputStream("A11.txt");
				temp = new byte[in.available()];
				in.read(temp);
				for (int i = 0; i < temp.length; i ++)
				{
					temp[i] -= key;
				}
			}
			catch (FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// get size;
		public boolean checkSize(Integer n, int min, int Max)
		{
			if (min > Max)
			{
				System.out.print("getSize: size error (min > Max)");
				return false;
			}
			if (n < min || n > Max)
			{
				JOptionPane.showMessageDialog(null, "Out of range!", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}
			return true;
		}
	}
}
