package ce1002.a11.s102502024;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.io.IOException;
import javax.swing.JButton;
import java.io.*;
import java.awt.event.*;
public class Dycrept extends JFrame {
	static JLabel l2=new JLabel("Result?");
	static JLabel l1=new JLabel("Key?");
	static JPanel p1=new JPanel();
	static JPanel p2=new JPanel();
	static JButton b=new JButton("Dycrept");
	static JTextField f=new JTextField();  //宣告textfield
	String key;  //存放key值
	int i;  //int型態的key值
	
	public static void main(String[] args) {
		Dycrept d=new Dycrept();
		d.setLayout(null);  //取消預設排版
		d.setLocationRelativeTo(null);  //使視窗置中
		d.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //設定關閉鍵
		d.setSize(300,100);
		d.setTitle("A11-102502024");
		p1.setBounds(10,10,30,30);
		p2.setBounds(10,40,180,50);
		l1.setBounds(0,0,30,30);
		l2.setBounds(90,40,150,50);
		p1.add(l1);
		p2.add(l2);
		f.setBounds(50,5,100,30);
		b.addActionListener(d.new Listener());  //設定事件聆聽者
		b.setBounds(160,5,100,30);
		d.add(p1);  //放上所有東西
		d.add(p2);
		d.add(f);
		d.add(b);
		d.setVisible(true);
	}

	class Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {  //button的功能
			if(f.getText().isEmpty())
			{
				empty();
			}
			else if(i<=0 || i>=10)
			{
				key=f.getText();
				i=Integer.valueOf(key);
				wrong();
			}
			else
			{
				key=f.getText();
				i=Integer.valueOf(key);
				readfile();
			}
		}
	}
	
	void readfile()  //讀取檔案並解碼
	{
		try{
		    File f=new File("A11.txt");
		    int size=(int)f.length();
			FileReader read=new FileReader(f);
			char[] buffer=new char[size];
			read.read(buffer);
			for(int i=0;i<size;i++)
			{
				buffer[i]=(char)(buffer[i]-this.i);
			}
			String str=new String(buffer);
			l2.setText(str);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	 void empty() {
		 JOptionPane.showMessageDialog(null, "File name can't be empty!", "A11", JOptionPane.ERROR_MESSAGE);  //彈出訊息是空白的視窗
	   }
	 void wrong(){
		 JOptionPane.showMessageDialog(null,"Out Of Range!", "A11", JOptionPane.ERROR_MESSAGE);  //彈出訊息是超出範圍的視窗
	 }
}
