package ce1002.a11.s102502017;
//hello world
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Decoder extends JFrame{
	
	JButton decrypt;
    JTextField key;
    JLabel result;
    ActionListener listener;
    
    Decoder(){
    	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    	this.setLayout(null);
    	this.setSize(500, 300);
    	this.setTitle("A11-102502017");
    	this.setVisible(true);
    	
    	newMembers();
    }
    
    void newMembers() {
        listener = new Decode_listener();
        
        decrypt = new JButton("Decrypt");
        decrypt.addActionListener(listener);
        decrypt.setBounds(5, 5, 80, 30);
        add(decrypt);
        
        key = new JTextField(10);
        key.setBounds(90, 5, 390, 30);
        add(key);
        
        result = new JLabel("Result will be shown here.");
        result.setBounds(5, 35, 475, 375);
        add(result);
    }

    
    class Decode_listener implements ActionListener {
    	String key;
        String content;
        
        public void actionPerformed(ActionEvent e) {
            key = Decoder.this.key.getText();

            if(key.isEmpty()) {
                show_error_message("Can't be empty!!");
            } else if(key.matches("^[1-9]$")) {
            	//*string.matches
                content = read_file("A11.txt");
                String str = "";
                int key_value = Integer.parseInt(key);
                for(char c : content.toCharArray()) {
                    str += (char)(c - key_value);
                }
                result.setText(str);
            } else {
                show_error_message("Out Of Range!");
            }
        }    
        
	    String read_file(String filename) {
	        String content = "";
	        try {
	            FileReader file = new FileReader("A11.txt");
	            BufferedReader input = new BufferedReader(file);
	            while (input.ready()) {
	                content += input.readLine() + "\n";
	            }
	        } catch (IOException event) {
	            event.printStackTrace();
	        }
	        return content;
	    }
	
	    void show_error_message(String massage) {
	        JOptionPane.showMessageDialog(null, massage, "A11", JOptionPane.ERROR_MESSAGE);
	    }
	    
	    }

}
