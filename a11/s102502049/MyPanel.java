package ce1002.a11.s102502049;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyPanel extends JPanel {
	private JLabel keyLabel = new JLabel("Key: ");
	private JLabel reLabel = new JLabel("Result?");
	private JButton button = new JButton("Decrypt");
	private JTextField text = new JTextField();

	MyPanel() {
		add(keyLabel);
		add(reLabel);
		add(button);
		add(text); // add component

		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Scanner cin = new Scanner(text.getText());
				int key = 0;
				try {
					key = cin.nextInt();
					if (key < 1 || key > 9) { // key out of range
						JFrame opFrame = new JFrame(); // JOptionPane
						JOptionPane.showMessageDialog(opFrame, "Out Of Range!");
					}
					else{
						try { // show answer
							FileInputStream input = new FileInputStream("A11.txt");
							int shift; // save next int and swift
							String answer = ""; // save the answer
							while ((shift = input.read()) != -1) { // whether stop
								if (shift != 32) // space
									shift -= key; // shift
								answer += String.valueOf((char) (shift)); // casting and turn to string
							}
							reLabel.setText(answer); // show answer
						} 
						catch (FileNotFoundException exception) { // file not found
							JFrame opFrame = new JFrame(); // JOptionPane
							JOptionPane.showMessageDialog(opFrame,"A11.txt doesn't exist!");
						} 
						catch (IOException exception) { // IOException
							JFrame opFrame = new JFrame(); // JOptionPane
							JOptionPane.showMessageDialog(opFrame,"IOException happen!");
						}
					}
				} 
				catch (NoSuchElementException exception) {
					JFrame opFrame = new JFrame(); // JOptionPane
					JOptionPane.showMessageDialog(opFrame, "Can't be empty!");
				}
			}//actionPerformer
		}); // end inner-class
	} // end constructor

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		keyLabel.setBounds((int) (getWidth() / 20), (int) (getHeight() / 6),
				50, 20); // set component size
		text.setBounds((int) (getWidth() / 20 * 3), (int) (getHeight() / 8),
				(int) (getWidth() / 2), 20);
		button.setBounds((int) (getWidth() / 20 * 14), (int) (getHeight() / 8),
				(int) (getWidth() / 4), (int) (getHeight() / 5));
		reLabel.setBounds((int) (getWidth() / 3), (int) (getHeight() / 5 * 3),
				(int) (getWidth() / 3), 20);
	}
}
