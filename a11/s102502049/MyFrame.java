package ce1002.a11.s102502049;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyPanel panel = new MyPanel();
	
	MyFrame(){
		setTitle("A11-102502049");	//	title
		setLocationRelativeTo(null);	// set frame
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 170);
		setVisible(true);
		
		add(panel); // panel
	}
}
