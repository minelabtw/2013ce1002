package ce1002.a11.s102502544;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class A11 {

	private JFrame frame;//宣告JFrame
	private JLabel key;
	private JLabel decoder;
	private JTextField writekey;
	private String result = "Result?";
	private int keynum;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A11 window = new A11();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public A11() {//建構子
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 291, 89);//設定邊界
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);//設定排版
		
		writekey = new JTextField();
		writekey.setBounds(34, 0, 120, 21);
		frame.getContentPane().add(writekey);
		writekey.setColumns(10);
		
		key = new JLabel("Key:");
		key.setBounds(10, 3, 46, 15);
		frame.getContentPane().add(key);
		
		JButton Decrypt = new JButton("Decrypt");
		Decrypt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent dec) {
			try{
				empty();
				keynum = Integer.valueOf(writekey.getText());
				
				if(keynum < 1 || keynum > 9)
				{
					Frame smallframe = new Frame("Out of Range!");
					smallframe.setVisible(true);
				}else{
					decodefile();
					decoder.setText(result);
				}
			}catch(Exception e){
				Frame smallframe = new Frame(e.getMessage());
				smallframe.setVisible(true);
			}
		}
		});
		Decrypt.setBounds(155, 0, 111, 21);
		frame.getContentPane().add(Decrypt);
		
		decoder = new JLabel(result);
		decoder.setBounds(112, 21, 154, 28);
		frame.getContentPane().add(decoder);
	}
	
	public void empty() throws Exception
	{	
		if(writekey.getText().isEmpty() == true)
		{
			throw new Exception("Can't be empty!!");
		}
	}
	
	public void decodefile() throws IOException
	{
		
		try {
			FileReader file = new FileReader("A11.txt");
			BufferedReader br = new BufferedReader(file);
	
			do{
				if(br.equals(null))
				{
					break;
				}else{
					String cutter = br.readLine();
					
					char[] bedecoded = cutter.toCharArray();
					
					for(int counter = 0;counter < bedecoded.length;counter++)
					{
						bedecoded[counter] -= keynum;
						result = String.valueOf(bedecoded);
					}
					break;
				}
			}while(true);
			
			br.close();
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
