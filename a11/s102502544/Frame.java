package ce1002.a11.s102502544;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Frame extends JFrame{
	
	JLabel outrange;//宣告Label
	
	public Frame(String text)
	{
		setBounds(150,150,100,50);//設定邊界
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		outrange = new JLabel(text);
		outrange.setBounds(0,0,80,40);
		add(outrange);
	}

}
