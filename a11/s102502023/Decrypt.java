package ce1002.a11.s102502023;

import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class Decrypt extends JFrame {
	private JTextField jtf = new JTextField(10); // initialize JTextField jtf and have 10 letter spaces
	private JButton jbt = new JButton("Decrypt"); // initialize JButton jbt and print "Decrypt" on the button
	private JTextArea jta = new JTextArea(); // initialize JTextArea jta
	private JLabel jbl = new JLabel("Key:"); // initialize JLabel jbl and print "Key:" 
	private JLabel jbl1 = new JLabel("Content: "); // initialize JLabel jbl1 and print "Content: "
  public Decrypt() {
	  jta.setLineWrap(true);
	  jta.setWrapStyleWord(true);
	  jta.setEditable(true);
	  
      jbt.addActionListener(new ButtonListener());
	  
	  JPanel p = new JPanel();
	  p.setLayout(new FlowLayout(FlowLayout.LEFT));
	  p.add(jbl);
	  p.add(jtf);
	  p.add(jbt);
	  
	  setLayout(new BorderLayout());
	  add(p, BorderLayout.NORTH);
	  add(jbl1, BorderLayout.WEST);
	  add(jta, BorderLayout.CENTER);
  }
  
  public static void main(String[] args) {
	  Decrypt frame = new Decrypt();
	  frame.setSize(500, 500);
	  frame.setLocationRelativeTo(null);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setVisible(true);
	  frame.setTitle("A11-102502023");
  }
  
  private class ButtonListener implements ActionListener {
	  @Override
	  public void actionPerformed(ActionEvent e) {
		if(jtf.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Can't be empty!");
			return;
		} 
		
		if ( Integer.parseInt(jtf.getText()) < 1 || Integer.parseInt(jtf.getText()) > 9) {
			JOptionPane.showMessageDialog(null, "Out of range!");
			return;
		}
		
		
		
		try {
			java.io.File file = new java.io.File("A11.txt");
			Scanner input = new Scanner(file);
			while (input.hasNext()) {
				String string = input.next();
				String s = "";
				int integer = Integer.parseInt(jtf.getText());
				//System.out.println(string);
				for (int i = 0; i < string.length(); i++) {
					//System.out.print((char)((int)(string.charAt(i)) - Integer.parseInt(jtf.getText())));
					s += Character.toString(((char)((int)(string.charAt(i)) - integer)));
				}
				jta.append(s);
			}

			input.close();
			}
			catch (FileNotFoundException ex) {
				System.out.println("There must be wrong");
				System.exit(1);
			}
		
	  }
  } 
}
