package ce1002.a11.s102502038;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.CharBuffer;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class A11 {
	public static void main(String[] args){
		GUI gui = new GUI();//build up GUI
	}
}
class GUI{
	JFrame frame;
	JPanel northPanel;
	JTextField smallText;
	JTextArea largeText;
	JButton button;
	GUI(){//GUI up pack
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("A11-102502038");
		frame.setVisible(true);
		northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel,0));
		smallText = new JTextField();
		largeText = new JTextArea();
		largeText.setLineWrap(true);
		button = new JButton("Encrypt");
		button.addActionListener(new Listener(smallText,largeText));//add action listener
		northPanel.add(new JLabel("Key:"));
		northPanel.add(smallText);
		northPanel.add(button);
		frame.getContentPane().add(northPanel,BorderLayout.NORTH);
		frame.getContentPane().add(largeText,BorderLayout.CENTER);
		frame.getContentPane().add(new JLabel("Content:"),BorderLayout.WEST);
		frame.setSize(300, 200);
	}
}
class Listener implements ActionListener{//listener
	private JTextField key;
	private JTextArea text;
	private JOptionPane alert;
	private String fileName = "A11.txt";
	Listener(JTextField key,JTextArea text){
		this.key = key;
		this.text = text;
	}
	public String encryte(char[] cs,int key){
		String temp = "";
		for(int i = 0;i<cs.length;i++){//minus acsii value of char
			temp += (char)((int)cs[i] - key);
		}
		return temp;
	}
//	@Override
	public void actionPerformed(ActionEvent arg0) {
		alert = new JOptionPane();
		if(!key.getText().isEmpty()){//check if key empty
			try {
				if(Integer.parseInt(key.getText()) > 9 || Integer.parseInt(key.getText()) < 1)
					throw new NumberFormatException();
				FileReader file = new FileReader(new File(fileName));
				CharBuffer cbuf = CharBuffer.allocate(8192);
				file.read(cbuf);
				text.setText(encryte(cbuf.array(),Integer.parseInt(key.getText())));//read text,encryte,and throw into JTextField 
				file.close();
			} catch (NumberFormatException e){
				alert.showMessageDialog(null, "Out Of Range!!",null,0);
				System.out.print(e.getMessage());
			} catch (IOException e) {
				alert.showMessageDialog(null, e.getMessage());
			}
		}else{
			alert.showMessageDialog(null, "Can't be empty!!",null,0);//alert when key name empty
		}
	}
	
}