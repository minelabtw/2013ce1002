package ce1002.a11.s102502517;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;

public class A11 extends JFrame {
	private JTextField txtKey;
	private JLabel resultLabel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A11 frame = new A11();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public A11() {
		setTitle("A11-102502517");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		setLayout(null); //不預設輸出格式

		txtKey = new JTextField();
		txtKey.setBounds(54, 31, 96, 21);
		txtKey.addKeyListener(new KeyAdapter() { //宣告KeyAdapter使textfield內只能輸入數字
			public void keyTyped(KeyEvent e) {
				int keyChar = e.getKeyChar();

				if (keyChar >= KeyEvent.VK_0 && keyChar <= KeyEvent.VK_9) { //若數字在0到9之間則不做反應

				} else {
					e.consume(); // 消除錯誤輸入
				}
			}
		});
		add(txtKey);
		txtKey.setColumns(10);

		JLabel lblNewLabel = new JLabel("key:");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		lblNewLabel.setBounds(23, 33, 46, 15);
		add(lblNewLabel);

		resultLabel = new JLabel("result", JLabel.CENTER);
		resultLabel.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		resultLabel.setBounds(10, 70, 414, 86);
		add(resultLabel);

		JButton button = new JButton("確定");
		button.setBounds(160, 30, 87, 23);
		button.addActionListener(new ActionListener() { //宣告ActionListener使按鈕被點擊後開始動作
			public void actionPerformed(ActionEvent e) {
				int key = Integer.parseInt(txtKey.getText()); //將textfield內的文字轉換為integer
				String plain = ""; //宣告空字串

				if (key < 1 || key > 9)
					JOptionPane.showMessageDialog(new JFrame(), "Out Of Range!"); //若key不在範圍內則跳出視窗
				else {
					try {
						FileInputStream input = new FileInputStream("A11.txt"); //讀取A11.txt
						int word; //用來儲存文字編碼
						
						while ((word = input.read()) != -1) { //若文件尚未結束
							plain += (char) (word - key); //解碼並儲存至字串中
						}
						input.close(); //結束檔案
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					resultLabel.setText(plain); //輸出結果
				}
			}
		});
		add(button);
	}
}
