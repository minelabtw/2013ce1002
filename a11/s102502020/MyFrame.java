package ce1002.a11.s102502020;

import javax.swing.JFrame;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;

public class MyFrame extends JFrame{
	
	private JButton button;
	private JTextField textfield;
	private JLabel key = new JLabel("Key :"); 
	private JLabel result = new JLabel("Result ?");
	
	public MyFrame() {
		setTitle("A11-102502020");
		setSize(300, 150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		//setLayout(null);                        //有這行會怪怪的
		setVisible(true);
 
		textfield = new JTextField();
		textfield.setBounds(35, 10, 100, 20);
		textfield.setColumns(10);
		
 
		button = new JButton("Decrypt");
		button.setBounds(150, 5, 100, 30);
		button.addActionListener(new DecryptListener());
		button.setHorizontalTextPosition(JButton.CENTER);
		
		Font font1 = new Font(Font.DIALOG, Font.BOLD, 12);    //字形和大小
		key.setFont(font1);       
		key.setBounds(5,10,40,25); 
		
		Font font2 = new Font(Font.DIALOG, Font.BOLD, 12);
		result.setFont(font2);
		result.setBounds(100,50,70,25);
		add(textfield);
		add(button);
		add(key);
		add(result);
	}
	class DecryptListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(textfield.getText().isEmpty()){
				JOptionPane.showMessageDialog(null, "Can’t be empty!!", "訊息", JOptionPane.INFORMATION_MESSAGE);     //跳出訊息視窗
			}else{
				try {
					int key = Integer.parseInt(textfield.getText());                        //將textfield的整數存給key
					if(key<1 || key>9){
						JOptionPane.showMessageDialog(null, "Out Of Range!", "訊息", JOptionPane.INFORMATION_MESSAGE);
					}else{
						FileReader file = new FileReader("A11.txt");                        //讀取A11.txt
						BufferedReader buffered = new BufferedReader(file);                 //將這個檔案的內容寫入buffered中
						char ASC2 [] = buffered.readLine().toCharArray();                 //將buffered(A11.txt)的內容轉成字元
						for(int i = 0 ; i < ASC2.length ; i++){                           //將每個字元以ASCII來解密
							ASC2[i] = (char)((int)ASC2[i] - key);
						}
						String sceret = new String(ASC2);                            //將字元轉換成字串
						result.setText(sceret);                                       //在result中輸出字串
					    }
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			}
		}

	}

}
