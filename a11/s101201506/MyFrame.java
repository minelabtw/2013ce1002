package ce1002.a11.s101201506;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame {//KEY RESULT DECRYPT BUTTOM
	private JLabel label1 = new JLabel("Key: ");
	private JLabel label2 = new JLabel("Result?");
	private JTextField text = new JTextField();
	private JButton button = new JButton("Decrypt");

	MyFrame() {// 排版
		setTitle("A11-101201506");
		setSize(300, 150);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);

		label1.setBounds(0, 20, 30, 30);
		label2.setBounds(0, 60, 50, 30);
		text.setBounds(30, 20, 100, 30);
		button.setBounds(130, 20, 90, 30);
		text.setText(null);

		SaveListener savelistener = new SaveListener();
		button.addActionListener(savelistener);
		// add
		add(label1);
		add(label2);
		add(text);
		add(button);
	}

	public class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			try {// 設定解密
				if (text.getText().equals("")) {

					throw new NullPointerException();
				} else {

					int Ciphertext = Integer.parseInt(text.getText());
					if (Ciphertext > 0 && Ciphertext < 10) {

						int key = Integer.parseInt(text.getText());

						FileReader file = new FileReader("A11.txt");
						Scanner input = new Scanner(file);
						label2.setText("");

						while (input.hasNext()) {
							String data = input.next();
							for (int i = 0; i < data.length(); i++) {
								label2.setText(label2.getText()
										+ Decrypt(data.charAt(i), key));
							}
						}

					} else {
						throw new Exception();
					}

				}
				// 設定輸入範圍 否則無法解密
			} catch (NullPointerException e1) {
				JOptionPane.showMessageDialog(new JFrame("訊息"),
						"Can't be empty!!");
			} catch (Exception e2) {
				JOptionPane
						.showMessageDialog(new JFrame("訊息"), "Out Of Range!");
			}

		}

		public char Decrypt(char a, int key) {
			int number = (int) a;
			number = number - key;
			return (char) number;
		}

		public void test(int Ciphertext) {
			try {

			} catch (Exception e2) {

			}
		}
	}

}
