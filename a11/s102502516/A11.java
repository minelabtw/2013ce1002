package ce1002.a11.s102502516;

import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.AllPermission;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class A11 extends JFrame {

	private JLabel keyJLabel = new JLabel("Key:");
	private TextField keyTextField = new TextField();
	private JButton decryptButton = new JButton("Decrypt");
	private JLabel plainText = new JLabel("Result?");

	A11() {
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(900, 600, 250, 130);
		setVisible(true);
		setTitle("A11-102502516"); // 視窗設定

		keyJLabel.setBounds(15, 15, 30, 20);
		add(keyJLabel); // 加入文字

		keyTextField.setBounds(55, 15, 50, 20);
		add(keyTextField); // 加入key輸入框

		decryptButton.setBounds(125, 14, 80, 20);
		decryptButton.addActionListener(new DecryptButtonListener());
		add(decryptButton); // 加入按鈕

		plainText.setBounds(50, 50, 120, 30);
		add(plainText); // 加入輸出區

	}

	class DecryptButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			if (keyTextField.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Can't be empty!");
				// 沒有輸入
			} else { // 有輸入
				int key = Integer.parseInt(keyTextField.getText());
				if (key > 9 || key < 1)
					JOptionPane.showMessageDialog(null, "Out of range!"); // 超出範圍
				else { // 範圍內
					try {
						String s = "";
						DataInputStream input = new DataInputStream(
								new FileInputStream("A11.txt")); // 讀檔
						while (input.available() != 0) {
							s += (char) (input.readByte() - key); // 解碼、串接文字
						}
						plainText.setText(s); // 顯示
						input.close();
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		A11 a11 = new A11();
	}

}
