package ce1002.a11.s102502536;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class MyFrame extends JFrame {

	private JPanel panel;
	private JTextField tKey;
	private JButton decrypt;
	private JLabel Content;
	private JLabel lKey;
	private JLabel lContent;
	
	
	public MyFrame() {
		setTitle("A11-102502536");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 320, 190);
	    
		panel = new JPanel();
	    panel.setBounds(0, 0, 300, 170);
	    panel.setLayout(null);
 
		Content = new JLabel();
		Content.setBounds(80, 30, 215, 145);
		panel.add(Content);
 
		tKey = new JTextField();
		tKey.setBounds(80, 5, 100, 20);
		panel.add(tKey);
 
		decrypt = new JButton("Decrypt");
		decrypt.setBounds(185, 5, 110, 20);
		decrypt.addActionListener(new DecryptActionListener(this));
		panel.add(decrypt);
		
		lKey = new JLabel("Key:");
		lKey.setBounds(5, 5, 70, 20);
		panel.add(lKey);
		
		lContent = new JLabel("Content:");
		lContent.setBounds(5, 95, 70, 20);
		panel.add(lContent);	
		
		add(panel);
	}
	
	class DecryptActionListener implements ActionListener {
		JFrame frame;
 
		public DecryptActionListener(JFrame frame) {
			this.frame = frame;
		}
 
		@Override
		public void actionPerformed(ActionEvent e) {
			
			int key = Integer.parseInt(tKey.getText());
			
			if (tKey.getText().isEmpty())// the value of key must be non-empty
				showEmptyKeyValueMsg();
			else if (key < 1 || key > 9)// the value of key 
				showOutOfRangeMsg();
			else
			{
				try 
				{
					 
				}
				catch (Exception ex) 
				{
					ex.printStackTrace();
				}
		    }
		}
	
		// dialog
		private void showOutOfRangeMsg() {
			JOptionPane.showMessageDialog(frame, "Out Of Range!");
		}
 
		private void showEmptyKeyValueMsg() {
			JOptionPane.showMessageDialog(frame, "Can��t be empty!!");
		}
		
		public class IoStudy { 
		        String read() throws IOException{ 
		        BufferedReader br=new BufferedReader(new FileReader("A11.txt")); 
		        String s; 
		        StringBuilder sb=new StringBuilder(); 
		        while((s=br.readLine())!=null) 
		            sb.append(s+"\n"); 
		        br.close(); 
		        return sb.toString(); 
		  }
		}
		
	}
	
}
