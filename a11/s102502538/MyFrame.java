package ce1002.a11.s102502538;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.*;

public class MyFrame extends JFrame{

	private JTextField  key;
	private JButton decrypt;
	
	public MyFrame() {
		
		decrypt = new JButton("Decrypt");
		decrypt.addActionListener(new ButtonActionListener(this));

		
		key = new JTextField();
		key.setColumns(10);
		Panel n= new Panel();
		
		n.add (
				new JLabel("KEY: "),BorderLayout.WEST);
		n.add(key,BorderLayout.CENTER);
		n.add(decrypt,BorderLayout.EAST);
		add(n,BorderLayout.NORTH);
		
	}
	 class ButtonActionListener implements ActionListener {
			JFrame frame;
			
			public ButtonActionListener(JFrame frame) {
				this.frame = frame;
				}

			@Override
			public void actionPerformed(ActionEvent e) {

				int k = Integer.parseInt(key.getText());
				 
				if (k<1||k>9)// 1<=key<=9
					showEmptyFileNameMsg();
				else {
					String code;
						try {
							Scanner input = new Scanner(new File ("A11.txt"));
							code = input.nextLine();
							showdecodeMsg(code,k);
							
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}
							}
						}
			private void showdecodeMsg(String code,int k) {
				char[] arr2 = new char [20];
				for ( int i = 0; i < code.length(); i++ ){ 
					char x = code.charAt( i );
					int[] arr = new int [code.length()];
			        arr[i] = (int) x-k;
			        arr2[i]= (char)arr[i];    
                }    
				String decode = String.valueOf(arr2);
				JOptionPane.showMessageDialog(frame,decode);
				
			}

			private void showEmptyFileNameMsg() {
				JOptionPane.showMessageDialog(frame, "Out of Range!!");
			}

		}

}
