package ce1002.a11.s102502551;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle;

public class MyFrame extends JFrame {
	Label label = new Label("Key:");
	Button dButton = new Button("Decrypt");
	TextField textField = new TextField(10);
	Label outLabel = new Label("Result?");
	File file =new File("A11.txt");
	int key;
	
	public MyFrame() {
		// TODO Auto-generated constructor stub
		setLayout(new FlowLayout());
		add(label);
		add(textField);
		add(dButton);
		add(outLabel);
		dButton.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(textField.getText().equals("")){
					JOptionPane.showMessageDialog(MyFrame.this, "Can��t be empty!!");
				}
				
				key = Integer.parseInt(textField.getText());
				
				if(key<1 || key>9){
					JOptionPane.showMessageDialog(MyFrame.this, "Out Of Range!");
					return;
				}
				outLabel.setText(Decrypt());
			}
		});
		
	}
	
	public String Decrypt() {
		Scanner in = null;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Result?";
		}
		
		String hasdecrypt=new String("");
		
		while (in.hasNext()){
			for (char a: in.nextLine().toCharArray()) {
				a-=key;
				hasdecrypt+=a;
			}
		}
		return hasdecrypt;
	}
}
