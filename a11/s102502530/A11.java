package ce1002.a11.s102502530;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class A11 {
	public static void main(String[] args) {
		//set main frame
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 130);
		frame.setTitle("A11-102502530");
		frame.setLayout(new FlowLayout());
		
		//set main panel
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(300, 130));
		panel.setLayout(new FlowLayout());
		
		//key label
		JLabel label = new JLabel("Key:");
		label.setPreferredSize(new Dimension(50, 40));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(label);
		
		//key input
		final JTextField textField = new JTextField();
		textField.setPreferredSize(new Dimension(140, 40));
		panel.add(textField);
		
		//decrypt button
		JButton button = new JButton();
		button.setPreferredSize(new Dimension(80, 40));
		button.setText("Decrypt");
		panel.add(button);
		
		//result label
		label = new JLabel("Result:");
		label.setPreferredSize(new Dimension(50, 40));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		panel.add(label);
		
		//result
		final JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);

		//make result scrollable
		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setPreferredSize(new Dimension(220, 40));
		panel.add(scrollPane);
		
		//set listener of decrypt button
		button.addActionListener(new ActionListener() { 	
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				//no key
				if (textField.getText().isEmpty()) {
					JOptionPane.showMessageDialog(frame, "Can't be empty!!", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				int shift = Integer.valueOf(textField.getText());
				
				//invalid key
				if (shift < 1 || shift > 9) {
					JOptionPane.showMessageDialog(frame, "Out Of Range!", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				//decrypt
				try {
					BufferedReader bufferedReader = new BufferedReader(new FileReader("A11.txt"));
					String string;
					char[] code;
					textArea.setText("");
					while ((string = bufferedReader.readLine()) != null) {
						code = string.toCharArray();
						for (int i = 0; i < code.length ; i++)
							code[i] -= shift;
						string = String.valueOf(code);
						textArea.append(string);
					}
				} catch (Exception e) {}
			}
		});
		
		frame.add(panel);
		
		frame.setVisible(true);
	}
}
