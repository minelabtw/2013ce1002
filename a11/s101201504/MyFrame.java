package ce1002.a11.s101201504;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JLabel label1 = new JLabel("Key: ");
	private JLabel label2 = new JLabel("Result");
	private JTextField text = new JTextField();
	private JButton button = new JButton("Decrypt");

	MyFrame() {
		// set data of the Myframe
		setTitle("A11-101201504");
		setSize(600, 200);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true); // set the location
		label1.setBounds(0, 20, 100, 35);
		label2.setBounds(0, 100, 500, 35);
		text.setBounds(100, 20, 200, 50);
		button.setBounds(300, 20, 200, 50);
		text.setText(null); // set the listener
		SaveListener savelistener = new SaveListener();
		button.addActionListener(savelistener); // add the data to the MyFrame
		add(label1);
		add(label2);
		add(text);
		add(button);
	}

	public class SaveListener implements ActionListener // the listener
	{
		public void actionPerformed(ActionEvent e) {

			try {
				if (text.getText().equals("")) // empty
				{

					throw new NullPointerException();
				} else { // judge the number of the text
					int number = Integer.parseInt(text.getText());
					if (number> 0 && number < 10) {

						int key = Integer.parseInt(text.getText());
						// open the file
						FileReader file = new FileReader("A11.txt");
						Scanner input = new Scanner(file);
						label2.setText("");
						// reader the file and transform the data and output to
						// the label2
						while (input.hasNext()) {
							String data = input.next();
							for (int i = 0; i < data.length(); i++) {
								label2.setText(label2.getText()
										+ Decrypt(data.charAt(i), key));
							}
						}

					} else {// the exception happen when the number out of range
						throw new Exception();
					}

				}

			} catch (NullPointerException e1) {// show the message
				JOptionPane.showMessageDialog(new JFrame("�T��"),
						"Can't be empty!!");
			} catch (Exception e2) {// show the message
				JOptionPane
						.showMessageDialog(new JFrame("�T��"), "Out Of Range!");
			}
		}

		public char Decrypt(char a, int key) { // decrpt
			int number = (int) a;
			number = number - key;
			return (char) number;
		}

		public void test(int number) {
			try {

			} catch (Exception e2) {

			}
		}
	}

}
