package ce1002.a11.s102502518;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MyFrame extends JFrame {
	private JButton decrypt = new JButton("Decrypt");
	private JTextField keytext = new JTextField(5);
	private JLabel key = new JLabel("Key:");
	private JLabel result = new JLabel("Result?");

	MyFrame() {
		
		setLayout(null);//frame
		setTitle("A11-102502518");
		setBounds(0,0,220,120);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p1 = new JPanel();//Panel 1
		p1.add(this.key);
		p1.add(this.keytext);
		p1.add(this.decrypt);
		p1.setBounds(-50, 0, 300, 40);

		JPanel p2 = new JPanel();//Panel 2
		p2.add(this.result);
		p2.setBounds(-50, 40, 300, 75);
		
		add(p1);
		add(p2);
		this.decrypt.addActionListener(new ButtonListener(this));
	}

	private class ButtonListener implements ActionListener {
		
		JFrame frame;
		ButtonListener(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent e) {
			if (MyFrame.this.keytext.getText().isEmpty()) {//empty
				empty();
			} 
			else if ((Integer.valueOf(MyFrame.this.keytext.getText()).intValue() < 1) || (Integer.valueOf(MyFrame.this.keytext.getText()).intValue() > 9)) {//out of range
				outofrange();
			} 
			else {
				String fileName = "A11.txt";
				try {
		          DataInputStream input = new DataInputStream(new FileInputStream(fileName));
		          
		          String str = "";
		          while (input.available() > 0)
		          {
		            char c = input.readChar();
		            int integer = Integer.valueOf(c).intValue();
		            int key = Integer.valueOf(MyFrame.this.keytext.getText()).intValue();
		            
		            c = (char)(integer - key);
		            str = str + c;
		          }
		          MyFrame.this.result.setText(str);
		          input.close();
		        }
				catch (FileNotFoundException ee) {//FileNotFound
					ee.printStackTrace();
				} 
				catch (NumberFormatException ee) {//NumberFormat
					ee.printStackTrace();
				} 
				catch (IOException ee) {//IO
					ee.printStackTrace();
				}
			}
		}

		private void empty() {//show "Can't be empty!"
			JOptionPane.showMessageDialog(this.frame, "Can't be empty!");
		}

		private void outofrange() {//show "Out Of Range!"
			JOptionPane.showMessageDialog(this.frame, "Out Of Range!");
		}
	}
}
