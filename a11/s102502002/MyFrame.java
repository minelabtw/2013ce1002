package ce1002.a11.s102502002;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MyFrame extends JFrame implements ActionListener {

	protected JLabel msg = new JLabel();
	protected JButton button = new JButton();
	protected JButton mbutton = new JButton();
	protected JTextField field = new JTextField();
	protected JLabel label = new JLabel();
	protected JLabel mlabel = new JLabel();
	protected JFrame frame = new JFrame();
	protected int [] asc;
	protected int [] ans;
	protected int key = 0;
	protected char [] mar;
	protected String out;
	protected String in;
	
	MyFrame() {
		
		
		BufferedReader reader = null;

		try {
		    File file = new File("A11.txt");
		    reader = new BufferedReader(new FileReader(file));
		    
		    in = reader.readLine();
		    
		    /*while ((in = reader.readLine()) != null) {
		        mar = in.toCharArray();
		    }*/
		} catch (IOException e) {
		    e.printStackTrace();
		}	
		
		setButton();
		setField();
		setLabel();
		setMessage();
		setTitle("A11-102502002");
		setVisible(true);
		setLayout(null);
		setBounds(300,200,450,200);
		//ChaToASC();
		//ASCToStr();
		add(button);
		add(field);
		add(label);
		add(msg);
		
	}
	
	private void setButton(){ // set button
		button.setText("Decrypt");
		button.setBounds(302, 20, 105, 45);
		button.setVisible(true);
		button.setLayout(null);
		button.addActionListener(this);
	}
	
	private void setField(){ // set text field
		field.setBounds(70, 20, 230, 45);
		field.setVisible(true);
		field.setLayout(null);
	}
	
	private void setLabel(){ // set label
		label.setVisible(true);
		label.setLayout(null);
		label.setText("Key:");
		label.setBounds(20,20,70,45);
	}
	
	private void setFrame(){ // set message frame
		frame.setTitle("Message");
		frame.setVisible(true);
		frame.setLayout(null);
		frame.setBounds(500,350,350,250);
		frame.add(mlabel);
		frame.add(mbutton);
	}
	
	private void setMessage(){// set message
		msg.setVisible(true);
		msg.setLayout(null);
		msg.setText("Result?");
		msg.setBounds(180,80,70,45);
	}
	
	/*private void ChaToASC(){
		int i=0;
		while(mar[i]!=null){
			asc[i] = mar[i];
			i++;
		}
	}
	
	private void ASCToStr(){ // change ASCII code to String
		int i=0;
		while(asc[i]!=null){
		ans[i] = asc[i] + key;
		out = new Character ((char) ans[i]).toString();
		i++;
		}
	}*/
	
	@Override
	public void actionPerformed(ActionEvent a) {
		// TODO Auto-generated method stub
		try{
			String strf = field.getText();
			key = Integer.parseInt(strf);;
			if(strf.isEmpty())
				throw new Exception();
			else if(key>9 || key<1){
				throw new Exception();
			}
			else{
				msg.setText(out);
			}
		}
		catch(Exception e) {
			mbutton.setText("�T�w"); // set button on message window
			mbutton.setBounds(110, 120, 100, 20);
			mbutton.setVisible(true);
			mbutton.setLayout(null);
			mbutton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					SwingUtilities.windowForComponent(mbutton).dispose(); // close window
				}
			});
			mlabel.setVisible(true);
			mlabel.setLayout(null);
			mlabel.setBounds(110, 90, 300, 20);
			if(key>9||key<0)
				mlabel.setText("Out of Range!");
			else
				mlabel.setText("Can't be empty!");
			setFrame();
		}
	}
}
