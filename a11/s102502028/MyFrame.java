package ce1002.a11.s102502028;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class MyFrame extends JFrame {

	JLabel key = new JLabel("key:");  //宣告與建立物件
	JTextField jtf = new JTextField();
	JButton decrypt = new JButton("Decrypt");
	JLabel result = new JLabel("Result?");
	char[] buffer = new char[1000];
	char[] decode ;
	int length = 0;
	int change = 0 ;

	MyFrame() {
		setTitle("A11-102502028"); // 設定框架
		setSize(300, 180);
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		key.setBounds(5, 10, 40, 20);  //設定元件位置
		jtf.setBounds(35, 9, 120, 20);
		decrypt.setBounds(170, 6, 100, 30);
		result.setBounds(70, 15, 230, 90);
		add(key);  //加入元件到框架中
		add(jtf);
		add(decrypt);
		add(result);

		decrypt.addActionListener(new decryptListener()); // 按鍵事件

		setVisible(true);
	}

	class decryptListener implements ActionListener {

		public void actionPerformed(ActionEvent a) {
			// TODO Auto-generated method stub
			try{
				if (jtf.getText().isEmpty())  //key不為空
			{
				JOptionPane.showMessageDialog(null, "Can’t be empty!!", "A11", JOptionPane.ERROR_MESSAGE);
			}
			else if (Integer.parseInt(jtf.getText()) < 1 || Integer.parseInt(jtf.getText()) > 9)  //key不符合規定
			{
				JOptionPane.showMessageDialog(null, "Out Of Range!", "A11", JOptionPane.ERROR_MESSAGE);
			}
			else if (Integer.parseInt(jtf.getText()) >= 1 && Integer.parseInt(jtf.getText()) <= 9)
			{            //解密
				try{
					FileReader fr = new FileReader("A11.txt") ;
					length = fr.read(buffer) ;
					//System.out.print(length);
					for(int i = 0 ; i < length ; i++)
					{
						change = (int)buffer[i] - Integer.parseInt(jtf.getText()) ;
						buffer[i] = (char)change ;
					}				
					fr.close();
					
					decode = new char[length] ;
					for (int j = 0 ; j < length ; j++)
					{
						decode[j] = buffer[j] ;
					}
					result.setText(String.valueOf(decode));
					}			
				catch (Exception ex)
				{					
				}
			}
			}
			catch (Exception ex)
			{
				JOptionPane.showMessageDialog(null, "Can’t be other character except integer!!");
			}
		
				
			}			
		}
	}


