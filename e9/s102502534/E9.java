package ce1002.e9.s102502534;

public class E9 {
	public static void main(String[] args) {
		// 第一種情形
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try {
			if (check("ce1234", "minelab")) {
			}
		} catch (Exception e) {
		}
		// 第二種情形
		System.out.println("test2 : Account = null , Password = minelab");
		try {
			if (check(null, "minelab")) {
			}
		} catch (Exception e) {
		}
		// 第三種情形
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try {
			if (check("ce1002", "minelab")) {
			}
		} catch (Exception e) {
		}
	}

	// function
	public static boolean check(String account, String password)
			throws Exception {
		if (account == "ce1234" && password == "minelab") {
			System.out.println("Failed");
			return false;
		} else if (account == null && password == "minelab") {
			System.out.println("Account or password can't not empty");
			//此情形為空所以要throw new Exception
			throw new Exception("Account or password can't not empty");
		} else if (account == "ce1002" && password == "minelab") {
			System.out.println("Succeed");
			return true;
		}
		return false;
	}
}
