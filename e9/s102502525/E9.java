package ce1002.e9.s102502525;

public class E9 {
	
	
	public static boolean check(String account , String password) throws Exception
	{
		if (account == "ce1002" && password == "minelab")
			return true;
		if (account == null || password == null)
			throw new Exception("Account or password can’t not empty");
		else
			return false;
		
	}
	/*
	 如果輸入的帳號密碼正確就回傳true，錯誤就回傳false
	 如果其中有一個是null的話就進入Exception
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try
		{
			boolean test = check("ce1234","minelab");
			if(test == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		System.out.println("Test2 : Account = null , Password = minelab");
		try
		{
			boolean test = check(null,"minelab");
			if(test == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try
		{
			boolean test = check("ce1002","minelab");
			if(test == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		/*
		 把判斷條件送入check中，讓結果回傳到test並輸出
		 若遇到Exception，則輸出throw來的訊息
		 */

	}

}
