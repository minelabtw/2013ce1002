package ce1002.e9.s101502205;

public class E9 {
	public static void main(String[] args) {

		// testing data
		String testAcc[] = {"ce1234", null, "ce1002"};

		for (int i=0; i<3; i++) {
			System.out.println("Test" + (i+1) + " : Account = " + testAcc[i] + " , Password = minelab");

			try{
				// check the given account and password
				if(check(testAcc[i], "minelab")) {
					// Succeed
					System.out.println("Succeed");
				}else{
					// failed
					System.out.println("Failed");
				}
			}catch (Exception e) {
				// exception handling
				System.out.println(e.getMessage());
			}
		}
	}


	// Check function
	public static boolean check(String account, String password) throws Exception {

		if (account==null) {
			// Error, throw exception
			throw new Exception("Account or password can't be empty");
		}
		
		// test account & password
		if (account=="ce1002" && password=="minelab") {
			return true;
		}
		
		// failed the testing
		return false;
	}
}