package ce1002.e9.s102502504;
import java.util.Scanner;
import java.lang.ArithmeticException;


public class E9 
{
	public static boolean check(String account , String password) 
	{	
		if(account=="ce1002" && password=="minelab")
		{
			return true;
		}
		else if(account==null || password==null)
		{
			throw new ArithmeticException("Account or password can��t not empty");
		}
		
		else
			return false;
		
		
	}

	public static void main(String[] args)
	{
	
		try
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			if(check("ce1234","minelab"))
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch(ArithmeticException ex)
		{
			System.out.println(ex.getMessage());
		}
		
		try
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			if(check(null,"minelab"))
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch(ArithmeticException ex)
		{
			System.out.println(ex.getMessage());
		}
		
		
		try
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			if(check("ce1002","minelab"))
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch(ArithmeticException ex)
		{
			System.out.println(ex.getMessage());
		}
		
		
	}

}
