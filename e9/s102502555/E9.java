package ce1002.e9.s102502555;

public class E9 {

	public static void main(String[] args) {
		//檢查第一種帳密
		try{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234", "minelab");
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		
		//檢查第二腫脹密
		try{
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null, "minelab");
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}

		//檢查第三腫脹密
		try{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002", "minelab");
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}
	}

	public static boolean check(String account , String password) throws Exception {
		if(account == null || password == null){  //丟出exception
			throw new Exception("Account or password can’t not empty");
		}else if (account != "ce1002" || password != "minelab"){
			System.out.println("Failed");
			return false;
		}else {
			System.out.println("Succeed");
			return true;
		}
	}
}
