package ce1002.e9.s102502546;

public class E9 {//這是E9

	public static boolean check(String account, String password)
			throws Exception {
		if (account == null || password == null) {//如果帳或密空白 則丟東西出去
			throw new Exception("Account or password can't not empty");
		}
		if (account != "ce1002" || password != "minelab") {
			System.out.println("Failed");
			return false;
		} else {
			System.out.println("Succeed");
			return true;
		}
	}

	public static void main(String[] args) {

		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234", "minelab");
		} catch (Exception ex) {//接東西
			ex.getMessage();
		}
		try {
			check(null, "minelab");
		} catch (Exception ex) {//接東西
			System.out.println("Test2 : Account = null , Password = minelab");
			System.out.println(ex.getMessage());
		}
		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002", "minelab");
		} catch (Exception ex) {//接東西
			ex.getMessage();
		}
	}
}
