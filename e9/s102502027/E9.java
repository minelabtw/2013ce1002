package ce1002.e9.s102502027;

import java.util.Scanner;

public class E9 {

	public static boolean check(String account, String password)
			throws Exception {
		if (account.equals("ce1002") && password.equals("minelab"))
			return true;
		if (account.equals("") ||  password.equals(""))
			throw new Exception("Account or password can't not empty");
		else
			return false;
	}

	public static void main(String[] args) {
		boolean a;
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			try {
				a = E9.check("ce1234", "minelab");
				if (a == true)
					System.out.println("Succeed");
				else
					System.out.println("Failed");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());

			}
		
			System.out.println("Test2 : Account = null , Password = minelab");
			try {
				a = E9.check("", "minelab");
				if (a == true)
					System.out.println("Succeed");
				else
					System.out.println("Failed");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());

			}
		
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			try {
				a = E9.check("ce1002", "minelab");
				if (a == true)
					System.out.println("Succeed");
				else
					System.out.println("Failed");
			} catch (Exception ex) {
				System.out.println(ex.getMessage());

			}
		
	}
}
