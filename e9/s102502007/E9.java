package ce1002.e9.s102502007;
public class E9 {
	public E9()
	{
		boolean situation;
		//set a boolean variable to know whether the check result is true or false
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try {
			situation = E9.check("ce1234", "minelab");
			if (situation == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
		finally	{
			
		}
	
		System.out.println("Test2 : Account = null , Password = minelab");
		try {
			situation = E9.check("", "minelab");
			if (situation == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
		finally	{
			
		}
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try {
			situation = E9.check("ce1002", "minelab");
			if (situation == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
		finally	{
			
		}

	}
	/*
	 * set up a boolean method to examine if there is an exception
	 */
	public static boolean check(String ac, String pw)
			throws Exception {
		//when a void string is detected , then throw a exception object with message
		if (ac.equals("") ||  pw.equals(""))
			throw new Exception("Account or password can't not empty");
		else if (ac.equals("ce1002") && pw.equals("minelab"))
			return true;
			return false;
			//none of ac and pw is correct and then return false
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E9();

	}

}
