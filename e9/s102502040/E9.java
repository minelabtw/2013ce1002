package ce1002.e9.s102502040;

public class E9 {
	
	public static boolean check(String account , String password) throws Exception {
		if(account == null || password == null){//帳密為空,輸出字元
			throw new Exception("Account or password can't not empty");
		}
		if(account == "ce1002" && password == "minelab"){//用函式定義帳號與密碼的正確
			System.out.println("Succeed");
			return true;
		}
		else{
			System.out.println("Failed");
			return false;
		}
	}
	public static void main(String[] args) throws Exception {
		String[] account = {"ce1234",null,"ce1002"};//依序定義帳號、密碼
		String[] password = {"minelab","minelab","minelab"};
		for( int i = 0 ; i < 3 ; i++ )
		{
			
			System.out.println("Test"+ (i+1) +" : Account = " + account[i] +" , Password = " + password[i] );
			try
			{
				check(account[i],password[i]);
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());//getMessage()為抓出Exception內所定義之字元
			}
			
		}
	}
}
