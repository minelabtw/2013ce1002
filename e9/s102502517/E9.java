package ce1002.e9.s102502517;

public class E9 {

	public static void main(String[] args) {
		getmsg(1,"ce1234","minelab");
		getmsg(2,null,"minelab");
		getmsg(3,"ce1002","minelab");
	}
	
	public static void getmsg(int num,String account,String password)
	{
		System.out.println("Test" + num + " : Account = " + account + " , Password = " + password);
		try
		{
			if(check(account,password)) //若為true
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		}
		catch(Exception e) //例外處理
		{
			System.out.println(e.getMessage()); //輸出Account or password can't not empty
		}
	}
	
	public static boolean check(String account , String password) throws Exception //若有account及password皆為null則throws Exception
	{
		if(account==null || password==null)
			throw new Exception("Account or password can't not empty");
		if(account=="ce1002" && password=="minelab")
			return true;
		else
			return false;
	}
}
