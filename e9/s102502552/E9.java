package ce1002.e9.s102502552;

public class E9 {

	public static void main(String[] args) {
		
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try{
			check("ce1234","minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}//測試一,將顯示失敗
		
		System.out.println("Test2 : Account = null , Password = minelab");
		try{
			check(null,"minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}//測試二,屬於例外
		
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try{
			check("ce1002","minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}//測試三,成功

	}
	
	public static boolean check(String account,String password) throws Exception
	{
		
		if(account == "ce1002" && password == "minelab")
		{
			System.out.println("Suceed");
			return true;
		}//帳密正確,輸出成功
		else if(account == null || password == null)
		{
			throw new Exception("Account or password can't not empty");
		}//有一個為空,拋出例外
		else
		{
			System.out.println("Failed");
			return false;
		}//帳密錯誤,輸出失敗
		
	}

}
