package ce1002.e9.s102502536;

public class E9 {
	
	public static boolean check(String account , String password) throws Exception {  // throw exception
		
		boolean result = false;
		String a = "ce1002";
		String p = "minelab";
		
		if (account == null || password == null)
			throw new Exception("Account or password can't not empty");
		else if (account == a && password == p)
			result = true;
		else
			result = false;
		
		return result;
	}
	


	public static void main(String[] args) {
		
		String r;
         
        for (int i = 0; i < 3; i++)  // test the function and catch the exception , output "failed" or "succeed" according of boolean , output the message when catching the exception 
        {
        	if (i == 0)
        	{
        		try
        		{
        			System.out.println("Test1 : Account = ce1234 , Password = minelab");
        			boolean result = check("ce1234" , "minelab");
        			if (result == false)
        				r = "Failed";
        			else 
        				r = "Succeed";
        			System.out.println(r);
        		}		
        		catch (Exception ex)
        		{
        			System.out.println(ex.getMessage());
        		}
        	}
        	else if (i == 1)
        	{
        		try
        		{
  
        			System.out.println("Test2 : Account = null , Password = minelab");
        			boolean result = check(null , "minelab");
        			if (result == false)
        				r = "Failed";
        			else 
        				r = "Succeed";
        			System.out.println(r);
        		}		
        		catch (Exception ex)
        		{
        			System.out.println(ex.getMessage());
        		}
        	}
        	else
        	{
        		try
        		{
        			System.out.println("Test3 : Account = ce1234 , Password = minelab");
        			boolean result = check("ce1002" , "minelab");
        			if (result == false)
        				r = "Failed";
        			else 
        				r = "Succeed";
        			System.out.println(r);

        		}		
        		catch (Exception ex)
        		{
        			System.out.println(ex.getMessage());
        		}
        	}
        	

        }
		 
	}
	
}
