package ce1002.e9.s102502008;

public class E9 
{
	public static void main(String args[])
	{
		try{
			System.out.println("Test1 : Account = ce1234 , Password = minelab") ;
			if(check("ce1234" , "minelab"))
				System.out.println("Succeed") ;
			else
				System.out.println("Failed");
		}catch(Exception e)
		{
			System.out.println(e.getMessage()) ;
		}
		//test 1
		try{
			System.out.println("Test2 : Account = null , Password = minelab") ;
			if(check(null , "minelab")) 
				System.out.println("Succeed") ;
			else
				System.out.println("Failed");
		}catch(Exception e)
		{
			System.out.println(e.getMessage()) ;
		}
		//test 2
		try{
			System.out.println("Test3 : Account = ce1002 , Password = minelab") ;
			if(check("ce1002" , "minelab"))
				System.out.println("Succeed") ;
			else
				System.out.println("Failed");
		}catch(Exception e)
		{
			System.out.println(e.getMessage()) ;
		}
		//test 3
	}
	public static boolean check(String account , String password) throws Exception
	{
		if(account=="ce1002"&&password=="minelab")
			return true ;
		else if(account==null || password== null)
			throw new Exception("Account or password can��t not empty");
		else
			return false ;
	}
}
