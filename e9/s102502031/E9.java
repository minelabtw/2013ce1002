package ce1002.e9.s102502031;

public class E9 {
	// existed accounts and correspond passwords
	final static String[] accounts = { "ce1002" };
	final static String[] passwords = { "minelab" };

	public static void main(String[] args) {
		// three test data
		final String[] testAccounts = { "ce1234", "null", "ce1002" };
		final String[] testPasswords = { "minelab", "minelab", "minelab" };

		for (int i = 0; i < testAccounts.length; i++) {
			System.out.print("Test" + (i + 1) + " : ");
			System.out.print("Account = " + testAccounts[i] + " , ");
			System.out.println("Password = " + testPasswords[i]);
			try {
				if (testAccounts[i].equals("null") || testPasswords[i].equals("null")) {
					throw new Exception(); // throw exception as test data is null which means no data in it
				}
				if (check(testAccounts[i], testPasswords[i])) {
					System.out.println("Succeed");
				}
				else {
					System.out.println("Failed");
				}
			} catch (Exception exception) {
				System.out.println("Account or password can't not empty");
			}
		}
	}

	public static boolean check(String account, String password) {
		for (int i = 0; i < accounts.length; i++) {
			if (account.equals(accounts[i]) && password.equals(passwords[i])) { 
				return true; // as we found at least one correct account with correspond password, return true
			}
		}
		return false; // if we found no correct account with correspond password, return false
	}
}
