package ce1002.e9.s101201524;

public class E9 {
	public static void main(String[] args) throws Exception{
		//check 3 test
		check("ce1234" , "minelab");
		check(null , "minelab");
		check("ce1002" , "minelab");
	}
	public static boolean check(String account , String password) throws Exception{
		i++;// add one test
		//show testing data
		System.out.println("Test" + i + " : Account = " + account + " , Password = " + password);
		try{
			//check the exception
			if(account == null || password == null)
				throw new Exception("Account or password can��t not empty");
			//check the correctness
			if(account != "ce1002" || password != "minelab"){
				System.out.println("Failed");
				return false;
			}
			else{
				System.out.println("Succeed");
				return true;
			}
		}
		catch(Exception e){
			//show that the exception happens
			System.out.println(e.getMessage());
			return false;
		}
	}
	static int i = 0; //count testing times
}