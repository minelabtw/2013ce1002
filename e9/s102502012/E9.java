package ce1002.e9.s102502012;

public class E9 {
	
	public static boolean check(String account, String password) throws Exception{
		
		if(account == null || password == null)
			throw new Exception("Account or password can not be empty");
			
		if(account == "ce1002" && password == "minelab")
			return true;
		else
			return false;
	}

	public static void main(String[] args) {
		String test[][] = {{"ce1234", "minelab"}, {null, "minelab"}, {"ce1002", "minelab"}};
		for(int i = 0; i < test.length; i++){
			System.out.println("Test" + (i + 1)  + " : Account = " + test[i][0] + " , Password = " + test[i][1]);
			try{
				if(check(test[i][0],test[i][1]))
					System.out.println("Succeed");
				else
					System.out.println("Failed");
			}
			catch(Exception e){
				System.out.println(e.getMessage());				
			}
		}
	}

}
