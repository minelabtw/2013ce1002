package ce1002.e9.s102502554;

public class E9 {
	
	public static void main (String [] args){
		try{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
		    check ("ce1234","minelab");
		}//test for  Account = ce1234 , Password = minelab"
		catch(Exception e){
			System.out.println(e.getMessage());
		}//catch the Exception
		try{
			System.out.println("Test2 : Account = null , Password = minelab");
		    check (null,"minelab");
		}//test for Account = null , Password = minelab"
		catch(Exception e){
			System.out.println(e.getMessage());
		}//catch the Exception
		try{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
		    check ("ce1002","minelab");
		}//test for Account = ce1002 , Password = minelab"
		catch(Exception e){
			System.out.println(e.getMessage());
		}//catch the Exception
	}
	
	 public static boolean check(String account , String password) throws Exception{
		 
		 if(account == null || password == null){
			 throw new Exception ("Account or password can't be empty");
		 }
		 else if (account != "ce1002" || password != "minelab"){
			 throw new Exception ("Failed");
	     }
		 else{
			 throw new Exception ("Succeed");
		 }//set the Exception for all situation 
    }

}
