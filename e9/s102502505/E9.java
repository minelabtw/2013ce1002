package ce1002.e9.s102502505;

public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String Array1[] = {"ce1234",null,"ce1002"};//建立帳號陣列
		String Array2[] = {"minelab","minelab","minelab"};//建立密碼陣列

		for(int i=0;i<3;i++){//利用迴圈跑三次
			
			System.out.println("Test"+(i+1)+" : Account = "+Array1[i]+" , Password = "+Array2[i]);//輸出字串
			
			try{//判斷狀況為true或false，再把例外丟入catch
				if(check(Array1[i],Array2[i])==true)
				{
					System.out.println("Succeed");
				}
				else if(check(Array1[i],Array2[i])==false&&Array1[i]==null)
				{
					throw new Exception();//將例外事件丟入exception中
				}
				else
				{
					System.out.println("Failed");
				}
			}
			
			catch(Exception ex){//例外時輸出下列字串
				System.out.println("Account or password can't not empty");
			}
		
		}
		
	}
	
	 public static boolean check(String acc , String pas) {//檢查帳號密碼
		 
		 	if(acc=="ce1002"&&pas=="minelab")
		 	{
		 		return true;
		 	}
			else
			{
				return false;
			}
	 }

}
