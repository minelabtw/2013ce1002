package ce1002.e9.s992001026;

public class E9 {
	// set function check has exception
	public static boolean check(String account , String password) throws Exception{
		if (account.equals("ce1002") && password.equals("minelab")){
			return true ;
		}else if (account.equals("") || password.equals("")){
			throw new Exception ("Account or password can��t be empty");
		}else {
			return false ;
		}
	}
	//main
	public static void main(String[] args) {
		
		// set array of accounts and passwords
		String accounts [] ={"ce1234","" , "ce1002" };
		String passwords [] = {"minelab","minelab","minelab"};
		
		// print out
		for (int i = 0 ; i < 3 ; ++i ){
			System.out.println ("Test"+ (i+1) +" : Account = "+ (accounts[i].equals("") ? "null": accounts[i] )+" , Password = " + passwords[i]);
		
			try {
				boolean bo = check( accounts[i],passwords[i]);
				if (bo) System.out.println("Succeed");
				else System.out.println("Failed");
			}catch( Exception e){
				System.out.println(e.getMessage());
				
			}		
		}	
	}

}
