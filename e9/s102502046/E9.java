package ce1002.e9.s102502046;

public class E9 {

	static int count = 1;
	// check
	public static boolean check(String account , String password) throws Exception
	{
		System.out.println("Test" + (count++) + " : Account = " + account + " , Password = " + password);
		if (account == null || password == null)
			throw new Exception("Account or password can't not empty");
		return account == "ce1002" && password == "minelab";
	}
	// login
	public void login(String account, String password)
	{
		try
		{
			if (check(account, password))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	// construct
	E9()
	{
		login("ce1234", "minelab");
		login(null, "minelab");
		login("ce1002", "minelab");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E9();
	}

}
