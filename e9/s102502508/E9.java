package ce1002.e9.s102502508;
public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		 try //利用try和catch去判斷帳號和密碼的輸入是否正確
		 {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			boolean a=check("ce1234","minelab");
			if(a) 
			{
				System.out.println("Succed");
			}
			else
			{
				System.out.println("Failed");
			}
			
         }
		 catch(Exception e)
		 {
			 System.out.println(e.getMessage());
		 }
		 try
		 {
			 
			System.out.println("Test2 : Account = null , Password = minelab");
			
		    Boolean b=check(null,"minelab");//若此行成立,則跳往catch,不再執行下一行
			if(b)
			{	
				System.out.println("Succed");
			}
			else
			{
				System.out.println("Failed");
			}
		 }
		 catch(Exception e)
		 {
			 System.out.println(e.getMessage());
		 }
		 try
		 {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			Boolean c=check("ce1002","minelab");
			if(c) 
			{
				System.out.println("Succed");
			}
			else
			{
				System.out.println("Failed");
			}
		 } catch(Exception e)
		 {
			 System.out.println(e.getMessage());
		 }
		 
	
	}
		
		
		
		
		 public static boolean check(String account , String password) throws Exception  //利用判別式迴傳真假值以便主程式能輸出對應的顯示
		 {
			
				 if(account=="ce1002" && password=="minelab")
				 {
					 
					 return true;
				 }
				 else if(account==null || password==null)
				  {
					  throw new Exception("Account or password can't not empty");
					  
				  }
				 else 
				 {   
					 return false;
				 }
			
			
			
			
		   
		 }
	}


