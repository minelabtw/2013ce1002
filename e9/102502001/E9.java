package ce1002.e9.s102502001;

public class E9 {

	public static void main(String[] args) {
		String acc = null;
		String pw = null;
		try {
			acc = "ce1234";
			pw = "minelab";
			System.out.println("Test1 : Account = ce1234 , Password = minelab");

			check(acc, pw);

		}

		catch (Exception e) {
			System.out.println("Account or password can��t not empty");
		}
		try {

			acc = null;
			pw = "minelab";
			System.out.println("Test2 : Account = null , Password = minelab");

			check(acc, pw);

		}

		catch (Exception e) {
			System.out.println("Account or password can��t not empty");
		}
		try {

			acc = "ce1002";
			pw = "minelab";
			System.out.println("Test3 : Account = ce1002 , Password = minelab");

			check(acc, pw);

		}

		catch (Exception e) {
			System.out.println("Account or password can��t not empty");
		}
	}

	public static boolean check(String account, String password)
			throws Exception {
		if (account == "ce1002" && password == "minelab") {
			System.out.println("Succeed");
			return true;
		}

		else if (account == null || password == null) {
			throw new Exception();
		} else {
			System.out.println("Failed");
			return false;
		}
	}
}
