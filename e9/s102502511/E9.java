package ce1002.e9.s102502511;

public class E9 {

	public static void main(String[] args) {
		String [] account = {"ce1234" , null , "ce1002"}; //記錄所有的帳號
		String [] password = {"minelab" , "minelab" , "minelab"}; //記錄所有的密碼
		for(int i = 0 ; i < 3 ; i++){ //重複三次try
			try{			
				System.out.println("Test" + (i+1) + " : Account = " + account[i] + " , Password = " + password[i]); //輸出內容
				check(account[i],password[i]); //檢測
				if(check(account[i],password[i])){ //要是他是true輸出下列內容
					System.out.println("Succeed");
				}else{ //false輸出下列內容
					System.out.println("Failed");
				}
			
			}catch(Exception ex){ //如果是例外
				System.out.println(ex.getMessage()); //輸出例外的message
			}
		}
	}
	
	 public static boolean check(String account , String password) throws Exception{ //定義例外
		 if(account != null && password != null){ //若都不是空的
			 if(account == "ce1002" && password == "minelab"){ //判斷是否正確 正確回傳true 錯的回傳false
				 return true;
			 }else{
				 return false; 
			 }
		 }else{ //其餘給定成例外
			 throw new Exception("Account or password can't not empty"); //定義例外的內容
		 }
	 }
}
