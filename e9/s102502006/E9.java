package ce1002.e9.s102502006;

public class E9 {
	
	public static boolean check(String account, String password) throws Exception
	{
		if(account == "ce1002" && password == "minelab"){ // right 
			System.out.println("Succeed");
			return true;
		}
		else if(account == null || password == null){ // null
			throw new Exception("Account or password can��t not empty");
		}
		else // wrong
		{
			System.out.println("Failed");
			return false;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int i=0; i<3; i++){
			try
			{
				if(i==0){
					System.out.println("Test1 : Account = ce1234 , Password = minelab");
					check("ce1234","minelab");
				}
				if(i==1){
					System.out.println("Test2 : Account = null , Password = minelab");
					check(null,"minelab");
				}
				if(i==2){
					System.out.println("Test3 : Account = ce1002 , Password = minelab");
					check("ce1002","minelab");
				}
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}
}
