package ce1002.e9.s101201519;

public class E9 {
	public static boolean check(String account,String password) throws Exception
	{
		if (account == "ce1002" && password == "minelab")
			return true;//成功
		else if (account == null || password == null)
			throw new Exception("Account or password can't not empty");//錯誤的輸入
		else
			return false;//失敗
	}
	
	public static void main(String[] args)
	{
		String account[]=new String[3];
		String password[]=new String[3];
		
		account[0]="ce1234";
		account[1]=null;
		account[2]="ce1002";
		password[0]="minelab";
		password[1]="minelab";
		password[2]="minelab";//輸入資料
		
		for(int i=1;i<=3;i++)
		{
			System.out.println("Test"+i+" : Account = "+account[i-1]+" , Password = "+password[i-1]);
			try
			{
				if (check(account[i-1],password[i-1]))
					System.out.println("Succeed");
				else
					System.out.println("Failed");
			}
			catch(Exception e)
			{
				System.out.println(e.getMessage());
			}
		}
	}

}
