package ce1002.e9.s102502050;
import java.util.Scanner;
public class E9 {
	
	//檢查帳號密碼
	 public static boolean check(String account , String password) throws Exception
	 {
		 if(account == null || password == null)
			 throw new Exception("Account or password can't not empty");
		 else if(account == "ce1002" && password == "minelab")
			 return true;
		 else 
			 return false;
	 }

	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String input;//輸入的字串
		
		//第一次輸入   ce1234  minelab
		input = scanner.next();
		try
		{
			if(check("ce1234" , "minelab")==true)
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		//第二次輸入  null  minelab
		input = scanner.next();
		try
		{
			if(check(null , "minelab")==true)
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		//第三次輸入 ce1002  minelab
		input = scanner.next();
		try
		{
			if(check("ce1002" , "minelab")==true)
			{
				System.out.println("Succeed");
			}
			else
			{
				System.out.println("Failed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}



	}

}
