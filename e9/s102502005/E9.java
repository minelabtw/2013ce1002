package ce1002.e9.s102502005;

public class E9 {

	public static void main(String[] args) {//執行三次check。
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		check("ce1234", "minelab");
		System.out.println("Test2 : Account = null , Password = minelab");
		check(null, "minelab");
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		check("ce1002", "minelab");

	}

	public static boolean check(String account , String password){
		try{
			if(account == "ce1002" && password=="minelab"){
				System.out.println("Succeed");
				return true ;
			}
			else if(account==null || password == null){
				throw new ArithmeticException("Account or password can't not empty");
			}
			else{
				System.out.println("Failed");
				return false;
			}
		}
		catch(ArithmeticException ex){
			System.out.println(ex.getMessage());
			return false;//不return一個boolean的話，check函式有可能無法結束。
		}
		
	}
}
