package ce1002.s102502514.e9;
import java.util.Scanner;

public class E9 {

	public static void main(String[] args) {
		try{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			getMessage("ce1234" , "minelab");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());  //如果為exception則取得"Account or password can’t not empty"的資訊
		}
		try{
			System.out.println("Test2 : Account = null , Password = minelab");
			getMessage(null , "minelab");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());  //如果為exception則取得"Account or password can’t not empty"的資訊
		}
		try{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			getMessage("ce1002" , "minelab");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());  //如果為exception則取得"Account or password can’t not empty"的資訊
		}
		

	}
	public static boolean check(String account , String password) throws Exception {
		if (account == "ce1002" && password == "minelab")  //正確帳號密碼
			return true;
		else if (account == null || password == null)  //錯誤帳號密碼
			throw new Exception("Account or password can’t not empty");
		else
			return false;
	}
	public static void getMessage(String account , String password) throws Exception{
		if(check(account , password)){  //呼叫check函式檢查是true、false或exception
			System.out.println("Succeed");
		}
		else{
			System.out.println("Failed");
		}
	}

}
