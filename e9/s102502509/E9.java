package ce1002.e9.s102502509;

public class E9 
{

	public static void main(String[] args) 
	{
		String [] account = {"ce1234", "", "ce1002"}; 
		String [] password = {"minelab", "minelab", "minelab"};//利用陣列儲存測值
		
		for(int i = 0; i < 3; i++)//利用for執行三次
		{
				try//執行try裡面的動作
				{
					System.out.println("Test " + (i+1) + ": Account = " + account[i] + ", Password = " + password[i] );
					check(account[i],password[i]);
					
					if(check(account[i],password[i]))
					{
						System.out.println("Succeed");
					}
					else
					{
						System.out.println("Failed");
					}					
				} 
				catch(Exception e)//如果發生例外，則執行裡面的動作
				{
					System.out.println(e);
				}
		}
	}
	
	public static boolean check(String account , String password) throws Exception
	{
		String str1 = "ce1002";
		String str2 = "minelab";
		String str3 = "";
		
		if(account.equals(str1) && password.equals(str2))//帳號和答案相同時
		{
			return true;//System.out.println("Succeed");			
		}
		
		else if(account.equals(str3) || password.equals(str3))//其中一個為空值
		{
			throw new Exception("Account or password can’t not empty");
		}
		
		else 
		{
			return false;
		}
	}
}
