package ce1002.e9.s102502537;

public class E9 {
	public static boolean check(String account , String password) throws Exception
	{
		if(account=="ce1002" && password=="minelab")
		{
			System.out.print("Succeed");
			return true;//題目規定的
		}
		else if(account==null || password==null)
		{
			throw new Exception("Account or password can’t not empty");
		}
		else
		{
			System.out.println("Failed");
			return false;
		}
	}

	
	public static void main(String[] args) 
	{
		try
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234","minelab");
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null,"minelab");//null!=  "   "
		}
		catch(Exception e1)
		{
			System.out.print(e1.getMessage());
		}
		
		try
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002","minelab");
		}
		catch(Exception e2)
		{
			System.out.print(e2.getMessage());
		}
		
	}

}
