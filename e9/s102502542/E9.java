package ce1002.e9.s102502542;
public class E9 
{
	public static boolean check(String account, String password) throws Exception 
	{
		if (account == "ce1234" && password == "minelab") 
		{
			System.out.println("Failed");
			return false;
		} 
		else if (account == "ce1002" && password == "minelab") 
		{
			throw new Exception("Succeed");
		}
		else 
		{
			throw new Exception("Account or password can't not empty");
		}
	}
	public static void main(String[] args) 
	{
		try 
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab ");
			check("ce1234", "minelab");
			System.out.println("Test2 : Account = null , Password = minelab ");
			check(null, "minelab");
		} 
		catch (Exception e) 
		{
			System.out.println(e.getMessage());
		}

		try 
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab ");
			check("ce1002", "minelab");
		} 
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}
