package ce1002.e9.s102502526;

public class E9 {
	/**
	 * @param args
	 */
	public static boolean check(String account,String password)throws Exception
    {
        if(account==null || password ==null)
            throw new Exception("Account or password can¡¦t not empty");
        if(account.equals("ce1002") && password.equals("minelab"))  //if the account and the password are correct
            return true;  //return true
        else  //if the account and the password aren't correct
            return false;  //return false
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  System.out.println("Test1 : Account = ce1234 , Password = minelab");  //first test
	        try
	        {
	            boolean islogin = check("ce1234","minelab"); 
	            if(islogin)
	                System.out.println("Succeed");
	            else
	                System.out.println("Failed");
	        }catch(Exception e)
	        {
	            System.out.println(e.getMessage());
	        }
	        System.out.println("Test2 : Account = null , Password = minelab");  //second test
	        try
	        {
	            boolean islogin = check(null,"minelab");
	            if(islogin)
	                System.out.println("Succeed");
	            else
	                System.out.println("Failed");
	        }catch(Exception e)
	        {
	            System.out.println(e.getMessage());
	        }
	        System.out.println("Test3 : Account = ce1002 , Password = minelab");  //third test
	        try
	        {
	            boolean islogin = check("ce1002","minelab");
	            if(islogin)
	                System.out.println("Succeed");
	            else
	                System.out.println("Failed");
	        }catch(Exception e)
	        {
	            System.out.println(e.getMessage());
	        }
	}

}
