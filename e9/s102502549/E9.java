package ce1002.e9.s102502549;

public class E9 {

	//一個叫check的方法，檢查輸入帳密是否正確
	public static boolean check(String account, String password)
			throws Exception {
		if (account == null || password == null) {
			throw new Exception("Account or password can’t not empty");
		} else {
			if (account == "ce1002" && password == "minelab") {
				return true;
			} else {
				return false;
			}
		}
	}

	public static void main(String[] args) {

		//第一次測試
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try {
			System.out.println(check("ce1234", "minelab")?"Succeed":"False");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		//第二次測試
		System.out.println("Test2 : Account = null , Password = minelab");
		try {
			System.out.println(check(null, "minelab")?"Succeed":"False");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		//第三次測試
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try {
			System.out.println(check("ce1002", "minelab")?"Succeed":"False");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
