package ce1002.e9.s102502550;

public class E9 {

	public static void main(String[] args) {
		try {			                                                          //寫死的測試
			String s = check("ce1234" , "minelab")?"Succeed":"Failed";
			System.out.println("Test1 : Account = ce1234 , Password = minelab\n"+s);
		} catch (Exception e) {
			
			System.out.println("Test1 : Account = ce1234 , Password = minelab\n"+e.getMessage());
		}
		
		
		try {																	  //寫死的測試
			String s = check(null , "minelab")?"Succeed":"Failed";
			System.out.println("Test1 : Account = null , Password = minelab\n"+s);
		} catch (Exception e) {
			
			System.out.println("Test1 : Account = null , Password = minelab\n"+e.getMessage());
		}
		
		
		try {																	  //寫死的測試
			String s = check("ce1002" , "minelab")?"Succeed":"Failed";
			System.out.println("Test1 : Account = ce1002 , Password = minelab\n"+s);
		} catch (Exception e) {
			
			System.out.println("Test1 : Account = ce1002 , Password = minelab\n"+e.getMessage());
		}

	}
	public static boolean check(String account , String password) throws Exception{
		
		if(account == null || password == null){
			throw new Exception("Account or password can’t be empty");  	//拋出例外
		}
		
		if(account == "ce1002" && password == "minelab"){                   //檢查
			return true;
		}
		else{
			return false;
		}
	}
}
