package ce1002.e9.s102502037;
import java.awt.*;
import javax.swing.*;
public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try//檢查
		{
			check("ce1234" , "minelab");
		}
		catch(Exception ex)//如果傳回例外
		{
			System.out.println("Account or password can't not empty");
		}
		System.out.println("Test1 : Account = null , Password = minelab");
		try
		{
			check(null , "minelab");
		}
		catch(Exception ex)
		{
			System.out.println("Account or password can't not empty");
		}
		System.out.println("Test1 : Account = ce1002 , Password = minelab");
		try
		{
			check("ce1002" , "minelab");
		}
		catch(Exception ex)
		{
			System.out.println("Account or password can't not empty");
		}
	}
public static boolean check(String account , String password) throws Exception
{
	if(account==null || password == null)//檢查 如果有空的 則回傳例外
	{
		throw new Exception();
	}
	else if(account=="ce1002" && password =="minelab" )
	{
		System.out.println("Succeed");
		return true;
	}
	else
	{
		System.out.println("Failed");
		return false;
	}
	}
}
