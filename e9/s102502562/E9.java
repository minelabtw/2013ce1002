package ce1002.e9.s102502562;

public class E9 {
	public static void main(String[] args) {
		try//第一次測試
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234","minelab");
		}
		catch(Exception e)//取得Exception
		{
			System.out.println(e.getMessage());
		}
		try//第二次測試
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null,"minelab");
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		try//第三次測試
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002","minelab");
		}
		catch(Exception e)
		{
				System.out.println(e.getMessage());
		}
	}
	public static boolean check(String account , String password)throws Exception
	{
		if(account==null || password==null)//當帳號或密碼為空值時跳出Exception
			throw new Exception("Account or password can’t not empty");
		else if(account!="ce1002" || password!="minelab")//當帳號或密碼不正確實回傳Failed
		{
			System.out.println("Failed");
			return false;
		}   
		else//當帳號和密碼都正確時回傳Succeed
		{
			System.out.println("Succeed");
			return true;
		}
	}
}
