package ce1002.e9.s102502049;

public class E9 {

	public static void main(String[] args) {
		
		System.out.println("Test1 : Account = ce1234 , Password = minelab"); // show input
		try{
			check("ce1234", "minelab"); // normal procedure
		}
		catch(Exception ex){ // exception happen
			System.out.println("Account or password can't not empty");
		}
		
		System.out.println("Test2 : Account = null , Password = minelab"); // second test
		try{
			check(null, "minelab");
		}
		catch(Exception ex){
			System.out.println("Account or password can't not empty");
		}
		
		System.out.println("Test3 : Account = ce1002 , Password = minelab"); // third test
		try{
			check("ce1002", "minelab");
		}
		catch(Exception ex){
			System.out.println("Account or password can't not empty");
		}
	}
	
	 public static boolean check(String account , String password) throws Exception{ // check method
		 if(account==null||password==null)
			 throw new Exception(); // check exception
		 else if(account=="ce1002" && password=="minelab"){ // check correct
			 System.out.println("Succeed");
			 return true;
		 }
		 else{ // fail
			 System.out.println("Failed");
			 return false;
		 }
		 
	 }

}
