package ce1002.e9.s102502030;

public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//丟入三種測資
		try {
			System.out.println( "Test1 : Account = ce1234 , Password = minelab" );
			if( check( "ce1234", "minelab" )==true ) {
				System.out.println( "Succeed" );
			}
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
		}
		try {
			System.out.println( "Test2 : Account = null , Password = minelab" );
			if( check( null, "minelab" )==true ) {
				System.out.println( "Succeed" );
			}
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
		}
		try {
			System.out.println( "Test3 : Account = ce1002 , Password = minelab" );
			if( check( "ce1002", "minelab" )==true ) {
				System.out.println( "Succeed" );
			}
		} catch( Exception e ) {
			System.out.println( e.getMessage() );
		}
	}

	public static boolean check( String account , String password ) throws Exception {
		String ac = "ce1002";
		String pw = "minelab";
		
		//正確回傳true
		if( account==ac && password==pw ) {
			return true;
		}
		//錯誤回傳exception
		else if( account==null || password==null ) {
			throw new Exception( "Account or password can't not empty" );
		}
		else {
			throw new Exception( "Failed" );
		}
	}
}