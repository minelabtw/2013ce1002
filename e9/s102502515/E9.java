package ce1002.e9.s102502515;

public class E9 {
	public static void main(String[] args) throws Exception {//add exception
		try{
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		if (check("ce1234" , "minelab")){//call check function
			System.out.println("Succeed");
		}
		else 
			System.out.println("Failed");
		}
		catch(Exception e)//if exception happen, do this
		{
			System.out.println(e);//print "e"
		}
		
		try{
		System.out.println("Test2 : Account = null , Password = minelab");
		if (check("" , "minelab")){
			System.out.println("Succeed");
		}
		else 
			System.out.println("Failed");
		}
		catch(Exception e)//if exception happen, do this
		{
			System.out.println(e);//print "e"
		}
		
		try{
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		if (check("ce1002" , "minelab")){
			System.out.println("Succeed");
		}
		else 
			System.out.println("Failed");
		}
		catch(Exception e)//if exception happen, do this
		{
			System.out.println(e);//print "e"
		}
	}
	
	
	public static boolean check(String account , String password) throws Exception {//check function adding exception
	
			if (account.equals("ce1002") && password.equals("minelab"))//the situation of right account and password
			{
				return true;
			}
			else if(account == "" || password == "")//if either account or password is empty
			{
				throw new Exception("Account or password can��t be empty");//throw exception with some words
			}
			else//remaining situation
				return false;
		
	}
}
