package ce1002.e9.s102502521;

public class E9 {

	/**
	 * @param args
	 */

	public static boolean check(String account, String password) throws Exception {

		//check account and password
		
		if (account == null || password == null) {
			throw new Exception("Account or password can't be empty");
		} 
		else if (account != "ce1002" || password != "minelab") {
			return false;
		} 
		else {
			return true;
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//test 1
		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");

			if (check("ce1234", "minelab")) {
				System.out.println("Succeed");
			} 
			else {
				System.out.println("Failed");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		//test 2
		try {
			System.out.println("Test2 : Account = null , Password = minelab");

			if (check(null, "minelab")) {
				System.out.println("Succeed");
			} 
			else {
				System.out.println("Failed");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		//test 3
		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");

			if (check("ce1002", "minelab")) {
				System.out.println("Succeed");
			} 
			else {
				System.out.println("Failed");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
