package ce1002.e9.s102502043;

public class E9 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		try{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");//判定有沒有例外
			check("ce1234","minelab");//再去check true false;
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		try{
			System.out.println("Test2 : Account = null , Password = null");
			check(null,"minelab");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		try{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002","minelab");
		}
		catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
	public static boolean check(String account , String password) throws Exception 
	{
		if(account==null||password==null)//設定例外情形
		{
			throw new Exception("Account or password can’t not empty");
		}
		else if(account!="ce1002"||password!="minelab")
		{
			System.out.println("Failed");
			return false;
		}
		else
		{
			System.out.println("Succed");
			return true;
		}
	}

}
