package ce1002.e9.s995001561;

public class E9 {
	
	 // check function
	 public static boolean check(String account , String password) throws Exception {
		 if(account == "null" || password == "null") throw new Exception("Account or password can't not empty");
		 else if(account == "ce1002" && password == "minelab") return true;
		 else return false;
		 }

	public static void main(String[] args) throws Exception {

	    try {
	    	// first try
	    	System.out.println("Test1 : Account = ce1234 , Password = minelab");
	    	check("ce1234","minelab");
	    	// print result
	    	if(check("ce1234","minelab")==true) System.out.println("Succeed");
	    	else System.out.println("Failed");

	    	// second try
	    	System.out.println("Test2 : Account = null , Password = minelab");
	    	check("null","minelab");
	    	// print result
	    	if(check("ce1234","minelab")==true) System.out.println("Succeed");
	    	else System.out.println("Failed");
	    	
	    	}
	        catch (Exception ex) {
	        // exception
	    	System.out.println("" + ex.getMessage());

	    	}
	    	// third try
	        check("ce1002","minelab");
	    	System.out.println("Test3 : Account = ce1002 , Password = minelab");
	    	// print result
	    	if(check("ce1002","minelab")==true) System.out.println("Succeed");
	    	else System.out.println("Failed");
	}	         
}


