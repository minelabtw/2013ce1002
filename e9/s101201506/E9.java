package ce1002.e9.s101201506;

public class E9 {
	//自訂的函式
	public static boolean check(String account , String password) throws Exception
	{
		if(account==null || password ==null)
            throw new Exception("Account or password can’t not empty");
        if(account.equals("ce1002") && password.equals("minelab"))
            return true;
        else
            return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 輸出帳號密碼
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
        try
        {
            boolean islogin = check("ce1234","minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println("Test2 : Account = null , Password = minelab");
        try
        {
            boolean islogin = check(null,"minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
        System.out.println("Test3 : Account = ce1002 , Password = minelab");
        try
        {
            boolean islogin = check("ce1002","minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
		
		
		

	}

}
