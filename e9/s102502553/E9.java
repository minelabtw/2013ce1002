package ce1002.e9.s102502553;

public class E9 {

	public static void main(String[] args) {
		
		try//測試狀況1和2
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234","minelab");
			
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null,"minelab");
		}
		catch(Exception ex)//例外情況
		{
			System.out.println(ex.getMessage());//輸出getMessage的刺串
		}
		
		try
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002","minelab");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
	    }
	
	}
	

	public static boolean check(String acc,String pass) throws Exception//判斷對或錯還有例外情況
	{
		String account = "ce1002";
		String password = "minelab";
		
		if( acc == null || !pass.equals("minelab"))
		{	
			throw new Exception("Account or password can't not empty");
		}
		else if((acc.equals(account)) && (pass.equals(password)))
		{
			System.out.println("Succeed");
			return true;
		}
		else 
		{
			System.out.println("Failed");
			return false;
		}
	}
}
