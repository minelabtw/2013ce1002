package ce1002.e9.s102502032;

public class E9
{
	public static void main(String[] args)
	{
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try
		{
			if (check("ce1234", "minelab") == true)
			{
				System.out.println("Succeed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Test2 : Account = null , Password = minelab");
		try
		{
			if (check(null, "minelab") == true)
			{
				System.out.println("Succeed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try
		{
			if (check("ce1002", "minelab") == true)
			{
				System.out.println("Succeed");
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
	}

	public static boolean check(String account, String password)
			throws Exception
	{
		boolean result = false;
		if (account == null || password == null)
		{
			throw new Exception("Account or password can't not empty");
		}
		else if (account != "ce1002" || password != "minelab")
		{
			throw new Exception("Failed");
		}
		else
		{
			result = true;
		}
		return result;
	}
}
