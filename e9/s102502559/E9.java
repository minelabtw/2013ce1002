package ce1002.e9.s102502559;

public class E9 {

	public static String account = "ce1002";
	public static String password = "minelab";
	public static boolean result;
	
	public static void main(String[] args) {
	
		try
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234","minelab");
			Login(result);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		try
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null,"minelab");
			Login(result);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		try
		{
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002","minelab");
			Login(result);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static boolean check( String AC,String PA ) throws Exception
	{
		if(AC == null || PA == null)
		{
			throw new Exception("Account or password can’t not empty");
		}
		result = (AC == account ) && (PA == password);
		return result;
	}
	
	public static void Login(boolean result)
	{
		if(result == true)
		{
			System.out.println("Succeed");
		}
		if(result == false)
		{
			System.out.println("Failed");
		}
	}
}
