package ce1002.e9.s102503017;

public class E9 {

	public static void main(String[] args) {
		
		//input the result into check function.
		System.out.println("Test 1: Account = ce1234 , Password = minelab");
		//for an Execption model, we have to surround the function within try and catch.
		try {
			check("ce1234", "minelab");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Test 2: Account = null , Password = minelab");
		try {
			check(null,"minelab");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Test 3: Account = ce1002 , Password = minelab");
		try {
			check("ce1002", "minelab");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public static boolean check(String account, String password) throws Exception
	{
		//when one of the input within an empty string, an exception occurs.
		if(account == null || password == null)
		{
			throw new Exception("Account or password cannot empty.");
			
			
		}
		//answer determination.
		if (account == "ce1002" && password == "minelab")
		{
			System.out.println("Succeed");
			return true;
		}
		else
		{
			System.out.println("Failed");
			return false;
		}
	}

}
