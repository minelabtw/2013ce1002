package ce1002.e9.s102502002;

public class E9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int i=1;
		String pw = "minelab";
		String t; // string to save the test string
		
		boolean IncorrectInput = true; // boolean variable to determine if the input is incorrect
		while(IncorrectInput==true){
			try{
				System.out.print("Test" + i + " : Account = ");
				if(i==1){ // set three different cases of input
				t = "ce1234";
				}
				else if (i==2){
					t=null;
				}
				else{
					t="ce1002";
				}
				System.out.println(t + " , Password = " + pw); // output the account and password
				IncorrectInput=check(t,pw); // check the correction
			}
			catch(Exception ex){ // if the input is wrong run the exception
				System.out.println("Account or password can't not empty");
			}
		i++; // plus the count
		}
		
	}
	public static boolean check(String account , String password) throws Exception {
		if(account=="ce1002" && password == "minelab"){ // if the input is correct, output succeed and return false of incorrect input
			System.out.println("Succeed");
			return false;
		}
		else if ((account!="ce1002" || password != "minelab") && account != null){ // if the input is not correct output fail
			System.out.println("Failed");
			return true;
		}
		else // if there's no input skip to exception
			throw new Exception();
	}

}


/* Example Output
Test1 : Account = ce1234 , Password = minelab
Failed
Test2 : Account = null , Password = minelab
Account or password can't not empty
Test3 : Account = ce1002 , Password = minelab
Succeed
*/