package ce1002.e9.s102502033;

public class E9
{
	public static boolean check(String account, String password) throws Exception//declare exception
	{
		if (account == null || password == null)
		{
			throw new Exception("Account or password can't be empty");//throw exception
		}
		if (account == "ce1002" && password == "minelab")
		{
			return true;
		} 
		else
			return false;

	}

	public static void main(String[] args)
	{
		boolean judge;
		try//catch exception
		{
			judge = check("ce1234", "minelab");
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			if(judge==true)
			{
				System.out.println("Succeed");
			}
			else
				System.out.println("Failed");
		}
		catch (Exception ex)
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			System.out.println(ex.getMessage());
		}
		try
		{
			
			judge = check(null, "minelab");
			System.out.println("Test2 : Account = null , Password = minelab");
			if(judge==true)
			{
				System.out.println("Succeed");
			}
			else
				System.out.println("Failed");
		}
		catch (Exception ex)
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			System.out.println(ex.getMessage());
		}
		try
		{			
			judge = check("ce1002", "minelab");
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			if(judge==true)
			{
				System.out.println("Succeed");
			}
			else
				System.out.println("Failed");			
		} 
		catch (Exception ex)
		{
			System.out.println("Test2 : Account = null , Password = minelab");
			System.out.println(ex.getMessage());
		}

	}
}
