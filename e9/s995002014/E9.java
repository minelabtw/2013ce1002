package ce1002.e9.s995002014;

public class E9 {

	public static void main(String[] args) {
		//執行三個測試
		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234" , "minelab");
		}
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		try {
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null , "minelab");
		}
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002" , "minelab");
		}
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
	//檢查method
	 public static boolean check(String account , String password) throws Exception{
		  if(account==null || password == null) {
			 throw new Exception("Account or password can’t not empty"); //設定例外訊息
		 }
		 if(account=="ce1002"&&password=="minelab"){
			 System.out.println("Succeed");
			 return true;
		 }
		 else {
			 System.out.println("Failed");
			 return false;
		 }
	 }
}
