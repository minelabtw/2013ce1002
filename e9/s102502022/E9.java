package ce1002.e9.s102502022;

public class E9 {

	public static void main(String[] args) {
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		check("ce1234", "minelab");
		System.out.println("Test1 : Account = null , Password = minelab");
		check(null, "minelab");
		System.out.println("Test1 : Account = ce1002 , Password = minelab");
		check("ce1002", "minelab");

	}

	// TODO Auto-generated method stub

	public static boolean check(String account, String password) {
		boolean word = false;
		try {
			if (account == "ce1234" && password == "minelab") {
				word = false;
				throw new Exception("Failed");
			}
			if (account == null || password == null) {
				word = false;
				throw new Exception("Account or password can't not empty");
			}
			if (account == "ce1002" && password == "minelab") {
				word = true;
				throw new Exception("Succeed");
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return word;
	}
}
