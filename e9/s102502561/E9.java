package ce1002.e9.s102502561;

public class E9 {
	public static void main(String[] args) {//main function
		try {								//try和catch一對	throw和catch一對
			check("ce1234", "minelab");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			check(null, "minelab");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			check("ce1002", "minelab");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public static boolean check(String account, String password)//check function
			throws Exception {
		if ("ce1234" == account && "minelab" == password) {
			throw new Exception(
					"Test1 : Account = ce1234 , Password = minelab \nSucceed");
		} else if (account == null || password == null) {
			throw new Exception(
					"Test2 : Account = null , Password = minelab \nAccount or password can't not empty");
		} else {
			throw new Exception(
					"Test3 : Account = ce1002 , Password = minelab \nFailed");
		}
	}
}
