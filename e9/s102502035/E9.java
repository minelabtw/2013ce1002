package ce1002.e9.s102502035;

public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 第一次
		try {
			System.out.print("Test" + 1);
			if (check("ce1234", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// 第二次
		try {
			System.out.print("Test" + 2);
			if (check(null, "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		// 第三次
		try {
			System.out.print("Test" + 3);
			if (check("ce1002", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static boolean check(String account, String password) {
		System.out.println(" : Account = " + account + " , Password = "
				+ password);
		if (password == null || account == null) {
			throw new RuntimeException("Account or password can't not empty");// 若出現帳號或密碼是null的情況
		}
		if (account == "ce1002") {// account 正確
			if (password == "minelab") {// password 正確
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
