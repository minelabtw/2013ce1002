package ce1002.e9.s102502535;

public class E9 {

	public static void main(String[] args) {

		//output three cases' result
		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234", "minelab");
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null, "minelab");
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002", "minelab");
		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	
	//check if the data is correct
	public static boolean check(String account, String password)
			throws Exception {
		if (account == "ce1002" && password == "minelab") {
			System.out.println("Succeed");
			return true;
		} else if (account == null || password == null)
			throw new Exception("Account or password can��t not empty");
		else {
			System.out.println("Failed");
			return false;
		}
	}

}
