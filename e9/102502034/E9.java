package ce1002.e9.s102502034;

public class E9 {

	public static void main(String[] args) {
		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			if (check("ce1234", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");

			System.out.println("Test2 : Account = null , Password = minelab");

			check(null, "minelab");
		} catch (Exception e1) {
			System.out.println(e1.getMessage());

		}
		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");

			if (check("ce1002", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");

		} catch (Exception e2) {

		}
		// test all condition

	}

	public static boolean check(String account, String password)
			throws Exception {

		if (account == "ce1002" && password == "minelab")
			return true;
		else if (account == null || password == null)
			throw new Exception("Account or password can't not empty");
		else
			return false;

	}
	// throw exception and check true or false

}
