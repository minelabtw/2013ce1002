package ce1002.e9.s100203020;

public class E9 {
	public static boolean check(String account , String password) throws Exception{
		//Exception
		if (account==null || password == null){
			throw new Exception("Account or password can’t not empty");	
			
		}
		//true
		if (account == "ce1002" &&password== "minelab"){
			return true;
		}
		//false
		else{
			return false;
		}
		
	}
	public static void main(String[] args) {
		//test 1
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		boolean test;
		try {
			test=check("ce1234" , "minelab");
			if(test)
				System.out.println("Succeed");
			else if(!test)
				System.out.println("Failed");
				
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
		}
		//test2
		System.out.println("Test2 : Account = null , Password = minelab");
		try {
			test=check(null , "minelab");
			if(test)
				System.out.println("Succeed");
			else if(!test)
				System.out.println("Failed");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());			
		}
		//test3
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try {
			test=check("ce1002" , "minelab");
			if(test)
				System.out.println("Succeed");
			else if(!test)
				System.out.println("Failed");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		
	}
	
	
	
	

}
