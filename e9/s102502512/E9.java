package ce1002.e9.s102502512;

public class E9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Test1 : Account = ce1234 , Password = minelab");	//Line 7-42: test the account and password
		try
        {
            boolean islogin = check("ce1234","minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
		System.out.println("Test2 : Account = null , Password = minelab");
		try
        {
            boolean islogin = check(null,"minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try
        {
            boolean islogin = check("ce1002","minelab");
            if(islogin)
                System.out.println("Succeed");
            else
                System.out.println("Failed");
        }catch(Exception e)
        {
            System.out.println(e.getMessage());
        }

	}
	
	public static boolean check (String account , String password) throws Exception		//Line 46-57: make a function to check the account and password 
	{
			if(account=="ce1002"&&password=="minelab")
			{
				return true;
			}
			if(account==null||password==null)
			{
				throw new Exception("Account or password can't not empty");
			}
			else
				return false;
	}

}
