package ce1002.e9.s102502543;

import java.io.*;

public class E9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234", "minelab");
			if (check("ce1234", "minelab") == true)
				System.out.println("Succeed");
			if (check("ce1234", "minelab") == false)
				System.out.println("Failed");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			System.out.println("Test2 : Account = null , Password = minelab");
			check(null, "minelab");
			if (check(null, "minelab") == true)
				System.out.println("Succeed");
			if (check(null, "minelab") == false)
				System.out.println("Failed");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		try {
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002", "minelab");
			if (check("ce1002", "minelab") == true)
				System.out.println("Succeed");
			if (check("ce1002", "minelab") == false)
				System.out.println("Failed");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public static boolean check(String account, String password)
			throws Exception {
		if (account == null || password == null) {
			throw new Exception("Account or password can��t not empty");
		} else if (account == "ce1002" && password == "minelab")
			return true;
		else
			return false;
	}

}
