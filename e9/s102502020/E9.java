package ce1002.e9.s102502020;

public class E9 {
	
	public static boolean check(String account , String password) throws Exception          //確認帳號密碼
	 {
		 if(account != null && password != null){                                           //沒null的情況
			 if(account !="ce1002" || password != "minelab" ){                              //輸入錯誤
				 System.out.println("Failed");
				 return false;
			 }
			 else{                                                                          //輸入正確
				 System.out.println("Succeed");
				 return true;
			 }
		 }
		 else{                                                                              //有null的情況
			 throw new Exception("Account or password can't not empty");                    //處理例外的情況
		 }
	 }
	public static void main(String[] args) {
		// TODO Auto-generated method stubs
		String [] account = {"ce1234",null,"ce1002"};                                       //三組帳號密碼
		String [] password = {"minelab","minelab","minelab"};
		
		try{                                                                                //給一個敘述句
			for(int i=0 ; i<3 ;i++){
				System.out.println("Test" +(i+1)+" : Account = "+account[i]+" , Password = "+password[i]);
				check(account[i],password[i]);
			}
		}
		catch(Exception ex1){                                                               //取得例外的情況，並做適當處理，跳出for
			System.out.println(ex1.getMessage());
		}
		
		try{
				System.out.println("Test3"+" : Account = "+account[2]+" , Password = "+password[2]);
				check(account[2],password[2]);
		}
		catch(Exception ex2){
		}
	}
	
}
