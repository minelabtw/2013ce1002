package ce1002.e9.s100204006;

public class E9 
{
	public static void main (String arg[]) 
    {
		try
		{
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234","minelab");
			System.out.println("Test2 : Account = null , Password = minelab");	
			check(null,"minelab");
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1234","minelab");
		}
		catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
		
    }
	
	public static boolean check(String account , String password) throws Exception
	{
		if(account == null || password == null)
		{
			throw new Exception("An Exception has been caught");
		}
		
		if(account.equals("ce1002") && password.equals("minelab"))
		{
			System.out.println("Succeed");
			return true;
		}
		else
		{
			System.out.println("Failed");
			return false;
		}
	}

	
	
	
}
