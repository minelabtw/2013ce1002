package ce1002.e9.s102502042;

public class main {

	public static boolean check(String account,String password)throws Exception
	{
		if(account=="ce1002"&&password=="minelab") //true
		{
			System.out.println("Succeed");
			return true;
		}
		else if(account==null||password==null) //exception
		{
			throw new Exception("Account or password can��t not empty");
		}
		else //wrong
		{
			System.out.println("Failed");
			return false;
		}
	}
	public static void main(String[] args) {
		//first
		try
		{
			check("ce1234" , "minelab");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		//second
		try
		{
			check(null , "minelab");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		//third
		try
		{
			check("ce1002" , "minelab");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

}
