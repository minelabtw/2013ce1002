package ce1002.e9.s102502556;

public class E9 {
	
	static String realaccount = "ce1002"; //真正的帳號
	static String realpassword = "minelab"; //真正的密碼


	public static void main(String[] args) throws Exception {
		
		String testaccount[] = {"ce1234", null, "ce1002"}; //測試帳號數據
		String testpassword[] = {"minelab", "minelab", "minelab"}; //測試密碼數據

		for ( int i = 1 ; i <= 3 ; i++ ) //用for迴圈做測試，並有例外回報的功能
		{
			try
			{
				System.out.println("Test" + i  + " : Account = " + testaccount[i - 1] + " , Password = " + testpassword[i - 1]);
				check(testaccount[i - 1], testpassword[i - 1]);			
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
			}
		}

	}

	//檢查函數
	public static boolean check(String account, String password) throws Exception {
		
		//如果帳號密碼其中一個為空，則回報錯誤
		if(account == null || password == null)
		{
			throw new Exception("Account or password can't not empty");
		}
		//如果帳號密碼都正確，則回傳true並顯示成功
		if( account == realaccount && password == realpassword )
		{
			System.out.println("Succeed");
			return true;
		}
		//否則，回傳false並顯示失敗
		else
		{
			System.out.println("Failed");
			return false;
		}
		
	}
}