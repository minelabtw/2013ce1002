package ce1002.e9.s984008030;

public class E9 {

	public static boolean check(String account, String password) throws Exception {
		
		// Check if account or password is null first
		if (account == null || password == null) {
			throw new Exception("Account or password can't be empty");
		}
		
		// Then check if account and password are correct
		if (account.equals("ce1002") && password.equals("minelab")) {
			System.out.println("Succeed");
			return true;
		}
		else {
			System.out.println("Failed");
			return false;
		}
	}
	
	public static void main(String[] args) {
		
		// TODO check if a series of account and password are correct
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try {
			check("ce1234" , "minelab");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		System.out.println("Test2 : Account = null , Password = minelab");
		try {
			check(null, "minelab");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try {
			check("ce1002" , "minelab");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
	}

}
