package ce1002.e9.s102502540;

public class E9 {

	public static void main(String[] args) throws Exception {
		try { // 第一次測試
			System.out.println("Test1 : Account = ce1234 , Password = minelab");
			check("ce1234", "minelab");
			if (check("ce1234", "minelab") == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} catch (Exception e1) {
			System.out.println(e1.getMessage());
		}
		try { // 第二次測試
			System.out.println("Test2 : Account = null , Password = minelab");
			check("null", "minelab");
		} catch (Exception e2) {
			System.out.println(e2.getMessage());
		}
		try { // 第三次測試
			System.out.println("Test3 : Account = ce1002 , Password = minelab");
			check("ce1002", "minelab");
			if (check("ce1002", "minelab") == true)
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} catch (Exception e3) {
			System.out.println(e3.getMessage());
		}
	}

	public static boolean check(String account, String password)
			throws Exception { //查三個不同的帳號密碼情況
		if (account == "ce1002" && password == "minelab")
			return true;
		else if (account == "null" || password == "null")
			throw new Exception("Account or password can’t not empty");
		else {
			return false;
		}
	}
}