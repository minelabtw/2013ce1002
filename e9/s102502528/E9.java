package ce1002.e9.s102502528;

public class E9 {
	public static String[][] testData = { { "ce1234", "minelab" },{ null, "minelab" }, { "ce1002", "minelab" } };
	//put all the test data in it
	public static void main(String[] args) {
		for (int i = 0; i != 3; i++) {  //do the check three times
			System.out.println("Test" + (i + 1) + " : Account = "+ testData[i][0] + " , Password = " + testData[i][1]);
			try {
				System.out.println(check(testData[i][0], testData[i][1]) ? "Succeed": "Failed");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
	}

	public static boolean check(String account, String password)
			throws Exception {  //exception
		if (account == null || password == null || account == ""|| password == "")
			throw new Exception("Account or password can't not empty.");
		return "ce1002" == account && "minelab" == password;
	}
}