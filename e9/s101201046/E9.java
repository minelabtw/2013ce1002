package ce1002.e9.s101201046;

public class E9 {
	 public static boolean check(String account , String password) throws Exception {
				
		if (account == null || password == null) // the condition of account or password is null
			throw new Exception("Account or password can't not empty");
		
		if (account.equals("ce1002") && password.equals("minelab")) //when account and password are correct
			return true;
		else //account or password is incorrect
			return false;
		 
	 }
	public static void main(String[] args) {
		
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try { //when account is ce1234 and password is minelab
			if (check("ce1234", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
			
		System.out.println("Test2 : Account = null , Password = minelab");
		try { //when account is null and password is minelab
			if (check(null, "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		System.out.println("Test3 : Account = ce1002 , Password = minelab");
		try { //when account is ce1002 and pasword is minelab
			if (check("ce1002", "minelab"))
				System.out.println("Succeed");
			else
				System.out.println("Failed");
		} 
		catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}
