package ce1002.e9.s102502038;

public class E9 {
	public static void main(String[] args){
		System.out.println("Test1 : Account = ce1234 , Password = minelab");
		try{//try and catch
			check("ce1234","minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("Test1 : Account = null , Password = minelab");
		try{
			check(null,"minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("Test1 : Account = ce1002 , Password = minelab");
		try{
			check("ce1002","minelab");
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
	public static boolean check(String account,String password)throws Exception{//function
        if(account == null || password == null){
            throw new Exception("Account or password can��t not empty");
        }
		if(account.equals("ce1002")&&password.equals("minelab")){
			System.out.println("Succeed");
			return true;
		}else{
			System.out.println("Failed");
			return false;
		}
	}
}