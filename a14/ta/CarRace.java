package ce1002.a14.ta;

import java.util.ArrayList;
import java.util.List;

public class CarRace implements Runnable {// 實作Runnable才能放進thread

	MyFrame frame;
	List<Car> cars;
	float totalDistance;
	boolean isFinished;

	public CarRace(MyFrame farme) {
		this.frame = farme;
		isFinished = false;
		totalDistance = 5000;
		cars = new ArrayList<Car>();
	}

	public void addCar() {
		Car car = new Car();
		car.id = cars.size();// id設為加入順序
		cars.add(car);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (!isFinished) {// 只要isFinished==false就會一直跑
			runCars();
		}
	}

	private void runCars() {
		for (Car car : cars) {
			car.distance += car.randomSpeed();// Car前進
			// System.out.println(car.toString() + ", distance=" +
			// car.distance);
			if (car.distance >= totalDistance) {// 過終點線
				isFinished = true;// 停止loop
				frame.showWinner(car);
			}
		}
	}

	public int carNum() {
		return cars.size();
	}

}
