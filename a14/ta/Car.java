package ce1002.a14.ta;

import java.util.Random;

public class Car {

	public int id;
	public float distance;

	public Car() {
		distance = 0;
		id = 0;
	}

	public float randomSpeed() {// 回傳0~100的隨機數
		Random r = new Random();
		return r.nextFloat() * 100;
	}

	@Override
	public String toString() {
		return "Car no." + id;
	}
}
