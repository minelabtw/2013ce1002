package ce1002.a4.s102502038;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class A4 {
	public static void main(String[] Args){
		int num;
		String inputln;
		// input section
		while(true){
			System.out.println("Please input the number of cars:");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				num = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(num<=0){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		// end of input section
		final Car[] carlist = new Car[num];
		for(int i = 0;i<num;i++){
			carlist[i] = new Car();
			// brand input section
			while(true){
				System.out.println("Input car's");
				System.out.print("Brand: ");
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
					carlist[i].setBrand(inputln);
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				break;
			}
			// end of brand input section
			// name input section
			while(true){
				System.out.print("Name: ");
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				carlist[i].setName(inputln);
				break;
			}
			// end of name input section
			// maxSpeed input section
			while(true){
				System.out.print("Max speed: ");
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				carlist[i].setMaxSpeed(Float.parseFloat(inputln));
				break;
			}
			// end of maxSpeed input section
			// fuel input section
			while(true){
				System.out.print("Fuel consumption: ");
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				carlist[i].setFuelConsumption(Float.parseFloat(inputln));
				break;
			}
			// end of fuel input section
			// price input section
			while(true){
				System.out.print("Price: ");
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				carlist[i].setPrice(Float.parseFloat(inputln));
				break;
			}
			// end of price input section
			System.out.println("");
		}
		System.out.println("Output car status");
		for(int i = 0;i<num;i++){
			System.out.println("Car brand is " + carlist[i].getBrand() + ".");
			System.out.println("Car name is " + carlist[i].getName() + ".");
			System.out.println("Car max speed is " + carlist[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption " + carlist[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + carlist[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			carlist[i].startTurbo();
			System.out.println("Turbo status is " + carlist[i].isTurbo() + ".");
			System.out.println("Current speed is " + carlist[i].currentSpeed(carlist[i].isTurbo()) + ".");
			System.out.println("");
		}
	}
}
