package ce1002.a4.s102502047;

public class Vehicle {
	private String b;	//品牌
	private String n;	//款式名稱
	private float m;	//最高速度
	private float f;	//油耗
	private float p;	//價格
	public Vehicle() {
		
	}
	public void sb(String b) {
		this.b=b;
	}
	public void sn(String n) {
		this.n=n;
	}
	public void sm(float m) {
		this.m=m;
	}
	public void sf(float f) {
		this.f=f;
	}
	public void sp(float p) {
		this.p=p;
	} 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String gb() {
		return b;
	}
	public String gn() {
		return n;
	}
	public float gm() {
		return m;
	}
	public float gf() {
		return f;
	}
	public float gp() {
		return p;
	}

	public float cs(boolean isTurbo) {	
		if(!isTurbo)
			m=(float)Math.random()*m;
		return m;
	}
}
