package ce1002.a4.s102502047;

public class Car 
	extends Vehicle{
	private boolean isTurbo;
	public Car(String b,String n,float m,float f,float p) {
		sb(b);
		sn(n);
		sm(m);
		sf(f);
		sp(p);
		startTurbo();
	}
	public void startTurbo() 
	{	
		 if(Math.random()>0.5)
			 isTurbo=true;
		 else
			 isTurbo=false;
	}
	public boolean isTurbo() 
	{		
		return isTurbo;
	}
}
