package ce1002.a4.s102502047;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		System.out.println("Please input the number of cars: ");
		Scanner t = new Scanner(System.in);
		int u=t.nextInt();
		for(;u<1;)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			u=t.nextInt();
		}
		
		Car c[]=new Car[u];
		for(int i=0;i<u;i++)
		{
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String b=t.next();
			System.out.print("Name: ");
			String n=t.next();
			System.out.print("Max speed: ");
			float m=t.nextFloat();
			System.out.print("Fuel consumption: ");
			float f=t.nextFloat();
			System.out.print("Price: ");
			float p=t.nextFloat();
			System.out.print("\n");
			c[i]=new Car(b,n,m,f,p);
		}
		
		System.out.println("Output car status");
		for(int i=0;i<u;i++)
		{
			System.out.println("Car Brand is "+c[i].gb());
			System.out.println("Car name is "+c[i].gn());
			System.out.println("Car max speed is "+c[i].gm());
			System.out.println("Car fuel consumption is "+c[i].gf());
			System.out.println("Car price is "+c[i].gp());
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+c[i].isTurbo());
			System.out.println("Current speed is "+c[i].cs(c[i].isTurbo()));
			System.out.print("\n");
		}
	}

}
