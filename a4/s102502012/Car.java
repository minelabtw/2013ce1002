package ce1002.a4.s102502012;
import java.util.*;

public class Car extends Vehicle {
	
	private boolean isTurbo;
	
	public void startTurbo(){
		Random tmp = new Random();
		if(tmp.nextInt(2) == 1)
			isTurbo = true;
		else
			isTurbo = false;
	}
	
	public boolean isTurbo(){
		return isTurbo;
	}
}
