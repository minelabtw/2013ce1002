package ce1002.a4.s102502012;
import java.util.Random;

public class Vehicle {
	
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	
	public void setBrand(String brand){
		this.brand = brand;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setMaxSpeed(float maxSpeed){
		this.maxSpeed = maxSpeed;
	}
	
	public void setFuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	
	public void setPrice(float price){
		this.price = price;
	}
	
	public String getBrand(){
		return brand;
	}
	
	public String getName(){
		return name;
	}
	
	public float getMaxSpeed(){
		return maxSpeed;
	}
	
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	
	public float getPrice(){
		return price;
	}
	
	public float currentSpeed(){
		Random fix = new Random();
		return maxSpeed * fix.nextFloat();
	}
	
	public float currentSpeed(boolean isTurbo){
		if(isTurbo)
			return maxSpeed;
		else
			return currentSpeed();
	}
}
