package ce1002.a4.s102502012;
import java.util.*;
public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n;
		do{
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
			if(n <= 0)
				System.out.println("Out of range!");
		}while(n <= 0);
		Car[] cars = new Car[n];
		for(int i = 0; i < n; i++){
			cars[i] = new Car();
			String brand, name;
			float maxSpeed, fuelConsumption, price;
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			
			// set information of cars
			cars[i].setBrand(brand);
			cars[i].setName(name);
			cars[i].setMaxSpeed(maxSpeed);
			cars[i].setFuelConsumption(fuelConsumption);
			cars[i].setPrice(price);
			
			System.out.println("");
		}
		System.out.println("Output car status");
		for(int i = 0; i < n; i++){ // print information of cars
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			if(i != n)
				System.out.println("");
		}
		input.close();
	}

}
