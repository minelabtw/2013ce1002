package ce1002.a4.s102502542;

import java.util.Random;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int id = input.nextInt();
		while (id <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			id = input.nextInt();
		}
		Car[] car = new Car[id];// 設定car物件陣列大小
		for (int i = 0; i < id; i++) {
			Car temp = new Car();// 設定暫存陣列 temp
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand = input.next();
			temp.setBrand(brand);
			System.out.print("Name: ");
			String name = input.next();
			temp.setName(name);
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			temp.setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			temp.setfuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			float price = input.nextFloat();
			System.out.print("\n");
			temp.setprice(price);
			Random ran = new Random();// 做亂數表
			int a = ran.nextInt(2);// 將亂數設定在0~1之間
			temp.startTurbo(a);
			car[i] = temp;// 將上述的設定丟給car陣列的第i個位置並儲存
		}
		System.out.println("Output car status");
		for (int i = 0; i < id; i++) // 輸出題目所需
		{
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName());
			System.out
					.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "
					+ car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getprice() + ".");
			System.out.println("StartTurbo! ");
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.print("\n");
		}

		// TODO Auto-generated method stub

	}

}
