package ce1002.a4.s102502542;

import java.util.Random;

public class Vehicle {
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;

	public Vehicle() {

	}

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String a) {
		brand = a;
	}

	public void setName(String a) {
		name = a;
	}

	public void setMaxSpeed(float a) {
		maxSpeed = a;
	}

	public void setfuelConsumption(float a) {
		fuelConsumption = a;
	}

	public void setprice(float a) {
		price = a;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	// 取得目前速度
	public float currentSpeed(float a) //無渦輪
	{
		return a;
	}

	public float currentSpeed(boolean isTurbo) //有渦輪
	{
		Random ran = new Random();
		if (isTurbo == true)
			return maxSpeed;
		else
			return maxSpeed * ran.nextFloat();
	}

}
