package ce1002.a4.s102502049;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 1;
		String brand; // 品牌
		String name; // 款式名稱
		float maxSpeed; // 最高速度
		float fuelConsumption; // 油耗
		float price; // 價格
		
		do {
			if (number < 1) {
				System.out.println("Out of range!");
			}
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
		} while (number < 1); // ask number of car

		Car car[] = new Car[number]; // construct Car array
		for (int i = 0; i < number; i++) {
			car[i] = new Car();
		}

		for (int i = 0; i < number; i++) {
			System.out.println("Input car's"); 
			System.out.print("Brand: "); // set properties
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			System.out.println();
			
			car[i].setBrand(brand);  // set properties
			car[i].setName(name);
			car[i].setMaxSpeed(maxSpeed);
			car[i].setComsumption(fuelConsumption);
			car[i].setPrice(price);
		}
		
		System.out.println("Output car status");
		for(int i=0; i<number; i++){
			System.out.println("Car brand is " + car[i].getBrand() + "."); // print properties
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println();
		}
		
	}

}
