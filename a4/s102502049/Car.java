package ce1002.a4.s102502049;

public class Car extends Vehicle {

	private boolean isTurbo;

	public Car() { // default constructor
	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		if ( ((int) (Math.random() * 2)) == 0) {
			isTurbo = false;
		} else
			isTurbo = true;
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
