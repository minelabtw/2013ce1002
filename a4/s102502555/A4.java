package ce1002.a4.s102502555;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;    //車數
		Car[] car;  //車車陣列
		
		//輸入車輛數不能小於等於零
		do{
			System.out.println("Please input the number of cars:");
			num = input.nextInt();
			if(num <= 0){
				System.out.println("Out of range!");
			}
		}while(num <= 0);
		
		//設定車子陣列大小
		car = new Car[num];
		
		//輸入車子資料
		for(int i = 0 ; i < num ; i++){
			car[i] = new Car();
			
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(input.next());
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(input.nextFloat());
			System.out.println();
		}
		
		//輸出車子資料
		System.out.println("Output car status");
		
		for(int i = 0 ; i < num ; i++){
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println();
		}
		
		input.close();
	}

}
