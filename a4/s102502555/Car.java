package ce1002.a4.s102502555;

import java.util.Random;

public class Car 
		extends Vehicle{
	
	private boolean isTurbo;  //渦輪狀態
	
	Car(){
		
	}
	
	//初始化渦輪
	public void startTurbo(){
		Random rand = new Random();
		isTurbo = rand.nextBoolean();
	}
	
	//取得渦輪目前狀態
	public boolean isTurbo(){
		return isTurbo;
	}
	
}
