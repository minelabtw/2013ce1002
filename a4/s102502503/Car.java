package ce1002.a4.s102502503;
import java.util.Random;
public class Car extends Vehicle{
	private boolean isTurbo;	// 渦輪狀態
	public Car(){
		
	}
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		 Random r = new Random();
		 boolean turbo = r.nextBoolean();  //boolean亂數
		 this.isTurbo=turbo;
		}
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		 return isTurbo;
		}
}
