package ce1002.a4.s102502503;
import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public float currentSpeed() {  //給機車用(假設渦輪皆開啟失敗)
		Random r = new Random();
		float rate = r.nextFloat();  //浮點數亂數(0.0~1.0)
		
		return maxSpeed * rate;  //回傳最大速度*浮點數亂數
	}
	public float currentSpeed(boolean isTurbo) {	
		if (isTurbo==true)  //假設渦輪開啟成功
			return maxSpeed;  //回傳最大速度
		else
		{	//假設渦輪開啟失敗
			Random r = new Random();
			float rate = r.nextFloat();	
		
			return maxSpeed * rate;
		}
	}
}