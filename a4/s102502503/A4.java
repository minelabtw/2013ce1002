package ce1002.a4.s102502503;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num;
		String brand;
		String name;
		float maxSpeed,fuel,price;
		do{  //輸入車輛數並為正整數
			System.out.println("Please input the number of cars: ");
			num = scanner.nextInt();
			if (num<=0)
				System.out.println("Out of range!");
		}while (num<=0);
		
		Car cars[] = new Car[num];  //宣告class型態的陣列
		
		for (int i=0; i<num; i++)
		{
			Car car = new Car();  //將class宣告成物件
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			brand = scanner.next();
			car.setBrand(brand);  //設定brand
			System.out.print("Name: ");
			name = scanner.next();
			car.setName(name);  //設定name
			System.out.print("Max speed: ");
			maxSpeed = scanner.nextFloat();
			car.setMaxSpeed(maxSpeed);  //設定速度
			System.out.print("Fuel consumption: ");
			fuel = scanner.nextFloat();
			car.setFuelConsumption(fuel);  //設定油耗
			System.out.print("Price: ");
			price = scanner.nextFloat();
			car.setPrice(price);  //設定價錢
			System.out.println();
			cars[i] = car;  //將物件存入陣列
		}
		
		scanner.close();
		
		System.out.println("Output car status");
		for (int i=0; i<num; i++)
		{
			System.out.println("Car brand is "+cars[i].getBrand()+".");  //取得brand
			System.out.println("Car name is "+cars[i].getName()+".");  //取得name
			System.out.println("Car max speed is "+cars[i].getMaxSpeed()+".");  //取得速度
			System.out.println("Car fuel consumption is "+cars[i].getFuelConsumption()+".");  //取得油耗
			System.out.println("Car price is "+cars[i].getPrice()+".");  //取得價錢
			System.out.println("StartTurbo!");
			cars[i].startTurbo();  //測試turbo是否成功開啟
			System.out.println("Turbo status is "+cars[i].isTurbo()+".");  //取得渦輪是否開啟
			System.out.println("Current speed is "+cars[i].currentSpeed(cars[i].isTurbo())+".");  //取得目前速度
			System.out.println();
		}
	}

}
