package ce1002.a4.s100201023;

import java.util.Random;

public class Vehicle
{
	//property
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	
	//setting method
	public void setBrand(String in)
	{
		brand = in;
	}
	
	
	
	public void setName(String in)
	{
		name = in;
	}
	
	public void setMaxSpeed(float in)
	{
		maxSpeed = in;
	}
	
	public void setfuelConsumption(float in)
	{
		fuelConsumption = in;
	}
	
	public void setPrice(float in)
	{
		price = in;
	}

	//getting method
	public String getBrand()
	{
		return brand;
	}
	
	public String getName()
	{
		return name;
	}
	
	public float getMaxSpeed()
	{
		return maxSpeed;
	}
	
	public float getfuelConsumption()
	{
		return fuelConsumption;
	}
	
	public float getPrice()
	{
		return price;
	}
	
	//getting current speed
	public float currentSpeed()
	{
		Random number = new Random();
		return maxSpeed * number.nextFloat();
	}
	
	public float currentSpeed(boolean isTurbo)
	{
		if(isTurbo)
			return maxSpeed;
		else
			return currentSpeed();
	}
	
}
