package ce1002.a4.s100201023;

import java.util.Random;

public class Car extends Vehicle
{
	//properties
	private boolean isTurbo;
	
	//method
	public void startTurbo()
	{
		Random number = new Random();
		
		if(number.nextInt(2) == 1)
			isTurbo = true;
		else
			isTurbo = false;
	}
	
	public boolean isTurbo()
	{
		return isTurbo;
	}
	
}
