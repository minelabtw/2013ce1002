package ce1002.a4.s100201023;

import java.util.Scanner;

public class A4
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int number;
		Car[] cars;
		
		// input number of cars
		while(true)
		{
			System.out.println("Please input the number of cars: ");
			number =  input.nextInt();
			if(number > 0)
				break;
			System.out.println("Out of range!");
		}
		
		//setup array for cars
		cars = new Car[number];
		
		//input car properties
		for(int i = 0 ; i < number ; ++i)
		{
			cars[i] = new Car();
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			cars[i].setBrand(input.next());
			
			System.out.print("Name: ");
			cars[i].setName(input.next());
			
			System.out.print("Max speed: ");
			cars[i].setMaxSpeed(input.nextFloat());
			
			System.out.print("Fuel consumption: ");
			cars[i].setfuelConsumption(input.nextFloat());
			
			System.out.print("Price: ");
			cars[i].setPrice(input.nextFloat());
			
			System.out.println();
		}
		
		//output car status
		System.out.println("Output car status");
		for(int i = 0 ; i < number ; ++i)
		{
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println();
		}
	}

}
