package ce1002.a4.s102502549;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;// 有無渦輪

	public Car() {
	}
	
	// 測試啟動渦輪，可能會成功或失敗。
	public void startTurbo() {
		Random random = new Random();
		isTurbo=random.nextBoolean();
	}

	// 取得渦輪目前狀態
	public boolean isTurbo() {
		return isTurbo;
	}
}
