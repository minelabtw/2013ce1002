package ce1002.a4.s102502549;

import java.util.Scanner;
import java.util.concurrent.CancellationException;

public class A4 {
	public static void main(String[] args) {
		int num_of_car;
		Scanner input = new Scanner(System.in);

		//輸入車數並檢查
		do {
			System.out.println("Please input the number of cars: ");
			num_of_car = input.nextInt();

			if (num_of_car <= 0)
				System.out.println("Out of range!");

		} while (num_of_car <= 0);

		Car[] cararr = new Car[num_of_car];

		// 創造車子並存入陣列
		for (int i = 0; i < num_of_car; i++) {
			Car cartemp = new Car();

			System.out.println("Input car's");

			System.out.print("Brand: ");
			cartemp.setBrand(input.next());

			System.out.print("Name: ");
			cartemp.setName(input.next());

			System.out.print("Max speed: ");
			cartemp.setMaxSpeed(input.nextFloat());

			System.out.print("Fuel consumption: ");
			cartemp.setFuelConsumption(input.nextFloat());

			System.out.print("Price: ");
			cartemp.setPrice(input.nextFloat());

			cararr[i] = cartemp;
			
			System.out.println();
		}

		System.out.println("Output car status");
		
		//印結果
		for(int i=0;i<num_of_car;i++)
		{
			System.out.println("Car brand is "+cararr[i].getBrand()+".");
			System.out.println("Car name is "+cararr[i].getName()+".");
			System.out.println("Car max speed is "+cararr[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+cararr[i].getFuelConsumption()+".");
			System.out.println("Car price is "+cararr[i].getPrice()+".");
			System.out.println("StartTurbo!");
			cararr[i].startTurbo();
			System.out.println("Turbo status is "+cararr[i].isTurbo()+".");
			System.out.println("Current speed is "+cararr[i].currentSpeed(cararr[i].isTurbo())+".");
			System.out.println(); 
		}
	}
}
