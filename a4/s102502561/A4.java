package ce1002.a4.s102502561;

import java.util.Scanner;

public class A4 {
	

	
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		// 1.設定車數量
		int number = 0;
		System.out.println("Please input the number of cars: ");
		number = sc.nextInt();
		while (number <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = sc.nextInt();
		}// 設定車數量完成
		Car car[] = new Car[number];
		String brand ;//= new String();
		String name ;//= new String();
		float maxSpeed = 0;
		float fuel = 0;
		float price = 0;
		// 2.setbrand setname setmaxspeed setfuelconsumption setprice
		for (int i = 0; i < number; i++) {
			car[i] = new Car();
			
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			brand = sc.next();
			car[i].setBrand(brand);
			
			System.out.print("Name: ");
			name = sc.next();
			car[i].setName(name);
			
			System.out.print("Max Speed: ");
			maxSpeed = sc.nextFloat();
			car[i].setMaxSpeed(maxSpeed);
			
			System.out.print("Fuel consumption: ");
			fuel = sc.nextFloat();
			car[i].setfuelconsumption(fuel);
			
			System.out.print("Price: ");
			price = sc.nextFloat();
			car[i].setprice(price);
			
			System.out.println();
		}
		sc.close();
		
		//3.output car static
		System.out.println("Output car status");
		for(int i=0;i<number;i++)
		{
			System.out.println("Car brand is " + car[i].getBrand()+ ".");
			System.out.println("Car name is " + car[i].getName()+ ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed()+ ".");
			System.out.println("Car fuel consumption is " + car[i].getfuelconsumption()+ ".");
			System.out.println("Car price is " + car[i].getprice()+ ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is "+ car[i].currentSpeed(car[i].isTurbo()) +".");
			System.out.println();
		}
	}
}
