package ce1002.a4.s102502561;

import java.lang.Math;

public class Car extends Vehicle {


	private boolean isTurbo;// turbo 有無

	public Car() {

	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		double turbo = Math.random();
		isTurbo = (turbo>0.5?true:false);
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		if (isTurbo==true){
			return true;
		}
		else{
			return false;
		}
	}
}
