package ce1002.a4.s102502561;

import java.lang.Math;
public class Vehicle {

	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {

		this.brand = brand;
	}

	public void setName(String name) {

		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {

		this.maxSpeed = maxSpeed;
	}

	public void setfuelconsumption(float fuel) {

		this.fuelConsumption = fuel;
	}

	public void setprice(float price) {
		
		this.price = price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		// ....
		return brand;
	}

	public String getName() {
		// ....
		return name;
	}

	public float getMaxSpeed() {
		// ....
		return maxSpeed;
	}

	public float getfuelconsumption(){
		
		return fuelConsumption;
	}
	
	public float getprice(){
		
		return price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() { // 給之後有關機車的class使用，A4不會使用到。
		// ....
			return maxSpeed;
		
	}

	public float currentSpeed(boolean isTurbo) {
		if(isTurbo==false){
		double turbo = Math.random();
		float currentSpeed = (float) (turbo*maxSpeed);
		return currentSpeed;
		}
		else{
			return maxSpeed;
		}
	}
}
