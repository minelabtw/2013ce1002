package ce1002.a4.s102502557;

import java.util.Scanner;

public class A4 {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		Car[] cars;
		int num;
		
		do{
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if(num<=0)
				System.out.println("Out of range!");
		}while(num<=0);
		
		cars = new Car[num];//for迴圈外面宣告要新建一個陣列
		
		for(int n=0;n<num;n++)
		{
		cars[n] = new Car();
		System.out.println("Input car's");
		
		System.out.print("Brand: ");
		cars[n].set_brand(input.next());
		
		System.out.print("Name: ");
		cars[n].set_name(input.next());
		
		System.out.print("Max speed: ");
		cars[n].set_maxSpeed(input.nextFloat());
		
		System.out.print("Fuel consumption: ");
		cars[n].set_fuelConsumption(input.nextFloat());
		
		System.out.print("Price: ");
		cars[n].set_price(input.nextFloat());			
		System.out.print("\n");//換行
		}
		
		System.out.print("Output car status\n");
		
		for(int n = 0; n<num; n++)
		{
			System.out.print("Car brand is " + cars[n].get_brand() + "." + "\n");
			System.out.print("Car name is " + cars[n].get_name() + ".\n" );
			System.out.print("Car max speed is " + cars[n].get_maxSpeed() + ".\n" );
			System.out.print("Car fuel consumption is " + cars[n].get_fuelConsumption() + ".\n" );
			System.out.print("Car price is " + cars[n].get_price() + ".\n" );
	        
			System.out.print("StartTurbo!\n");
			
			cars[n].startTurbo();
			System.out.print("Turbo status is " + cars[n].isTurbo() + "\n" );
			System.out.print("Current speed is " + cars[n].currentSpeed(cars[n].isTurbo()) + "\n" + "\n");
			
		}//這裡的current speed用到多載
		input.close();//要記得關掉
	}

	
}
