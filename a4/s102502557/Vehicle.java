package ce1002.a4.s102502557;

import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public void set_brand(String _brand)
	{
		brand = _brand;
	}
	public String get_brand()
	{
		return brand;
	}
	
	public void set_name(String _name)
	{
		name = _name;
	}
	public String get_name()
	{
		return name;
	}
	
	public void set_maxSpeed(float _maxSpeed)
	{
		maxSpeed = _maxSpeed;
	}
	public Float get_maxSpeed()
	{
		return maxSpeed;
	}
	
	public void set_fuelConsumption(float _fuelConsumption)
	{
		fuelConsumption = _fuelConsumption;
	}
	public Float get_fuelConsumption()
	{
		return fuelConsumption;
	}
	
	public void set_price(float _price)
	{
		price = _price;
	}
	public float get_price()
	{
		return price;
	}
	
	public float currentSpeed()//給機車用的
	{
		return maxSpeed;
	}
	
	public float currentSpeed(boolean isTurbo)
	{
		if (isTurbo == true )
		{
			return maxSpeed;
		}
		else 
		{
			Random x = new Random();//產生一個隨機變數n
			float i = x.nextFloat();//隨機產生０～１
			return maxSpeed * i;
		}
	}

}
