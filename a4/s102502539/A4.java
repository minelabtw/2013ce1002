package ce1002.a4.s102502539;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) 
	{
		int carnum;
		Scanner input = new Scanner(System.in);
		
		do
		{
			System.out.println("Please input the number of cars: ");
			carnum = input.nextInt();
			if ( carnum < 1 )
				System.out.println("Out of range!");
		} while ( carnum < 1 );
		
		Car car[] = new Car[carnum];	//物件陣列
		
		for ( int i = 0 ; i < carnum ; i++ )
		{
			car[i] = new Car();	//個別宣告
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(input.next());
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(input.nextFloat());
			System.out.println();
		}
		
		System.out.println("Output car status");
		for ( int i = 0 ; i < carnum ; i++ )
		{
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + "." );
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) );
			System.out.println();
		}
		
		

	}

}
