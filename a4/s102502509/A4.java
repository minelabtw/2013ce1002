package ce1002.a4.s102502509;

import java.util.Scanner;

public class A4 
{

	public static void main(String[] args) 
	{
		int number;
		String brand = null; // string 初始值為null
		String name = null;
		float maxspeed = 0;
		float fuelconsumption = 0;
		float price = 0; 
		
		Scanner input = new Scanner(System.in);
		do
		{
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number <= 0)
			{
				System.out.println("Out of range!");
			}
		}while (number <= 0);
		
		Car cars[] = new Car [number];// 運用陣列來存，否則會被複寫
		
		for(int i = 0; i < number; i++)
		{
			
			cars[i] = new Car(i);
			
			System.out.println("Input car's(" + (i+1) + ")" );
			
			System.out.print("Brand: ");
			brand = input.next(); // 字串輸入完，直接換下一項，而非nextLine的跳下一項
			cars[i].setBrand(brand); // 存入
			
			System.out.print("Name: ");
			name = input.next();
			cars[i].setName(name);
			
			System.out.print("Max speed: ");
			maxspeed = input.nextFloat();
			cars[i].setMaxSpeed(maxspeed);
			
			System.out.print("Fuel consumption: ");
			fuelconsumption = input.nextFloat();
			cars[i].setFuelConsumption(fuelconsumption);
			
			System.out.print("Price: ");
			price = input.nextFloat();
			cars[i].setPrice(price);
			System.out.println(" ");// 空一行
		}
		
		for (int i = 0; i < number; i++)
		{
			System.out.println("Output car status ("+ (i+1) + ")");
			System.out.println("Car brand is "+ cars[i].getBrand() + ".");
			System.out.println("Car name is "+ cars[i].getName() + ".");
			System.out.println("Car max speed is "+ cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "+ cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is "+ cars[i].getPrice() + ".");
			System.out.println("Car name is "+ cars[i].getName() + ".");
			
			System.out.println("StartTurbo!");// currentSpeed記得將亂數傳入而非新增變數，若新增變數則無法讀取
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println(" ");
		}
		input.close();
	}

}
