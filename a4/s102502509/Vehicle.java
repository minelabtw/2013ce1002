package ce1002.a4.s102502509;

public class Vehicle 
{
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	//回傳值到呼叫的地方
	public float getFuelConsumption() 
	{
		return fuelConsumption;
	}
	
	public float getPrice() 
	{
		return price;
	}
	
	public String getBrand() 
	{
		return brand;
	}
	public String getName() 
	{
		return name;
	}
	public float getMaxSpeed() 
	{
		return maxSpeed;
	}

	// 1.儲存使用者輸入值 2.this:為避免compiler認錯，而指向本class的標的物
	public void setBrand(String brand) 
	{
		this.brand = brand;
	}
	
	public void setName(String name) 
	{
		
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) 
	{
		this.maxSpeed = maxSpeed;
	}
	
	public void setFuelConsumption(float fuelConsumption) 
	{
		this.fuelConsumption = fuelConsumption;
	}
	
	public void setPrice(float price) 
	{
		this.price = price;
	}
	
	//public float currentSpeed()
	//{
		
	//}
	// 產生浮點樹亂數 回傳結果
	public float currentSpeed(boolean isTurbo)
	{
		
		float i = 0;
		i = (float)(Math.random()*1);
		
		if(isTurbo == true)
		{
			return maxSpeed;
		}
		else
		{
			return maxSpeed*i;
		}
	}
}
