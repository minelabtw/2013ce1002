package ce1002.a4.s102502016;
import java.util.Random;
public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	Random ran = new Random();
	
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String newbrand) {
		brand = newbrand;
	}
	public void setName(String newname) {
		name = newname;
	}
	public void setMaxSpeed(float newmaxSpeed) {
		maxSpeed = newmaxSpeed;
	}
	public void setFuelConsumption(float fuel){
		fuelConsumption = fuel;
	}
	public void setPrice(float newprice){
		price = newprice;
	}
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		return 0;
	}
	public float currentSpeed(boolean isTurbo) {	//回傳速度
		if(isTurbo)
		{
			return maxSpeed;
		}
		else
		{
			return maxSpeed*ran.nextFloat();//random的nextFloat()為0~1的隨機(含小數)
		}
	}
}
