package ce1002.a4.s102502016;
import java.util.Scanner;
import ce1002.a4.s102502016.Car;
public class A4 {

	public static void main(String[] args) {
		int number;
		float speed,fuelConsumption,price;
		String brand,name;
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		number = input.nextInt();
		while (number<1)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}
		Car Car[] = new Car[number];//宣告物件陣列
		for (int i=0;i<number;i++){
		Car[i] = new Car(i);//清空
		System.out.print("Input car's"+'\n'+"Brand: ");//'\n' 換行
		brand = input.next();
		Car[i].setBrand(brand);
		System.out.print("Name: ");
		name = input.next();
		Car[i].setName(name);
		System.out.print("Max speed: ");
		speed = input.nextFloat();
		Car[i].setMaxSpeed(speed);
		System.out.print("Fuel consumption: ");
		fuelConsumption = input.nextFloat();
		Car[i].setFuelConsumption(fuelConsumption);
		System.out.print("Price: ");
		price = input.nextFloat();
		Car[i].setPrice(price);
		System.out.println();
		}
		System.out.println("Output car status");
		for (int i=0;i<number;i++){
		System.out.println("Car brand is "+Car[i].getBrand()+".");
		System.out.println("Car name is "+Car[i].getName()+".");
		System.out.println("Car max speed is "+Car[i].getMaxSpeed()+".");
		System.out.println("Car fuel consumption is "+Car[i].getFuelConsumption()+".");
		System.out.println("Car price is "+Car[i].getPrice()+'\n'+"StartTurbo!");
		Car[i].startTurbo();
		System.out.println("Turbo status is "+Car[i].isTurbo()+".");
		System.out.println("Current speed is "+Car[i].currentSpeed(Car[i].isTurbo())+".");
		System.out.println();
		}
	}

}
