package ce1002.a4.s102502032;

import java.util.Scanner;

public class A4
{
	public static void main(String[] args)
	{
		Scanner jin = new Scanner(System.in);
		int numbers = getSize(0, "Please input the number of cars: ", jin);
		Car carList[] = new Car[numbers];
		carList = setList(numbers, jin);
		printList(carList);
		jin.close();
	}

	// print data
	public static void printList(Car List[])
	{
		System.out.println("Output car status.");
		for (int i = 0; i < List.length; i ++)
		{
			System.out.println("Car brand is " + List[i].getBrand() + ".");
			System.out.println("Car name is " + List[i].getName() + ".");
			System.out.println("Car max speed is " + List[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ List[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + List[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + List[i].getIsTurbo() + ".");
			System.out.println("Current speed is "
					+ List[i].currentSpeed(List[i].getIsTurbo()) + ".");
			System.out.println();
		}
	}

	// setting data
	public static Car[] setList(int size, Scanner jin)
	{
		Car List[] = new Car[size];
		for (int i = 0; i < size; i ++)
		{
			System.out.println("Input car's");
			List[i] = new Car(i);
			System.out.print("Brand: ");
			List[i].setBrand(jin.next());
			System.out.print("Name: ");
			List[i].setName(jin.next());
			System.out.print("Max speed: ");
			List[i].setMaxSpeed(jin.nextFloat());
			System.out.print("Fuel consumption: ");
			List[i].setFuelConsumption(jin.nextFloat());
			System.out.print("Price: ");
			List[i].setPrice(jin.nextFloat());
			List[i].setIsTurbo();
			System.out.println();
		}
		return List;
	}

	// get size; return int value
	public static int getSize(int min, String str, Scanner jin)
	{
		int size = 0;
		do
		{
			System.out.println(str);
			size = jin.nextInt();
			if (size <= min)
				System.out.println("Out of range!");
		}
		while (size <= min);
		return size;
	}
}
