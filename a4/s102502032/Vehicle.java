package ce1002.a4.s102502032;

import java.util.Random;

public class Vehicle
{
	private String			brand;
	private String			name;
	private float			maxSpeed;
	private float			fuelConsumption;
	private float			price;
	private static Random	rnd	= new Random(System.currentTimeMillis());

	// constructor
	public Vehicle()
	{
	}

	// set
	public void setBrand(String brand)
	{
		this.brand = brand;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setFuelConsumption(Float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}

	public void setPrice(Float price)
	{
		this.price = price;
	}

	public void setMaxSpeed(float maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}

	// get
	public String getBrand()
	{
		return brand;
	}

	public String getName()
	{
		return name;
	}

	public float getMaxSpeed()
	{
		return maxSpeed;
	}

	public float getFuelConsumption()
	{
		return fuelConsumption;
	}

	public float getPrice()
	{
		return price;
	}

	// method
	public float currentSpeed()
	{
		return maxSpeed * (rnd.nextInt(10001) % 10001) / 10000;
	}

	public float currentSpeed(boolean isTurbo)
	{
		if (isTurbo == true)
		{
			return maxSpeed;
		}
		else
		{
			return maxSpeed * (rnd.nextInt(10001) % 10001) / 10000;
		}
	}
}
