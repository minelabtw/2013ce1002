package ce1002.a4.s102502032;

import java.util.Random;

public class Car extends Vehicle
{
	private int		id;
	private float	maxSpeed;
	private boolean	isTurbo;

	// constructor
	public Car()
	{
		super();
	}

	Car(int carID)
	{
		id = carID;
	}

	// set
	public void setMaxSpeed(float maxSpeed)
	{
		super.setMaxSpeed(maxSpeed);
		this.maxSpeed = maxSpeed;
	}

	public void setIsTurbo()
	{
		Random rnd = new Random(System.currentTimeMillis());
		isTurbo = rnd.nextBoolean();
	}

	// get
	int getID()
	{
		return id;
	}

	public float getMaxSpeed()
	{
		return maxSpeed;
	}

	public boolean getIsTurbo()
	{
		return isTurbo;
	}
}
