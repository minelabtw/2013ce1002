package ce1002.a4.s995001561;


import java.util.Scanner;

import ce1002.a4.s995001561.Car;

public class A4 {

	public static void main(String[] args) {
		
		// input parameters
		int n;
		String Brand = new String();
		String Name = new String();
		float maxspeed;
		float fuelConsumption;
		float price;
		
		// ask for car number
		System.out.println( "Please input the number of cars: " );
		Scanner input = new Scanner(System.in);	
		n = input.nextInt();
		
		// check car number is legal
		while(n<1) {
			System.out.println( "Out of range!\nPlease input the number of cars: " );
			n = input.nextInt();
			}
		
		// create car array
		Car[] carArray = new Car[n];
		
		
		// input the data to cars
		for(int i=0;i<n;i++){
		
			carArray[i] = new Car();
		// input Brand
		System.out.println( "Input car's" );
		System.out.print( "Brand: " );
		Brand = input.next();
		carArray[i].setBrand(Brand);
		
		// input Name
		System.out.print( "Name: " );
		Name = input.next();
		carArray[i].setName(Name);
		
		// input MaxSpeed
		System.out.print( "Max speed: " );
		maxspeed = input.nextFloat();
		carArray[i].setMaxSpeed(maxspeed);
		
		// input FuelConsumption
		System.out.print( "Fuel consumption: " );
		fuelConsumption = input.nextFloat();
		carArray[i].setFuelConsumption(fuelConsumption);
		
		// input Price
		System.out.print( "Price: " );
		price = input.nextFloat();
		carArray[i].setPrice(price);
		
		System.out.print("\n");
        }
		
		System.out.println("Output car status");
		
		// output data of cars
		for(int i=0;i<n;i++){
			
			System.out.println("Car brand is " + carArray[i].getBrand() + ".");
			System.out.println("Car name is " + carArray[i].getName() + ".");
			System.out.println("Car max speed is " + carArray[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption " + carArray[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + carArray[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			carArray[i].startTurbo();
			System.out.println("Turbo status is " + carArray[i].isTurbo() + ".");
			// if Turbo status is true, output max speed
			// if Turbo status is false, output max speed*random
			if(carArray[i].isTurbo()==true) {
				System.out.println("Current speed is " + carArray[i].getMaxSpeed() + ".");
			}
			else {
			System.out.println("Current speed is " + carArray[i].getMaxSpeed()*Math.random() + ".");
			}
			System.out.print("\n");
			
		}
		
		input.close();
	}

}
