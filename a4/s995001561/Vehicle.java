package ce1002.a4.s995001561;

public class Vehicle {
	
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	//設定品牌
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	//設定名稱
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	//設定最高速度
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	//設定油耗
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	
	//設定價格
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}



}
