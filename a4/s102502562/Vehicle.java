package ce1002.a4.s102502562;

import java.util.Random;

public class Vehicle {
	private String Brand;
	private String Name;
	private Float Max_Speed;
	private Float Fuel_consumption;
	private Float Price;
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand)
	{
		Brand=brand;
	}
	public void setName(String name)
	{
		Name=name;
	}
	public void setMaxSpeed(float maxSpeed)
	{
		Max_Speed=maxSpeed;
	}
	public void setFuelconsumption(float fuelconsumption)
	{
		Fuel_consumption=fuelconsumption;
	}
	public void setPrice(float price)
	{
		Price=price;
	}
	//取得品牌、名稱、最高速度、油耗、價格
	public String getBrand()
	{
		return Brand;
	}
	public String getName()
	{
		return Name;
	}
	public float getMaxSpeed()
	{
		return Max_Speed;
	}
	public float getFuelconsumption()
	{
		return Fuel_consumption;
	}
	public float getPrice()
	{
		return Price;
	}
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed()
	{
		Random random=new Random();
		return Max_Speed*random.nextFloat();
	}
	public float currentSpeed(boolean isTurbo)
	{
		if(isTurbo==true)
		{
			return Max_Speed;
		}
		else
		{
			Random random=new Random();
			return Max_Speed*random.nextFloat();
		}
	}
}
