package ce1002.a4.s102502562;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number;  
		Car[] cars;
		do//讓使用者輸入要幾台車子並判斷是否在範圍內
		{
			System.out.println("Please input the number of cars: ");
			number=input.nextInt();	
			if(number<=0)
			{
				System.out.println("Out of range!");
			}
		}while(number<=0);
		cars=new Car[number];
		for(int i=0;i<number;i++)//讓使用者輸入車的屬性並存入陣列裡
		{
			Car car=new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car.setBrand(input.next());
			System.out.print("Name: ");
			car.setName(input.next());
			System.out.print("Max speed: ");
			car.setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car.setFuelconsumption(input.nextFloat());
			System.out.print("Price: ");
			car.setPrice(input.nextFloat());
			cars[i]=car;
			System.out.println("");
		}
		System.out.println("Output car status");
		for(int i=0;i<number;i++)//輸出車的屬性
		{
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelconsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println("");
		}
	}

}
