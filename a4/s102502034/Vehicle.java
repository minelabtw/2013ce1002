package ce1002.a4.s102502034;

import java.util.Random;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 名字
	private float maxSpeed; //  最高速
	private float fuelConsumption; // 油耗
	private float price; // 價格

	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;

	}

	public void setName(String name) {
		this.name = name;

	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;

	}

	public void setprice(float price) {
		this.price = price;

	}

	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;

	}

	//取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;

	}

	public String getName() {
		return name;

	}

	public float getMaxSpeed() {
		return maxSpeed;

	}

	public float getfuelConsumption() {
		return fuelConsumption;

	}

	public float getprice() {
		return price;

	}

	//取得目前速度(多載：汽車有渦流，但機車沒有。)

	public float currentSpeed() { //給之後有關機車的class使用，A4不會使用到。
		Random ran = new Random();
		return maxSpeed * ran.nextFloat();
	}

	public float currentSpeed(boolean isTurbo) {
		Random ran = new Random(); 
		if (isTurbo == true) {  //若為有Turbo 輸出maxSpeed

			return maxSpeed * 1;

		} else {
			return maxSpeed * ran.nextFloat();  //若無 則輸出maxSpeed * 0<=亂數<1
		}

	}
}
