package ce1002.a4.s102502034;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int NumOfCars;
		float Speed, FuelCumsumption, Price;
		// 設定變數
		String Brand, Name;
		System.out.println("Please input the number of cars:");
		NumOfCars = scan.nextInt();

		while (NumOfCars <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			NumOfCars = scan.nextInt();
		}
		// 輸入題目所需
		Car[] car = new Car[NumOfCars]; // 創立一個Array of Objects
		for (int i = 0; i <= NumOfCars - 1; i++) {
			car[i] = new Car();
			System.out.println("Input car's");

			System.out.print("Brand: ");
			Brand = scan.next();
			car[i].setBrand(Brand);

			System.out.print("Name: ");
			Name = scan.next();
			car[i].setName(Name);

			System.out.print("Max speed: ");
			Speed = scan.nextFloat();
			car[i].setMaxSpeed(Speed);

			System.out.print("Fuel Cumsumption: ");
			FuelCumsumption = scan.nextFloat();
			car[i].setfuelConsumption(FuelCumsumption);

			System.out.print("Price: ");
			Price = scan.nextFloat();
			car[i].setprice(Price);
			car[i].startTurbo();
			car[i].currentSpeed(car[i].isTurbo());
			System.out.println();
		}
		// 要求使用者輸入題目所需
		System.out.println("Output car status");
		for (int i = 0; i <= NumOfCars - 1; i++) {
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out
					.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel cumsumption is "
					+ car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getprice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");

			System.out.println("Current speed is "
					+ car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println();
			// 輸出題目所需
		}

	}
}
