package ce1002.a4.s102502034;
import java.util.Random;
public class Car extends Vehicle {
	private boolean isTurbo;

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		boolean a;
		Random ran = new Random();
		a = ran.nextBoolean();
		if (a == true) {
			isTurbo = true;
		} else if (a == false) {
			isTurbo = false;

		}
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;

	}

}
