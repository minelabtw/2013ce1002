package ce1002.a4.s102502036;
import java.util.Random;
public class Vehicle {
	
	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;	
	//設定品牌、名稱、最高速度、油耗、價格
	Random ran = new Random() ;
	public Vehicle(){
		
	}
	public void setBrand(String brand) {
		this.brand = brand ;
	}
	public void setName(String name) {
		this.name = name ;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed ;
	}
	public void setfuelConsumption(float c) {
		fuelConsumption = c ;
	}
	public void setprice(float c) {
		price = c ;
	}	
	
	//回傳值
	public String getBrand() {
		return brand ;
		
	}
	public String getName() {
		return name ;
	}
	public float getMaxSpeed() {
		return maxSpeed ;
		}
	public float getfuelConsumption() {
		return fuelConsumption ;
		}
	public float getprice() {
		return price ;
		}	
	
	public float currentSpeed() {	
		return maxSpeed*ran.nextFloat() ;
	}
	public float currentSpeed(boolean isTurbo) {	
	    if(isTurbo==true){
	    	return maxSpeed ;
	    }
	    else {
	    	return maxSpeed*ran.nextFloat() ;
	    }
	}

}
