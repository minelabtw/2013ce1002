package ce1002.a4.s102502001;
import java.util.Random;
public class Car extends Vehicle {
	
	public Car(){
		
	}
	
	private boolean isTurbo;
	private Random rnd=new Random();
	
	public void startTurbo() {	//test isturbo(true or false)
		isTurbo=rnd.nextBoolean();
	}
	public boolean isTurbo() {	//get isturbo
		return isTurbo;
	}
}
