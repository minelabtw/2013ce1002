package ce1002.a4.s102502001;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		int num;
		String Brand;
		String Name;
		float MaxSpeed;
		float Fuelconsumption;
		float Price;
		Car[] cars;
		
		
		do{                        //input variable 
			System.out.println("Please input the number of cars: ");
			num = scanner.nextInt();
			if(num<0){
				System.out.println("Out of range!");
			}
			}while(num<0);
			
			cars= new Car[num];     //declare array
			
			for(int i=0;i<num;i++){ //input variable
				Car car = new Car();
				System.out.println("Input car's\nBrand: ");
				Brand = scanner.next();
				System.out.println("Name: ");
				Name = scanner.next();
				System.out.println("Max speed: ");
				MaxSpeed = scanner.nextFloat();
				System.out.println("Fuel consumption: ");
				Fuelconsumption = scanner.nextFloat();
				System.out.println("Price: ");
				Price = scanner.nextFloat();
				System.out.println();
				car.setBrand(Brand);    //using class of car to store the input 
				car.setName(Name);
				car.setMaxSpeed(MaxSpeed);
				car.setFuelConsumption(Fuelconsumption);
				car.setPrice(Price);
				car.startTurbo();
				cars[i]=car;
			}
			scanner.close();           //close it
			
			System.out.println("Output car status");
			
			for(int j=0;j<num;j++){
				System.out.println("Car brand is " + cars[j].getBrand() + ".");
				System.out.println("Car name is " + cars[j].getName() + ".");
				System.out.println("Car max speed is " + cars[j].getMaxSpeed() + ".");
				System.out.println("Car fuel consumption " + cars[j].getFuelConsumption() + ".");
				System.out.println("Car price is " + cars[j].getPrice() + ".");
				System.out.println("StartTurbo!\nTurbo status is " + cars[j].isTurbo()+".");
				System.out.println("Current speed is " + cars[j].currentSpeed(cars[j].isTurbo())+".\n");
			}
		}
	}