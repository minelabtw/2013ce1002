package ce1002.a4.s102502001;

public class Vehicle {

	
	private String brand;	//brand
	private String name;	//name
	private float maxSpeed;	//maxspeed
	private float fuelConsumption;	//fuelconsumption
	private float price;	//price

	//set all the input above
	public void setBrand(String brand) {
		this.brand=brand;
	}
	public void setName(String name) {
		this.name=name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption){
		this.fuelConsumption=fuelConsumption;
	}
	public void setPrice(float price){
		this.price=price;
	}
	
	 
	//get all the input above	
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
		
	//get currentSpeed(cars need to set isturbo, scooters don't)
	public float currentSpeed(float currentSpeed) {	//give it to class of scooters
		return 0;
	}
	float currentSpeed;
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo==true){
			currentSpeed=maxSpeed;
		}
		else{
			float rate;
			rate=(float)Math.random()*1;
			currentSpeed=maxSpeed*rate;
		}
		return currentSpeed;
	}


	

}
