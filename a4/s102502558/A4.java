package ce1002.a4.s102502558;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n;
		// input 
		while(true)
		{
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
			if (n > 0) 
				break;
			System.out.println("Out of range!");
		}
		Car[] car = new Car[n];
		// setup cars
		for (int i=0;i<n;i++)
		{
			car[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(input.next());
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(input.nextFloat());
			System.out.println();
		}
		System.out.print("Output car status");
		// print status
		for (int i=0;i<n;i++)
		{
			System.out.println();
			System.out.println("Car brand is " + car[i].getBrand()  + ".");
			System.out.println("Car name is " + car[i].getName()  + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed()  + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()));
		}
		// close input
		input.close();
	}

}
