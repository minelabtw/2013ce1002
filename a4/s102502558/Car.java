package ce1002.a4.s102502558;

import java.util.Random;
public class Car extends Vehicle {
	private boolean isTurbo;	// 渦輪狀態
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		Random rnd = new Random();
		isTurbo = rnd.nextInt(2) == 0 ? true : false;
	}
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		return isTurbo;
	}
	
}
