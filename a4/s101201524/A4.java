package ce1002.a4.s101201524;
import java.util.Scanner;

public class A4 {
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		int number;
		//ask the number of cars
		do{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number < 1)
				System.out.println("Out of range!");
		}while(number < 1);
		//declare car matrix with size number
		Car[] cars = new Car[number];
		//ask and set data of cars
		for(int count = 0; count < number; count++){
			cars[count] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cars[count].setBrand(input.next());
			System.out.print("Name: ");
			cars[count].setName(input.next());
			System.out.print("Max speed: ");
			cars[count].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			cars[count].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			cars[count].setPrice(input.nextFloat());
			System.out.println();
		}
		//output car's data
		System.out.println("Output car status");
		for(int count = 0; count < number; count++){
			System.out.println("Car brand is " + cars[count].getBrand());
			System.out.println("Car name is " + cars[count].getName());
			System.out.println("Car max speed is " + cars[count].getMaxSpeed());
			System.out.println("Car fuel consumption is " + cars[count].getFuelConsumption());
			System.out.println("Car price is " + cars[count].getPrice());
			System.out.println("StartTurbo!");
			cars[count].startTurbo();
			System.out.println("Turbo status is " + cars[count].isTurbo());
			System.out.println("Current speed is " + cars[count].currentSpeed(cars[count].isTurbo()) + "\n");
		}
		input.close();
	}
}
