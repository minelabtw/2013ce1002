package ce1002.a4.s101201524;

public class Car extends Vehicle{
	private boolean isTurbo;
	public Car(){
	}
	//use random to set true or false of isTurbo
	public void startTurbo(){  
		if((int)Math.random()*2 == 1)
			isTurbo = true;
		else
			isTurbo = false;
	}
	// get isTurbo
	public boolean isTurbo(){
		return isTurbo;
	}
}
