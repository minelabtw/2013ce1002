package ce1002.a4.s101201524;

public class Vehicle {
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	//set brand
	public void setBrand(String brand) {
		this.brand = brand;
	}
	//set name
	public void setName(String name) {
		this.name = name;
	}
	//set maxSpeed
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	//set FuelConsumption
	public void setFuelConsumption(float fuelConsumption){
		this.fuelConsumption =  fuelConsumption;
	}
	//set price
	public void setPrice(float price){
		this.price = price;
	}
	//get brand
	public String getBrand() {
		return brand;
	}
	//get name
	public String getName() {
		return name;
	}
	//get maxSpeed
	public float getMaxSpeed() {
		return maxSpeed;
	}
	//get fuelConsumption
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	//get price
	public float getPrice(){
		return price;
	}
	//get motorcycle's currentSpeed
	public float currentSpeed() {
		return maxSpeed*(float)Math.random();
	}
	//get car's currentSpeed by isTurbo
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo == true){
			return maxSpeed;
		}
		else
			return maxSpeed*(float)Math.random();
	}
}
