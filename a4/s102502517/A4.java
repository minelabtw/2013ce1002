package ce1002.a4.s102502517;

import java.util.Scanner; //import Scanner
import ce1002.a4.s102502517.Car; //import Car.java

public class A4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberofcar = 0;
		
		do //輸入車數
		{
		System.out.println("Please input the number of cars: ");
		numberofcar = scanner.nextInt();
		if(numberofcar<=0)
			System.out.println("Out of range!");
		}
		while(numberofcar<=0);
		
		Car[] car = new Car[numberofcar]; //宣告 Car矩陣
		
		for(int i=0; i<=numberofcar-1; i++) //實體化 car[數字]
			car[i] = new Car();
		
		for(int j=0; j<=numberofcar-1; j++) //輸入汽車資料
		{
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[j].setBrand(scanner.next());
			System.out.print("Name: ");
			car[j].setName(scanner.next());
			System.out.print("Max speed: ");
			car[j].setMaxSpeed(scanner.nextFloat());
			System.out.print("Fuel consumption: ");
			car[j].setFuelConsumption(scanner.nextFloat());
			System.out.print("Price: ");
			car[j].setPrice(scanner.nextFloat());
			System.out.println("");
		}
		
		for(int k=0; k<=numberofcar-1; k++) //輸出汽車資料
		{
			System.out.println("Output car status");
			System.out.println("Car brand is " + car[k].getBrand() +".");
			System.out.println("Car name is " + car[k].getName() + ".");
			System.out.println("Car max speed is " + car[k].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[k].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[k].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[k].startTurbo();
			System.out.println("Turbo status is "+ car[k].isTurbo() + ".");
			System.out.println("Current speed is " + car[k].currentSpeed(car[k].isTurbo()) + ".");
			System.out.println("");
		}
	}

}
