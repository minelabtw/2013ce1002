package ce1002.a4.s102502517;

import ce1002.a4.s102502517.Vehicle; //import Vehicle.java
import java.util.Random; //使用Random

public class Car extends Vehicle {
	private boolean isTurbo;
	Random rand = new Random();
	
	public Car()
	{
	}
	
	public void startTurbo() //決定渦輪是否啟動
	{
		int random_number = rand.nextInt(2);
		if(random_number==0)
			isTurbo = true;
		else
			isTurbo = false;
	}
	
	public boolean isTurbo()
	{
		return isTurbo;
	}
}
