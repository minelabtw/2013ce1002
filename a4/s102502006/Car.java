package ce1002.a4.s102502006;

import java.util.Random;

public class Car extends Vehicle{
	private boolean isTurbo;
	
	public Car(){
	
	}
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		
		Random random = new Random();
		
		isTurbo = (random.nextInt(2)==0) ? true : false;	// isturbo's value	  
	}
	public boolean getisTurbo() {	// 取得渦輪目前狀態	
		return this.isTurbo;  
	}
}
