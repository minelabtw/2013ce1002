package ce1002.a4.s102502006;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		
		int num=0; // number of cars
		
		while(num<1){ // input cars
			System.out.println("Please input the number of cars: ");
			num = scanner.nextInt();
			if(num<1)System.out.println("Out of range!");
		}
		
		Car[] cars = new Car[num]; 
		
		for(int i=0;i<num;i++){
			Car car = new Car(); 
			car.startTurbo();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car.setBrand(scanner.next());
			System.out.print("Name: ");
			car.setName(scanner.next());
			System.out.print("Max speed: ");
			car.setMaxSpeed(scanner.nextFloat());
			System.out.print("Fuel consumption: ");
			car.setFuelConsumption(scanner.nextFloat());
			System.out.print("Price: ");
			car.setPrice(scanner.nextFloat());
			System.out.println();
		
			cars[i] = car;		// put in 
		}
		
		for(int i=0;i<num;i++){
			System.out.println("Output car status");
			System.out.println("Car brand is " + cars[i].getBrand());
			System.out.println("Car name is  " + cars[i].getName());
			System.out.println("Car max speed is " + cars[i].getMaxSpeed());
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption());
			System.out.println("Car price is " + cars[i].getPrice());
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[i].getisTurbo());
			System.out.println("Current speed is "+ cars[i].currentSpeed(cars[i].getisTurbo())) ;
			System.out.println();
		}
	}
}
