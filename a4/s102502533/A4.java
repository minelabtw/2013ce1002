package ce1002.a4.s102502533;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Car[] cars;// class array
		int numOfCars;// 賽車陣列
 
		do {
			System.out.println("Please input the number of cars: ");// 幾台賽車
			numOfCars = input.nextInt();
			if (numOfCars <= 0)
				System.out.println("Out of range!");
		} while (numOfCars <= 0);
 
		cars = new Car[numOfCars];
		for (int i = 0; i < cars.length; i++) {
			Car car = new Car();//跑CAR class
			System.out.println("Input car's");
			//要求使用者輸入
			String brands;
			System.out.print("Brand is ");
			brands = input.next();
			car.setBrand(brands);
			
			String names;
			System.out.print("Name: ");
			names = input.next();
			car.setName(names);
			
			float maxSpeeds=0;
			System.out.print("Max speed: ");
			maxSpeeds = input.nextFloat();
			car.setMaxSpeed(maxSpeeds);
			
			float fuelConsumptions;
			System.out.print("Fuel consumption: ");
			fuelConsumptions = input.nextFloat();
			car.setfuelConsumption(fuelConsumptions);
			
			float prices;
			System.out.print("Price: ");
			prices = input.nextFloat();
			car.setprice(prices);
			
			car.startTurbo();
			cars[i] = car;// 放入array裡
			System.out.println();
		}
		input.close();// 用不到了，要關掉
 
		System.out.println("Output car status.");// 賽車狀態
 
		for (int i = 0; i < cars.length; i++) {
			System.out.println("Car brand is" + cars[i].getBrand() + ".");//顯示品牌
			System.out.println("Car name is " + cars[i].getName() + ".");//顯示名字
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");//顯示最大速度
			System.out.println("Car fuel consumption is " + cars[i].getfuelConsumption()+ ".");//顯示油耗
			System.out.println("Car price is " + cars[i].getprice() + ".");//顯示價格
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");// 顯示渦輪
			System.out.println("Current speed is " + cars[i].getspeed2() + ".");//顯示目前速度
			System.out.println();
		}
 

	}

}
