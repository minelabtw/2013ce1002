package ce1002.a4.s102502535;

import java.util.Random; // import random

public class Car extends Vehicle {

	private boolean isTurbo;

	public Car() {
	}

	public void startTurbo() {
		Random turbo = new Random();
		isTurbo = turbo.nextBoolean();
	} // test if turbo is started

	public boolean isTurbo() {
		return isTurbo;
	} // get the status of turbo
}
