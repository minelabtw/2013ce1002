package ce1002.a4.s102502535;

import java.util.Random; // import random

public class Vehicle {

	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;

	public void setBrand(String brand) {
		this.brand = brand;
	} // save brand

	public void setName(String name) {
		this.name = name;
	} // save name

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	} // save max speed

	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	} // save fuel consumption

	public void setPrice(float price) {
		this.price = price;
	} // save price

	public String getBrand() {
		return brand;
	} // get brand

	public String getName() {
		return name;
	} // get name

	public float getMaxSpeed() {
		return maxSpeed;
	} // get max speed

	public float getFuelConsumption() {
		return fuelConsumption;
	} // get fuel consumption

	public float getPrice() {
		return price;
	} // get price

	public float currentSpeed() {
		return maxSpeed;
	}

	public float currentSpeed(boolean isTurbo) {

		Random Turbo = new Random();

		if (isTurbo == false)
			return maxSpeed * Turbo.nextFloat();
		else
			return maxSpeed;
	} // determine the status of turbo and return the speed
}
