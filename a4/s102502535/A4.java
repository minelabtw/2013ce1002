package ce1002.a4.s102502535;

import java.util.Scanner; // import scanner

public class A4 {

	public static void main(String[] args) {

		int carnum = 0;
		String brand;
		String name;
		float maxSpeed;
		float fuelConsumption;
		float price;
		Car[] cars; // declare class array

		Scanner input = new Scanner(System.in);

		System.out.println("Please input the number of cars: ");
		carnum = input.nextInt();
		while (carnum <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			carnum = input.nextInt();
		} // if carnum<0, enter the number again and again until it's reasonable

		cars = new Car[carnum];

		for (int i = 0; i < cars.length; i++) {
			Car car = new Car();
			cars[i] = car;
			System.out.println("Input car's ");
			System.out.print("Brand: ");
			brand = input.next();
			cars[i].setBrand(brand);
			System.out.print("Name: ");
			name = input.next();
			cars[i].setName(name);
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			cars[i].setMaxSpeed(maxSpeed);
			System.out.print("Fuel comsumption: ");
			fuelConsumption = input.nextFloat();
			cars[i].setFuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			price = input.nextFloat();
			cars[i].setPrice(price);
			cars[i].startTurbo();
			System.out.println();
		} // input each car's data and save them

		System.out.println("Output car status");
		for (int i = 0; i < cars.length; i++) {
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel sonsumption is "
					+ cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println();
		} // output each car's data

		input.close(); // close the scanner
	}

}
