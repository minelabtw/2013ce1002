package ce1002.a4.s102502547;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int a = 0;
		do {
			System.out.println("Please input the number of cars: "); // 輸入車數，範圍不合的話重新輸入
			a = scanner.nextInt();
			if (a < 1)
				System.out.println("Out of range!");
		} while (a < 1);
		
		Car[] x = new Car[a];
		for (int i = 0; i < a; i++) {
			Car car = new Car();
			System.out.print("Input car's\nBrand: ");
			car.setBrand(scanner.next()); // 設定品牌
			System.out.print("Name: ");
			car.setName(scanner.next()); // 設定名稱
			System.out.print("Max speed: ");
			car.setMaxSpeed(scanner.nextFloat()); // 設定最大速度
			System.out.print("Fuel consumption: ");
			car.setFuelConsumption(scanner.nextFloat()); // 設定油耗
			System.out.print("Price: ");
			car.setPrice(scanner.nextFloat()); // 設定價錢
			x[i] = car;
			System.out.println();
		}
		scanner.close();
		System.out.println("Output car status");
		for (int i = 0; i < a; i++) {
			System.out.println("Car brand is "+x[i].getBrand()+".");
			System.out.println("Car name is "+x[i].getName()+".");
			System.out.println("Car max speed is "+x[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+x[i].getFuelConsumption()+".");
			System.out.println("Car price is "+x[i].getPrice()+".");
			x[i].startTurbo();
			System.out.println("StartTurbo!\nTurbo status is "+x[i].isTurbo()+".");
			System.out.println("Current speed is "+x[i].currentSpeed(x[i].isTurbo())+".\n");
		}
		
	}

}
