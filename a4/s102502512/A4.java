package ce1002.a4.s102502512;

import java.util.*;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Car ca = new Car();
		int n=0;
		String bra;
		String na;
		float spe=0;
		float fue=0;
		float pri=0;
		boolean de;
		do
		{
		System.out.println("Please input the number of cars:");							//Line 20-48: ask user to input the car's contain
		n = input.nextInt();
		if(n<1)
		{
			System.out.println("Out of range!");
		}
		}while(n<1);
		Vehicle rec[] = new Vehicle[n];
		for(int i=0;i<n;i++)
		{
			System.out.println("Input car's");
			rec[i] = new Vehicle();
			System.out.print("Brand: ");
			bra=input.next();
			rec[i].setBrand(bra);
			System.out.print("Name: ");
			na=input.next();
			rec[i].setName(na);
			System.out.print("Max speed: ");
			spe=input.nextFloat();
			rec[i].setMaxSpeed(spe);
			System.out.print("Fuel consumption: ");
			fue=input.nextFloat();
			rec[i].setFuelConsumption(fue);
			System.out.print("Price: ");
			pri=input.nextFloat();
			rec[i].setPrice(pri);
			System.out.println();
		}
		System.out.println("Output car status");											//Line 49-64: print out the contain of the car and it's current speed
		for(int j=0;j<n;j++)
		{
			System.out.println("Car brand is "+rec[j].getBrand());
			System.out.println("Car name is "+rec[j].getName());
			System.out.println("Car max speed is "+rec[j].getMaxSpeed());
			System.out.println("Car fuel consumption is "+rec[j].getFuelConsumption());
			System.out.println("Car price is "+rec[j].getPrice());
			System.out.println("StartTurbo!");
			ca.starTurbo();
			de=ca.isTurbo();
			System.out.println("Turbo status is "+de);
			System.out.println("Current speed is "+rec[j].currentSpeed(de));
			System.out.println();
		}
		input.close();
	}

}



