package ce1002.a4.s102502512;
import java.util.*;

public class Vehicle {									//Line 4-45: make methods to set and get the contain of car
	Random ran = new Random();
	private float r=0;
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float currentSpeed()
	{
		return 0;
	}
	public float currentSpeed(boolean isTurbo)						//Line 46-56: return the current speed randomly
	{
		if(isTurbo)
		{
			return maxSpeed;
		}
		else
		{
			r=ran.nextFloat();
			return r*maxSpeed;
		}
	}
}
