package ce1002.a4.s102502512;
import java.util.*;

public class Car extends Vehicle{
	private boolean isTurbo;
	public void starTurbo()						//Line 6-10: make a method to get the isTurbo's value randomly
	{
		Random ran = new Random();
		this.isTurbo=ran.nextBoolean();
	}
	public boolean isTurbo()					//Line 11-14: return the value of isTurbo
	{
		return this.isTurbo;
	}
}
