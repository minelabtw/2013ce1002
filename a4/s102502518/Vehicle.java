package ce1002.a4.s102502518;

public class Vehicle {
	public Vehicle() {
		
	}//constructor
	private String brand;//brand
	private String name;//name
	private float maxSpeed;//maxSpeed
	private float fuelConsumption;//fuelConsumption
	private float price;//price
	
	public void setBrand(String brand) {//set the car's brand
		this.brand=brand;
	}
	public void setName(String name) {//set the car's name
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {//set the car's max speed 
		this.maxSpeed=maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption) {//set the car's fuel consumption
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price) {//set the car's price
		this.price = price;
	}
		
	public String getBrand() {//get the car's brand
		return brand;
	}
	public String getName() {//get the car's name
		return name;
	}
	public float getMaxSpeed() {//get the car's max speed
		return maxSpeed;
	}
	public float getFuelConsumption() {//get the car's fuel consumption
		return fuelConsumption;
	}
	public float getPrice() {//get the car's price
		return price;
	}
		
	public float currentSpeed() {//unused in A4
		return 0;
	}
	public float currentSpeed(boolean isTurbo) { //return the current speed	
		if(isTurbo==true)
		    return this.maxSpeed;
		else
			return this.maxSpeed*(float)(Math.random());
	}

}
