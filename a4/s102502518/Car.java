package ce1002.a4.s102502518;

public class Car extends Vehicle {
	public Car() {
		
	}//constructor
	private boolean isTurbo;	// turbo state
	
	public void startTurbo() {// test starting turbo
		
		int value = (int)(Math.random()*2+1);//produce random number
		
		if(value==1)
			this.isTurbo=true;//if random number is 1, then turbo on
		else
			this.isTurbo=false;//if random number is 2, then turbo off
	}
	public boolean isTurbo() {//get turbo state
		return isTurbo;
	}
		
}
