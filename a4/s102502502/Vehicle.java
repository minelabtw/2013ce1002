package ce1002.a4.s102502502;

public class Vehicle {
	private String brand;	         // the brand of Vehicle
	private String name;	         // the name of Vehicle
	private float maxSpeed;	         // the maxSpeed of Vehicle
	private float fuelConsumption;	 // the fuelconsumption of Vehicle
	private float price;	         // the price of Vehicle
	public Vehicle(){                // the constructor of Vehicle
		
	}
	// set the brand, name, maxSpeed, fuelconsumption, price of car 
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	// get the brand, name, maxSpeed, fuelconsumption, price of car 
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public float getPrice() {
		return price;
	}
	//set currentSpeed (overloaded;car has Turbo, but motorcycle doesn't. )
	public float currentSpeed() {	                  //this one for motorcycle
		float q =  (float)(Math.random()*1);         // set random(convert it from double to float)
		return maxSpeed*q;                           // and q is stored in q between o~1
	}
	public float currentSpeed(boolean isTurbo) {     // this one for car (Turbo)
		float q =  (float)(Math.random()*1);         // set random(convert it from double to float)
		if (isTurbo == true){                        // and q is stored in q between o~1
			return maxSpeed;
		}
		else 
			return maxSpeed*q;
	}
}
