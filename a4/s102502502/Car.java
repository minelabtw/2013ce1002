package ce1002.a4.s102502502;

public class Car extends Vehicle{
	public Car(){                    // the constructor of Car
		super();
	}

	private boolean isTurbo;	     // the status of car
	public void startTurbo() {	     // try to active Turbo, and it may false or true.
		int q = (int) (Math.random()*1);       // set random(convert it from double to int) 
		if (q == 0){                           // q is stored in q between o~1
			isTurbo = false;
		}
		else 
			isTurbo = true;
	}
	public boolean getTurbo() {	     // get the status of car	
		  return isTurbo;
		}

}
