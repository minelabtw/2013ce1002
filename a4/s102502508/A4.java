package ce1002.a4.s102502508;
import java.util.Scanner ;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input= new Scanner(System.in);
		
		int noc=0 ;
		
		
		do{//利用後測試迴圈使使用者輸入車子的數目並加以判斷是否在合理範圍內
			System.out.println("Please input the number of cars: ");
			noc=input.nextInt();
			
			if(noc<=0)
			{
				System.out.println("Out of range!");
			}
		}while(noc<=0);
		
	   Car[] cars= new Car[noc] ;//建立物件陣列(停車場)
	   
	   for(int i=0 ; i<noc ; i=i+1)
	   {   
		   Car Kart =new Car() ;//建立新車
		   //使使用者在Car的物件中建立每輛車的特性
		   System.out.println("Input car's");
		   System.out.println("Brand: ");
		   Kart.setBrand(input.next());
		   System.out.println("Name: ");
		   Kart.setName(input.next());
		   System.out.println("Max speed: ");
		   Kart.setMaxSpeed(input.nextFloat());
		   System.out.println("Fuel consumption: ");
		   Kart.setFuelConsumption(input.nextFloat());
		   System.out.println("Price: ");
		   Kart.setPrice(input.nextFloat());
		   
		   cars[i]=Kart ;//將所造之新車停入停車場的特定位子
	   }
	       System.out.println("Output car status");
	   for(int j=0 ; j<noc ;j=j+1)
	   {   //利用迴圈搭配Car類別中的函式回傳值一一列出每台被設計過的車子屬性
		   System.out.println("Car brand is "+cars[j].getBrand());
		   System.out.println("Car name is "+cars[j].getName());
		   System.out.println("Car max speed is "+cars[j].getMaxSpeed());
		   System.out.println("Car fuel consumption is "+cars[j].getFuelConsumption());
		   System.out.println("Car price is "+cars[j].getPrice());
		   System.out.println("StartTurbo!");
		   cars[j].startTurbo();//啟動Car類別中的隨機布林值來決定是否啟動渦輪
		   System.out.println("Turbo status is "+cars[j].isTurbo());
		   System.out.println("Current speed is "+cars[j].currentSpeed(cars[j].isTurbo()));
		   System.out.println();
		   
	   }
	   input.close();    
	   

	}

}
