package ce1002.a4.s102502559;
import java.util.Random;
public class Car extends Vehicle{
	private boolean isTurbo;

	public Car() {

	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		Random ran = new Random();
		this.isTurbo = ran.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return this.isTurbo;
	}
}
