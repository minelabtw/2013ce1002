package ce1002.a4.s102502559;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		System.out.println("Please input the number of cars: ");
		//新增一個Scanner類別的變數input 當作輸入器
		Scanner input = new Scanner(System.in);
		int car_num = input.nextInt();
		while (car_num <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			car_num= input.nextInt();
		}
		//宣告一個Car類別的陣列叫做car,陣列長度為car_num
		Car[] car = new Car[car_num];
		//將car[i]這個物件的各個屬性利用set函數輸入給Class內的private屬性
		for(int i = 0;i<car_num;i++)
		{
			car[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String _brand = input.nextLine();
			car[i].setBrand(_brand);
			System.out.print("Name: ");
			String _name = input.nextLine();
			car[i].setName(_name);
			System.out.print("\nMax speed: ");
			Float _maxSpeed = input.nextFloat();
			car[i].setMaxSpeed(_maxSpeed);
			System.out.print("\nFuel consumption: ");
			Float _fuelConsumption = input.nextFloat();
			car[i].setfuelConsumption(_fuelConsumption);
			System.out.print("\nPrice: ");
			Float _price = input.nextFloat();
			car[i].setprice(_price);
		}
		System.out.println("");
		//利用get函數return出Class內的private屬性,並且print出來
		for(int i = 0;i<car_num;i++)
		{
			System.out.println("Output car status");
			System.out.println("Car brand is " + car[i].getBrand()+ ".");
			System.out.println("Car name is " + car[i].getName()+ ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed()+ ".");
			System.out.println("Car fuel consumption is " + car[i].getfuelConsumption()+ ".");
			System.out.println("Car price is " + car[i].getprice()+ ".");
			System.out.println("StartTurbo!");
			Boolean _isTurbo = car[i].isTurbo();
			System.out.println("Turbo status is " + _isTurbo + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(_isTurbo) + ".\n");
		}
	}
	
}
