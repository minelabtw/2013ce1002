package ce1002.a4.s102502559;

import java.util.Random;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格
//以下set開頭的函數用於Class以外的地方可呼叫這個函數修改此函數內的private屬性
	public void setBrand(String _brand) {
		this.brand = _brand;
	}

	public void setName(String _name) {
		this.name = _name;
	}

	public void setMaxSpeed(float _maxSpeed) {
		this.maxSpeed = _maxSpeed;
	}
	public void setfuelConsumption(float _fuelConsumption)
	{
		this.fuelConsumption = _fuelConsumption;
	}
	public void setprice(float _price)
	{
		this.price = _price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return this.brand;
	}

	public String getName() {
		return this.name;
	}

	public float getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public float getfuelConsumption()
	{
		return this.fuelConsumption;
	}
	
	public float getprice()
	{
		return this.price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() { // 給之後有關機車的class使用，A4不會使用到。
		return this.maxSpeed;
	}

	public float currentSpeed(boolean isTurbo) {
		Random ran = new Random();
		float product = ran.nextFloat();//宣告變數product的值為隨機產生的float
		
		if (isTurbo == false) {
			return this.maxSpeed * product;
		} 
		else {
			return this.maxSpeed;
		}
	}

}
