package ce1002.a4.s102502526;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setfuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	public void setprice(float price){
		this.price = price;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return this.brand;
	}
	public String getName() {
		return this.name;
	}
	public float getMaxSpeed() {
		return this.maxSpeed;
	}
	public float getfuelConsumption(){
		return this.fuelConsumption;
	}
	public float getprice(){
		return this.price;
	}
		
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	
		return maxSpeed * (float)java.lang.Math.random();
	}
	public float currentSpeed(boolean isTurbo) {	//判斷渦輪的狀態
		if (isTurbo == true)
			return maxSpeed;
		else
			return maxSpeed * (float)java.lang.Math.random();
	}
}
