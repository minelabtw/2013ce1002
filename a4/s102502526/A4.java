package ce1002.a4.s102502526;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner a = new Scanner(System.in);
		int num = 0;
		System.out.print("Please input the number of cars: ");
		num = a.nextInt();
		while (num <= 0){
			System.out.print("Out of range! \nPlease input the number of cars: ");
			num = a.nextInt();
		}
		
		Car[] cars = new Car[num];  //產生一個Car class array 叫做 cars，大小為num
		
		for (int i = 0; i<num ;i++){  //讓使用者設定
			cars[i] = new Car();
			System.out.print("Input car's\n");
			
			System.out.print("Brand: ");
			cars[i].setBrand(a.next());

			System.out.print("Name: ");
			cars[i].setName(a.next());
			
			System.out.print("Max Speed: ");
			cars[i].setMaxSpeed(a.nextFloat())
			;
			System.out.print("Fuel consumption: ");
			cars[i].setfuelConsumption(a.nextFloat());
			
			System.out.print("Price: ");
			cars[i].setprice(a.nextFloat());
			System.out.print("\n");
		}
		//
		System.out.print("Output car status\n");
		//
		for (int i = 0 ; i<num ;i++){  //Output car status
			System.out.print("Car brand is "+cars[i].getBrand()+"\n");
			System.out.print("Car name is "+cars[i].getName()+"\n");
			System.out.print("Car max speed is "+cars[i].getMaxSpeed()+"\n");
			System.out.print("Car fuel consumption is "+cars[i].getfuelConsumption()+"\n");
			System.out.print("Car price is "+cars[i].getprice()+"\n");
			System.out.print("StartTurbo!\n");
			System.out.print("Turbo's status is "+cars[i].isTurbo()+"\n");
			if(cars[i].isTurbo() == true)
				System.out.print("Current speed is "+cars[i].getMaxSpeed()+"\n\n");
			else	
			    System.out.print("Current speed is "+cars[i].currentSpeed()+"\n\n");
			
		}
	}

}
