package ce1002.a4.s102502513;

import java.util.Random;

public class Vehicle 
{
	private String brand;
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;
	
	//設定品牌、名稱、最高速度、油耗、價格
	Vehicle () 
	{
		
	}
	public void setBrand(String brand) 
	{
		this.brand = brand;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setMaxSpeed(float maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}
	
	public void setFuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}
	
	public void setPrice(float price) 
	{
		this.price = price;
	}
	 
	
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand()
	{
		return brand;
	}
	public String getName() 
	{
		return name;
	}
	
	public float getMaxSpeed() 
	{
		return maxSpeed;
	}	
	
	public float getFuelConsumption()
	{
		return fuelConsumption;
	}	
	
	public float getPrice()
	{
		return price;
	}	
	
	//取得速度
	public float Speed(boolean isTurbo) 
	{
		if ( isTurbo == true )
		{
			return maxSpeed;
		}
		
		else 
		{
			Random rand = new Random();
			float i = rand.nextFloat();
			return maxSpeed * i;
		}
	}
}