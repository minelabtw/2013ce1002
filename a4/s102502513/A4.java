package ce1002.a4.s102502513;

import java.util.Scanner;

public class A4 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in); 
		
		int num ; 
		
		do{
		System.out.println("Please input the number of cars: ");
		num = input.nextInt();
		
		if(num <= 0)
		{
			System.out.println("Out of range!");
		}
		
		}while(num <= 0);
		
		Car[] car = new Car[num];  //宣告名為car的陣列
		
		for( int i = 0 ; i < num ; i++ ) 
		{
			Car first = new Car(); 
			
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			String brand = input.next();
			first.setBrand(brand);
			
			System.out.print("Name: ");
			String name = input.next();
			first.setName(name);
			
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			first.setMaxSpeed(maxSpeed);
			
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			first.setFuelConsumption(fuelConsumption);
			
			System.out.print("Price: ");
			float price = input.nextFloat();
			first.setPrice(price);
			
			car[i] = first; 
			System.out.println("");
		}
		input.close(); 
		
		System.out.println("Output car status");
		
		for ( int i = 0 ; i < num ; i++ ) //輸出每台車子的屬性
		{
			System.out.println("Car brand is "+car[i].getBrand()+".");
			System.out.println("Car name is "+car[i].getName()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[i].getFuelConsumption()+".");
			System.out.println("Car price is "+car[i].getPrice()+".");
			System.out.println("StartTurbo!");
			car[i].firstTurbo();
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			boolean turbo = car[i].isTurbo(); 
			System.out.println("Current speed is "+car[i].Speed(turbo));
			System.out.println("");
		}
	}
}
