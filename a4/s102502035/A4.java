package ce1002.a4.s102502035;

import java.util.Scanner;//導入輸入函式庫

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// 建立 scanner class input object
		System.out.println("Please input the number of cars: ");// 提示輸入
		int carnum = input.nextInt();// 輸入

		while (carnum <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");// 再提示輸入
			carnum = input.nextInt();// 輸入
		}
		Car[] cararr = new Car[carnum];// Car class cararr object array
		for (int i = 0; i < carnum; i++) {
			Car car = new Car();// Car class car object
			System.out.println("Input car's");
			System.out.print("Brand: ");// 提示輸入
			car.setBrand(input.next());// 輸入
			System.out.print("Name: ");// 提示輸入
			car.setName(input.next());// 輸入
			System.out.print("Max speed: ");// 提示輸入
			car.setMaxSpeed(input.nextFloat());// 輸入
			System.out.print("Fuel consumption: ");// 提示輸入
			car.setFuelConsumption(input.nextFloat());// 輸入
			System.out.print("Price: ");// 提示輸入
			car.setPrice(input.nextFloat());// 輸入
			System.out.println();
			cararr[i] = car;
		}
		System.out.println("Output car status");
		for (int i = 0; i < carnum; i++) {// 依題目要求output
			System.out.println("Car brand is " + cararr[i].getBrand() + ".");
			System.out.println("Car name is " + cararr[i].getName() + ".");
			System.out.println("Car max speed is " + cararr[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cararr[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cararr[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cararr[i].startTurbo();// 開Turbo
			System.out.println("Turbo status is " + cararr[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cararr[i].currentSpeed(cararr[i].isTurbo()) + ".");
			System.out.println();
		}
	}
}
