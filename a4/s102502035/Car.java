package ce1002.a4.s102502035;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;

	public Car() {
		super();
		isTurbo = false;
	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		Random rand = new Random();// 隨機
		isTurbo = rand.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
