package ce1002.a4.s102502543;

import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String newBrand) {
		brand=newBrand;
	}
	public void setName(String newName) {
		name=newName;
	}
	public void setMaxSpeed(float newMaxSpeed) {
		maxSpeed=newMaxSpeed;
	}
	public void setFuelConsumption(float newFuelConsumption){
		fuelConsumption=newFuelConsumption;
	}
	public void setPrice(float newPrice){
		price=newPrice;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格	
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	
	public float currentSpeed() {	
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {	
		Random rand = new Random();
		if(isTurbo==true)
			return maxSpeed;
		else
			return maxSpeed * rand.nextFloat();
	}


}
