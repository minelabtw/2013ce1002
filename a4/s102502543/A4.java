package ce1002.a4.s102502543;
import java.util.Scanner;


public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n;
		do {
			System.out.println("Please input the number of cars: "); //輸入車的數量
			n = input.nextInt();
			if (n < 1)
				System.out.println("Out of range!");
		} while (n < 1);
		Car[] car = new Car[n];
		for(int a=0;a<n;a++){ //輸入資料
			car[a] = new Car();
			System.out.print("Input car's\nBrand: ");
			car[a].setBrand(input.next());
			System.out.print("Name: ");
			car[a].setName(input.next());
			System.out.print("Max speed: ");
			car[a].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[a].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[a].setPrice(input.nextFloat());
			System.out.print("\n");
		}
		System.out.println("Output car status"); //輸出
		for(int a=0; a<n;a++){
			System.out.println("Car brand is "+car[a].getBrand()+".");
			System.out.println("Car name is "+car[a].getName()+".");
			System.out.println("Car max speed is "+car[a].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[a].getFuelConsumption()+".");
			System.out.println("Car price is "+car[a].getPrice()+".");
			System.out.println("StartTurbo!");
			car[a].startTurbo();
			System.out.println("Turbo status is "+car[a].isTurbo()+".");
			System.out.println("Current speed is "+car[a].currentSpeed(car[a].isTurbo()));
			System.out.print("\n");
		}
		

	}

}
