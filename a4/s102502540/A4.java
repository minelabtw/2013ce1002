package ce1002.a4.s102502540;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int id;
		do {
			System.out.println("Please input the number of cars: ");
			id = input.nextInt();
			if (id < 1)
				System.out.println("Out of range!");
		} while (id < 1);
		Car[] cararr = new Car[id]; // Car class cararr object array
		for (int i = 0; i < id; i++) {
			cararr[i] = new Car(); // construct
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cararr[i].setBrand(input.next());
			System.out.print("Name: ");
			cararr[i].setName(input.next());
			System.out.print("Max speed: ");
			cararr[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			cararr[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			cararr[i].setPrice(input.nextFloat());
			System.out.println();
		}
		System.out.println("Output car status"); // 輸出結果
		for (int i = 0; i < id; i++) {
			System.out.println("Car brand is " + cararr[i].getBrand() + ".");
			System.out.println("Car name is " + cararr[i].getName() + ".");
			System.out.println("Car max speed is " + cararr[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cararr[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cararr[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cararr[i].startTurbo(i);
			System.out.println("Turbo status is " + cararr[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cararr[i].currentSpeed(cararr[i].isTurbo()) + ".");
			System.out.println();
		}
	}
}