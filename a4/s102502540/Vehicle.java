package ce1002.a4.s102502540;

import java.util.Random;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	public Vehicle() {

	}

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String newBrand) {
		brand = newBrand;
	}

	public void setName(String newName) {
		name = newName;
	}

	public void setMaxSpeed(float newMaxSpeed) {
		maxSpeed = newMaxSpeed;
	}

	public void setFuelConsumption(float newFuelConsumption) {
		fuelConsumption = newFuelConsumption;
	}

	public void setPrice(float newPrice) {
		price = newPrice;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}

	public float getPrice() {
		return price;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed(float a) // 給之後有關機車的class使用，A4不會使用到。
	{
		return a;
	}

	public float currentSpeed(boolean isTurbo) { //Turbo成功開啟，則目前速度=最高速度；Turbo開啟失敗，則目前速度=最高速度 * rand值
		Random ran = new Random();
		if (isTurbo == true) {
			return maxSpeed;
		} else {
			return maxSpeed * ran.nextFloat();
		}
	}

}
