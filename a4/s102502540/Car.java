package ce1002.a4.s102502540;

import java.util.Random;

public class Car extends Vehicle {

	private boolean isTurbo; // 渦輪狀態

	public Car() {

	}

	public void startTurbo(int a) { // 測試啟動渦輪，可能會成功或失敗。
		Random ran = new Random();
		isTurbo = ran.nextBoolean();
		if (a == 0) {
			isTurbo = true;
		} else if (a == 1) {
			isTurbo = false;
		}
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}

}
