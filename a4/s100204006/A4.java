package ce1002.a4.s100204006;

import java.util.Scanner;

public class A4 
{
	public static void main(String[] args) 
	{
		
			int N;
			Scanner scanner = new Scanner(System.in) ;      //System.in用來取得使用者的輸入
			System.out.print("Please input the number of cars:");
			N = scanner.nextInt();
			
			while (N<0)
			{
				System.out.println("Out of range!");
				System.out.print("Please input the number of cars:");
				N = scanner.nextInt();
			}
			Car carList[] = new Car[N];		// set parameters 
			for(int i = 0; i < N ; i++)
			{
				Car c = new Car();
				System.out.println("Input car's");
				System.out.println("Brand: ");
				String B;
				B = scanner.next();
				c.setBrand(B);
					
										
				System.out.println("Name: ");
				String Na;
				Na = scanner.next();
				c.setName(Na);
					
					
				System.out.println("Max speed: ");
				float M;
				M = scanner.nextFloat();
				c.setMaxSpeed(M);
									
					
				System.out.println("Fuel consumption: ");
				float F;
				F = scanner.nextFloat();
				c.setfuelConsumption(F);
														
					
				System.out.println("Price: ");
				float P;
				P = scanner.nextFloat();
				c.setprice(P);
					
					
				System.out.println("");
												
				carList[i]=c;
			}	
					
			System.out.println("Output car status.");
			for(int i = 0; i < N ; i++)
			{
				System.out.println("Car brand is "+carList[i].getBrand()+".");
				System.out.println("Car name is "+carList[i].getName()+".");
				System.out.println("Car max speed is "+carList[i].getMaxSpeed()+".");
				System.out.println("Car fuel consumption is "+carList[i].getFuelConsumption()+".");
				System.out.println("Car price is "+carList[i].getPrice()+".");
				System.out.println("StartTurbo!");	
				carList[i].startTurbo();					
				System.out.println("Turbo status is "+carList[i].getIsTurbo()+".");
				System.out.println("Current speed is "+ carList[i].currentSpeed(carList[i].getIsTurbo())+".");	
				System.out.println("");
			}
	}
			
}
	

