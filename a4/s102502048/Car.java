package ce1002.a4.s102502048;

public class Car extends Vehicle 
{
	private boolean isTurbo;// 渦輪有無成功	
	
	public Car()
	{
		
	}	
	public void startTurbo() // 測試啟動渦輪，可能會成功或失敗。
	{	
		float ran=0;	
		do
		{
			ran=(float)(Math.random()*2);//亂數		
			if(ran==1)
				isTurbo=true;
			if(ran<=1)
				isTurbo=false;
		}while(ran>1);		
	}
	public boolean isTurbo() // 取得渦輪目前狀態
	{		
		  return isTurbo;
	}
	
}