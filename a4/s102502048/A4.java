package ce1002.a4.s102502048;
import java.util.Scanner;
import java.io.*;
public class A4 
{
	public static void main(String[] args) throws IOException
	{
		Scanner input = new Scanner(System.in);
		BufferedReader buf;
		buf=new BufferedReader(new InputStreamReader(System.in));		
		int carnum=0;		
		float maxspeed=0,fuel=0,price=0;
		do
		{
			System.out.println("Please input the number of cars: ");//顯示
			carnum = input.nextInt();//輸入		
			if(carnum <=0)
				System.out.println("Out of range!");//顯示
		}while(carnum <=0);
		
		Car[] car = new Car[carnum];//建構陣列		
		for(int i=0;i<carnum;i++)
		{			
			Car _car = new Car();//建構物件
			System.out.println("Input car's");//顯示
			System.out.print("Brand: ");//顯示
				String brand =  buf.readLine();
				_car.setBrand(brand);
				
			System.out.print("Name: ");//顯示
				String name =  buf.readLine(); //輸入
				_car.setName(name);
				
			System.out.print("Max speed: ");//顯示
				maxspeed = input.nextFloat();//輸入
				_car.setMaxSpeed(maxspeed);
				
			System.out.print("Fuel consumption: ");//顯示
				fuel = input.nextFloat();//輸入
				_car.setFuel(fuel);
				
			System.out.print("Price: ");//顯示
				price = input.nextFloat();//輸入
				_car.setPrice(price);				
			car[i]=_car;//丟進陣列			
		}
		System.out.println("Output car status.");//顯示		
		for(int i=0;i<carnum;i++)//顯示數據
		{			
			System.out.println("Car brand is "+car[i].getBrand()+".");
			System.out.println("Car name is "+car[i].getName()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[i].getfuel()+".");
			System.out.println("Car price is "+car[i].getprice()+".");			
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			System.out.println("Current speed is "+car[i].currentSpeed(car[i].isTurbo())+".");			
		}
	}
}
