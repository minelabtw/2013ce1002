package ce1002.a4.s102502011;

import java.util.Random;

public class Car extends Vehicle { //繼承Vehicle
	private boolean isTurbo;
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		Random random = new Random() ;
		int n = 0 ;
		n = random.nextInt(2) ;
		
		if ( n == 1 ) 
			isTurbo = true ;
		else
			isTurbo = false ;
		}
	
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		return isTurbo ;
	    }

}
