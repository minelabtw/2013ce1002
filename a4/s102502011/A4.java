package ce1002.a4.s102502011;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int carid;// 賽車陣列
		
		do {
			System.out.println("Please input the number of cars: ");// 幾台賽車
			carid = input.nextInt();
			if (carid <= 0)
				System.out.println("Out of range!");
		} while (carid <= 0);
 
		Car[] car = new Car[carid] ;
		
		for (int i = 0; i < carid; i++) {
			float maxSpeed = 0 , fuelconsumption = 0 , price = 0 ; //輸入資料
			String strBrand , strName ;
			car[i] = new Car() ;

			System.out.println("Input car's") ;
			System.out.print("Brand: ") ;
			strBrand = input.next() ;
			car[i].setBrand(strBrand) ;
			
			System.out.print("Name: ") ;
		    strName = input.next() ;
		    car[i].setName(strName) ;
			
			System.out.print("Max speed: ") ;
			maxSpeed = input.nextFloat();
			car[i].setMaxSpeed(maxSpeed) ;
			
			System.out.print("Fuel consumption: ") ;
			fuelconsumption = input.nextFloat();
			car[i].setFuelConsumption(fuelconsumption) ;
			
			System.out.print("Price: ") ;
			price = input.nextFloat();
			car[i].setPrice(price) ;
			System.out.println() ;
			
		}
		input.close();// 用不到了，要關掉
 
		System.out.println("Output car status.");// 賽車狀態
 
		for (int i = 0; i < carid;i++) {
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");  
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + "." );
			System.out.println("StartTurbo!") ;
			System.out.println("Turbo status is " + car[i].isTurbo() + "." ) ;
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo() ) + "." ) ;
			System.out.println();
			}

	}

}
