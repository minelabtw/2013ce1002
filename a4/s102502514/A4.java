package ce1002.a4.s102502514;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String brand,name;
		float maxspeed,fuelconsumption,price;
		
		System.out.println("Please input the number of cars: ");
		int number = input.nextInt();
		while (number<=0)  //number>0,否則請求重新輸入
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}
		Car[] cars = new Car[number];  //宣告一物件陣列變數cars[]
		for (int i=0; i<number; i++)
		{
			cars[i] = new Car();  //對物件陣列變數cars[i]初始化
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxspeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelconsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			System.out.println("");
			cars[i].setBrand(brand);  //呼叫父類別的設定品牌
			cars[i].setName(name);  //呼叫父類別的設定款式名稱
			cars[i].setMaxSpeed(maxspeed);  //呼叫父類別的設定最高速度
			cars[i].setFuelConsumption(fuelconsumption);  //呼叫父類別的設定油耗
			cars[i].setprice(price);  //呼叫父類別的設定價格
		}
		System.out.println("Output car status");
		for(int j=0; j<number; j++)
		{
			System.out.println("Car brand is " + cars[j].getBrand() + ".");  //呼叫父類別的取得品牌
			System.out.println("Car name is " + cars[j].getName() + ".");  //呼叫父類別的取得款式名稱
			System.out.println("Car max speed is " + cars[j].getMaxSpeed() + ".");  //呼叫父類別的取得最高速度
			System.out.println("Car fuel consumption is " + cars[j].getFuelConsumption() + ".");  //呼叫父類別的取得油耗
			System.out.println("Car price is " + cars[j].getprice() + ".");  //呼叫父類別的取得價格
			System.out.println("StartTurbo!");
			cars[j].startTurbo();  //呼叫子類別的測試啟動渦輪
			System.out.println("Turbo status is " + cars[j].isTurbo() + ".");  //呼叫子類別的取得渦輪狀態
			System.out.println("Current speed is " + cars[j].currentSpeed(cars[j].isTurbo()) + ".");  //呼叫父類別的目前速度
			System.out.println("");
		}
		input.close();
	}
}
