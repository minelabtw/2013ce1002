package ce1002.a4.s102502545;

import java.util.Random;

public class Car extends Vehicle {//繼承Vehicle

	private boolean isTurbo; // 宣告一布林變數

	public Car() {
                                //建構子
	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		Random random = new Random();     
		isTurbo = random.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
             return isTurbo;
	}
	
	 
}
