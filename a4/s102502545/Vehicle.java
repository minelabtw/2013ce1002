package ce1002.a4.s102502545;

import java.util.Random;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	public Vehicle() {

	}

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {//設定
		this.brand = brand;
	}

	public void setName(String name) {//設定
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {//設定
		this.maxSpeed = maxSpeed;
	}

	public void setfuelConsumption(float fuelConsumption) {//設定
		this.fuelConsumption = fuelConsumption;
	}

	public void setprice(float price) {//設定
		this.price = price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)

	public float currentSpeed(boolean isTurbo) {//判斷在有無渦輪情況下 使用何種回傳直
		if (isTurbo == true)
			return maxSpeed;
		else {
			Random ran = new Random();
			return maxSpeed * ran.nextFloat();
		}
	}

}
