package ce1002.a4.s102502545;

import java.util.Scanner;
import java.util.Random;

public class A4 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int number = 0;
		String brand; // 品牌
		String name; // 款式名稱
		float maxSpeed; // 最高速度
		float fuelConsumption; // 油耗
		float price; // 價格
		boolean isTurbo;

		do {/* 限定變數的範圍 */
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number < 1)
				System.out.println("Out of range!");
		} while (number < 1);

		Car C[] = new Car[number];// 建立一個陣列存放資料

		for (int i = 0; i < number; i++) {// 利用迴圈使用car class 寫入資料
			C[i] = new Car();// 利用迴圈,使每次都能建立新的物件 EX: Car(1) Car(2)

			System.out.println("Input car's");//分別將我們要的數值輸入進去
			System.out.print("Brand: ");
			brand = input.next();
			C[i].setBrand(brand);

			System.out.print("Name:");
			name = input.next();
			C[i].setName(name);

			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			C[i].setMaxSpeed(maxSpeed);

			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			C[i].setfuelConsumption(fuelConsumption);

			System.out.print("Price: ");
			price = input.nextFloat();
			C[i].setprice(price);
			System.out.println("");

			isTurbo = C[i].isTurbo();

		}

		System.out.println("Output car status");
		for (int i = 0; i < number; i++) {//輸出資料庫的物件
			System.out.println("Car brand is " + C[i].getBrand());
			System.out.println("Car name is " + C[i].getName());
			System.out.println("Car max speed is " + C[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "
					+ C[i].getfuelConsumption());
			System.out.println("Price: " + C[i].getprice());
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + C[i].isTurbo());
			System.out.println("Current speed is "
					+ C[i].currentSpeed(C[i].isTurbo()));
		}

	}
}
