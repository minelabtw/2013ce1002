package ce1002.a4.s101201519;
import java.lang.Math;
public class Car extends Vehicle {
	
	private boolean isTurbo;
	
	public void startTurbo() // 測試啟動渦輪，可能會成功或失敗。
	{	
		isTurbo = (Math.random() <= 0.5);//random出來的數字介於0~0.9之間 所以用0.5做分界 機率各占一半
	}
	public boolean isTurbo() // 取得渦輪目前狀態	
	{	
		  return isTurbo;
	}
}
