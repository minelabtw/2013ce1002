package ce1002.a4.s101201519;
import java.util.Scanner;
public class A4 {
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int sum=0;
		Car[] car;
		while(sum<=0)//先設定車子數量
		{
			System.out.println("Please input the number of cars: ");
			sum = input.nextInt();
			if(sum<=0)
			{
				System.out.println("out of range!");
			}
		}
		car = new Car[sum];
		
		for(int i=0; i<sum ;i++)//開始輸入資訊
		{
			car[i] = new Car();
			
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(input.next());
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(input.nextFloat());
			System.out.println();
		}
		
		System.out.println("Output car status");//output cars' status
		
		for (int i=0 ;i<sum ;i++) //輸出資訊
		{
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".");
		}
		
		input.close();
	}

}
