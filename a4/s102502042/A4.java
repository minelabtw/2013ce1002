package ce1002.a4.s102502042;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);	//Scanner~
		System.out.println("Please input the number of cars: ");
		int num = input.nextInt();
		while(num <= 0)	
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car[] car = new Car[num];
		//��J~~~
		for(int i = 0; i<num; ++i)
		{
			Car tmp = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand = input.next();
			System.out.print("Name: ");
			String name = input.next();
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			float fuelconsumption = input.nextFloat();
			System.out.print("Price: ");
			float price = input.nextFloat();
			System.out.println();
			tmp.setBrand(brand);
			tmp.setName(name);
			tmp.setMaxSpeed(maxSpeed);
			tmp.setFuelConsumption(fuelconsumption);
			tmp.setPrice(price);
			car[i] = tmp;	//��^�}�C
		}
		//��X~~~
		for(int i = 0; i<num; ++i)
		{
			System.out.println("Output car status");
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();	//�}�Ҵ���!!!
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".");
			if(i<num-1)
				System.out.println();
		}
		input.close();	//close Scanner
	}

}
