package ce1002.a4.s102502544;

public class Car extends Vehicle{
	
	private boolean isTurbo; // 渦輪狀態
	
	public void startTurbo(){	// 測試啟動渦輪，可能會成功或失敗。
		isTurbo=rand.nextBoolean();
	}
	public boolean isTurbo(){	// 取得渦輪目前狀態	
			return isTurbo;
	}

}
