package ce1002.a4.s102502544;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		
		System.out.println("Please input the number of cars: ");//輸出
		int num=input.nextInt();
		while(num<=0){//設定範圍
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num=input.nextInt();
		}
		Car arr[];//陣列
		arr=new Car[num];//陣列大小
		for(int i=0 ; i<num ; i++){ //記錄問題的輸入
		System.out.println("Input car's");
		arr[i]= new Car();
		System.out.print("Brand: ");
		arr[i].setBrand(input.next());
		
		System.out.print("Name: ");
		arr[i].setName(input.next());
		
		System.out.print("Max speed: ");
		arr[i].setMaxSpeed(input.nextFloat());
		
		System.out.print("Fuel consumption: ");
		arr[i].setFuelconsumption(input.nextFloat());
		
		System.out.print("Price: ");
		arr[i].setPrice(input.nextFloat());
		System.out.println();
		}
		
		System.out.println("Output car status");
		for(int j=0 ; j<num ; j++){ //輸出問題的記錄
		System.out.println("Car brand is "+arr[j].getBrand()+".");
		System.out.println("Car name is "+arr[j].getName()+".");
		System.out.println("Car max speed is "+arr[j].getMaxSpeed()+".");
		System.out.println("Car fuel consumption is "+arr[j].getFuelconsumption()+".");
		System.out.println("Car price is "+arr[j].getPrice()+".");
		System.out.println("StartTurbo!");
		System.out.println("Turbo status is "+arr[j].isTurbo()+".");
		System.out.println("Current speed is "+arr[j].currentSpeed(arr[j].isTurbo())+".");
		System.out.println();
		}
	}

}
