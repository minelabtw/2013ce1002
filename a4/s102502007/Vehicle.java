package ce1002.a4.s102502007;

public class Vehicle {
	public Vehicle()
	{
	}//constructor
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) 
	{
		this.brand=brand;
	}//set up a method to store the brand
	public void setName(String name) 
	{
		this.name = name;
	}//set up a method to store the name
	public void setMaxSpeed(float maxSpeed) 
	{
		this.maxSpeed=maxSpeed;
	}//set up a method to store the maxspeed 
	public void setFuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}//set up a method to store the fuelconsumption
	public void setPrice(float price)
	{
		this.price = price;
	}//set up a method to store the price
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() 
	{
		return brand;
	}//to get the car's brand
	public String getName() 
	{
		return name;
	}//to get the car's name
	public float getMaxSpeed() 
	{
		return maxSpeed;
	}//to get the car's maxspeed
	public float getFuelConsumption()
	{
		return fuelConsumption;
	}//to get the car's fuelconsumption
	public float getPrice()
	{
		return price;
	}//to get the car's price
		
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() 
	{	//給之後有關機車的class使用，A4不會使用到。
		return 0;
	}
	public float currentSpeed(boolean isTurbo) 
	{	
		if(isTurbo==true)
		return this.maxSpeed;
		else
			return this.maxSpeed*(float)(Math.random());
	}//return the current speed

}
