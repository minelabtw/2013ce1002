package ce1002.a4.s102502007;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int amountOfCars;//amountOfCars
		do
		{
		System.out.println("Please input the number of cars: ");
		amountOfCars = input.nextInt();
		if(amountOfCars<=0)
			System.out.println("Out of range!");
		}while(amountOfCars<=0);//loop till the correct number is input
		Car[] cars = new Car[amountOfCars];//creat an object array
		
		for(int i=0;i<amountOfCars;++i)
		{
			cars[i] = new Car();//creat object 
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cars[i].setBrand(input.next());
			System.out.print("Name: ");
			cars[i].setName(input.next());
			System.out.print("Max speed: ");
			cars[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			cars[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			cars[i].setPrice(input.nextFloat());
			System.out.println();//input the attributes term by term
		}
		System.out.println("Output car status");
		for(int i=0;i<amountOfCars;++i)
		{
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption()  + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println("");//display the attributes
		}
		input.close();
		
	}

}
