package ce1002.a4.s101201506;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;

	public void startTurbo() {
		Random ran = new Random(); // 隨機的數字
		isTurbo = ran.nextBoolean();
	}

	public boolean isTurbo() { //建構式
		return isTurbo;
	}
}
