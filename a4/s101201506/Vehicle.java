package ce1002.a4.s101201506;

public class Vehicle {

	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String xbrand) {
		brand = xbrand;

	}

	public void setName(String xname) {
		name = xname;
	}

	public void setMaxSpeed(float xmaxSpeed) {
		maxSpeed = xmaxSpeed;
	}

	public void setFuelConsumption(float xfuelConsumption) {
		fuelConsumption = xfuelConsumption;
	}

	public void setPrice(float xprice) {
		price = xprice;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public float getprice(){
		return price ;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() { // 給之後有關機車的class使用，A4不會使用到。
		return maxSpeed;
	}

	public float currentSpeed(boolean isTurbo) {
		if (isTurbo == true) {
			return maxSpeed;
		} else {
			return maxSpeed * (float) Math.random();
		}
	}
}
