package ce1002.a4.s102502524;

public class Vehicle {

	Vehicle(){}
	
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public void setBrand(String brand) {
		this.brand = brand;		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public void setfuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}
	
	public void setPrice(float price)
	{
		this.price = price;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		
		return this.brand;
	}
	
	public String getName() {
		
		return this.name;
	}
	
	public float getMaxSpeed() {
	
		return this.maxSpeed;
	}
	
	public float setfuelConsumption()
	{
		return this.fuelConsumption;
	}
	
	public float setPrice()
	{
		return this.price;
	}
		
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		
		return this.maxSpeed*(float)(Math.random());
	}
	
	public float currentSpeed(boolean isTurbo)
	{	
		float bonus = 0;
		if (isTurbo == true)
			bonus = 1;
		if(isTurbo == false)
			bonus = (float)(Math.random());
		return this.maxSpeed*bonus;
	}

}
