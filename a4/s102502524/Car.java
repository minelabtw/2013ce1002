package ce1002.a4.s102502524;

public class Car extends Vehicle {
	
	private boolean isTurbo;
	
	public void startTurbo() 
	{	// 測試啟動渦輪，可能會成功或失敗。
		 int ran = (int)(Math.random()*2);
		 
		 if(ran == 0)
			 this.isTurbo = true;
		 if(ran == 1)
			 this.isTurbo =  false;
	}
	
	
	public boolean isTurbo() 
	{	// 取得渦輪目前狀態	
		  
		return this.isTurbo;
	}

}
