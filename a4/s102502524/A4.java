package ce1002.a4.s102502524;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		int num = 0;
		String brand;
		String name;
		float speed = 0;
		float fuel = 0;
		float price = 0;
		
			
		System.out.println("Please input the number of cars: ");		//輸入賽車總數
		num = input.nextInt();
		while(num<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		
		Car c[]= new Car[num];											//建立Car陣列物件
		for (int i=0;i<num;i++)											//輸入各項參數
		{
			c[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand = input.next();
			c[i].setBrand(brand);
			System.out.print("Name: ");
			name = input.next();
			c[i].setName(name);
			System.out.print("Max speed: ");
			speed = input.nextFloat();
			c[i].setMaxSpeed(speed);
			System.out.print("Fuel consumption: ");
			fuel = input.nextFloat();
			c[i].setfuelConsumption(fuel); 
			System.out.print("Price: ");
			price = input.nextFloat();
			c[i].setPrice(price);
			System.out.println(" ");
		}
		
		input.close();
		
		System.out.println("Output car status");						//輸出輸入之內容
		for(int i=0;i<num;i++)
		{
			System.out.println("Car brand is " + c[i].getBrand() + ".");
			System.out.println("Car name is " + c[i].getName() + ".");
			System.out.println("Car max speed " + c[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption " + c[i].setfuelConsumption() + ".");
			System.out.println("Car price is " + c[i].setPrice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + c[i].isTurbo() + ".");
			System.out.println("Current speed is " + c[i].currentSpeed(c[i].isTurbo()) + ".");
			System.out.println(" ");
		}
	}

}
