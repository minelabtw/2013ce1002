package ce1002.a4.s101201504;

public class Vehicle {

	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;
	public void setBrand(String intbrand) {           //save brand
		brand=intbrand ;
	}
	public void setName(String intname) {       //save name
		name=intname;
	}
	public void setMaxSpeed(float intmaxSpeed) {    //xave maxspeed 
		maxSpeed=intmaxSpeed;
	}
	public void setfuel(float intfuel){       //save  fuel consumption
	   fuelConsumption=intfuel ;
	}
	public void setprice(float intprice){       //save price
		price=intprice;
	}
		
	public String getBrand() {
		return brand ;
	}
	public String getName() {
		return name ; 
	}
	public float getMaxSpeed() {
		return maxSpeed  ;
	}
	public float getfuel(){
		return fuelConsumption;
	}
	public float getprice(){
		return price ;
	}
	public float currentSpeed() {	
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {	
		
			if (isTurbo==true)
			{
				return maxSpeed;     //if true equal maxspeed
			}
			else
			{
				
				return maxSpeed*(float)Math.random();
			}
		
}
	}
