package ce1002.a4.s102502019;
public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	public void setbrand(String a)
	{
		brand = a;
	}
	public void setname(String b)
	{
		name = b;
	}
	public void setfuelconsumption(float c)
	{
		fuelConsumption = c;
	}
	public void setmaxspeed(float d)
	{
		maxSpeed = d;
	}
	public void setprice(float e)
	{
		price = e;
	}
	public String getbrand()
	{
		return brand;
	}
	public String getname()
	{
		return name;
	}
	public float getmaxspeed()
	{
		return maxSpeed;
	}
	public float getfuelconsumption()
	{
		return fuelConsumption;
	}
	public float getprice()
	{
		return price;
	}
	public float currentSpeed()
	{
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo)
	{
		float p = (float)Math.random();
		if(isTurbo == true)
		{
			return maxSpeed;
		}else
		{
			return maxSpeed*p;
		}
	}
}
