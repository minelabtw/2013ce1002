package ce1002.a4.s102502019;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.out.print("Please input the number of cars: \n");
Scanner input = new Scanner(System.in);
int number = input.nextInt();
while(number<1)                                                  //輸入條件
{
	System.out.print("Out of range!\n"+"Please input the number of cars: ");
	number = input.nextInt();
}
Car []car = new Car[number];                                    //宣告陣列
for(int i=0; i<number; i++)                                     //用for迴圈輸入完全部車的屬性	
{
	car[i] = new Car();                                         //宣告新物件
	System.out.print("Input car's\n"+"Brand: ");
    String b = input.next();
    car[i].setbrand(b);
    System.out.print("Name: ");
    String name = input.next();
    car[i].setname(name);
    System.out.print("max speed: ");
    float m = input.nextFloat();
    car[i].setmaxspeed(m);
    System.out.print("Fuel consumption: ");
    float f = input.nextFloat();
    car[i].setprice(f);
    System.out.print("Price: ");
    float p = input.nextFloat();
    car[i].setprice(p);
    System.out.print("\n");
}
System.out.print("Output car status\n");
for(int p=0; p<number; p++)                                            //輸出車的屬性
{
	System.out.print("Car brand is "+car[p].getbrand()+".\n");
	System.out.print("Car name is "+car[p].getname()+".\n");
	System.out.print("Car max speed is "+car[p].getmaxspeed()+".\n");
	System.out.print("Car fuel consumption is "+car[p].getfuelconsumption()+".\n");
	System.out.print("Car price is "+car[p].getprice()+".\n");
	System.out.print("StartTurbo!\n");
	car[p].startTurbo();
	System.out.print("Turbo status is "+car[p].isTurbo()+".\n");
	System.out.print("Current speed is "+car[p].currentSpeed(car[p].isTurbo())+".\n\n");
}
input.close();
	}

}
