package ce1002.a4.s102502033;

import java.util.Random;

public class Vehicle
{
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	Vehicle()
	{

	}

	public void setBrand(String b)
	{
		brand = b;
	}

	public void setName(String n)
	{
		name = n;
	}

	public void setMaxSpeed(float m)
	{
		maxSpeed = m;
	}

	public void setfuel(float f)
	{
		fuelConsumption = f;
	}

	public void setprice(float p)
	{
		price = p;
	}

	public String getBrand()
	{
		return brand;
	}

	public String getName()
	{
		return name;
	}

	public float getMaxSpeed()
	{
		return maxSpeed;
	}

	public float getfuel()
	{
		return fuelConsumption;
	}

	public float getprice()
	{
		return price;
	}
	public float currentSpeed()
	{	
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) 
	{	
		Random ran= new Random();
		if(isTurbo==true)
		{
			return maxSpeed;
		}
		else
			return maxSpeed*ran.nextFloat();//0-1
	}


}
