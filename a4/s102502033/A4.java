package ce1002.a4.s102502033;
import java.util.Scanner;

public class A4
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		int number;
		String brand; // 品牌
		String name; // 款式名稱
		float max; // 最高速度
		float fuel; // 油耗
		float price; // 價格
		Scanner input= new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		number= input.nextInt();
		while(number<=0)
		{
			System.out.println("Out of Range!");
			System.out.println("Please input the number of cars: ");
			number= input.nextInt();
		}
		Car[] car = new Car[number];
		for(int i=0;i<number;i++)
		{
			car[i]= new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand= input.next();
			System.out.print("Name: ");
			name= input.next();
			System.out.print("Max speed: ");
			max= input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuel= input.nextFloat();
			System.out.print("Price: ");
			price= input.nextFloat();
			System.out.println();
			car[i].setBrand(brand);//set
			car[i].setName(name);
			car[i].setMaxSpeed(max);
			car[i].setfuel(fuel);
			car[i].setprice(price);			
		}
		System.out.println("Output car status");
		for(int i=0;i<number;i++)
		{
			System.out.println("Car brand is "+car[i].getBrand()+".");//get
			System.out.println("Car name is "+car[i].getName()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[i].getfuel()+".");
			System.out.println("Car price is "+car[i].getprice()+".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			System.out.println("Current speed is "+car[i].currentSpeed(car[i].isTurbo())+".");
			System.out.println();
		}
		

	}

}
