package ce1002.a4.s102502033;

import java.util.Random;

public class Car extends Vehicle
{
	private boolean isTurbo;

	Car()
	{
		
	}
	
	public void startTurbo()
	{
		Random ran= new Random();//ran object
		isTurbo= ran.nextBoolean();//true or false
	}

	public boolean isTurbo()
	{
		return isTurbo;
	}

}
