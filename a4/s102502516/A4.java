package ce1002.a4.s102502516;

import java.util.Scanner;

import ce1002.a4.s102502516.Car;

public class A4 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in); 
		int carsNumber;
		do {
			System.out.println("Please input the number of cars: ");
			carsNumber = scanner.nextInt();
			if( carsNumber <= 0 )
				System.out.println("Out of range!");
		} while ( carsNumber <= 0 );    //決定幾台車
		
		Car [] cars = new Car[carsNumber];//產生車的物件陣列
		for (int i = 0; i < cars.length; i++) {
			cars[i] = new Car();
		}
		for (int i = 0; i < cars.length; i++) {
			System.out.print("Input car's\nBrand: ");
			cars[i].setBrand(scanner.next());
			System.out.print("Name: ");
			cars[i].setName(scanner.next());
			System.out.print("Max speed: ");
			cars[i].setMaxSpeed(scanner.nextFloat());
			System.out.print("Fuel consumption: ");
			cars[i].setFuelConsumption(scanner.nextFloat());
			System.out.print("Price: ");
			cars[i].setPrice(scanner.nextFloat());
			System.out.println();
		}//設定各種參數
		System.out.print("Output car status\n");
		for (int i = 0; i < cars.length; i++) {
			System.out.print("Car brand is " + cars[i].getBrand() + ".\nCar name is  " + cars[i].getName() + ".\nCar max speed is " + cars[i].getMaxSpeed() + ".\nCar fuel consumption is " + cars[i].getFuelConsumption() + ".\nCar price is " + cars[i].getPrice());
			cars[i].startTurbo();//可能成功可能失敗
			System.out.print("\nStartTurbo!\nTurbo status is " + cars[i].isTurbo() + ".\nCurrent speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".\n\n");
		}//印出各種參數
		scanner.close();
		
	}
}
