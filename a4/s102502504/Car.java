package ce1002.a4.s102502504;



class Car extends Vehicle
{
	private boolean isTurbo;
	
	public Car()
	{
		
	}

	public void startTurbo() // 測試啟動渦輪，可能會成功或失敗。
	{
		int turbo = (int)(Math.random()*2); //turbo是亂數
		if(turbo==1) //如果turbo是1 isTurbo就是true
		{
			isTurbo = true;
		}
		else if(turbo==0) //如果turbo是0 isTurbo就是false
		{
			isTurbo = false;
		}
	}
	
	public boolean isTurbo() // 取得渦輪目前狀態	
	{
		return isTurbo;
	}

	
	
	
}

