package ce1002.a4.s102502504;

import java.util.Scanner;

public class A4
{
	
	
	public static void main(String[] args)
	{
		int carnum; //車子有幾台

		Scanner input = new Scanner(System.in);
        do
        {
        	System.out.print("Please input the number of cars: ");
        	carnum = input.nextInt();
        	if(carnum<=0)
        	{
        		System.out.println("Out of range!");
        	}
        }while(carnum<=0);
        
        int [] cars = new int [carnum]; 
        String [] arraybrand = new String [cars.length]; //存brand
        String [] arrayname = new String [cars.length]; //存name
        float [] arraymaxspeed = new float [cars.length];  //存maxspeed
        float [] arrayfuelConsumption = new float [cars.length]; //存油耗
        float [] arrayprice = new float [cars.length]; //存price
        boolean [] arrayTurbo = new boolean [cars.length]; //存亂數出的turbo
        float [] arraycurrentspeed = new float [cars.length]; //存目前速度
        
        Car car = new Car(); //new一個名為car的物件
        
        for(int i = 0; i < cars.length; i++) 
        {
        	System.out.println("Input car's");
        	
        	System.out.print("Brand: ");
            String brand = input.next(); //(為何不是nextLine?) 輸入brand
            car.setBrand(brand); //利用car所繼承的vehicle中的setbrand來設定brand
            arraybrand[i]=car.getBrand(); //利用car所繼承的vehicle中的getbrand來取得brand，並存入陣列
            
            System.out.print("Name: ");
            String name = input.next();
            car.setName(name);
            arrayname[i]=car.getName();
            
            System.out.print("Max speed: ");
            float maxSpeed = input.nextFloat();
            car.setMaxSpeed(maxSpeed);
            arraymaxspeed[i]=car.getMaxSpeed();
            
            System.out.print("Fuel consumption: ");
            float fuelConsumption = input.nextFloat();
            car.setfuelConsumption(fuelConsumption);
            arrayfuelConsumption[i]=car.getfuelConsumption();
            
            System.out.print("Price: ");
            float price = input.nextFloat();
            car.setprice(price);
            arrayprice[i]=car.getprice();
        	
            car.startTurbo(); //利用car本身具有的startTurbo來測試啟動渦輪
            car.isTurbo(); //利用car本身具有的isTurbo來得到isTurbo是true還false
            arrayTurbo[i]=car.isTurbo(); //把isTurbo存入陣列
            car.currentSpeed(car.isTurbo()); //利用car所繼承的vehicle中的currentSpeed取得目前速度
            arraycurrentspeed[i]=car.currentSpeed(car.isTurbo());  //把目前的速度存入陣列
            
            System.out.println(); //空一行
        }
        
        System.out.println("Output car status");
        
        for(int i = 0; i < cars.length; i++) //照剛剛存入陣列的順序依序輸出下列內容
        {
        	System.out.println("Car brand is " + arraybrand[i] + ".");
        	System.out.println("Car name is " + arrayname[i] + ".");
        	System.out.println("Car max speed is " + arraymaxspeed[i] + ".");
        	System.out.println("Car fuel consumption is " + arrayfuelConsumption[i] + ".");
        	System.out.println("Car price is " + arrayprice[i] + ".");
        	System.out.println("StartTurbo!");
        	System.out.println("Turbo status is " + arrayTurbo[i] + ".");
        	System.out.println("Current speed is " + arraycurrentspeed[i] + ".");
        	System.out.println(); //空一行
        }
        
        input.close(); //沒用到要關掉
	}
	
}
