package ce1002.a4.s102502009;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;
	Random ran = new Random();

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		this.isTurbo = ran.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
