package ce1002.a4.s102502009;

import java.util.Scanner;

import ce1002.a4.s102502009.Car;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int total = 0;
		do // input total
		{
			System.out.println("Please input the number of cars: ");
			total = input.nextInt();
			if (total <= 0)
				System.out.println("Out of range!");
		} while (total <= 0);

		Car[] car = new Car[total];// set car

		float maxSpeed = 0;
		float fuelConsumption = 0;
		float price = 0;

		for (int i = 0; i < total; i++) { // input car data
			car[i] = new Car(); // 非常重要
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(input.next());
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			car[i].setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			car[i].setFuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			price = input.nextFloat();
			car[i].setPrice(price);
			System.out.println("");
		}

		System.out.println("Output car status."); // output car status
		for (int i = 0; i < total; i++) {
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out
					.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "
					+ car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println("");
		}
		input.close();

	}
}
