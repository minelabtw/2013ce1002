package ce1002.a4.s102502043;
import java.util.Scanner;
public class A4 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("Please input the number of cars: ");
		Scanner input = new Scanner(System.in);
		int num = input.nextInt();
		while(num<=0)							//限制數字範圍
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		
		Car car[] = new Car[num];
		String b;
		String n;
		Float ms;
		Float f;
		Float p;
		for(int i=0;i<num;i++)						//用一個暫存的物件將資料存入游物件組成的陣列
		{
			Car temp = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			b = input.next();
			System.out.print("Name: ");
			n = input.next();
			System.out.print("Max speed: ");
			ms = input.nextFloat();
			System.out.print("Fuel consumption: ");
			f = input.nextFloat();
			System.out.print("Price: ");
			p = input.nextFloat();
			temp.setBrand(b);
			temp.setName(n);
			temp.setMaxSpeed(ms);
			temp.setFuelConsumption(f);
			temp.setPrice(p);
			car[i] = temp ;
			System.out.println("");
		}
		for(int i=0;i<num;i++)							//將物件陣列的資料依依表示
		{
			System.out.println("Output car status");
			System.out.println("Car brand is "+car[i].getBrand());
			System.out.println("Car name is "+car[i].getName());
			System.out.println("Car max speed is "+car[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "+car[i].getFuelConsumption());
			System.out.println("Car price is "+car[i].getPrice());
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is "+car[i].isTurbo());
			if(car[i].isTurbo()==false)
			{
				System.out.println("Current speed is "+car[i].currentSpeed(car[i].isTurbo()));
			}
			else
			{
				System.out.println("Current speed is "+car[i].getMaxSpeed());
			}
			if(num+1>=i)
				{
					System.out.println("");
				}
			
		}
		input.close();
	}

}
