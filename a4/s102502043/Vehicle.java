package ce1002.a4.s102502043;
import java.util.Random;
public class Vehicle 
{
	Vehicle()
	{
										//建構子
	}
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	private Random ran = new Random();
	
	public void setBrand(String brand) 		//存入各個變數
	{
		this.brand = brand;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) 
	{
		this.maxSpeed = maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price)
	{
		this.price = price;
	}
	public float getPrice()					//回傳資料
	{
		return price;
	}
	public float getFuelConsumption()
	{
		return fuelConsumption;
	}
	public String getBrand() 
	{
		return brand;
	}
	public String getName()
	{
		return name;
	}
	public float getMaxSpeed() 
	{
		return maxSpeed;
	}
	
	//public float currentSpeed() { }
	public float currentSpeed(boolean isTurbo) 
	{	
		return maxSpeed*ran.nextFloat();
	}
}
