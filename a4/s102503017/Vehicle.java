package ce1002.a4.s102503017;

import java.util.Random;

public class Vehicle
{
	Random random = new Random();
	private String brand, name;
	private float maxSpeed, fuel, price;
	private boolean isTurbo;
	
	Vehicle()
	{
		
	}
	//default constructor.
	Vehicle(String inputBrand, String inputName, float inputMaxSpeed, float inputFuel, float inputPrice, boolean inputTurbo)
	{
		brand = inputBrand;
		name = inputName;
		maxSpeed = inputMaxSpeed;
		fuel = inputFuel;
		price = inputPrice;
		isTurbo = inputTurbo;
	}
	//parameter constructor.
	
	void setBrand (String inputBrand)
	{
		brand = inputBrand;
	}
	
	
	void setName (String inputName)
	{
		name = inputName;
	}
	
	void setMaxSpeed (float inputMaxSpeed)
	{
		maxSpeed = inputMaxSpeed;
	}

	void setFuel (float inputFuel)
	{
		fuel = inputFuel;
	}
	
	void setPrice (float inputPrice)
	{
		price = inputPrice;
	}
	
	void setTurbo(boolean inputTurbo)
	{
		isTurbo = inputTurbo;
	}
	
	String getBrand()
	{
		return brand;
	}
	
	String getName()
	{
		return name;
	}
	
	float getMaxSpeed()
	{
		return maxSpeed;
	}
	
	float getFuel()
	{
		return fuel;
	}
	
	float getPrice()
	{
		return price;
	}
	
	boolean getTurbo()
	{
		return isTurbo;
	}
	
	//set and get method for each elements.
	
	float currentSpeed()
	{
		float expectedSpeed = maxSpeed * random.nextFloat();
		
		return expectedSpeed;
	}
	//currentSpeed method overloading.
	float currentSpeed(boolean isTurbo)
	{
		if(isTurbo == true)
		{
			return maxSpeed;
		}
		else
		{
			float expectedSpeed = maxSpeed * random.nextFloat();
			
			return expectedSpeed;
		}
	}
	//If the object is on turbo, then output maxSpeed.Otherwise output a speed which is maxSpeed times a number of 0 < num < 1.
}//end class.
