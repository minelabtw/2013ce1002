package ce1002.a4.s102503017;

public class Car extends Vehicle 
{
	//each elements are inherited from Class Vehicle.
	Car()
	{
		super();
	}
	
	Car(String brand, String name, float maxSpeed, float fuel, float price, boolean isTurbo)
	{
		super(brand, name, maxSpeed, fuel, price, isTurbo);
	}
	
	void setBrand(String brand)
	{
		super.setBrand(brand);
	}
	
	void setName(String name)
	{
		super.setName(name);
	}
	
	void setMaxSpeed(float maxSpeed)
	{
		super.setMaxSpeed(maxSpeed);
	}
	
	void setFuel(float fuel)
	{
		super.setFuel(fuel);
	}
	
	void setPrice(float price)
	{
		super.setPrice(price);
	}
	
	void setTurbo(boolean inputTurbo)
	{
		super.setTurbo(inputTurbo);	
		}
	
	String getBrand()
	{
		return super.getBrand();
	}
	
	String getName()
	{
		return super.getName();
	}
	
	float getMaxSpeed()
	{
		return super.getMaxSpeed(); 
	}
	
	float getFuel()
	{
		return super.getFuel();
	}
	
	float getPrice()
	{
		return super.getPrice();
	}
	
	boolean getTurbo()
	{
		return super.getTurbo();
	}
	
	float currentSpeed(boolean isTurbo)
	{
		return super.currentSpeed(isTurbo);
	}
	
}

	