package ce1002.a4.s102503017;

import java.util.Scanner;

import java.util.Random;

public class A4 
{
	private static Scanner scanner;

	public static void main(String[] args)
	{
		int num;
		String brand, name;
		float maxSpeed, fuel, price;
		boolean isTurbo;
		
		System.out.println("Please input the number of cars:");
		
		do
		{
			scanner = new Scanner(System.in);
			num = scanner.nextInt();

			if(num <= 0)
			{
				System.out.println("Out of range!\nPlease input the number of cars:");
			}
		}while(num <= 0);
		//input checking
		
		Car[] garage = new Car[num];
		//object array for Car.
		for(int i = 0 ; i < num ; i++)
		{
			
			System.out.print("Input car's\nBrand: ");
			scanner.nextLine();
			brand = scanner.nextLine();
			System.out.print("Name:");
			name = scanner.nextLine();
			System.out.print("Max Speed: ");
			maxSpeed = scanner.nextFloat();
			System.out.print("Fuel consumption: ");
			fuel = scanner.nextFloat();
			System.out.print("Price: ");
			price = scanner.nextFloat();
			//scanning the parameter.
			Random random = new Random();
			isTurbo = random.nextBoolean();
			//determine the turbo is on or not.
			Car car = new Car(brand, name, maxSpeed, fuel, price, isTurbo);
			garage[i] = car;
			//create a car object and put it into the object array.
		}
		

		System.out.println("Output cars status:");
			for(int i = 0 ; i < num ; i++)
			{
				System.out.println("Car brand is " + garage[i].getBrand() + ".\nCar name is " + garage[i].getName() + ".\nCar max speed is "
						           + garage[i].getMaxSpeed() + ".\nCar fuel consumtion is " + garage[i].getFuel() + ".\nCar price is " +
						             garage[i].getPrice() + ".\nStart turbo!");
				
				if(garage[i].getTurbo() == true)
				{
					System.out.println("Turbo status is " + garage[i].getTurbo() + ".\nCurrent speed is " + garage[i].getMaxSpeed() + ".");
				}
				else
				{
					System.out.println("Turbo status is " + garage[i].getTurbo() + ".\nCurrent speed is " + garage[i].currentSpeed(garage[i].getTurbo()) + ".");
				}
			}//end for.
			//output the status.
	}//end main.
}//end class.
