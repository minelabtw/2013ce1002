package ce1002.a4.s102502013;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int carnumber = 0;//declare a variable for car's number
		carnumber = input.nextInt();
		while(carnumber<=0){//while car's number does not in the range, output "Out of range!" and repeat input
			System.out.println("Out of range!");
			carnumber = input.nextInt();
		}
		Car [] cars = new Car [carnumber];//declare an array for car
		for(int i=0;i<carnumber;i++){
			cars[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand;
			brand = input.next();
			cars[i].setBrand(brand);//set brand
			System.out.print("Name: ");
			String name;
			name = input.next();
			cars[i].setName(name);//set name
			System.out.print("Max speed: ");
			float ms = 0;
			ms = input.nextFloat();
			cars[i].setMaxSpeed(ms);//set maxSpeed
			System.out.print("Fuel consumption: ");
			float Fc = 0;
			Fc = input.nextFloat();
			cars[i].setFuelConsumption(Fc);//set fuelConsumption
			System.out.print("Price: ");
			float p = 0;
			p = input.nextFloat();
			cars[i].setPrice(p);//set price
			System.out.println("");
		}
		System.out.println("Output car status");
		for(int i=0;i<carnumber;i++){//output car status
			System.out.println("Car brand is " + cars[i].getBrand());
			System.out.println("Car name is " + cars[i].getName());
			System.out.println("Car max speed is " + cars[i].getMaxSpeed());
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption());
			System.out.println("Car price is " + cars[i].getPrice());
			System.out.println("StartTurbo!");
			cars[i].startTurbo();//judge the turbo work or doesn't work
			boolean Tb;
			Tb = cars[i].getTurbo();
			System.out.println("Turbo status is " + cars[i].getTurbo());
			System.out.println("Current speed is " + cars[i].currentSpeed(Tb));
			System.out.println("");
		}
	}
}
