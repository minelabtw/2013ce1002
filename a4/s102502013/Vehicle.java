package ce1002.a4.s102502013;

public class Vehicle {
	private String brand;//declare a variable for car's brand
	private String name;//declare a variable for car's name
	private float maxSpeed;//declare a variable for car's maxSpeed
	private float fuelConsumption;//declare a variable for car's fuelConsumption
	private float price;//declare a variable for car's price
	public Vehicle(){//consturctoe for Vehicle
		maxSpeed = 0;
		fuelConsumption = 0;
		price = 0;
	}
	public void setBrand(String b) {//setBrand
		brand = b;
	}
	public void setName(String N) {//setName
		name = N;
	}
	public void setMaxSpeed(float ms) {//setMaxSpeed
		maxSpeed = ms;
	}
	public void setFuelConsumption(float Fc){//setFuelConsumption
		fuelConsumption = Fc;
	}
	public void setPrice(float P){//setPrice
		price = P;
	}
	public String getBrand() {//getBrand
		return brand;
	}
	public String getName() {//getName
		return name;
	}
	public float getMaxSpeed() {//getMaxSpeed
		return maxSpeed;
	}
	public float getFuelConsumption(){//getFuelConsumption
		return fuelConsumption;
	}
	public float getPrice(){//getPrice
		return price;
	}
	public float currentSpeed(boolean isTurbo) {//get currentSpeed
		if(isTurbo==true){
			return maxSpeed;
		}
		else{
			return maxSpeed * (float)Math.random();
		}
	}
	public float currentSpeed(){
		return maxSpeed;
	}
}
