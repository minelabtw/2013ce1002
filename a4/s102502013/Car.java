package ce1002.a4.s102502013;

public class Car extends Vehicle{
	private boolean isTurbo;//declare a variable for turbo
	private int tmp;//declare a temporary variable about turbo
	Car(){//constructor for Car
		isTurbo = false;
		tmp = 0;
	}
	public void startTurbo(){
		tmp = (int)Math.random()%2;//tmp's value is 0 or 1
	}
	public boolean getTurbo(){
		if(tmp ==1){
			isTurbo = true;
		}
		return isTurbo;
	}
}
