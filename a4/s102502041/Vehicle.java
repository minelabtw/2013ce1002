package ce1002.a4.s102502041;
import java.util.Random;
public class Vehicle {
	Random rand = new Random();
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	public void setBrand(String s)
	{
		brand = s;
	}
	public void setName(String s)
	{
		name = s;
	}
	public void setMaxSpeed(float s)
	{
		maxSpeed = s;
	}
	public void setFuelConsumption(float s)
	{
		fuelConsumption = s;
	}
	public void setPrice(float s)
	{
		price = s;
	}
	public String getBrand(){return brand;}
	public String getName(){return name;}
	public float getMaxSpeed(){return maxSpeed;}
	public float getFuelConsumption(){return fuelConsumption;}
	public float getPrice(){return price;}
	public float currentSpeed()
	{
		return maxSpeed*rand.nextFloat();
	}
	public float currentSpeed(boolean isTurbo)
	{
		if(isTurbo==true)return maxSpeed;
		else return maxSpeed*rand.nextFloat();
	}
}
