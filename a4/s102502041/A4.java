package ce1002.a4.s102502041;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int number;
		System.out.println("Please input the number of cars: ");
		do{
			number=cin.nextInt();
			if(number<=0)
				System.out.println("Out of range!\nPlease input the number of cars: ");
		}while(number<=0);
		Car[] cars = new Car[number];
		for(int i=0;i<number;++i)
		{
			Car car = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car.setBrand(cin.next());
			System.out.print("Name: ");
			car.setName(cin.next());
			System.out.print("Max speed: ");
			car.setMaxSpeed(cin.nextFloat());
			System.out.print("Fuel consumption: ");
			car.setFuelConsumption(cin.nextFloat());
			System.out.print("Price: ");
			car.setPrice(cin.nextFloat());
			System.out.println();
			cars[i]=car;
		}
		
		System.out.println("Output car status");
		for(int i=0;i<number;++i)
		{
			System.out.println("Car brand is "+cars[i].getBrand()+".");
			System.out.println("Car name is "+cars[i].getName()+".");
			System.out.println("Car max speed is "+cars[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+cars[i].getFuelConsumption()+".");
			System.out.println("Car price is "+cars[i].getPrice()+".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is "+cars[i].isTurbo()+".");
			System.out.println("Current speed is "+cars[i].currentSpeed(cars[i].isTurbo())+".");
			System.out.println();
		}
		cin.close();
	}

}
