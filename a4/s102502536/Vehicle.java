package ce1002.a4.s102502536;

import java.util.Random;

public class Vehicle {
	
	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;	
	
	public Vehicle(){
		
	}
	
	//set brand,name,maxSpeed,fuelConsumption,price
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price){
		this.price = price;
	}
	 
	//get brand,name,maxSpeed,fuelConsumption,price		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
		
	//get current speed
	public float currentSpeed() {	// not used
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {	
		
		Random ran = new Random();
						
		if (isTurbo == false)
		    return maxSpeed * ran.nextFloat();
		else
			return maxSpeed;
	}

}
