package ce1002.a4.s102502536;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// declare variables
		int num;
		String brand;
		String name; 
		float maxSpeed;
		float fuelConsumption;
		float price;
		
		Scanner input = new Scanner(System.in);
		// print out the line
		System.out.println("Please input the number of cars:");
		// set range
		do
		{
			num = input.nextInt();
			
			if (num <= 0)
				System.out.println("Out of range!\nPlease input the number of cars:");
			
		} while (num <= 0);
		// declare object array
		Car car[] = new Car[num];
		// loop, input the status of each car
		for (int i = 0; i < num; i++)
		{
			car[i] = new Car();
			
			System.out.print("Input car's\nBrand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			System.out.println();
			
			car[i].setBrand(brand);
			car[i].setName(name);
			car[i].setMaxSpeed(maxSpeed);
			car[i].setFuelConsumption(fuelConsumption);
			car[i].setPrice(price);
			car[i].startTurbo();
		}
		
		System.out.println("Output car status");
		// loop, output the status of each car
		for (int i = 0; i < num; i++)
		{
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!\nTurbo status is " + car[i].isTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println();
		}

        input.close();
	}

}
