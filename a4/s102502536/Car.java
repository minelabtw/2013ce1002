package ce1002.a4.s102502536;

import java.util.Random;

public class Car extends Vehicle {
	
	private boolean isTurbo;
	
	public Car(){
		
	}
	
	public void startTurbo() {	// isTurbo True or False
		int a;
		
		Random ran = new Random();
		a = ran.nextInt(2);
		
		if (a == 1)
		    isTurbo = true;
		else 
			isTurbo = false;
	}
		
	public boolean isTurbo() {	// get isTurbo 
		
		return isTurbo;
	}

}
