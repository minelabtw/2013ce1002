package ce1002.a4.s101303504;

public class Vehicle {
	
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public Vehicle(){
		
	}
	
	public void setBrand(String brand){
		this.brand = brand;
	}
	
	public String getBrand(){
		return brand;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setmaxSpeed(float maxSpeed){
		this.maxSpeed = maxSpeed;
	}

	public float getmaxSpeed(){
		return maxSpeed;
	}
	
	public void setfuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	
	public float getfuelConsumption(){
		return fuelConsumption;
	}
	
	public void setPrice(float price){
		this.price = price;
	}
	
	public float getPrice(){
		return price;
	}
}
