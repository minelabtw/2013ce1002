package ce1002.a4.s101303504;

public class Car extends Vehicle {

	private boolean isTurbo;
	
	public Car(){
		
	}
	public void setisTurbo(boolean isTurbo){
		this.isTurbo = isTurbo;
	}
	
	public boolean getisTurbo(){
		return isTurbo;
	}
}
