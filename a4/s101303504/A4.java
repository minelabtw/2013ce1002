package ce1002.a4.s101303504;
import java.util.Scanner;
import java.util.Random;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		Random rand = new Random();
		Car []car;// set array
		int numOfCar = 0 ;
		do{
			System.out.print("Input the number of cars:");
			numOfCar = input.nextInt();
			if(numOfCar<=0)
				System.out.println("Out of range!");
		}while(numOfCar<=0);
		
		car = new Car[numOfCar];// let the input number into car array
		for(int i=0;i<numOfCar; i++){
			Car carsId = new Car();// 跑質用
			String brand,name;
			float maxSpeed = 0 ;
			float fuelConsumption = 0 ;
			float price = 0;
			System.out.println("Input car's");
			System.out.print("brand:");
			brand = input.next();//input brand
			carsId.setBrand(brand);// store brand
			System.out.print("Name:");
			name = input.next();//input name
			carsId.setName(name);//store name
			System.out.print("Max speed:");
			maxSpeed = input.nextFloat();// input max speed
			carsId.setmaxSpeed(maxSpeed);// store max speed
			System.out.print("Fuel Consumption:");
			fuelConsumption = input.nextFloat();// input fuel Consumption
			carsId.setfuelConsumption(fuelConsumption);// store
			System.out.print("Price:");
			price = input.nextFloat();//input price
			carsId.setPrice(price);// store price
			System.out.println();
			car[i] = carsId;
		}
		
		for(int i=0 ; i<numOfCar; i++){// start the turbo
			boolean turbo;
			turbo=rand.nextBoolean();//隨機指定true or false
			car[i].setisTurbo(turbo);
		}
		for(int i=0;i<numOfCar;i++){
			float nowSpeed;
			System.out.println("Car brand is " + car[i].getBrand());
			System.out.println("Car name is "+ car[i].getName());
			System.out.println("Car max speed is "
			+ car[i].getmaxSpeed());
			System.out.println("Car fuel consumption is "
					+ car[i].getfuelConsumption());
			System.out.println("Car price is "+ car[i].getPrice());
			System.out.println("Start Turbo!");
			System.out.println("Turbo status is "+ car[i].getisTurbo());
			if(car[i].getisTurbo()==false){//if turbo flase give a random
				nowSpeed = car[i].getmaxSpeed() * rand.nextFloat();
			}
			else{// current speed is max speed
				nowSpeed = car[i].getmaxSpeed();
			}
			System.out.println("Current speed is "
					+ nowSpeed);
			System.out.println();
		}
	}

}
