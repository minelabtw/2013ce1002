package ce1002.a4.s102502507;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int number = 1;
		String brand;
		String name;
		float maxSpeed;
		float fuelConsumption;
		float price;
		boolean a;

		do {
			if (number < 1) {
				System.out.println("Out of range!");
			}
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		} while (number < 1);// 設限制
		Car[] carArray = new Car[number]; // 存入物體

		for (int i = 0; i < number; i++) {
			System.out.println("Input car's");
			carArray[i] = new Car();
			// 存入陣列
			System.out.print("Brand: ");
			brand = input.next();
			carArray[i].setBrand(brand);
			System.out.print("Name: ");
			name = input.next();
			carArray[i].setName(name);
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			while (maxSpeed <= 0) // 設限制
			{
				System.out.println("Out of range !");
				System.out.println("Max speed: ");
				maxSpeed = input.nextFloat();
			}
			carArray[i].setMaxSpeed(maxSpeed);

			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			while (fuelConsumption <= 0) // 設限制
			{
				System.out.println("Out of range !");
				System.out.println("Fuel consumption: ");
				fuelConsumption = input.nextFloat();
			}
			carArray[i].setfuelConsumption(fuelConsumption);

			System.out.print("Price: ");
			price = input.nextFloat();
			while (price <= 0) {
				System.out.println("Out of range !");
				System.out.print("Price: ");
				price = input.nextFloat();
			}
			carArray[i].setprice(price);

			carArray[i].startTurbo();
			System.out.println();

		}
		input.close();
		for (int i = 0; i < number; i++) {
			System.out.println("Output car status");
			System.out.println("Car brand is " + carArray[i].getBrand());
			System.out.println("Car name is " + carArray[i].getName());
			System.out.println("Car max speed is " + carArray[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "
					+ carArray[i].getfuelConsumption());
			System.out.println("Car price is " + carArray[i].getprice());
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + carArray[i].isTurbo());
			a = carArray[i].isTurbo();
			System.out.println("Current speed is "
					+ carArray[i].currentSpeed(a));
			System.out.println();

		}

	}

}
