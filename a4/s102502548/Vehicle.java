package ce1002.a4.s102502548;

import java.util.Random ;

public class Vehicle {
	Vehicle(){
		maxSpeed=0 ;//全部初始化為零
		fuelConsumption=0 ;
		price=0 ;
		brand="" ;
		name="" ;
	}
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public void setBrand(String s){
		brand=s ;
	}
	public void setname(String s){
		name=s ;
	}
	public void setmaxSpeed(float speed){
		maxSpeed=speed ;
	}
	public void setfuelConsumption(float con){
		fuelConsumption=con ;
	}
	public void setprice(float p){
		price=p ;
	}
	
	public String getBrand() {
		return brand ;
	}
	public String getName(){
		return name ;
	}
	public float getmaxSpeed(){
		return maxSpeed ;
	}
	public float getfuelConsumption(){
		return fuelConsumption ;
	}
	
	public float getprice(){
		return price ;
	}
	
	public float currentSpeed(float n) {
		maxSpeed=maxSpeed*n ;
		
		return maxSpeed ;
		
	}
	
	public float currentSpeed() {
			return maxSpeed ;
	}
	

}
