package ce1002.a4.s102502548;

public class Car extends Vehicle {
	Car(int number){
		isTurbo=true ;
	}
	
	private boolean isTurbo;
	
	public void startTurbo(int r) {
		if (r==0){
			isTurbo=true ;
		}
		
		else{
			isTurbo=false ;
		}
	}
	
	public boolean getisTurbo() {
		return isTurbo ;
	}

}
