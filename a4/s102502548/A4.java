package ce1002.a4.s102502548;

import java.util.Random ;

import java.util.Scanner ;

public class A4 {

	public static void main(String[] args) {
		int number=0 ;
		int r=0 ;
		String B="" ;
		String N="" ;
		float M ;
		float F ;
		float P ;
		float n ;
		
		Scanner sc=new Scanner(System.in) ;
		Random ran=new Random(2) ;
		
		while (true){
			System.out.println("Please input the number of cars: ") ;
			
			number=sc.nextInt();
			
			if (number<=0){
				System.out.println("Out of range!") ;
			}
			else break ;
		}
		
		Car[] car=new Car[number] ;
		
		for (int x=0;x<number;x++){
			car[x]=new Car(x) ;
			
			System.out.print("Input car's"+"\n"+"Brand: ") ;
			
			B=sc.next() ;
			
			car[x].setBrand(B) ;
			
			System.out.print("Name: ") ;
			
			N=sc.next() ;
			
			car[x].setname(N) ;
			
			System.out.print("Max speed: ") ;
			
			M=sc.nextFloat();
			
			car[x].setmaxSpeed(M) ;
			
			System.out.print("Fuel consumption: ") ;
			
			F=sc.nextFloat();
			
			car[x].setfuelConsumption(F) ;
			
			System.out.print("Price: ") ;
			
			P=sc.nextFloat();
			
			car[x].setprice(P) ;
			
			r=ran.nextInt(2);
			
			car[x].startTurbo(r) ;
			
			System.out.print("\n") ;
		}
		
		Random ra=new Random(3) ;
		
		n=ra.nextFloat();
		
		System.out.println("Output car status") ;
		
		for (int x=0;x<number;x++){
			System.out.print("Car brand is "+car[x].getBrand()+".\n") ;
			System.out.print("Car name is "+car[x].getName()+".\n") ;
			System.out.print("Car max speed is "+car[x].getmaxSpeed()+".\n") ;
			System.out.print("Car fuel consumption is "+car[x].getfuelConsumption()+".\n") ;
			System.out.print("Car price is "+car[x].getprice()+".\n") ;
			System.out.println("StartTurbo!") ;
			System.out.print("Turbo status is "+car[x].getisTurbo()+".\n") ;
			if (r==0){
				System.out.print("Current speed is "+car[x].currentSpeed()+".\n\n") ;
			}
			else{
				System.out.print("Current speed is "+car[x].currentSpeed(n)+".\n\n") ;
			}
		}

	}

}
