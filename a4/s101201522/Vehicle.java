package ce1002.a4.s101201522;

public class Vehicle {
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;
	
	public void setBrand (String brand) {//set vehicle's brand
		this.brand = brand;
	}
	
	public void setName (String name) {//set vehicle's name
		this.name = name;
	}
	
	public void setMaxSpeed (float maxSpeed) {//set the value of vehicle's maxSpeed
		this.maxSpeed = maxSpeed;
	}
	
	public void setFuelConsumption (float fuelConsumption) {//set the value of vehicle's fuel consumption
		this.fuelConsumption = fuelConsumption; 
	}
	
	public void setPrice (float price) {//set vehicle's price
		this.price = price;
	}
	
	public String getBrand () {//get vehicle's brand
		return brand;
	}
	
	public String getName () {//get vehicle's name
		return name;
	}
	
	public float getMaxSpeed () {//get the value of vehicle's maxSpeed
		return maxSpeed;
	}
	
	public float getFuelConsumption () {//get the value of vehicle's fuel consumption
		return fuelConsumption;
	}
	
	public float getPrice () {//get vehicle's price
		return price;
	}
	
	public float currentSpeed () {//get current speed for motorcycle
		return maxSpeed*(float)Math.random();
	}
	
	public float currentSpeed (boolean isTurbo) {//get current speed for car
		return isTurbo ? maxSpeed : maxSpeed*(float)Math.random();
	}
}
