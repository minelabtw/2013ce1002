package ce1002.a4.s101201522;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i,num;//num is the number of cars
				
		do {
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if (num <= 0)
				System.out.println("Out of range!");
		} while (num <= 0);//input num
		
		Car[] cars = new Car[num];//construct cars
		
		for (i=0;i<num;i++) {//input cars' status
			cars[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cars[i].setBrand(input.next());
			System.out.print("Name: ");
			cars[i].setName(input.next());
			System.out.print("Max speed: ");
			cars[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			cars[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			cars[i].setPrice(input.nextFloat());
			System.out.println();
		}
		
		System.out.println("Output car status");//output cars' status
		for (i=0;i<num;i++) {
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + (i == num-1 ? "." : ".\n"));
		}
		
		input.close();
	}

}
