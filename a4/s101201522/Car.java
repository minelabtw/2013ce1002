package ce1002.a4.s101201522;

import java.lang.Math;

public class Car extends Vehicle{
	private boolean isTurbo;//Turbo status
	
	public void startTurbo () {//test starting Turbo, success or fail
		isTurbo = (Math.random() >= 0.5);
	}
	
	public boolean isTurbo () {//get Turbo status
		return isTurbo;
	}
}
