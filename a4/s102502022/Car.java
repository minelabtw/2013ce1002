package ce1002.a4.s102502022;
import java.util.Random;
class Car extends Vehicle{
    
	private boolean isTurbo;
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		Random rand= new Random();
		isTurbo = rand.nextInt(2)==0;
	}
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		 return isTurbo; 
	}
}
