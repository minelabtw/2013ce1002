package ce1002.a4.s102502022;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        System.out.println("Please input the number of cars:");
        Scanner scn= new Scanner(System.in);
        int n= scn.nextInt();     //宣告變數
        while(n<1){      //迴圈
        	System.out.println("Out of range!");
        	System.out.println("Please input the number of cars:");
        	n= scn.nextInt();
        }
        Car[] car=new Car[n];    //宣告物件陣列
        for(int t=0;t<n;t++)    //迴圈
        {
        	car[t]= new Car();
        	System.out.println("Input car's");
        	System.out.print("Brand: ");
        	car[t].setBrand(scn.next());   //傳入輸入的數
        	System.out.print("Name: ");
        	car[t].setName(scn.next());
        	System.out.print("Max speed: ");
        	car[t].setMaxSpeed(scn.nextFloat());
        	System.out.print("Fuel consumption: ");
        	car[t].setFuelConsumption(scn.nextFloat());
        	System.out.print("Price: ");
        	car[t].setPrice(scn.nextFloat());
        	System.out.println("    ");
        }
        System.out.println("Output car status");
        for(int t=0;t<n;t++)    //迴圈
        {
        	System.out.println("Car brand is "+car[t].getBrand()+".");
        	System.out.println("Car name is "+car[t].getName()+".");
        	System.out.println("Car max speed is "+car[t].getMaxSpeed()+".");
        	System.out.println("Car fuel consumption is "+car[t].getFuelConsumption()+".");
        	System.out.println("Car price is "+car[t].getPrice()+".");
        	System.out.println("StartTurbo!");
        	car[t].startTurbo();    //呼叫函數
        	System.out.println("Turbo status is "+car[t].isTurbo()+".");
        	System.out.println("Current speed is "+car[t].currentSpeed(car[t].isTurbo())+".");
        	System.out.println("   ");
        }
        scn.close();
	}

}
