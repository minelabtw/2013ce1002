package ce1002.a4.s984008030;
import java.util.Random;

public class Car {
	
	private boolean isTurbo;// 渦輪狀態
	
	public void startTurbo() {// 測試啟動渦輪，可能會成功或失敗。
		Random rand = new Random();
		isTurbo = rand.nextBoolean();
	}
	public boolean isTurbo() {// 取得渦輪目前狀態
		return this.isTurbo;
	}
	
}

