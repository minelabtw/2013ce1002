package ce1002.a4.s984008030;
import ce1002.a4.s984008030.*;
import java.util.Scanner;


public class A4 {

	public static void main(String[] args) {
		int numOfVeh = 1;//宣告int形態的車子數量並初始化為1
		Vehicle[] vehicleArray;//宣告汽車物件陣列
		Scanner scanner = new Scanner(System.in);
		while (true) {// 取得車子數目
			System.out.println("Please input the number of cars: ");
			numOfVeh = scanner.nextInt();
			if (numOfVeh < 0) {
				System.out.println("Out of range!");
			}
			else {
				break;
			}
		}
		
		vehicleArray = new Vehicle[numOfVeh];
		for (int i = 0; i < vehicleArray.length; i++) {// 初始化汽車陣列的物件的成員值
			vehicleArray[i] = new Vehicle();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			vehicleArray[i].setBrand(scanner.next());
			System.out.print("Name: ");
			vehicleArray[i].setName(scanner.next());
			System.out.print("Max speed: ");
			vehicleArray[i].setMaxSpeed(scanner.nextFloat());
			System.out.print("Fuel consumption: ");
			vehicleArray[i].setFuelConsumption(scanner.nextFloat());
			System.out.print("Price: ");
			vehicleArray[i].setPrice(scanner.nextFloat());
			System.out.println();
		}
		
		scanner.close();//關閉scanner
		
		System.out.println("Output car status");//開始輸出結果
		for (int i = 0; i < vehicleArray.length; i++) {
			System.out.println("Car brand is " + vehicleArray[i].getBrand() + ".");
			System.out.println("Car name is " + vehicleArray[i].getName() + ".");
			System.out.println("Car max speed is " + vehicleArray[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + vehicleArray[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + vehicleArray[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			vehicleArray[i].startTurbo();
			System.out.println("Turbo status is " + vehicleArray[i].isTurbo() + ".");
			System.out.println("Current speed is " + vehicleArray[i].currentSpeed(true) + ".\n");
		}
	}

}
