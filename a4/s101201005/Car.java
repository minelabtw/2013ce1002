package ce1002.a4.s101201005;
import java.util.Random;
public class Car extends Vehicle {
	private boolean isTurbo;	// 渦輪狀態
	public void startTurbo()    // 測試啟動渦輪，可能會成功或失敗。
	{	
		  Random random=new Random();
		  if(random.nextInt(2)==1)
				isTurbo = true;
		  else
				isTurbo = false;
	}
	public boolean isTurbo()	// 取得渦輪目前狀態	
	{
		return isTurbo;
	}
}
