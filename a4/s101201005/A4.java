package ce1002.a4.s101201005;
import java.util.Scanner;

public class A4 {
	public static void main(String[] args) {
		int n=0; // the number of cars
		float m=0; // the max speed of one car
		Scanner input = new Scanner(System.in);	
		System.out.println("Please input the number of cars: ");
		n=input.nextInt(); //input=n;
		while (n<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");	
			n=input.nextInt();
		}
		Car []x = new Car [n]; // define a car array 
		for(int i=0; i<n; i++)
		{
			x[i]=new Car(); //動態產生物件Car，指派給x[i]
			System.out.println("Input car's");
			System.out.print("Brand: ");
			x[i].setBrand(input.next());	
			System.out.print("Name: ");
			x[i].setName(input.next());
			System.out.print("Max speed: ");
			x[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			x[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			x[i].setPrice(input.nextFloat());
			System.out.println();
		}
		System.out.println("Output car status");
		for(int i=0; i<n; i++)
		{
			System.out.println("Car brand is "+ x[i].getBrand() + ".");
			System.out.println("Car name is "+ x[i].getName() + ".");
			System.out.println("Car max speed is "+ x[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "+ x[i].getFuelconsumption() + ".");
			System.out.println("Car price is "+ x[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			x[i].startTurbo();
			System.out.println("Turbo status is "+x[i].isTurbo()+".");
			System.out.println("Current speed is "+x[i].currentSpeed(x[i].isTurbo())+".");
			System.out.println();
		}


	}

}
