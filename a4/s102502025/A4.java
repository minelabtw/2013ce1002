package ce1002.a4.s102502025;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		int Carnumber = 0;		//車子數量
		
		do		//判斷數量正確
		{
			System.out.println("Please input the number of cars: ");
			Carnumber = scanner.nextInt();
			if(Carnumber<1)
			{
				System.out.println("Out of range!");
			}
		}
		while(Carnumber<1);
		
		Car[] car = new Car[Carnumber];		//汽車矩陣
		String brand;
		String name;
		float maxspeed = 0;
		float fuelconsumption = 0;
		float price = 0;
		
		for (int i = 0; i < Carnumber; i++)		//輸入車種
		{
			Car Car = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand = scanner.next();
			System.out.print("Name: ");
			name = scanner.next();
			System.out.print("Max Speed: ");
			maxspeed = scanner.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelconsumption = scanner.nextFloat();
			System.out.print("Price: ");
			price = scanner.nextFloat();
			Car.setBrand(brand);
			Car.setName(name);
			Car.setMaxSpeed(maxspeed);
			Car.setFuelConsumption(fuelconsumption);
			Car.setPrice(price);
			Car.startTurbo();
			car[i] = Car;
			System.out.println();
		}
		System.out.println("Output Car status.");
		for (int x = 0; x < Carnumber; x++)		//輸出車種
		{
			System.out.println("Car brand is " + car[x].getBrand() + ".");
			System.out.println("Car name is " + car[x].getName() + ".");
			System.out.println("Car max speed is " + car[x].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+car[x].getFuelConsumption() + ".");
			System.out.println("Car price is " + car[x].getPrice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + car[x].isTurbo() + ".");
			System.out.println("Current speed is "
					+ car[x].currentSpeed(car[x].isTurbo()) + ".");
			System.out.println();
		}
		
		scanner.close();
	}
}
