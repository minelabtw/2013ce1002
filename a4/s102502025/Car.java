package ce1002.a4.s102502025;

class Car extends Vehicle{
	private boolean isTurbo; // 渦輪狀態

	public Car() {

}

	public boolean isTurbo() {		// 測試啟動渦輪，可能會成功或失敗。
		return isTurbo;
	}

	public void startTurbo() {		// 取得渦輪目前狀態
		int on = (int)(Math.random()*2);
		if(on == 1)
		{
			isTurbo = true;
		}
		else
		{
			isTurbo = false;
		}
	}
}
