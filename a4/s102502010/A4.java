package ce1002.a4.s102502010;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		int n,i;
		String brand,name;
		float maxspeed,consumption,price;
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		n=input.nextInt();
		while(n<=0)  //input
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=input.nextInt();
		}
		Car[] car = new Car[n];  //car
		for(i=0;i<n;i++)
		{
			car[i] = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand=input.next();
			car[i].setBrand(brand);
			System.out.print("Name: ");
			name=input.next();
			car[i].setName(name);
			System.out.print("Max speed: ");
			maxspeed=input.nextFloat();
			car[i].setMaxSpeed(maxspeed);
			System.out.print("Fuel consumption: ");
			consumption=input.nextFloat();
			car[i].setfuelConsumption(consumption);
			System.out.print("Price: ");
			price=input.nextFloat();
			car[i].setprice(price);
			System.out.println("");
		}
		System.out.println("Output car status");  //output
		for(i=0;i<n;i++)
		{
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getprice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + car[i].getIsTurbo() + ".");
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].getIsTurbo()) + ".");
			System.out.println("");
		}
		input.close();
	}

}
