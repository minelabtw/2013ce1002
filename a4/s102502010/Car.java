package ce1002.a4.s102502010;

import java.util.Random;

public class Car extends Vehicle{
	private boolean isTurbo;//declare a variable for car's Turbo
	Car(){
		isTurbo=false;
	}
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		Random ran = new Random();
		if(ran.nextInt(2)==0)
			isTurbo=false;
		else
			isTurbo=true;
	}
	public boolean getIsTurbo(){//get Turbo
		return isTurbo;
	}
}
