package ce1002.a4.s102502531;

import java.util.Random;

public class Vehicle {
	private String brand;	 //all variable
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;	
	
	public Random ran = new Random();
	public void setBrand(String brand) { //all vehicle
		this.brand=brand;
	}
	public void setName(String name) {
		this.name=name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption=fuelConsumption;
	}
	public void setprice(float price) {
		this.price=price;
	}
	
	 
		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getfuelConsumption() {
		return fuelConsumption;
	}
	public float getprice() {
		return price;
	}
	
	
	
		
	
	public float currentSpeed() {	
		return maxSpeed*ran.nextFloat();
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo==true)
		{
		return maxSpeed;
		}
		else
			return maxSpeed*ran.nextFloat();
	}
}
