package ce1002.a4.s102502531;

import java.util.Scanner;
import java.util.Random;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Car[] cars;// class array
		int numOfCars;

		do {
			System.out.println("Please input the number of cars: ");
			numOfCars = input.nextInt();
			if (numOfCars <= 0)
				System.out.println("Out of range!");
		} while (numOfCars <= 0);

		cars = new Car[numOfCars];

		for (int i = 0; i < cars.length; i++) { //input vehicle
			Car car = new Car();
			String brand;
			String name;
			float maxSpeed = 0;
			float fuelConsumption = 0;
			float price = 0;
			System.out.print("Input car's\nBrand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			car.setBrand(brand);
			car.setName(name);
			car.setMaxSpeed(maxSpeed);
			car.setfuelConsumption(fuelConsumption);
			car.setprice(price);
			cars[i] = car;
			System.out.println();
		}
		input.close();

		System.out.println("Output car status");

		for (int i = 0; i < cars.length; i++) { //output
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is  " + cars[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cars[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getprice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo());
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()));
		}

	}
}
