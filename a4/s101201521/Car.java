package ce1002.a4.s101201521;
//class Car is a subclass of class Vehicle
public class Car extends Vehicle{
	//constructor
	public Car(String brand, String name, float maxSpeed,
			float fuelConsumption, float price){
		//extend constructor of class Vehicle
		super(brand, name, maxSpeed, fuelConsumption, price);
	}
	//turbo is start or not
	private boolean isTurbo;
	//start turbo
	public void startTurbo(){
		isTurbo = new java.util.Random().nextBoolean();
	}
	//get turbo status
	public boolean isTurbo(){
		return isTurbo;
	}
}
