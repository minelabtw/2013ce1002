package ce1002.a4.s101201521;

public class Vehicle {
	//constructor
	public Vehicle(String brand, String name, float maxSpeed,
				float fuelConsumption, float price){
		setBrand(brand);
		setName(name);
		setMaxSpeed(maxSpeed);
		setFuelConsumption(fuelConsumption);
		setPrice(price);
	}
	//datafield
	
	//vehicle's brand
	private String brand;
	//vehicle's name
	private String name;
	//vehicle's max speed
	private float maxSpeed;
	//vehicle's fuel consumption
	private float fuelConsumption;
	//vehicle's price
	private float price;
	
	//methods
	
	//set vehicle's brand
	public void setBrand(String brand){
		this.brand = brand;
	}
	//set vehicle's name
	public void setName(String name){
		this.name = name;
	}
	//set vehicle's max speed
	public void setMaxSpeed(float maxSpeed){
		this.maxSpeed = maxSpeed;
	}
	//set vehicle's fuel consumption
	public void setFuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	//set vehicle's price
	public void setPrice(float price){
		this.price = price;
	}
	//return vehicle's brand
	public String getBrand(){
		return brand;
	}
	//return vehicle's name
	public String getName(){
		return name;
	}
	//return vehicle's max speed
	public float getMaxSpeed(){
		return maxSpeed;
	}
	//return vehicle's fuel consumption
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	//return vehicle's price
	public float getPrice(){
		return price;
	}
	//return vehicle's current speed with no parameters
	public float currentSpeed(){
		return maxSpeed * (float)Math.random();
	}
	//return vehicle's current speed with turbo status 
	public float currentSpeed(boolean isTurbo){
		if(isTurbo)
			return maxSpeed;
		else
			return maxSpeed * (float)Math.random();
	}
}
