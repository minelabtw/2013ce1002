package ce1002.a4.s101201521;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int numberOfCars = 0;
		//prompt for number of cars and check range
		do{
			System.out.println("Please input the number of cars: ");
			numberOfCars = input.nextInt();
			if(numberOfCars <= 0)
				System.out.println("Out of range!");
		}while(numberOfCars <= 0);
		Car[] cars = new Car[numberOfCars];
		//prompt for cars' brand, name, max speed, fuel consumption, and price respectively
		for(int i = 0; i < cars.length; i++){
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand = input.next();
			System.out.print("Name: ");
			String name = input.next();
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			float price = input.nextFloat();
			System.out.println();
			cars[i] = new Car(brand, name, maxSpeed, fuelConsumption, price);
		}
		input.close();
		//print cars' status respectively
		System.out.println("Output car status");
		for(int i = 0; i < cars.length; i++){
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			if(i < cars.length - 1)
				System.out.println();			
		}
	}

}
