package ce1002.a4.s995002014;

import java.util.Scanner;

public class A4 {
	private static Scanner scanner;
	public static void main(String[] args) {
		
		int numOfCar;
		//使用者輸入
		scanner = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		do 
		{
			numOfCar = scanner.nextInt();
			if (numOfCar<=0)
				System.out.println("Out of range!\nPlease input the number of cars: ");
		} while (numOfCar<=0);
		
		Car[] CarArray = new Car[numOfCar]; //Car陣列
		
		int i;
		//使用者輸入汽車數據
		for(i=0;i<numOfCar;i++) {
			Car cartemp = new Car(); //暫存一輛車
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cartemp.setBrand(scanner.next());
			System.out.print("name: ");
			cartemp.setName(scanner.next());
			System.out.print("Max Speed: ");
			cartemp.setMaxSpeed(scanner.nextFloat());
			System.out.print("Fuel consumption: ");
			cartemp.setFuel(scanner.nextFloat());
			System.out.print("Price: ");;
			cartemp.setPrice(scanner.nextFloat());
			System.out.print("\n");
			CarArray[i]=cartemp; //放入 car array
		}
		
		System.out.println("Output car status");
		
		//印出結果
		for(i=0;i<numOfCar;i++) {
			System.out.println("Car brand is "+CarArray[i].getBrand());
			System.out.println("Car name is "+CarArray[i].getName());
			System.out.println("Car max speed is "+CarArray[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "+CarArray[i].getFuel());
			System.out.println("Car price is "+CarArray[i].getPrice());
			System.out.println("StartTurbo!");
			CarArray[i].startTurbo();
			System.out.println("Turbo status is "+CarArray[i].isTurbo());
			System.out.println("Current speed is "+CarArray[i].currentSpeed(CarArray[i].isTurbo()));
			System.out.print("\n");
		}
	}

}
