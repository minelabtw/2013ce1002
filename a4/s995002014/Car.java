package ce1002.a4.s995002014;
import java.util.Random;
public class Car extends Vehicle {
	private boolean isTurbo;
	
	Car() { //建構子，先設為空
		
	}
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		 Random rand = new Random();
		 isTurbo=rand.nextBoolean(); //布林亂數
	}
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		 return isTurbo;
	}
}