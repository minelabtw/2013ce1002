package ce1002.a4.s102502529;
import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	public void setBrand(String brand) {					//設定
		this.brand=brand;
	}
	public void setName(String name) {
		this.name=name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	public void setPrice(float price){
		this.price=price;
	}
	public void setfuelConsumption(float fuelConsumption){
		this.fuelConsumption=fuelConsumption;
	}

	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {								//輸出
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getPrice(){
		return price;
	}
	public float getfuelConsumption(){
		return fuelConsumption;
	}
		
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		Random r=new Random();
		maxSpeed=r.nextFloat()*maxSpeed;
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {							//先取得是否turbo 在輸出
		if(isTurbo==true)
		return maxSpeed;
		else
		return  currentSpeed();
	}

}
