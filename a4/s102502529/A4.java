package ce1002.a4.s102502529;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		int num=0;
		do{																	//判斷
			System.out.println("Please input the number of cars: ");
			num=s.nextInt();
			if(num<=0)
				System.out.println("Out of range!");
			else
				break;
		}while(true);
		Car[] car=new Car[num];												//宣告物件陣列
		for(int i=0;i<num;i++)												//實體化
		{
			car[i]=new Car(i);
		}
		for(int i =0 ;i<num;i++){											//輸入
			System.out.print("Input car's");
			System.out.print("Brand: ");
			car[i].setBrand(s.next());
			System.out.print("Name: ");
			car[i].setName(s.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(s.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setfuelConsumption(s.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(s.nextFloat());
			System.out.println("");
		}
		for(int i =0 ;i<num;i++){													//輸出
			System.out.println("Output car status");
			System.out.println("Car brand is "+car[i].getBrand()+".");
			System.out.println("Car name is "+car[i].getName()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[i].getfuelConsumption()+".");
			System.out.println("Car price is "+car[i].getPrice()+".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			System.out.println("Current speed is "+car[i].currentSpeed(car[i].isTurbo())+".");	//先取得boolean值  在進行判斷
			System.out.println();
		}
		s.close();

	}

}
