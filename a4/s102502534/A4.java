package ce1002.a4.s102502534;

import java.util.Scanner;

import ce1002.a4.s102502534.Car;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 0;
		int i=0;
		// 檢查輸入的範圍
		while (number <= 0) {
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number <= 0) {
				System.out.println("Out of range!");
			}
		}
		// 宣告car陣列存放資料
		Car[] cars = new Car[number];
		for(int j=0;j<number;j++)
		{
			cars[j] = new Car();
		}
		
		while(i<number)
		{
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			String brand=input.next();
			cars[i].setBrand(brand);
			
			System.out.print("Name: ");
			String name=input.next();
			cars[i].setName(name);
			
			System.out.print("Max speed: ");
			Float maxSpeed=input.nextFloat();
			cars[i].setMaxSpeed(maxSpeed);
			
			System.out.print("Fuel consumption: ");
			Float fuelConsumption=input.nextFloat();
			cars[i].setFuelconsumption(fuelConsumption);
			
			System.out.print("Price: ");
			Float price=input.nextFloat();
			cars[i].setprice(price);
			
			System.out.println();
			
			i++;
		}
		// 輸出結果
		System.out.println("Output car status");
		for (int k = 0; k < number; k++) {
			System.out.println("Car brand is " + cars[k].getBrand() + ".");
			System.out.println("Car name is " + cars[k].getName() + ".");
			System.out.println("Car max speed is " + cars[k].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[k].getFuelconsumption() + ".");
			System.out.println("Car price is "+cars[k].getprice()+".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[k].isTurbo() + ".");
			System.out.println("Current speed is "+cars[k].currentSpeed(cars[k].isTurbo())+".");
			System.out.println();
		}		input.close();
	}
	}

