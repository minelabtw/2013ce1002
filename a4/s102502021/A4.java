package ce1002.a4.s102502021;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int num;
		do
		{
			System.out.println("Please input the number of cars:");
			num = input.nextInt();
			if(num<=0)
			{
				System.out.println("Out of range!");
			}
		}
		while(num<=0);
		Car Car[] = new Car[num];
		for (int i = 0; i < num; i++)
		{
			Car[i]=new Car(); 
		}
		for (int i = 0; i < num; i++)
		{
			System.out.println("Input car's");
			System.out.println("Brand: "); 
			Car[i].setBrand(input.next()); 
			System.out.println("Name: ");
			Car[i].setName(input.next());
			System.out.println("Max speed: ");
			Car[i].setMaxSpeed(input.nextFloat()); 
			System.out.println("Fuel consumption: ");
			Car[i].setfuelConsumption(input.nextFloat());
			System.out.println("Price: ");
			Car[i].setprice(input.nextFloat()); 
			System.out.println(); 
			}
		    System.out.println("Output car status"); 
			for (int i = 0; i <num; i++)
			{
				System.out.println("Car brand is "+Car[i].getBrand()+".");
				System.out.println("Car name is "+Car[i].getName()+".");
				System.out.println("Car max speed is "+Car[i].getMaxSpeed()+"."); 
				System.out.println("Car fuel consumption "+Car[i].getfuelConsumption()+".");
				System.out.println("Car price is "+Car[i].getprice()+".");
				System.out.println("StartTurbo!");
				Car[i].startTurbo();
				System.out.println("Turbo status is "+Car[i].isTurbo()+"."); 
				System.out.println("Current speed is "+Car[i].currentSpeed(Car[i].isTurbo())+"."); 
				System.out.println(); 
			}
			input.close();
			

	}

}
