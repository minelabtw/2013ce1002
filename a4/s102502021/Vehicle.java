package ce1002.a4.s102502021;
import java.util.Random;
public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	public Random random = new Random();
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String x) {
		brand = x;
	
	}
	public void setName(String y) {
		name = y;
	
	}
	public void setMaxSpeed(float z) {
		maxSpeed = z;
	
	}
	
	 
	//取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public void setfuelConsumption(float a) {	//給之後有關機車的class使用，A4不會使用到。
		fuelConsumption = a;
	
	}
	public float getfuelConsumption() 
	{
		return fuelConsumption; 
	}
	public float currentSpeed(boolean isTurbo) {	
		if (isTurbo != true)
		{
			return maxSpeed * random.nextFloat();
		}
		else 
		{
			return maxSpeed; 
			}
	}
	public void setprice(float b)
	{
		price = b; 
	}
	public float getprice() 
	{
		return price;
	} 
	

}
