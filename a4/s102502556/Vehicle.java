package ce1002.a4.s102502556;

import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	Vehicle () //Constructor
	{
		
	}
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}	
	public float getFuelConsumption() {
		return fuelConsumption;
	}	
	public float getPrice() {
		return price;
	}	
	
	//取得目前速度(多載：汽車有渦輪，但機車沒有。)
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		float i = (float)Math.random();
		return maxSpeed * i ; 
	}
	public float currentSpeed(boolean isTurbo) {
		if ( isTurbo == true ) //若渦輪啟動成功，則目前速度必為最高速度
		{
			return maxSpeed;
		}
		else //若渦輪啟動失敗，則目前速度介於0和最高速度之間，
		{
			Random rand = new Random();
			float i = rand.nextFloat();
			return maxSpeed * i;
		}
	}
}
