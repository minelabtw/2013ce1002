package ce1002.a4.s102502556;

import java.util.Scanner;

public class A4 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		System.out.println("Please input the number of cars: ");
		int num = input.nextInt(); //宣告一個int變數num，用來儲存車子的數量
		while( num <= 0 ) //檢查使用者的輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car[] cars = new Car[num]; //宣告一個名為cars的Car陣列
		for( int i = 0 ; i < num ; i++ ) //設定每台車的屬性
		{
			Car temp = new Car(); //temp為暫存的車子
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand = input.next();
			temp.setBrand(brand);
			System.out.print("Name: ");
			String name = input.next();
			temp.setName(name);
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			temp.setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			temp.setFuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			float price = input.nextFloat();
			temp.setPrice(price);
			cars[i] = temp; //將temp存進陣列裡面
			System.out.println("");
		}
		input.close(); //關閉Scanner
		System.out.println("Output car status");
		for ( int i = 0 ; i < num ; i++ ) //顯示出每台車子的屬性
		{
			System.out.println("Car brand is "+cars[i].getBrand()+".");
			System.out.println("Car name is "+cars[i].getName()+".");
			System.out.println("Car max speed is "+cars[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+cars[i].getFuelConsumption()+".");
			System.out.println("Car price is "+cars[i].getPrice()+".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is "+cars[i].isTurbo()+".");
			boolean turbo = cars[i].isTurbo(); 
			System.out.println("Current speed is "+cars[i].currentSpeed(turbo));
			System.out.println("");
		}
	}
}
