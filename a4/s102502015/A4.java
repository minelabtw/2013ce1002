package ce1002.a4.s102502015;

import java.util.Scanner;

import ce1002.a4.s102502015.Car;

public class A4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); // 新的SCANNER
		int carnumber;			//Car陣列長度
		do {
			System.out.println("Please input the number of cars: ");
			carnumber = scanner.nextInt();
			if (carnumber <= 0)
				System.out.println("Out of range!");
		} while (carnumber <= 0);
		Car Car[] = new Car[carnumber];			//宣告物件陣列
		for (int i = 0; i < carnumber; i++) {	//初始化
			Car[i]=new Car();
		}
		for (int i = 0; i < carnumber; i++) {			//設定
			System.out.println("Input car's");
			System.out.println("Brand: ");
			Car[i].setBrand(scanner.next());
			System.out.println("Name: ");
			Car[i].setName(scanner.next());
			System.out.println("Max speed: ");
			Car[i].setMaxSpeed(scanner.nextFloat());
			System.out.println("Fuel consumption: ");
			Car[i].setfuelConsumption(scanner.nextFloat());
			System.out.println("Price: ");
			Car[i].setprice(scanner.nextFloat());
			System.out.println();
		}
		
		System.out.println("Output car status"); //印出
		for (int i = 0; i < carnumber; i++) {
			System.out.println("Car brand is "+Car[i].getBrand()+".");
			System.out.println("Car name is "+Car[i].getName()+".");
			System.out.println("Car max speed is "+Car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption "+Car[i].getfuelConsumption()+".");
			System.out.println("Car price is "+Car[i].getprice()+".");
			System.out.println("StartTurbo!");
			Car[i].startTurbo();
			System.out.println("Turbo status is "+Car[i].isTurbo()+".");
			System.out.println("Current speed is "+Car[i].currentSpeed(Car[i].isTurbo())+".");
			System.out.println();
		}
		

		scanner.close();
	}

}
