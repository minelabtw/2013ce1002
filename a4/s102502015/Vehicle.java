package ce1002.a4.s102502015;

import java.util.Random;

public class Vehicle {

	public Random random = new Random();
	private String brand = ""; // 品牌
	private String name = ""; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String _brand) {
		brand = _brand;
	}

	public void setName(String _name) {
		name = _name;
	}

	public void setMaxSpeed(float _maxSpeed) {
		maxSpeed = _maxSpeed;
	}

	public void setfuelConsumption(float _fuelConsumption) {
		fuelConsumption = _fuelConsumption;
	}

	public void setprice(float _price) {
		price = _price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed(boolean isTurbo) {
		if (isTurbo != true) {
			return maxSpeed * random.nextFloat();
		} else {
			return maxSpeed;
		}
	}
}
