package ce1002.a4.s102502015;

public class Car extends Vehicle{
	Car()  //初始化
	{
	}
	private boolean isTurbo; // 渦輪狀態

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		isTurbo=random.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
