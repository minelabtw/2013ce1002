package ce1002.a4.s102502528;
import java.util.Random;

public class Vehicle {
	Random ran = new Random();
	private String brand;	//brand
	private String name;	//name
	private float maxSpeed;	//maxSpeed
	private float fuelConsumption;	// fuelConsumption
	private float price;	//price
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	} 
	public void setprice(float price) {
		this.price = price;
	}
	
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getfuelConsumption() {
		return fuelConsumption;
	} 
	public float getprice() {
		return price;
	}
		
	public float currentSpeed() {	
		return 0;
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo == false){
			return maxSpeed*ran.nextFloat();
		}
		else {
			return maxSpeed;
		}
		
	}
}
