package ce1002.a4.s102502528;
import java.util.Scanner;

public class A4 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int numOfCars;
		do {
			System.out.println("Please input the number of cars: ");  //judge the input
			numOfCars = input.nextInt();
			if (numOfCars <= 0)
				System.out.println("Out of range!");
		} while (numOfCars <= 0);
		Car[] cars = new Car[numOfCars];                    //class array
		boolean[] isTurbo=new boolean[numOfCars];           //boolean array to put isTurbo
		for (int i = 0; i < cars.length; i++)              //input all
		{
			cars[i] = new Car();
			float maxSpeed = 0,fuelConsumption,price;
			String brand,name;
			System.out.println("input car's");
			System.out.print("Brand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			maxSpeed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			System.out.println("");
			cars[i].setBrand(brand);                       //put them in to class array
			cars[i].setName(name);
			cars[i].setMaxSpeed(maxSpeed) ;
			cars[i].setfuelConsumption(fuelConsumption);
			cars[i].setprice(price);
			cars[i].startTurbo();                        //random the "isTurbo"
			isTurbo[i] = cars[i].isTurbo();
		}
		input.close();
		System.out.println("Output car status");
		for (int i = 0; i < numOfCars; i++) {                                 //output
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName()+ ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption " + cars[i].getfuelConsumption()+ ".");
			System.out.println("Car price is " + cars[i].getprice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + isTurbo[i]);
			System.out.println("Current speed is " + cars[i].currentSpeed(isTurbo[i]));
			System.out.println("");
		}
		
	}

}
