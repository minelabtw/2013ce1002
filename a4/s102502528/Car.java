package ce1002.a4.s102502528;
import java.util.Random;

public class Car extends Vehicle{
	Random ran = new Random();
	private boolean isTurbo;
	public Car() {	
	}
	public void startTurbo() 
	{
		isTurbo = ran.nextBoolean();
	}
	public boolean isTurbo() 
	{
		return isTurbo;
	}

}
