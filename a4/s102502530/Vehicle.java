package ce1002.a4.s102502530 ;   //package name
import java.util.Random ;  //import Random

public class Vehicle
{
   private String brand ;  //declare
   private String name ;
   private float maxSpeed ;
   private float fuelConsumption ;
   private float price ;
   public Random random ;

   public Vehicle()  //constructor
   {
      random = new Random() ;
   }
   public void setBrand(String brand)  //set brand
   {
      this.brand = brand ;
   }
   public void setName(String name)    //set name
   {
      this.name = name ;
   }
   public void setMaxSpeed(float maxSpeed)   //set max speed
   {
      this.maxSpeed = maxSpeed ;
   }
   public void setFuelConsumption(float fuelConsumption)    //set fuel consumption
   {
      this.fuelConsumption = fuelConsumption ;
   }
   public void setPrice(float price)   //set price
   {
      this.price = price ;
   }

   public String getBrand()   //get brand
   {
      return brand ;
   }
   public String getName()    //get name
   {
      return name ;
   }
   public float getMaxSpeed()    //get max speed
   {
      return maxSpeed ;
   }
   public float getFuelConsumption()   //get fuel consumption
   {
      return fuelConsumption ;
   }
   public float getPrice()    //get price
   {
      return price ;
   }
           
   public float currentSpeed()   //get current speed
   {
      return maxSpeed * random.nextFloat() ;
   }
   public float currentSpeed(boolean isTurbo)   //get current speed with turbo
   {	
      return isTurbo ? maxSpeed : currentSpeed() ;
   }
}
