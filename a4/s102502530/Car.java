package ce1002.a4.s102502530 ;   //package name
import ce1002.a4.s102502530.Vehicle ;  //import vehicle

public class Car extends Vehicle
{
   private boolean isTurbo;   //declare

   public void startTurbo()   //start turbo
   {
      isTurbo = random.nextBoolean() ;
   }
   public boolean isTurbo()   //get isturbo
   {
      return isTurbo ;
   }
}
