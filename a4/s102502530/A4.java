package ce1002.a4.s102502530 ;   //package name
import ce1002.a4.s102502530.Car ;   //import Car
import java.util.Scanner ;    //import Scanner

public class A4
{
   public static void main(String[] args)
   {
      int m ;  //declare
      Scanner scanner = new Scanner(System.in) ;

      System.out.println("Please input the number of cars: ") ;   //input
      do
      {
         m = scanner.nextInt() ;
         if(m <= 0)
            System.out.println("Out of range!") ;
      } while(m <= 0) ;
      Car[] car = new Car[m] ;
      for(int i = 0 ; i != m ; i++)
      {
         car[i] = new Car() ;
         System.out.println("Input car's") ;
         System.out.print("Brand: ") ;
         car[i].setBrand(scanner.next()) ;
         System.out.print("Name: ") ;
         car[i].setName(scanner.next()) ;
         System.out.print("Max speed: ") ;
         car[i].setMaxSpeed(scanner.nextFloat()) ;
         System.out.print("Fuel consumption: ") ;
         car[i].setFuelConsumption(scanner.nextFloat()) ;
         System.out.print("Price: ") ;
         car[i].setPrice(scanner.nextFloat()) ;
         System.out.println("") ;
      }

      scanner.close() ;    //close scanner

      System.out.print("Output car status") ;   //output
      for(int i = 0 ; i != m ; i++)
      {
         System.out.println("") ;
         System.out.println("Car brand is " + car[i].getBrand() + '.') ;
         System.out.println("Car name is " + car[i].getName() + '.') ;
         System.out.println("Car max speed is " + car[i].getMaxSpeed() + '.') ;
         System.out.println("Car fuel consumption is " + car[i].getFuelConsumption() + '.') ;
         System.out.println("Car price is " + car[i].getPrice() + '.') ;
         System.out.println("StartTurbo!") ;
         car[i].startTurbo() ;
         System.out.println("Turbo status is " + car[i].isTurbo() + '.') ;
         System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + '.') ;
      }
   }
}
