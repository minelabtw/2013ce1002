package ce1002.a4.s102502008;
import java.util.Scanner ;  
import java.util.Random ;
public class A4 
{
	public static void main(String args[])
	{
		Scanner input= new Scanner(System.in) ;
		int number ;
		do 
		{
			System.out.println("Please input the number of cars: ") ;
			number= input.nextInt() ;
			if(number<=0)
				System.out.println("Out of range!") ;
		}while(number<=0) ;
		//get the number
		Car[] cars= new Car[number] ;
		
		for(int i= 0 ; i<number; i++ )
		{
			Car newCar= new Car() ;
			newCar.startTurbo();
			System.out.println("Input car's") ;
			System.out.print("Brand: ");
			newCar.setBrand(input.next());
			System.out.print("Name: ");
			newCar.setName(input.next());
			System.out.print("Max speed: ");
			newCar.setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			newCar.setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			newCar.setPrice(input.nextFloat());
			System.out.println() ;
			
			cars[i]= newCar ;
		}
		//build the car
		//Output car status
		System.out.println("Output car status") ;
		for(int i= 0; i< number; i++)
		{
			System.out.println("Car brand is "+ cars[i].getBrand());
			System.out.println("Car name is "+ cars[i].getName());
			System.out.println("Car max speed is "+ cars[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "+ cars[i].getFuelConsumption());
			System.out.println("Car price is "+ cars[i].getPrice());
			//output the Turbo and speed
			System.out.println("StartTurbo!") ;
			System.out.println("Turbo status is "+ cars[i].getIsTurbo()) ;
			System.out.println("Current speed is "+ cars[i].currentSpeed(cars[i].getIsTurbo())) ;
			System.out.println();
		}
	}
}
