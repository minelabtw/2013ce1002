package ce1002.a4.s102502050;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int carnumber;
		do
		{
			System.out.println("Please input the number of cars:");
			carnumber = scanner.nextInt();
			if(carnumber<=0)
			{
				System.out.println("Out of range!");
			}
		}while( carnumber<=0);
		
		Car[] car = new Car[carnumber];
		for(int i=0;i<carnumber;i++)//輸入品牌、名稱、最高速度、油耗、價格
		{
			car[i] = new Car();
			System.out.println("Input car's");
			System.out.println("Brand: ");
			car[i].Setbrand(scanner.next());
			System.out.println("Name: ");
			car[i].Setname(scanner.next());
			System.out.println("Max speed: ");
			car[i].SetmaxSpeed(scanner.nextFloat());
			System.out.println("Fuel consumption: ");
			car[i].SetfuelConsumption(scanner.nextFloat());
			System.out.println("Price: ");
			car[i].Setprice(scanner.nextFloat());	
		}
		
		System.out.println("Output car status");
		for(int i=0;i<carnumber;i++)//輸出品牌、名稱、最高速度、油耗、價格、當前速度、渦輪是否成功啟動
		{
			
			System.out.println("Car brand is " + car[i].Getbrand() +".");
			System.out.println("Car name is " + car[i].Getname()+".");
			System.out.println("Car max speed is " + car[i].GetmaxSpeed()+".");
			System.out.println("Car fuel consumption is " + car[i].GetfuelConsumption()+".");
			System.out.println("Car price is " + car[i].Getprice()+".");
			System.out.println("StartTurbo!");
			car[i].startTubro();
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			
			System.out.println("Current speed is " +car[i].currentSpeed(car[i].isTurbo()) +".");
			System.out.println("");
		}
	}

}
