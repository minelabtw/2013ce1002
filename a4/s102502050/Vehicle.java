package ce1002.a4.s102502050;
import java.util.Random;
public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public Vehicle ()
	{
		
	}
	
	//設定品牌、名稱、最高速度、油耗、價格
	public void Setbrand(String brand)
	{
		this.brand = brand;
	}
	public void Setname(String name)
	{
		this.name = name;
	}
	public void SetmaxSpeed(float maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}
	public void SetfuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}
	public void Setprice(float price)
	{
		this.price = price;
	}
	

	//取得品牌、名稱、最高速度、油耗、價格	
	public String Getbrand()
	{
		return brand;
	}
	public String Getname()
	{
		return name;
	}
	public float GetmaxSpeed()
	{
		return maxSpeed;
	}
	public float GetfuelConsumption()
	{
		return fuelConsumption;
	}
	public float Getprice()
	{
		return price;
	}
	
	//取得目前速度
	public float currentSpeed()
	{
		float currentspeed=0;
		return currentspeed;
	}
	public float currentSpeed(boolean isTurbo)
	{
		Random random = new Random();
		if( isTurbo== true )
		{
			return maxSpeed;
		}
		else
		{
			return maxSpeed * (random.nextFloat());
		}
		
	}
	
	
}
