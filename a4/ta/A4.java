package ce1002.a4.ta;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		Car[] cars;		//用來存放car物件的陣列
		int numOfCars;	//car的數量
		
		do {
			System.out.println("Please input the number of cars: ");
			numOfCars = input.nextInt();
			if (numOfCars <= 0)
				System.out.println("Out of range!");
		} while (numOfCars <= 0);
		cars = new Car[numOfCars];
		
		//輸入各項Car屬性並進行設定
		for (int i = 0; i < cars.length; i++) {
			Car car = new Car();
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			String brand = input.next();
			car.setBrand(brand);
			
			System.out.print("Name: ");
			String name = input.next();
			car.setName(name);
			
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			car.setMaxSpeed(maxSpeed);
			
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			car.setfuelConsumption(fuelConsumption);
			
			System.out.print("Price: ");
			float price = input.nextFloat();
			car.setPrice(price);

			cars[i] = car;// 將car放到cars陣列裡
			System.out.println();
		}

		input.close();// 用不到了，要關掉
		
		//輸出各項Car屬性，並輸出試乘測試Turbo之結果
		System.out.println("Output car status");
		for (int i = 0; i < cars.length; i++) {
			
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + ".");
			System.out.println();
		}
	}
}
