package ce1002.a4.s100203020;
import java.util.Random;

public class Vehicle {

	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelconsumption;
	private float price;
	
	

	//getting brand, name, maxSpeed, fuelconsumption, and price.
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelconsumption() {
		return fuelconsumption;
	}

	public float getPrice() {
		return price;
	}

	
	//setting brand, name, maxSpeed, fuelconsumption, and price.
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setFuelconsumption(float fuelconsumption) {
		this.fuelconsumption = fuelconsumption;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	//for next but not now
	public float currentSpeed(){
		Random ran = new Random();
		return maxSpeed * ran.nextFloat();
	}
	public float currentSpeed(boolean isTurbo){
		Random ran = new Random();
		return (isTurbo) ? (maxSpeed) : (maxSpeed * ran.nextFloat());
	}
	
	
}
