package ce1002.a4.s100203020;

import java.util.Scanner;

public class A4 {
	
	private static Scanner scanner;
	
	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		
		Car[] car;//array of car
		int carNum;//determine numbers of car
		
		
		do{
			System.out.println("Please input the number of cars: ");
			carNum = scanner.nextInt();
			
			//numbers of car must be bigger than 0;
			if (carNum<=0)
				System.out.println("Out of range!");
		}while(carNum<=0);
		
		//declare car
		car = new Car[carNum];
		
		for(int i=0 ; i < carNum; i++){
			
			car[i]=new Car();
	
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			car[i].setBrand(scanner.next());
			
			System.out.print("Name: ");
			car[i].setName(scanner.next());
			
			System.out.print("Max Speed: ");
			car[i].setMaxSpeed(scanner.nextFloat());
			
			System.out.print("Fuel consumption: ");
			car[i].setFuelconsumption(scanner.nextFloat());
			
			System.out.print("Price: ");
			car[i].setPrice(scanner.nextFloat());
			System.out.println();
		}
		for(int i=0 ; i < carNum; i++){
			System.out.println("Output car stutas");
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + car[i].getFuelconsumption() + ".");
			System.out.println("Car price is " + car[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			car[i].startTurbo();
			System.out.println("Turbo status is " + car[i].isTurbo() + ".");
		
			System.out.println("Current speed is " + car[i].currentSpeed(car[i].isTurbo()) + ".\n");
		}
		
		
		scanner.close();
		
	}
		
}

