package ce1002.a4.s100203020;
import java.util.Random;
public class Car extends Vehicle{
	
	private boolean isTurbo; //the condition of Turbo.
	
	public void startTurbo(){ //Testing start Turbo.
		Random ran = new Random();
		this.isTurbo = ran.nextBoolean();
	}
	
	public boolean isTurbo(){ //To get condition of Turbo .
		return isTurbo;
	}

}
