package ce1002.a4.s102302053;

public class Car extends Vehicle {//繼承vehicle
	private boolean isTurbo;
	public int carid;
	public Car(int id){
		carid = id;		
	}

	public void startTurbo() {//測試啟動渦輪，可能會成功或失敗。
		float temp = ran.nextFloat();//隨機產生一個介於0跟1之間的數,決定渦輪狀態
		if (temp > 0.5){
			isTurbo = true;
		}
		else{
			isTurbo = false;
		}		
	}
	public boolean isTurbo() {//取得渦輪目前狀態
		return isTurbo;
	}
}
