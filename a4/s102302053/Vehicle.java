package ce1002.a4.s102302053;
import java.util.Random;
public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	Random ran = new Random();
	public Vehicle(){//設置初始屬性
		brand = "0";
		name = "0";
		maxSpeed = 0;
		fuelConsumption = 0;
		price = 0;
	
	}
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
	    this.maxSpeed = maxSpeed;
	}
	public void setfuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	public void setPrice(float price){
		this.price = price;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;	
	}
	public float getMaxSpeed() {
		return maxSpeed;	
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {//給之後有關機車的class使用，A4不會使用到。
		float temp =ran.nextFloat();
		return maxSpeed * temp;
	}
	public float currentSpeed(boolean isTurbo) {
		float temp =ran.nextFloat();//隨機產生一個介於0跟1之間的數
		if(isTurbo == true){//渦輪啟動成功,傳回最高速度作為當前速度
			return maxSpeed;
		}
		else{//渦輪啟動失敗,回傳一個介於0跟最高速度之間的數作為當前速度
			return maxSpeed * temp;
		}
	}
}


