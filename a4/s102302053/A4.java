package ce1002.a4.s102302053;

import java.util.Scanner;

public class A4 {
	private static Scanner scanner;

	public static void main(String[] args) {
		int carnum;
		scanner = new Scanner(System.in);
		do//提示使用者輸入車輛數目,並且檢查範圍是否符合要求
		{
			System.out.println("Please input the number of cars: ");
			carnum = scanner.nextInt();
			if(carnum <= 0){
				System.out.println("Out of range!");
			}			
		}while(carnum <= 0);
		Car[] car=new Car[carnum];//創造car陣列
		
		String tempBrand, tempName;//作為輸入資料的暫存值
		float tempMaxSpeed, tempFuelConsumption, tempPrice;
		for(int a = 0; a < carnum; a++){//依序提示使用者輸入車輛的資訊
			System.out.print("Input car's\nBrand: ");
			tempBrand = scanner.next();
			System.out.print("Name: ");
			tempName = scanner.next();
			System.out.print("Max speed: ");
			tempMaxSpeed = scanner.nextFloat();
			System.out.print("Fuel consumption: ");
			tempFuelConsumption = scanner.nextFloat();
			System.out.print("Price: ");
			tempPrice = scanner.nextFloat();
			car[a] = new Car(a);//依序將剛剛輸入的資訊傳到對應的car物件中
			car[a].setBrand(tempBrand);
			car[a].setName(tempName);
			car[a].setMaxSpeed(tempMaxSpeed);
			car[a].setfuelConsumption(tempFuelConsumption);
			car[a].setPrice(tempPrice);	
			System.out.println();
		}
		System.out.println("Output car status");
		for(int a = 0; a < carnum; a++){//依序輸出車輛資訊
			System.out.println("Car brand is " + car[a].getBrand() + "." + "\nCar name is " + car[a].getName() + "." + "\nCar max speed is " + 
			car[a].getMaxSpeed() + "." + "\nCar fuel consumption is " + car[a].getFuelConsumption() + "." + "\nCar price is " + car[a].getPrice() + ".");
			System.out.print("StartTurbo!\nTurbo status is ");
			car[a].startTurbo();
			System.out.println(car[a].isTurbo() + "." + "\nCurrent speed is " + car[a].currentSpeed(car[a].isTurbo()) + ".\n");
						
		}
	

	}

}
