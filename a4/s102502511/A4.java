package ce1002.a4.s102502511;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		
	int carsnumber; //車輛數
	String brand; //車種
	String name; //車名
	float maxspeed; //最高速
	float fuelconsumption; //耗油量
	float price; //價錢
	
	Scanner input = new Scanner(System.in);
	
	do{ //檢查車輛數
		System.out.println("Please input the number of cars: ");
		carsnumber = input.nextInt();
		if(carsnumber < 1 ){
			System.out.println("Out of range!");
		}
	}while(carsnumber < 1 );
	
	Car cars[] = new Car[carsnumber]; ////宣告car陣列裝Car裡的東西
	
	for(int i = 0 ; i < carsnumber ; i++){
		cars[i] = new Car(i);//此陣列存所有car變數的資料 以及做Car裡面的動作
		
		System.out.println("Input car's");
		System.out.print("Brand: ");
		brand = input.next(); //儲存輸出的車種
		cars[i].setBrand(brand); //做記錄動作
		
		System.out.print("Name: ");
		name = input.next(); //儲存輸入的名字
		cars[i].setName(name); //做記錄動作
		
		System.out.print("Max speed: ");
		maxspeed = input.nextFloat(); //儲存最高速度
		cars[i].setMaxSpeed(maxspeed); //做記錄動作
		
		System.out.print("Fuel consumption: ");
		fuelconsumption = input.nextFloat();//儲存油耗量
		cars[i].setfuelConsumption(fuelconsumption);//做記錄動作
		
		System.out.print("Price: ");
		price = input.nextFloat();//儲存價錢
		cars[i].setprice(price);//做記錄動作
		
		cars[i].starTurbo(); //做隨機變數
		
		System.out.println();
	}
	
	System.out.println("Output car status");
	for(int i = 0 ; i < carsnumber ; i ++){ //顯示所有儲存在函式裡的內容
		System.out.println("Car brand is " + cars[i].getBrand()+"."); 
		System.out.println("Car name is " + cars[i].getName()+".");
		System.out.println("Car max speed is " + cars[i].getMaxSpeed()+".");
		System.out.println("Car fuel consumption is " + cars[i].getfuelConsumption()+".");
		System.out.println("Car price is " + cars[i].getprice()+".");
		System.out.println("StartTurbo!");
		System.out.println("Turbo status is " + cars[i].isTurbo()+".");
		System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo())+".");
		System.out.println();
	}
	
	input.close();
	
	}

}
