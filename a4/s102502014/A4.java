package ce1002.a4.s102502014;

import java.util.Scanner;

import ce1002.a4.s102502014.Car;
import ce1002.a4.s102502014.Vehicle;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a; // 車子數目
		String b;
		String n;
		float maxS;
		float fc;
		float p;
		Scanner scanner = new Scanner(System.in); // new一個scanner物件
		System.out.println("Please input the number of cars: ");
		a = scanner.nextInt(); // scanner物件使用nextInt()方法
		while (a <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			a = scanner.nextInt();
		}
		Car Car[] = new Car[a]; // new一個物件陣列 儲存物件
		for (int i = 0; i < a; i++) { //用建構函式初始化物件陣列
			Car[i] = new Car(i);
		}
		for (int i = 0; i < a; i++) {
			System.out.println("Input car's");
			System.out.print("Brand: ");
			b = scanner.next();
			Car[i].setBrand(b);
			System.out.print("Name: ");
			n = scanner.next();
			Car[i].setName(n);
			System.out.print("Max speed: ");
			maxS = scanner.nextFloat();
			Car[i].setMaxSpeed(maxS);
			System.out.print("Fuel consumption: ");
			fc = scanner.nextFloat();
			Car[i].setfuelConsumption(fc);
			System.out.print("Price: ");
			p = scanner.nextFloat();
			Car[i].setprice(p);
			System.out.println("");

		}
		System.out.println("Output car status.");
		for (int i = 0; i < a; i++) {
			System.out.println("Car brand is " + Car[i].getBrand() + ".");
			System.out.println("Car name is " + Car[i].getName() + ".");
			System.out
					.println("Car max speed is " + Car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "
					+ Car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + Car[i].getprice() + ".");
			System.out.println("StartTurbo!");
			Car[i].startTurbo();
			System.out.println("Turbo status is " + Car[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ Car[i].currentSpeed(Car[i].isTurbo()) + ".");
			System.out.println("");
		}
		scanner.close();
	}
}
