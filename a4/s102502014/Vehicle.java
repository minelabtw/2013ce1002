package ce1002.a4.s102502014;

import java.util.Random;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	//設定品牌、名稱、最高速度、油耗、價格
	Random rand = new Random();    

	public void setBrand(String b) {
		brand=b;
	}
	public void setName(String n) {
		name=n;
	}
	public void setMaxSpeed(float maxS) {
		maxSpeed=maxS;
	}
	public void setfuelConsumption(float fc) {
		fuelConsumption=fc;
	}
	public void setprice(float p) {
		price=p;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getfuelConsumption() {
		return fuelConsumption;
	}
	public float getprice() {
		return price;
	}	
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	//public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。

	//}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo==true)
		{return maxSpeed;}
		else
		return maxSpeed*rand.nextFloat();
	}
}
