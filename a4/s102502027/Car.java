package ce1002.a4.s102502027;

import java.util.Random;

public class Car extends Vehicle {

	private boolean isTurbo;

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		Random ran = new Random();
		int r= ran.nextInt(2);
		if (r == 1)
			isTurbo = true;
		else
			isTurbo = false;
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}

	public Car(String brand, String name, float maxSpeed,
			float fuelConsumption, float price) {
		startTurbo();
		setBrand(brand); 
		setName(name);
		setMaxSpeed(maxSpeed);
		setfuelConsumption(fuelConsumption);
		setprice(price);
		currentSpeed(isTurbo);
	}

}
