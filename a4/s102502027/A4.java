package ce1002.a4.s102502027;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		String brand= null; // declare
		String name =null;
		float speed = 0;
		float cons = 0;
		float price = 0;
		int num = 0;
		System.out.println("Please input the number of cars: ");
		num = input.nextInt(); 
		while (num <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car car[] = new Car[num]; // declare array
		for (int i = 0; i < num; i++) { 
			System.out.print("Input car's ");
			System.out.print("Brand: ");
			brand = input.next();
			System.out.print("Name: ");
			name = input.next();
			System.out.print("Max speed: ");
			speed = input.nextFloat();
			System.out.print("Fuel consumption: ");
			cons = input.nextFloat();
			System.out.print("Price: ");
			price = input.nextFloat();
			System.out.println();
			car[i] = new Car(brand, name, speed, cons, price);
			// insert into car[]
		}
		for (int i = 0; i < num; i++) { // output
			System.out.println("Output car status");
			System.out.println("Car brand is " + car[i].getBrand() + ".");
			System.out.println("Car name is " + car[i].getName() + ".");
			System.out
					.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "
					+ car[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[i].getprice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+car[i].isTurbo()+ ".");
			System.out.println("Current speed is "+ car[i].currentSpeed(car[i].isTurbo()) + ".");
			System.out.println();
		}
	}

}
