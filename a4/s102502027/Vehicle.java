package ce1002.a4.s102502027;

import java.util.Random;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;  //reset
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public void setprice(float price) {
		this.price = price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand; // 回傳
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	public float currentSpeed() {
		return maxSpeed;
	}
	
	public float currentSpeed(boolean isTurbo) {
		Random ran =new Random();
		float r= ran.nextFloat(); //產生隨機變數
		if(isTurbo==false)
		return maxSpeed * r;
		else
		return maxSpeed;
	}

}
