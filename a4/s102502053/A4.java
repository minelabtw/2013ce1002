package ce1002.a4.s102502053;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		//call variables
		int number;
		float speed;
		float fuel;
		float price;
		String brand;
		
		//number of cars input and data validation
		do
		{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number<=0)
			{
				System.out.println("Out of range!");
			}
		}while(number<=0);
		
		//create new object;
		Car[] arr = new Car[number];
		
		//data input and save to array
		for(int a = 0; a<number; a++)
		{
			arr[a] = new Car();
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			arr[a].setBrand(input.next());
			
			System.out.print("Name: ");
			arr[a].setName(input.next());
			
			System.out.print("Max speed: ");
			arr[a].setMaxSpeed(input.nextFloat());
			
			System.out.print("Fuel consumption: ");
			arr[a].setFuelConsumption(input.nextFloat());
			
			System.out.print("Price: ");
			arr[a].setPrice(input.nextFloat());
			
			System.out.print("\n");
		}
		
		//result output
		System.out.println("Output car status");
		for( int a = 0; a <number; a++ ) 
		{
			System.out.println("Car brand is " + arr[a].getBrand() + ".");
			System.out.println("Car name is " + arr[a].getName() + ".");
			System.out.println("Car max speed is " + arr[a].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + arr[a].getFuelConsumption()  + ".");
			System.out.println("Car price is " + arr[a].getPrice() + ".");
			
			System.out.println("StartTurbo!");
			arr[a].startTurbo();
			System.out.println("Turbo status is " + arr[a].isTurbo() + ".");
			System.out.println("Current speed is " + arr[a].currentSpeed(arr[a].isTurbo()) + ".");
			System.out.print("\n");

		}		
		
	}

}
