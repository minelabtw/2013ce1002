package ce1002.a4.s102502053;

import java.util.Random;

public class Car extends Vehicle{
	
	private boolean isTurbo;
	
	Car() //constructor
	{
		
	}
	
	public void startTurbo()  //random for turbo
	{
	
		Random random = new Random();
		
		if(random.nextBoolean() == true)
		{
			isTurbo = true;
		}else 
		isTurbo = false;
		
	}
	
	public boolean isTurbo()  //return turbo
	{	
		
		return isTurbo;	
	}
}
