package ce1002.a4.s102502053;

import java.util.Random;

public class Vehicle {

	//call variables
	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;
	
	//set variables
	public void setBrand(String Brand)
	{
		brand = Brand;
	}
	public void setName(String Name) 
	{
		name = Name;
	}
	public void setMaxSpeed(float MaxSpeed) 
	{
		maxSpeed = MaxSpeed;
	}
	public void setFuelConsumption(float FuelConsumption)
	{
		fuelConsumption = FuelConsumption;
	}
	public void setPrice(float Price)
	{
		price = Price;
	}
		
		 
	//return methods	
	public String getBrand() 
	{
		return brand;
	}
	public String getName() 
	{
		return name;
	}
	public float getMaxSpeed() 
	{
		return maxSpeed;
	}
	public float getFuelConsumption()
	{
		return fuelConsumption;
	}
	public float getPrice()
	{
		return price;
	}
		
	//turbo and calculate for speed
	public float currentSpeed(boolean isTurbo) 
	{	
		Random random2 = new Random();
		if(isTurbo==true)
		    return maxSpeed;
		else
			return maxSpeed*(random2.nextFloat());
	}
}
