package ce1002.a4.s102502039;

import java.util.Scanner;

public class A4 {

	private static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);

		System.out.println("Please input the number of cars: ");// 輸出題目
		int number = input.nextInt();// 輸入變數
		while (number < 1) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}
		Car[] car = new Car[number];// 存入陣列
		for (int i = 0; i < number; i++) {
			car[i] = new Car();
		}
		for (int i = 0; i < number; i++) {// 進行輸入
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand = input.next();
			car[i].setBrand(brand);
			System.out.print("Name: ");
			String name = input.next();
			car[i].setName(name);
			System.out.print("Max speed: ");
			float maxSpeed = input.nextFloat();
			car[i].setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			car[i].setfuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			float price = input.nextFloat();
			car[i].setprice(price);
			System.out.println();
		}
		System.out.println("Output car status");// 進行輸出
		for (int j = 0; j < number; j++) {

			System.out.println("Car brand is " + car[j].getBrand() + ".");
			System.out.println("Car name is " + car[j].getName() + ".");
			System.out
					.println("Car max speed is " + car[j].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is "
					+ car[j].getfuelConsumption() + ".");
			System.out.println("Car price is " + car[j].getprice() + ".");
			System.out.println("StartTurbo!");
			car[j].startTurbo();
			System.out.println("Turbo status is " + car[j].isTurbo() + ".");
			System.out.println("Current speed is "
					+ car[j].currentSpeed(car[j].isTurbo()) + ".");
			System.out.println();
		}
	}

}
