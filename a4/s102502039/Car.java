package ce1002.a4.s102502039;

public class Car extends Vehicle {
	private boolean isTurbo;// 渦輪狀態

	public Car() {
		super();
	}

	public void startTurbo() {// 測試啟動渦輪，可能會成功或失敗
		isTurbo = (Math.random() > 0.5);
	}

	public boolean isTurbo() {// 取得渦輪目前狀態
		return isTurbo;
	}

}
