package ce1002.a4.s102502039;

public class Vehicle {
	private String brand;// 品牌
	private String name;// 款式名稱
	private float maxSpeed;// 最高速度
	private float fuelConsumption;// 油耗
	private float price;// 價格

	public void setBrand(String brand) {// 設定品牌、名稱、最高速度、油耗、價格
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public void setprice(float price) {
		this.price = price;
	}

	public String getBrand() {// 取得品牌、名稱、最高速度、油耗、價格
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	public float currentSpeed() {
		return maxSpeed;
	}

	public float currentSpeed(boolean isTurbo) {
		float a = (float) (Math.random() * 1);
		if (isTurbo == true) {
			return maxSpeed;
		} else {
			return maxSpeed * a;
		}
	}

}
