package ce1002.a4.s102502505;

import java.util.Scanner;
import java.util.Random;

public class A4 extends Car{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		boolean randnum1 = false;//定義渦輪亂數
		double randnum2;//定義隨機變數為型態
		int carnum=0;//定義車子數量，並把初始值設為0
		Car[] cars;//定義cars陣列
		Random ran = new Random();//將隨機變數定為物件
		
		System.out.println("Please input the number of cars: ");//輸出字串
		carnum=input.nextInt();//讀取車子數量
		while(carnum<0)//將車子數量限制範圍>0
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			carnum=input.nextInt();
		}
		
		cars = new Car[carnum];//將cars陣列設為物件
		for (int a=0;a<cars.length;a++)//利用迴圈重複下列動作
		{
			Car car = new Car(a);//將car設為在Car.java下的物件
			
			System.out.println("Input car's");//輸出字串
			System.out.print("Brand: ");
			car.setBrand(input.next());//將品牌名稱傳入car下的brand名
			
			System.out.print("Name: ");//以下皆重複下列工作
			car.setName(input.next());
			
			System.out.print("Max speed: ");
			car.setMaxSpeed(input.nextFloat());
		
			System.out.print("Fuel consumption: ");
			car.setFuelConsumption(input.nextFloat());
			
			System.out.print("Price: ");
			car.setPrice(input.nextFloat());
			
			System.out.println();
			cars[a] = car;//放入cars陣列中
		}input.close();//關閉input
		
		System.out.println("Output car status");//輸出字串
		
		for(int b=0;b<carnum;b++)
		{
			Car car = new Car(0);//將Car.java變成物件
			randnum1 = ran.nextBoolean();//設布林亂數
			randnum2 = ran.nextDouble();//設現在速度的亂數
			
			System.out.println("Car brand is "+cars[b].getBrand()+".");//輸出字串，並顯示位於陣列中的brand名
			System.out.println("Car name is "+cars[b].getName()+".");
			System.out.println("Car maxspeed is "+cars[b].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+cars[b].getFuelConsumption()+".");
			System.out.println("Car price is "+cars[b].getPrice()+".");
			
			System.out.println("StartTurbo!");//假如隨機值為1，即是開啟渦輪成功
			if (randnum1==true)
			{
				car.startTurbo(true);//將true傳回startTurbo函式
				System.out.println("Turbo status is "+car.isTurbo()+".");//輸出所存之結果
				System.out.println("Current speed is "+cars[b].getMaxSpeed()+".");//輸出最大速度
			}
			else//其他則失敗
			{
				car.startTurbo(false);
				System.out.println("Turbo status is "+car.isTurbo()+".");
				System.out.println("Current speed is "+cars[b].getMaxSpeed()*randnum2+".");//輸出當下速度
			}
			
			System.out.println();//空一行
		}
		
		
		
		
		
		

	}

}
