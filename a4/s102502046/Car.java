package ce1002.a4.s102502046;

import java.util.Random;

public class Car extends Vehicle
{
	private boolean isTurbo;
	public void startTurbo() // 測試啟動渦輪，可能會成功或失敗。
	{	
		Random ran = new Random();
		int x = ran.nextInt(2) ;
		if (x==0)
			isTurbo=false ;
		else 
			isTurbo=true ;
	}
	public boolean isTurbo() // 取得渦輪目前狀態	
	{	
			return isTurbo;
	}
}
