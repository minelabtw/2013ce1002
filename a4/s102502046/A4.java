package ce1002.a4.s102502046;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) 
	{
		Scanner in = new Scanner(System.in);
		int n;
		String Brand="",Name="";
		float FuelConsumption,Price,MaxSpeed;
		while(true) 
		{
			System.out.println("Please input the number of cars: ");
			n = in.nextInt();
			if(n>0)
				break;
			System.out.println("Out of range!");			
		}  
		
		Car[] c = new Car[n];
		
		for (int i=0;i<n;i++)
		{
			c[i] = new Car() ;
			System.out.println("Input car's");
			System.out.print("Brand: ");
			c[i].setBrand(in.next());
			
			System.out.print("Name: ");
			c[i].setName(in.next());
			
			System.out.print("Max speed: ");
			c[i].setMaxSpeed(in.nextInt());
			
			System.out.print("Fuel consumption: ");
			c[i].setFuelConsumption(in.nextFloat());
			
			System.out.print("Price: ");
			c[i].setPrice(in.nextInt());
			
			System.out.println();
		}
		System.out.print("Output car status");
		for (int i=0;i<n;i++)
		{
			System.out.println();
			System.out.println("Car brand is " + c[i].getBrand()  + ".");
			System.out.println("Car name is " + c[i].getName()  + ".");
			System.out.println("Car max speed is " + c[i].getMaxSpeed()  + ".");
			System.out.println("Car fuel consumption is " + c[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + c[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			c[i].startTurbo();
			System.out.println("Turbo status is " + c[i].isTurbo() + ".");
			System.out.println("Current speed is " + c[i].currentSpeed(c[i].isTurbo()));
		}
		
	}

}
