package ce1002.a4.s102502002;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int numc=0;
		numc = input.nextInt();
		while(numc<0){// check if number of cars is out of range
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			numc = input.nextInt(); // 
		}
		Car car[] = new Car[numc];
		System.out.println("Input car's"); // input car status	
		for(int i=0; i<numc; i++){
			car[i] = new Car();
			System.out.print("Brand: ");
			car[i].setBrand(input.next()); // set car brand using setBrand function in class Vehicle
			System.out.print("Name: ");
			car[i].setName(input.next());
			System.out.print("Max speed: ");
			car[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			car[i].setConsumption(input.nextFloat());
			System.out.print("Price: ");
			car[i].setPrice(input.nextFloat());
			System.out.println();
		}
		input.close();//close object input after using
		System.out.println("Output car status"); // output the result
		for(int i=0; i<car.length; i++){
			car[i].startTurbo(); //start turbo, could be true or false
			System.out.println("Car brand is "+car[i].getBrand()+".");//output brand, get from cars array
			System.out.println("Car name is "+car[i].getName()+".");
			System.out.println("Car max speed "+car[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[i].getConsumption()+".");
			System.out.println("Car price is "+car[i].getPrice()+".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+car[i].isTurbo()+".");
			System.out.println("Current speed is "+car[i].currentSpeed(car[i].isTurbo())+".");
			System.out.println();
			
		}
	}

}
