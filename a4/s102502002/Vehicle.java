package ce1002.a4.s102502002;
import java.util.Random;
public class Vehicle {

	private String brand;
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand=brand; // change the string brand
	}
	public void setName(String name) {
		this.name=name; // change string name
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	public void setConsumption(float fuelConsumption) {
		this.fuelConsumption=fuelConsumption;
	}
	public void setPrice(float price) {
		this.price=price;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	
	public float currentSpeed() {
		return 0;
	}
	public float currentSpeed(boolean isTurbo) {	
		Random ran = new Random();
		float speed=0;
		if(isTurbo==false) // if there's no turbo, return a value smaller or equals to maxspeed 
			speed=maxSpeed*ran.nextFloat();
		else if(isTurbo==true) // if there is turbo, return maxspeed
			speed=maxSpeed;
		return speed;
	}
}
