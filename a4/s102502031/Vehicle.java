package ce1002.a4.s102502031;

public class Vehicle {
	private String brand;
	private String name;
	private float maxSpeed;
	private float fuelConsumption;
	private float price;

	public Vehicle() {
	}

	public void set_brand(String brand) {
		this.brand = brand;
	}

	public void set_name(String name) {
		this.name = name;
	}

	public void set_maxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void set_fuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public void set_price(float price) {
		this.price = price;
	}

	public String get_brand() {
		return brand;
	}

	public String get_name() {
		return name;
	}

	public float get_maxSpeed() {
		return maxSpeed;
	}

	public float get_fuelConsumption() {
		return fuelConsumption;
	}

	public float get_price() {
		return price;
	}

	public float currentSpeed() {
		return (float) (maxSpeed * Math.random()); // motorcycle has no turbo
	}

	public float currentSpeed(boolean isTurbo) {
		if (isTurbo)
			return maxSpeed;
		else
			return (float) (maxSpeed * Math.random());
	}
}
