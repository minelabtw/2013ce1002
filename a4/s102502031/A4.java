package ce1002.a4.s102502031;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner jin = new Scanner(System.in);
		int number = numberTest(jin);
		Car[] a = new Car[number];
		setProperty(a, jin);
		printResult(a);
		jin.close();
	}

	public static int numberTest(Scanner input) {
		double test;
		do {
			System.out.println("Please input the number of cars: ");
			test = input.nextDouble();
			if ((double) ((int) test) != test) { // if number is not integer, change its value to be 0
				test = 0;
			}
			if (test <= 0) { // reprompt if the value is not in range
				System.out.println("Out of range!");
			}
		} while (test <= 0);
		return ((int) test);
	}

	public static void setProperty(Car[] x, Scanner input) {
		for (int i = 0; i < x.length; i++) {
			x[i] = new Car(); // creat the car in array car
			System.out.println("Input car's");
			System.out.println("Brand: ");
			x[i].set_brand(input.next());
			System.out.println("Name: ");
			x[i].set_name(input.next());
			System.out.println("Max speed: ");
			x[i].set_maxSpeed(input.nextFloat());
			System.out.println("Fuel consumption: ");
			x[i].set_fuelConsumption(input.nextFloat());
			System.out.println("Price: ");
			x[i].set_price(input.nextFloat());
			System.out.println();
		}
	}

	public static void printResult(Car[] x) {
		for (int i = 0; i < x.length; i++) {
			System.out.println("Output car status");
			System.out.println("Car brand is " + x[i].get_brand() + ".");
			System.out.println("Car name is " + x[i].get_name() + ".");
			System.out.println("Car max speed is " + x[i].get_maxSpeed() + ".");
			System.out.println("Car fuel consumption is " + x[i].get_fuelConsumption() + ".");
			System.out.println("Car price is " + x[i].get_price() + ".");
			System.out.println("StartTurbo!");
			x[i].startTurbo(); // random the turbo
			System.out.println("Turbo status is " + x[i].isTurbo() + ".");
			System.out.println("Current speed is " + x[i].currentSpeed(x[i].isTurbo()) + ".");
			if (i + 1 != x.length)
				System.out.println();
		}
	}
}
