package ce1002.a4.s102502031;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;

	public Car() {
	}

	public void startTurbo() {
		Random a = new Random();
		isTurbo = a.nextBoolean();
	}

	public boolean isTurbo() {
		return isTurbo;
	}
}
