package ce1002.a4.s102502040;

public class Vehicle {
	
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格

	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String _brand) {
		brand = _brand;
	}
	public void setName(String _name) {
		name = _name;
	}
	public void setMaxSpeed(float _maxSpeed) {
		maxSpeed = _maxSpeed;
	}
	public void setFuelconsumption(float _fuelConsumption){
		fuelConsumption = _fuelConsumption;
	}
	public void setPrice(float _price){
		price = _price;
	}
	
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		return maxSpeed * ((float)java.lang.Math.random() *1);
	}
	public float currentSpeed(boolean isTurbo) {
		return  (isTurbo) ? (maxSpeed) : maxSpeed * ((float)java.lang.Math.random()*1);
	}
}
