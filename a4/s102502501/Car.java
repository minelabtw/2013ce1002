package ce1002.a4.s102502501;
public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
 
	public Car(int id) {// constructor,有傳入id
		this.id = id;
	}
 
	public int getId() {
		return id;
	}
 
	public float getMaxSpeed() {
		return maxSpeed;
	}
 
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
 
	public boolean isTurbo() {
		return isTurbo;
	}
 
	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}
 
}
