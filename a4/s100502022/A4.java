package ce1002.a4.s100502022;
import java.util.*;
public class A4 {
	public static void main(String args[]){
		Scanner s = new Scanner(System.in);
		int number;
		//check numberOfcars
		do{
			System.out.println("Please input the number of cars: ");
			number=s.nextInt();
			if(number>0){
				break;
			}
			else{
				System.out.println("Out of range!");
			}
		}while(number<=0);
		//initial
		String brand;//
		String name;//
		float maxSpeed;//
		float fuelConsumption;//
		float price;
		//new a array for cars
		Car carr[]=new Car[number];
		
		for(int i=0;i<number;i++){
			//**very important about object array**
			//new a car ant insert into carr one by one
			Car thecar = new Car();
			carr[i]=thecar;
			//input
			System.out.print("Input car's");
			System.out.print("Brand: ");
			brand=s.next();
			System.out.print("Name: ");
			name=s.next();
			System.out.print("MaxSpeed: ");
			maxSpeed=s.nextFloat();
			System.out.print("FuelConsumption: ");
			fuelConsumption=s.nextFloat();
			System.out.print("Price: ");
			price=s.nextFloat();
			System.out.println();
			//set
			carr[i].setBrand(brand);
			carr[i].setName(name);
			carr[i].setMaxSpeed(maxSpeed);
			carr[i].setfuelConsumption(fuelConsumption);
			carr[i].setPrice(price);
		}
		System.out.print("Output car status.");
		//output
		for(int i=0;i<number;i++){
			carr[i].startTurbo();
			System.out.print("\nBrand: "+carr[i].getBrand());
			System.out.print(".\nName: "+carr[i].getName());
			System.out.print(".\nMaxSpeed: "+carr[i].getMaxSpeed());
			System.out.print(".\nFuelConsumption: "+carr[i].getFuelConsumption());
			System.out.print(".\nPrice: "+carr[i].getPrice());
			System.out.print(".\nStartTurbo!");
			System.out.print(".\nTurbo status is "+carr[i].isTurbo());
			System.out.println(".\nCurrent speed is "+carr[i].currentSpeed(carr[i].isTurbo())+".\n");
		}
		//close 
		s.close();
		
	}
}
