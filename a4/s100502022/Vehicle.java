package ce1002.a4.s100502022;

public class Vehicle {
	//initial class member;
	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;	
	private float price;	
	//set
	public void setBrand(String brand) {
		this.brand=brand;
	}
	public void setName(String name) {
		this.name=name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption=fuelConsumption;
	}
	public void setPrice(float price) {
		this.price=price;
	}
	//get	
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	//return currentSpeed
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo==true){
			return maxSpeed;
		}
		else{
			return  maxSpeed*(float)Math.random();
		}
	}
}
