package ce1002.a4.s102502525;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	//設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String Brand) {
		 brand=Brand;
	}
	public void setName(String Name) {
		name=Name;
	}
	public void setMaxSpeed(float MaxSpeed) {
		 maxSpeed=MaxSpeed;
	}
	public void setFuelConsumption(float FuelConsumption){
		fuelConsumption=FuelConsumption;
	}
	public void setPrice(float Price){
		price=Price;
	}
	
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {		
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getFuelConsumption(){
		return fuelConsumption;
	}
	public float getPrice(){
		return price;
	}
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {	
		return maxSpeed;
	}
}
