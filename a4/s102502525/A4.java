package ce1002.a4.s102502525;
import java.util.Scanner;
import java.lang.Math;
public class A4 {
	public static void main(String[] args) {
		Car c=new Car();
		Scanner input=new Scanner(System.in);//六個input
		Scanner input1=new Scanner(System.in);
		Scanner input2=new Scanner(System.in);
		Scanner input3=new Scanner(System.in);
		Scanner input4=new Scanner(System.in);
		Scanner input5=new Scanner(System.in);
		int number=0,count=0;//count用來使迴圈輸入次數等於number
		String brand;//設定五個變數
		String name;
		float maxspeed=0;
		float fuel=0;
		float price=0;
		double random=Math.random();//亂數出一個介於0~1的變數
		do
		{
			System.out.println("Please input the number of cars: ");
			number=input.nextInt();
			if(number<0 || number==0)
			System.out.println("Out of range!");
		}while(number<0 || number==0);//設定一個迴圈讓車子數量大於零且為整數
		String Brand[]=new String[number];//設五個陣列用來儲存屬性
		String Name[]=new String[number];
		float Maxspeed[]=new float[number];
		float Fuel[]=new float[number];
		float Price[]=new float[number];
		do{//輸入屬性並存到Car所繼承的vehicle父類別，再存入陣列
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand=input1.nextLine();
			c.setBrand(brand);
			Brand[count]=c.getBrand();
			System.out.print("Name: ");
			name=input2.nextLine();
			c.setName(name);
			Name[count]=c.getName();
			System.out.print("Max speed: ");
			maxspeed=input3.nextFloat();
			c.setMaxSpeed(maxspeed);
			Maxspeed[count]=c.getMaxSpeed();
			System.out.print("Fuel consumption: ");
			fuel=input4.nextFloat();
			c.setFuelConsumption(fuel);
			Fuel[count]=c.getFuelConsumption();
			System.out.print("Price: ");
			price=input5.nextFloat();
			System.out.println();
			c.setPrice(price);
			Price[count]=c.getPrice();
			count++;//count每次加一，直到等於number跳出迴圈
		}while(count!=number);
		for(int i=0;i<number;i++)//印出所有屬性
		{
			System.out.println("Output car status");
			System.out.println("Car brand is "+Brand[i]);
			System.out.println("Car name is "+Name[i]);
			System.out.println("Car max speed is "+Maxspeed[i]);
			System.out.println("Car fuel consumption is "+Fuel[i]);
			System.out.println("Car price is "+Price[i]);
			System.out.println("StartTurbo!");
			c.startTurbo();
			System.out.println("Turbo status is "+c.isTurbo());
			System.out.print("Current speed is ");
			if(c.isTurbo()==true)//如果turbo不是true，則乘以介於0~1的變數
				System.out.println(Maxspeed[i]);
			else
				System.out.println(Maxspeed[i]*random);
			System.out.println();
		}
	}
}
