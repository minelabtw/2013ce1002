package ce1002.a4.s102502506;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		int num;
		float M,F,P;
		String B,N;
		Scanner put =new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		num = put.nextInt();
		Car C[] = new Car[num];                 //設一個Car類別名為C的物件陣列
		for(int i=0; i<num; i++){
			C[i]=new Car(i);                    //每台車給它一個編號從零開始
			System.out.println("Input car's");
			System.out.print("Brand: ");
			B = put.next();
			C[i].setBrand(B);                   //存入第i台車的品牌.款式.最高速.油耗.價格
			System.out.print("Name: ");
			N = put.next();
			C[i].setName(N);
			System.out.print("Max speed: ");
			M = put.nextFloat();
			C[i].setMaxspeed(M);
			System.out.print("Fuel consumption: ");
			F = put.nextFloat();
			C[i].setfuelConsumption(F);
			System.out.print("Price: ");
			P = put.nextFloat();
			C[i].setPrice(P);
			System.out.print('\n');
		}
		System.out.println("Output car status");
		for(int i=0; i<num; i++){
			System.out.println("Car brand is " + C[i].getBrand() + ".");                       //呼叫函式來return
			System.out.println("Car name is " + C[i].getName() + ".");
			System.out.println("Car max speed is " + C[i].getmaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + C[i].getfuelConsumption() + ".");  
			System.out.println("Car price is " + C[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			C[i].startTurbo();
			System.out.println("Turbo status is " + C[i].isTurbo() + ".");
			System.out.println("Current speed is " + C[i].currentSpeed(C[i].isTurbo()) + ".");  //把isTurbo return出來的值丟入函式currentSpeed然後再return出計算出來的值
			System.out.print('\n');
		}
		put.close();
	}

}
