package ce1002.a4.s102502506;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	public void setBrand(String brand){
		this.brand = brand;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setMaxspeed(float maxspeed){
		this.maxSpeed = maxspeed;
	}
	public String getBrand(){
		return brand;
	}
	public String getName(){
		return name;
	}
	public float getmaxSpeed(){
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo){     
		if(isTurbo==true){
			return maxSpeed;
		}
		else{
			return maxSpeed*(float)Math.random();
		}
	}
	public void setfuelConsumption(float setfuelConsumption){
		this.fuelConsumption=setfuelConsumption;
	}
	public float getfuelConsumption(){
		return fuelConsumption;
	}
	public void setPrice(float Prise){
		this.price=Prise;
	}
	public float getPrice(){
		return price;
	}
}
