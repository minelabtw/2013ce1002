package ce1002.a4.s101201508;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//款式名稱
	private float max_speed;	//最高速度
	private float fuel_consumption;	//油耗
	private float price;	//價格
	
	public void set_brand(String in_brand)
	{
		brand=in_brand;
	}
	public void set_name(String in_name)
	{
		name=in_name;
	}
	public void set_max_speed(float in_max_speed)
	{
		max_speed=in_max_speed;
	}
	public void set_fuel_consumption(float in_fuel_consumption)
	{
		fuel_consumption=in_fuel_consumption;
	}
	public void set_price(float in_price)
	{
		price=in_price;
	}
	public String get_brand()
	{
		return brand;
	}
	public String get_name()
	{
		return name;
	}
	public float get_max_speed()
	{
		return max_speed;
	}
	public float get_fuel_consumption()
	{
		return fuel_consumption;
	}
	public float get_price()
	{
		return price;
	}
	public float current_speed()
	{
		return 0;
	}
	public float current_speed(boolean isTurbo)
	{
		if (isTurbo==true)
		{
			return max_speed;
		}
		else
		{
			
			return max_speed*(float)Math.random();
		}
	}
}
