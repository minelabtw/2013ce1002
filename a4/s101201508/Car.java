package ce1002.a4.s101201508;

import java.util.Random;

public class Car extends Vehicle{
	private boolean isTurbo;
	public void start_isTurbo()
	{
		Random ran=new Random();
		isTurbo=ran.nextBoolean();
	}
	public boolean get_isTurbo()
	{
		return isTurbo;
	}
}
