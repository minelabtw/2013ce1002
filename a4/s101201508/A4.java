package ce1002.a4.s101201508;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n;//choose the n
		Scanner sc=new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		n=sc.nextInt();
		while (n<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=sc.nextInt();
		}
		Car cars[]=new Car [n];//new car array
		for (int i=0;i<n;i++)//input the cars' data and input the car into car array 
		{
			Car car=new Car();
			String brand;
			String name;
			float max_speed;
			float fuel_consumption;
			float price;
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand=sc.next();
			car.set_brand(brand);
			System.out.print("Name: ");
			name=sc.next();
			car.set_name(name);
			System.out.print("Max speed: ");
			max_speed=sc.nextInt();
			car.set_max_speed(max_speed);
			System.out.print("Fuel consumption: ");
			fuel_consumption=sc.nextInt();
			car.set_fuel_consumption(fuel_consumption);
			System.out.print("Price: ");
			price=sc.nextInt();
			car.set_price(price);
			System.out.println("");
			cars[i]=car;
		}
		System.out.println("Output car status");
		for (int i=0;i<n;i++)//output the data
		{
			System.out.println("Car brand is "+cars[i].get_brand()+".");
			System.out.println("Car name is "+cars[i].get_name()+".");
			System.out.println("Car max speed is "+cars[i].get_max_speed()+".");
			System.out.println("Car fuel consumption is "+cars[i].get_fuel_consumption()+".");
			System.out.println("Car price is "+cars[i].get_price()+".");
			cars[i].start_isTurbo();
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is  "+cars[i].get_isTurbo()+".");
			System.out.println("Car brand is "+cars[i].current_speed(cars[i].get_isTurbo())+".");
			System.out.println("");
		}
		
		
		
	}

}
