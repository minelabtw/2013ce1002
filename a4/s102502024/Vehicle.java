package ce1002.a4.s102502024;

public class Vehicle {
	private String brand;	
	private String name;	
	private float maxSpeed;	
	private float fuelConsumption;
	private float price;
	
	public void setBrand(String bran)  //設定屬性
	{
		brand=bran;
	}
	public void setName(String nam)  //設定屬性
	{
		name=nam;
	}
	public void setMaxSpeed(float maxSpee)  //設定屬性
	{
		maxSpeed=maxSpee;
	}
	public void setFuelConsumption(float fuelConsumptio)  //設定屬性
	{
		fuelConsumption=fuelConsumptio;
	}
	public void setPrice(float pric)  //設定屬性
	{
		price=pric;
	}
	public String getBrand()  //回傳屬性
	{
		return brand;
	}
	public String getName()  //回傳屬性
	{
		return name;
	}
	public float getMaxSpeed()  //回傳屬性
	{
		return maxSpeed;
	}
	public float getFuelConsumption()  //回傳屬性
	{
		return fuelConsumption;
	}
	public float getPrice()  //回傳屬性
	{
		return price;
	}
	public float currentSpeed(boolean isTurbo)  //回傳現在速度
	{
		float b=(float)(Math.random()*1);
		if(isTurbo==true)
		{
			return maxSpeed;
		}
		else
		{
			return maxSpeed*b;
		}
	}
}
