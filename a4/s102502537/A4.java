package ce1002.a4.s102502537;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// scanner input
		System.out.println("Please input the number of cars: ");
		int nub = input.nextInt();// input next

		while (nub <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			nub = input.nextInt();// input next
		}
		Car[] cararr = new Car[nub];// Car class cararr object array
		for (int i = 0; i < nub; i++) {
			cararr[i] = new Car();// construct
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cararr[i].setBrand(input.next());// input next
			System.out.print("Name: ");
			cararr[i].setName(input.next());// input next
			System.out.print("Max speed: ");
			cararr[i].setMaxSpeed(input.nextFloat());// input next
			System.out.print("Fuel consumption: ");
			cararr[i].setFuelConsumption(input.nextFloat());// input next
			System.out.print("Price: ");
			cararr[i].setPrice(input.nextFloat());// input next
			System.out.println();
		}
		System.out.println("Output car status");
		for (int i = 0; i < nub; i++) {// 依題目要求輸出
			System.out.println("Car brand is " + cararr[i].getBrand() + ".");
			System.out.println("Car name is " + cararr[i].getName() + ".");
			System.out.println("Car max speed is " + cararr[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cararr[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cararr[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			cararr[i].startTurbo();
			System.out.println("Turbo status is " + cararr[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cararr[i].currentSpeed(cararr[i].isTurbo()) + ".");
			System.out.println();
		}
	}
}
