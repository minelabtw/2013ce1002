package ce1002.a4.s102502537;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo;

	public Car() {
		super();
	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		Random ran = new Random();// 隨機
		isTurbo = ran.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
