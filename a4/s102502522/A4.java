package ce1002.a4.s102502522;
import java.util.Scanner;
public class A4 {
	public static void main(String[] args) {
		int num;
		String brand;	//品牌
		String name;	//款式名稱
		float maxSpeed;	//最高速度
		float fuelConsumption;	//油耗
		float price;	//價格
		Scanner input = new Scanner( System.in );
		System.out.println("Please input the number of cars: ");
		num=input.nextInt();
		while(num<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num=input.nextInt();	
		}
		Car car[]=new Car[num];//宣告矩陣
		
		for(int count=1;count<=num;count++)//輸入資料
		{
			
		System.out.println("Input Car's");
		System.out.println("Brand");
		brand=input.next();
		System.out.println("name");
		name=input.next();
		System.out.println("Maxspeed");
		maxSpeed=input.nextFloat();
		System.out.println("fuelConsumption");
		fuelConsumption=input.nextFloat();
		System.out.println("price");
		price=input.nextFloat();
		car[count-1]=new Car();//存入第一台車中
		car[count-1].setbrand(brand);
		car[count-1].setname(name);
		car[count-1].setmaxspeed(maxSpeed);
		car[count-1].setfuelConsumption(fuelConsumption);
		car[count-1].setprice(price);
		}
		 input.close();
		 System.out.println("Output car status");
		for(int count=1;count<=num;count++)//輸出資料
		{
			System.out.println("Car brand is "+car[count-1].getbrand());
			System.out.println("Car name is "+car[count-1].getname());
			System.out.println("Car max speed is "+car[count-1].getmaxspeed()); 
			System.out.println("Car fuelConsumption is "+car[count-1].getfuelConsumption());
			System.out.println("Car price is "+car[count-1].getprice());
			car[count-1].startTurbo();
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+car[count-1].isTurbo());
			System.out.println("Current speed is "+car[count-1].currentSpeed(car[count-1].isTurbo()));
		
		}
		
	}

}
