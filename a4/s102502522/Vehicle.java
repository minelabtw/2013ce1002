package ce1002.a4.s102502522;
import java.util.Random;
public class Vehicle {

	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	Random ran = new Random();
	
	public void setbrand(String brand)
	{
		this.brand=brand;
	}
	public void setname(String name)
	{
		this.name=name;
	}
	public void setmaxspeed(float speed)
	{
		this.maxSpeed=speed;
	}
	public void setfuelConsumption(float fuelConsumption)
	{
		this.fuelConsumption=fuelConsumption;
	}
	public void setprice(float price)
	{
		this.price=price;
	}
	public String getbrand()
	{
		return brand;
	}
	public String getname()
	{
		return name;
	}
	public Float getmaxspeed()
	{
		return maxSpeed;
	}
	public Float getfuelConsumption()
	{
		return fuelConsumption;
	}
	public Float getprice()
	{
		return price;
	}
	/*public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
		
	}*/
	public float currentSpeed(boolean isTurbo) //接收布林值 並計算最大速度
	{	
		if(isTurbo==false)
		{
		float rate;
		float currentspeed;
		rate=ran.nextFloat();
		currentspeed=rate*maxSpeed;
		return currentspeed;
		}
		else
		{
			return maxSpeed; 
		}
		}
}