package ce1002.a4.s102502522;
import java.util.Random;
public class Car extends Vehicle {
	Random ran = new Random();//隨機
	private boolean isTurbo;
	public Car()//建構子
	{
		
	}
	public void startTurbo() 	// 測試啟動渦輪，可能會成功或失敗。
	{
		int random;
		random=ran.nextInt(2);
		if(random==0)
		{
			isTurbo=false;
		}
		else
		{
			isTurbo=true;
		}
	}
	public boolean isTurbo() 	// 取得渦輪目前狀態	
	{		
		super.currentSpeed(isTurbo);
		return isTurbo;	
	}
}
