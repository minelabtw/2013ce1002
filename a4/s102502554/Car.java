package ce1002.a4.s102502554;

public class Car extends Vehicle{
	
	private int id ;// 賽車id
	private float maxSpeed ;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	
    public Car(){
		
	}
	
	public Car(int carname){
		id = carname;
	}//建構子，其傳入的值為id
	
	int getid (){
		return id;
	}//輸出id
	
	void setspeed (float i){
	    maxSpeed = i;
	}//設定速度
	
	float getspeed(){
		return maxSpeed;
	}//輸出速度
	
	boolean getisTurbo () 
	{
		return isTurbo;
	}//輸出是否有渦輪
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		  int a = (int)(Math.random()*2);
		  if (a == 0){
			  isTurbo = false;
		  }
		  if (a == 1){
			  isTurbo = true;
		  }
	}
	
		public boolean isTurbo() {	// 取得渦輪目前狀態	
		  return isTurbo;
	}

}
