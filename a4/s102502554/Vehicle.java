package ce1002.a4.s102502554;

import java.util.Random;

public class Vehicle {
	Random k = new Random();
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public Vehicle(){
		
	}
	
	public void setBrand (String Brand){
		brand = Brand;
	}
	
	public void setName (String Name){
		name = Name;
	}
	
	public void setMaxSpeed (float i){
		maxSpeed = i;
	}
	
	public void setfuelConsumption (float i){
		fuelConsumption = i;
	}
	
	public void setprice (float i){
		price = i;
	}
	
	public String getBrand (){
		return brand;
	}
	
	public String getName (){
		return name;
	}
	
	public float getMaxSpeed (){
		return maxSpeed;
	}
	
	public float getfuelConsumption (){
		return fuelConsumption;
	}
	
	public float getprice (){
		return price;
	}
	
	public float currentSpeed() {	//給之後有關機車的class使用，A4不會使用到。
	    return maxSpeed;
	}
	
	public float currentSpeed ( boolean isTurbo) {
		if (isTurbo == true){
			return maxSpeed;
		}
		else{
			return maxSpeed*k.nextFloat();
		}
		
	}//return current speed
	
}
