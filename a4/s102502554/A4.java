package ce1002.a4.s102502554;

import	java.util.Scanner;

public class A4 {
	public static void main (String [] args){
		int cars;
		System.out.println("Please input the number of cars: ");
		Scanner input = new Scanner(System.in);
		cars = input.nextInt();
		while (cars <= 0){
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			cars = input.nextInt();
		}//enter how many cars
		Car [] car = new Car [cars];//�ŧiCar�}�C
		
		for (int i = 0 ; i < cars ; i++){
			Car newcar = new Car (i);
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			String Brand = input.next();
			newcar.setBrand(Brand);
			
			System.out.print("Name:");
			String Name = input.next();
			newcar.setName(Name);
			
			System.out.print("Max speed: ");
			float Mspeed = input.nextFloat();
			while (Mspeed <= 0){
				System.out.println("Out of range!");
				System.out.print("Max speed: ");
				Mspeed = input.nextInt();
			}
			newcar.setMaxSpeed(Mspeed);
			
			System.out.print("Fuel consumption: ");
			float fuelConsumption = input.nextFloat();
			while (fuelConsumption <= 0){
				System.out.println("Out of range!");
				System.out.print("Fuel consumption: ");
				fuelConsumption = input.nextInt();
			}
			newcar.setfuelConsumption(fuelConsumption);
			
			System.out.print("Price: ");
			float price = input.nextFloat();
			while (price <= 0){
				System.out.println("Out of range!");
				System.out.print("Price: ");
				price = input.nextInt();
			}
			newcar.setprice(price);
			
			System.out.print("\n");
			
			car[i] = newcar;
		}//input the data of cars
		
		System.out.println("Output car status");
		for (int i = 0 ; i < cars ; i++){
		System.out.println("Car brand is " + car[i].getBrand());
		System.out.println("Car name is "+ car[i].getName());
		System.out.println("Car max speed is "+car[i].getMaxSpeed());
		System.out.println("Car fuel consumption is "+car[i].getfuelConsumption());
		System.out.println("Car price is "+car[i].getprice());
		System.out.println("StartTurbo!");
		System.out.println("Turbo status is "+car[i].getisTurbo());
		System.out.println("Current speed is "+car[i].currentSpeed());
		System.out.print("\n");
		}//output the data of cars
		
		input.close(); //����Scanner
	}

}
