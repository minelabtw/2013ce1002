package ce1002.a4.s102502521;
import java.util.*;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void setFuelconsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	
	public void setPrice(float price){
		this.price=price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	public float getFuelconsumption(){
		return fuelConsumption;
	}
	
	public float getPrice(){
		return price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() { 
		return maxSpeed;
	}

	public float currentSpeed(boolean isTurbo) {
		if(isTurbo==false) {
			Random random=new Random();
			return maxSpeed*random.nextFloat();
		}
		return maxSpeed;
	}
}
