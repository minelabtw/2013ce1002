package ce1002.a4.s102502521;

import java.util.*;

public class A4 {
	public static Scanner scanner;
	//public static Car car[];
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num=0;
		scanner=new Scanner(System.in);
		
		//判斷車數
		do{
			System.out.println("Please input the number of cars: ");
			num=scanner.nextInt();
			
			if(num<=0){
				System.out.println("Out of range!");
			}
		}while(num<=0);
		
		
		//建立物件陣列
		Car car[]=new Car[num];
		
		for(int x=0;x<num;x++){
			car[x]=new Car(x);
		}
		
		//輸入資料
		for(int x=0;x<num;x++){
			
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			car[x].setBrand(scanner.next());
			
			System.out.print("Name: ");
			car[x].setName(scanner.next());
			
			System.out.print("Max speed: ");
			car[x].setMaxSpeed(scanner.nextFloat());
			
			System.out.print("Fuel consumption: ");
			car[x].setFuelconsumption(scanner.nextFloat());
			
			System.out.print("Price: ");
			car[x].setPrice(scanner.nextFloat());
			
			car[x].startTurbo();
			
			System.out.println("");
		}
		
		System.out.println("Output car status");
		
		//列印
		for(int x=0;x<num;x++){
			System.out.println("Car brand is "+car[x].getBrand()+".");
			System.out.println("Car name is "+car[x].getName()+".");
			System.out.println("Car max speed is "+car[x].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+car[x].getFuelconsumption()+".");
			System.out.println("Car price is "+car[x].getPrice()+".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+car[x].isTurbo()+".");
			System.out.println("Current speed is "+car[x].currentSpeed(car[x].isTurbo())+".");
			System.out.println("");
		}
	}
}
