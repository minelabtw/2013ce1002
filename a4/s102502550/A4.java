package ce1002.a4.s102502550;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);                                  //輸入物件宣告
		int a=0;
		
		
		do{                                                                   //檢查輸入
			System.out.println("Please input the number of cars: ");
		    a = in.nextInt();
		    
		    if(a<=0)
		    	System.out.println("Out of range!");
		}while(a<=0);
		
		Car[] cars = new Car[a];
		
		for(int i=0;i<a;i++){	                                              //輸入車的資訊
			
			cars[i] = new Car(); 
			
			System.out.println("Input car's");
			
			System.out.println("Brand: ");		
			
			cars[i].setBrand(in.next());
			
			System.out.println("Name: ");
			cars[i].setName(in.next());
			
			System.out.println("Max speed: ");
			cars[i].setMaxSpeed(in.nextFloat());
			
			System.out.println("Fuel consumption: ");
			cars[i].setFuelConsumption(in.nextFloat());
			
			System.out.println("Price: ");
			cars[i].setPrice(in.nextFloat());
			
			System.out.println("");
			
		}
		System.out.println("Output car status");
		
		for(int i=0;i<a;i++){                                                 //輸出結果	                                                                                        //
			System.out.println("Brand: "+cars[i].getBrand()+".");
			System.out.println("Name: "+cars[i].getName()+".");
			System.out.println("MaxSpeed: "+cars[i].getMaxSpeed()+".");
			System.out.println("Fuel consumption: "+cars[i].getFuelConsumption()+".");
			System.out.println("Price: "+cars[i].getPrice()+".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is "+cars[i].isTurbo()+".");
			System.out.println("Current speed is "+cars[i].currentSpeed( cars[i].isTurbo() )+".");
			System.out.println("");
		}
		in.close();
		

	}

}
