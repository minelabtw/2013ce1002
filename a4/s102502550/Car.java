package ce1002.a4.s102502550;



public class Car extends Vehicle {

	private boolean isTurbo;	// 渦輪狀態
	
	public Car(){}
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		
		isTurbo = ( Math.random() >= 0.5 )? true:false;
	}
	
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		return isTurbo;
	}

}
