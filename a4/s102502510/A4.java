package ce1002.a4.s102502510;

import java.util.Scanner;

public class A4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);//create a new Scanner
		Car[] cars;
		int numofcars;
		do {
			System.out.println("Please input the number of cars: ");
			numofcars = input.nextInt();
			if (numofcars <=0) {
				System.out.println("Out of range!");
			}
		} while (numofcars <=0);
		cars=new Car[numofcars];
		for(int i=0;i<numofcars;++i)//input car
		{
			cars[i]=new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			cars[i].setBrand(input.next());
			System.out.print("Name: ");
			cars[i].setName(input.next());
			System.out.print("Max speed: ");
			cars[i].setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			cars[i].setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			cars[i].setprice(input.nextFloat());
		}
		System.out.println("Output car status");
		for(int i=0;i<numofcars;++i)//output car
		{
			System.out.println("Car brand is "+cars[i].getBrand());
			System.out.println("Car name is "+cars[i].getName());
			System.out.println("Car max speed is "+cars[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "+cars[i].getfuel());
			System.out.println("Car price is "+cars[i].getprice());
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is "+cars[i].isTurbo());
			System.out.println("Current speed is "+cars[i].currentSpeed(cars[i].isTurbo()));
		}
	}
}
