package ce1002.a4.s102502510;
import java.util.Random;
public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setFuelConsumption(float fuel)
	{
		this.fuelConsumption=fuel;
	}
	public void setprice(float price)
	{
		this.price=price;
	}

	// 取得品牌、名稱、最高速度、油耗、價格
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo==true)
		{
			return maxSpeed;
		}
		else
		{
			Random ran=new Random();
			return maxSpeed*ran.nextFloat();
		}
	}
	public float getfuel()
	{
		return fuelConsumption;
	}
	public float getprice()
	{
		return price;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
