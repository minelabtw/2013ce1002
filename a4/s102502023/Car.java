package ce1002.a4.s102502023;

import java.util.Random;

public class Car extends Vehicle {
	private boolean isTurbo; // initialize boolean isTurbo
	
	public Car() {
	}
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		Random random = new Random();
		  this.isTurbo = random.nextBoolean();
	}
	public boolean isTurbo() {	// 取得渦輪目前狀態	
	  return isTurbo;
	}

}
