package ce1002.a4.s102502023;

import java.util.Scanner;

public class A4 {
  public static void main(String[] args) {
	  int num = 0; // initialize num to 0
	  Car[] cars; // initialize Car array cars
	  Scanner input = new Scanner(System.in); // initialize Scanner input
	  do {
	  System.out.print("Please input the number of cars:\n");
	  num = input.nextInt();
	  if (num <= 0)
		  System.out.println("Out of range!");
	  }while (num <= 0); // do... while loop to check whether num is on demand
	  
	  cars = new Car[num]; // initialize array cars' elements to 0
	  for (int i = 0; i < cars.length; i++) {
		  Car car = new Car();
		  System.out.println("Input car's");
		  System.out.print("Brand: ");
		  car.setBrand(input.next());
		  System.out.print("Name: ");
		  car.setName(input.next());
		  System.out.print("Max speed: ");
		  car.setMaxSpeed(input.nextFloat());
		  System.out.print("Fuel consumption: ");
		  car.setFuelConsumption(input.nextFloat());
		  System.out.print("Price: ");
		  car.setPrice(input.nextFloat());
		  car.startTurbo();		  
		  cars[i] = car;
		  System.out.print("\n");
	  }// for loop to prompt user to input
	  
	  // output results
	  System.out.println("Output car status\n");
	  for (int i = 0; i < cars.length; i++) {
		  
		  System.out.println("Car brand is " + cars[i].getBrand());
		  System.out.println("Car name is " + cars[i].getName());
		  System.out.println("Car max speed is " + cars[i].getMaxSpeed());
		  System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption());
		  System.out.println("Car price is " + cars[i].getPrice());
		  System.out.println("StartTurbo!");
		  System.out.println("Turbo status is " + cars[i].isTurbo());
		  System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()) + "\n");
		  
	  }
	  
	  input.close();
  }
}
