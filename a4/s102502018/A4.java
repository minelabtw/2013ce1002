package ce1002.a4.s102502018;
import java.util.Scanner;
public class A4 {

	public static void main(String[] args) {
		int number;
		Scanner scan =new Scanner(System.in);
		do
		{
			System.out.println("Please input the number of cars: ");
			number = scan.nextInt();
			if(number<=0)
			{
				System.out.println("Out of range!");
			}
		}while(number<=0);
		Car[] cars;        // class array
		cars = new Car[number];
		String brand;
		String name;
		float maxSpeed;
		float fuelConsumption;
		float price;		
		for(int i=0;i<cars.length;i++)
		{
			Car car =new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand=scan.next();			
			System.out.print("Name: ");
			name=scan.next();
			System.out.print("Max speed: ");
			maxSpeed=scan.nextFloat();
			System.out.print("Fuel consumption: ");
			fuelConsumption=scan.nextFloat();
			System.out.print("Price: ");
			price=scan.nextFloat();
			System.out.println();
			car.setBrand(brand);
			car.setName(name);
			car.setMaxSpeed(maxSpeed);
			car.setfuelConsumption(fuelConsumption);
			car.setprice(price);
			cars[i]=car;        //將所有屬性存進class array

		}
		scan.close();
		System.out.println("Output car status");
		for(int i=0;i<cars.length;i++)
		{	
			Car car =new Car();
			System.out.println("Car brand is "+cars[i].getBrand());
			System.out.println("Car name is "+cars[i].getName());
			System.out.println("Car max speed is "+cars[i].getMaxSpeed());
			System.out.println("Car fuel consumption is "+cars[i].getfuelConsumption());
			System.out.println("Car price is "+cars[i].getprice());
			System.out.println("StartTurbo!");
			car.startTurbo();
			System.out.println("Turbo status is "+car.isTurbo()+".");
			System.out.println("Current speed is "+cars[i].currentSpeed(car.isTurbo())+".");
			System.out.println();
		}
	}

}
