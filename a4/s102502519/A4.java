.package ce1002.a4.s102502519;

import java.util.*;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		int num1=0;
		do
		{
			System.out.println("Please input the number of cars: ");
			num1 = input.nextInt();
			if(num1<=0)
				System.out.println("Out of range!");
		}while(num1<=0);    //使num1大於0    
		
		Car car[] = new Car[num1];    //宣告car陣列當作Car裡的建構子
		for(int i=0;i<num1;i++){    
			car[i] = new Car(i);
		}
		for(int j=0;j<num1;j++){
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String str1 = input.next();
			System.out.print("Name: ");
			String str2 = input.next();
			System.out.print("Max speed: ");
			float f1 = input.nextFloat();
			System.out.print("Fuel consumption: ");
			float f2 = input.nextFloat();
			System.out.print("Price: ");
			float f3 = input.nextFloat();
			System.out.println();
			
			car[j].setBrand(str1);
			car[j].setName(str2) ;
			car[j].setMaxSpeed(f1);
			car[j].setFuelConsumption(f2);
			car[j].setPrice(f3);
			
		}
		input.close();
		
		for(int k=0;k<num1;k++){
			System.out.println("Output car status");
			System.out.println("Car brand is " + car[k].getBrand());
			System.out.println("Car name is " + car[k].getName());
			System.out.println("Car max speed is " + car[k].getMaxSpeed());
			System.out.println("Car fuel consumption is " + car[k].getFuelConsumption());
			System.out.println("Car price is " + car[k].getPrice());
			System.out.println("StartTurbo!");
			car[k].startTurbo();
			System.out.println("Turbo status is " + car[k].isTurbo( ));
			System.out.println("Current speed is " + car[k].currentSpeed(car[k].isTurbo()));
			System.out.println();
		}
		
		
		
	}

}
