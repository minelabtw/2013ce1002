package ce1002.a4.s102502020;

import java.util.Random;

public class Car extends Vehicle{

	private boolean isTurbo;
	
	public Car(){
		
	}
	public void startTurbo() {	         // 測試啟動渦輪，可能會成功或失敗
		Random ran = new Random();
		float i = ran.nextInt(2);
		if(i == 1){
			isTurbo = true;
		}
		else{
			isTurbo = false;
		}
	}
	public boolean isTurbo() {	         // 取得渦輪目前狀態
		return isTurbo;
	}
	
	


}
