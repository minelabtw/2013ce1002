package ce1002.a4.s102502020;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number = 0;
		Scanner input = new Scanner(System.in);
		do{
			System.out.println("Piease input the number of cars: ");
			number = input.nextInt();                        //輸入車的數量
			if(number <= 0){
				System.out.println("Out of range!");
			}
		}while(number <= 0);
		Car cars[]=new Car[number];                          //宣告陣列
		for(int i = 0; i < number; i++){
			Car car = new Car();                             //呼叫Car Class
			System.out.println("Input car's");
			System.out.print("Brand: ");
			car.setBrand(input.next());                      //輸入品牌
			System.out.print("Name: ");
			car.setName(input.next());                       //輸入款式名稱
			System.out.print("Max speed: ");
			car.setMaxSpeed(input.nextFloat());              //輸入最高速度
			System.out.print("Fuel consumption: ");
			car.setFuelConsumption(input.nextFloat());       //輸入油耗
			System.out.print("Price: ");
			car.setPrice(input.nextFloat());                 //輸入價格
			System.out.println();
			car.startTurbo();                                //測試啟動渦輪
			cars[i] = car;                                   //存入陣列
		}
		input.close();
		System.out.println("Output car status");
		for(int i = 0; i < number; i++){
			System.out.println("Car brand is " + cars[i].getBrand() + ".");                        //輸出輸出車的屬性
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getPrice() + ".");
			System.out.println("StartTurbo!");                                                     //啟動渦輪後
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");  
			System.out.println("Current speed is " + cars[i].currentSpeed(cars[i].isTurbo()));     //現在速度
			System.out.println();
		}
	}

}
