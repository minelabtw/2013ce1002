package ce1002.a4.s102502020;

public class Vehicle {

	private String brand;	          //品牌
	private String name;	          //款式名稱
	private float maxSpeed;	          //最高速度
	private float fuelConsumption;	  //油耗
	private float price;	          //價格
	
	public Vehicle(){
		
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float currentSpeed() {                       //回傳現在速度
		return maxSpeed;
	}
	public float currentSpeed(boolean isTurbo) {        //依據渦輪是否啟動，來決定現在速度
			float ran = (float)(Math.random()*1);
			if(isTurbo == false)
			{
				maxSpeed = maxSpeed * ran;
			}
			return currentSpeed();
	}
}
