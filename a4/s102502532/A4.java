package ce1002.a4.s102502532;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Please input the number of cars:");
		int carnumber = input.nextInt();
		while (carnumber <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			carnumber = input.nextInt();
		}

		Car[] cars = new Car[carnumber]; // 物件陣列

		for (int i = 0; i < carnumber; i++) {
			cars[i] = new Car(); // 要初始化
			System.out.println("Input car's");

			System.out.print("Brand: ");
			String Brandi = input.next(); // 讀取空白前字串
			cars[i].setBrand(Brandi);
			System.out.print("Name: ");
			String Namei = input.next(); // 不用\n
			cars[i].setName(Namei);
			System.out.print("Max speed: ");
			float speedi = input.nextFloat();
			cars[i].setMaxSpeed(speedi);
			System.out.print("Fuel consumption: ");
			float Fueli = input.nextFloat();
			cars[i].setfuelConsumption(Fueli);
			System.out.print("Price: ");
			float Pricei = input.nextFloat();
			cars[i].setprice(Pricei);

			System.out.println();
		}

		System.out.println("Output car status"); // 印出
		for (int i = 0; i < carnumber; i++) {
			System.out.println("Car brand is " + cars[i].getBrand() + ".");
			System.out.println("Car name is " + cars[i].getName() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cars[i].getfuelConsumption() + ".");
			System.out.println("Car price is " + cars[i].getprice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[i].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cars[i].currentSpeed(cars[i].isTurbo()) + "."); // 傳入isTurbo

			System.out.println();
		}

		input.close();
	}

}
