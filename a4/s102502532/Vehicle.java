package ce1002.a4.s102502532;

public class Vehicle {

	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed = 0; // 最高速度
	private float fuelConsumption = 0; // 油耗
	private float price = 0; // 價格

	public Vehicle() {

	}

	// 設定品牌、名稱、最高速度、油耗、價格
	public void setBrand(String brand) {
		this.brand = brand; // 同名 用this.
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void setfuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public void setprice(float price) {
		this.price = price;
	}

	// 取得品牌、名稱 ......
	public String getBrand() {
		return brand;
	}

	public String getName() {
		return name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public float getfuelConsumption() {
		return fuelConsumption;
	}

	public float getprice() {
		return price;
	}

	// 取得目前速度
	public float currentSpeed() { // 給之後有關機車的class使用，A4不會使用到。
		float n2 = (int) (1) / 100000000;
		float n1 = (float) (Math.random());
		return maxSpeed * (n1 + n2); // (0 <= random值 <= 1)
	}

	public float currentSpeed(boolean isTurbo) {
		if (isTurbo == true) {
			return maxSpeed;
		} else {
			float n2 = (int) (1) / 100000000;
			float n1 = (float) (Math.random());
			return maxSpeed * (n1 + n2);
		}
	}
}
