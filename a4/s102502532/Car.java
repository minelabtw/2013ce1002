package ce1002.a4.s102502532;

import java.util.Random; //使用Random類別

public class Car extends Vehicle {

	private boolean isTurbo; // 渦輪狀態
	Random random1 = new Random(); // 建立物件 目前時間為種子

	Car() {

	}

	public void startTurbo() { // 測試啟動渦輪 可能會成功或失敗。
		isTurbo = random1.nextBoolean();
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
