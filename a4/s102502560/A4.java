package ce1002.a4.s102502560;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		
		Scanner STDIN=new Scanner(System.in);
		
		int n;
		while(true){
			System.out.println("Please input the number of cars: ");
			n=STDIN.nextInt();
			if(n>0)break;
			System.out.println("Out of range!");
		}
		
		Car[] cars=new Car[n];
		
		for(int i=0;i<n;i++){
			Car mycar=new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			mycar.setBrand(STDIN.next());
			System.out.print("Name: ");
			mycar.setName(STDIN.next());
			System.out.print("Max speed: ");
			mycar.setMaxSpeed(STDIN.nextFloat());
			System.out.print("Fuel consumption: ");
			mycar.setFuelConsumption(STDIN.nextFloat());
			System.out.print("Price: ");
			mycar.setPrice(STDIN.nextFloat());
			cars[i]=mycar;
			System.out.println(" ");
		}
		
		STDIN.close();
		
		System.out.println("Output car status");
		for(int i=0;i<n;i++){
			System.out.println("Car brand is "+cars[i].getBrand()+".");
			System.out.println("Car name is "+cars[i].getName()+".");
			System.out.println("Car max speed is "+cars[i].getMaxSpeed()+".");
			System.out.println("Car fuel consumption is "+cars[i].getFuelConsumption()+".");
			System.out.println("Car price is "+cars[i].getPrice()+".");
			System.out.println("StartTurbo!");
			cars[i].startTurbo();
			System.out.println("Turbo status is "+cars[i].isTurbo()+".");
			System.out.println("Current speed is "+cars[i].currentSpeed(cars[i].isTurbo())+".");
			System.out.println(" ");
		}

	}

}
