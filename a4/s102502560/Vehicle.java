package ce1002.a4.s102502560;

public class Vehicle {
	private String brand;	//�i�v
	private String name;	//�������i
	private float maxSpeed;	//�ō����x
	private float fuelConsumption;	//����
	private float price;	//�J�i
	
	//setters of: brand name maxSpeed fuelConsumption price
	public void setBrand(String brand){
		this.brand=brand;
	}
	
	public void setName(String name) {
		this.name=name;
	}
	
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed=maxSpeed;
	}
	
	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption=fuelConsumption;
	}
	
	public void setPrice(float price) {
		this.price=price;
	}	
	 
	//getters of: brand name maxSpeed fuelConsumption price
	public String getBrand(){
		return brand;
	}
	
	public String getName() {
		return name;
	}
	
	public float getMaxSpeed() {
		return maxSpeed;
	}
	
	public float getFuelConsumption() {
		return fuelConsumption;
	}
	
	public float getPrice() {
		return price;
	}
	
	//
	public float currentSpeed() {
		return (float) (maxSpeed*Math.random());
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo){return maxSpeed;}
		else{return (float) (maxSpeed*Math.random()); }
	}
}
