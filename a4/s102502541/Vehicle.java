package ce1002.a4.s102502541;

import java.util.Random;

public class Vehicle {
	Random cspeed = new Random();
	private String brand;	//品牌
	private String name;	//款式名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public Vehicle(){
		
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setprice(float price){
		this.price = price;
	}
	public void setfuelConsumption(float fuelConsumption){
		this.fuelConsumption = fuelConsumption;
	}
	 
	//取得品牌、名稱、最高速度、油耗、價格		
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}	
	//取得目前速度(多載：汽車有渦流，但機車沒有。)
	public float currentSpeed() {	
		return maxSpeed;
	}
	public float getprice(){
		return price;
	}
	public float getfuelConsumption(){
		return fuelConsumption;
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo == true)
			return maxSpeed;
		else
			return maxSpeed*cspeed.nextFloat();//當沒有開啟isTurbo speed = maxspeed*(0-1之間的小數)
	}

}
