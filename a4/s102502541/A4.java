package ce1002.a4.s102502541;
import java.util.Scanner;
public class A4 {
	
	public static void main(String[] args) {
		int carnum;
		String brand;	//品牌
		String name;	//款式名稱
		float maxSpeed;	//最高速度
		float fuelConsumption;	//油耗
		float price;	//價格
		Scanner scanner = new Scanner(System.in);
		do{
			System.out.println("Please input the number of cars: ");
			carnum = scanner.nextInt();
			
		}while(carnum<1);
		Car car[] = new Car[carnum];
		for(int i=0; i<carnum; i++)
		{
			car[i] = new Car();//宣告多個物件
			System.out.println("Input car's");
			System.out.print("Brand: ");
			brand = scanner.next();
			car[i].setBrand(brand);
			System.out.print("Name: ");
			name = scanner.next();
			car[i].setName(name);
			System.out.print("Max speed: ");
			maxSpeed = scanner.nextFloat();
			car[i].setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ");
			fuelConsumption = scanner.nextFloat();
			car[i].setfuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			price = scanner.nextFloat();
			car[i].setprice(price);
			if(i!=carnum-1)
			System.out.println(" ");
		}
		System.out.println(" ");
		System.out.println("Output car status");
		for(int i=0;i<carnum; i++)
		{
			System.out.println("Car brand is "+car[i].getBrand());
			System.out.println("Car name is "+ car[i].getName());
			System.out.println("Car max speed is "+ car[i].getMaxSpeed());
			System.out.println("Car fuel consumption "+ car[i].getfuelConsumption());
			System.out.println("Car price is "+ car[i].getprice());
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is "+ car[i].isTurbo());
			System.out.println("Current speed is "+ car[i].currentSpeed(car[i].isTurbo()));
			System.out.println(" ");
			
		}

	}

}
