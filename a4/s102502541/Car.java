package ce1002.a4.s102502541;

import java.util.Random;

public class Car extends Vehicle{
	Random test = new Random();
	private boolean isTurbo;
	
	public Car(){
		
	}
	public void startTurbo() {
		int temp;
		temp = test.nextInt(2);//將0or1隨機給temp
		if(temp == 1)
			isTurbo = true;
		else
			isTurbo = false;
	}
	public boolean isTurbo() {
		return isTurbo;
		
	}

}
