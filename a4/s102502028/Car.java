package ce1002.a4.s102502028;

public class Car extends Vehicle{
	private boolean isTurbo;	// 渦輪狀態
	
	public void startTurbo() {	// 測試啟動渦輪，可能會成功或失敗。
		int a = (int)(Math.random()*10)%2 ;
		if (a == 0)
		isTurbo = false ;
		else
			isTurbo = true ;
    }
  
	public boolean isTurbo() {	// 取得渦輪目前狀態	
		  return isTurbo ;
	}


}
