package ce1002.a4.s102502028;
import java.util.Scanner ;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number = 0 ;   //宣告
		String brand;	//品牌
		String name;	//款式名稱
		float maxSpeed;	//最高速度
		float fuelConsumption;	//油耗
		float price;	//價格
		
		System.out.println("Please input the number of cars: ") ;  //輸入車數
		Scanner cin = new Scanner(System.in) ;
		number = cin.nextInt();
		while (number <= 0)   //判斷車數是否合理
		{
			System.out.println("Out of range!") ;
			System.out.println("Please input the number of cars: ") ;
			number = cin.nextInt();
		}
				
		Car[] array = new Car[number] ; 
		for (int a = 0 ; a < number ; a++)   //輸入車子屬性
		{
			Car car = new Car() ;
			System.out.println("Input car's") ;  
			System.out.print("Brand: ") ;  //品牌
			brand = cin.next();
			car.setBrand(brand);
			System.out.print("Name: ") ;   //名字
			name = cin.next();
			car.setName(name);
			System.out.print("Max speed: ") ;  //最大速度
			maxSpeed = cin.nextFloat();
			car.setMaxSpeed(maxSpeed);
			System.out.print("Fuel consumption: ") ;  //油耗
			fuelConsumption = cin.nextFloat() ;
			car.setFuelConsumption(fuelConsumption);
			System.out.print("Price: ") ;  //價錢
			price = cin.nextFloat() ;
			car.setPrice(price);
			
			System.out.println();
			
			array[a] = car ;			
		}
		
		System.out.println("Output car status");
		
		for (int b = 0 ; b < number ; b++)  //輸出結果
		{
			System.out.println("Car brand is " + array[b].getBrand() + ".") ;
			System.out.println("Car name is " + array[b].getName() + ".") ;
			System.out.println("Car max speed is " + array[b].getMaxSpeed() + ".") ;
			System.out.println("Car fuel consumption is " + array[b].getFuelConsumption() + ".") ;
			System.out.println("Car price is " + array[b].getPrice() + ".") ;
			System.out.println("StartTurbo!") ;
			array[b].startTurbo() ;
			System.out.println("Turbo status is " + array[b].isTurbo() + ".") ;
			System.out.println("Current speed is " + array[b].currentSpeed(array[b].isTurbo()) + ".");
			System.out.println();			
		}		
        cin.close();
	}

}
