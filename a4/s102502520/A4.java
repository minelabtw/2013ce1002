package ce1002.a4.s102502520;
import java.util.Scanner;
import java.util.Random;
public class A4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		Random ran = new Random();
		
		Car[] cars;// class array
		int num;
		do 
		{
			System.out.println("Please input the number of cars: ");//output
			num = cin.nextInt();
			if (num <= 0)//check
				System.out.println("Out of range!");
		} while (num <= 0);
		cars = new Car[num];
		for(int x=0;x<num;x++)//set cars status
		{
			Car car = new Car(x);
			System.out.println("Input car's");
			System.out.print("Brand: ");
			String brand=cin.next();
			car.setBrand(brand);
			System.out.print("Name: ");
			String name=cin.next();
			car.setName(name);
			System.out.print("Max Speed: ");
			float maxSpeed=cin.nextFloat();
			car.setMaxSpeed(maxSpeed);
			System.out.print("fuel consumption: ");
			float fuelConsumption=cin.nextFloat();
			car.setFuelConsumption(fuelConsumption);
			System.out.print("Price: ");
			float price=cin.nextFloat();
			car.setPrice(price);
			cars[x]=car;
			System.out.println("");
		}
		cin.close();	
		System.out.println("Output car status");//output status
		for(int y=0 ;y<num;y++)
		{
			System.out.println("Car brand is "+cars[y].getBrand());
			System.out.println("Car name is "+cars[y].getName());
			System.out.println("Car max speed is "+cars[y].getMaxSpeed());
			System.out.println("Car fuel consumption is "+cars[y].getFuelConsumption());
			System.out.println("Car price is "+cars[y].getPrice());
			System.out.println("StartTurbo!");//start turbo
			int rand=ran.nextInt(2);
			boolean isTurbo = false;
			if (rand==1)
			{
				isTurbo = true;//success
			}
			if (rand==0)
			{
				isTurbo = false;//fail
			}
			cars[y].startTurbo(isTurbo);
			System.out.println("Turbo status is "+cars[y].isTurbo());
			if (rand==0)
			{
			System.out.println("Current speed is "+cars[y].currentSpeed(isTurbo));
			}
			if (rand==1)
			{
			System.out.println("Current speed is "+cars[y].getMaxSpeed());
			}
			System.out.println("");
		}
	}
}
