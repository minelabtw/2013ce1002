package ce1002.a4.s102502520;

public class Vehicle {
	private String brand;	//brand
	private String name;	//name
	private float maxSpeed;	//maxspeed
	private float fuelConsumption;	//fuel consumption
	private float price;	//price
	
	public void setBrand(String brand) {//set brand
		this.brand=brand;
	}
	
	public void setName(String name) {//set name
		this.name=name;
	}
	
	public void setMaxSpeed(float maxSpeed) {//set maxspeed
		this.maxSpeed=maxSpeed;
	}
	
	public void setFuelConsumption(float fuelConsumption) {//set fuel consumption
		this.fuelConsumption=fuelConsumption;
	}
	
	public void setPrice(float price) {//set price
		this.price=price;
	}
	
	public String getBrand(){//get brand
		return brand;
	}
	
	public String getName() {//get name
		return name;
	}
	
	public float getMaxSpeed() {//get maxspeed
		return maxSpeed;
	}
	
	public float getFuelConsumption() {//get fuel consumption
		return fuelConsumption;
	}
	
	public float getPrice() {//get price
		return price;
	}
}
