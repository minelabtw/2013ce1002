package ce1002.a4.s102502520;

public class Car extends Vehicle{
	private boolean isTurbo;	// status of turbo
	
	
	public Car(int x) {//constructor
		
	}

	public void startTurbo(boolean isTurbo) {//start turbo
		this.isTurbo=isTurbo;
	}
	
	public boolean isTurbo() {//get turbo
		return isTurbo;
	}
	
	public float currentSpeed(boolean isTurbo) {//fall to start turbo
		float rand= (float)Math.random();
		float currentSpeed=getMaxSpeed()*rand;//speed < maxspeed
		return currentSpeed;//get current speed
		
	}
}
