package ce1002.a4.s102502553;


import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int carnum;
		String brand;
		String name;
		float maxspeed;
		float fuel;
		float price;
		
		do//�P�_���l�ƶq
		{
			System.out.println("Please input the number of cars:");
			carnum = input.nextInt();
			if(carnum <= 0)
				System.out.println("Out of range!");
		}
		while(carnum <= 0);
		
		Car arr[] = new Car[carnum];//建立型態Car，名稱為arr的陣列
		
		for(int i = 0;i < carnum;i++)//輸入車子資料
		{
			arr[i] = new Car();
			System.out.println("Input car's");
			
			System.out.print("Brand: ");
			brand = input.next();
			arr[i].setBrand(brand);
			
			System.out.print("Name: ");
			name = input.next();
			arr[i].setName(name);
			
			System.out.print("Max speed: ");
			maxspeed = input.nextFloat();
			arr[i].setMaxSpeed(maxspeed);
			
			System.out.print("Fuel consumption: ");
			fuel = input.nextFloat();
			arr[i].setfuel(fuel);
			
			System.out.print("Price: ");
			price = input.nextFloat();
			arr[i].setprice(price);
			
			System.out.println();
		}
		
		System.out.println("Output car status");
		
		for(int i = 0;i < carnum;i++)//輸出車子資料
		{
			System.out.println("Car brand is " + arr[i].getBrand() + ".");
			System.out.println("Car name is " + arr[i].getName() + ".");
			System.out.println("Car max speed is " + arr[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + arr[i].getfuel() + ".");
			System.out.println("Car price is " + arr[i].getprice() + ".");
			System.out.println("StartTurbo!");
			arr[i].startTurbo();
			System.out.println("Turbo status is " + arr[i].isTurbo() + ".");
			System.out.println("Current speed is " + arr[i].currentSpeed(arr[i].isTurbo()) + ".");
			System.out.println();
		}
		
      input.close();
	}

}
