package ce1002.a4.s102502553;

import java.lang.Math;

public class Vehicle {
	
	private String brand;	//品牌
	private String name;	//名稱
	private float maxSpeed;	//最高速度
	private float fuelConsumption;	//油耗
	private float price;//價錢
	
	//儲存資料
	public Vehicle(){
		
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public void setfuel(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}
	public void setprice(float price)
	{
		this.price = price;
	}
	 
	//回傳資料	
	public String getBrand() {
		return brand;
	}
	public String getName() {
		return name;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public float getfuel()
	{
		return fuelConsumption;
	}
	public float getprice()
	{
		return price;
	}
	
	public float currentSpeed() {	//速度
		 float speed;
		 float rand;
		 rand = (float)(Math.random());
		 return speed = maxSpeed * rand;
	}
	public float currentSpeed(boolean isTurbo) {	
		if(isTurbo == true)
		{	 	 
			 return maxSpeed;
		}
		else
		{
			 float rand;
			 rand = (float)(Math.random());
			 return  maxSpeed * rand;
		}
	}
	

}
