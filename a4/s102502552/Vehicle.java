package ce1002.a4.s102502552;

public class Vehicle {
	private String brand;	//品牌
	private String name;	//車款
	private float maxspeed;	//極速
	private float fuelConsumption;	//油耗
	private float price;	//價格
	
	public Vehicle()
	{
	}//建構子
	
	public void setBrand(String brand)
	{
		this.brand = brand;
	}//設定品牌函式
	
	public void setName(String name)
	{
		this.name = name;
	}//設定車款函式
	
	public void setMaxspeed(float maxSpeed)
	{
		maxspeed = maxSpeed;
	}//設定極速函式
	
	public void setFuelconsumption(float fuelConsumption)
	{
		this.fuelConsumption = fuelConsumption;
	}//設定油耗函式
	
	public void setPrice(float price)
	{
		this.price = price;
	}//設定價格函式
	
	public String getBrand() 
	{
		return brand;
	}//取得品牌函式
	public String getName() 
	{
		return name;
	}//取得車款函式
	public float getMaxspeed() 
	{
		return maxspeed;
	}//取得極速函式
	
	public float getFuelconsumption()
	{
		return fuelConsumption;
	}//取得油耗函式
	
	public float getPrice()
	{
		return price;
	}//取得價格函式
	
	public float currentSpeed(boolean isTurbo)
	{
		float speed = maxspeed;
		if(isTurbo == false)
		{
			speed = speed * (float)(Math.random());
		}//若沒有渦輪，速度將乘上一個小於1的亂數
		return speed;
	}//瞬時速度
	

}
