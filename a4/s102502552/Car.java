package ce1002.a4.s102502552;

import java.lang.Math;

public class Car extends Vehicle{
	private boolean isTurbo;//渦輪檢查器
	
	public Car()
	{
	}//建構子
	
	public void  startTurbo()
	{
		int decider = (int)(Math.random() * 2 );//做出一個0或的亂數
		if(decider == 1)
		{
			isTurbo = true;
		}//若是1,則有渦輪
		else
		{
			isTurbo = false;
		}//檢查渦輪函式
	}
	
	public boolean isTurbo()
	{
		return isTurbo;
	}//取得渦輪狀態函式
}
