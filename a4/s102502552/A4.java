package ce1002.a4.s102502552;

import java.util.Scanner;

import ce1002.a4.s102502552.Car;

public class A4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int num = 0;
		String brand;
		String name;
		float maxSpeed = 0;
		float fuelConsumption = 0;
		float price = 0;
		boolean isTurbo;//需要導入的各項參數
		
		do{
			System.out.print("Please input the number of cars:");
			num = sc.nextInt();
			if(num < 1)
				System.out.println("Out of range!");
		}while(num < 1);//輸入車輛數，檢查合理性
		
		Car car[] = new Car[num];//創造汽車物件
		
		for(int i = 0;i < num;i++)//利用迴圈創造多輛汽車
		{
			car[i] = new Car();
			
			System.out.println("Input car's:");
			
			System.out.print("Brand:");
			brand = sc.next();
			car[i].setBrand(brand);//輸入品牌
			
			System.out.print("Name:");
			name = sc.next();
			car[i].setName(name);//輸入車款
			
			System.out.print("Maxspeed:");
			maxSpeed = sc.nextFloat();
			car[i].setMaxspeed(maxSpeed);//輸入極速
			
			System.out.print("Fuel consumption:");
			fuelConsumption = sc.nextFloat();
			car[i].setFuelconsumption(fuelConsumption);//輸入油耗
			
			System.out.print("Price:");
			price = sc.nextFloat();
			car[i].setPrice(price);//價格
		}	
		
		System.out.println();
		System.out.println("Output car status");
		
		for(int i = 0;i< num;i++)//顯示汽車各項參數
		{
			System.out.println("Car brand is " + car[i].getBrand());
			System.out.println("Car name is " + car[i].getName());
			System.out.println("Car maxspeed is " + car[i].getMaxspeed());
			System.out.println("Car Fuel consumption is " + car[i].getFuelconsumption());
			System.out.println("Car price is:" + car[i].getPrice());
			System.out.println("Start turbo!");
			car[i].startTurbo();//開始檢查渦輪
			System.out.println("Turbo status is " + car[i].isTurbo());//顯示檢查結果
			isTurbo = car[i].isTurbo();
			System.out.println("Current speed is " + car[i].currentSpeed(isTurbo));//顯示瞬時速度
			System.out.println();
		}
		
		sc.close();
	}
}
