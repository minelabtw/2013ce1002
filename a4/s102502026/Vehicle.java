package ce1002.a4.s102502026;

public class Vehicle {
	private String brand; // 品牌
	private String name; // 款式名稱
	private float maxSpeed; // 最高速度
	private float fuelConsumption; // 油耗
	private float price; // 價格

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public float getFuelConsumption() {
		return fuelConsumption;
	}

	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	// 取得目前速度(多載：汽車有渦流，但機車沒有。)


	public float currentSpeed(boolean isTurbo) {	//for result of currentSpeed if the turbo is on or off
		float a = (float) (Math.random() * 1);
		float b = 0;
		b = maxSpeed;
		if (isTurbo == true)
			return b;
		if (isTurbo == false)
			b = a * maxSpeed;
		return b;
	}
}
