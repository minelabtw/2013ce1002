package ce1002.a4.s102502026;

import java.util.Scanner;

public class A4 {
	public static Scanner input;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input = new Scanner(System.in);
		int n = 0;
		do // ask numbers of cars >1
		{
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
			if (n < 1)
				System.out.println("Out of Range!");
		} while (n < 1); // defining n>1
		Car[] cars = new Car[n]; // defining cars to be array
		String br;
		String nm;
		float sp = 0;
		float fc = 0;
		float p = 0;
		boolean t;
		for (int i = 0; i < n; i++) // doing for all input
		{
			Car car = new Car();
			System.out.println("Input car's");
			System.out.print("Brand: ");
			br = input.next();
			System.out.print("Name: ");
			nm = input.next();
			System.out.print("Max Speed: ");
			sp = input.nextFloat();
			System.out.print("Fuel consumption: ");
			fc = input.nextFloat();
			System.out.print("Price: ");
			p = input.nextFloat();
			car.setBrand(br);
			car.setName(nm);
			car.setMaxSpeed(sp);
			car.setFuelConsumption(fc);
			car.setPrice(p);
			car.startTurbo();
			cars[i] = car; // put all input in one array for each car
			System.out.println();
		}
		System.out.println("Output Car status.");
		for (int x = 0; x < n; x++) // printing all cars status
		{
			System.out.println("Car brand is " + cars[x].getBrand() + ".");
			System.out.println("Car name is " + cars[x].getName() + ".");
			System.out.println("Car max speed is " + cars[x].getMaxSpeed()
					+ ".");
			System.out.println("Car fuel consumption is "
					+ cars[x].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[x].getPrice() + ".");
			System.out.println("StartTurbo!");
			System.out.println("Turbo status is " + cars[x].isTurbo() + ".");
			System.out.println("Current speed is "
					+ cars[x].currentSpeed(cars[x].isTurbo()) + ".");
			System.out.println();
		}
		input.close();	//close scanner
	}

}
