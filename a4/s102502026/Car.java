package ce1002.a4.s102502026;

class Car extends Vehicle {
	private boolean isTurbo;

	public Car() {

	}

	public void startTurbo() { // 測試啟動渦輪，可能會成功或失敗。
		int r = (int) (Math.random() * 2);
		if (r == 1)
			isTurbo = false;
		else
			isTurbo = true;
	}

	public boolean isTurbo() { // 取得渦輪目前狀態
		return isTurbo;
	}
}
