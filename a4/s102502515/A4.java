package ce1002.a4.s102502515;
import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;//賽車數量

		do{//限制賽車數量大於零
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if (num < 1)
				{
				System.out.println("Out of range!");
				}
		}
		while (num < 1);
		
		Car cars[] = new Car[num];//class array
		String brandname = null;
		String name = null;
		Float maxSpeed;
		Float fuelConsumption;
		Float price;
		
		for (int i = 0 ; i < num ; i++){
		Car car = new Car();//產生物件
		System.out.println("Input car's");
		System.out.print("Brand: ");
		brandname = input.next();//輸入品牌
		car.setBrand(brandname);//set品牌
		System.out.print("Name: ");
		name = input.next();//輸入名字
		car.setName(name);
		System.out.print("Max speed: ");
		maxSpeed = input.nextFloat();//輸入最高速
		car.setMaxSpeed(maxSpeed);
		System.out.print("Fuel consumption: ");
		fuelConsumption = input.nextFloat();
		car.setFuelConsumption(fuelConsumption);
		System.out.print("Price: ");
		price = input.nextFloat();
		car.setPrice(price);
		System.out.println();
		cars[i] = car;//將car匯入陣列
		}
		
		System.out.println("Output car status");
		
		for (int j = 0 ; j < num ; j++)
		{
			System.out.println("Car brand is " + cars[j].getBrand() + ".");
			System.out.println("Car name is " + cars[j].getName() + ".");
			System.out.println("Car max speed is " + cars[j].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + cars[j].getFuelConsumption() + ".");
			System.out.println("Car price is " + cars[j].getPrice() + ".");
			System.out.println("StartTurbo!");
			cars[j].startTurbo();//call陣列
			System.out.println("Turbo status is " + cars[j].isTurbo() + ".");
			System.out.print("Current speed is ");
			if (cars[j].isTurbo())//if true則原最高速度
			{
				System.out.println(cars[j].getMaxSpeed() + ".");
			}
			else//false則減速
			{
				System.out.println(cars[j].getMaxSpeed()*Math.random() + ".");
			}
			System.out.println();
		}		
		input.close();//用不到，關閉
	}

}
