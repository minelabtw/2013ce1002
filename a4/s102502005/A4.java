package ce1002.a4.s102502005;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {

		int carnum;
		Car[] carlist;	//宣告儲存Car的陣列。
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input the number of cars: ");//要求使用者輸入正確的車數。
		carnum = input.nextInt();
		while(carnum<=0){
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			carnum = input.nextInt();	
		}
		
		carlist = new Car[carnum];

		for(int i=0;i<carnum;i++){ //要求使用者輸入車輛資料，並將資料存入陣列裡的Car物件。
			Car tempCar = new Car(i);//建立暫存的Car物件。
			
			System.out.println("Input car's");
			System.out.print("Brand: ");
			tempCar.setBrand(input.next());
			System.out.print("Name: ");
			tempCar.setName(input.next());
			System.out.print("Max speed: ");
			tempCar.setMaxSpeed(input.nextFloat());
			System.out.print("Fuel consumption: ");
			tempCar.setFuelConsumption(input.nextFloat());
			System.out.print("Price: ");
			tempCar.setPrice(input.nextFloat());
			System.out.println("");
			carlist[i] = tempCar;
		}

		input.close();//不需要用到input後就把input關掉。
		
		System.out.println("Output car status");//印出車輛資料。
		for(int i=0;i<carnum;i++){
			System.out.println("Car brand is " + carlist[i].getBrand() + ".");
			System.out.println("Car name is " + carlist[i].getName() + ".");
			System.out.println("Car max speed is " + carlist[i].getMaxSpeed() + ".");
			System.out.println("Car fuel consumption is " + carlist[i].getFuelConsumption() + ".");
			System.out.println("Car price is " + carlist[i].getPrice() + ".");
			System.out.println("StartTurbo!");
			carlist[i].startTurbo();
			System.out.println("Turbo status is " + carlist[i].isTurbo() + ".");
			System.out.println("Current speed is " + carlist[i].currentSpeed(carlist[i].isTurbo())+ ".");
			System.out.println("");
		}
	
	}
}
