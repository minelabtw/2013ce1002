package ce1002.a4.s102502005;

import java.util.Random;

public class Vehicle {
	private String brand; 
	private String name;  
	private float maxSpeed;  
	private float fuelConsumption;  
	private float price; 

	//set各data。
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public void setFuelConsumption(float fuelConsumption) {
		this.fuelConsumption = fuelConsumption;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	//get各data。
	public String getBrand() {
		return this.brand;
	}

	public String getName() {
		return this.name;
	}

	public float getMaxSpeed() {
		return this.maxSpeed;
	}

	public float getFuelConsumption() {
		return this.fuelConsumption;
	}

	public float getPrice() {
		return this.price;
	}


	public float currentSpeed() { 
		return this.maxSpeed;
	}

	//算出現在的速度。
	public float currentSpeed(boolean isTurbo) {
		Random ran = new Random();
		if (isTurbo == true) {
			return this.maxSpeed;
		}
		
		else {
			return this.maxSpeed*ran.nextFloat();
		}
	}
}
