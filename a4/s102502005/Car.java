package ce1002.a4.s102502005;

import java.util.Random;

public class Car extends Vehicle {

	private int id;
	private boolean isTurbo;

	public Car(int carid) { //Constructor
		id = carid;
	}

	public void startTurbo() { //用Random隨機確認Turbo是否開啟。
		Random ran = new Random();
		this.isTurbo = ran.nextBoolean();
	}

	public boolean isTurbo() {	//回傳Turbo是否開啟。
		  return this.isTurbo;
		}
}
