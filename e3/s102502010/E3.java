package ce1002.e3.s102502010;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		int n,w,h;
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		n=input.nextInt();
		while(n<1 || n>2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			n=input.nextInt();
		}
		System.out.println("Input the width: ");
		w=input.nextInt();
		while(w<1 || w>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w=input.nextInt();
		}
		System.out.println("Input the height: ");
		h=input.nextInt();
		while(h<1 || h>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			h=input.nextInt();
		}
		if(n==1)//by constructor parameters
		{
			Rectangle rectangle = new Rectangle(w, h);
			System.out.println("The width of this rectangle is " + rectangle.getwidth());
			System.out.println("The height of this rectangle is " + rectangle.getheight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
		else//create by setter
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
			System.out.println("The width of this rectangle is " + rectangle.getwidth());
			System.out.println("The height of this rectangle is " + rectangle.getheight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
	}
}
