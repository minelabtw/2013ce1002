package ce1002.e3.s102502516;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		
			Scanner scanner = new Scanner(System.in);
			int way, widthInput,heightInput;
			do {
				System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
				way = scanner.nextInt();
				if (way <= 0 || way >=3)
					System.out.println("Out of range!");
			} while (way <= 0 || way >=3);//問方法
			
			do {
				System.out.println("Input the width: ");
				widthInput = scanner.nextInt();
				if (widthInput < 1 || widthInput > 20)
					System.out.println("Out of range!");
			} while (widthInput < 1 || widthInput > 20);//問wigth
			do {
				System.out.println("Input the height: ");
				heightInput = scanner.nextInt();
				if (heightInput < 1 || heightInput > 20)
					System.out.println("Out of range!");
			} while (heightInput < 1 || heightInput > 20);//問height
			
			if ( way == 1 ) {
				Rectangle rectangle = new Rectangle( widthInput , heightInput );
				rectangle.GetArea();//印出面積
			}//方法1
			else {
				Rectangle rectangle = new Rectangle();
				rectangle.SetWidth(widthInput);// set the width
				rectangle.SetHeight(heightInput);// set the height
				rectangle.GetArea();//印出面積
			}//方法2
			scanner.close();
	}
}
