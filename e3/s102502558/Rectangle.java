package ce1002.e3.s102502558;

public class Rectangle {
	private int width;
	private int height;
	
	// default constructor
	public Rectangle()
	{
		width  = 0;
		height = 0;
	}
	// constructor with width height
	public Rectangle(int w,int h)
	{
		width  = w;
		height = h;
	}
	
	// setter
	public void setWidth(int w)
	{
		width = w;
	}
	
	public void setHeight(int h)
	{
		height = h;
	}
	// return area
	public int getArea()
	{
		return width * height;
	}
}
