package ce1002.e3.s102502558;

import java.util.Scanner;
public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int way = 0, width, height;
		// 取得way
		while (true)
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			way = scanner.nextInt();
			if (way == 1 || way == 2)
				break;
			System.out.println("Out of range!");
		}
		// 取得width		
		while (true)
		{
			System.out.println("Input the width: ");
			width = scanner.nextInt();
			if (width >= 1 && width <= 20)
				break;
			System.out.println("Out of range!");
		}
		// 取得height
		while (true)
		{
			System.out.println("Input the height: ");
			height = scanner.nextInt();
			if (height >= 1 && height <= 20)
				break;
			System.out.println("Out of range!");
		}
		// 宣告rectangle
		Rectangle rec;
		// 用不同的方法建構
		if (way == 1)
		{
			rec = new Rectangle(width, height);
		}
		else
		{
			rec = new Rectangle();
			rec.setWidth(width);
			rec.setHeight(height);
		}
		//印出結果
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + rec.getArea());
		scanner.close();
	}

}
