package ce1002.e3.s100201023;

import java.util.Scanner;

public class E3 
{
	public static void main(String[] args) 
	{
		int method = 0 , width = 0 , height = 0;
		Scanner input = new Scanner(System.in);
		
		// input method of compute area
		while(true)
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();
			
			if(method == 1 || method == 2)
				break;
			
			System.out.println("Out of range!");
		}
		
		//input width
		while(true)
		{
			System.out.println("Input the width: ");
			width = input.nextInt();
			
			if(width > 0 && width < 21)
				break;
			
			System.out.println("Out of range!");
		}
		
		//input height
		while(true)
		{
			System.out.println("Input the height: ");
			height = input.nextInt();
			
			if(height > 0 && height < 21)
				break;
			
			System.out.println("Out of range!");
		}
		
		if(method == 1)
		{
			//method 1
			Rectangle graph = new Rectangle(width, height);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + graph.getArea());
		}
		else 
		{
			//method 2
			Rectangle graph = new Rectangle();
			graph.setWidth(width);
			graph.setHeight(height);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + graph.getArea());
		}
		
	}
}
