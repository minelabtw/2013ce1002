package ce1002.e3.s100201023;

public class Rectangle 
{
	// declare variable
	private int width;
	private int height;
	
	//constructure
	public Rectangle()
	{
		
	}
	public Rectangle(int a , int b)
	{
		width = a;
		height = b;
	}
	
	//set width
	public void setWidth(int a)
	{
		width = a;
	}
	
	//set height
	public void setHeight(int a)
	{
		height = a;
	}
	
	public int getArea()
	{
		return width * height;
	}
	
}
