package ce1002.e3.s102502518;

public class Rectangle {

	private int width;
	private int height;

	public Rectangle(int w ,int h ) {//方法一
		width = w;
		height = h;
	}
	public Rectangle() {//方法二
		
	}
	public void SetWidth(int w) {//設width
		width = w;
	}
	public void SetHeight(int h) {//設height
		height = h;
	}
	public int GetWidth() {//獲取width
		return width;
	}
	public int GetHeight() {//獲取height
		return height;
	}
	public int GetArea() {//獲取area
		return width*height;
	}
	
}
