package ce1002.e3.s102502518;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);	
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		int way = scanner.nextInt();
		while (way <= 0 || way >=3) {//判斷為範圍之內
			System.out.println("Out of range!\nCreate rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			way = scanner.nextInt();
		}
		
		int width;
	    int height;
             
               if (way == 1) {
                   System.out.println("Input the width: "); 
                   width = scanner.nextInt();
                   while (width <= 0 || width >=21) {//判斷為範圍之內
			           System.out.println("Out of range!\nInput the width: ");
		               width = scanner.nextInt();
                   }
                   System.out.println("Input the height: ");
                   height = scanner.nextInt();
                   while (height <= 0 || height >=21) {//判斷為範圍之內
			           System.out.println("Out of range!\nInput the height: ");
			           height = scanner.nextInt();
                   }
                   Rectangle rectangle = new Rectangle(width, height);//方法一
                   System.out.println("The width of this rectangle is " + rectangle.GetWidth());
                   System.out.println("The height of this rectangle is " + rectangle.GetHeight());
                   System.out.println("The area of this rectangle is " + width*height);
               }
               else {
                   System.out.println("Input the width: "); 
                   width = scanner.nextInt();
                   while (width <= 0 || width >=21) {//判斷為範圍之內
			           System.out.println("Out of range!\nInput the width: ");
		               width = scanner.nextInt();
                   }
                   System.out.println("Input the height: ");
                   height = scanner.nextInt();
                   while (height <= 0 || height >=21) {//判斷為範圍之內
			           System.out.println("Out of range!\nInput the height: ");
			           height = scanner.nextInt();
                   }
                   Rectangle rectangle = new Rectangle();//方法二
                   rectangle.SetWidth(width);
                   rectangle.SetHeight(height);
                   System.out.println("The width of this rectangle is " + rectangle.GetWidth());
                   System.out.println("The height of this rectangle is " + rectangle.GetHeight());
                   System.out.println("The area of this rectangle is " + rectangle.GetArea());
               }
        }
        
	}