package ce1002.e3.s102502515;
import java.util.Scanner;

public class E3 {
	
	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	int choice = 0;
	int width = 0;
	int height = 0;
	int area = 0;
	
	do{//選擇方式，確認輸入值在範圍內
	System.out.println("Create rectangle in 2 ways.");
	System.out.println("1)create by constructor parameters,2)create by setter: ");
	choice = input.nextInt();
	
	
	
	if (choice != 1 && choice != 2)//不在範圍內則輸出
		{
		System.out.println("Out of range!");
		}
	}
	while (choice != 1 && choice != 2);
		
	do{//確認寬的輸入值在範圍內
		System.out.println("Input the width: ");
		width = input.nextInt();
	
		if (width > 20 || width < 1)
		{
			System.out.println("Out of range!");
		}	
	}
	while (width > 20 || width < 1);

	do{//確認高的輸入值在範圍內
		System.out.println("Input the height: ");
		height = input.nextInt();
	
		if (height > 20 || height < 1)
		{
			System.out.println("Out of range!");
		}	
	}
	while (height > 20 || height < 1);
	
	
	if (choice == 1)//方法1
	{
		Rectangle rectangle = new Rectangle(width,height);//直接將值變成初始值
		area = rectangle.getArea();//取得面積
	}
	
	else{//方法2
	Rectangle rectangle = new Rectangle();
	rectangle.setWidth(width);//將寬的數值給Class Rectangle
	rectangle.setHeight(height);//將高的數值給Class Rectangle
	area = rectangle.getArea();//取得面積
	}
	
	System.out.println("The width of this rectangle is " + width);
	System.out.println("The height of this rectangle is " + height);
	System.out.println("The area of this rectangle is " + area);
	
	input.close();
	}

}
