package ce1002.e3.s102502515;

public class Rectangle {
		private int width;
		private int height;
	
		public Rectangle(int a, int b) {//方法1
			width = a;
			height = b;
		}

		public Rectangle() {//方法2，初始值為0
			width = 0;
			height = 0;
		}
		
		
		public void setWidth(int W)//set值
		{
			width = W;
		}
		
		public void setHeight(int H)
		{
			height = H;
		}

		public int getWidth() {//回傳寬
			return width;
		}

		public int getHeight() {//回傳高
			return height;
		}

		public int getArea()//回傳面積
		{
			return width*height;
		}

}
