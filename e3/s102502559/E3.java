package ce1002.e3.s102502559;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		Scanner input = new Scanner(System.in);//新增輸入器
		int mode = input.nextInt();
		//判斷mode範圍
		while (mode < 1 || mode > 2) {
			System.out.println("Out of range!");
			System.out
					.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			mode = input.nextInt();
		}
		System.out.println("Input the width: ");
		int w = input.nextInt();
		//判斷範圍,若超出範圍要求使用者再輸入一次
		while (w < 1 || w > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w = input.nextInt();
		}
		System.out.println("Input the height: ");
		int h = input.nextInt();
		//判斷範圍,若超出範圍要求使用者再輸入一次
		while (h < 1 || h > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			h = input.nextInt();
		}
		//使用者輸入的mode為1時,新建一個類別為Rectangle的物件,使用建構子+參數初始化物件
		if (mode == 1) {
			Rectangle rectangle = new Rectangle(w,h);
		}
		//使用者輸入的mode為2時,新建一個類別為Rectangle的物件,使用setter設定物件
		if (mode == 2)
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
			rectangle.compute(w,h);
		}
	}

}
