package ce1002.e3.s102502559;

public class Rectangle {

	private int width;
	private int height;
	public Rectangle()
	{
	}
	public Rectangle(int w,int h)
	{
		width = w;
		height = h;
		int area = width*height;
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + area);
	}
	public void setWidth(int w)
	{
		width = w;
	}
	public void setHeight(int h)
	{
		height = h;
	}
	public void compute(int width,int height)
	{
		int area = width*height;
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + area);
	}
}
