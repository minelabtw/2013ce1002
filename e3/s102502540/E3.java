package ce10021.e3.s102502540;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int way, width, height;  //宣告變數
		do {
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:"); //輸出
			way = input.nextInt();
			if (way != 1 && way != 2)
				System.out.println("Out of range!");
		} while (way != 1 && way != 2);
		do {
			System.out.println("Input the width:");
			width = input.nextInt();
			if (width < 1 || width > 20)
				System.out.println("Out of range!");
		} while (width < 1 || width > 20);
		do {
			System.out.println("Input the height:");
			height = input.nextInt();
			if (height < 1 || height > 20)
				System.out.println("Out of range!");
		} while (height < 1 || height > 20);
		if (way == 1) { //方法一
			Rectangle rectangle = new Rectangle(width, height);
			System.out.println("The width of this rectangle is " + rectangle.getwidth()); //輸出
			System.out.println("The height of this rectangle is " + rectangle.getheight());
			System.out.print("The area of this rectangle is "
					+ rectangle.getarea());
		} else { //方法二
			Rectangle rectangle = new Rectangle();
			rectangle.setwidth(width);
			rectangle.setheight(height);
			System.out.println("The width of this rectangle is " + rectangle.getwidth());
			System.out.println("The height of this rectangle is " + rectangle.getheight());
			System.out.print("The area of this rectangle is "
					+ rectangle.getarea());
		}
	}
}