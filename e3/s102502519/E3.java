package ce1002.e3.s102502519;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner imput = new Scanner(System.in);
		int num1=0;
		int width=0,height=0;
		
		do
		{
			System.out.print("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			num1 = imput.nextInt();
			
			if(num1<1  || num1>2)
				System.out.println("Out of range!");
		}while(num1<1  || num1>2);
		
		do
		{
			System.out.println("Input the width: ");
			width = imput.nextInt();
			
			if(width<1 || width>20)
				System.out.println("Out of range!");
		}while(width<1 || width>20);    //����b1~20
		
		do
		{
			System.out.println("Input the height: ");
			height = imput.nextInt();
			
			if(height<1 || height>20)
				System.out.println("Out of range!");
		}while(height<1 || height>20);    //����b1~20
		
		Rectangle rectangle;
		
		if(num1 == 1){
			rectangle= new Rectangle(width, height);
		}		
		else
		{
			rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
		}
				
		
		System.out.println("The width of this rectangle is " + rectangle.getWidth());
		System.out.println("The height of this rectangle is " + rectangle.getHeight());
		System.out.println("The area of this rectangle is " + rectangle.getArea());
	}	

}
