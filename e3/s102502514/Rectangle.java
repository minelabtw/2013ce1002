package ce1002.e3.s102502514;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle(int _width,int _height)
	{
		width = _width;
		height = _height;
	}
	public Rectangle()
	{
		
	}
	public int getArea()
	{
		return width * height;
	}
	public void setWidth(int _width)
	{
		width = _width;
	}
	public void setHeight(int _height)
	{
		height = _height;
	}

}
