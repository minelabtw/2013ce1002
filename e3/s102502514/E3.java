package ce1002.e3.s102502514;
import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int _width, _height;
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		int number = input.nextInt();
		while (number!=1 && number!=2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			number = input.nextInt();
		}
		System.out.println("Input the width: ");
		_width = input.nextInt();
		while (_width<0 || _width>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			_width = input.nextInt();
		}
		System.out.println("Input the height: ");
		_height = input.nextInt();
		while (_height<0 || _height>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			_height = input.nextInt();
		}
		System.out.println("The width of this rectangle is " + _width);
		System.out.println("The height of this rectangle is " + _height);
		if (number==1)  //如果是方法1，則用"使用建構子+參數初始化物件"的方法
		{
			Rectangle rectangle = new Rectangle(_width , _height);
			System.out.print("The area of this rectangle is " + rectangle.getArea() );
		}
		if (number==2)  //如果是方法2，則使用"使用setter設定物件"的方法
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(_width);
			rectangle.setHeight(_height);
			System.out.print("The area of this rectangle is " + rectangle.getArea() );
		}
		
	}

}
