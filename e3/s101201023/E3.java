package ce1002.e3.s101201023;

import java.util.Scanner;

public class E3 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		int assign=0;
		int w=0,h=0;
		Scanner input = new Scanner(System.in);
		
		//判斷方法1or2
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		assign = input.nextInt();
		while(assign!=1 && assign!=2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			assign = input.nextInt();
		}
		
		
		if(assign==1)
		{
			//輸入寬
			System.out.println("Input the width: ");
			w = input.nextInt();
			while(w<1 || w>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w = input.nextInt();
			}
			
			//輸入高
			System.out.println("Input the height: ");
			h = input.nextInt();
			while(h<1 || h>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h = input.nextInt();
			}
			
			//將寬高存入Rectangle裡，並輸出寬高及面積
			Rectangle rectangle = new Rectangle(w,h);
			System.out.print("The width of this rectangle is ");
			System.out.println(rectangle.getWidth());
			System.out.print("The height of this rectangle is ");
			System.out.println(rectangle.getHeight());
			System.out.print("The area of this rectangle is ");
			System.out.print(rectangle.getArea());
		}
		
		if(assign==2)
		{
			System.out.println("Input the width: ");
			w = input.nextInt();
			while(w<1 || w>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w = input.nextInt();
			}
			
			System.out.println("Input the height: ");
			h = input.nextInt();
			while(h<1 || h>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h = input.nextInt();
			}
			
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.print("The width of this rectangle is ");
			System.out.println(rectangle.getWidth());
			System.out.print("The height of this rectangle is ");
			System.out.println(rectangle.getHeight());
			System.out.print("The area of this rectangle is ");
			System.out.print(rectangle.getArea());
		}
	}

}
