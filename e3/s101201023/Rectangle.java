package ce1002.e3.s101201023;

public class Rectangle 
{
	private int width=1;
	private int height=1;
	
	Rectangle()
	{
		
	}
	
	Rectangle(int w , int h)
	{
		width=w;
		height=h;
	}
	
	void setWidth(int w)
	{
		width=w;
	}
	
	void setHeight(int h)
	{
		height=h;
	}
	
	int getWidth()
	{
		return width;
	}
	
	int getHeight()
	{
		return height;
	}
	
	int getArea()
	{
		return width*height;
	}
}
