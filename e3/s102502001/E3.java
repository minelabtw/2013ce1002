package ce1002.e3.s102502001;
import java.util.Scanner;

public class E3 {

	
	public static void main(String[] args) {
		Scanner input = new Scanner( System.in );
		int way=0;
		int wid;
		int hei;
		do{
			System.out.println("Create rectangle in 2 ways.");     //輸出字串
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			way=input.nextInt();    //輸入way
			if(way<1 || way>2){		//判斷範圍
			System.out.println("Out of range!");
			}
		}while(way<1 || way>2);
		
		do{
			System.out.println("Input the width: ");              //輸出字串
			wid=input.nextInt();    //輸入寬
			if(wid<1 || wid>20){    //判斷範圍
				System.out.println("Out of range!");
			}
		}while(wid<1 || wid>20);
		
		do{
			System.out.println("Input the height: ");            //輸出字串
			hei=input.nextInt();    //輸入高
			if(hei<1 || hei>20){    //判斷範圍
				System.out.println("Out of range!");
			}
		}while(hei<1 || hei>20);
		
		if(way==1){
			Rectangle rec= new Rectangle(wid,hei);    //宣告一個較rec的物件,並且在建構子中設定寬和高
			rec.Setarea();          //宣告rec中的Setarea函式
		}
		
		else{
			Rectangle rec=new Rectangle();           //宣告一個較rec的物件,並且另外寫入寬和高
			rec.Setwidth(wid);
			rec.Setheight(hei);
			rec.Setarea();
		}
	}

}
