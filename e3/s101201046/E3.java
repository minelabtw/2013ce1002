package ce1002.e3.s101201046;

import java.util.Scanner;


public class E3 {

	public static void main(String[] args) {
		int opt = 0;
		int w = 0, h = 0;
		Scanner scan = new Scanner(System.in); //set input
		
		do { //give 2 options for construct rectangle
			System.out.print("Create rectangle in 2 ways.\n" +
					 		 "1)create by constructor parameters,2)create by setter:\n");
			
			opt = scan.nextInt(); //input the option
			
			if (opt > 2 || opt < 1) //check error input
				System.out.println("Out of range!");
		} while (opt > 2 || opt < 1);
		
		do {
			System.out.println("Input the width:"); // input the width 
			
			w = scan.nextInt(); 
			
			if (w > 20 || w < 1) //check error input
				System.out.println("Out of range!");
		} while(w > 20 || w < 1);
		

		do {
			System.out.println("Input the height:"); //input the height
			
			h = scan.nextInt();
			
			if (h > 20 || h < 1) //check error input
				System.out.println("Out of range!");
		} while(h > 20 || h < 1);
		
		
		if (opt == 1) { //the work of option 1
			Rectangle rectangle = new Rectangle(w, h);
			System.out.println("The width of this rectangle is " + w);
			System.out.println("The height of this rectangle is " + h);
			System.out.println("The area of this rectangle is " + (w*h));
		}
		else { //the work of option 2
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			
			System.out.println("The width of this rectangle is " + w);
			System.out.println("The height of this rectangle is " + h);
			System.out.println("The area of this rectangle is " + (w*h));
		}
				
	scan.close();
	return;
	}

}
