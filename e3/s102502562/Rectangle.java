package ce1002.e3.s102502562;

public class Rectangle {
	private static int width;
	private static int height;
	
	public Rectangle()
	{
		
	}
	public Rectangle(int w,int h)//用建構子初始化寬長
	{
		width= w;
		height=h;
	}
	public static int getArea()//計算面積
	{
		return width*height;
	}
	public int setWidth(int w)//設定寬
	{
		return width=w;
	}
	public int setHeight(int h)//設定長
	{
		return height=h;
	}
}
