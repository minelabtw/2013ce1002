package ce1002.e3.s102502562;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner( System.in );
		int number,width,height,area;
		do//詢問要使用1或2的方法並判斷輸入是否在範圍內
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			number=input.nextInt();
			if(number<1 || number>2)
			{
				System.out.println("Out of range!");
			}
		}while(number<1 || number>2);
		if(number==1)//第一個方法
		{
			do//詢問使用者的寬並判斷是否在範圍內
			{
				System.out.println("Input the width:");
				width=input.nextInt();
				if(width<1 || width>20)
				{
					System.out.println("Out of range!");
				}
			}while(width<1 || width>20);
			do//詢問使用者的長並判斷是否在範圍內
			{
				System.out.println("Input the height:");
				height=input.nextInt();
				if(height<1 || height>20)
				{
					System.out.println("Out of range!");
				}
			}while(height<1 || height>20);
			Rectangle rectangle=new Rectangle(width,height);//用建構子初始化寬長
			area=Rectangle.getArea();//計算面積
			System.out.println("The width of this rectangle is " + width);//輸出矩形的寬長和面積
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + area);
		}
		if(number==2)//第二個方法
		{
			do
			{
				System.out.println("Input the width:");
				width=input.nextInt();
				if(width<1 || width>20)
				{
					System.out.println("Out of range!");
				}
			}while(width<1 || width>20);
			do
			{
				System.out.println("Input the height:");
				height=input.nextInt();
				if(height<1 || height>20)
				{
					System.out.println("Out of range!");
				}
			}while(height<1 || height>20);
			Rectangle rectangle=new Rectangle();
			rectangle.setWidth(width);//設定寬
			rectangle.setHeight(height);//設定長
			area=Rectangle.getArea();
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + area);
		}
	}

}
