package ce1002.e3.s992001026;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
        int n ,w,h ,a;
        // 輸入方法
		Scanner scn = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		
		n = scn.nextInt();
		
		while ( n != 1 && n !=2 ){
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			
			n = scn.nextInt();
		}
		// 輸入寬度
		System.out.println("Input the width:");
		w = scn.nextInt();		

		while ( w >20 || w<1 ){
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			w = scn.nextInt();		
		}
		
		//輸入長度
		System.out.println("Input the height:");
		h = scn.nextInt();		

		while ( h >20 || h<1 ){
			System.out.println("Out of range!");
			System.out.println("Input the height:");
			h = scn.nextInt();		
		}
		
		// 假如方法為1 則使用建構子+參數初始化物件 否則使用setter設定物件
	     if ( n == 1 ){
			Rectangle rectangle = new Rectangle(w, h);
			a = rectangle.getArea();
		}else {
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
			a = rectangle.getArea();
		}
		
		// 輸出
		System.out.println("The width of this rectangle is " + w) ;
        System.out.println("The height of this rectangle is " + h);
        System.out.println("The area of this rectangle is "+ a );
	}

}
