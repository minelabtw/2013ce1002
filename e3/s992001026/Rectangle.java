package ce1002.e3.s992001026;

public class Rectangle {

	 private int width ;
	 private int height ;
	 
	 public  Rectangle() {
		 
	 }
	 // 設定初始直 (使用建構子+參數初始化物件
	 public Rectangle( int i , int j ){
		 width = i ; 
		 height = j ;
	 }
	//使用setter設定物件
	 public void setWidth( int i ){
		 width = i ;
	 }
	 public void setHeight( int j ){
		 height = j ;
	 }
	 // 計算面積及迴船
	 public int getArea(){
		 return width*height;
	 }

}
