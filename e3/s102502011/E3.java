package ce1002.e3.s102502011;
import java.util.Scanner;


public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in) ;

		int choose , w , h ; //宣告方法 長 寬
		
		System.out.println("Create rectangle in 2 ways.") ;
		
		do {
			System.out.println("1)create by constructor parameters,2)create by setter:") ;
		    choose = input.nextInt() ;
		    if (choose != 1 && choose != 2 )    
		    	System.out.println("Out of range!") ;
		}
		while (choose != 1 && choose != 2 ) ;
		
		switch(choose) { //方法一和方法二
		case 1 :
			do {
				System.out.println("Input the width: ") ;  //輸入寬
				w = input.nextInt() ;
				if (w<1 || w>20 )
					System.out.println("Out of range!") ;
			}
			while (w<1 || w>20 ) ;
			
			do {
				System.out.println("Input the height: ") ;  //輸入長
				h = input.nextInt() ;
				if (h<1 || h>20 )
					System.out.println("Out of range!") ;
			}
			while (h<1 || h>20 ) ;
			
			Rectangle rectangle = new Rectangle(w, h) ; //使用建構子+參數初始化物件
			
			rectangle.Area(); //呼叫面積公式
			
			break;
		case 2 :
			do {
				System.out.println("Input the width: ") ;
				w = input.nextInt() ;
				if (w<1 || w>20 )
					System.out.println("Out of range!") ;
			}
			while (w<1 || w>20 ) ;
			
			do {
				System.out.println("Input the height: ") ;
				h = input.nextInt() ;
				if (h<1 || h>20 )
					System.out.println("Out of range!") ;
			}
			while (h<1 || h>20 ) ;
			
			Rectangle rectangle2 = new Rectangle(); //使用setter設定物件
			rectangle2.setWidth(w);// set the width
			rectangle2.setHeight(h);// set the height
			
			rectangle2.Area();
			
			break ;
		}
		
		input.close();

	}

}
