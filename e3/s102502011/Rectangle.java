package ce1002.e3.s102502011;

public class Rectangle {
	private int width, height , area ; //宣告三變數
	
	public Rectangle(int w,int h ) { //方法一 會傳值
		width = w;
		height = h;
	}
	
	public Rectangle() { //第二個方法並不會傳值 故空白

	}
	
	public void setWidth(int w){
		width = w;
	}
	
	public void setHeight(int h) {
		height = h ;
	}
	
	public void Area() {         //面積公式及輸出
		area = width * height ;
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + area);
	}

}
