package ce1002.e3.s102502503;
import java.util.Scanner;
import ce1002.e3.s102502503.Rectangle;
public class E3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int w;				//寬高 模式
		int h;
		int mode;
		do {  //選擇模式
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			mode = scanner.nextInt();
			if (mode < 1 || mode > 2) {
				System.out.println("Out of range!");
			}
		} while (mode < 1 || mode > 2);
		do {  //輸入寬
			System.out.println("Input the width: ");
			w = scanner.nextInt();
			if (w <= 0 || w > 20) {
				System.out.println("Out of range!");
			}
		} while (w <= 0 || w > 20);
		do {  //輸入高
			System.out.println("Input the height: ");
			h = scanner.nextInt();
			if (h <= 0 || h > 20) {
				System.out.println("Out of range!");
			}
		} while (h <= 0 || h > 20);
		if (mode == 1) {			//模式1
			Rectangle rectangle = new Rectangle(w,h);
			System.out.println("The width of this rectangle is "+rectangle.getWidth());
			System.out.println("The height of this rectangle is "+rectangle.getHeight());
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
		if (mode == 2) {  //模式2
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
			System.out.println("The width of this rectangle is "+rectangle.getWidth());
			System.out.println("The height of this rectangle is "+rectangle.getHeight());
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
		scanner.close();
	}
}
