package ce1002.e3.s102502053;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int w; // call variables
		int h;
		int mode;
		
		//data validation for input
		do
		{
			System.out.print("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: \n");
			mode = input.nextInt();
			if(mode != 1 && mode != 2)
			{
				System.out.print("Out of range!\n");
			}
		}while(mode != 1 && mode != 2);
		
		do //data validation for input
		{
			System.out.print("Input the width: \n");
			w = input.nextInt();
			if(w<1 || w>20)
			{
				System.out.print("Out of range!\n");
			}
		}while(w<1 || w>20);
		
		do //data validation for input
		{
			System.out.print("Input the height: \n");
			h = input.nextInt();
			if(h<1 || h>20)
			{
				System.out.print("Out of range!\n");
			}
		}while(h<1 || h>20);
		
		switch(mode) //the mode that will selected to calculate
		{
		case 1:
		{
			Rectangle rectangle = new Rectangle(w, h);
			System.out.print("The width of this rectangle is " + w + "\n");
			System.out.print("The height of this rectangle is " + h + "\n");
			System.out.print("The area of this rectangle is " + rectangle.getArea() + "\n");
			break;
		}
		case 2:
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.print("The width of this rectangle is " + w + "\n");
			System.out.print("The height of this rectangle is " + h + "\n");
			System.out.print("The area of this rectangle is " + rectangle.getArea() + "\n");
			break;
		}
			
		}

}
}

//class rectangle
class Rectangle { 
	private int width;
	private int height;
	
	Rectangle(int a, int b) //constructor	
	{
		width = a;
		height = b;
	}
	
	Rectangle() //constructor
	{
		
	}
	
	void setWidth(int w)
	{
		width = w;
	}
	
	void setHeight(int h)
	{
		height = h;
	}
	
	int getArea() //calculate for the area
	{
		return width*height;
	}
}