package ce1002.e3.s100204006;

import java.util.Scanner;
public class E3 
{
	public static void main(String[] args) 
	{
		while (true)
		{
			int N;
			Scanner scanner = new Scanner(System.in);      //System.in用來取得使用者的輸入
			System.out.print("Create rectangle in 2 ways.1)create by constructor parameters,2)create by setter: ");
			N = scanner.nextInt() ;
			
			if(N>=1&&N<3)
			{
				
				if(N==1)        //使用方法一
				{
					int w;
					scanner = new Scanner(System.in); 
					System.out.print("Input the width:");
					w = scanner.nextInt();
					
					if(w>=1&&w<21)
					{
						int h;
						scanner = new Scanner(System.in);
						System.out.print("Input the height:");
						h = scanner.nextInt();
						Rectangle rectangle = new Rectangle(w, h);
						
						if(h>=1&&h<21)
						{
							System.out.println("The width of this rectangle is " +w);
							System.out.println("The height of this rectangle is " +h);
							System.out.println("The area of this rectangle is " + rectangle.getarea());
						}
					}
					else
					{
						System.out.println("Out of range!");
					}
				}
				else            //使用方法二
				{
					int w;
					System.out.print("Input the width:");
                    w = scanner.nextInt();
					
					if(w>=1&&w<21)
					{
						int h;
						scanner = new Scanner(System.in);
						System.out.print("Input the height:");
						h = scanner.nextInt();
						Rectangle rectangle = new Rectangle();
						rectangle.setwidth(w);        // set the width
						rectangle.setheight(h);      // set the height
						
						if(h>=1&&h<21)
						{
							System.out.println("The width of this rectangle is " +rectangle.getwidth());
							System.out.println("The height of this rectangle is " +rectangle.getheight());
							System.out.println("The area of this rectangle is " + rectangle.getarea());
						}
					}
					else
					{
						System.out.println("Out of range!");
					}			
				}
			}
			else
			{
				System.out.println("Out of range!");
			}
		}
	}
}
