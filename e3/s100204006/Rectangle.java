package ce1002.e3.s100204006;

public class Rectangle 
{
	private int width;    //長方形的寬
	private int height;    //長方形的高
	
	public Rectangle()
	{
		
	}
	public Rectangle(int width, int height)
	{
		this.width = width;
		this.height = height;
	}
	public void setwidth(int width)     //set width
	{
		this.width = width;
	}
	public int getwidth()          //get width
	{
		return this. width;
	}
	public void setheight(int height)        //set height
	{
		this.height = height;
	}
	public int getheight()         //get height
	{
		return this.height;
	}
	public int getarea()
	{
		return this.width*height;     //get area
	}
}
