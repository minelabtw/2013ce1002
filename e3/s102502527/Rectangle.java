package ce1002.e3.s102502527;

public class Rectangle {
	
	private int Width;
	private int Height;
	
	public Rectangle(int width,int height)//方法1直接回傳
	{
		Width = width;
		Height = height;
	}
	
	public Rectangle(){}//方法二
	
	public void change(int width,int height)//方法二中的改變變數
	{
		Width = width;
		Height = height;
	}
	
	public int getArea()//計算面積
	{
		return Width * Height;
	}

}
