package ce1002.e3.s102502527;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		int number = 0;
		int width = 0;
		int height = 0;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Create rectangle in 2 ways.");//書出字元並判斷是否為1或2
		System.out.println("1)create by constructor parameters,2)create by setter:");
		number = input.nextInt();
		while ( number != 1 && number != 2 )
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			number = input.nextInt();
		}
		
		System.out.println("Input the width:");//判斷寬和高的值是否為範圍內
		width = input.nextInt();
		while ( width < 1 || width > 20 )
		{
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			width = input.nextInt();
		}
		
		System.out.println("Input the height:");
		height = input.nextInt();
		while ( height < 1 || height > 20 )
		{
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			height = input.nextInt();
		}
		
		if ( number == 1 )//第一種方法時回傳值
		{
			Rectangle rectangle = new Rectangle(width,height);
		
			System.out.println("The width of this rectangle is " + width );
			System.out.println("The height of this rectangle is " + height );
			System.out.print("The area of this rectangle is " + rectangle.getArea());
		}
		
		else//第二種方法時先叫出函數再回傳
		{
			Rectangle rectangle = new Rectangle();
			rectangle.change(width, height);
			
			System.out.println("The width of this rectangle is " + width );
			System.out.println("The height of this rectangle is " + height );
			System.out.print("The area of this rectangle is " + rectangle.getArea());
		}
		
		
		
		
	}

}
