package ce1002.e3.s102502504;

import java.util.Scanner;

public class E3 
{

	public static void main(String[] args) 
	{
		int a; //模式1或模式2
		int w=0,h=0; //長 寬
		do //判斷輸入值為1或2
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			Scanner input = new Scanner(System.in);
			a =input.nextInt();
			if(a!=1 && a!=2)
			System.out.println("Out of range!");
		}while(a!=1 && a!=2);
		
		do //取得寬
		{
			System.out.println("Input the width: ");
			Scanner input = new Scanner(System.in);
			w =input.nextInt();
			if(w<1 || w>20)
			System.out.println("Out of range!");	
		}while(w<1 || w>20);
		
		
		do //取得高
		{
			System.out.println("Input the height: ");
			Scanner input = new Scanner(System.in);
			h =input.nextInt();
			if(h<1 || h>20)
			System.out.println("Out of range!");	
		}while(h<1 || h>20);
		
		if(a==1) //若a=1則進入以下
		{
		Rectangle rec1 = new Rectangle(w, h);
		}
		else //若a=2則進入以下
		{
		Rectangle rec2 = new Rectangle();
		rec2.setWidth(w);
		rec2.setHeight(h);
		int area = getarea(w,h);
		System.out.println("The area of this rectangle is " + area);
		}
	}

	private static int getarea(int w, int h) {
		// TODO Auto-generated method stub
		return 0;
	}
}
