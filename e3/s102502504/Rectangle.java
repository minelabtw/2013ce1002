package ce1002.e3.s102502504;

import java.util.Scanner;

public class Rectangle
{
	
		private int width;
		private int height;
		

		
		Rectangle(int w, int h) //模式1
		{
			width = w;
			height = h;
			System.out.println("The width of this rectangle is " +width );
			System.out.println("The height of this rectangle is " +height );
			int area = getarea(w,h); //利用getarea計算面積
			System.out.println("The area of this rectangle is " + area);
		}
		
		Rectangle() //模式2
		{
			
		}
		
		public void setWidth(int w) {
			System.out.println("The width of this rectangle is " +w );
		}
		
		public void setHeight(int h) {
			System.out.println("The height of this rectangle is " +h );
		}
		
		public static int getarea(int width, int height) //用來計算面積的工具
		{
			return width*height; 
		}

}
