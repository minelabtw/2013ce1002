package ce1002.e3.s102502511;

import java.util.Scanner;

public class E3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int ways; //方法的變數
		int width; //寬的變數
		int height; //高的變數
		Scanner input = new Scanner (System.in); 
		
		do{ //選擇法一或法二
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			ways = input.nextInt();
			if(ways != 1 && ways != 2)
			{
				System.out.println("Out of range!");
			}
		}while(ways != 1 && ways != 2);
		
		do{ //輸入寬 並且檢驗
			System.out.println("Input the width:");
			width = input.nextInt();
			if(width <= 0 || width > 20)
			{
				System.out.println("Out of range!");
			}
		}while(width <= 0 || width > 20);
		
		do{ //輸入高 並且檢驗
			System.out.println("Input the height:");
			height = input.nextInt();
			if(height <= 0 || height > 20)
			{
				System.out.println("Out of range!");
			}
		}while(height <= 0 || height > 20);
		
		switch(ways){ // 用法一或法二來做
			case 1:
				Rectangle rec1 = new Rectangle(width,height); //rec1為存入Rectangle建構值的東西(存取private width 和 height的數據 和 做有參數值建構值裡的動作) 用來建立新的長方形
				rec1.ShowData(); //將長方形的內容輸出
				break;
				
			case 2:
				Rectangle rec2 = new Rectangle(); //rec2為存入Rectangle建構值的東西(只存取private width 和 height的數據 和 作沒參數那個建構值裡的東西) 並建立新的長方形
				rec2.Setwidth(width); //使用Rectangle裡的Setwidth函式 來設定寬
				rec2.Setheight(height);//使用Rectangle裡的Setheight函式 來設定高
				rec2.ShowData(); //將長方形的內容輸出
				break;
			
			default:
				break;			 
		}
	
		input.close(); //關閉input
	}
}
