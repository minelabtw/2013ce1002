package ce1002.e3.s102502511;

public class Rectangle {
	private int width; //長方形的寬
	private int height; //長方形的高
	
	Rectangle(){ 
		
	}//建立一個沒參數的建構值
	
	Rectangle(int w,int h){
		width = w;
		height = h;
	}//建立一個有參數的建構值
	
	void Setwidth(int w){
		width = w;
	}//函式:用來設定寬
	
	void Setheight(int h){
		height = h;
	}//函式:用來設定高
	
	void ShowData(){
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + width*height);
	}//函式:用來顯示要的資料
}
