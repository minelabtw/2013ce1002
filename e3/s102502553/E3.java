package ce1002.e3.s102502553;

import java.util.Scanner;

public class E3 {

	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		int met;
		int widths;
		int heights;
		do//判斷用哪種方法
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameter,2)create by setter:");
			met = input.nextInt();
			if(met > 2 || met < 1)
				System.out.println("Out of range!");
		}
		while(met > 2 || met < 1);
		
		do//輸入寬
		{
			System.out.println("Input the width:");
			widths = input.nextInt();
			if(widths > 20 || widths <= 0)
				System.out.println("Out of range!");
		}
		while(widths > 20 || widths <= 0);
		
		do//輸入高
		{
			System.out.println("Input the height:");
			heights = input.nextInt();
			if(heights > 20 || heights <= 0)
				System.out.println("Out of range!");
		}
		while(heights > 20 || heights <= 0);
		
		if(met == 1)
		{
			Rectangle rectangle = new Rectangle(widths, heights);//建構constructor,直接在constructor裡蒜面積
			System.out.println("The width of this rectangle is " + widths);
			System.out.println("The height of this rectangle is " + heights);
			System.out.println("The area of this rectangle is " + rectangle.Area);
		}
		else
		{
			Rectangle rectangle = new Rectangle();//建構constructor
			rectangle.setWidth(widths);//導入寬度
			rectangle.setHeight(heights);//導入高度
			System.out.println("The width of this rectangle is " + widths);
			System.out.println("The height of this rectangle is " + heights);
			System.out.println("The area of this rectangle is " + rectangle.area);
		}
		
		input.close();
	}

}
