package ce1002.e3.s101201524;
import java.util.Scanner;

public class E3 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int method = 0, w = 0, h = 0;
		Rectangle rectangle;
		//ask the method
		while(method < 1 || method > 2){
			System.out.println("Create rectangle in 2 ways. ");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();
			if(method < 1 || method > 2)
				System.out.println("Out of range!");
		}
		// set the width
		while(w < 1 || w > 20){
			System.out.println("Input the width: ");
			w = input.nextInt();
			if(w < 1 || w > 20)
				System.out.println("Out of range!");
		}
		// set the height
		while(h < 1 || h > 20){
			System.out.println("Input the height: ");
			h = input.nextInt();
			if(h < 1 || h > 20)
				System.out.println("Out of range!");
		}
		if(method == 1)
			rectangle = new Rectangle(w, h);// set the width and height
		else{
			rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
		}
		//show width and height and area
		rectangle.showWidth();
		rectangle.showHeight();
		rectangle.showArea();
	}

}
