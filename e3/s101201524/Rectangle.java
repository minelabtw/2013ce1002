package ce1002.e3.s101201524;

public class Rectangle { 
	private int width;
	private int height;
	
	public Rectangle(){
	}
	public Rectangle(int w, int h){
		width = w;// set the width
		height = h;// set the height
	}
	public void setWidth(int w){
		width = w;// set the width
	}
	public void setHeight(int h){
		height = h;// set the height 
	}
	public void showWidth(){
		System.out.println("The width of this rectangle is " + width);//print width
	}
	public void showHeight(){
		System.out.println("The height of this rectangle is " + height);//print height
	}
	public void showArea(){
		System.out.println("The area of this rectangle is " + (width * height));//print area
	}
}
