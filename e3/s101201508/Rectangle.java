package ce1002.e3.s101201508;

public class Rectangle {
	private int width=1;
	private int height=1;
	Rectangle()//建構子1
	{
		
	}
	Rectangle(int w,int h)//建構子2
	{
		width=w;
		height=h;
	}
	void set_width(int w)//設定寬
	{
		width=w;
	}
	void set_height(int h)//設定高
	{
		height=h;
	}
	int get_width()//回傳寬
	{
		return width;
	}
	int get_height()//回傳高
	{
		return height;
	}
	int get_area()//回傳面積
	{
		return width*height;
	}
	
}
