package ce1002.e3.s101201508;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int way;
		Scanner sc =new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");//決定是方法一還是方法二
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		way=sc.nextInt();
		while (way<1 || way>2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			way=sc.nextInt();
		}
		int w=1;
		int h=1;
		System.out.println("Input the width: ");//決定寬為多少
		w=sc.nextInt();
		while (w<1 || w>20)
		{
			System.out.println("Out of ramge!");
			System.out.println("Input the width: ");
			w=sc.nextInt();
		}
		System.out.println("Input the height: ");//決定高為多少
		h=sc.nextInt();
		while (h<1 || h>20)
		{
			System.out.println("Out of ramge!");
			System.out.println("Input the height: ");
			h=sc.nextInt();
		}
		Rectangle rectangle=new Rectangle();//為了要讓之後還是可以用rectangle，所以要在IF外面宣告
		if (way==1)//使用方法一
		{
			rectangle = new Rectangle(w, h);
		}
		else if (way==2)//使用方法二
		{
			rectangle= new Rectangle();
			rectangle.set_width(w);
			rectangle.set_height(h);
		}
		System.out.println("The width of this rectangle is "+rectangle.get_width());//輸出長、高、面積
		System.out.println("The height of this rectangle is "+rectangle.get_height());
		System.out.println("The area of this rectangle is "+rectangle.get_area());
		
	}

}
