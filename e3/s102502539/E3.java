package s102502539;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int type;
		int width = 0;
		int height = 0;
		Scanner input = new Scanner (System.in);
		
		do
		{
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			type = input.nextInt();
			if ( type != 1 && type != 2 )
				System.out.println("Out of range!");
		} while ( type != 1 && type != 2 );
		
		do
		{
			System.out.println("Input the width: ");
			width = input.nextInt();
			if ( width < 1 || width > 20 )
				System.out.println("Out of range!");
		} while ( width < 1 || width > 20 );
		
		do
		{
			System.out.println("Input the height: ");
			height = input.nextInt();
			if ( height < 1 || height > 20 )
				System.out.println("Out of range!");
		} while ( height < 1 || height > 20 );
		
		if ( type == 1 )	//Method 1
		{
			Rectangle rectangle = new Rectangle( width , height );
			
			System.out.print("The width of this rectangle is " + width
							+ "\nThe height of this rectangle is " + height 
							+ "\nThe area of this rectangle is " + rectangle.getArea( width , height ) );	
		}
		else	//Method 2
		{
			Rectangle rectangle = new Rectangle();
			
			System.out.print("The width of this rectangle is " + width
							+ "\nThe height of this rectangle is " + height 
							+ "\nThe area of this rectangle is " + rectangle.getArea( width , height ) );	
		}

	}

}
