package s102502539;

public class Rectangle {

	private int width;
	private int height;
	
	Rectangle () { }	//Setter
	
	Rectangle ( int x , int y )	//Constructor
	{
		x = 0;
		y = 0;
	}
	
	int getArea( int x , int y)
	{
		return x * y;
	}
	
	void setWidth ( int x )
	{
		width = x;

	}
	
	void setHeight ( int y )
	{
		height = y;

	}

}
