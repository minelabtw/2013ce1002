package ce1002.e3.s102502036;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle() {//default constructer

	}

	public Rectangle(int x, int y) {//parameter constructer
		width = x;
		height = y;
	}

	public int getArea() {

		return width * height;//算面積
	}

	public int returnWidth() {

		return width;//回傳寬
	}

	public int returnHeight() {

		return height;//回傳高
	}

	public void setWidth(int i) {
		width = i;//設定寬
	}

	public void setHeight(int j) {
		height = j;//設定高
	}

}
