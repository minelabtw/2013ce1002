package ce1002.e3.s102502036;
import java.util.Scanner ;
public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int h=1 ;
		int w=1 ;
		int type ;
		int area ;
		Scanner input = new Scanner(System.in) ;
		System.out.println("Create rectangle in 2 ways.") ;
		System.out.println("1)create by constructor parameters,2)create by setter:") ;
		type = input.nextInt() ;
		while(type!=1&&type!=2){   
			System.out.println("Out of range!") ;//依提意輸出
			System.out.println("Create rectangle in 2 ways.") ;
			System.out.println("1)create by constructor parameters,2)create by setter:") ;
			type = input.nextInt() ;
		}
		do{
			if(w<1||w>20){
				System.out.println("Out of range!") ;//依提意輸出
			}
			
			System.out.println("Input the width: ") ;
			w = input.nextInt() ;
			
		}while(w<1||w>20);
		
		do{
			if(h<1||h>20){
				System.out.println("Out of range!") ;//依提意輸出
			}
			
			System.out.println("Input the height: ") ;
			h = input.nextInt() ;
			
		}while(h<1||h>20);
		if(type==1){
			Rectangle rectangle = new Rectangle(w, h);//建立物件並傳入值
			System.out.println("The width of this rectangle is "+rectangle.returnWidth() ) ;//回傳寬
			System.out.println("The height of this rectangle is "+rectangle.returnHeight() ) ;//回傳高
			System.out.println("The area of this rectangle is "+rectangle.getArea() ) ;//回傳面積
		}
		else if(type==2){
			Rectangle rectangle = new Rectangle();//建立物
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.println("The width of this rectangle is "+rectangle.returnWidth() ) ;//回傳寬
			System.out.println("The height of this rectangle is "+rectangle.returnHeight() ) ;//回傳高
			System.out.println("The area of this rectangle is "+rectangle.getArea() ) ;//回傳面積
			
		}
		
		
		
	}

}
