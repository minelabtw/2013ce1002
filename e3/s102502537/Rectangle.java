package ce1002.e3.s102502537;

public class Rectangle {
	private int width;
	private int height;
	
	Rectangle(){
		
	}
	
	Rectangle(int w, int h){
		width = w;
		height = h;
	}
	
	int getArea(){
		return width*height;
	}
	
	void setwidth(int newwidth){
		width = newwidth;
	}
	
	void setheight(int newheight){
		height = newheight;
	}

}
