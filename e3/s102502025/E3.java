package ce1002.e3.s102502025;

import java.util.Scanner;	//開啟有關input工具夾指令

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		//開啟input指令
		
		int choose=0;		//宣告選擇變數
		
		while(choose!=1 && choose!=2)		//規定選擇範圍
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			choose = input.nextInt();
			if(choose!=1 && choose!=2)		//提示超出範圍
			{
				System.out.println("Out of rang!");
			}
		}
		
		int w=0;		//宣告寬度變數
		int h=0;		//宣告高度變數
		
		while(w<1 || w>20)		//規定寬度範圍
		{
			System.out.println("Input the width: ");
			w = input.nextInt();
			if(w<1 || w>20)		//提示超出範圍
			{
				System.out.println("Out of range!");
			}
		}
		
		while(h<1 || h>20)		//規定高度範圍
		{
			System.out.println("Input the height: ");
			h = input.nextInt();
			if(h<1 || h>20)		//提示超出範圍
			{
				System.out.println("Out of range!");
			}
		}
		
		switch(choose)		//做出選擇方法
		{
		case 1:
		{
			Rectangle rectangle = new Rectangle(w, h);		//建構子+參數初始化物件
			System.out.println("The width of this rectangle is " + w);
			System.out.println("The height of this rectangle is " + h);
			System.out.print("The area of this rectangle is " + rectangle.getArea());
			break;
		}
		case 2:
		{
			Rectangle rectangle = new Rectangle();		//setter設定物件
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.println("The width of this rectangle is " + w);
			System.out.println("The height of this rectangle is " + h);
			System.out.print("The area of this rectangle is " + rectangle.getArea());
			break;
		}
		}
		input.close();
	}

}