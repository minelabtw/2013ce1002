package ce1002.e3.s102502025;

	public class Rectangle {		//建立新的class

		private int width;		//建立寬度屬性
		private int height;		//建立高度屬性
		
		public Rectangle(int a, int b)		//方法一
		{
			width = a;
			height = b;
		}
		
		public Rectangle()
		{
			
		}
		
		public void setWidth(int w)		//方法二
		{
			width = w;
		}
		
		public void setHeight(int h)
		{
			height = h;
		}
		
		public int getArea()
		{
			return width*height;
		}
	}
