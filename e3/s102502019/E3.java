package ce1002.e3.s102502019;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.out.print("Create rectangle in 2 ways.\n"+"1)create by constructor parameters,2)create by setter: ");
Scanner input = new Scanner (System.in);
int way= input.nextInt();                  //輸入便宣告變數    
while(way!=1 && way!=2)                    //設定輸入條件
{
	System.out.print("Out of range!\n"+"Create rectangle in 2 ways.\n"+"1)create by constructor parameters,2)create by setter: ");
	way = input.nextInt();
}
System.out.print("Input the width: ");
int w = input.nextInt();
while(w<1 || w>20)                         //設定輸入條件
{
	System.out.print("Out of range!\n"+"Input the width: ");
	w = input.nextInt();
}
System.out.print("Input the height: ");
int h = input.nextInt();
while(h<1 || h>20)                        //設定輸入條件
{
	System.out.print("Out of range!\n"+"Input the height: ");
	h = input.nextInt();
}
System.out.print("The width of this rectangle is "+ w+"\n");
System.out.print("The height of this rectangle is "+h+"\n");
switch (way)                              //選擇方法
{
case 1:
{
	Rectangle rectangle = new Rectangle(w, h);  //建立新物件
	System.out.print("The area of this rectangle is "+rectangle.getarea() );
}
break;
case 2:
{
	Rectangle rectangle = new Rectangle();      //建立新物件
	rectangle.setWidth(w);
	rectangle.setHeight(h);
	System.out.print("The area of this rectangle is "+rectangle.getarea() );
}
}
	}

}
