package ce1002.e3.s102503017;

import java.util.Scanner;

public class E3 
{

	private static Scanner scanner;
	
	public static void main(String[] args)
	{
		int method, num1, num2;
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter");
		scanner = new Scanner(System.in);
		do
		{
			method = scanner.nextInt();
			if(method != 1 && method != 2)
			{
				System.out.println("Out of range!\nCreate rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter");

			}
		}while(method != 1 && method != 2);
		
		System.out.println("input the width:");
		do
		{
			num1 = scanner.nextInt();
			if(num1 < 1 || num1 > 20)
			{
				System.out.println("Out of range!\ninput the width:");
			}
		}while(num1 < 1 || num1 > 20);
			
		System.out.println("input the height:");
		do
		{
			num2 = scanner.nextInt();
			if(num2 < 1 || num2 > 20)
			{
				System.out.println("Out of range!\ninput the height:");
			}
		}while(num2 < 1 || num2 > 20);

		System.out.println("The width of this rectangle is " + num1);
		System.out.println("The height of this rectangle is " + num2);
		
		
		if(method == 1)
		{
			Rectangle rectangle = new Rectangle(num1, num2);
			System.out.print(rectangle.getArea());
			System.out.println("The area of this rectangle is " + rectangle.getArea());

		}
		else
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(num1);
			rectangle.setHeight(num2);
			System.out.println("The area of this rectangle is " + rectangle.getArea());		
		}		
	}

}
