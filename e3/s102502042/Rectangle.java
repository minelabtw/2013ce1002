package ce1002.e3.s102502042;

public class Rectangle {
	
	private int width;
	private int height;
	
	/***************
	 * constructor *
	 ***************/
	public Rectangle(){
		
	}
	
	public Rectangle(int w, int h)
	{
		width = w;
		height = h;
	}
	
	/*****************
	 * return method *
	 *****************/
	public int getArea()
	{
		return width * height;
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	/*************
	 *  setter	 *
	 *************/
	public void setWidth(int n)
	{
		width = n;
	}
	
	public void setHeight(int n)
	{
		height = n;
	}
}
