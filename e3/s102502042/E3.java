package ce1002.e3.s102502042;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);	//scanner
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		int num = input.nextInt();
		int w, h;
		while(num != 1 && num != 2)	//input type
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			num = input.nextInt();
		}
		System.out.println("Input the width: ");
		w = input.nextInt();	//input width
		while(w < 1 || w > 20)	
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w = input.nextInt();
		}
		
		System.out.println("Input the height: ");
		h = input.nextInt();	//input height
		while(h < 1 || h > 20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			h = input.nextInt();
		}
		//output the answer ~~~~!!!!!
		if(num == 1)
		{
			Rectangle r = new Rectangle(w, h);
			System.out.println("The width of this rectangle is " + r.getWidth());
			System.out.println("The height of this rectangle is " + r.getHeight());
			System.out.println("The area of this rectangle is " + r.getArea());
		}
		else
		{
			Rectangle r = new Rectangle();
			r.setWidth(w);
			r.setHeight(h);
			System.out.println("The width of this rectangle is " + r.getWidth());
			System.out.println("The height of this rectangle is " + r.getHeight());
			System.out.println("The area of this rectangle is " + r.getArea());
		}
		input.close();	//close
		
	}

}
