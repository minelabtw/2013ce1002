package ce1002.e3.ta;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle() {
	}

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}

	public int getWidth() {// getter of width
		return width;
	}

	public void setWidth(int width) {// setter of width
		this.width = width;
	}

	public int getHeight() {// getter of height
		return height;
	}

	public void setHeight(int height) {// setter of height
		this.height = height;
	}

	public int getArea() {// 計算並回傳面積
		return this.width * this.height;
	}

}
