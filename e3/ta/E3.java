package ce1002.e3.ta;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int createWay = 0;
		String msg = "Create rectangle in 2 ways.\n"
				+ "1)create by constructor parameters,2)create by setter: ";// 兩種不同的建立物件方式
		createWay = GetInt(input, msg, 1, 2);//取得使用者輸入的函式
		Rectangle rectangle = null;
		int width = GetInt(input, "Input the width: ", 1, 20);
		int height = GetInt(input, "Input the height: ", 1, 20);
		input.close();// 用不到了，要關掉
		switch (createWay) {
		case 1:// constructor parameters
			rectangle = new Rectangle(width, height);
			break;
		case 2:// setter
			rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			break;
		default:
			rectangle = new Rectangle(5, 10);// 避免null pointer
			break;
		}
		System.out.println("The width of this rectangle is "
				+ rectangle.getWidth());
		System.out.println("The height of this rectangle is "
				+ rectangle.getHeight());
		System.out.println("The area of this rectangle is "
				+ rectangle.getArea());// output the area

	}

	public static int GetInt(Scanner input, String msg, int lowerbound,
			int upperbound) {
		int value = 0;
		do {
			System.out.println(msg);
			value = input.nextInt();
			if (value > upperbound || value < lowerbound)
				System.out.println("Out of range!");
		} while (value > upperbound || value < lowerbound);
		return value;
	}

}
