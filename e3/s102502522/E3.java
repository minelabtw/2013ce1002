package ce1002.e3.s102502522;
import java.util.Scanner;
import ce1002.e3.s102502522.Rectangle;
public class E3 {
	public static void main(String[] args) {
		
	Scanner scanner = new Scanner(System.in);
	int mode;
	int w;
	int h;
	
	System.out.println("reate rectangle in 2 ways.");
	System.out.println("1)create by constructor parameters,2)create by setter:");
	mode = scanner.nextInt();
	while(mode<=0 || mode>=3)
	{
		System.out.println("Out of range!");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		mode = scanner.nextInt();
	}
	System.out.println("Input the width");
	w = scanner.nextInt();
	while(w<=0)
	{
		System.out.println("Out of range!");
		System.out.println("Input the width");
		w = scanner.nextInt();
	}
	System.out.println("Input the height");
	h = scanner.nextInt();
	while(h<=0)
	{
		System.out.println("Out of range!");
		System.out.println("Input the height");
		h = scanner.nextInt();
	}
	
	if(mode==1)
	{
		Rectangle rectangle = new Rectangle(w, h);
		System.out.println("The width of this rectangle is "+rectangle.gotwidth());
		System.out.println("The height of this rectangle is "+rectangle.gotheight());
		System.out.println("The area of this rectangle is "+rectangle.gotarea());
	}
	else
	{
		Rectangle rectangle = new Rectangle();
		rectangle.setwidth(w);
		rectangle.setheight(h);
		System.out.println("The width of this rectangle is "+rectangle.gotwidth());
		System.out.println("The height of this rectangle is "+rectangle.gotheight());
		System.out.println("The area of this rectangle is "+rectangle.gotarea());
	}
	
	
	

	
	}
	
	}
