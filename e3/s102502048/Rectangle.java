package ce1002.e3.s102502048;

public class Rectangle 
{
	int width=0,height=0;
	public Rectangle()
	{
		
	}
	public Rectangle(int w, int h)
	{
		width=w;
		height=h;
	}
	public void setWidth(int w)
	{
		width=w;
	}
	public void setHeight(int h)
	{
		height=h;
	}
	public int getArea()
	{
		return width*height;
	}
}
