package ce1002.e3.s102502048;
import java.util.Scanner;
public class E3 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int mode=0,width=0,height=0;
		
		System.out.println("Create rectangle in 2 ways.");//顯示
		do
		{
			System.out.println("1)create by constructor parameters,2)create by setter: ");//顯示
			mode = input.nextInt();//輸入		
			if(mode !=1 && mode != 2)
				System.out.println("Out of range!");//顯示
		}while(mode !=1 && mode != 2);
		
		do
		{
			System.out.println("Input the width: ");//顯示
			width = input.nextInt();//輸入		
			if(width <1 || width >20)
				System.out.println("Out of range!");//顯示
		}while(width <1 || width >20);
		
		do
		{
			System.out.println("Input the height: ");//顯示
			height = input.nextInt();//輸入		
			if(height <1 || height >20)
				System.out.println("Out of range!");//顯示
		}while(height <1 || height >20);
		if(mode==1)
		{
			Rectangle rec = new Rectangle(width,height);
			System.out.println("The width of this rectangle is "+width);//顯示
			System.out.println("The height of this rectangle is "+height);//顯示
			System.out.println("The area of this rectangle is "+rec.getArea());//顯示
		}
		else
		{
			Rectangle rec = new Rectangle();
			rec.setHeight(height);
			rec.setWidth(width);
			System.out.println("The width of this rectangle is "+width);//顯示
			System.out.println("The height of this rectangle is "+height);//顯示
			System.out.println("The area of this rectangle is "+rec.getArea());//顯示
		}
		
	}
}
