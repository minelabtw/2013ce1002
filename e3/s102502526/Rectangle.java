package ce1002.e3.s102502526;

public class Rectangle {
	private int width;
	private int height;
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	
	Rectangle(int h, int w){
       height=h;
       width=w;
	}     //法一，將輸入的長寬值丟進規定的建構子裡面
	Rectangle(){
		
	}     //法二，建構子都在主函式裡面
	
	public int getArea()    //計算面積
	{
		return width * height;
	}
	
}
