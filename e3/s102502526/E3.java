package ce1002.e3.s102502526;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int h;
		int w;
		int a;
		
		System.out.print("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
		a = scanner.nextInt();
		while(a<1 || a>2)
		{
			System.out.print("Out of range!\n");
			System.out.print("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
			a = scanner.nextInt();
		}
		
		
		
			System.out.print("Input the width: ");    //輸入寬
			w = scanner.nextInt();
			while(w>20 || w<1)
			{
				System.out.print("Out of range!\nInput the width: ");
				w = scanner.nextInt();
			}
			System.out.print("Input the height: ");   //輸入長
			h = scanner.nextInt();
			while(h>20 || h<1)
			{
				System.out.print("Our of range!\nInput the height: ");
				h = scanner.nextInt();
			}
			
			if (a == 1)                 //判斷方法  = 1
			{
				Rectangle rectangle = new Rectangle(w,h);
				System.out.print("The width of this rectangle is "+rectangle.getWidth()+"\n");
				System.out.print("The height of this rectangle is "+rectangle.getHeight()+"\n");
				System.out.print("The area of this rectangle is "+rectangle.getArea()+"\n");
			}
			else if (a == 2)            //判斷方法  = 2
			{
                Rectangle rectangle = new Rectangle();
			    rectangle.setWidth(w);
			    rectangle.setHeight(h);
			    System.out.print("The width of this rectangle is "+rectangle.getWidth()+"\n");
				System.out.print("The height of this rectangle is "+rectangle.getHeight()+"\n");
				System.out.print("The area of this rectangle is "+rectangle.getArea()+"\n");
			}
			scanner.close();
			
	}

}
