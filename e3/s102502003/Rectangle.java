package ce1002.e3.s102502003;

public class Rectangle {

	private int width;
	private int height;
	
	public Rectangle(int w, int h){
		width=w;
		height=h;
	}
	
	public Rectangle(){
		
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getArea(){
		int area=0;
		area=width * height;
		return area;
	}

}
