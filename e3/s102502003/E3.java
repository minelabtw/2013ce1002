package ce1002.e3.s102502003;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		
		int w=0;
		int h=0;
		int choose=0;
		
		System.out.println("Create rectangle in 2 ways.");
		
		while(choose!=1&&choose!=2){
			
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			choose=scanner.nextInt();
			
			switch(choose)
			{
			case 1:
				do
				{
					System.out.println("Input the width: ");
					w=scanner.nextInt();
					if(w>20||w<1)
						System.out.println("Out of range!");					
				}while(w>20||w<1);
				
				do
				{
					System.out.println("Input the height: ");
					h=scanner.nextInt();
					if(h>20||h<1)
						System.out.println("Out of range!");
				}while(h>20||h<1);
				
				Rectangle rectangle = new Rectangle(w,h);
				System.out.println("The width of this rectangle is "+w);
				System.out.println("The height of this rectangle is "+h);
				System.out.println("The area of this rectangle is "+rectangle.getArea());
				break;
				
			case 2:
				do
				{
					System.out.println("Input the width: ");
					w=scanner.nextInt();
					if(w>20||w<1)
						System.out.println("Out of range!");					
				}while(w>20||w<1);
				
				do
				{
					System.out.println("Input the height: ");
					h=scanner.nextInt();
					if(h>20||h<1)
						System.out.println("Out of range!");
				}while(h>20||h<1);
				
				Rectangle r = new Rectangle();
				r.setWidth(w);
				r.setHeight(h);
				
				System.out.println("The width of this rectangle is "+r.getWidth());
				System.out.println("The height of this rectangle is "+r.getHeight());
				System.out.println("The area of this rectangle is "+r.getArea());
				break;
				
				
			default:
					System.out.println("Out of range!");
					break;
					
			}
		
		}


	}

}
