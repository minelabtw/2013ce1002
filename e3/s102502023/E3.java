package ce1002.e3.s102502023;
import java.util.Scanner;


public class E3 {
    public static void main(String[] args) {
    Rectangle r = new Rectangle(); // initialize class Rectangle	
	int num = 0; // initialize num to 0	
    Scanner input = new Scanner(System.in);
    
    do {
    	System.out.print("Create rectangle in 2 ways.\n" + 
    			"1)create by constructor parameters,2)create by setter:\n");
    	num = input.nextInt();
    	if(num != 1 && num != 2)
    		System.out.print("Out of range!\n");
    }
    while(num != 1 && num != 2); // do... while loop to check whether num is on demand
    
    r.info();
    r.select(num);
    
    System.out.print("The width of this rectangle is " + r.getWidth() +
    		"\nThe height of this rectangle is " + r.getHeight() +
    		"\nThe area of this rectangle is " + r.getArea());
    
    input.close();
}
}

