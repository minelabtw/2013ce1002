package ce1002.e3.s102502023;

import java.util.Scanner;

class Rectangle {
	Scanner input = new Scanner(System.in);
	private int width = 0; // initialize width to 0
    private int height = 0; // initialize height to 0
    private int area = 0; // initialize area to 0
    
    public void info() {
    	do {
    	System.out.print("Input the width:\n");
    	width = input.nextInt();
    	if(width <= 0 || width > 20)
    		System.out.print("Out of range!\n");
    	}while(width <= 0 || width > 20); // do... while loop to check whether width is on demand
    	
    	do {
    		System.out.print("Input the height:\n");
    		height = input.nextInt();
    		if(height <= 0 || height > 20)
    			System.out.print("Out of range!\n");
    	}while(height <= 0 || height > 20);
    }
    
    public void select(int n) {
    	if(n == 1)
    		setArea(width, height);
    	else if(n == 2)
    		{
    		 setWidth(width);
    		 setHeight(height);
    		 setArea(width, height);
    		}
    } // select mode
	
    public Rectangle() {} // constructor
    
    public void setArea(int w, int h) {
    	area = w * h;
    }
    
    private int setWidth(int w) {
    	w = width; 
    	return w; 	
    }
    
    private int setHeight(int h) {
    	h = height;
    	return h;
    }    
    
    public int getArea() {
    	return area;
    }
    public int getWidth() {
    	return width;
    }
    
    public int getHeight() {
    	return height;
    }
    
}
