package ce1002.e3.s102502505;
import java.util.Scanner;

public class e3 {
	
	public static int getArea(int i1,int i2)//把長寬回傳至此，並計算面積值
	{
		int area=0;//定義初始值
		area=i1*i2;//計算面積
		return area;//回傳
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int choice;//定義選擇方法的變數
		int width;//定義寬
		int height;//定義高
		Scanner scanner = new Scanner(System.in);//定義輸入
		
		
		System.out.println("Create rectangle in 2 ways.");//輸出字串
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		choice=scanner.nextInt();//輸入數值
		while(choice!=1&&choice!=2)//利用迴圈來限制範圍
		{
			System.out.println("Out of range!");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			choice=scanner.nextInt();
		}
		
		if(choice==1)//當選擇方法1時
		{
			Rectangle rectangle = new Rectangle(5, 10);//使用建構子，並設置初始值
		}
		
		else//其餘則選擇方法2
		{
			Rectangle rectangle1 = new Rectangle();
			rectangle1.setWidth(5);// set the width
			rectangle1.setHeight(10);// set the height
		}
		
		System.out.println("Input the width: ");//輸出下列字
		width=scanner.nextInt();//輸入寬
		while(width>20||width<0)//限制範圍
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width=scanner.nextInt();
		}
		
		System.out.println("Input the height: ");
		height=scanner.nextInt();
		while(height>20||height<0)
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			height=scanner.nextInt();
		}
		
		System.out.println("The width of this rectangle is "+width);//輸出字串
		System.out.println("The height of this rectangle is "+height);
		System.out.println("The area of this rectangle is "+getArea(width,height));//輸出字串並呼叫getArea()
	}

}
