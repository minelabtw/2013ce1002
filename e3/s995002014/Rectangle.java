package ce1002.e3.s995002014;

public class Rectangle {
	private int width;
	private int height;
	
	public int getArea(){
		return width*height;
	}
	public void setWidth(int x) {
		width=x;
	}
	public void setHeight(int x){
		height=x;
	}
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	Rectangle(){
		
	}
	Rectangle(int x,int y){
		width=x;
		height=y;
	}
}

