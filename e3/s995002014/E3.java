package ce1002.e3.s995002014;

import java.util.Scanner;

public class E3 {
	public static void main(String[] args) {
		Scanner input = new Scanner( System.in );
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
		int mode,width,height;
		
		//ㄏノ匡拒家Α
		mode = input.nextInt();
		while( mode < 1 || mode > 10 )
		{
			System.out.println( "Out of range!");
			System.out.println( "Create rectangle in 2 ways\n.1)create by constructor parameters,2)create by setter:");
			mode = input.nextInt();
		}
		
		//よ猭1 ㄏノ把计篶
		if(mode==1){ 
			System.out.println("Input the width:");
			width=input.nextInt();
			while( width < 1 || width> 20 )
			{
				System.out.println( "Out of range!" );
				System.out.println( "Input the width:");
				width = input.nextInt();
			}
			
			System.out.println( "Input the height:");
			height=input.nextInt();
			while( height < 1 || height> 20 )
			{
				System.out.println( "Out of range!" );
				System.out.println( "Input the height:");
				height = input.nextInt();
			}
			Rectangle rectangle = new Rectangle(width, height); //把计肚篶
			System.out.println("The width of this rectangle is "+rectangle.getWidth());
			System.out.println("The height of this rectangle is "+rectangle.getHeight());
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
		
		//よ猭2  ㄏノフ篶ㄏノ摸よ猭砞﹚糴
		else if (mode==2) {
			System.out.println("Input the width:");
			width=input.nextInt();
			while( width < 1 || width> 20 )
			{
				System.out.println( "Out of range!" );
				System.out.println( "Input the width:");
				width = input.nextInt();
			}
			
			System.out.println( "Input the height:");
			height=input.nextInt();
			while( height < 1 || height> 20 )
			{
				System.out.println( "Out of range!" );
				System.out.println( "Input the height:");
				height = input.nextInt();
			}
			Rectangle rectangle = new Rectangle();//フ篶
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
	}
}
