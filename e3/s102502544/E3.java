package ce1002.e3.s102502544;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int select=0;//變數
		int width=0;
		int height=0;
		
		System.out.println("Create rectangle in 2 ways.");//輸出
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		select = input.nextInt();//輸入
		while(select!=1 && select!=2){//設定只能輸出1和2
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			select = input.nextInt();
		}
		System.out.println("Input the width: ");
		width = input.nextInt();
		while(width<=0 || width>20){//設定範圍
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width = input.nextInt();
		}
		System.out.println("Input the height: ");
		height = input.nextInt();
		while(height<=0 || height>20){
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			height = input.nextInt();
		}
		switch(select){//2種方法
		case 1:
			Rectangle rectangle1 = new Rectangle(width,height);
			rectangle1.Output();
			break;
		case 2:
			Rectangle rectangle2 = new Rectangle();
			rectangle2.setWidth(width);
			rectangle2.setHeight(height);
			rectangle2.Output();
			break;
		}

	}

}
