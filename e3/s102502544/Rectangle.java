package ce1002.e3.s102502544;

public class Rectangle {
	
	private int width;
	private int height;
	
	Rectangle(){ //建構子
		
	}
	Rectangle(int a,int b){
		width = a;
		height = b;
	}
	void setWidth(int x){
		width = x;
	}
	void setHeight(int y){
		height = y;
	}
	int Areaget(){
		return width*height;
	}
	void Output(){ //輸出最後資料
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.print("The area of this rectangle is " + Areaget());
	}

}
