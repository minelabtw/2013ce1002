package ce1002.e3.s102502542;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways." + "\n"
				+ "1)create by constructor parameters,2)create by setter: ");// 輸出
		int number = input.nextInt();// 輸入
		while (number != 1 && number != 2) {
			System.out.println("Out of range!");
			System.out
					.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			number = input.nextInt();
		} // 當(number不等於1也不等於2時候進入迴圈)
		if (number == 2) //方案2
		{
			Rectangle rectangle = new Rectangle();
			System.out.println("Input the width: ");
			int width = input.nextInt();
			while (width < 1 || width > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = input.nextInt();
			}
			rectangle.setwidth(width);
			System.out.println("Input the height: ");
			int height = input.nextInt();
			while (height < 1 || height > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = input.nextInt();
			}
			rectangle.setheight(height);
			System.out.println("The width of this rectangle is "
					+ rectangle.getwidth());
			System.out.println("The height of this rectangle is "
					+ rectangle.getheight());
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());
		} 
		else if (number == 1) //方案1
		{
			System.out.println("Input the width: ");
			int width = input.nextInt();
			while (width < 1 || width > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = input.nextInt();
			}
			System.out.println("Input the height: ");
			int height = input.nextInt();
			while (height < 1 || height > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = input.nextInt();
			}
			Rectangle rectangle2 = new Rectangle(width, height);
			System.out.println("The width of this rectangle is "
					+ rectangle2.getwidth());
			System.out.println("The height of this rectangle is "
					+ rectangle2.getheight());
			System.out.println("The area of this rectangle is "
					+ rectangle2.getArea());
		}

		// TODO Auto-generated method stub

	}

}
