package ce1002.e3.s984008030;
import java.util.Scanner;

import ce1002.e3.s984008030.Rectangle;

public class E3 {

	public static Boolean isOutOfRange(int input){//浪琩input琌禬絛瞅
		if(input < 1 || input > 20)
			return true;
		else
			return false;
	}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);//Scanner篈scannerン ﹍て
		Rectangle rectangle;//Rectangle篈痻
		int type = 1;//int篈篶よΑ ﹍て1
		int width = 1, height = 1;//int篈㎝糴 ﹍て1
		while (true) {//眔篶よ猭
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			type = scanner.nextInt();
			if (type == 1 || type == 2) {
				break;
			}
			else {
				System.out.println("Out of range!");
			}
		}
		
		while (true) {//眔
			System.out.println("Input the width: ");
			width = scanner.nextInt();
			if (isOutOfRange(width)) {
				System.out.println("Out of range!");
			}
			else {
				break;
			}
		}
		
		while (true) {//眔糴
			System.out.println("Input the height: ");
			height = scanner.nextInt();
			if (isOutOfRange(height)) {
				System.out.println("Out of range!");
			}
			else {
				break;
			}
		}
		
		if (type == 1) {//ㄏノ盿把计篶Α
			rectangle = new Rectangle(width,height);
		}
		else {//ㄏノぃ盿把计篶Α ノsetter﹍て跑计
			rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
		}
		
		//块挡狦
		System.out.println("The width of this rectangle is " + rectangle.getWidth());
		System.out.println("The height of this rectangle is " + rectangle.getHeight());
		System.out.println("The area of this rectangle is " + rectangle.getArea());
	}

}
