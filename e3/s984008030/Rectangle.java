package ce1002.e3.s984008030;

public class Rectangle {
	private int width;//長
	private int height;//寬
	
	public Rectangle(int _width, int _height) {//帶參數建構式
		this.width = _width;
		this.height = _height;
	}
	
	public Rectangle() {//不帶參數建構式
		this(1, 1);
	}
	
	public void setWidth(int _width){//設定長
		this.width = _width;
	}
	
	public int getWidth() {//取得長
		return this.width;
	}
	
	public void setHeight(int _height){//設定寬
		this.height = _height;
	}
	
	public int getHeight() {//取得寬
		return this.height;
	}
	
	public int getArea() {//取得面積
		return this.width * this.height;
	}
}
