package ce1002.e3.s102502536;

import java.util.Scanner;

public class E3 {
	
	public static Scanner input;
	
	public static void main(String[] args) {
		  //宣告變數
		int width;
		int height;
		int way;
		
		input = new Scanner(System.in);
		  //輸出字串
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
		  //設定範圍
		do
		{
			way = input.nextInt();
			
			if (way != 1 && way != 2)
				System.out.println("Out of range!\nCreate rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
			
		} while (way != 1 && way != 2);
		  //方法一:使用建構子+參數初始化物件
		if (way == 1)
		{
			System.out.println("Input the width:");
			 
			do
			{
				width = input.nextInt();
				
				if (width < 1 || width > 20)
					System.out.println("Out of range!\nInput the width:");
				
			} while (width < 1 || width > 20);
			
			System.out.println("Input the height:");
			 
			do
			{
				height = input.nextInt();
				
				if (height < 1 || height > 20)
					System.out.println("Out of range!\nInput the height:");
				
			} while (height < 1 || height > 20);
			
			Rectangle F_rectangle = new Rectangle(width , height);
			
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + F_rectangle.getArea());
		}
	      //方法二:使用setter設定物件
		if (way == 2)
		{
			System.out.println("Input the width:");
			 
			do
			{
				width = input.nextInt();
				
				if (width < 1 || width > 20)
					System.out.println("Out of range!\nInput the width:");
				
			} while (width < 1 || width > 20);
			
			System.out.println("Input the height:");
			 
			do
			{
				height = input.nextInt();
				
				if (height < 1 || height > 20)
					System.out.println("Out of range!\nInput the height:");
				
			} while (height < 1 || height > 20);
			
			Rectangle S_rectangle = new Rectangle();
			S_rectangle.setWidth(width);
			S_rectangle.setHeight(height);
			
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + S_rectangle.getArea());
		}
	}

}
