package ce1002.e3.s102502038;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import ce1002.e3.s102502038.Rectangle;

public final class E3 {
	public static void main(String[] args){
		int num;
		String inputln;
		// input section
		while(true){
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				num = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(num != 1 && num != 2){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		// end of input section
		int mode = num;
		// input section
		num = 0;
		while(true){
			System.out.println("Input the width: ");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				num = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(num <= 0){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		int wid = num;
		// end of input section
		// input section
				num = 0;
				while(true){
					System.out.println("Input the height: ");
					try{
						BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
						inputln = buff.readLine();
						num = Integer.parseInt(inputln);
					}catch(IOException error){
						System.out.println("Input Section Error: " + error);
						continue;
					}catch(java.lang.NumberFormatException error){
						System.out.println("Input Section Error: " + error);
						continue;
					}
					if(num <= 0){
						System.out.println("Out of range!");
						continue;
					}
					break;
				}
				int hei = num;
				// end of input section
		if(mode == 1){//by constructor parameters
			Rectangle rectangle = new Rectangle(wid, hei);
			System.out.println("The width of this rectangle is " + wid);
			System.out.println("The height of this rectangle is "+ hei);
			System.out.println("The area of this rectangle is "+ rectangle.getScale());
		}
		if(mode == 2){//by setter
			final Rectangle rectangle = new Rectangle();
			rectangle.setWidth(wid);// set the width
			rectangle.setHeight(hei);// set the height
			System.out.println("The width of this rectangle is " + wid);
			System.out.println("The height of this rectangle is "+ hei);
			System.out.println("The area of this rectangle is "+ rectangle.getScale());
		}
	}
}
