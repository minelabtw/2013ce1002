package ce1002.e3.s102502038;

public class Rectangle {
	private int width;
	private int height;
	Rectangle(){
		width = 0;
		height = 0;
	}
	Rectangle(int wid,int hei){
		setWidth(wid);
		setHeight(hei);
	}
	public void setWidth(int wid){
		width = wid;
	}
	public void setHeight(int hei){
		height = hei;
	}
	public int getScale(){
		return width*height;
	} 
}
