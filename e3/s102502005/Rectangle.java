package ce1002.e3.s102502005;

public class Rectangle {
	private int width;
	private int height;
	private int area;//面積。

	public Rectangle() {

	}

	public Rectangle(int w, int h) {
		width = w;
		height = h;
	}

	//兩個setter。
	void setWidth(int w) {
		width = w;
	}

	void setHeight(int h) {
		height = h;
	}

	//三個get分別傳回面積、寬、高。
	int getArea() {
		area = width * height;
		return area;
	}

	int getWidth() {
		return width;
	}

	int getHeight() {
		return height;
	}

}
