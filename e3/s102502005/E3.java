package ce1002.e3.s102502005;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int method,w,h;//宣告暫存用的長和寬
		Scanner input = new Scanner(System.in);
		Rectangle rectangle;//矩形物件的位址。
		
		System.out.println("Create rectangle in 2 ways.");//要求使用者選擇建立矩形物件的方法
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		method = input.nextInt();
		while(method != 1 && method !=2){
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();
		}
		
		System.out.println("Input the width: ");//要求使用者輸入寬。
		w = input.nextInt();
		while(w<1 || w>20){
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w = input.nextInt();
		}
		
		System.out.println("Input the height: ");//要求使用者輸入高。
		h = input.nextInt();
		while(h<1 || h>20){
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			h = input.nextInt();
		}
		
		switch(method){
		case 1: rectangle = new Rectangle(w, h);//利用第一個方法建立新的矩形物件並利用建構子建立高和寬的值。
			break;
		
		case 2:rectangle = new Rectangle();//利用第二個方法建立新的矩形物件，並用setter建立高和寬的值。
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);// set the height
			break;
		
		default:
			rectangle = new Rectangle();//這裡要寫一個new，不寫的話java會判定你有可能不會建立新的rectangle。
			break;
		}
		
		System.out.println("The width of this rectangle is " + rectangle.getWidth());//輸出結果。
		System.out.println("The height of this rectangle is " + rectangle.getHeight());
		System.out.println("The area of this rectangle is " + rectangle.getArea());
	}
}

