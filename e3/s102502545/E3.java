package ce1002.e3.s102502545;

import java.util.Scanner;

import ce1002.e3.s102502545.Rectangle;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int mod;

		do {/* 限定變數的範圍 */
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter:");
			mod = input.nextInt();
			if (mod != 1 && mod != 2)
				System.out.println("Out of range!");
		} while (mod != 1 && mod != 2);

		int width;

		do {/* 限定變數的範圍 */
			System.out.println("Input the width:");
			width = input.nextInt();
			if (width < 1 || width > 20)
				System.out.println("Out of range!");
		} while (width < 1 || width > 20);

		int height;

		do {/* 限定變數的範圍 */
			System.out.println("Input the height:");
			height = input.nextInt();
			if (height < 1 || height > 20)
				System.out.println("Out of range!");
		} while (height < 1 || height > 20);

		if(mod == 1)//利用constructor方法去設定width,height
		{
			Rectangle apple = new Rectangle(width,height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			apple.getArea();
		}
		else//利用method的方法去設定width,height
		{
			Rectangle apple = new Rectangle();
			apple.setWidth(width);// set the width
			apple.setHeight(height);// set the height
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			apple.getArea();
		}
		
		
	}
}
