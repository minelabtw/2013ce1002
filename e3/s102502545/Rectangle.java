package ce1002.e3.s102502545;

public class Rectangle {

	private int width;
	private int height;

	public Rectangle() {
	}

	public Rectangle(int w, int h) {
		width = w;
		height = h;

	}

	public void setWidth(int w) {
		width = w;
	}

	public void setHeight(int h) {
		height = h;
	}

	public void getArea() {
		System.out.println("The area of this rectangle is "+ width*height); 
	}
}
