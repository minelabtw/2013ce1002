package ce1002.e3.s102502050;
import java.util.Scanner;
public class E3 {


	public static void main(String[] args) {
		int ways;
		int width,height;
		
		Scanner scanner = new Scanner (System.in);
		do
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			ways = scanner.nextInt();
			if(ways!=1 && ways!=2)
			{
				System.out.println("Out of range!");				
			}
		}while(ways!=1 && ways!=2);//輸入選擇的方法並檢查
		
		do
		{
			System.out.println("Input the width:");
			width = scanner.nextInt();
			if(width<=0 || width >20)
			{
				System.out.println("Out of range!");				
			}
		}while(width<=0 || width >20);//輸入寬並檢查
		
		do
		{
			System.out.println("Input the height:");
			height = scanner.nextInt();
			if(height<=0 || height >20)
			{
				System.out.println("Out of range!");				
			}
		}while(height<=0 || height >20);//輸入長並檢查
		
		
		if(ways==1)//依不同方法建立 rectangle 並輸出
		{
			Rectangle rectangle = new Rectangle();
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + rectangle.getArea() );
		}
		else
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setHeight(height);
			rectangle.setWidth(width);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + rectangle.getArea() );
		}
		

		
		
	}

}
