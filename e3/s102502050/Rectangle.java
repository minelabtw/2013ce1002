package ce1002.e3.s102502050;

public class Rectangle {

	private int height;
	private int width;
	public Rectangle()
	{
		
	}
	public Rectangle(int inwidth,int inheight)
	{
		width=inwidth;
		height=inheight;
	}
	public void setWidth(int number)
	{
		width=number;
	}
	public void setHeight(int number)
	{
		height=number;
	}
	public int getArea()
	{
		return ( height * width);
	}
	

}
