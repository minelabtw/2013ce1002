package ce1002.e3.s102502006;

import java.util.Scanner;
// import ce1002.e3.s102502006.Rectangle;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			int mode=0;
			int width=0,height=0;
			
			Scanner scanner= new Scanner(System.in);
			
			
			
			
			while(mode!=1 && mode!=2)
			{
				
				System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
				mode = scanner.nextInt();
				if(mode!=1 && mode!=2)System.out.println("Out of range!");
			}
			
			while(width<1 || width>20)
			{
				System.out.println("Input the width: ");
				width=scanner.nextInt();
				if(width<1 || width>20)System.out.println("Out of range!");
			}
			
			while(height<1 || height>20)
			{
				System.out.println("Input the height: ");
				height=scanner.nextInt();
				if(height<1 || height>20)System.out.println("Out of range!");
			}
			
			switch(mode){
			case 1:
				Rectangle rectangle1= new Rectangle(width,height);
				rectangle1.getArea();
				break;
				
			case 2:
				Rectangle rectangle2= new Rectangle();
				rectangle2.setWidth(width);
				rectangle2.setHeight(height);
				rectangle2.getArea();
				break;
				
			default:
				break;
			
			}
	}

}
