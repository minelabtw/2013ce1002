package ce1002.e3.s100502022;

import java.util.Scanner;

public class E3 {
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		int way;
		//way check
		do{
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			way=s.nextInt();
			if(way!=1&&way!=2){
				System.out.println("Out of range! ");
			}
		}while(way!=1&&way!=2);
		//width check
		int width;
		do{
			System.out.println("Input the width: ");
			width=s.nextInt();
			if(width<1||width>20){
				System.out.println("Out of range! ");
			}
		}while(width<1||width>20);
		//height check
		int height;
		do{
			System.out.println("Input the height: ");
			height=s.nextInt();
			if(height<1||height>20){
				System.out.println("Out of range! ");
			}
		}while(height<1||height>20);	
		//constructor choose
		Rectangle r ;
		if(way==1)
		{
			//set value in constructor
			r=new Rectangle(width,height);
		}
		else
		{
			//construct by set method
			r=new Rectangle();
			r.setheight(height);
			r.setwidth(width);
		}
		//print out
		System.out.println("The width of this rectangle is "+r.getwidth());
		System.out.println("The height of this rectangle is "+r.getheight());
		System.out.println("The area of this rectangle is "+r.getArea());
	}
}
