package ce1002.e3.s100502022;

public class Rectangle {
	private int width;
	private int height;
	//constructor
	public Rectangle(){
		this(0, 0);
	}
	public Rectangle(int w ,int h){
		this.width=w;
		this.height=h;
	}
	//set
	public void setwidth(int w){
		this.width=w;
	}
	public void setheight(int h){
		this.height=h;
	}
	//get
	public int getwidth(){
		return width;
	}
	public int getheight(){
		return height;
	}
	public int getArea(){
		return width*height;
	}
}
