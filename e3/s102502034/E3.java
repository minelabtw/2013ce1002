package ce1002.e3.s102502034;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		int choice, width, height;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Create rectangle in 2 ways.");
		System.out
				.println("1)create by constructor parameters,2)create by setter:");

		choice = scanner.nextInt();
		while (choice != 1 && choice != 2) {
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter:");
			choice = scanner.nextInt();
		}
		// 輸入輸出題目所需
		if (choice == 1) {
			System.out.println("Input the width: ");
			width = scanner.nextInt();
			while (width < 1 || width > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = scanner.nextInt();
			}
			System.out.println("Input the width: ");
			height = scanner.nextInt();
			while (height < 1 || height > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = scanner.nextInt();
			}
			Rectangle way1 = new Rectangle(width, height);// 建立物件 並且設定參數
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ way1.getArea());
			// 輸出結果
		}
		// 方法一
		else if (choice == 2) {
			System.out.println("Input the width: ");
			width = scanner.nextInt();
			while (width < 1 || width > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = scanner.nextInt();
			}
			System.out.println("Input the height: ");
			height = scanner.nextInt();
			while (height < 1 || height > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = scanner.nextInt();
			}
			Rectangle way2 = new Rectangle();// 建造物件
			way2.setHeight(height);// 設定參數 高
			way2.setWidth(width);// 設定參數 寬
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ way2.getArea());
			// 輸出結果
		}
		// 方法二
		scanner.close();
	}
}
