package ce1002.e3.s102502512;
import java.util.*;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);													//Line 8-39: let user input the square and choose the method to calculate the area
		int de;
		int w=0,h=0,a=0;
		do
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			de=input.nextInt();
			if(de!=1&&de!=2)
			{
				System.out.println("Out of range!");
			}
		}while(de!=1&&de!=2);

		do
		{
			System.out.println("Input the width:");
			w=input.nextInt();
			if(w>20||w<1)
			{
				System.out.println("Out of range!");
			}
		}while(w>20||w<1);
		do
		{
			System.out.println("Input the height:");
			h=input.nextInt();
			if(h>20||h<1)
			{
				System.out.println("Out of range!");
			}
		}while(h>20||h<1);
		if(de==1)											//Line 40-51: calculate the area
		{
			Rectangle rectangle = new Rectangle(w,h);
		}
		else
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);// set the width
			rectangle.setHeight(h);
			a=rectangle.getArea(w,h);
			System.out.println("The area of this rectangle is "+a);
		}
	}

}
