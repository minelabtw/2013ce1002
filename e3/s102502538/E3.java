package ce1002.e3.s102502538;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner( System.in );
		int set;
		int width=0,height=0;
		int area;
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		set = input.nextInt();
		while(set<1||set>2){//檢驗是否在正確的範圍內
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			set = input.nextInt();
		}
		System.out.println("Input the width: ");
		width=  input.nextInt();
		while(width<1||width>20){//檢驗是否在正確的範圍內
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width =  input.nextInt();
		}
		System.out.println("Input the height:");
		height =  input.nextInt();
		while(height<1||height>20){//檢驗是否在正確的範圍內
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			height =  input.nextInt();
		}	
		switch(set){//根據set的輸入採取不同的動作
		case 1 :
		{
			Rectangle rectangle = new Rectangle(width,height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());
			break;
		}
		case 2 :
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());	
			break;
		}
		}
	}
}
