package ce1002.e3.s102502028;

public class Rectangle {

	private int width;
	private int height;
	
	public Rectangle()
	{
		//System.out.println("this is constructor");
	}
	
	public Rectangle(int w,int h)  //用constructor方法初始化
	{
		//System.out.println("this is constructor 2");
		width = w ;
		height = h ;
	}
	
	public void setWidth(int w)   //用method方法初始化
	{
		//System.out.println("this is method");
		width = w ;
	}
	
	public void setHeight(int h)
	{
		height = h ;
	}
	
	public void getArea()    //輸出面積
	{
		System.out.print("The area of this rectangle is " + width*height) ;
	}
	

}
