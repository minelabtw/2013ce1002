package ce1002.e3.s102502028;

import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int method = 1 ;    //宣告
		int w = 1 ;
		int h = 1 ;
		
		System.out.println("Create rectangle in 2 ways.") ;   //輸入方法及判斷
		System.out.println("1)create by constructor parameters,2)create by setter: ") ;
		Scanner cin = new Scanner (System.in) ;
		method = cin.nextInt();
		
		while (method < 1 || method > 2)
		{
			System.out.println("Out of range!") ;
			System.out.println("Create rectangle in 2 ways.") ;
			System.out.println("1)create by constructor parameters,2)create by setter: ") ;
			method = cin.nextInt();		
		}
		
		System.out.println("Input the width: ") ;      //輸入寬及判斷
		w = cin.nextInt() ;
		while (w < 1 || w > 20)
		{
			System.out.println("Out of range!") ;
			System.out.println("Input the width: ") ;
			w = cin.nextInt() ;
		}
		
		System.out.println("Input the height: ") ;     //輸入高及判斷
		h = cin.nextInt() ;
		while (h < 1 || h > 20)
		{
			System.out.println("Out of range!") ;
			System.out.println("Input the height: ") ;
			h = cin.nextInt() ;
		}
		
		if (method == 1)              //用方法一輸出結果
		{
			Rectangle rectangle = new Rectangle(w,h);
			System.out.println("The width of this rectangle is "+ w) ;
			System.out.println("The height of this rectangle is "+ h) ;
			rectangle.getArea() ;
		}
		
		if (method == 2)              //用方法二輸出結果
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.println("The width of this rectangle is "+ w) ;
			System.out.println("The height of this rectangle is "+ h) ;
			rectangle.getArea() ;
		}
		
		/*Rectangle rectangle = new Rectangle(5, 10);
		rectangle.setWidth(3);*/
		

	}

}
