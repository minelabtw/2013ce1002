package ce1002.e3.s102502013;

import java.util.Scanner;

public class Rectangle {
	
		private int width;//declare a variable width
		private int height;//declare a variable height
		Rectangle(int w, int h){//declare a constructor for method1使用建構子+參數初始化物件
			width = w;
			height = h;
		}
		Rectangle(){//declare a constructor for method2使用setter設定物件
			width = 0;
			height = 0;
		}
		Scanner input = new Scanner(System.in);
		public void setWidth(){//set width
			int w = 0;
			System.out.println("Input the width: ");
			w = input.nextInt();
			while(w<=0||w>20){
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w = input.nextInt();
			}
			width = w;
		}
		public void setHeight(){//set height
			int h = 0;
			System.out.println("Input the height: ");
			h = input.nextInt();
			while(h<=0||h>20){
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h = input.nextInt();
			}
			height = h;
		}
		public int getWidth(){//get width
			return width;
		}
		public int getHeight(){//get height
			return height;
		}
		public int getArea(){//get area
			int area = 0;
			area = width * height;
			return area;
		}
}
