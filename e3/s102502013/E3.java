package ce1002.e3.s102502013;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		int method = 0;//declare a variable for method
		Scanner input = new Scanner(System.in);
		method = input.nextInt();// input method
		int w = 0;
		int h = 0;
		while(method<=0||method>2){//when method is not in the range, output "Out of range!"and repeat input method
			System.out.println("Out of range!");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();
		}
		if(method == 1){//method1
			System.out.println("Input the width: ");
			w = input.nextInt();
			while(w<=0||w>20){
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w = input.nextInt();
			}
			System.out.println("Input the height: ");
			h = input.nextInt();
			while(h<=0||h>20){
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h = input.nextInt();
			}
			Rectangle rectangle = new Rectangle(w, h);
			System.out.println("The width of this rectangle is " + rectangle.getWidth());
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
			
		}
		else if (method == 2){//method2
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth();
			rectangle.setHeight();
			System.out.println("The width of this rectangle is " + rectangle.getWidth());
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
	}
}
