package ce1002.e3.s102502032;

public class Rectangle
{
	private int	width;
	private int	height;

	Rectangle()
	{
	}

	Rectangle(int w, int h)
	{
		width = w;
		height = h;
	}

	public void setWidth(int w)
	{
		width = w;
	}

	public void setHeight(int h)
	{
		height = h;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public int getArea()
	{
		return width * height;
	}
}
