package ce1002.e3.s102502032;

import java.util.Scanner;

public class E3
{
	public static void main(String[] args)
	{
		Scanner jin = new Scanner(System.in);
		int method = getMethod(jin);
		Rectangle rectangle = setRectangle(method, jin);
		printResults(rectangle);
		jin.close();
	}

	// print width, height and area
	public static void printResults(Rectangle rectangle)
	{
		System.out.println("The width of this rectangle is "
				+ rectangle.getWidth());
		System.out.println("The height of this rectangle is "
				+ rectangle.getHeight());
		System.out.println("The area of this rectangle is "
				+ rectangle.getArea());
	}

	// set up an object
	public static Rectangle setRectangle(int method, Scanner jin)
	{
		Rectangle temp = new Rectangle();
		switch (method)
		{
			case 0:
				temp = new Rectangle(getSize(1, 20, "Input the width: ", jin),
						getSize(1, 20, "Input the height: ", jin));
				break;
			case 1:
				temp.setWidth(getSize(1, 20, "Input the width: ", jin));
				temp.setHeight(getSize(1, 20, "Input the height: ", jin));
				break;
			default:
				break;
		}
		return temp;
	}

	// ask for passing method
	public static int getMethod(Scanner jin)
	{
		int method = -1;
		while (method != 0 && method != 1)
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter: ");
			method = jin.nextInt();
			if (method != 0 && method != 1)
				System.out.println("Out of range!");
		}
		return method;
	}

	// get size;
	public static int getSize(int min, int Max, Scanner jin)
	{
		if (min > Max)
		{
			System.out.print("getSize: size error (min > Max)");
			return -1;
		}
		int size = 0;
		do
		{
			size = jin.nextInt();
			if (size < min || size > Max)
				System.out.println("Out of range!");
		}
		while (size < min || size > Max);
		return size;
	}

	// get size;
	public static int getSize(int min, int Max, String str, Scanner jin)
	{
		if (min > Max)
		{
			System.out.print("getSize: size error (min > Max)");
			return -1;
		}
		int size = 0;
		do
		{
			System.out.println(str);
			size = jin.nextInt();
			if (size < min || size > Max)
				System.out.println("Out of range!");
		}
		while (size < min || size > Max);
		return size;
	}
}
