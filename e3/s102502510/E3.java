package ce1002.e3.s102502510;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);//create a Scanner
		int method = 0;
		int width = 0;
		int height = 0;

		while (method != 1 && method != 2) 
			{
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.print("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();
			if (method != 1 && method != 2) {
				System.out.println("Out of range!");
			}
		}
		while (width < 1 || width > 20) {
			System.out.println("Input the width: ");
			width = input.nextInt();
			if (width < 1 || width > 20) {
				System.out.println("Out of range!");
			}
		}
		while (height < 1 || height > 20) {
			System.out.println("Input the height: ");
			height = input.nextInt();
			if (height < 1 || height > 20) {
				System.out.println("Out of range!");
			}
		}
		if (method == 1) //use method 1 to create rectangle
		{
			Rectangle rectangle = new Rectangle(width, height);
			System.out.println("The width of this rectangle is "
					+ rectangle.getwidth());
			System.out.println("The height of this rectangle is "
					+ rectangle.getheight());
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());
		} 
		else //use method 2 to create rectangle
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setheight(height);
			rectangle.setwidth(width);
			System.out.println("The width of this rectangle is "
					+ rectangle.getwidth());
			System.out.println("The height of this rectangle is "
					+ rectangle.getheight());
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());

		}

	}
}
