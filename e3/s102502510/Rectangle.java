package ce1002.e3.s102502510;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle() //initialize method 2
	{}

	public Rectangle(int w, int h) //initialize method 1
	{
		this.width = w;
		this.height = h;
	}

	public void setwidth(int w) {
		this.width = w;
	}

	public void setheight(int h) {
		this.height = h;
	}

	public int getArea() {
		return this.height * this.width;
	}
	public int getwidth()
	{
		return this.width;
	}
	public int getheight()
	{
		return this.height;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

}
