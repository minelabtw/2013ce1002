//用兩種物件方式求矩形面積

package ce1002.e3.s102502513;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner (System.in);

		int method=0;
		int width=0, height=0;

		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			method = input.nextInt();

		if(method!=1 && method!=2)
		{
			System.out.println("Out of range!");
		}

		}while(method!=1 && method!=2);

		do{
			System.out.println("Input the width: ");
			width = input.nextInt();

			if(width<1||width>20)
			{
				System.out.println("Out of range!");
			}

		}while(width<1||width>20);

		do{
			System.out.println("Input the height: ");
			height = input.nextInt();

			if(height<1||height>20)
			{
				System.out.println("Out of range!");
			}

		}while(height<1||height>20);

		switch(method)
			{
			case 1:                                                      //呼叫物件一
				Rectangle choose1 = new Rectangle(width,height);
				break;

			case 2:                                                      //呼叫物件二
				Rectangle choose2 = new Rectangle();
				choose2.setWidth(width);
				choose2.setHeight(height);
				break;

			default:
				break;
			}
		input.close();
		 
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + width*height);
	}
}
