package ce1002.e3.s102502007;
import java.util.Scanner;
class Rectangle
{
	private int width;
	private int height;
	public Rectangle(int wid , int hei)
	{
		width=wid;
		height=hei;
	}//constructor 建立物件的同時傳入參數
	public Rectangle()
	{
		
	}//constructor 不傳入參數
	public void setWidth(int wid)
	{
		width=wid;
	}//存入寬
	public void setHeight(int hei)
	{
		height=hei;
	}//存入高
	public void getArea()
	{
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + width*height);
	}//計算面積
}
public class E3 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int mode;//兩種方法
		int width,height;
		Scanner input = new Scanner(System.in);
		do
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			mode = input.nextInt();
			if(mode!=1 && mode!=2)
				System.out.println("Out of range!");
		}while(mode!=1 && mode!=2);//loop till the correct number is input
		
		switch(mode)
		{
		case 1:
		do
		{
			System.out.println("Input the width: ");
			width = input.nextInt();
			if(width<1 || width>20)
			{
				System.out.println("Out of range!");
				continue;
			}
		}while(width<1 || width>20);
		do
		{
			System.out.println("Input the height: ");
			height = input.nextInt();
			if(height<1 || height>20)
			{
				System.out.println("Out of range!");
				continue;
			}
		}while(height<1 || height>20);
		Rectangle rectangle1 = new Rectangle(width,height);	
		rectangle1.getArea();//第一種方法
		break;
		case 2:
			Rectangle rectangle2 = new Rectangle();
			do
			{
				System.out.println("Input the width: ");
				width = input.nextInt();
				if(width<1 || width>20)
				{
					System.out.println("Out of range!");
					continue;
				}
				rectangle2.setWidth(width);
			}while(width<1 || width>20);
			do
			{
				System.out.println("Input the height: ");
				height = input.nextInt();
				if(height<1 || height>20)
				{
					System.out.println("Out of range!");
					continue;
				}
				rectangle2.setHeight(height);
			}while(height<1 || height>20);
			rectangle2.getArea();//第二種方法
			break;
			default:
				break;
		}
		
	}

}
