package ce1002.e3.s102502043;
import java.util.Scanner;
public class E3 
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		int k = input.nextInt();
		while(k!=1&&k!=2)								//限定模式選擇
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			k = input.nextInt();
		}
		System.out.println("Input the width: ");
		int w = input.nextInt();
		while(w<1||w>20)								//限制寬
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w = input.nextInt();
		}
		System.out.println("Input the height: ");
		int h = input.nextInt();
		while(h<1||h>20)								//限定常
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			h = input.nextInt();
		}
		if(k==1)										//模式1
		{
			Rectangle r = new Rectangle(w,h);			
			System.out.println("The width of this rectangle is "+r.rwidth());
			System.out.println("The height of this rectangle is "+r.rheight());
			System.out.println("The area of this rectangle is "+r.getArea());
			
		}
		if(k==2)										//模式2
		{
			Rectangle r = new Rectangle();
			r.Width(w);
			r.Height(h);
			System.out.println("The width of this rectangle is "+r.rwidth());
			System.out.println("The height of this rectangle is "+r.rheight());
			System.out.println("The area of this rectangle is "+r.getArea());
			
		}	
		input.close();
	}

}
