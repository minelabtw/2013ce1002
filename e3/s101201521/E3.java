package ce1002.e3.s101201521;
import java.util.Scanner;
public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Rectangle rectangle;//declare rectangle as a Rectnagle reference
		int whatCreate = 0, width = 0, height = 0;
		//prompt for create mode
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			whatCreate = input.nextInt();
			if(whatCreate != 1 && whatCreate != 2)
				System.out.println("Out of range!");
		}while(whatCreate != 1 && whatCreate != 2);
		//prompt for rectangle width
		do{
			System.out.println("Input the width:");
			width = input.nextInt();
			if(width < 1 || width > 20)
				System.out.println("Out of range!");
		}while(width < 1 || width > 20);
		//prompt for rectangle height
		do{
			System.out.println("Input the height:");
			height = input.nextInt();
			if(height < 1 || height > 20)
				System.out.println("Out of range!");
		}while(height < 1 || height > 20);
		//check create mode
		if(whatCreate == 1){
			rectangle = new Rectangle(width, height);
		}
		else{
			rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
		}
		//print rectangle width, height, and area
		System.out.println("The width of this rectangle is " + rectangle.getWidth());
		System.out.println("The height of this rectangle is " + rectangle.getHeight());
		System.out.println("The area of this rectangle is " + rectangle.getArea());
	}

}
