package ce1002.e3.s101201521;

public class Rectangle {
	private int width;//width of the rectangle
	private int height;//height of the rectangle
	//default constructor
	public Rectangle(){
	}
	//my constructor
	public Rectangle(int wid, int hei){
		width = wid;
		height = hei;
	}
	//set the width of the rectangle
	public void setWidth(int wid){
		width = wid;
	}
	//set the height of the rectangle
	public void setHeight(int hei){
		height = hei;
	}
	//calculate area of the rectangle and return
	public int getArea(){
		return width * height;
	}
	//return rectangle width
	public int getWidth(){
		return width;
	}
	//return rectangle height
	public int getHeight(){
		return height;
	}
}
