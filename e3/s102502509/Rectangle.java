package ce1002.e3.s102502509;

public class Rectangle
{
	private int width;
	private int height;
	
	Rectangle() // 抓private 預設值
	{

	}
	
	Rectangle(int a, int b)
	{
		width = a;
		height = b;
	}
	
	int Width() // 回傳值
	{
		return width;
	} 
	
	int Height()
	{
		return height;
	}
	
	int getArea()
	{
		return width * height;
	}
	
	void setWidth(int x)// 預設值
	{
		width = x;
	}
	
	void setHeight(int y)
	{
		height = y;
	}
	

}
