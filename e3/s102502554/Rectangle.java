package ce1002.e3.s102502554;

public class Rectangle {
	private int width;//寬
	private int height;//高
	
	Rectangle(){
		
	}//建構子
	
	Rectangle(int w, int h){
		width = w;
		height = h;
	}
	
	int getArea(){
		return width*height;
	}//計算面積
	
	void setwidth(int newwidth){
		width = newwidth;
	}//輸入寬
	
	void setheight(int newheight){
		height = newheight;
	}//輸入高

}
