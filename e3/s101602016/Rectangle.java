package ce1002.e3.s101602016;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle(int a,int b){//將傳入的寬、高，存入width,height兩個變數中
		width=a;
		height=b;
	}
	public Rectangle(){
		
	}
	public void setWidth(int n){
		width=n;//將寬存入width
	}
	public void setHeight(int m){
		height=m;//將高存入height
	}
	public int getArea(){
		return width*height;//回傳面積(寬乘高)
	}
}
