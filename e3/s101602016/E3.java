package ce1002.e3.s101602016;
import java.util.Scanner;

public class E3 {
	private static Scanner scanner;
	public static void main(String[] args) {
		scanner=new Scanner(System.in);
		int w,h,area,method;//寬、高、面積、方法
		do{//讓使用者輸入方法，如超出範圍則重新輸入
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:"); 
			method = scanner.nextInt();
			if(method<1||2<method)
				System.out.println("Out of range!");
		}while(method<1||2<method);
		
		do{//讓使用者輸入寬，如超出範圍則重新輸入
			System.out.println("Input the width: ");
			w=scanner.nextInt();
			if(w<1||20<w)
				System.out.println("Out of range!");
		}while(w<1||20<w);
		
		do{//讓使用者輸入高，如超出範圍則重新輸入
			System.out.println("Input the height: ");
			h=scanner.nextInt();
			if(h<1||20<h)
				System.out.println("Out of range!");
		}while(h<1||20<h);
		
		switch(method){
				case 1://方法1
					Rectangle rectangle = new Rectangle(w,h);//將寬、高傳入Rectangle函數
					area=rectangle.getArea();//計算面積
					break;
				default://方法2
					Rectangle rectangle2 = new Rectangle();
					rectangle2.setWidth(w);//將寬傳入Rectangle
					rectangle2.setHeight(h);//將高傳入Rectangle
					area=rectangle2.getArea();//計算面積
		}
		
		System.out.println("The width of this rectangle is "+w);//輸出寬
		System.out.println("The height of this rectangle is "+h);//輸出高
		System.out.println("The area of this rectangle is "+area);//輸出面積
	}
}
