package ce1002.e3.s101201504;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner input = new Scanner (System.in);
		int number=0;
		do{   //choose way 1 or way 2 or not
			System.out.println("Create rectangle in 2 ways");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			number=input.nextInt();
			if(number<1||number>2)
				System.out.println("Out of rangle");
		}while(number<1||number>2);
	
	int width ;
	do{               //input width
	  System.out.println("Input the width: ");
	  width=input.nextInt();
	  if(width<1||width>20)
			System.out.println("Out of rangle");
	}while(width<1||width>20);
	int height;
	do{	         //input height
		System.out.println("Input the height: ");
		height=input.nextInt();
		if(height<1||height>20)
			System.out.println("Out of rangle");
	}while(height<1||height>20);
	
	
	if(number==1){       //way 1
		Rectangle a = new Rectangle(width, height);
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + a.getArea());
		
	
	}
	if(number==2){	   //way 2
		Rectangle a = new Rectangle();
		a.setWidth(width);
		a.setHeight(height);
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + a.getArea());
		
	}
	
	}
		
}
