package ce1002.e3.s102502529;
import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in) ;
		int width=0;
		int height=0;
		int test=0;
		do											//判斷方法
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			test=s.nextInt();
			if(test==1||test==2)
				break;
			System.out.println("Out of range!");
		}while(true);
		do{															//決定寬高
			System.out.println("Input the width: ");
			width=s.nextInt();
			if(1<=width||width<=20)
				break;
			else
				System.out.println("Out of range!");
		}while(true);
		do{
			System.out.println("Input the height: ");
			height=s.nextInt();
			if(1<=height||height<=20)
				break;
			else
				System.out.println("Out of range!");
		}while(true);
		switch (test){														//使用不同的方法
		case 1:
		{
			Rectangle r = new Rectangle(width,height);
			r.generate();
			break;
		}
		case 2:
		{
			Rectangle r = new Rectangle();
			r.setwidth(width);
			r.setheight(height);
			r.generate();
			break;
		}
		default :
			break;
		}
	}

}
