package ce1002.e3.s101303504;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String msg = "Create rectangle in 2 ways.\n"
				+ "1)create by constructor parameters,2)create by setter: ";
		Scanner input = new Scanner(System.in);
		int c = 0;
		c = GetInt(input, msg, 1, 2);
		Rectangle rectangle = null;
		int width = GetInt(input, "Input the width: ", 1, 20);
		int height = GetInt(input, "Input the height: ", 1, 20);
		input.close();
		
		switch (c) {
		case 1:
			rectangle = new Rectangle(width, height);
			break;
		case 2:
			rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
			break;
		default:
			break;
		}
		System.out.println("The width of this rectangle is "
				+ rectangle.getWidth());
		System.out.println("The height of this rectangle is "
				+ rectangle.getHeight());
		System.out.println("The area of this rectangle is "
				+ rectangle.getArea());
	}


	public static int GetInt(Scanner input, String msg, int lowerbound,
			int upperbound) {
		int value = 0;
		do {
			System.out.println(msg);
			value = input.nextInt();
			if (value > upperbound || value < lowerbound)
				System.out.println("Out of range!");
		} while (value > upperbound || value < lowerbound);
		return value;
}
	}