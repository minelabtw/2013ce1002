package ce1002.e3.s102502532;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Create rectangle in 2 ways.");
		System.out
				.println("1)create by constructor parameters,2)create by setter:");

		int select = input.nextInt();
		while (select != 1 && select != 2) { // 不是 or
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter:");
			select = input.nextInt();
		}

		System.out.println("Input the width:");
		int width = input.nextInt();
		while (width < 1 || width > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			width = input.nextInt();
		}

		System.out.println("Input the height:");
		int height = input.nextInt();
		while (height < 1 || height > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the height:");
			height = input.nextInt();
		}

		if (select == 1) { // 1.使用建構子+參數初始化物件
			Rectangle rectangle = new Rectangle(width, height);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());
		} else { // 2.使用setter設定物件
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width); // set the width
			rectangle.setHeight(height); // set the height

			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());
		}

	}
}
