package ce1002.e3.s102502532;

public class Rectangle {
	private int width = 0; // private 在class Rectangle 才能用
	private int height = 0;

	Rectangle() { // 要打

	}

	Rectangle(int newW, int newH) {
		width = newW;
		height = newH;
	}

	int setWidth(int width2) {
		width = width2;
		return width;
	}

	int setHeight(int height2) {
		height = height2;
		return height;
	}

	int getArea() { // 方法
		return width * height;
	}

}
