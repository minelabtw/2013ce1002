package ce1002.e3.s102502547;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = 0;
		int b = 0;
		int x = 0;
		do {
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
			x = scanner.nextInt();
			if (x != 1 && x != 2)
				System.out.println("Out of range!");
		} while (x != 1 && x != 2); //輸入方法

		do {
			System.out.println("Input the width: ");
			a = scanner.nextInt();
			if (a < 1 || a > 20)
				System.out.println("Out of range!");
		} while (a < 1 || a > 20); //輸入長

		do {
			System.out.println("Input the height: ");
			b = scanner.nextInt();
			if (b < 1 || b > 20)
				System.out.println("Out of range!");
		} while (b < 1 || b > 20); //輸入寬

		if (x == 1) { //方法1
			Rectangle rectangle = new Rectangle(a, b);
			System.out.println("The width of this rectangle is " + a);
			System.out.println("The height of this rectangle is " + b);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}

		if (x == 2) { //方法2
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(a);// set the width
			rectangle.setHeight(b);// set the height
			System.out.println("The width of this rectangle is " + a);
			System.out.println("The height of this rectangle is " + b);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
	}

}
