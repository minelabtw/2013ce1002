package ce1002.e3.s102502547;

public class Rectangle {

	private int width;
	private int height;

	Rectangle() { //建構子

	}

	Rectangle(int a, int b) { //建構子+參數
		width = a;
		height = b;
	}

	public void setWidth(int a) {
		width = a;
	}

	public void setHeight(int a) {
		height = a;
	}

	public int getArea() {
		return width * height;
	}
}
