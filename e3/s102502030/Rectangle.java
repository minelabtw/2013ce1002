package ce1002.e3.s102502030;

public class Rectangle {
	private int width;
	private int height;
	Rectangle()
	{
		
	}
	Rectangle( int i, int j )
	{
		width = i;
		height = j;
	}
	public int getArea()
	{
		return width*height;
	}
	public void setWidth( int x )
	{
		width=x;
	}
	public void setHeight( int x )
	{
		height=x;
	}
}
