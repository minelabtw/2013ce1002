package ce1002.e3.s102502030;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int mode;
		int W;
		int H;
		int A;
		//模式 寬 高 面積
		Scanner input = new Scanner( System.in );
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		do
		{
			mode = input.nextInt();
			if( mode!=1 && mode!=2 )
			{
				System.out.println("Out of range!");
				System.out.println("Create rectangle in 2 ways.");
				System.out.println("1)create by constructor parameters,2)create by setter: ");
			}
		} while( mode!=1 && mode!=2 );	
		//選擇模式
		System.out.println("Input the width: ");
		do
		{
			W = input.nextInt();
			if( W<1 || W>20 )
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
			}
		} while( W<1 || W>20 );
		System.out.println("Input the height: ");
		//輸入寬
		do
		{
			H = input.nextInt();
			if( H<1 || H>20 )
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
			}
		} while( H<1 || H>20 );
		//輸入高
		input.close();
		Rectangle rectangle;
		switch( mode )
		{
		case 1:
			rectangle = new Rectangle( W, H );
			A=rectangle.getArea();
			System.out.println("The width of this rectangle is " + W);
			System.out.println("The height of this rectangle is " + H);
			System.out.println("The area of this rectangle is " + A);
			break;
		case 2:
			rectangle = new Rectangle();
			rectangle.setWidth( W );
			rectangle.setHeight( H );
			A=rectangle.getArea();
			System.out.println("The width of this rectangle is " + W);
			System.out.println("The height of this rectangle is " + H);
			System.out.println("The area of this rectangle is " + A);
			break;
		default:
			break;
		}
		//兩種物件
	}

}
