package ce1002.e3.s102502047;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		int n=0;
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		Scanner p = new Scanner(System.in);
		n=p.nextInt();
		for(;n!=1&&n!=2;)
		{
			System.out.println("Out of range !");
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
			n=p.nextInt();
		}
		
		int w=0,h=0;
		System.out.println("Input the width: ");
		w=p.nextInt();
		for(;w>20||w<1;)
		{
			System.out.println("Out of range !");
			System.out.println("Input the width: ");
			w=p.nextInt();
		}
		System.out.println("Input the height: ");
		h=p.nextInt();
		for(;h>20||h<1;)
		{
			System.out.println("Out of range !");
			System.out.println("Input the height: ");
			h=p.nextInt();
		}
		
		
		switch(n)//兩種選擇
		{
			case 1:
				Rectangle r = new Rectangle(w,h);//使用建構子+參數初始化物件
				System.out.println("The width of this rectangle is "+ r.gw());
				System.out.println("The height of this rectangle is "+ r.gh());
				System.out.print("The area of this rectangle is "+ r.ga());
				break;
			case 2:
				Rectangle r1 = new Rectangle();//使用setter設定物件
				r1.sw(w);
				r1.sh(h);
				System.out.println("The width of this rectangle is "+ r1.gw());
				System.out.println("The height of this rectangle is "+ r1.gh());
				System.out.print("The area of this rectangle is "+ r1.ga());
				break;
		}
		
	}

}
