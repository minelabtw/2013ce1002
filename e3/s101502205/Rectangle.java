package ce1002.e3.s101502205;

public class Rectangle {
	// Data
	private int width;
	private int height;
	
	// Constructors
	public Rectangle(){
		
	}
	public Rectangle(int w, int h){
		width = w;
		height = h;
	}
	
	// Setters
	public void setWidth(int w){
		width = w;
	}
	public void setHeight(int h){
		height = h;
	}
	
	// Getters
	public int getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	public int getArea(){
		return width*height;
	}
}
