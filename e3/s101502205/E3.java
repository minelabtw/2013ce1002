package ce1002.e3.s101502205;

import java.util.Scanner;

public class E3 {

	private static Scanner input;
	
	public static void main(String[] args) {
		
		// Create a scanner Object
		input = new Scanner(System.in);
		
		// User input: Which way to create rectangle
		int selection;
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		selection = input.nextInt();
		
		while(selection!=1 && selection!=2){
			// Error
			System.out.println("Out of range!");
			// User Input
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			selection = input.nextInt();
		}
		
		// User inputs: Input the width
		int width, height;
		System.out.println("Input the width: ");
		width = input.nextInt();
		
		while(width<1 || width>20){
			// Error
			System.out.println("Out of range!");
			// User Input
			System.out.println("Input the width: ");
			width = input.nextInt();
		}
		
		// User input: Input the height
		System.out.println("Input the height: ");
		height = input.nextInt();
		
		while(height<1 || height>20){
			// Error
			System.out.println("Out of range!");
			// User Input
			System.out.println("Input the height: ");
			height = input.nextInt();
		}
		
		Rectangle superRectangle;
		
		if(selection == 1){
			// Set values through constructor
			superRectangle = new Rectangle(width, height);
		}else{
			// Set values through setters
			superRectangle = new Rectangle();	
			superRectangle.setWidth(width);
			superRectangle.setHeight(height);
		}
		
		// Outputs
		System.out.println("The width of this rectangle is " + superRectangle.getWidth());
		System.out.println("The height of this rectangle is " + superRectangle.getHeight());
		System.out.println("The area of this rectangle is " + superRectangle.getArea());
	}

}
