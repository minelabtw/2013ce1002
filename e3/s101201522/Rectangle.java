package ce1002.e3.s101201522;

public class Rectangle {
	private int width;
	private int height;
	
	Rectangle (int w, int h) { //construct a rectangle when initialization
		width = w;
		height = h;
	}
	
	Rectangle () {
		width = height = 0;
	}
	
	public void setWidth (int w) { //set width
		width = w;
	}
	
	public void setHeight (int h) { //set height
		height = h;
	}
}
