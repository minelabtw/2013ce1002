package ce1002.e3.s102502506;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle(){
		this(0, 0);
	}
	public Rectangle(int w ,int h){
		this.width=w;
		this.height=h;
	}
	public void setwidth(int w){
		this.width=w;
	}
	public void setheight(int h){
		this.height=h;
	}
	public int getwidth(){
		return width;
	}
	public int getheight(){
		return height;
	}
	public int Area(){
		return width*height;
	}
	public void print(){ 
		System.out.println("The width of this rectangle is "+getwidth());
		System.out.println("The height of this rectangle is "+getheight());
		System.out.println("The area of this rectangle is "+Area());
	}
}