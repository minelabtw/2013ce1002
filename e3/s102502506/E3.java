package ce1002.e3.s102502506;

import java.util.Scanner;

public class E3 {
	public static void main(String[] args){
	Scanner put = new Scanner(System.in);
	System.out.println("Create rectangle in 2 ways.");  //選方法
	int way;
	do{
	System.out.println("1)create by constructor parameters,2)create by setter: ");
	way=put.nextInt();
	if(way!=1&&way!=2){  
		System.out.println("Out of range! ");
		}
	}while(way!=1&&way!=2);
	int width;
	do{                //輸入width
		System.out.println("Input the width: ");
		width=put.nextInt();
		if(width<1||width>20){
		System.out.println("Out of range! ");
	}
	}while(width<1||width>20);
	int height;
	do{         //輸入height
		System.out.println("Input the height: ");
		height=put.nextInt();
		if(height<1||height>20){
		System.out.println("Out of range! ");
		}
	}while(height<1||height>20);	
	Rectangle R ;       //把Rectangle叫做R
	if(way==1)    //第一種方法
	{
		R=new Rectangle(width,height);
	}
	else    //第二種方法
	{
		R=new Rectangle();
		R.setheight(height);
		R.setwidth(width);
	}
	R.print();    //使用print函式輸出
	}
}
