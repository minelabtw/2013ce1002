package ce1002.e3.s102502552;

import java.util.Scanner;

public class Rectangle {
	Scanner sc = new Scanner(System.in);
	
	private int width;
	private int height;//參數
	
	public Rectangle(int w,int h)
	{
		width = w;
		height = h;
	}//方法一建構子
	
	public Rectangle()
	{
	}//方法二建構子
	
	public void setwidth(int W)
	{
		do{
			System.out.print("Input the width:");
			W = sc.nextInt();
			if(W < 1 || W > 20)
				System.out.println("Out of range!");
		}while(W < 1 || W > 20);
		
		width = W;
	}//方法二中設定寬的函式
	
	public void setheight(int H)
	{
		do{
			System.out.print("Input the height:");
			H = sc.nextInt();
			if(H < 1 || H > 20)
				System.out.println("Out of range!");
		}while(H < 1 || H > 20);
		
		height = H;
	}//方法二中設定長的函式
	
	public void showthem()
	{
		System.out.println("The width of this rectangle is " + this.width);
		System.out.println("The height of this rectangle is " + this.height);
	}//顯示函式
	
	public int getArea()
	{
		int area = this.width * this.height;
		
		return area;
	}//算出與回傳面積的函式

}
