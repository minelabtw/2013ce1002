package ce1002.e3.s102502552;

import java.util.Scanner;

public class E3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);//建立掃描機
		
		int Method = 0,W = 0,H = 0;
		
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.print("1)create by constructor parameters,2)create by setter:");
			Method = sc.nextInt();
			if(Method != 1 &&Method != 2)
				System.out.println("Out of range!");
		}while(Method != 1 &&Method != 2);//要求輸入方法種類並檢查合理性
		
		switch(Method)
		{
		case 1://方法一
			do{
				System.out.print("Input the width:");
				W = sc.nextInt();
				if(W < 1 || W > 20)
					System.out.println("Out of range!");
			}while(W < 1 || W > 20);//輸入寬並檢查範圍
			
			do{
				System.out.print("Input the height:");
				H = sc.nextInt();
				if(H < 1 || H > 20)
					System.out.println("Out of range!");
			}while(H < 1 || H > 20);//輸入長並檢查範圍
			
			Rectangle rectanglecase1 = new Rectangle(W,H);//建立物件
			rectanglecase1.showthem();//顯示參數
			System.out.print("The area of this rectangle is " + rectanglecase1.getArea());//顯示面積
			break;
			
		case 2://方法二
			Rectangle rectanglecase2 = new Rectangle();//建立物件
			rectanglecase2.setwidth(W);//設定寬
			rectanglecase2.setheight(H);//設定長
			rectanglecase2.showthem();//顯示參數
			System.out.print("The area of this rectangle is " + rectanglecase2.getArea());//顯示面積
			break;
		}
		
	sc.close();//關閉掃描機
	}

}
