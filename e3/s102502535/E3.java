package ce1002.e3.s102502535;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {

		int method = 0;
		int width = 0 ;
		int height = 0 ;  //設變數。

		Scanner input = new Scanner(System.in);

		System.out.println("Create rectangle in 2 ways.") ;
		System.out.println("1)create by constructor parameters,2)create by setter:") ;
		method = input.nextInt() ;  //使輸入方法。

		while (method != 1 && method != 2) {
			System.out.println("Out of range! ") ;
			System.out.println("Create rectangle in 2 ways.") ;
			System.out.println("1)create by constructor parameters,2)create by setter:") ;
			method = input.nextInt() ;
		}  //若輸入之數字超出範圍，則使重新輸入。

		if (method == 1) {
			System.out.println("Input the width: ") ;
			width = input.nextInt() ;
			while ( width <= 0 || width > 20 )
			{
				System.out.println("Out of range!") ;
				System.out.println("Input the width: ") ;
				width = input.nextInt() ;
			}  //輸入寬。超出範圍，則重新輸入。
			System.out.println("Input the height: ") ;
			height = input.nextInt() ;
			while ( height <= 0 || height > 20 )
			{
				System.out.println("Out of range!") ;
				System.out.println("Input the height: ") ;
				height = input.nextInt() ;
			}  //輸入高。超出範圍，則重新輸入。
			Rectangle one = new Rectangle ( width , height ) ;
			System.out.println("The width of this rectangle is " + width) ;
			System.out.println("The height of this rectangle is " + height ) ;
			System.out.println("The area of this rectangle is " + one.getArea() ) ;  //輸出結果。
		}  //使用方法1：使用建構子+參數初始化物件。

		else if (method == 2) {
			System.out.println("Input the width: ") ;
			width = input.nextInt() ;
			while ( width <= 0 || width > 20 )
			{
				System.out.println("Out of range!") ;
				System.out.println("Input the width: ") ;
				width = input.nextInt() ;
			}  //輸入寬。超出範圍，則重新輸入。
			System.out.println("Input the height: ") ;
			height = input.nextInt() ;
			while ( height <= 0 || height > 20 )
			{
				System.out.println("Out of range!") ;
				System.out.println("Input the height: ") ;
				height = input.nextInt() ;
			}  //輸入高。超出範圍，則重新輸入。
			Rectangle two = new Rectangle () ;
			two.setWidth( width ) ;
			two.setHeight( height ) ;
			System.out.println("The width of this rectangle is " + width) ;
			System.out.println("The height of this rectangle is " + height ) ;
			System.out.println("The area of this rectangle is " + two.getArea() ) ;  //輸出結果。
		}  //使用方法二：使用setter設定物件。

		input.close();
	}

}
