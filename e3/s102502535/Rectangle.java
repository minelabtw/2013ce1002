package ce1002.e3.s102502535;

public class Rectangle {

	private int width;
	private int height;

	    public Rectangle (){
		}
	    
	    public Rectangle( int w , int h )
	    {
	       width = w ;
	       height = h ;
	    }  //方法一：建構子。
	    
	    
	    public void setWidth ( int w )
	    {
	    	width = w ;	    	
	    }  //方法二。
	    
	    public void setHeight ( int h )
	    {
	    	height = h ;
	    }  //方法二。
	    
	    
	    public int getArea()
	    {
	    	int area ;
	    	area = width * height ;
	    	return area ;
	    }  //計算面積。

}
