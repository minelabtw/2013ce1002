package ce1002.e3.s102502009;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle(int w, int h) // 建構子
	{
		height = h;
		width = w;
	}

	public Rectangle() { // 非建構子
	}

	public void setWidth(int w) {
		width = w;
	}

	public void setHeight(int h) {
		height = h;
	}

	public int getWidth() { // 取得值
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getArea() {
		return width * height;
	}
}