package ce1002.e3.s102502040;
import java.util.Scanner;
public class E3 {
	Rectangle rectangle = new Rectangle();


	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		//設定輸入方法
		Scanner input = new Scanner(System.in);
		int way;
		way = input.nextInt();
		//方法只有兩種
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			if( way<0 || way >3){
				System.out.println("Out of range!");
				System.out.println("Create rectangle in 2 ways.");
				System.out.println("1)create by constructor parameters,2)create by setter:");
			}
		}while( way<0 || way >3 );
		int width = 0;
		int height = 0;
		int area = 0;
		//設定長寬
			do{
				System.out.println("Input the width: ");
				if( width <0 || width > 20){
					System.out.println("Out of range!");
					System.out.println("Input the width: ");
				}
			}while( width <0 || width > 20);
			do{
				System.out.println("Input the height: ");
				if( height <0 || height > 20 ){
					System.out.println("Out of range!");
					System.out.println("Input the height: ");
				}
			}while( height <0 || height > 20);
			//設定回傳值,若是方法1則使用建構子,方法2使用function回傳值
			if(way == 1){
				Rectangle rectangle = new Rectangle(width,height);
				area = width * height;
			}
			else{
				Rectangle rectangle = new Rectangle();
				rectangle.setWidth(width);// set the width
				rectangle.setHeight(height);// set the height
				rectangle.returnArea();
			}
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + area);
		}
	}
