package ce1002.e3.s102502037;
import java.util.Scanner;
public class Rectangle {

		private int width;
		private int height;
		public Rectangle(int a,int b)//建構式
		{
			width=a;
			height=b;
		}
		public Rectangle()
		{
			
		}
		
		/////////////////////////////////
		
		public void print(int area)//輸出
		{
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+area);
		}
		public void setWidth(int w)//設定長寬
		{
			width=w;
		}
		public void setHeight(int h)
		{
			height=h;
		}
		public int getArea()//計算面積
		{
			return width*height;
		}

}
