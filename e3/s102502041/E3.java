package ce1002.e3.s102502041;
import java.util.Scanner;
class Rectangle {
	Rectangle(){}
	Rectangle(int w, int h)
	{
		width=w;
		height=h;
	}
	void setWidth(int n)
	{
		width=n;
	}
	void setHeight(int n)
	{
		height=n;
	}
	int getArea()
	{
		return width*height;
	}
	private int width;
	private int height;
}

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int mode,width,height;
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		do 
		{
			mode=cin.nextInt();
			if (mode!=1&&mode!=2)
				System.out.println("Out of range!\nCreate rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		} while (mode!=1&&mode!=2);
		System.out.println("Input the width: ");
		do 
		{
			width=cin.nextInt();
			if (width < 1 || width > 20)
				System.out.println("Out of range!\nInput the width: ");
		} while (width < 1 || width > 20);
		System.out.println("Input the height: ");
		do 
		{
			height=cin.nextInt();
			if (height < 1 || height > 20)
				System.out.println("Out of range!\nInput the height: ");
		} while (height < 1 || height > 20);
		switch(mode)
		{
		case 1:
			Rectangle rectangle1 = new Rectangle(width, height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle1.getArea());
			break;
		case 2:
			Rectangle rectangle2 = new Rectangle();
			rectangle2.setWidth(width);
			rectangle2.setHeight(height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle2.getArea());
		}

	}

}
