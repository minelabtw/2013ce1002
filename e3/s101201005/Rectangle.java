package ce1002.e3.s101201005;

public class Rectangle {
	
	private int width;
	private int height;
	
	public Rectangle(int width, int height)
	{
		this.width=width;
		this.height=height;
	}

	public int getArea()
	{
		return width * height;
	}
	
	public Rectangle()
	{
		
	}
	
	public void setWidth(int width)
	{
		this.width=width;
	}
	
	public void setHeight(int height)
	{
		this.height=height;
	}


	
}
