package ce1002.e3.s101201005;
import java.util.Scanner;
public class E3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n=0, a=0, b=0;
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		n=input.nextInt();
		while(n<1 || n>2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			n=input.nextInt();
		}	
			System.out.println("Input the width: ");
			a=input.nextInt();
			while (a<1 || a>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				a=input.nextInt();
			}
			
			System.out.println("Input the height: ");
			b=input.nextInt();
			while (a<1 || a>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				b=input.nextInt();
			}
		if(n==1)
		{
			Rectangle rectangle = new Rectangle(a,b);
			System.out.println("The width of this rectangle is " + a);
			System.out.println("The height of this rectangle is " + b);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
		
		else if (n==2)
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(a);// set the width
			rectangle.setHeight(b);// set the height
			System.out.println("The width of this rectangle is " + a);
			System.out.println("The height of this rectangle is " + b);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}

		
		
	}

}
