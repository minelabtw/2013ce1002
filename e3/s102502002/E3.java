package ce1002.e3.s102502002;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		int choice=0;
		choice=input.nextInt();
		while(choice!=1&&choice!=2)
		{
			System.out.println("Out of range!");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			choice=input.nextInt();
		}
		int wid=0;
		System.out.println("Input the width: ");
		wid=input.nextInt();
		while(wid<1||wid>20)
		{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				wid=input.nextInt();
			
			}
			int hgt=0;
				System.out.println("Input the height: ");
				hgt=input.nextInt();
			while(hgt<1||hgt>20)
			{
					System.out.println("Out of range!");
					System.out.println("Input the height: ");
					hgt=input.nextInt();
			
			}
			if(choice==1)
			{
				Rectangle rectangle = new Rectangle(wid, hgt);//��k�@
				rectangle.output();
			}
			else if(choice==2)
			{
				Rectangle rectangle = new Rectangle();
				rectangle.setWidth(wid);// set the width
				rectangle.setHeight(hgt);// set the height
				rectangle.output();
				
			}
			
	}

	

}
