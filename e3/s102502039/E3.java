package ce1002.e3.s102502039;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);// 輸出題目
		System.out.println("Create rectangle in 2 ways.");
		System.out
				.println("1)create by constructor parameters,2)create by setter:");
		int option = input.nextInt();// 輸入選項
		while (option < 1 || option > 2) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter:");
			option = input.nextInt();
		}
		System.out.println("Input the width: ");// 輸出寬度
		int a = input.nextInt();// 輸入寬度
		while (a < 1 || a > 20) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			a = input.nextInt();
		}
		System.out.println("Input the height: ");// 輸出高度
		int b = input.nextInt();// 輸入高度
		while (b < 1 || b > 20) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			b = input.nextInt();
		}
		Rectangle rectangle = new Rectangle();
		if (option == 1) {
			rectangle = new Rectangle(a, b);
		} else {
			rectangle.setWidth(a);
			rectangle.setHeight(b);
		}
		System.out.println("The width of this rectangle is " + a);// 輸出結果
		System.out.println("The height of this rectangle is " + b);
		System.out.println("The area of this rectangle is "
				+ rectangle.getarea());
	}

}
