package ce1002.e3.s102502550;

public class Rectangle {
	private int width;                                //長
	private int height;                               //寬
	
	public Rectangle(){                               //空白建構子
		
	}
	
	public  Rectangle(int w,int h){                   //有引數的建構子
		setWidth(w);
		setHeight(h);
	}
	
	public boolean setWidth(int w){                   //set method
		if(w<1 || w>20)
			return false;
		else{
			width = w;
			return true;
		}
			
	}
	
	public boolean setHeight(int h){                  
		if(h<1 || h>20)
			return false;
		else{
			height = h;
			return true;
		}
	}
	
	public int getWidth(){                           //get method
		return width;
	}
	
	public int getHeight(){
		return height;
	}
}
