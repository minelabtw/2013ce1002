package ce1002.e3.s102502550;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);                                  //輸入物件宣告
		int a=0,w=0,h=0;
		
		do{                                                                   //檢查輸入
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ");
		    a = in.nextInt();
		    
		    if(a!=1 && a!=2)
		    	System.out.println("Out of range!");
		}while(a!=1 && a!=2);

		do{                                                                   //檢查輸入
			System.out.println("Input the width: ");
		    w = in.nextInt();
		    
		    if(w<1 || w>20)
		    	System.out.println("Out of range!");
		}while(w<1 || w>20);
		
		do{                                                                   //檢查輸入
			System.out.println("Input the height: ");
		    h = in.nextInt();
		    
		    if(h<1 || h>20)
		    	System.out.println("Out of range!");
		}while(h<1 || h>20);
		
		if(a==1){                                                             //使用兩種建構方式21
			Rectangle rec = new Rectangle(w , h);
			System.out.println("The width of this rectangle is "+ rec.getWidth());
			System.out.println("The height of this rectangle is "+ rec.getHeight());
			System.out.println("The area of this rectangle is "+ rec.getWidth()*rec.getHeight());
		}
		else{
			Rectangle rec = new Rectangle();
			rec.setWidth(w);
			rec.setHeight(h);
			System.out.println("The width of this rectangle is "+ rec.getWidth());
			System.out.println("The height of this rectangle is "+ rec.getHeight());
			System.out.println("The area of this rectangle is "+ rec.getWidth()*rec.getHeight());
		}
		
		in.close();
	}

}
