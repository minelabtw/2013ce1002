package ce1002.e3.s102502557;

public class Rectangle 
{
	private static int width;
	
	private int height;
	
	Rectangle(){
	}//給第二種方法的建構元
	Rectangle(int w,int h){
		width = w;
		height = h;
	} //給第一種方法的建構元
	
	void set_width(int w){
		width = w;
	}//method 2   static代表靜態 記憶體內會一直

	void set_height(int h){
		height = h;
	}//method 2
	
	int  get_area(){
	return width * height;
	}
	
	void show_width(){
		System.out.print("The width of this rectangle is ");
		System.out.println(width);
	}
	void show_height(){
		System.out.print("The height of this rectangle is ");
		System.out.println(height);
	}
	void show_area(){
		System.out.print("The area of this rectangle is ");
		System.out.println(get_area());
	}

}
