package ce1002.e3.s102502557;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int way, width=0, height=0;
		
		Rectangle rectangle;//在這裡新建一個物件 但先不決定要用哪種建構元
		
		do
		{
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
        way = input.nextInt();
        if(way!=1&&way!=2)
        {
        	System.out.println("Out of range!");
        }
		}while(way!=1&&way!=2);
		
		do
		{
				System.out.println("Input the width: ");
				width = input.nextInt();
				if(width<1||width>20)
				{
					System.out.println("Out of range!");
				}
		}while(width<1||width>20);
		do
		{
			System.out.println("Input the height: ");
			height = input.nextInt();
			if(height<1||height>20)
			{
				System.out.println("Out of range!");
			}
		}while(height<1||height>20);
	
	
	switch(way)
	{
	case 1:
		rectangle = new Rectangle(width,height) ;//建一個新的object 並呼叫第一種建構元
		rectangle.show_width();
	    rectangle.show_height();
	    rectangle.show_area();
		break;
	case 2:
		rectangle = new Rectangle() ;
		rectangle.set_width(width);
		rectangle.set_height(height);
		
		rectangle.show_width();
	    rectangle.show_height();
	    rectangle.show_area();
		break;
	}
	    
	
		
		
		
		
		
	}
}
