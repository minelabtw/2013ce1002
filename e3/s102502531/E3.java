package ce1002.E3.s102502531;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int a; //變數
		int b;
		int c;
		System.out.println("Create rectangle in 2 ways.");
		do // 選擇方法
		{
			System.out
					.println("1)create by constructor parameters,2)create by setter: ");
			Scanner scanner = new Scanner(System.in);
			a = scanner.nextInt();
			if (a < 1 || a > 2) {
				System.out.println("Out of range!");
			}
		} while (a < 1 || a > 2);
		// TODO Auto-generated method stub
		do     // 設定長寬
		{
			System.out.println("Input the width: ");
			Scanner scanner = new Scanner(System.in);
			b = scanner.nextInt();
			if (b < 1 || b > 20) {
				System.out.println("Out of range!");
			}

		} while (b < 1 || b > 20);
		do {
			System.out.println("Input the height: ");
			Scanner scanner = new Scanner(System.in);
			c = scanner.nextInt();
			if (c < 1 || c > 20) {
				System.out.println("Out of range!");
			}

		} while (c < 1 || c > 20);
		if (a == 1) // 方法一
		{
			Rectangle rectangle = new Rectangle(b, c);
			System.out.println("The width of this rectangle is " + b);
			System.out.println("The height of this rectangle is " + c);
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea(b, c));
		}
		if (a == 2) // 方法二
		{
			Rectangle rectangle = new Rectangle();
			System.out.println("The width of this rectangle is "
					+ rectangle.setWidth(b));
			System.out.println("The height of this rectangle is "
					+ rectangle.setHeight(c));
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea(b, c));
		}
	}

}
