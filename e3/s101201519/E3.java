package ce1002.e3.s101201519;
import java.util.Scanner;
public class E3 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int type=0;//設定類別
		int width=0,height=0;
		
		while(type<=0 || type >=3)//設定類別範圍
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			type = input.nextInt();
			if(type<=0 || type >=3)
			{
				System.out.println("Out of range!");
			}
		}
		
		while(width<=0 || width>20)//設定寬
		{
			System.out.println("Input the width: ");
			width = input.nextInt();
			if(width<=0 || width>20)
			{
				System.out.println("Out of range!");
			}
		}
		
		while(height<=0 || height>20)//設定高
		{
			System.out.println("Input the height: ");
			height = input.nextInt();
			if(height<=0 || height>20)
			{
				System.out.println("Out of range!");
			}
		}
		
		if(type==1)
		{
			Rectangle rectangle = new Rectangle(width, height);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
		
		if(type==2)
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is " + rectangle.getArea());		
		}	
	}
}
