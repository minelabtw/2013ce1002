package ce1002.e3.s102502508;
import java.util.Scanner ;
public class E3 {
	private static Scanner input ;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner input = new Scanner(System.in) ;
		 
		  int c=0 ;      //使用者輸入
		  int w=0 ;      //寬度
		  int h=0 ;      //高度
		  
		  
		 System.out.println("Create rectangle in 2 ways.");
		 do{//利用後測式迴圈讓使用者做選擇
			  System.out.print("1)create by constructor parameters,2)create by setter: ");
		      c=input.nextInt() ;
		      
		      if(c==1 || c==2)
		      {
		    	  break ;   //如果符合結果就跳出迴圈
		      }
		      else if(c!=1 && c!=2)
		      {
		    	  System.out.println("Out of range!");//若不符合要求就輸出超出範圍
		      }
		 
		 	
		 }while(c!=1 && c!=2);
		 
		 
		 do{
			 System.out.print("Input the width: ");
			 w=input.nextInt();
			 
			 if(w>=1 && w<=20)
			 {
				 break ;     //如果符合結果就跳出迴圈
			 }
			 else if(w<1 || w>10)
			 {
				 System.out.println("Out of range!");//若不符合要求就輸出超出範圍
				
			 }
			 
		 }while(w<1 || w>10);
		 
		 do{
			 System.out.print("Input the height: ");
			 h=input.nextInt();
			 
			 if(h>=1 && h<=20)
			 {
				 break ;
			 }
			 else if(h<1 || h>10)
			 {
				 System.out.println("Out of range!");
			 }
			 
		 }while(h<1 || h>10);
		 
		 switch(c)
			 {
			   case 1://利用狀況一的方法印出該長方形的長度,寬度和面積
			   { Rectangle rectangle = new Rectangle(w, h);
				 System.out.println("The width of this rectangle is "+ w);
				 System.out.println("The height of this rectangle is "+h);     
				 System.out.println("The area of this rectangle is "+ rectangle.getarea());
				 break ;
				 }
		
			   case 2 ://利用狀況二的方法印出該長方形的長度,寬度和面積
				   {Rectangle rectangle = new Rectangle();
				   rectangle.setWidth(w);// set the width
				   rectangle.setHeight(h);// set the height
				   System.out.println("The width of this rectangle is "+ w);
				   System.out.println("The height of this rectangle is "+h);
                   System.out.println("The area of this rectangle is "+ rectangle.getarea());
				   break ;
				   }
			   default:
				   break ;
	         }
		 }
					   
			
			 
			 
		 }
		 
		 
		

	


