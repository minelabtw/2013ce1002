package ce1002.e3.s102502541;
import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int method;
		int w;
		int h;
		System.out.println("Create rectangle in 2 ways.");
		do{
			System.out.println("1)create by constructor parameters,2)create by setter:");
			method = scanner.nextInt();
			if(method!=1 && method!=2)
				System.out.println("Out of range!");
		}while(method!=1 && method!=2);//方法選擇
		do{
			System.out.println("Input the width:");
			w = scanner.nextInt();
		}while(w<1 || w>20);
		do{
			System.out.println("Input the height:");
			h = scanner.nextInt();
		}while(h<1 || h>20);
		switch(method)
		{
		case 1:
			Rectangle rectangle1 = new Rectangle();
			rectangle1.setWidth(w);//把w的數值給寬
		    rectangle1.setHeight(h);//把h的數值給高
		    System.out.println("The width of this rectangle is "+rectangle1.getWidth());
			System.out.println("The height of this rectangle is "+rectangle1.getHeight());
			System.out.println("The area of this rectangle is "+rectangle1.getArea());
		    break;
		case 2:
			Rectangle rectangle2 = new Rectangle(w,h);
			System.out.println("The width of this rectangle is "+rectangle2.getWidth());
			System.out.println("The height of this rectangle is "+rectangle2.getHeight());
			System.out.println("The area of this rectangle is "+rectangle2.getArea());
			break;
		}
	}

}
