package ce1002.e3.s102502541;

public class Rectangle {

	private int width;
	private int height;
	public Rectangle (){
		
	}
	public Rectangle (int x , int y){
		width = x;
		height = y;
	}
	public int getArea()//取得面積
	{
		return width*height;
	}
	public void setWidth(int x)//設定寬
	{
		width = x;
	}
	public void setHeight(int y)//設定高
	{
		height = y;
	}
	public int getWidth()//取得寬
	{
		return width;
	}
	public int getHeight()//取得高
	{
		return height;
	}
}
