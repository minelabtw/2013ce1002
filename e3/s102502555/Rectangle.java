package ce1002.e3.s102502555;

public class Rectangle {
	private int width;  //三角形的寬
	private int height;  //三角形的長
	
	Rectangle(){  //沒參數的建構子
	}
	
	Rectangle(int w , int h){  //有長寬參數的建構子
		width = w;
		height = h;
	}
	
	void setWidth(int w){  //設定寬
		width = w;
	}
	
	void setHeight(int h){  //設定長
		height = h;
	}
	
	void showData(){  //輸出長寬面積
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + getArea());
	}
	
	int getArea(){  //算面積
		return width * height;
	}
	
}
