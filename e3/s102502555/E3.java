package ce1002.e3.s102502555;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		int way;  //方法
		int width;  //寬
		int height;  //長
		Scanner input = new Scanner(System.in);
		
		//選方法1或2
		do{
			System.out.println("Create rectangle in 2 ways.");
			way = input.nextInt();
			
			if(way != 1 && way != 2){
				System.out.println("Out of range!");
			}
			
		}while(way != 1 && way != 2);
		
		//輸入寬
		do{
			System.out.println("Input the width:");
			width = input.nextInt();
			if(width >20 || width < 1){
				System.out.println("Out of range!");
			}
		}while(width >20 || width <= 0);
		
		//輸入長
		do{
			System.out.println("Input the height:");
			height = input.nextInt();
			if(height > 20 || height < 1){
				System.out.println("Out of range!");
			}
		}while(height > 20 || height < 1);
		
		//用方法1或2來做
		switch(way){
			case 1:
				Rectangle rectangle1 = new Rectangle(width , height);  //建立新的長方形
				rectangle1.showData();  //輸出長方形的資料
				break;
				
			case 2:
				Rectangle rectangle2 = new Rectangle();  //建立新的長方形
				rectangle2.setWidth(width);  //設定寬
				rectangle2.setHeight(height);  //設定長
				rectangle2.showData();  //輸出長方形的資料
				break;
				
			default:
				break;
			
		}
		
		input.close();
	}
	
}
