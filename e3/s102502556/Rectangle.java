package ce1002.e3.s102502556;

public class Rectangle {
	private int width;//矩形的寬
	private int height;//矩形的高
	Rectangle( int i , int j ) //有參數傳入的建構子(constructer)，方法一
	{
		width = i;
		height = j;
	}
	Rectangle () //沒參數傳入的建構子(constructer)，方法二
	{
		
	}
	int getArea () //回傳面積的值
	{
		return width * height;
	}
	void setWidth ( int x ) //設定矩形的寬
	{
		width = x;
	}
	void setHeight ( int y ) //設定矩形的高
	{
		height = y;
	}
}
