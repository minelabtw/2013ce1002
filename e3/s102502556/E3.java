package ce1002.e3.s102502556;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		int method = input.nextInt(); //宣告一個int變數method，用來儲存使用者所選擇的方法代碼
		while ( method != 1 && method != 2 ) //檢查使用者的輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			method = input.nextInt();
		}
		System.out.println("Input the width:");
		int width = input.nextInt(); //宣告一個int變數width，用來儲存矩形的寬
		while ( width <= 0 || width > 20 ) //檢查使用者的輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			width = input.nextInt();	
		}
		int height = 0; //宣告一個int變數height，用來儲存矩形的高
		System.out.println("Input the height:"); 
		height = input.nextInt();
		while ( height <= 0 || height > 20 ) //檢查使用者的輸入是否合乎標準，否則要求其重新輸入
		{
			System.out.println("Out of range!");
			System.out.println("Input the height:");
			height = input.nextInt();
		}
		input.close(); //關閉Scanner
		int area = 0; //宣告一個int變數area，用來儲存矩形的面積
		if ( method == 1 ) //使用方法一，取得矩形面積
		{
			Rectangle rectangle = new Rectangle (width, height);
			area = rectangle.getArea();
		}
		else ////使用方法二，取得矩形面積
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			area = rectangle.getArea();
		}
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + area);		
	}
}
