package ce1002.e3.s102502533;

import java.util.Scanner;
public class E3 {

	/** * @param args */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner( System.in );
		int way,w,h;
		
		System.out.print("Create rectangle in 2 ways.");//輸入用哪種形式
		System.out.println("\n1)create by constructor parameters,2)create by setter: ");
		way = input.nextInt();
		while(way != 1 && way != 2)
		{
			System.out.print( "Out of range!" );
			System.out.print("\nCreate rectangle in 2 ways.");
			System.out.println("\n1)create by constructor parameters,2)create by setter: ");
			way = input.nextInt();
		}
		System.out.println("Input the width: ");//輸入長 寬
		w = input.nextInt();
		while(w <=0 || w>20)
		{
			System.out.print( "Out of range!" );
			System.out.println( "\nInput the width:" );
		    w = input.nextInt();
		}
		
		System.out.println("Input the height: ");
		h = input.nextInt();
		while(h <=0 || h>20)
		{
			System.out.print( "Out of range!" );
			System.out.println( "\nInput the height:" );
		    h = input.nextInt();
		}
		
		if(way ==1)//以傳入方式計算面積
		{
			Rectangle rectangle = new Rectangle(w, h);	
			System.out.print("The width of this rectangle is " + w);
			System.out.print("\nThe height of this rectangle is " + h);
			System.out.print("\nThe area of this rectangle is " + rectangle.a);
		}
		if(way == 2){//已改變private的方式蒜面積
			Rectangle rectangle = new Rectangle();
			rectangle.setwidth(w);// set the width
			rectangle.setheight(h);// set the height
			System.out.print("The width of this rectangle is " + w);
			System.out.print("\nThe height of this rectangle is " + h);
			System.out.print("\nThe area of this rectangle is " + rectangle.area());
		}
	}

}
