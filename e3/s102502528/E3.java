package ce1002.e3.s102502528;
import java.util.Scanner;
import ce1002.e3.s102502528.Rectangle;

public class E3 {
	public static void main(String[] args)
	   {
	      Rectangle rectangle;
	      Scanner scanner = new Scanner(System.in);
	      int n, width, height;

	      System.out.println("Create rectangle in 2 ways.");
	      do          //�I���`��
	      {
	         System.out.println("1)create by constructor parameters,2)create by setter: ");
	         n = scanner.nextInt();
	         if(n != 1 && n != 2)
	            System.out.println("Out of range!");
	      } while(n != 1 && n != 2);
	      do        //�A����
	      {
	         System.out.println("Input the width: ");
	         width = scanner.nextInt();
	         if(width < 1 || width > 20)
	            System.out.println("Out of range!");
	      } while(width < 1 || width > 20);
	      do        //�A����
	      {
	         System.out.println("Input the height: ");
	         height = scanner.nextInt();
	         if(height < 1 || height > 20)
	            System.out.println("Out of range!");
	      } while(height < 1 || height > 20);
	      scanner.close();

	      switch (n)
	      {
	         case 1:     //�˝��`�����s���I����
	            rectangle = new Rectangle(width, height);
	            break ;
	         case 2:
	            rectangle = new Rectangle();
	            rectangle.setWidth(width);
	            rectangle.setHeight(height);
	            break ;
	         default:
	            rectangle = new Rectangle();
	      }

	      System.out.println("The width of this rectangle is " + rectangle.getWidth());       //�A�o����
	      System.out.println("The height of this rectangle is " + rectangle.getHeight());
	      System.out.println("The area of this rectangle is " + rectangle.getArea());
	   }

}
