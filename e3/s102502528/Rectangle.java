package ce1002.e3.s102502528;

public class Rectangle
{
   private int width;
   private int height;
   
   public Rectangle(int w, int h)
   {
      width = w;
      height = h;
   }

   public Rectangle()
   {
      width = 1;
      height = 1;
   }

   public void setWidth(int w)
   {
      width = w;
   }
   
   public void setHeight(int h)
   {
      height = h;
   }

   public int getArea()
   {
      return width * height;
   }

   public int getWidth()
   {
      return width;
   }

   public int getHeight()
   {
      return height;
   }


}
