package ce1002.e3.s102502524;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		int a = 0;
		int w = 0;
		int h = 0;
		
		System.out.println("Create rectangle in 2 ways.");	//選擇方法
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		a = input.nextInt();
		while(a<1 || a>2)									//判斷範圍
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			a = input.nextInt();
		}
		
		System.out.println("Input the width: ");			//輸入width
		w = input.nextInt();
		while(w<1 || w>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			w = input.nextInt();
		}
		
		System.out.println("Input the height: ");			//輸入height
		h = input.nextInt();
		while(h<1 || h>20)
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			h = input.nextInt();
		}
		
		switch(a)
		{
		case 1:												//方法一
			Rectangle meth1 = new Rectangle(w,h);
			break;
			
		case 2:												//方法二
			Rectangle meth2 = new Rectangle();
			meth2.meth2w(w);
			meth2.meth2h(h);
			break;
		}
		
		Rectangle rec = new Rectangle();					//輸出結果
		rec.getArea();
	}

}
