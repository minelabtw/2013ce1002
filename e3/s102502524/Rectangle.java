package ce1002.e3.s102502524;

public class Rectangle {

	private int width;
	private int height;
	
	public Rectangle(int a , int b)			//方法一
	{
		width = a;
		height=b;
	}
	
	public Rectangle(){}					//方法二
	void meth2w(int wpass)
	{
		width = wpass;
	}
	
	void meth2h(int hpass)
	{
		height = hpass;
	}
	
	void getArea()							//輸出結果
	{
		System.out.println("The width of this rectangle is " + width );
		System.out.println("The height of this rectangle is " + height );
		System.out.println("The width of this rectangle is " + width*height );
	}
}
