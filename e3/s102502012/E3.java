package ce1002.e3.s102502012;
import java.util.*;

public class E3 {

	private static boolean outOfRange(){
		System.out.println("Out of range!");
		return true;
	}
	
	
	
	public static void main(String[] args) {
		int way, width, height;
		Scanner input = new Scanner(System.in);
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			way = input.nextInt();
		}while(way != 1 && way != 2 && outOfRange());
		do{
			System.out.println("Input the width: ");
			width = input.nextInt();	 
		}while((width < 1 || width > 20) && outOfRange());
		do{
			System.out.println("Input the height: ");
			height = input.nextInt();
		}while((height < 1 || height > 20) && outOfRange());
		
		switch(way){
		case 1: // version with constructor two parameters
			Rectangle rect1 = new Rectangle(width, height);
			rect1.printRectangle();
			break;
		case 2: // version with constructor no parameter
			Rectangle rect2 = new Rectangle();
			rect2.setHeight(height);
			rect2.setWidth(width);
			rect2.printRectangle();
			break;
		}
		
		input.close();
	}

}
