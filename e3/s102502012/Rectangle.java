package ce1002.e3.s102502012;

public class Rectangle {
	private int width;
	private int height;
	
	Rectangle(){}
	
	Rectangle(int width, int height){
		setWidth(width);
		setHeight(height);
	}
	
	public void setWidth(int width){
		this.width = width;
	}
	
	public void setHeight(int height){
		this.height = height;
	}

	public int getArea(){
		return width * height;
	}
	
	public void printRectangle(){
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + getArea());
	}
}
