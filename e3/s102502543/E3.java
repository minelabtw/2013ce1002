package ce1002.e3.s102502543;

import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int m, w, h;  //宣告變數
		do {
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:"); //輸出
			m = input.nextInt();
			if (m != 1 && m != 2)
				System.out.println("Out of range!");
		} while (m != 1 && m != 2);
		do {
			System.out.println("Input the width:");
			w = input.nextInt();
			if (w < 1 || w > 20)
				System.out.println("Out of range!");
		} while (w < 1 || w > 20);
		do {
			System.out.println("Input the height:");
			h = input.nextInt();
			if (h < 1 || h > 20)
				System.out.println("Out of range!");
		} while (h < 1 || h > 20);
		if (m == 1) { //方法一
			Rectangle rectangle = new Rectangle(w, h);
			System.out.println("The width of this rectangle is " + rectangle.getWidth()); //輸出
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.print("The area of this rectangle is "
					+ rectangle.getArea());
		} else { //方法二
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(w);
			rectangle.setHeight(h);
			System.out.println("The width of this rectangle is " + rectangle.getWidth());
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.print("The area of this rectangle is "
					+ rectangle.getArea());
		}
	}
}
