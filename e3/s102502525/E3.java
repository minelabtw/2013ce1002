package ce1002.e3.s102502525;
import java.util.Scanner;
public class E3 {
	public static void main(String[] args) {
	Scanner input=new Scanner(System.in); 	
	System.out.println("Create rectangle in 2 ways.");
	int num;
	int width,height;
	int area;
	
	do
	{
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		num=input.nextInt();
		if(num<1 || num>2)
		System.out.println("Out of range!");
	}while(num<1 || num>2);
	do
	{
		System.out.println("Input the width: ");
		width=input.nextInt();
		if(width<1 || width>20)
		System.out.println("Out of range!");
	}while(width<1 || width>20);
	do
	{
		System.out.println("Input the height: ");
		height=input.nextInt();
		if(height<1 || height>20)
		System.out.println("Out of range!");
	}while(height<1 || height>20);
	if(num==1)
	{
	Rectangle r=new Rectangle(width,height);
	System.out.println("The width of this rectangle is "+width);
	System.out.println("The height of this rectangle is "+height);
	System.out.println("The area of this rectangle is "+r.getArea());
	}
	if(num==2)
	{
		Rectangle r=new Rectangle();
		r.setWidth(width);
		r.setHeight(height);
		System.out.println("The width of this rectangle is "+width);
		System.out.println("The height of this rectangle is "+height);
		System.out.println("The area of this rectangle is "+r.getArea());
	}
	
	
  }
}
