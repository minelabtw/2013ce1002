package ce1002.e3.s102502049;

public class Rectangle {

	private int width;
	private int height;

	public Rectangle() { // default constructor

	}

	public Rectangle(int x, int y) { // parameter constructor
		width = x;
		height = y;
	}

	public void setWidth(int x) {
		width = x;
	}

	public void setHeight(int y) {
		height = y;
	}

	public int getArea() {
		return width * height;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
}
