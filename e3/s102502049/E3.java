package ce1002.e3.s102502049;

import java.util.Scanner;

public class E3 {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number=1; // mode
		int width=1;
		int height=1;
		
		do{
			if(number!=1 && number!=2)
			{
				System.out.println("Out of range!");				
			}
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			number = input.nextInt();
		}
		while(number!=1 && number!=2); // choose mode
		
		do{
			if(width<1 || width>20)
			{
				System.out.println("Out of range!");				
			}
			System.out.println("Input the width: ");
			width = input.nextInt();
		}
		while(width<1 || width>20); // set width
		
		do{
			if(height<1 || height>20)
			{
				System.out.println("Out of range!");				
			}
			System.out.println("Input the height:");
			height = input.nextInt();
		}
		while(height<1 || height>20); // set height
		
		if(number==1) // mode1
		{
			Rectangle rectangle = new Rectangle(width,height);
			System.out.println("The width of this rectangle is " + rectangle.getWidth() ); // show answer
			System.out.println("The height of this rectangle is " + rectangle.getHeight() );
			System.out.println("The area of this rectangle is " + rectangle.getArea() );
		}
		else if (number==2) // mode2
		{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			System.out.println("The width of this rectangle is " + rectangle.getWidth() ); // show answer
			System.out.println("The height of this rectangle is " + rectangle.getHeight() );
			System.out.println("The area of this rectangle is " + rectangle.getArea() );
		}
		
	}

}
