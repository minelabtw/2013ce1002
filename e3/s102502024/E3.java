package ce1002.e3.s102502024;
import java.util.Scanner;
public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int num;  //宣告變數
		int w;
		int h;
		Scanner scn=new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");  //輸出題目
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		num=scn.nextInt();  //選擇方式
		while(num!=1 && num!=2)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			num=scn.nextInt();
		}
		switch(num)
		{
		case 1 :
			System.out.println("Input the width: ");
			w=scn.nextInt();
			while(w<1 || w>20)  //判斷是否符合條件
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w=scn.nextInt();
			}
			System.out.println("Input the height: ");
			h=scn.nextInt();
			while(h<1 || h>20)  //判斷是否符合條件
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h=scn.nextInt();
			}
			Rectangle rectangle = new Rectangle(w,h);  //建構元
			rectangle.getArea(w,h);  //輸出面積
			break;
		case 2 :
			System.out.println("Input the width: ");
			w=scn.nextInt();
			while(w<1 || w>20)  //判斷是否符合條件
			{
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				w=scn.nextInt();
			}
			System.out.println("Input the height: ");
			h=scn.nextInt();
			while(h<1 || h>20)  //判斷是否符合條件
			{
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				h=scn.nextInt();
			}
			Rectangle rec = new Rectangle();  //建構元
			rec.setWidth(w);  //設定寬
			rec.setHeight(h);  //設定長
			rec.getArea(w,h);  //輸出面積
			break;
		}
		// TODO Auto-generated method stub

	}

}
