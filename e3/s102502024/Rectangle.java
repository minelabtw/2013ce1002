package ce1002.e3.s102502024;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle(int w,int h)  //建構元
	{
		width=w;
		height=h;
	}
	public Rectangle()  //建構元
	{
		
	}
	public void setWidth(int w)  //設定寬
	{
		width=w;
	}
	public void setHeight(int h)  //設定長
	{
		height=h;
	}
	public void getArea(int w,int h)  //輸出面積
	{
		System.out.println("The width of this rectangle is "+width);
		System.out.println("The height of this rectangle is "+height);
		System.out.println("The area of this rectangle is "+width*height);
	}
	/**
	 * @param args
	 */
}
