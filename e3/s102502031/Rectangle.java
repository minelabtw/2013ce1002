package ce1002.e3.s102502031;

public class Rectangle {
	private int width;
	private int height;
	private int area;

	// constructor method 1
	public Rectangle() {
	}

	// constructor method 2
	public Rectangle(int newWidth, int newHeight) {
		width = newWidth;
		height = newHeight;
		area = width * height;
	}

	// calculater
	private void calculateArea() {
		area = width * height;
	}

	// set width for method 2
	void setWidth(int newWidth) {
		width = newWidth;
		calculateArea();
	}

	// set width for method 2
	void setHeight(int newHeight) {
		height = newHeight;
		calculateArea();
	}

	// output for one line
	private void printOneResult(String valueName, int value) {
		System.out.println("The " + valueName + " of this rectangle is " + value);
	}

	// output all
	void printAllResult() {
		printOneResult("width", width);
		printOneResult("height", height);
		printOneResult("area", area);
	}
}