package ce1002.e3.s102502031;

import java.util.Scanner;
import ce1002.e3.s102502031.Rectangle;

public class E3 {
	public static void main(String[] args) {
		// object
		Scanner jin = new Scanner(System.in);

		// variable
		int type;

		type = rangePrompt(1, 2, "Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter: ", jin);
		switch (type) {
		case 1:
			Rectangle rectangle1 = new Rectangle(rangePrompt(1, 20, "Input the width: ", jin), rangePrompt(1, 20, "Input the height: ", jin));
			rectangle1.printAllResult();
			break;
		case 2:
			Rectangle rectangle2 = new Rectangle();
			rectangle2.setWidth(rangePrompt(1, 20, "Input the width: ", jin));
			rectangle2.setHeight(rangePrompt(1, 20, "Input the height: ", jin));
			rectangle2.printAllResult();
			break;
		}
		
		jin.close();
		
	}

	public static int rangePrompt(int rangeMinimum, int rangeMaxmum, String promptQuestion, Scanner input) {
		int value;
		double valueIntergerTest;

		do {
			System.out.println(promptQuestion);
			valueIntergerTest = input.nextDouble();
			value = (int) valueIntergerTest;
			if (valueIntergerTest != (double) value) {
				value = -1;
			}
			if (value > rangeMaxmum || value < rangeMinimum) {
				System.out.println("Out of range!");
			}
		} while (value > rangeMaxmum || value < rangeMinimum);

		return value;
	}
}
