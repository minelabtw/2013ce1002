package ce1002.e3.s102502018;
import java.util.Scanner;
public class E3 {

	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		int method = scan.nextInt();
		while(method<1||method>2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			method = scan.nextInt();
		}
		if(method==1)
		{
			System.out.println("Input the width:");
			int width = scan.nextInt();
			while(width<1||width>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the width:");
				width = scan.nextInt();
			}
			System.out.println("Input the height:");
			int height = scan.nextInt();
			while(height<1||height>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the height:");
				height = scan.nextInt();
			}
			Rectangle rectangle = new Rectangle(width, height);      //set width and height
			System.out.println("The width of this rectangle is "+rectangle.getwidth());
			System.out.println("The height of this rectangle is "+rectangle.getheight());
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
		if(method==2)
		{
			System.out.println("Input the width:");
			int width = scan.nextInt();
			while(width<1||width>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the width:");
				width = scan.nextInt();
			}
			System.out.println("Input the height:");
			int height = scan.nextInt();
			while(height<1||height>20)
			{
				System.out.println("Out of range!");
				System.out.println("Input the height:");
				height = scan.nextInt();
			}
			Rectangle rectangle = new Rectangle();
			rectangle.setwidth(width);// set the width
			rectangle.setheight(height);// set the height
			System.out.println("The width of this rectangle is "+rectangle.getwidth());
			System.out.println("The height of this rectangle is "+rectangle.getheight());
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
	}

}
