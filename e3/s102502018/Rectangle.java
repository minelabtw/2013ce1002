package ce1002.e3.s102502018;

public class Rectangle {

	private int width;
	private int height;
	
	public Rectangle(int width,int height)
	{
		setwidth(width);		//設定寬
		setheight(height);		//設定高
	}
	public Rectangle()
	{
		
	}

	
	public void setwidth(int a)
	{
		width=a;		
	}
	public void setheight(int b)
	{
		height=b;
	}
	public int getwidth()
	{
		return width;
	}
	public int getheight()
	{
		return height;
	}
	public int getArea()
	{
		return width*height;
	}
	
}
