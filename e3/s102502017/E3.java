package ce1002.e3.s102502017;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int  select;
		int width;
		int height;
		System.out.println("Create rectangle in 2 ways.");
		//Determine whether the input is out of range or not
		while(true){
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			select = input.nextInt();
			if(select==1 || select == 2)break;
			else System.out.println("Out of range!");
		}
		//Determine whether the input is out of range or not
		while(true){
			System.out.println("Input the width: ");
			width=input.nextInt();
			if(width>0 && width<21)break;
			else System.out.println("Out of range!");
		}
		//Determine whether the input is out of range or not
		while(true){
			System.out.println("Input the height: ");
			height=input.nextInt();
			if(height>0 && height<21)break;
			else System.out.println("Out of range!");
		}
		if(select==1){
			Rectangle rectangle = new Rectangle(width,height);
			rectangle.showWidth();
			rectangle.showHeight();
			rectangle.showArea();
		}
		else {
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
			rectangle.showWidth();
			rectangle.showHeight();
			rectangle.showArea();
		}
		input.close();
	}
}


