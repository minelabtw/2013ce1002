package ce1002.e3.s102502546;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num, width, height = 0;

		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");

		num = input.nextInt();
		while (num != 1 && num != 2) {
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			num = input.nextInt();

		}
		System.out.println("Input the width: ");
		width = input.nextInt();
		while (width < 0 || width > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width = input.nextInt();
		}
		System.out.println("Input the height: ");
		height = input.nextInt();
		while (height < 0 || height > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			height = input.nextInt();
		}
		input.close() ;
		int answer = 0;//rectangle if柑 ┮砞﹚把计 р弄ㄓ
		if (num == 1) {
			Rectangle rectangle = new Rectangle(width, height);//弄篶琌Τ把计
			answer = rectangle.getArea();
		}

		if (num == 2) {
			Rectangle rectangle = new Rectangle();//弄篶⊿把计
			rectangle.setWidth(width);
			rectangle.setHeight(height);
			answer = rectangle.getArea();
		}
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + answer);

	}

}
