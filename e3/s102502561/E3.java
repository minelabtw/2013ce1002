package ce1002.e3.s102502561;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {

		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter");// ways
		Scanner input = new Scanner(System.in); // new scanner
		int way = input.nextInt();
		while (way != 1 && way != 2) { // choose way input
			System.out.println("Out of range !");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter");// ways
			way = input.nextInt();
		}

		System.out.println("Input the width:");
		int width = input.nextInt(); // width input
		while (width < 1 || width > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the width:");
			width = input.nextInt();
		}
		System.out.println("Input the height:");
		int height = input.nextInt(); // height input
		while (height < 1 || height > 20) {
			System.out.println("Out of range!");
			System.out.println("Input the height:");
			height = input.nextInt();
		}

		// �A�ӵ�rectangle
		Rectangle r ;
		if (way == 1) {//way1
			r =new Rectangle(width,height);
						System.out.println("The width of this rectangle is "+ width);// final
			// output
			System.out.println("The height of this rectangle is "+ height);
			System.out.println("The area of this rectangle is "+ r.getarea());
		} 
		else {//way2
			r =new Rectangle();
			r.setwidth(width);
			r.setheight(height);
			System.out.println("The width of this rectangle is "+ width);// final
			// output
			System.out.println("The height of this rectangle is "+ height);
			System.out.println("The area of this rectangle is "+ r.getarea());
		}

		

	}
}
