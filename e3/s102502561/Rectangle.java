package ce1002.e3.s102502561;

public class Rectangle {
	
	private int Width ;
	private int Height ;
	
	public Rectangle(int width, int height){//way1
		Width = width;
		Height = height;
	}
	public Rectangle(){//way2
		
	}
	public void setwidth(int width){//way2
		Width = width;
	}
	public void setheight(int height){//way2
		Height = height;
	}
	public int getarea(){//area
		return Width*Height;
	}
}
