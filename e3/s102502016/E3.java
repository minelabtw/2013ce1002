package ce1002.e3.s102502016;
import java.util.Scanner;
import ce1002.e3.s102502016.Rectangle;
public class E3 {
	
	public static void main(String[] args) {
		int way,width,height;
	Scanner input = new Scanner(System.in);
	System.out.println("Create rectangle in 2 ways.");
	System.out.println("1)create by constructor parameters,2)create by setter: ");
	way = input.nextInt();
	while (way != 1 && way != 2)
	{
		System.out.println("Out of range!");
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		way = input.nextInt();
	}
	System.out.println("Input the width: ");
	width = input.nextInt();
	while (width<1 || width>20)
	{System.out.println("Out of range!");
	System.out.println("Input the width: ");
	width = input.nextInt();
	}
	System.out.println("Input the height: ");
	height = input.nextInt();
	while (height<1 || height>20)
	{System.out.println("Out of range!");
	System.out.println("Input the height: ");
	height = input.nextInt();
	}
	if (way == 1)
	{Rectangle rectangle = new Rectangle(width,height);
	System.out.println("The width of this rectangle is "+rectangle.getWidth());
	System.out.println("The height of this rectangle is "+rectangle.getHeight());
	System.out.println("The area of this rectangle is "+rectangle.getArea());	
	}
	else
	{
	Rectangle rectangle = new Rectangle();
	rectangle.setWidth(width);
	rectangle.setHeight(height);
	System.out.println("The width of this rectangle is "+rectangle.getWidth());
	System.out.println("The height of this rectangle is "+rectangle.getHeight());
	System.out.println("The area of this rectangle is "+rectangle.getArea());
	}
	input.close();
}
}
