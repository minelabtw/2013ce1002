package ce1002.e3.s102502502;

public class Rectangle {
	private int width;
	private int height;
	public int getWidth(int w)            //回傳輸入的寬值
	{
		width = w;
		return width;
	}
	public int getHeight(int h)            //回傳輸入的高值
	{
		height = h;
		return height;
	}
	public Rectangle(int i, int j)        // the constructor of way 1
	{
		width = i;
		height = j;
	}
	public Rectangle(){}                  // the constructor of way 2
 void setWidth(int w1)                    // the setter of width 
 {
	 width = w1;
 }
void setHeight(int h1)                    // the setter of height 
{
	 height = h1;
}
public int getArea(int w,int h)           // the behavior of this object
{
	width = w;
	height = h;
	return width*height;
}
}
