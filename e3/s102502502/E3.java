package ce1002.e3.s102502502;
import java.util.Scanner;

public class E3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways."); 
		System.out.println("1)create by constructor parameters,2)create by setter: ");   //請求選擇方法  
		int n1 = input.nextInt();    
		while (n1 < 1 || n1 > 2)                                                  //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Create rectangle in 2 ways."); 
			System.out.println("1)create by constructor parameters,2)create by setter: "); 
			n1 = input.nextInt();
		}
		System.out.println("Input the width: ");                                        //請求輸入寬 
		int n2 = input.nextInt();    
		while (n2 < 1 || n2 > 20)                                                 //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Input the width: ");
			n2 = input.nextInt();
		}
		System.out.println("Input the height: ");                                       //請求輸入高
		int n3 = input.nextInt();    
		while (n3 < 1 || n3 > 20)                                                 //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Input the  height: ");
			n3 = input.nextInt();
		}
		switch(n1)
		{
		case 1:                                                                      //方法一
			Rectangle a = new Rectangle(n2,n3);                                    
			System.out.println("The width of this rectangle is " + a.getWidth(n2) );
			System.out.println("The height of this rectangle is " + a.getHeight(n3) );
			System.out.println("The width of this rectangle is " + a.getArea(n2,n3) );
			break;
		case 2:                                                                      //方法二
			Rectangle b = new Rectangle();
			b.setWidth(n2);// set the width
			b.setHeight(n3);// set the height
			System.out.println("The width of this rectangle is " + b.getWidth(n2) );
			System.out.println("The height of this rectangle is " + b.getHeight(n3) );
			System.out.println("The width of this rectangle is " + b.getArea(n2,n3) );
		}
	}
}