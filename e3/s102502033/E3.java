package ce1002.e3.s102502033;
import java.util.Scanner;


public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int method=0;//選擇方法
		int width=0;
		int height=0;
		
		System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
		Scanner input= new Scanner(System.in);
		method= input.nextInt();
		while(method!=1 && method!=2)//出錯
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.\n1)create by constructor parameters,2)create by setter:");
			method= input.nextInt();
		}
		
		System.out.println("Input the width: ");
		width=input.nextInt();
		while(width<1 || width>20)//出錯
		{
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width=input.nextInt();
		}
		
		System.out.println("Input the height: ");
		height=input.nextInt();
		while(height<1 || height>20)//出錯
		{
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			height=input.nextInt();
		}
		
		if(method==1)//給參數的方法
		{
			Rectangle rectangle= new Rectangle(width,height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}
		else//不給參數的方法
		{
			Rectangle rectangle= new Rectangle();
			rectangle.setwidth(width);
			rectangle.setheight(height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());
			
		}
		
		

	}

}
