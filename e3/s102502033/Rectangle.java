package ce1002.e3.s102502033;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle()//不給參數的建構子
	{
		
	}
	
	public Rectangle(int a, int b)//給參數的建構子
	{
		width=a;
		height=b;
	}
	
	public void setwidth(int a)
	{
		width=a;
	}
	
	public void setheight(int b)
	{
		height=b;
	}
	
	public int getArea()
	{
		return width*height;
	}
}
