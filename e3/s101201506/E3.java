package ce1002.e3.s101201506;
import java.util.Scanner;

public class E3 {
	private static Scanner scanner;
	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		int method; //方法!!
		int width; //寬
		int height;//長
		int area;//面積
		
		do{
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		method = scanner.nextInt();
		if(method<1 || method>2)
			System.out.println("Out of range!");
		}while(method<1 || method>2);
		
		do{
			System.out.println("Input the width: ");
			width = scanner.nextInt();
			
			if(width<1 || width>20)
				System.out.println("Out of range!");
			}while(width<1 || width>20);
			
			do{
				System.out.println("Input the height: ");
				height = scanner.nextInt();
				
				if(height<1 || height>20)
					System.out.println("Out of range!");
				}while(height<1 || height>20);
		
		switch(method){  //看用方法一還是二
			case 1:
				Rectangle rectangle = new Rectangle(width,height);
				area=rectangle.getArea();
				break;
			
			default:
				Rectangle rectangle2 = new Rectangle();
				rectangle2.setWidth(width);
				rectangle2.setHeight(height);
				area=rectangle2.getArea();
			
			
		}
			//輸出!!
		System.out.println("The width of this rectangle is " + width);
		System.out.println("The height of this rectangle is " + height);
		System.out.println("The area of this rectangle is " + area);
	
		
	}
	

}
