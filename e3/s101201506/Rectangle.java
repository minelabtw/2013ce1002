package ce1002.e3.s101201506;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle(int a,int b){
		width=a;
		height=b;
	}
	public Rectangle(){
		
	}
	public void setWidth(int n){
		width=n;
	}
	public void setHeight(int m){
		height=m;
	}
	public int getArea(){
		return width*height;
	}
}