package ce1002.e3.s102502560;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		
		Scanner STDIN=new Scanner(System.in);
		
		int mode;
		
		while(true){
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			mode=STDIN.nextInt();
			if(mode==1||mode==2){break;}
			System.out.println("Out of range!");
		}
		
		int width, height;
		
		while(true){
			System.out.println("Input the width:");
			width=STDIN.nextInt();
			if(width>=1&&width<=20){break;}
			System.out.println("Out of range!");
		}
		
		while(true){
			System.out.println("Input the height:");
			height=STDIN.nextInt();
			if(height>=1&&height<=20){break;}
			System.out.println("Out of range!");
		}
		
		STDIN.close();
		
		Rectangle rectangle;
		
		if(mode==1){
			rectangle = new Rectangle(width, height);	//mode 1: initial members by constructor
		}else if(mode==2){
			rectangle = new Rectangle();				//mode 2: initial members by setters
			rectangle.setWidth(width);
			rectangle.setHeight(height);
		}else{
			rectangle = new Rectangle();				//hold for default, prevent null object
		}
		
		System.out.println("The width of this rectangle is "+rectangle.getWidth());
		System.out.println("The height of this rectangle is "+rectangle.getHeight());
		System.out.println("The area of this rectangle is "+rectangle.getArea());
		
	}

}
