package ce1002.e3.s102502521;
import java.util.*;

public class E3 {

	/**
	 * @param args
	 */
	private static Scanner scanner;
	private static Rectangle rectangle;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//設定變數
		int way=0;
		int height=0;
		int width=0;
		scanner=new Scanner(System.in);
		
		//判斷值
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			
			way=scanner.nextInt();
			
			if(way>2||way<1){
				System.out.println("Out of range!");
			}
		}while(way>2||way<1);
		
		do{
			System.out.println("Input the width: ");
			
			width=scanner.nextInt();
			
			if(width<1||width>20){
				System.out.println("Out of range!");
			}
		}while(width<1||width>20);
		
		do{
			System.out.println("Input the height: ");
			
			height=scanner.nextInt();
			
			if(height<1||height>20){
				System.out.println("Out of range!");
			}
		}while(height<1||height>20);
		
		//判斷方法
		if(way==1){
			rectangle = new Rectangle(width,height);
		}
		else{
			rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
		}
		
		//列印出結果
		rectangle.show();
	}

}
