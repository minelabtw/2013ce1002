package ce1002.e3.s102502521;

public class Rectangle {

	//變數
	private int width;
	private int height;
	
	public Rectangle(){
		
	}
	
	//方法1
	public Rectangle(int width,int height){
		this.width=width;
		this.height=height;
	}
	
	//方法2
	public void setWidth(int width){
		this.width=width;
	}
	
	public void setHeight(int height){
		this.height=height;
	}
	
	//算面積
	public int getArea(){
		return width*height;
	}
	
	//列印
	public void show(){
		System.out.println("The width of this rectangle is "+width);
		System.out.println("The height of this rectangle is "+height);
		System.out.println("The area of this rectangle is "+getArea());
	}
}	
