package ce1002.e3.s102502035;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle() {
		width = 0;
		height = 0;
	}// 建構子

	public Rectangle(int w, int h) {
		width = w;
		height = h;
	}// overlad 建構子

	public void setWidth(int w) {
		width = w;
	}// 設寬

	public void setHeight(int h) {
		height = h;
	}// 設高

	public int getWidth() {
		return width;
	}// 得寬

	public int getHeight() {
		return height;
	}// 得高

	public int getArea() {
		return height * width;
	}// 得積
}
