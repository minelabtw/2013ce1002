package ce1002.e3.s102502035;

import java.util.Scanner;//導入輸入函式庫

public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// Scanner class input object
		System.out.println("Create rectangle in 2 ways.");
		System.out
				.println("1)create by constructor parameters,2)create by setter: ");// 提示輸入
		int form = input.nextInt();// 輸入
		while (form != 1 && form != 2) {
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter: ");// 提示輸入
			form = input.nextInt();// 輸入
		}
		if (form == 1) {
			System.out.println("Input the width: ");// 提示輸入
			int w = input.nextInt();// 輸入
			while (w < 1 || w > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");// 提示輸入
				w = input.nextInt();// 輸入
			}
			System.out.println("Input the height: ");// 提示輸入
			int h = input.nextInt();// 輸入
			while (h < 1 || h > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");// 提示輸入
				h = input.nextInt();// 輸入
			}
			Rectangle rectangle = new Rectangle(h, w);// 建立物件並傳值
			System.out.println("The width of this rectangle is "
					+ rectangle.getWidth());
			System.out.println("The height of this rectangle is "
					+ rectangle.getHeight());
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());// 輸出結果
		} else {
			System.out.println("Input the width: ");// 提示輸入
			int w = input.nextInt();// 輸入
			while (w < 1 || w > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the width: ");// 提示輸入
				w = input.nextInt();// 輸入
			}
			System.out.println("Input the height: ");// 提示輸入
			int h = input.nextInt();// 輸入
			while (h < 1 || h > 20) {
				System.out.println("Out of range!");
				System.out.println("Input the height: ");// 提示輸入
				h = input.nextInt();// 輸入
			}
			Rectangle rectangle = new Rectangle();// 建立物件不傳值
			rectangle.setWidth(h);// set the width
			rectangle.setHeight(w);// set the height
			System.out.println("The width of this rectangle is "
					+ rectangle.getWidth());
			System.out.println("The height of this rectangle is "
					+ rectangle.getHeight());
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());// 輸出結果
		}

	}
}
