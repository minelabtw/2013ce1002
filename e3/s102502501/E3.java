package ce1002.e3.s102502501;
import java.util.Scanner;
public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner( System.in);
		int number = 1;
		int width = 1;
		int height = 1;
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter:");
		number = input.nextInt();
		while(number < 1 || number > 2)
		{
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			number  = input.nextInt();
		}
		switch(number) { //方法一和方法二
		case 1 :
			do {
				System.out.println("Input the width: ") ;  //輸入寬
				width = input.nextInt() ;
				if (width < 1 || width > 20 )
					System.out.println("Out of range!") ;
			}
			while (width < 1 || width > 20 ) ;
			
			do {
				System.out.println("Input the height: ") ;  //輸入長
				height = input.nextInt() ;
				if (height < 1 || height > 20 )
					System.out.println("Out of range!") ;
			}
			while (height < 1 || height > 20 ) ;
			
			Rectangle rectangle = new Rectangle(width , height) ; //使用建構子+參數初始化物件
			
			rectangle.Area(); //呼叫面積公式
			
			break;
		case 2 :
			do {
				System.out.println("Input the width: ") ;  //輸入寬
				width = input.nextInt() ;
				if (width < 1 || width > 20 )
					System.out.println("Out of range!") ;
			}
			while (width < 1 || width > 20 ) ;
			
			do {
				System.out.println("Input the height: ") ;  //輸入長
				height = input.nextInt() ;
				if (height < 1 || height > 20 )
					System.out.println("Out of range!") ;
			}
			while (height < 1 || height > 20 ) ;
			
			Rectangle rectangle2 = new Rectangle(); //使用setter設定物件
			rectangle2.setWidth( width );// set the width
			rectangle2.setHeight( height );// set the height
			
			rectangle2.Area();
			
			break ;
		}
		
		input.close();

	}
	

}
