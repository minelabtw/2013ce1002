package ce1002.e3.s102502501;

public class Rectangle {

private int w, h , area ; //宣告三變數
	
	public Rectangle(int width,int height ) { //方法一 會傳值
		w = width;
		h = height;
	}
	
	public Rectangle() { //第二個方法並不會傳值 故空白

	}
	
	public void setWidth(int width){
		w = width;
	}
	
	public void setHeight(int height) {
		h = height ;
	}
	
	public void Area() {         //面積公式及輸出
		area = w * h ;
		System.out.println("The width of this rectangle is " + w);
		System.out.println("The height of this rectangle is " + h);
		System.out.println("The area of this rectangle is " + area);
	}


}
