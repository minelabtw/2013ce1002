package ce1002.e3.s102502021;
import java.util.Scanner;

public class E3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int w;
		int l;
		int choose;
		do 
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			choose = input.nextInt();
			if(choose > 2 || choose < 1)
			{
				System.out.println("Out of range!");
			}
			
		}
		while(choose > 2 || choose < 1);
		do
		{
			System.out.println("Input the width: ");
			w = input.nextInt();
			if(w>20 || w<1)
			{
				System.out.println("Out of range!");
			}
		}
		while(w>20 || w<1);
		do
		{
			System.out.println("Input the width: ");
			l = input.nextInt();
			if(l>20 || l<1)
			{
				System.out.println("Out of range!");
			}
		}
		while(l>20 || l<1);
		switch(choose)
		{
		case 1 :
		{
			Rectangle rectangle = new Rectangle(w,l);
			System.out.println("The width of this rectangle is " + w );
			System.out.println("The height of this rectangle is " + l );
			System.out.println("The area of this rectangle is "+ rectangle.Area());
			break;
		}
		case 2:
		{
			Rectangle rectangle = new Rectangle();
			rectangle.getwidth(w);
			rectangle.getheight(l);
			System.out.println("The width of this rectangle is " + w );
			System.out.println("The height of this rectangle is " + l );
			System.out.println("The area of this rectangle is "+ rectangle.Area());
			break;
		}
		}
		input.close();

	}

}
