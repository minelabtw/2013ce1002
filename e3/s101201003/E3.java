package ce1002.e3.s101201003;

import java.util.Scanner;
public class E3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		Scanner number=new Scanner(System.in);//宣告Scanner類別的物件
		int choose=0,_width=0,_height=0;
		
		do{
			System.out.println("Create rectangle in 2 ways.");
			System.out.print("1)create by constructor parameters,2)create by setter: ");
			choose=number.nextInt();
			if(choose<1||choose>2)
				System.out.println("Out of range!");
		}while(choose<1||choose>2);//問欲顯示的方法,如果輸入範圍錯誤就重新輸入
		
		do{
			System.out.print("Input the width: ");
			_width=number.nextInt();
			if(_width<1||_width>20)
				System.out.println("Out of range!");
		}while(_width<1||_width>20);//問欲顯示的寬,如果輸入範圍錯誤就重新輸入
		
		do{
			System.out.print("Input the height: ");
			_height=number.nextInt();
			if(_height<1||_height>20)
				System.out.println("Out of range!");
		}while(_height<1||_height>20);//問欲顯示的高,如果輸入範圍錯誤就重新輸入
		
		if(choose==1){//方法一 建構子+參數初始化物件
			Rectangle rectangle=new Rectangle(_width,_height);
			System.out.println("The width of this rectangle is "+_width);
			System.out.println("The height of this rectangle is "+_height);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
		else if(choose==2){//方法二 setter設定物件
			Rectangle rectangle=new Rectangle();
			rectangle.setWidth(_width);
			rectangle.setHeight(_height);
			System.out.println("The width of this rectangle is "+_width);
			System.out.println("The height of this rectangle is "+_height);
			System.out.println("The area of this rectangle is " + rectangle.getArea());
			
		}
		number.close();
	}

}
