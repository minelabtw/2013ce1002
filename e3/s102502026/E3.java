package ce1002.e3.s102502026;
import java.util.Scanner;
public class E3 {
	private static Scanner input;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input = new Scanner(System.in);
		int i=0;
		int w=0;
		int h=0;
	
	do
	{
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		i= input.nextInt();
		if(i!= 1 && i!=2)
			System.out.println("Out of Range!");
	}while(i!= 1 && i!=2);	//defining what way to do 1 or 2
	
	do
	{
		System.out.println("Input the width: ");
		w= input.nextInt();
		if(w<1 || w>20)
			System.out.println("Out of Range!");
	}while(w<1 || w>20);	//ask width
	do	
	{
		System.out.println("Input the height: ");
		h= input.nextInt();
		if(h<1 || h>20)
			System.out.println("Out of Range!");
	}while(h<1 || h>20);	//ask height
	
	switch (i)	//cases 1, 2
	{
	case 1:
	{
		Rectangle rectangle = new Rectangle(w, h); //do Rectangle class of Rectangle(int, int)
		System.out.println("The width of this rectangle is "+ w);
		System.out.println("The height of this rectangle is "+ h);
		System.out.println("The area of this rectangle is "+ rectangle.getArea() );
		break;
	}
	case 2:
	{
		Rectangle rectangle = new Rectangle();	//do Rectangle class of Rectangle()
		rectangle.setWidth(w);// set the width into Rectangle()
		rectangle.setHeight(h);// set the height into Rectangle()
		System.out.println("The width of this rectangle is "+ w);
		System.out.println("The height of this rectangle is "+ h);
		System.out.println("The area of this rectangle is "+ rectangle.getArea() );
		break;
	}
	}
	}
	
}//end

