package ce1002.e3.s102502026;

public class Rectangle {
	private int width;
	private int height;
	
	public Rectangle(int w, int h)
	{
		width= w;
		height= h;
	}
	
	public Rectangle()
	{
		
	}
	
	void setWidth(int w)
	{
		width=w;
	}
	
	void setHeight(int h)
	{
		height=h;
	}
	
	int getArea()
	{
		return width*height;
	}
	
}
