package ce1002.e3.s995001561;

class Rectangle {

	private int width;
	private int height;

	
	Rectangle(int i, int j) {
		width = i;
		height = j;
	}
	
	
	public Rectangle() {
		width=1;
		height=1;
	}

	
	public void setWidth(int width) {
		this.width = width;
	}
		

	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getArea() {
		return width * height;
	}
	
	

}
