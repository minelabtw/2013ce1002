package ce1002.e3.s995001561;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		
		int n;
		int width;
		int height;
		
		// choose method 1 or 2
		System.out.println( "Create rectangle in 2 ways." );
		System.out.println( "1)create by constructor parameters,2)create by setter: " );
		Scanner input = new Scanner(System.in);	
		n = input.nextInt();
		
		while( n != 1 && n != 2) {
		System.out.println( "Out of range!\nCreate rectangle in 2 ways." );
		System.out.println( "1)create by constructor parameters,2)create by setter: " );
		n = input.nextInt();
		}
		
		// use method 1
		if(n==1) {
			System.out.println( "Input the width: " );
			width = input.nextInt();
			while( width < 1 || width >20) {
				System.out.println( "Out of range!\nInput the width: " );
				width = input.nextInt();
				}
			System.out.println( "Input the height: " );
			height = input.nextInt();
			while( height < 1 || height >20) {
				System.out.println( "Out of range!\nInput the height: " );
				height = input.nextInt();
				}
			Rectangle rectangle = new Rectangle(width, height);
			
			System.out.println( "The width of this rectangle is " + width );
			System.out.println( "The height of this rectangle is " + height );
			System.out.println( "The area of this rectangle is "+ rectangle.getArea());
		}
		
		// use method 2
		else {
			System.out.println( "Input the width: " );
			width = input.nextInt();
			while( width < 1 || width >20) {
				System.out.println( "Out of range!\nInput the width: " );
				width = input.nextInt();
				}
			System.out.println( "Input the height: " );
			height = input.nextInt();
			while( height < 1 || height >20) {
				System.out.println( "Out of range!\nInput the height: " );
				height = input.nextInt();
				}
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			
			System.out.println( "The width of this rectangle is " + width );
			System.out.println( "The height of this rectangle is " + height );
			System.out.println( "The area of this rectangle is "+ rectangle.getArea());
		}
		
   input.close();
   
	}

}