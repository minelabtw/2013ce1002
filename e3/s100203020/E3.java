package ce1002.e3.s100203020;
import java.util.Scanner;

public class E3 {
	
	private static Scanner scanner;

	//compare range
	public static void testrange(int number, int min, int max){
		if(number < min || number > max)
			System.out.println("Out of range!");
	}
	
		
		
	
	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		
		
		
		//method select
		int methodSelect;
		do{			
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			methodSelect = scanner.nextInt();
			testrange(methodSelect,1,2);
		} while (methodSelect < 1 || methodSelect > 2);
		
		
		
		//input width
		int width;
		do{
			System.out.println("Input the width: ");
			
			width = scanner.nextInt();
			testrange(width,1,20);
		}while (width < 1 || width > 20);
		
		//input height
		int height;
		do{
			System.out.println("Input the height: ");
			
			height = scanner.nextInt();
			testrange(height,1,20);
		}while (height < 1 || height > 20);
		
		
		//method 1
		if(methodSelect==1)	{ 
			Rectangle rectangle = new Rectangle(width, height);
			
			System.out.println("The width of this rectangle is " + rectangle.getWidth());
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
			}
		//method 2
		else{
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);
			rectangle.setHeight(height);
			
			System.out.println("The width of this rectangle is " + rectangle.getWidth());
			System.out.println("The height of this rectangle is " + rectangle.getHeight());
			System.out.println("The area of this rectangle is " + rectangle.getArea());
		}
		
		
	}

}

