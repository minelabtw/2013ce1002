package ce1002.e3.s100203020;

public class Rectangle {
	
	private int width;
	private int height;
	
	public Rectangle() {
	}
	public Rectangle(int W,int H) {
		this.width = W;
		this.height = H;
	}
	
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getArea() {
		return height*width;
	}


}
