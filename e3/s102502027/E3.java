package ce1002.e3.s102502027;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {
		int width; // 宣告
		int height;
		int ways;
		Scanner input = new Scanner(System.in);
		Scanner input1 = new Scanner(System.in);
		Scanner input2 = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out
				.println("1)create by constructor parameters,2)create by setter: ");
		ways = input.nextInt();
		while (ways != 1 && ways != 2) { //方法判斷
			System.out.println("Out of range!");
			System.out
					.println("1)create by constructor parameters,2)create by setter: ");
			ways = input.nextInt();
		}
		System.out.println("Input the width: ");
		width = input1.nextInt();
		while (width > 20 || width < 0) { //是否符合
			System.out.println("Out of range!");
			System.out.println("Input the width: ");
			width = input.nextInt();
		}

		System.out.println("Input the height: ");
		height = input2.nextInt();
		while (height > 20 || height < 0) {
			System.out.println("Out of range!");
			System.out.println("Input the height: ");
			height = input.nextInt();
		}

		if (ways == 1) { //法一的計算
			Rectangle rectangle = new Rectangle(width, height);
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());
		}
		if (ways == 2) { //法二的計算
			Rectangle rectangle = new Rectangle();
			rectangle.setWidth(width);// set the width
			rectangle.setHeight(height);// set the height
			System.out.println("The width of this rectangle is " + width);
			System.out.println("The height of this rectangle is " + height);
			System.out.println("The area of this rectangle is "
					+ rectangle.getArea());

		}

	}

}
