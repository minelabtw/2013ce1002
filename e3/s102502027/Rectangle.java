package ce1002.e3.s102502027;

import java.util.Scanner;

public class Rectangle {
	private int width;
	private int height;

	public Rectangle() { //建構子
		width = 0;
		height = 0;
	}
	public Rectangle(int Width,int Height) { //建構子
		width = Width;
		height = Height;
	}
	public void setWidth(int Width) {
		width=Width;
	}
	public void setHeight(int Height) {
		height=Height;
	}
    public int getArea(){ //計算面積
    	return height*width;
    }
}
