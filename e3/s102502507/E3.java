package ce1002.e3.s102502507;
import java.util.Scanner;
public class E3 {
private static Scanner input;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input=new Scanner(System.in);
		int i;//宣告變數
		int w;
		int h;
		do
		{
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter:");
			i=input.nextInt();
			if(i!=1 && i!=2)//設定只能輸入1或2
			{
				System.out.println("Out of range!");
			}
		}
		while(i!=1 && i!=2);
		do
		{
			System.out.println("Input the width: ");
			w=input.nextInt();
			if(w<1 || w>20)
			{
				System.out.println("Out of range!");
			}
		}//限制寬
		while(w<1 || w>20);
		do
		{
			System.out.println("Input the height: ");
			h=input.nextInt();
			if(h<1 || h>20)
			{
				System.out.println("Out of range!");
			}
		}//限制高
		while(h<1 || h>20);
		
	switch(i)
	{
	case 1:
	{
		Rectangle rectangle=new Rectangle(w,h);//呼應class
		System.out.println("The width of this rectangle is "+ w);
		System.out.println("The height of this rectangle is "+ h);
		System.out.println("The area of this rectangle is "+ rectangle.getArea());
		break;
	}
	case 2:
	{
		Rectangle rectangle=new Rectangle();//呼應class
		rectangle.setWidth(w);
		rectangle.setHeight(h);
		System.out.println("The width of this rectangle is "+ w);
		System.out.println("The height of this rectangle is "+ h);
		System.out.println("The area of this rectangle is "+ rectangle.getArea());
		break;
	}
	}
		input.close();
	}

}
