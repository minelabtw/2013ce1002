package ce1002.e3.s102502507;

public class Rectangle {

	private int width;
	private int height;
	
    public  Rectangle (int w,int h)
	{
		width=w;
		height=h;
	}
	public Rectangle()
	{
		
	}
	public void setWidth(int w)
	{
		width=w;
	}
	public void setHeight(int h)
	{
		height=h;
	}
    public int getArea()
	{ 
		return width*height;
	}
}
