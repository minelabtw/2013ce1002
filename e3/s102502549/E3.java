package ce1002.e3.s102502549;

import java.util.Scanner;

public class E3 {

	public static void main(String[] args) {

		int mode, w, h;
		Scanner input = new Scanner(System.in);

		// 輸入並檢查模式
		do {
			System.out.println("Create rectangle in 2 ways.");
			System.out
					.println("1)create by constructor parameters,2)create by setter: ");
			mode = input.nextInt();

			if (mode != 1 && mode != 2) {
				System.out.println("Out of range!");
			}
		} while (mode != 1 && mode != 2);

		//輸入並檢查寬度
		do {
			System.out.println("Input the width: ");
			w = input.nextInt();

			if (w <= 0) {
				System.out.println("Out of range!");
			}
		} while (w <= 0);

		//輸入並檢查高度
		do {
			System.out.println("Input the height: ");
			h = input.nextInt();

			if (h <= 0) {
				System.out.println("Out of range!");
			}
		} while (h <= 0);

		//模式1
		if (mode == 1) {
			Rectangle rectangle = new Rectangle(w, h);
			System.out.println("The width of this rectangle is "+ rectangle.getwidth());
			System.out.println("The height of this rectangle is "+ rectangle.getheight());
			System.out.println("The area of this rectangle is "+ rectangle.getArea());
		} 
		
		//模式2
		else {
			Rectangle rectangle = new Rectangle();
			rectangle.setwidth(w);
			rectangle.setheight(h);
			System.out.println("The width of this rectangle is "+ rectangle.getwidth());
			System.out.println("The height of this rectangle is "+ rectangle.getheight());
			System.out.println("The area of this rectangle is "+ rectangle.getArea());
		}
	}
}
