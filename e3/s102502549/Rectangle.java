package ce1002.e3.s102502549;

public class Rectangle {

	private int width;
	private int height;

	//甚麼都沒有的建構式
	public Rectangle(){
	}
	
	//有初始值的建構式
	public Rectangle(int w, int h) {
		width = w;
		height = h;
	}

	//設定寬度
	public void setwidth(int w) {
		width = w;
	}

	//設定高度
	public void setheight(int h) {
		height = h;
	}

	//取得寬度
	public int getwidth() {
		return width;
	}
	
	//取得高度
	public int getheight() {
		return height;
	}
	
	//取得面積
	public int getArea() {
		return width * height;
	}
}
