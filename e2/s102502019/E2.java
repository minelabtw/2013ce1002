package ce1002.e2.s102502019;
import java.util.*;
public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Please input first number (3~10): ");  //輸出
	      Scanner input = new Scanner(System.in);                //輸入
	      Random kk = new Random();                              //產生隨機變數
	      int l=input.nextInt();
	      while(l<3 || l>10)                                     //輸入條件
	      {
	      System.out.print("Out of range!\n"+"Please input first number (3~10): ");
	      l=input.nextInt();
		}
	    System.out.print("Please input second number (3~10): ");
	    int w=input.nextInt();
	    while(w<3 || w>10)
	    {
	    	 System.out.print("Out of range!\n"+"Please input second number (3~10): ");
	         w=input.nextInt();
	    }
	    int [][] p= new int [l][w];                             //宣告陣列
	    for(int y=0; y<w; y++)                                  //將值存入陣列
	    {
	    	for(int x=0; x<l; x++)
	    	{
	    		p[x][y]=kk.nextInt(2);
	    	}
	    }
	    for(int y=0; y<w; y++)                                  //輸出陣列
	    {
	    	for(int x=0; x<l; x++)
	    	{
	    		System.out.print(p[x][y]);
	    	}
	    	System.out.print("\n");
	    }
	    int t=0;
	    for(int y=0; y<w; y++)                                  //判定相鄰
	    {  
	    	for(int x=0; x<l-1; x++)
	    	{
	    		if(p[x][y]==1 && p[x+1][y]==1)
	    			t=t+1;	
	    	}
	    }
	    for(int x=0; x<l; x++)
	    {  
	    	for(int y=0; y<w-1; y++)
	    	{
	    		if(p[x][y]==1 && p[x][y+1]==1)
	    			t=t+1;	
	    	}
	    }
    System.out.print("total: "+t);
	}

}
