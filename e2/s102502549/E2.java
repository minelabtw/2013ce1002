package ce1002.e2.s102502549;

import java.util.Random;
import java.util.Scanner;

public class E2 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Random random = new Random();// 宣告Random物件
		int l, w, count = 0;// 長度，寬度，1連續出現次數

		do// 輸入並檢查
		{
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();

			if (l < 3 || l > 10)
				System.out.println("Out of range!");
		} while (l < 3 || l > 10);

		do// 輸入並檢查
		{
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();

			if (w < 3 || w > 10)
				System.out.println("Out of range!");
		} while (w < 3 || w > 10);

		int arr[][] = new int[l][w];

		for (int i = 0; i < l; i++)// 塞亂數
		{
			for (int j = 0; j < w; j++) {
				arr[i][j] = random.nextInt(2);
			}
		}

		for (int i = 0; i < l; i++)// 印陣列
		{
			for (int j = 0; j < w; j++) {
				System.out.print(arr[i][j]+" ");
			}

			System.out.println();
		}

		for (int j = 0; j < w - 1; j++)// 檢查每列1連續出現次數
		{
			for (int i = 0; i < l; i++) {
				if (arr[i][j] == 1 && arr[i][j + 1] == 1)
					count++;
			}
		}

		for (int i = 0; i < l - 1; i++)// 檢查每行1連續出現次數
		{
			for (int j = 0; j < w; j++) {
				if (arr[i][j] == 1 && arr[i + 1][j] == 1)
					count++;
			}
		}

		System.out.print("total: " + count);// 印total
	}
}
