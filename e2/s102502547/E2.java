package ce1002.e2.s102502547;
import java.util.Scanner;
import java.util.Random;
public class E2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); 
		int l=0;
		int w=0;
		do
		{
		System.out.print("Please input first number (3~10): "); //輸入列，範圍不合的話重新輸入
		l = scanner.nextInt();
		if(l<3 || l>10)
			System.out.println("Out of range!");
		}while(l<3 || l>10);
		
		do
		{
		System.out.print("Please input second number (3~10): "); //輸入行，範圍不合的話重新輸入
		w = scanner.nextInt();
		if(w<3 || w>10)
			System.out.println("Out of range!");
		}while(w<3 || w>10);

		int[][] a;
		a=new int[l][w]; //宣告陣列，行列數為剛才輸入的值
		Random ran = new Random(); //隨機產生0,1
		for(int i=0;i<l;i++)
		{
			for(int j=0;j<w;j++)
			{
				a[i][j]=ran.nextInt(2);
				System.out.print(a[i][j]);
		        System.out.print(" ");
			}
	        System.out.println();
		}
		
		int x=0;
		for(int i=0;i<l;i++)
		{
			for(int j=0;j<w-1;j++)
			{
				if(a[i][j]==1 && a[i][j+1]==1) //計算水平連續的1
					x++;
			}
		}
		
		for(int j=0;j<w;j++)
		{
			for(int i=0;i<l-1;i++)
			{
				if(a[i][j]==1 && a[i+1][j]==1) //計算垂直連續的1
					x++;
			}
		}
		
		System.out.print("Total: "+x);
	}	
		
}
