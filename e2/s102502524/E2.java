package ce1002.e2.s102502524;

import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int l = 0;													// 設定變數
		int w = 0;
		int c = 0;
		
		Scanner input = new Scanner(System.in);						//使用者輸入
		
		System.out.print("Please input first number (3~10): ");		//輸入長
		l = input.nextInt();
		while(l<3 || l>10)											//判斷範圍是否符合
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
		}
		
		System.out.print("Please input second number (3~10): ");	//輸入寬
		w = input.nextInt();
		while(w<3 || w>10)											//判斷範圍是否符合
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
		}
		
		int[][]a = new int [l][w];
		
		for(int k=0;k<l;k++)										//亂數放入陣列中
		{
			for(int i=0;i<w;i++)
			{
				a[k][i]=(int)(Math.random()*2);
				System.out.print(a[k][i]+" ");
			}
			System.out.println();
		}
		
		for(int m=0;m<l;m++)										//橫向檢查相鄰的1
		{
			for(int n=0;n<w-1;n++)
			{
				if(a[m][n]==a[m][n+1] && a[m][n]==1)
					c++;
			}
		}

		for(int m=0;m<w;m++)										//直向檢查相鄰的1
		{
			for(int n=0;n<l-1;n++)
			{
				if(a[n][m]==a[n+1][m] && a[n][m]==1)
					c++;
			}
		}
		
		System.out.println("total: " + c);							//輸出統計結果
	}
}
