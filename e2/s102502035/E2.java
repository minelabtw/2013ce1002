package ce1002.e2.s102502035;

import java.util.Scanner;//導入輸入
import java.util.Random;//導入亂數

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// input object
		Random rand = new Random();// rand object
		System.out.print("Please input first number (3~10): ");// 提示輸入
		int l = input.nextInt();// 輸入
		while (l < 3 || l > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");// 提示輸入
			l = input.nextInt();// 輸入
		}
		System.out.print("Please input second number (3~10): ");// 提示輸入
		int w = input.nextInt();// 輸入
		while (w < 3 || w > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");// 提示輸入
			w = input.nextInt();// 輸入
		}
		int[][] array = new int[l][w];// 宣告變數
		int total = 0;// 宣告變數
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < w; j++) {
				array[i][j] = rand.nextInt(2);// 存亂數
			}
		}
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < w; j++) {
				System.out.print(array[i][j] + " ");// 輸出亂數
			}
			System.out.println();// 換行
		}
		for (int i = 0; i < l - 1; i++) {
			for (int j = 0; j < w - 1; j++) {
				if (array[i][j] == 1 && array[i + 1][j] == 1)// 計算相連
					total++;
				if (array[i][j] == 1 && array[i][j + 1] == 1)// 計算相連
					total++;
			}
		}
		for (int i = 0; i < l - 1; i++) {
			if (array[i][w - 1] == 1 && array[i + 1][w - 1] == 1)// 尾巴相連
				total++;
		}
		for (int j = 0; j < w - 1; j++) {
			if (array[l - 1][j] == 1 && array[l - 1][j + 1] == 1)// 尾巴相連
				total++;
		}
		System.out.print("total: " + total);// 輸出結果
	}
}