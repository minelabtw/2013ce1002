package ce1002.a1.s102502518;

import java.util.Scanner;
public class A1 {
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int number;
        Scanner input = new Scanner(System.in);
		System.out.print("Please input a number (5~30): ");
        number = input.nextInt();
        
        while (number<5 || number>30) {
            System.out.print("Out of range!\n" + "Please input again (5~30): ");
            number = input.nextInt();
        }//判定數字在5~30之間
        
        for (int i=1; i <= number; i++) {
        	for (int j=1; j <= number; j++) {
        		if ((i+j) % 2 == 0)
        		    System.out.print("X");
        	    else
        		    System.out.print("O");
        	}      	
        	System.out.print("\n");
        }//畫棋盤
	}
}
