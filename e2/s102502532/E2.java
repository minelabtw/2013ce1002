package ce1002.e2.s102502532;

import java.util.Scanner; //匯入Scanner類別

public class E2 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); // 建立Scanner物件

		System.out.print("Please input first number (3~10): ");
		int l = input.nextInt();
		while (l < 3 || l > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		int w = input.nextInt();
		while (w < 3 || w > 10) {
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
		}

		int[][] array; // 宣告二維陣列
		array = new int[l][w];

		for (int y = 0; y < l; y++) { // 給值
			for (int x = 0; x < w; x++) {
				array[y][x] = (int) (Math.random() * 10) % 2;// 亂數
			}
		}

		for (int y = 0; y < l; y++) { // 印出陣列
			for (int x = 0; x < w; x++) {
				System.out.print(array[y][x]);
				System.out.print(" ");
			}
			System.out.println("");
		}

		int totalnum = 0;

		for (int y = 0; y < l - 1; y++) { // 記得-1
			for (int x = 0; x < w - 1; x++) {
				if (array[y][x] == 1) {
					if (array[y][x] == array[y][x + 1]) {// 因為這裡+1(會超出for的範圍
						totalnum++;
					}
				}
			}
		}
		for (int x = 0; x < w - 1; x++) {
			for (int y = 0; y < l - 1; y++) {
				if (array[y][x] == 1) {
					if (array[y][x] == array[y + 1][x]) {
						totalnum++;
					}
				}
			}
		}
		System.out.print("total: ");
		System.out.print(totalnum);
	}

}
