package ce1002.e2.s102502040;

import java.util.Scanner;

public class E2 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//設定Scanner輸入器讓我們輸入兩個初始值,分別為行.列
		Scanner input = new Scanner(System.in);
		System.out.print("Please input first number (3~10):");
		int number1 = input.nextInt();
		//大小
		while (number1 < 3 || number1 > 10){
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10):");
			number1 = input.nextInt();
		}
		System.out.print("Please input second number (3~10):");
		int number2 = input.nextInt();
		while (number2 < 3 || number2 > 10){
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10):");
			number2 = input.nextInt();
		}
		//設定二維振烈
		int arr[][];
		arr = new int[number1][number2];
		for(int i=0 ; i <=number1 -1 ; i++ ){
			for (int j=0 ; j <= number2 -1 ; j++ ){
				//陣列內容為0或1的隨機變數
				int vary = (int)(Math.random() * 2);
				arr[i][j]= vary ;
				System.out.print(arr[i][j]);
			}				
			System.out.print("\n");
		}
		//判斷有幾個鄰近的1
		int a=0;
		int b=0;
		for(int i=0 ; i <=number1 - 1 ; i++ ){
			for (int j=0 ; j <= number2 -2 ; j++ ){
				if(arr[i][j]==arr[i][j+1] && arr[i][j]==1 && arr[i][j+1]==1){
					a = a+1;
				}
			}
		}
		for(int i=0 ; i <=number1 -2 ; i++ ){
			for (int j=0 ; j <= number2 -1 ; j++ ){
				if(arr[i][j]==arr[i+1][j] && arr[i][j]==1 && arr[i+1][j]==1){
					b = b+1;
				}
			}
		}
		int all = a + b;
		System.out.print("total: " + all );
	}
}