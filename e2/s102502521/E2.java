package ce1002.e2.s102502521;
import java.util.*;

public class E2 {

	/**
	 * @param args
	 */
	private static Scanner scanner;
	private static Random random;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int fnum=0;    //設定變數
		int snum=0;
		int count=0;
		int [][] arr;
		arr=new int [9][9];
			
		scanner=new Scanner(System.in);
		random=new Random();
		
		//判斷值的正確
		do{
			System.out.print("Please input first number (3~10): ");
			fnum=scanner.nextInt();
			if(fnum>10||fnum<3){
				System.out.println("Out of range!");
			}
		}while(fnum>10||fnum<3);
		
		do{
			System.out.print("Please input second number (3~10): ");
			snum=scanner.nextInt();
			if(snum>10||snum<3){
				System.out.println("Out of range!");
			}
		}while(snum>10||snum<3);
		
		//儲存數字進陣列
		for(int y=0;y<fnum;y++){
			for(int x=0;x<snum;x++){
				arr[x][y]=random.nextInt(2);
			}
		}
		
		//印出陣列
		for(int y=0;y<fnum;y++){
			for(int x=0;x<snum;x++){
				System.out.print(arr[x][y]);
			}
			System.out.println();
		}
		
		//數數
		for(int y=0;y<fnum;y++){
			for(int x=0;x<snum;x++){
				if(arr[x][y]==1){
					if(arr[x+1][y]==1){
						count++;
					}
					if(arr[x][y+1]==1){
						count++;
					}
				}
			}
		}
		System.out.print("total: "+count);
	}

}
