package ce1002.e2.s995001561;

import java.util.Random;
import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {

		int l, w, tol=0;
		Random random1 = new Random(3);
		
		// input l until value is included between 3 and 10.
		System.out.print( "Please input first number (3~10): " );
		Scanner input = new Scanner(System.in);	
		l = input.nextInt(); 
		
		while(l<3 || l>10) {
		System.out.print( "Out of range!\nPlease input first number (3~10): " );
		l = input.nextInt();
		}
		
		// input w until value is included between 3 and 10.
		System.out.print( "Please input second number (3~10): " );
		w = input.nextInt(); 
		
		while(w<3 || w>10) {
		System.out.print( "Out of range!\nPlease input second number (3~10): " );
		w = input.nextInt();
		}
		
       input.close();
		
		int[][] matrix = new int[l][w];
		
		// Give the matrix value
		for(int i=0; i<=l-1; i++){
			for(int j=0; j<=w-1; j++){
				matrix[i][j] = random1.nextInt(2);
			}
		}
		
		// Print the matrix
		for(int i=0; i<=l-1; i++){
			for(int j=0; j<=w-1; j++){
				System.out.print(matrix[i][j]);
				System.out.print(" ");
			}
			System.out.print("\n");
			
		}
		
		// calculate row of the same 1
		for(int i=0; i<=l-1; i++){
			for(int j=0; j<=w-2; j++){
				if(matrix[i][j]==1&&matrix[i][j+1]==1) tol = tol +1;
			}
		}
		
		// calculate column of the same 1
		for(int i=0; i<=w-1; i++){
			for(int j=0; j<=l-2; j++){
				if(matrix[j][i]==1&&matrix[j+1][i]==1) tol = tol +1;
			}
		}
		
		// print total
		System.out.print("total: " + tol );
	}

}