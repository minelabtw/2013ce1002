package ce1002.e2.s102502527;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int length,width;
		int sum = 0;
		do{
			System.out.print("Please input first number (3~10): ");//輸出字彙及輸入數值,並判斷是否在範圍內
			length = input.nextInt();
			if ( length < 3 || length > 10 )
				System.out.println("Out of range!");
		}while ( length < 3 || length > 10 );
		
		do{
			System.out.print("Please input second number (3~10): ");
			width = input.nextInt();
			if ( width < 3 || width > 10 )
				System.out.println("Out of range!");
		}while ( width < 3 || width > 10 );
		
		int array[][] = new int [length][width];//將長寬值存入陣列
		Random ran = new Random();//產生亂數
		
		for ( int i = 0 ; i < length ; i++ )
		{
			for ( int j = 0 ; j < width ; j++ )
			{
				array[i][j] = ran.nextInt(2);//將亂數值依序存入陣列
			}
		}
		
		for ( int k = 0 ; k < length ; k++ )//將值依序表示出來
		{
			for ( int l = 0 ; l < width ; l++ )
			{
				System.out.print(array[k][l]);
			}
			System.out.println();
		}
		
		for ( int a = 0 ; a < length ; a++ )//計算出連續的1
		{
			for ( int b = 0 ; b < length - 1 ; b++ )
			{
				if ( array[a][b] == array[a][b+1] && array[a][b] == 1 )
				{
					sum = sum + 1;
				}
					
			}
		}
		
		for ( int c = 0 ; c < length - 1 ; c++ )
		{
			for ( int d = 0 ; d < length ; d++ )
			{
				if ( array[c][d] == array[c+1][d] && array[c][d] == 1 )
				{
					sum = sum + 1;
				}
					
			}
		}
		
		System.out.print("total: "+sum);
	}

}
