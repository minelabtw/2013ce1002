package ce1002.e2.s102502502;
import java.util.Scanner;
public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int l,w;                                                //設定變數
		int c = 0;
		final int f = 10;
		int [] [] squr = new int [f] [f];                       //設定陣列
		System.out.print("Please input first number (3~10): ");     //請求輸入第一數
		l = input.nextInt();                                       
		while (l < 3 || l > 10)                                     //範圍不對則顯示超出範圍，並要求重新輸入
		{
			System.out.println("Out of range! ");
			System.out.print("Please input first number (1~10): ");
			l = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");    //請求輸入第二數
		w = input.nextInt();
		while (w < 3 || w > 10)                                     //範圍不對則顯示超出範圍，並要求重新輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Please input second number (1~10): ");
			w = input.nextInt();
		}	
		for (int i = 0; i < l; i++)                          //檢查直行與橫行中數字1相等情形
		{
			for (int j =0; j < w; j++)
			{
			 squr[i][j] = (int) (Math.random()*2);
			 System.out.print(squr[i][j] + " ");
			}
			System.out.println();
		}
		for (int m = 0; m < l; m++)
		{
			for (int n =0; n < w-1; n++)
			{
				if (squr[m][n]==squr[m][n+1] && squr[m][n]==1)
					c++;
			}
		}
		for (int m = 0; m < w; m++)
		{
			for (int n =0; n < l-1; n++)
			{
				if (squr[n][m]==squr[n+1][m] && squr[n][m]==1)
					c++;
			}
		}
		System.out.print("total : " + c);                          //輸出總相等情況數
	}
	}