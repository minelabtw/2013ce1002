package ce1002.e2.s100502022;
import java.util.Random;
import java.util.Scanner;
public class E2 {
	public static void main(String[] args){
		//object produce
		Random random = new Random();
		Scanner s = new Scanner(System.in);
		int input1,input2;
		boolean opt= false;
		//opt control input
		do{
			System.out.print("Please input first number(3~10):");
			input1 = s.nextInt();
			if(input1<3||input1>10){
				System.out.println("Out of range!");
			}
			else opt=true;
		}while(opt!=true);
		opt=false;
		do{
			System.out.print("Please input second number(3~10):");
			input2 = s.nextInt();
			if(input2<3||input2>10){
				System.out.println("Out of range!");
			}
			else opt=true;
		}while(opt==false);
		//random produce Mat
		int[][] Mat= new int[input1][input2];
		for(int i=0;i<input1;i++){
			for(int j=0;j<input2;j++){
				Mat[i][j]=random.nextInt(2);
				System.out.print(Mat[i][j]);
			}
			System.out.println();
		}
		
		int total=0;
		//water line compute
		for(int i=0;i<input1;i++){
			for(int j=0;j<input2-1;j++){
				
				if(Mat[i][j]==1&&Mat[i][j+1]==1){
					total++;
				}
			}
		}
		//vertical compute
		for(int i=0;i<input2;i++){
			for(int j=0;j<input1-1;j++){
				if(Mat[j][i]==1&&Mat[j+1][i]==1){
					total++;
				}
			}
		}
		System.out.print("total:"+total);
	}
}
