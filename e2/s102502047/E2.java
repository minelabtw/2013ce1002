package ce1002.e2.s102502047;

import java.util.Scanner;

public class E2 {
	public static void main(String[] args){
		int a=0,b=0;
		System.out.print("Please input first number (3~10): ");
		Scanner p = new Scanner(System.in);
		a=p.nextInt();
		for(;a>10||a<3;)
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			a=p.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		b=p.nextInt();
		for(;b>10||b<3;)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			b=p.nextInt();
		}
		int r[][]=new int[a][b];
		for(int i=0;i<a;i++)
			for(int j=0;j<b;j++)
				r[i][j]=(int)(Math.random()*10)%2;
		int s=0;
		for(int i=0;i<a;i++)
			for(int j=0;j<b-1;j++)
			{
				if(r[i][j]==r[i][j+1]&&r[i][j]==1)
					s++;
			}
		for(int j=0;j<b;j++)
			for(int i=0;i<a-1;i++)
			{
				if(r[i][j]==r[i+1][j]&&r[i][j]==1)
					s++;
			}
		for(int i=0;i<a;i++)
		{	
			for(int j=0;j<b;j++)
				System.out.print(r[i][j]+" ");
			System.out.print("\n");
		}
		System.out.print(s);
	}
}