package ce1002.a2.s102502012;
import java.util.Scanner;

public class A2 {
	public static boolean outOfRange(){ /* for return value */
		System.out.println("Out of range!");
		return true;
	}
	public static void main(String[] args) {
		final int MAXSIZE = 15;
		int n, pascal[][] = new int[MAXSIZE][MAXSIZE];
		Scanner input = new Scanner(System.in);
		do{
			System.out.println("Please input a number (1~10): ");
			n = input.nextInt();
		}while((n < 1||n > 10) && outOfRange());
		
		/* generate pascal triangle */
		for(int i = 1; i <= n; i++){
			pascal[i][1] = 1;
			for(int j = 2; j < i; j++)
				pascal[i][j] = pascal[i - 1][j - 1] + pascal[i - 1][j];
			pascal[i][i] = 1;
		}
		/* print result */
		for(int i = 1; i <= n; i++){
			for(int j = 1; j <= i; j++)
				System.out.print(pascal[i][j] + " ");
			System.out.print('\n');
		}
		input.close();
	}
}
