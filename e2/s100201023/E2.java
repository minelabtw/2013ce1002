package ce1002.e2.s100201023;

import java.util.*;

public class E2 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int row , col , total = 0;
		int[][] number;
		Random ran = new Random();
		
		// input row of table
		while(true)
		{
			System.out.print("Please input first number (3~10): ");
			row = input.nextInt();
			
			if(row > 2 && row < 11)
				break;
			
			System.out.println("Out of range!");
		}
		
		//input col of table
		while(true)
		{
			System.out.print("Please input second number (3~10): ");
			col = input.nextInt();
			
			if(col > 2 && col < 11)
				break;
			
			System.out.println("Out of range!");
		}
		
		// set table and random number
		number = new int[row][col];
		for(int i = 0 ; i < row ; ++i)
			for(int j = 0 ; j < col ; ++j)
				number[i][j] = ran.nextInt(2);
		
		
		//output table and count number of continue 1
		for(int i = 0 ; i < row ; ++i)
		{
			for(int j = 0 ; j < col ; ++j)
			{
				System.out.print(number[i][j] + " ");
				
				if(j != col - 1)
					if(number[i][j] == 1 && number[i][j + 1] == 1)
						++total;
				
				if(i != row - 1)
					if(number[i][j] == 1 && number[i + 1][j] == 1)
						++total;
			}
			System.out.println();
		}
		
		//output total number of continue 1
		System.out.println("total: " + total);
		
	}
}
