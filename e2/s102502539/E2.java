package s102502539;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num;
		int length = 0;
		int height = 0;
		int rannum[][] = new int [10][10];	//二微陣列
		int total = 0;
		Scanner scanner = new Scanner(System.in);
		Random random = new Random();	//隨機變數
		
		System.out.print("Please input first number (3~10): ");
		do
		{
			num = scanner.nextInt();
			if ( num < 3 || num > 10 )
				System.out.print("Out of range!\nPlease input first number (3~10): ");
			length = num;
		} while ( num < 3 || num > 10 );
		
		System.out.print("Please input second number (3~10): ");
		do
		{
			num = scanner.nextInt();
			if ( num < 3 || num > 10 )
				System.out.print("Out of range!\nPlease input second number (3~10): ");
			height = num;
		} while ( num < 3 || num > 10 );
		
		for ( int j = 0 ; j < height ; j++ )
		{
			for ( int i = 0 ; i < length ; i++ )
			{
				rannum[i][j] = random.nextInt(2);	//隨機給予 0 或 1
				System.out.print(rannum[i][j] + " ");
				if ( ( i != 0 && rannum[i-1][j] + rannum[i][j] == 2 ) || ( j != 0  &&  rannum[i][j-1] + rannum[i][j] == 2 ) )	//判斷
					total++;
			}
			System.out.println();
		}
		
		System.out.print("total: " + total);
	}

}
