package ce1002.e2.s102502043;
import java.util.Scanner;
import java.util.Random;
public class E2 
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		int Array[][] = new int [10][10];
		Scanner input = new Scanner(System.in);
		int num=0;
		int w; 
		int l; 
		Random ran = new Random();                                   //宣告亂數
		System.out.print("Please input first number (3~10): ");
		w = input.nextInt();
		while(w<3||w>10)                                                   //限制長寬大小
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			w = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		l = input.nextInt();
		while(l<3||l>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			l = input.nextInt();
		}
		for(int i=0;i<w;i++)
		{
			for(int j=0;j<l;j++)
			{
				Array[i][j] = ran.nextInt(2);                                //亂數存入陣列
			}
		}
		for(int i=0;i<w;i++)
		{
			for(int j=0;j<l;j++)
			{
				System.out.print(Array[i][j]+" ");                              //顯現出陣列
			}
			System.out.println("");
		}
		for(int i=0;i<w-1;i++)
		{
			for(int j=0;j<l-1;j++)
			{
				if(Array[i+1][j] == Array[i][j]&&Array[i][j]==1)
				{
					num=num+1;                                            //紀錄重複直
				}
				if(Array[i][j+1] == Array[i][j]&&Array[i][j]==1)
				{
					num=num+1;
				}
			}
		}
		System.out.print("total: "+num);
		input.close();
	}

}
