package ce1002.e2.s102502005;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//第一個數字是列數，直行橫列。
		int w,l,total=0;//陣列的列數，行數，連續值。
		int [][] matrix;//陣列位址。
		matrix = new int [10][10];
		Scanner input = new Scanner(System.in);
		Random ran = new Random();
		
		System.out.print("Please input first number (3~10): ");//要求使用者輸入行數和列數。
		w = input.nextInt();
		
		while(w<3 || w>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			w = input.nextInt();
		}
		
		System.out.print("Please input second number (3~10): ");
		l = input.nextInt();
		
		while(l<3 || l>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			l = input.nextInt();
		}
		
		for(int i=0;i<w;i++)//隨機產生內容。
		{
			for(int j=0;j<l;j++)
			{
				matrix[i][j] = ran.nextInt(2);
			}
		}
		
		for(int i=0;i<w;i++)//印出內容
		{
			for(int j=0;j<l;j++)
			{
				System.out.print(matrix[i][j]);
			}
			System.out.print("\n");
		}
		
		for(int i=0;i<w;i++)//判斷連續
		{
			for(int j=0;j<l;j++)
			{
				if(matrix[i][j] == matrix[i+1][j] && matrix[i][j] == 1 && i+1<w)//上下
				{
					total = total+1;
				}
				
				if(matrix[i][j] == matrix[i][j+1] && matrix[i][j] == 1 && j+1<l)//左右
				{
					total = total+1;
				}
			}
		}
		
		System.out.println("total: "+total);//印出結果
	}

}
