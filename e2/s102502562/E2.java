package ce1002.e2.s102502562;

import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int l,w,total=0;
		int[][] array;
		do//讓使用者輸入第一個數並判斷是否符和範圍
		{
	        System.out.print("Please input first number (3~10): ");
	        l = input.nextInt();
	        if(l<3 || l>10)
	        {
	        	System.out.println("Out of range!");
	        }
		} while (l<3 || l>10);
		do//讓使用者輸入第二個數並判斷是否符和範圍
		{
	        System.out.print("Please input second number (3~10): ");
	        w = input.nextInt();
	        if(w<3 || w>10)
	        {
	        	System.out.println("Out of range!");
	        }
		} while (w<3 || w>10);
		array = new int[l][w];//宣告一個新的陣列其長和寬為l和w
		for(int i=0;i<l;i++)//用亂數0和1填滿陣列
		{
			for(int j=0;j<w;j++)
			{
				array[i][j]=(int)(Math.random()*2);
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		for(int i=0;i<l-1;i++)//判斷上下是否都是1
		{
			for(int j=0;j<w;j++)
			{
				if(array[i][j]==1 && array[i][j]==array[i+1][j])
				{
					total++;
				}
			}
		}
		for(int i=0;i<l;i++)//判斷左右是否都是1
		{
			for(int j=0;j<w-1;j++)
			{
				if(array[i][j]==1 && array[i][j]==array[i][j+1])
				{
					total++;
				}
			}
		}
		System.out.print("total: " + total);

	}

}
