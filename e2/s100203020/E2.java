package ce1002.e2.s100203020;
import java.util.Scanner;
import java.lang.Math;
public class E2 {

	private static Scanner scanner;

	public static void main(String[] args) {
		//declare row  column 
		int row, column;
		int total=0;
		
		scanner = new Scanner(System.in);
		
		System.out.print("Please input first number (3~10): ");
		
		do 
		{
			row = scanner.nextInt();
			if (row < 3 || row > 10)
				System.out.print("Out of range!\nPlease input first number (3~10): ");
		} while (row < 3 || row > 10);
		
		System.out.print("Please input second number (3~10): ");
		do 
		{
			column = scanner.nextInt();
			if (column < 3 || column > 10)
				System.out.print("Out of range!\nPlease second number (3~10): ");
		} while (column < 3 || column > 10);
		
		
		//declare array
		int[][] array;
		array = new int [row][column];
		
		// random put 0 or 1 in array
		for(int i=0;i<row;i++)
			for(int j=0;j<column;j++)
				array[i][j] = (int)(Math.random()<0.5?0:1);
		//determine
		for(int i=0;i<row;i++){
			for(int j=0;j<column;j++)
				System.out.print(array[i][j]+" ");
			System.out.println();
		}
		
		for(int i=0;i<row;i++)
			for(int j=0;j<column;j++){
				if (j<column-1)
					if (array[i][j]+array[i][j+1] == 2) 
						total++;
				if (i<row-1)
					if (array[i][j] + array[i+1][j] == 2)
						total++;
	 	        }

		System.out.print("total: "+total);
				
	



	}

}
