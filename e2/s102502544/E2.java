package ce1002.E2.s102502544;

import java.util.Scanner;
import java.util.Random; 

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Random rand = new Random();
		
		System.out.print("Please input first number (3~10): ");
		int l=input.nextInt();//輸入的數字
		while(l<3 || l>10){//設定範圍
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l=input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		int w=input.nextInt();
		while(w<3 || w>10){
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w=input.nextInt();
		}
		int arr[][];//二維陣列
		arr=new int[l][w];//陣列大小
		for(int i=0 ; i<l ; i++){
			for(int j=0 ; j<w ; j++){
				arr[i][j]=rand.nextInt(2);//設定陣列裡的亂數
			}
		}
		for(int x=0 ; x<l ; x++){//輸出陣列
			for(int y=0 ; y<w ;y++){
				System.out.print(arr[x][y]);
				System.out.print(" ");
			}
			System.out.println();
		}
		
		int total=0;//變數
		for(int a=0 ; a<l ;a++){//設定左右連續為1的情況
			for(int b=0 ; b<w-1 ;b++){
				if(arr[a][b]==1 && arr[a][b+1]==1)
					total++;
			}
		}
		for(int c=0 ; c<l-1 ; c++){//設定上下連續為1的情況
			for(int d=0 ; d<w ; d++){
				if(arr[c][d]==1 && arr[c+1][d]==1)
					total++;
			}
		}
		System.out.print("total: ");
		System.out.print(total);//輸出總和

	}

}
