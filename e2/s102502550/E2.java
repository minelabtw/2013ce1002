package ce1002.e2.s102502550;
import java.util.*;                                                         //匯入程式褲 
public class E2 {

	public static void main(String[] args) {
		Scanner in = new  Scanner(System.in);                               //宣告輸入物件
        Random ran = new Random();                                          //宣告亂數物件
		int L=0,W=0,total=0,p = 0;
		boolean flag = false;
		
		do{                                                                 //輸入提示
			
		   System.out.print("Please input first number (3~10): ");	
           L = in.nextInt();
           
           if(L<3 || L>10){
        	   System.out.println("Out of range!");
           }
		}while(L<3 || L>10);

		do{                                                                 //輸入提示
			
		   System.out.print("Please input second number (3~10): ");	
           W = in.nextInt();
           
           if(W<3 || W>10){
        	   System.out.println("Out of range!");
           }
		}while(W<3 || W>10);
		
		
		
		
		int[][] arr = new int[L][W];                                        //宣告儲存用陣列

		
		for(int i=0;i<L;i++){
			for(int j=0;j<W;j++){
				
				 arr[i][j] = ran.nextInt(2);
				 System.out.print(arr[i][j] + " ");
				 
			}
			System.out.println("");
		}
			
		for(int i=0;i<L;i++){                                               //橫向加法
			flag = false;
			p=0;
			for(int j=0;j<W;j++){
		         
				
		        	 if(!flag && arr[i][j] == 1){
		        		 p = j;
		        		 flag = true;
		        	 }
		        	 if( flag && arr[i][j] == 0 ){
		        		 total = total + (j - p - 1);
		        		 flag = false;
		        	 }		        	 
				     if( flag && j==W-1 && arr[i][j] == 1){
				    	 total = total + (j - p);
		        		 flag = false;
				     }
				
			}   
			
		}
		
		for(int i=0;i<W;i++){                                             //直向加法
			flag = false;
			p=0;
			for(int j=0;j<L;j++){
		         
		        	 if(!flag && arr[j][i] == 1){
		        		 p = j;
		        		 flag = true;
		        	 }
		        	 if( flag && arr[j][i] == 0 ){
		        		 total = total + (j - p - 1);
		        		 flag = false;
		        	 }
		        	 if(flag && j==L-1 && arr[j][i] == 1){
		        		 total = total + (j - p);
		        		 flag = false;
		        	 }
		        	 
		        	 		     			
			}   
			
		}
		System.out.print("total: "+total);                               //輸出最後結果
	}

}
