package ce1002.e2.s102502018;
import java.util.Scanner;
import java.util.Random;
public class E2 {
	public static void main(String[] args) {
		
		int l=0;
		int w=0;
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");
		do
		{
			l = scanner.nextInt();
			if(l<3||l>10)
			{
				System.out.println("Out of range!");
				System.out.print("Please input first number (3~10): ");				
			}
		}
		while(l<3||l>10);                //判斷範圍
		System.out.print("Please input second number (3~10): ");
		do
		{
			w = scanner.nextInt();
			if(w<3||w>10)
			{
				System.out.println("Out of range!");
				System.out.print("Please input second number (3~10): ");
			}
		}
		while(w<3||w>10);                //判斷範圍
		int arr[][];
		arr = new int[l][w];
		int num;		
		int a=0;
		int b=0;		
		for(a=0;a<l;a++)
		{
			for(b=0;b<w;b++)
			{
				num = (int)(Math.random() * 2);       //將亂數代入num
				arr[a][b]=num;						  //將num代入陣列
			}
		}
		for(a=0;a<l;a++)
		{
			for(b=0;b<w;b++)
			{
				System.out.print(arr[a][b]);          //輸出陣列
			}
			System.out.println("");                   //換行
		}
		int total=0;
		for(a=0;a<l;a++)                              //判斷橫向是否相連
		{
			for(b=0;b<w;b++)
			{
				if(b==w-1)
				{
					break;
				}
				else if(arr[a][b]==1 && arr[a][b+1]==arr[a][b])
				{
					total++;
				}
			}			
		}
		for(b=0;b<w;b++)							   //判斷縱向是否相連
		{
			for(a=0;a<l;a++)
			{
				if(a==l-1)
				{
					break;
				}
				else if(arr[a][b]==1 && arr[a+1][b]==arr[a][b])
				{
					total++;
				}
			}			
		}
		
		System.out.print("total= "+total);

	}

}
