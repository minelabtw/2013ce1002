package ce1002.e2.s102502561;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		int total = 0, w = 0, l = 0; // 宣告變數 w高 l寬
		Scanner input = new Scanner(System.in); // 宣告scanner
		System.out.println("Please input first number (3~10): ");
		w = input.nextInt(); // 讀入一整數
		while (w > 10 || w < 3) {
			System.out.println("Out of range!");
			System.out.println("Please input first number (3~10): ");
			w = input.nextInt();
		}

		System.out.println("Please input second number (3~10): ");
		l = input.nextInt();
		while (l > 10 || l < 3) {
			System.out.println("Out of range!");
			System.out.println("Please input first number (3~10): ");
			l = input.nextInt();
		}
		input.close();	//關閉scanner
		int[][] arr = new int[w][l]; //宣告二維陣列
		Random r = new Random();	//宣告亂數
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < l; j++) {
				arr[i][j] = r.nextInt(2);
				System.out.print(arr[i][j] + " ");
			}
			System.out.println("");
		}
		for (int i = 0; i < w - 1; i++) {			//檢查是否連續
			for (int j = 0; j < l - 1; j++) {
				if (arr[i][j] == arr[i + 1][j] && arr[i][j] == 1) {
					total = total + 1;
				}

				if (arr[i][j] == arr[i][j + 1] && arr[i][j] == 1) {
					total = total + 1;
				}

			}
		}
		for (int i = 0; i < w-1; i++) {		//檢查最後行或列 是否連續
			if (arr[i][l-1] == arr[i+1][l-1] && arr[i][l-1] == 1) {
				total = total + 1;
			}
		}
		for (int j = 0; j < l-1; j++) {
			if (arr[w-1][j] == arr[w-1][j+1] && arr[w-1][j] == 1) {
				total = total + 1;
			}
		}

		System.out.print("total: " + total);	//輸出總額
	}
}
