package ce1002.e2.s102502558;

// 引入需要的libaray
import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int l = 0, w = 0, total = 0;
		Scanner scanner = new Scanner(System.in);
		Random rnd = new Random(); //亂數產生器
		
		// 重複輸入寬
		while(true)
		{
			System.out.print("Please input first number (3~10): ");
			w = scanner.nextInt();
			if (w >= 3 && w <= 10)
				break;
			System.out.println("Out of range!");
		}
		// 重複輸入長
		while(true)
		{
			System.out.print("Please input second number (3~10): ");
			l = scanner.nextInt();
			if (l >= 3 && l <= 10)
				break;
			System.out.println("Out of range!");
		}
		
		int[][] arr = new int[l][w]; // 產生指定大小的陣列
		// 填上 0 1
		for (int i=0;i<l;i++)
		{
			for (int j=0;j<w;j++)
			{
				arr[i][j] = rnd.nextInt(2);
				System.out.print(arr[i][j] + " ");
			}
			System.out.print("\n");
		}
		
		// 數橫的
		for (int i=0;i<l;i++)
		{
			for (int j=1;j<w;j++)
			{
				if (arr[i][j] == 1 && arr[i][j-1] == 1)
					total++;
			}
		}
		
		// 數直的
		for (int i=0;i<w;i++)
		{
			for (int j=1;j<l;j++)
			{
				if (arr[j][i] == 1 && arr[j-1][i] == 1)
					total++;
			}
		}
		// 印出結果
		System.out.print("total: " + total);
		scanner.close();
	}

}
