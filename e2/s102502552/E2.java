package ce1002.e2.s102502552;

import java.util.Scanner;

public class E2 {
public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	
	int l,w,total = 0;//宣告橫豎與總量的三個變數
	
	do{
		System.out.print("Please input first number (3~10):");
		l = sc.nextInt();
		if(l < 3 || l > 10)
			System.out.println("Out of range!");
	}while(l < 3 || l > 10);//輸入,判斷長是否合理
	
	do{
		System.out.print("Please input second number (3~10):");
		w = sc.nextInt();
		if(w < 3 || w > 10)
			System.out.println("Out of range!");
	}while(w < 3 || w > 10);//輸入,判斷寬是否合理
	
	int[][] plate = new int[l][w];
	
	for(int i = 0;i < l;i++)
	{
		for(int j = 0;j < w;j++)
		{
			plate[i][j] = (int)(Math.random() * 2);
			System.out.print(plate[i][j] + " ");
		}System.out.println();
	}//將長寬導入二維陣列,亂數計算後並顯示
	
	for(int i = 0;i < l;i++)//豎向座標迴圈
	{
		for(int j = 0;j < w;j++)//橫向座標迴圈
		{
			if(j == w - 1)
			{
				continue;
			}//最後一個省略
			else if(plate[i][j] == 1 && plate[i][j + 1] == 1)
			{
				total += 1;
			}//其餘前後都是一的總數加一
		}
	}//橫向尋找連續
	
	for(int i = 0;i < l;i++)//豎向座標迴圈
	{
		for(int j = 0;j < w;j++)//橫向座標迴圈
		{
			if(i == l - 1)
			{
				continue;
			}//最後一個省略
			else if(plate[i][j] == 1 && plate[i + 1][j] == 1)
			{
				total += 1;
			}//其餘上下都是一的總數加一
		}
	}//豎向尋找連續
	
	System.out.print("Total: " + total);//輸出連續的結果
	
	}	

}




