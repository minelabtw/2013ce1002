package ce1002.e2.s102502027;

import java.util.Scanner; //宣告類別
import java.util.Random;

public class E2 {

	public static void main(String[] args) {

		Scanner input1 = new Scanner(System.in); // 宣告輸入變數
		Scanner input2 = new Scanner(System.in);
		Scanner Array = new Scanner(System.in);
		int number1, number2;
		int total = 0;
		Random ran = new Random(); // 宣告隨機變數
		// 執行
		do {

			System.out.print("Please input first number (3~10): ");
			number1 = input1.nextInt();

			if (number1 > 10 || number1 < 3) {
				System.out.println("Out of range!");
			}

		} while (number1 > 10 || number1 < 3);

		do {
			System.out.print("Please input second number (3~10): ");
			number2 = input2.nextInt();
			if (number2 > 10 || number2 < 3) {
				System.out.println("Out of range!");
			}
		} while (number2 > 10 || number2 < 3);

		int array[][] = new int[number1][number2];  //宣告陣列
		for (int k = 0; k < number1; k++) {
			for (int j = 0; j < number2; j++) {
				array[k][j] = ran.nextInt(2); //把隨機變數塞入
			}
		}
		// 輸出隨機變數
		for (int a = 0; a <= number1 - 1; a++) {
			for (int b = 0; b <= number2 - 1; b++) {
				System.out.print(array[a][b]);
				System.out.print(" ");
			}
			System.out.println();
		}
		// 算總數
		for (int i = 0; i < number1; i++) {
			for (int k = 0; k < number2 - 1; k++) {
				if (array[i][k] == array[i][k + 1] && array[i][k + 1] == 1) {
					total++;
				}
			}
		}
		for (int i = 0; i < number1 - 1; i++) {
			for (int k = 0; k < number2; k++) {
				if (array[i + 1][k] == array[i][k] && array[i][k] == 1) {
					total++;
				}
			}
		}
		System.out.print("total: ");
		System.out.print(total);
	}
}
