package ce1002.e2.s102502021;
import java.util.Scanner;
public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	Scanner input = new Scanner(System.in);
		
		int k=0;
		int l = 0;
		int count=0 ;
		System.out.println("Please input first number (3~10): ");
		k = input.nextInt();
		while(k < 3 || k > 10)         //迴圈
		{
			System.out.println("Out of range!");
			System.out.println("Please input first number (3~10): ");
			k = input.nextInt();
		}
		System.out.println("Please input second number (3~10): ");
		l = input.nextInt();
		while(l < 3 || l > 10)     //迴圈
		{
			System.out.println("Out of range!");
			System.out.println("Please input second number (3~10): ");
			l = input.nextInt();
		}
		int arr[][] = new int [10][10];    //宣告二微陣列
		for (int t=0 ; t < k ; t++)
		{
			for (int a=0 ; a < l ; a++)
			{
				System.out.print( arr[a][t] = (int)(Math.random()*2));    //亂數存入陣列
				System.out.print(" ");
			}
			System.out.println("");
		}
		for (int b=0 ; b < l ; b++)      //尋找連續1的次數
		{
			for(int c=0 ; c < k; c++ )
			{
				if (arr[b][c]==1)
				{
					if ( arr[b][c] == arr[b][c+1])  //與右一格比較
					{
						count++;
					}
					if ( arr[b][c] == arr[b+1][c]) //與下一格比較
					{
						count++;
					}
				}
			}
		}
		System.out.println("total: " + count);
		input.close();
		

	}

}
