package ce1002.e2.s102502560;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
	    
		Scanner STDIN=new Scanner(System.in);
	    Random rand=new Random();
	    
		int height,width;
		int[][] arr;

		while(true){
			System.out.print("Please input first number (3~10): ");
			height=STDIN.nextInt();
			if(height>=3&&height<=10)break;
			System.out.println("Out of range!");
		}
		
		while(true){
			System.out.print("Please input second number (3~10): ");
			width=STDIN.nextInt();
			if(width>=3&&width<=10)break;
			System.out.println("Out of range!");
		}
		
		STDIN.close();
	    
		arr=new int[height][width];
		
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				arr[i][j]=rand.nextInt(2);				//generate random number 1 or 0
				System.out.print(arr[i][j]+" ");		//print out
			}
			System.out.println();
		}
		
		int count=0;
		
		for(int i=0;i<height;i++){						//check each line
			for(int j=0;j<width-1;j++){
				if(arr[i][j]==1&&arr[i][j+1]==1)count++;
			}
		}
		for(int j=0;j<width;j++){						//check each column
			for(int i=0;i<height-1;i++){
				if(arr[i][j]==1&&arr[i+1][j]==1)count++;
			}
		}
		
		System.out.println("total: "+count);			//print result
	}

}
