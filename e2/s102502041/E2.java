package ce1002.e2.s102502041;
import java.util.Scanner;
import java.lang.Math;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		int length,width;
		Scanner cin = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");
		do 
		{
			length = cin.nextInt();
			if (length < 3 || length > 10)
				System.out.print("Out of range!\nPlease input first number (3~10): ");
		} while (length < 3  || length > 10);
		System.out.print("Please input second number (3~10): ");
		do 
		{
			width = cin.nextInt();
			if (width < 3 || width > 10)
				System.out.print("Out of range!\nPlease input second number (3~10): ");
		} while (width < 3  || width > 10);
		int[][] arr = new int[length][width];
		int l,w,total=0;
		for(l=0;l<length;l++)
		{
			for(w=0;w<width;w++)
			{
				arr[l][w]=(int)(Math.random()*10%2);
			}
		}
		for(l=0;l<length;l++)
		{
			for(w=0;w<width;w++)
			{
				System.out.print(arr[l][w]);
				System.out.print(" ");
			}
			System.out.print("\n");
		}
		for(l=0;l<length;l++)
		{
			for(w=0;w<width-1;w++)
			{
				if(arr[l][w]==arr[l][w+1]&&arr[l][w]==1) total+=1;
			}
		}
		for(w=0;w<width;w++)
		{
			for(l=0;l<length-1;l++)
			{
				if(arr[l][w]==arr[l+1][w]&&arr[l][w]==1) total+=1;
			}
		}
		System.out.print("total: ");
		System.out.print(total);
	}

}
