package ce1002.e2.s102502031;

import java.util.Scanner;

public class E2 {
	public static void main(String[] args) {
		Scanner getData = new Scanner(System.in);
		int[][] array = new int[10][10];
		int length;
		int width;
		int total = 0;

		length = getDataValue("first", getData);
		width = getDataValue("second", getData);
		getData.close();
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < width; j++) {
				array[i][j] = (int) (Math.random() * 2); // get value randomly, it may be 0 or 1
				System.out.print(array[i][j] + " "); // print the result of target array
				// calculate the total
				if (i != 0 && j != 0) {
					if (array[i][j] == 1) {
						if (array[i - 1][j] == 1)
							total++;
						if (array[i][j - 1] == 1)
							total++;
					}
				} else if (i == 0 && j != 0) {
					if (array[i][j] == 1) {
						if (array[i][j - 1] == 1)
							total++;
					}
				} else if (i != 0 && j == 0) {
					if (array[i][j] == 1) {
						if (array[i - 1][j] == 1)
							total++;
					}
				}
			}
			System.out.println();
		}
		System.out.println("total: " + total);
	}

	public static int getDataValue(String a, Scanner jin) {
		// get the value of data until it's legal
		int data;
		do {
			System.out.print("Please input " + a + " number (3~10): ");
			data = jin.nextInt();
			if (data < 3 || data > 10)
				System.out.println("Out of Range!");
		} while (data < 3 || data > 10);
		return data;
	}
}
