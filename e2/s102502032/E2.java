package ce1002.e2.s102502032;

import java.util.Random;
import java.util.Scanner;

public class E2
{
	public static void main(String[] args)
	{
		Scanner jin = new Scanner(System.in);
		int i = getSize(3, 10, "first", jin);
		int w = getSize(3, 10, "second", jin);
		int arr[][] = rndArrFilling(i, w);
		PrintArray(arr, i, w);
		System.out.print("total: " + countPairs(arr));
		jin.close();
	}

	// get size
	public static int getSize(int min, int Max, String str, Scanner jin)
	{
		// declaration
		int size = 0;
		do
		{
			System.out.print("Please input " + str + " number (" + min + "~"
					+ Max + "): ");
			size = jin.nextInt();
			if (size < min || size > Max)
				System.out.println("Out of range!");
		}
		while (size < min || size > Max);
		return size;
	}

	// set random values to array's elements
	// d1: dimension 1; d2: dimension 2
	public static int[][] rndArrFilling(int d1, int d2)
	{
		int arr[][] = new int[d1][d2];
		Random rnd = new Random(System.currentTimeMillis());
		for (int i = 0; i < d1; i ++)
		{
			for (int j = 0; j < d2; j ++)
			{
				arr[i][j] = rnd.nextInt(2);
			}
		}
		return arr;
	}

	// print 2-dimension array
	// d1: dimension 1; d2: dimension 2
	public static void PrintArray(int arr[][], int d1, int d2)
	{
		for (int i = 0; i < d1; i ++)
		{
			for (int j = 0; j < d2; j ++)
			{
				System.out.print(arr[i][j] + " ");
			}
			// change line
			System.out.println();
		}
	}

	// count how many pairs of 1 there are
	public static int countPairs(int arr[][])
	{
		int count = 0;
		for (int i = 0; i < arr.length; i ++)
		{
			for (int j = 0; j < arr[0].length; j ++)
			{
				switch (arr[i][j])
				{
					case 1:
						if (j < arr[0].length - 1)
						{
							if (arr[i][j + 1] == 1)
							{
								count ++;
							}
						}
						if (i < arr.length - 1)
						{
							if (arr[i + 1][j] == 1)
							{
								count ++;
							}
						}
						break;
					default:
						break;
				}
			}
		}
		return count;
	}
}
