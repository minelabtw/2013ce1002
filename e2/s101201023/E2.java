package ce1002.e2.s101201023;

import java.util.Scanner;
import java.util.Random;

public class E2 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		int l=0 , w=0;
		int M[][] = new int [10][10];
		int total =0;
		Scanner input = new Scanner(System.in);
		Random ran = new Random();
		
		//輸入整數，判斷是否符合範圍
		System.out.print("Please input first number (3~10): ");
		l = input.nextInt();
		while(l<3 || l>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
		}
		
		System.out.print("Please input second number (3~10): ");
		w = input.nextInt();
		while(w<3 || w>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
		}
		
		//隨機輸入二微陣列的變數
		for(int i=0 ; i < l ; i++)
		{
			for(int j=0 ; j < w ; j++)
			{
				M[i][j] = ran.nextInt(2);
				System.out.print(M[i][j]);
			}
			System.out.println();
		}
		
		//計算相連1的數量
		for(int i=0 ; i < l ; i++)
		{
			for(int j=0 ; j < w-1 ; j++)
			{
				if(M[i][j]==M[i][j+1] && M[i][j]==1)
					total++;
			}
		}
		for(int j=0 ; j < w ; j++)
		{
			for(int i=0 ; i < l-1; i++)
			{
				if(M[i][j]==M[i+1][j] && M[i][j]==1)
					total++;
			}
		}
		System.out.print("total: "+ total);
	}
	
}
