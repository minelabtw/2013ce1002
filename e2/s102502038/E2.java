package ce1002.e2.s102502038;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public final class E2 {
	public static void main(String[] args){
		String inputln = null;
		int numA = 0,numB = 0;
		// input section
		while(true){
			System.out.print("Please input first number (3~10): ");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				numA = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(numA > 10 || numA < 3){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		while(true){
			inputln = null;
			System.out.print("Please input second number (3~10): ");
			try{
				BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
				inputln = buff.readLine();
				numB = Integer.parseInt(inputln);
			}catch(IOException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}catch(java.lang.NumberFormatException error){
				System.out.println("Input Section Error: " + error);
				continue;
			}
			if(numB > 10 || numB < 3){
				System.out.println("Out of range!");
				continue;
			}
			break;
		}
		// end of input section
		long seed = System.currentTimeMillis();
		Random rnd = new Random(seed);
		int[][] arr = new int[numA][numB];
		for(int i = 0;i<numA;i++){
			for(int j = 0;j<numB;j++){
				arr[i][j] = (rnd.nextBoolean()?1:0);
			}
		}
		int sum = 0;
		//start output
		for(int i = 0;i<numA;i++){
			for(int j = 0;j<numB;j++){
				System.out.print(arr[i][j]);
				if(arr[i][j] == 1 && i<numA-1){
					sum += (arr[i+1][j] == 1?1:0);
				}
				if(arr[i][j] == 1 && j<numB-1){
					sum += (arr[i][j+1] == 1?1:0);
				}
			}
			System.out.println("");
		}
		System.out.println("total: " + String.valueOf(sum));
	}
}
