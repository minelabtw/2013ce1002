package ce1002.e2.s995002014;

import java.util.Scanner;

public class E2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		
		//User input
		int l,w,count=0;
		scanner = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");
		//Set range between 5 to 30
		do 
		{
			l = scanner.nextInt();
			if (l < 3 || l > 10)
				System.out.print("Out of range!\nPlease input first number (3~10): ");
		} while (l < 3 || l > 10);
		
		System.out.print("Please input second number (3~10): ");
		
		do 
		{
			w = scanner.nextInt();
			if (w < 3 || w > 10)
				System.out.print("Out of range!\nPlease input second number (3~10): ");
		} while (w < 3 || w > 10);
		
		Integer[][] a = new Integer[l][w];

		for (int i = 0; i < l; i++) 
		{
			for (int j = 0; j < w; j++) 
			{
				a[i][j]=(int)((Math.random()<0.5)?0:1);
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
		for (int i = 0; i < l; i++) 
		{
			for (int j = 0; j < w; j++) 
			{
				if(a[i][j]==1&&j!=w-1){
					if(a[i][j]==a[i][j+1])count++;
				}
				if(a[i][j]==1&&i!=l-1){
					if(a[i][j]==a[i+1][j])count++;
				}
			}
		}
		System.out.println("Totel: "+count);
	}
}


