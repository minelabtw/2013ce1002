package ce1002.e2.s102502514;
import java.util.Scanner;
public class E2 {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);  
		
		int l,w,count =0;  //l為列數、w為行數、count為相黏個數
		do {
			System.out.print("Please input first number (3~10): ");
			l = scanner.nextInt();
			if( l < 3 || l > 10 )
				System.out.println("Out of range!");	//不符合規定的輸入
		} while ( l < 3 || l > 10 ); //問l
		do {
			System.out.print("Please input second number (3~10): ");
			w = scanner.nextInt();
			if( w < 3 || w > 10 )
				System.out.println("Out of range!");	//不符合規定的輸入
		} while ( w < 3 || w > 10 ); //問w

		int arr[][] = new int [l+1][w+1];  //把陣列右邊多一行，下面多一行
		
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < w; j++) {
				arr[i][j] = (int)(Math.random()*2);
			}
		} //把使用者要的區域填上亂數
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < w; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}  //輸出使用者要求大小的數列
		
		for (int i = 0; i < l+1; i++) {
			for (int j = 0; j < w+1; j++) {
				
				if ( arr[i][j] == 1 && arr[i][j+1] == 1) //跟右邊的比
					count ++;
				if ( arr[i][j] == 1 && arr[i+1][j] == 1) //跟下面的比
					count ++;
			}
		}
		System.out.println("total: " + count);  //輸出個數
		}
}
