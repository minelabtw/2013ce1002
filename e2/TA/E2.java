package ce1002.e2.s102522042;

import java.util.Random;
import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner( System.in );
		int [][] arr;
		int firstnum , secondnum , total = 0;//供使用者輸入陣列大小的變數
		
		System.out.print( "Please input first number (3~10): " );//確認使用者輸入在範圍內
		
		firstnum = input.nextInt();
		while( firstnum < 3 || firstnum > 10)
		{
			System.out.println( "Out of range!" );
			System.out.print( "Please input first number (3~10): " );
			firstnum = input.nextInt();
		}
		
		System.out.print( "Please input second number (3~10): " );
		secondnum = input.nextInt();
		while( secondnum < 3 || secondnum > 10)
		{
			System.out.println( "Out of range!" );
			System.out.print( "Please input second number (3~10): " );
			secondnum = input.nextInt();
		}
		
		arr = new int [firstnum][secondnum];//宣告一個使用者輸入大小的二維陣列
		
		Random ran = new Random();
		
		for( int i = 0 ; i < firstnum ; i++ )//隨機給予一個1或0的值
		{
			for( int j = 0 ; j < secondnum ; j++ )
			{
				arr[i][j] = ran.nextInt( 2 );
			}
		}
		
		for( int i = 0 ; i < firstnum ; i++ )//計算列有多少連續兩個1的元素
		{
			for( int j = 0 ; j < secondnum - 1; j++ )
			{
				if( arr[i][j] == 1 && arr[i][j+1] == 1 )
					total++;
			}
		}
		
		for( int i = 0 ; i < firstnum - 1 ; i++ )//計算行有多少連續兩個1的元素
		{
			for( int j = 0 ; j < secondnum ; j++ )
			{
				if( arr[i][j] == 1 && arr[i+1][j] == 1 )
					total++;
			}
		}
		
		for( int i = 0 ; i < firstnum ; i++ )//最後印出陣列中的數字
		{
			for( int j = 0 ; j < secondnum ; j++ )
			{
				System.out.print( arr[i][j] + " " );
			}
			System.out.println( "" );
		}
		System.out.println( "total: " + total );
		
	}
}