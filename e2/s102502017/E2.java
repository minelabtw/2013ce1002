package ce1002.e2.s102502017;
import java.util.Scanner;
public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int height=0;
		int width=0;
		
		while(true){
			System.out.print("Please input first number (3~10): ");
			height = input.nextInt();
			if(height>=3 && height<=10)break;
			else System.out.println("Out of range!");
		}
		while(true){
			System.out.print("Please input second number (3~10): ");
			width = input.nextInt();
			if(width>=3 && width<=10)break;
			else System.out.println("Out of range!");
		}
		
		int[][] x = new int[height][width];
		//宣告一個大小符合輸入的陣列，type為int
		
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				x[i][j] = (int)(Math.random()*2);
			}
		}
		
		int sum=0;
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				if(x[i][j]==1){
					if(i!=height-1 && j!=width-1){
						//not at the bottom margin and either at the right margin
						if(x[i+1][j]==1)sum++;
						if(x[i][j+1]==1)sum++;
					}
					else if(i==height-1 && j!=width-1){
						//at the bot margin but not at the bot and right point
						if(x[i][j+1]==1)sum++;
					}
					else if(j==width-1 && i!=height-1){
						//at the right margin but not at the bot and right point 
						if(x[i+1][j]==1)sum++;
					}
					
				}
			}
		}
		//print the array
		for(int i=0;i<height;i++){
			for(int j=0;j<width;j++){
				if(j==width-1)System.out.println(x[i][j]);
				else System.out.print(x[i][j]+" ");
			}
		}
		// print the total
		System.out.println("total: "+sum);
		//close the object"input"
		input.close();
	}
}
