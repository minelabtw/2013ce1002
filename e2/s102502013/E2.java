package ce1002.e2.s102502013;

import java.util.Scanner;

public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int w=0, l=0;//declare two variable about wide and length
		System.out.print("Please input first number (3~10): ");
		l = input.nextInt();//input length
		while (l<3||l>10)//when length out of range, output "Out of range!" and input length
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		w = input.nextInt();
		while (w<3||w>10)//when wide out of range, output "Out of range!" and input wide
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
		}
		int p[][] = new int[w][l];//declare an array
		for(int j=0;j<l;j++)
		{
			for(int i=0;i<w;i++)
			{
				p[j][i] = (int)(Math.random()*2);//create random value and store in the array
			}
		}
		int count = 0;
		for(int j=0;j<l;j++)
		{
			for(int i=0;i<w;i++)
			{
				if(i+1<w)
				{
					if(p[j][i]==p[j][i+1]&&p[j][i]==1)//if right margin = left margin count +1
					{
						count++;
					}
				}
				if(j+1<l)
				{
					if(p[j][i]==p[j+1][i]&&p[j][i]==1)//if top = bottom count +1
					{
						count++;
					}
				}
			}
		}
		for(int j=0;j<l;j++)//output array
		{
			for(int i=0;i<w;i++)
			{
				System.out.print(p[j][i] + " ");
			}
			System.out.println("");
		}
		System.out.println("total: " + count);//output count's value
	}
}
