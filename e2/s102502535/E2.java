package ce1002.e2.s102502535;

import java.util.Scanner ; 
import java.lang.Math ;

public class E2 {

	public static void main(String[] args) {

		int length = 0 ;  
		int width = 0 ;   
	    int counter = 0 ;  //宣告三變數，分別為長、寬以及最後的計算數。
		
		Scanner input = new Scanner (System.in) ; 
		
		System.out.print("Please input first number (3~10): ") ; 
		length = input.nextInt() ; 
		while ( length < 3 || length > 10 ) 
		{ 
		     System.out.println("Out of range!\nPlease input first number (3~10): ") ; 
		     length = input.nextInt() ; 
		}   //使輸入長，並判斷是否在範圍內，否則重新輸入。
		
		System.out.print("Please input second number (3~10): ") ;
		width = input.nextInt() ;
		while ( width < 3 || width > 10 )
	    { 
		     System.out.println("Out of range!\nPlease input second number (3~10): ") ; 
		     width = input.nextInt() ; 
		}   //使輸入寬，並判斷是否在範圍內，否則重新輸入。
		
		int arr[][] = new int [length][width] ;  //宣告一個二維陣列，長寬分別為剛剛輸入之數字。
		
		for ( int i = 0 ; i < length ; i++ )
		 { 
		    for ( int j = 0 ; j < width ; j ++ )
		    { 
		        arr [i][j] = (int)(Math.random()*2) ;
                System.out.print(arr[i][j]) ;
		      } 
	        System.out.println() ;
		}  //在此二維陣列中隨機放入0或1。 
		
		for ( int l = 0 ; l < length - 1 ; l ++ )
		{
			for ( int w = 0 ; w < width ; w ++ )
			{
			    if ( arr[l][w] == 1 && arr[l+1][w] == 1 )
					counter = counter + 1 ;
			}
		}  //計算1和1相連之次數。
		
		for ( int l = 0 ; l < length ; l ++ )
		{
			for ( int w = 0 ; w < width - 1 ; w ++ )
			{
			    if ( arr[l][w] == 1 && arr[l][w+1] == 1 )
					counter = counter + 1 ;
			}
		}  //計算1和1相連之次數。

		System.out.print("total: " + counter ) ;  //使輸出總次數。
		
		input.close() ; 
	}
}
