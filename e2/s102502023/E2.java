package ce1002.e2.s102502023;

import java.util.Scanner; // import java.util.Scanner to receive the values

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method 
		int number = 0,number1 = 0,sum = 0; // initialize number, number1, and sum
		Scanner input = new Scanner(System.in); 
		int [][] array = new int[10][10]; // initialize array 
		do {
			System.out.print("Please input first number (3~10): ");
			number = input.nextInt();
			if(number < 3 || number > 10)
				System.out.print("Out of range!" + "\n");
			
		} while (number < 3 || number > 10); // do... while loop to check whether number is on demand
		
		do
		{
			System.out.print("Please input second number (3~10): ");
			number1 = input.nextInt();
			if(number1 < 3 || number1 > 10)
				System.out.print("Out of range!" + "\n");
		}while(number1 < 3 || number1 > 10); // do... while loop to check whether number1 is on demand
		
		// for loop to generate random numbers and send to array
		for ( int i = 0; i < number; i++)
		{
			for( int j = 0; j < number1; j++)
			{
				array[i][j] = (int)(Math.random()*2);
			}
		}
        
		// for loop to output array
		for( int i = 0; i < number; i++)
		{
			for( int j = 0; j < number1; j++)
			{
				System.out.print(array[i][j] + " ");
			}
			System.out.print("\n");
		}
		
		// for loop to check 1's
		for( int i = 0; i < number; i++)
		{
			for( int j = 0; j < number1; j++)
			{
			    if(array[i][j] == 1)
			    {
			    	if(array[i][j] == array[i][j+1])
			    		sum++;
			    }
			}
		}
		
		// for loop to check 1's
		for( int j = 0; j < number1; j++)
		{
			for( int i = 0; i < number; i++)
			{
				if(array[i][j] == 1)
				{
					if(array[i][j] == array[i+1][j])
						sum++;
				}
			}
		}
		
		System.out.print("total: " + sum); // to output sum
	}

}
