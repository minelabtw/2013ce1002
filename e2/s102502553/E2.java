package ce1002.e2.s102502553;

import java.util.Random;//建立隨機選取系統
import java.util.Scanner;

public class E2 {



	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int l;
		int w;
		int counter = 0;
		do//判斷範圍
		{
			System.out.println("Please input first number (3~10): "); 
			l = input.nextInt();
	     if(l > 10 || l < 3)
	    	 System.out.println("Out of range!");
		}
		while(l > 10 || l < 3);
		do
		{	
			System.out.println("Please input second number (3~10): ");
		 w = input.nextInt();
		if(w > 10 || w < 3)
		System.out.println("Out of range!");
		}
		while(w > 10 || w < 3);
		
		Random ran = new Random();//建立變數選取
		int [][] arr = new int [l][w];
		for(int i = 0;i < l;i++)//做0 1填入陣列
		{
			for(int j = 0;j < w;j++)
			{
				arr[i][j] =  ran.nextInt(2);
		    }
		}
		
		for(int i = 0;i < l;i++)//印出陣列
		{
			for(int j = 0;j < w;j++)
				{
				System.out.print(arr[i][j] + " ");
		}
		System.out.println();
		}
		
		for(int i = 0;i < l;i++)//數痕的重複次數
		{
			for(int j = 1;j < w;j++)
			{
				if((arr[i][j] == arr[i][j-1]))
				{
					if(arr[i][j] == 1)
					{
						counter++;
					}
					}
			}
		}
		
		for(int i = 1;i < l;i++)//數直的重複次數
		{
			for(int j = 0;j < w;j++)
			{
				if(arr[i][j] == arr[i-1][j])
				{
					if(arr[i][j] == 1)
					counter++;	
				}
			}
		}
		System.out.println(counter);
	}
}
