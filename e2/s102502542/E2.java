package ce1002.e2.s102502542;
import java.util.Scanner;
import java.util.Random;
public class E2 {
	private static Scanner scanner;
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");//輸出
		int l = scanner.nextInt();//輸入
		while(l<3||l>10)//當長度小於3或是大於10進入迴圈
		{System.out.print("Out of range!");
		System.out.print("\n");
		System.out.print("Please input first number (3~10): ");
		l = scanner.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		int w = scanner.nextInt();
		while(w<3||w>10)
		{System.out.print("Out of range!");
		System.out.print("\n");
		System.out.print("Please input second number (3~10): ");
		w = scanner.nextInt();
		}
		int arr[][];//宣告陣列
		arr = new int[l][w];//將陣列大小設定為 l x w
		Random ran = new Random();//作亂數表
		for(int a=0;a<l;a++)
		{for(int b=0;b<w;b++)
		{
			arr[a][b]=ran.nextInt(2);//將亂數表的值固定在0~1之間
			System.out.print(arr[a][b]+" ");
		}
		System.out.print("\n");
		}
		int total1=0;
		int total2=0;
		int total=0;
		for(int a=0;a<l;a++)//判斷式 是否有連續的1
		{for(int b=0;b<w-1;b++)
		{
			if(arr[a][b]==1 && arr[a][b+1]==1)
				total1=total1+1;
		}
		}
		for(int a=0;a<l-1;a++)//判斷式 是否有連續的1
		{for(int b=0;b<w;b++)
		{
			if(arr[a][b]==1 && arr[a+1][b]==1)
				total2=total2+1;
		}
		}
		total=total1+total2;
		System.out.print("total: " + total);//將全部出現連續1的次數輸出
		//System.out.print("total: " + total);
	} 
		// TODO Auto-generated method stub

	}


