package ce1002.e2.s102502050;
import java.util.Scanner;
import java.util.Random;
public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		Scanner scanner = new Scanner(System.in);
		int width,length;
		do
		{
			System.out.print("Please input first number (3~10): ");
			length = scanner.nextInt();
			if(length <3 || length >10)
			{
				System.out.println("Out of range!");//輸入並檢查
			}
		}while(length <3 || length >10);
		
		do
		{
			System.out.print("Please input second number (3~10): ");
			width = scanner.nextInt();
			if(width <3 || width >10)
			{
				System.out.println("Out of range!");//輸入並檢查
			}
		}while(width <3 || width >10);

		int[][] map = new int [width][length];
		for(int i=0;i<width;i++)
		{
			for(int j=0;j<length;j++)
			{
				map[i][j]= random.nextInt(2);//隨機產生 1或 0
				System.out.print(map[i][j] +" ");
				
			}
			System.out.println("");
		}
		
		int total=0;
		for(int i=0;i<width;i++)
		{
			for(int j=0;j<length;j++)
			{
				if(map[i][j]==1)//計算相鄰的1的總數
				{
					if( (i+1)<width &&map[i+1][j]==1    )
						total++;
					if( (j+1)<length &&map[i][j+1]==1    )
						total++;
				}
				
			}
		}
		System.out.println(total);
		
		

	}

}
