package ce1002.e2.s102502049;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		java.util.Scanner input = new Scanner(System.in);

		int number1 = 3;
		int number2 = 3;
		do // ask to set number
		{
			if (number1 < 3 || number1 > 10)
				System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			number1 = input.nextInt();
		} while (number1 < 3 || number1 > 10);

		do {
			if (number2 < 3 || number2 > 10)
				System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			number2 = input.nextInt();
		} while (number2 < 3 || number2 > 10);

		int figure[][] = new int[number1][number2]; // set array size

		java.util.Date date = new java.util.Date();
		Random random = new Random(date.getTime());

		for (int i = 0; i < number1; i++) // set random number
		{
			for (int j = 0; j < number2; j++) {
				figure[i][j] = random.nextInt(2);
			}
		}

		for (int i = 0; i < number1; i++) // draw figure
		{
			for (int j = 0; j < number2; j++) {
				System.out.print(figure[i][j] + " ");
			}
			System.out.print("\n"); // next line
		}

		int counter = 0;

		for (int i = 0; i < number1; i++) // count row repeated 1
		{
			for (int j = 0; j < (number2 - 1); j++) {
				if (figure[i][j] + figure[i][j + 1] == 2)
					counter++;
			}
		}

		for (int i = 0; i < number2; i++) // count column repeated 1
		{
			for (int j = 0; j < (number1 - 1); j++) {
				if (figure[j][i] + figure[j + 1][i] == 2)
					counter++;
			}
		}
		System.out.print("total: ");
		System.out.print(counter); // show total
	}
}
