package ce1002.e2.s102502003;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int length=0;
		int width=0;
		
		Scanner scanner = new Scanner(System.in);
		
		do  //規範列數範圍
		{
			System.out.print("Please input first number (3~10): ");
			length=scanner.nextInt();
			if(length<3||length>10)
				System.out.println("Out of range!");
		}while(length<3||length>10);
		
		do  //規範行數範圍
		{
			System.out.print("Please input second number (3~10): ");
			width=scanner.nextInt();
			if(width<3||width>10)
				System.out.println("Out of range!");
		}while(width<3||width>10);
		
		int[][] binary = new int[length][width]; 
		Random random =new Random();  //產生隨機數
		int counter=0;
		
		for(int i=0; i<length; i++)  //將隨機值存入陣列並輸出結果
		{
			for(int j=0; j<width; j++)
			{
				binary[i][j]=random.nextInt(2);
				System.out.print(binary[i][j]);				
			}
			System.out.println();
		}
		
		for(int i=0; i<length; i++)  //判斷橫排連續
		{
			for(int j=0; j<width; j++)
			{
				if(j==width-1)
					break;
				else if(binary[i][j]==1 && binary[i][j+1]==binary[i][j])
					counter++;
			}
					
		}
		
		for(int j=0; j<width; j++)  //判斷直行連續
		{
			for(int i=0; i<length; i++)
			{
				if(i==length-1)
					break;
				else if(binary[i][j]==1 && binary[i+1][j]==binary[i][j])
					counter++;
				
			}
		}
		
		System.out.print("total: "+counter);
		

	}

}
