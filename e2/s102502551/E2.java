package s102502551;

import java.util.Scanner;
import java.util.Random;

public class E2 {
	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);

		System.out.print("Please input first number (3~10): ");

		int a=0,b=0,n=0;
		int[][] arr=new int[10][10];

		Random ran = new Random();
		
		while(a<3||a>10)
		{
			a=input.nextInt(); 											//輸入

			if(a<3||a>10)	{											//判斷
				System.out.println("Out of range !");
				System.out.println("Please input first number (3~10): ");
			}
		}
		
		System.out.print("Please input second number (3~10): ");
		
		while(b<3||b>10)
		{
			b=input.nextInt(); 											//輸入

			if(b<3||b>10)	{											//判斷
				System.out.println("Out of range !");
				System.out.println("Please input second number (3~10): ");
			}
		}		

		
		for(int i=0;i<a;i++)											//傳進陣列
		{
			for(int j=0;j<b;j++)
			{
				arr[i][j]=(ran.nextInt(2));
			}
		}

		for(int i=0;i<a;i++)											//印出來
		{
			for(int j=0;j<b;j++)
			{
				System.out.print(arr[i][j]);
				System.out.print(" ");
			}
			System.out.println();
		}		
		
		for(int i=0;i<a;i++)											//算橫的
		{
			for(int j=1;j<b;j++)
			{
				if(arr[i][j]==arr[i][j-1])
				{
					if(arr[i][j]==1)
					{
						n=n+1;
					}
				}
			}
		}
		
		for(int i=1;i<a;i++)											//算直的
		{
			for(int j=0;j<b;j++)
			{
				if(arr[i][j]==arr[i-1][j])
				{
					if(arr[i][j]==1)
					{
						n=n+1;
					}
				}
			}
		}
		System.out.print("total: ");
		System.out.print(n);
	}
}
