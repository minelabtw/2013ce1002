package ce1002.e2.s102502543;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);  
		int l; //宣告變數
		int w;
		do {
			System.out.print("Please input first number (3~10): "); //輸出
			l = scanner.nextInt(); //輸入
			if (l < 3 || l > 10)
				System.out.println("Out of range!");
		} while (l < 3 || l > 10);
		do {
			System.out.print("Please input second number (3~10): "); 
			w = scanner.nextInt();
			if (w < 3 || w > 10)
				System.out.println("Out of range!");
		} while (w < 3 || w > 10);
		int a[][]; //宣告陣列
		a = new int[l][w];  //陣列大小
		Random ran = new Random();
		for (int i=0;i<l;i++)
		{
			for (int j=0;j<w;j++)
			{
				a[i][j]=ran.nextInt(2);
			}
		}
		for (int i=0;i<l;i++)
		{
			for (int j=0;j<w;j++)
			{
				System.out.print(a[i][j]);
				System.out.print(" ");
			}
			System.out.print("\n");
		}
		int total=0;
		for (int i=0;i<l;i++)  //判斷橫向
		{
			for (int j=0;j<w-1;j++)
			{
				if(a[i][j]==1 && a[i][j+1]==1)
					total++;
			}
		}
		for (int i=0;i<w;i++) //判斷縱向
		{
			for (int j=0;j<l-1;j++)
			{
				if(a[j][i]==1 && a[j+1][i]==1)
					total++;
			}
		}
		System.out.print("total: ");
		System.out.print(total);
		
		
		

	}

}
