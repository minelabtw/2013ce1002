package ce1002.e2.n102502002;
import java.util.Scanner; //Import Scanner
import java.util.Random;  //Import random number

public class E2 {

	public E2() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	private static Scanner scanner;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][] arr;
		int n1=0;
		scanner = new Scanner(System.in);
		arr = new int[10][10];
		System.out.print("Please input first number (3~10): "); //input the first number, and see if it is out of range
		n1 = scanner.nextInt();
		while(n1<3||n1>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			n1 = scanner.nextInt();
		}

		int n2=0;
	    System.out.print("Please input second number (3~10): "); //input second number
		n2 = scanner.nextInt();
		while(n2<3||n2>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			n2 = scanner.nextInt();
		}

		Random rnd = new Random();
		for(int i=0;i<n1;i++)
		{
			for(int j=0; j<n2;j++)
			{
				arr[i][j]=rnd.nextInt(2); //save random number in an array
				System.out.print(arr[i][j]); //print the array
				System.out.print(" ");
			}
			System.out.println();
		}

		int counter=0;
		for(int i=0;i<n1;i++)
		{
			for(int j=0;j<n2;j++)
			{
				if(arr[i][j]==1&&arr[i][j]==arr[i][j+1]) //if there are two 1s vertically, counter plus one
					counter++;
				if(arr[i][j]==1&&arr[i][j]==arr[i+1][j]) //if ther are two 1s horizontally, counter plus one
					counter++;
			}
		}

		System.out.print("total: "); //out put the result
		System.out.print(counter);

	}

}
