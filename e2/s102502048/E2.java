package ce1002.e2.s102502048;
import java.util.Scanner;
import java.util.Random;
public class E2 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int[][] arr = new int[10][10];
		int w=0,l=0;
		do
		{			
			System.out.print("Please input first number (3~10): ");//輸入列
			w = input.nextInt();
			if(w<3 || w>10)
				System.out.print("Out of range!\n");
		}while(w<3 || w>10);
		do
		{
			System.out.print("Please input second number (3~10): ");//輸入行
			l = input.nextInt();
			if(l<3 || l>10)
				System.out.print("Out of range!\n");
		}while(l<3 || l>10);
		
        for(int i =0; i< w;i++)//亂數寫入陣列
        {
        	for(int j =0; j< l; j++)
            {
            	Random ran = new Random();//產生亂數
            	arr[i][j]=ran.nextInt(2);   
            	System.out.print(arr[i][j]);
            }
        	System.out.print("\n");        	
        }
        
        int total = 0;
        for(int i =0; i< w;i++)//判斷橫相連
        {
        	for(int j =0; j< l-1; j++)
            {
            	if(arr[i][j]==arr[i][j+1] && arr[i][j]==1)
            		total++;
            }	
        }
        for(int i =0; i< l;i++)
        {
        	for(int j =0; j< w-1; j++)//判斷直相連
            {
            	if(arr[j][i]==arr[j+1][i] && arr[j][i]==1)
            		total++;
            }	
        }
        System.out.print("total: "+total);
	}
}
