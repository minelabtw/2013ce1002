package ce1002.e2.s102502033;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input= new Scanner(System.in);//輸入物件
		int i=0;//有幾列
		int w=0;//有幾行
		int total=0;
		System.out.print("Please input first number (3~10): ");
		i= input.nextInt();
		while(i>10 || i<3)
		{
			System.out.print("Out of range!\nPlease input first number (3~10): ");
			i= input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		w= input.nextInt();
		while(w>10 || w<3)
		{
			System.out.print("Out of range!\nPlease input second number (3~10): ");
			w= input.nextInt();
		}
		int [][] arr= new int[i][w];//宣告陣列
		Random ran= new Random();//變數物件
		for(int W=0;W<w;W++)
		{
			for(int L=0;L<i;L++)
			{
				arr[L][W]= ran.nextInt(2);
				System.out.print(arr[L][W]+" ");//輸出陣列表
			}
			System.out.println();
		}
		
		for(int a=0;a<w;a++)
		{
			for(int b=0;b<i;b++)
			{
				if(b!=i-1 && arr[b][a]==arr[b+1][a] && arr[b][a]==1)//跟右邊比同時又不能等於末行
				{
					total=total+1;
				}
				if(a!=w-1 && arr[b][a]==arr[b][a+1] && arr[b][a]==1)//跟下面比同時又不能是末列
				{
					total=total+1;
				}
				
			}
		}
		System.out.print("total: "+total);
		

	}

}
