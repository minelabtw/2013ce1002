package ce1002.e2.s102502534;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int length = 0;
		int width = 0;
		int total = 0;
		//判斷輸入是否在3~10
		while (length < 3 || length > 10) {
			System.out.print("Please input first number (3~10): ");
			length = input.nextInt();
			if (length < 3 || length > 10) {
				System.out.println("Out of range!");
			}
		}
		while (width < 3 || width > 10) {
			System.out.print("Please input second number (3~10): ");
			width = input.nextInt();
			if (width < 3 || width > 10) {
				System.out.println("Out of range!");
			}
		}
		int[][] array = new int[length][width];
		Random rand = new Random();
		//用亂數0.1填滿陣列
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < width; j++)
				array[i][j] = rand.nextInt(2);

		}
		//印出陣列
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print(array[i][j] + " ");
			}
			System.out.println("");
		}
		//算重複幾個1
		for (int i = 0; i < length-1; i++) {
			for (int j = 0; j < width-1; j++) {
				if (array[i][j] == 1 && array[i][j + 1] == 1) {
					total++;
				}
				if (array[i][j] == 1 && array[i + 1][j] == 1) {
					total++;
				}
			}
		}
		//輸出有幾個1重複
		System.out.print("total: " + total);
	}
}
