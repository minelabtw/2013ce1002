package ce1002.e2.s101502205;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	private static Scanner scanner;
	private static Random ran;
	
	public static void main(String[] args) {
		
		//Create a scanner object
		scanner = new Scanner(System.in);
		//Create a random object
		ran = new Random();
		
		int length, width;
		
		//User input
		//input length(height)
		System.out.print("Please input first number (3~10): ");
		length = scanner.nextInt();
		while(length<3 || length>10){
			//error
			System.out.println("Out of range!");
			//input again
			System.out.print("Please input first number (3~10): ");
			length = scanner.nextInt();
		}
		//input widtht
		System.out.print("Please input second number (3~10): ");
		width = scanner.nextInt();
		while(width<3 || width>10){
			//error
			System.out.println("Out of range!");
			//input again
			System.out.print("Please input second number (3~10): ");
			width = scanner.nextInt();
		}
		
		int i, j, tmp, counter=0, row[];
		
		row = new int[width]; //storing the result of last row
		
		for(i=0; i<length; i++){
			
			for(j=0; j<width; j++){
				
				//random a number(0 or 1) for [i][j] block
				tmp = ran.nextInt(2);
				
				//test if the [i-1][j] and [i][j] are consecutive 1
				if(i!=0){
					if(row[j]==1 && tmp==1)
						counter++;
				}
				
				row[j]=tmp;
				
				//test if the [i][j-1] and [i][j] are consecutive 1 
				if(j!=0){
					if(row[j-1]==1 && row[j]==1)
						counter++;
				}
				//output the [i][j] block
				System.out.print(row[j]);
				System.out.print(" ");
			}
			System.out.println(); //end of a row
		}
		
		//output the counter
		System.out.print("total: ");
		System.out.println(counter);
	}

}
