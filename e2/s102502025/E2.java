package ce1002.e2.s102502025;
import java.util.Scanner;		//給予input指令
public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		//允許輸入
		
	    int xy [][] = new int[10][10];		//矩陣
		int x = 0;		//排
		int y = 0;		//行
		int total = 0;		//總數
		
		while(x<3 || x>10)		//判斷排範圍
		{
			System.out.print("Please input first number (3~10): ");
			x = input.nextInt();
			if(x<3 || x>10)		//假如符合條件，將進行一下條件。
			{
				System.out.print("Out of range!\n");
			}
		}
		
		while(y<3 || y>10)		////判斷行範圍
		{
			System.out.print("Please input second number (3~10): ");
			y = input.nextInt();
			if(y<3 || y>10)		//假如符合條件，將進行一下條件。
			{
				System.out.print("Out of range!\n");
			}
		}
		
		for (int k=0 ; k<x ; k++)		//排移動數
		{
			for (int l=0 ; l<y ; l++)		//行移動數
			{
				System.out.print(xy[k][l] = (int)(Math.random()*2));
				if (k<x)
				{
					System.out.print(" ");
				}
			}
			System.out.println();
		}
		
		for (int k=0 ; k<x ; k++)		//算連續1的總數
		{
			for (int l=0 ; l<y ; l++)
			{
				if(xy[k][l]==1)
				{
					if(xy[k][l]==xy[k][l+1])
					{
						total++;
					}
					if (xy[k][l]==xy[k+1][l])
					{
						total++;
					}
				}
			}
		}
		
		System.out.print("Total: " + total);		//輸出答案
	    input.close();
	}

}
