package ce1002.e2.s102502010;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		int w,l,i,j,total;
		final int m=10;
		int n[][] = new int[m][m];
		Scanner input = new Scanner(System.in);
		System.out.print("Please input first number (3~10): ");
		l=input.nextInt();
		while(l<3 || l>10)  //輸入列
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l=input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		w=input.nextInt();
		while(w<3 || w>10)  //輸入行
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w=input.nextInt();
		}
		Random ran = new Random();  
		for(i=0;i<l;i++)
			for(j=0;j<w;j++)
				n[i][j]=ran.nextInt(2);  //random
		total=0;
		for(i=0;i<l;i++)
		{
			for(j=0;j<w;j++)  //計算連續的一
			{
				if(j+1<w)
					if(n[i][j]+n[i][j+1]==2)
						total++;
				if(i+1<l)
					if(n[i][j]+n[i+1][j]==2)
						total++;
			}
		}
		for(i=0;i<l;i++)
		{
			for(j=0;j<w;j++)
				System.out.print(n[i][j]+" ");
			System.out.println("");
		}
		System.out.println("total: "+total);
	}
}
