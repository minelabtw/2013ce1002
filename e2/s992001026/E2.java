package ce1002.e2.s992001026;

import java.util.*;

public class E2 {


	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
        int n ,m , x = 0 ;
		System.out.print("Please input first number (3~10): ");
        n = scn.nextInt();
        
        // 判斷輸入行高是否超出範圍
        while ( n < 3 || n > 10){
        	System.out.println("Out of range!");
        	System.out.print("Please input first number (3~10): ");
        	n = scn.nextInt();
        }
        
        
        System.out.print("Please input second number (3~10): ");
        // 列寬
        m = scn.nextInt();
        while ( m < 3 || m > 10){
        	System.out.println("Out of range!");
        	System.out.print("Please input second number (3~10): ");
        	m = scn.nextInt();
        }
        
        Random ran = new Random();
        
        int[][] num= new int[n][m];
        
        // 把亂數存入陣列 以及判斷是否有連續數值1
        for ( int i = 0 ; i < n ; ++i){
        	for (int j = 0 ; j < m ; ++j ){
        		num[i][j] = ran.nextInt(2);
        	    if( j !=0 && num[i][j-1] == 1 && num[i][j]==1 )
        	    	x++;
        	    if( i != 0 && num[i-1][j]== 1 && num[i][j] == 1)
        	    	x++;
          	}
        }
         
        //輸出陣列
        for ( int i = 0 ; i < n ; ++i){
        	for (int j = 0 ; j < m ; ++j ){
        		System.out.print(num[i][j]);
        	}
        	System.out.println();
        }
          
        System.out.println("total: "+ x );
	}
}
