package ce1002.e2.s102502515;

import java.util.Scanner;


public class E2 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);//匯入輸入
		int num1 , num2;
		int table[][] = new int [10][10];//宣告陣列
		int count = 0;
		
		do{
		System.out.print("Please input first number (3~10): ");
		num1 = input.nextInt();
		if (num1 > 10 || num1 < 3)
		{
			System.out.println("Out of range!");
		}
		}
		while (num1 > 10 || num1 < 3);//利用do while確認輸入範圍
	
		do{
		System.out.print("Please input second number (3~10): ");
		num2 = input.nextInt();
		if (num2 > 10 || num2 < 3)
		{
			System.out.println("Out of range!");
		}
		}
		while (num2 > 10 || num2 < 3);//利用do while確認輸入範圍
		
		for (int l=1 ; l<=num1 ; l++)//l for 列
		{
			for (int w=1 ; w<=num2 ; w++)//w for 行
			{
				System.out.print(table[l][w] = (int)(Math.random()*2));//將亂數0-0.999變成0-1.9999，然後取整數
				System.out.print(" ");//將每個數字區分
			}
			System.out.println("");//換行
		}
		
		for (int i = 1 ; i <= num1 ; i++)
		{
			for (int j = 1 ; j <= num2 ; j++)
			{
				if (table[i][j]==1)//將兩者相等，卻為0的狀況排除
				{
					if (table[i][j] == table[i][j+1])//向右一格比較
					{
						count++;
					}
					if (table[i][j] == table[i+1][j])//向下一格比較，因此不會重複
					{
						count++;
					}
				}
			}
		}
		
		System.out.print("total: " + count);
		input.close();//把input關掉
	}
}
