package ce1002.e2.s102502007;

import java.util.Scanner;
import java.util.Random;
public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int length=0 , width=0;
		java.util.Scanner input = new Scanner(System.in);
		Random random = new Random();//set up the random object
		do
		{
			System.out.print("Please input first number (3~10): ");
            width = input.nextInt();
			if(width>10 || width<3)
				System.out.println("Out of range!");
		}while(width>10 || width<3);//loop until the correct number is input
		//the first number represent column
		do
		{
			System.out.print("Please input second number (3~10): ");
			length = input.nextInt();
			if(length>10 || length<3)
				System.out.println("Out of range!");
		}while(length>10 || length<3);//loop until the correct number is input
		//the second number represent row
		input.close();
		
		int counter=0;//calculate the total continuity of the matrix
		int[][] matrix = new int[width][length];
		for(int i=0;i<width;i++)
		{
			for(int j=0;j<length;j++)
				matrix[i][j]=random.nextInt(2);
		}
		for(int i=0;i<width;i++)
		{
			{
			for(int j=0;j<length;j++)
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
			}
//print the matrix		  
		for(int i=0;i<width;i++)
			for(int j=0;j<length-1;j++)
			if(matrix[i][j]+matrix[i][j+1]==2)
			counter++;
		//once two consecutive "1" number is discovered in a row , the counter +1
		for(int i=0;i<width-1;i++)
			for(int j=0;j<length;j++)
			if(matrix[i][j]+matrix[i+1][j]==2)
			counter++;
		//once two consecutive "1" number is discovered in a column , the counter +1
		System.out.print("total: " + counter);
			}
}
