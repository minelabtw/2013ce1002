package ce1002.e2.s101201005;
import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		int l=0, w=0, count=0;
		int [][] x=new int [10][10];
		for (int i=0; i<x.length; i++)
		{
			for(int j=0; j< x[i].length; j++)
				x[i][j]=i+j;
		}
		
		Scanner input = new Scanner(System.in);
		Random ran = new Random();
		
		System.out.print("Please input first number (3~10): ");
		l=input.nextInt();
		while ( l<3 || l>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			l=input.nextInt();
		}
		System.out.print("Please input second number (3~10): ");
		w=input.nextInt();
		while ( w<3 || w>10)
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			w=input.nextInt();
		}
		

		for (int i=0; i<l; i++)
		{
			for(int j=0; j<w; j++)
			{
				x[i][j]=ran.nextInt(2);
			    System.out.print(x[i][j]+" ");
			    
			    if (x[i][j]==1 && j>0)
			    {
			    	if (x[i][j]==x[i][j-1])
			    		count++;
			    }
			    if (x[i][j]==1 && i>0)
			    {
			    	if (x[i][j]==x[i-1][j])
			    		count++;
			    }
			}
			System.out.println();
		}
		System.out.print("total: "+count);

	}

}
