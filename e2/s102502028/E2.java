package ce1002.e2.s102502028;
import java.util.Scanner;
public class E2 {

	public static void main(String[] args) {
		int l = 0 ;  //宣告
		int w = 0 ;

		System.out.print("Please input first number (3~10): ") ;  //輸入符合條件的2個數字
		Scanner cin = new Scanner(System.in) ;
		l = cin.nextInt() ;
		while (l < 3 || l > 10)
		{
			System.out.println("Out of range!") ;
			System.out.print("Please input first number (3~10): ") ;
			l = cin.nextInt();
		}
		
		System.out.print("Please input second number (3~10): ") ;
		w = cin.nextInt();
		while (w < 3 || w > 10)
		{
			System.out.println("Out of range!") ;
			System.out.print("Please input second number (3~10): ") ;
			w = cin.nextInt();
		}
		
		int random [][] = {} ;
		random = new int [l][w] ;
		for (int a = 0 ; a < l ; a++)  //隨機產生的數字存在二維陣列裡面
		{
			for (int b = 0 ; b < w ; b++)
			{
				random [a][b] = (int)(Math.random()*10 % 2) ;
			}
		}
		
		for (int a = 0 ; a < l ; a++)  //印出隨機數字
		{
			for (int b = 0 ; b < w ; b++)
			{
				System.out.print(random[a][b]);
			}
			System.out.println();
		}
		
		int total = 0 ;           //計算total
		for (int a = 0 ; a < l ; a++)
		{
			for (int b = 0 ; b < w-1 ; b++)
			{
				if (random[a][b] == random[a][b+1] && random[a][b] == 1)
					total = total + 1 ;
			}
		}
		
		for (int a = 0 ; a < w ; a++)
		{
			for (int b = 0 ; b < l-1 ; b++)
			{
				if (random[b][a] == random[b+1][a] && random[b][a] == 1)
					total = total + 1 ;
			}
		}
		
		System.out.print("total: " + total);  //輸出total

	}

}
