package ce1002.e2.s102502519;

import java.util.*;

public class E2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1=0,num2=0;    //宣告二變數
		Scanner sc = new Scanner(System.in);
		while(num1<3 || num1>10){
			System.out.print("Please input first number (3~10): ");
			num1=sc.nextInt();
			if(num1<3 || num1>10){
				System.out.println("Out of range!");
			}
		}
		while(num2<3 || num2>10){
			System.out.print("Please input second number (3~10): ");
			num2=sc.nextInt();
			if(num2<3 || num2>10){
				System.out.println("Out of range!");
			}
		}
		int[][] arr = new int[10][10];    //宣告一陣列
		Random rand = new Random();    //亂數
		
		for(int i=0;i<num1;i++){
			for(int j=0;j<num2;j++){
				arr[i][j]=rand.nextInt(2);   //0和1的亂數
				System.out.print(arr[i][j] + " ");
		}
			System.out.println();
			}
		int total=0;
		for(int k=0;k<num1;k++){
			for(int l=0;l<num2;l++){
				if(l != num2-1){
					if(arr[k][l] == arr[k][l+1] && arr[k][l]==1)
						total++;
				}
				if(k != num1-1){
					if(arr[k][l] == arr[k+1][l] && arr[k][l]==1)
						total++;
				}
			}
		}
		System.out.print("total: " + total);
		}
}
