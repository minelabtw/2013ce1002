package ce1002.e2.s102502511;

import java.util.Scanner;
import java.util.Random; //叫出亂數的類組

public class E2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int l;
		int w;
		int a = 0; //用來記錄出現次數
		int [][] arrary = new int [10][10]; //用來存入亂數
		
		Scanner input = new Scanner (System.in); //紀錄輸入的數字
		
		System.out.print("Please input first number (3~10): ");
		
		do
		{
			l  = input.nextInt();
			if(l < 3 || l >10)
			{
				System.out.print("Out of range!\nPlease input first number (3~10): ");
			}
		}while(l < 3 || l >10);
		
		System.out.print("Please input second number (3~10): ");
		
		do
		{
			w  = input.nextInt();
			if(w < 3 || w >10)
			{
				System.out.print("Out of range!\nPlease input second number (3~10): ");
			}
		}while(w < 3 || w >10);
		
		Random ran = new Random();//使用亂數
		
		for(int i = 0; i < l; i++)
		{
			for(int j = 0; j < w; j++)
			{
				arrary[i][j] = ran.nextInt(2);//括號中的數字等於變數個數
				System.out.print(arrary[i][j]);
				
			}
			System.out.println();
		}
		for(int i = 0; i < l;i++)
		{
			for(int j = 0; j < w; j++)
			{
				if(arrary[i][j] + arrary[i][j+1] == 2)
				{
					a++;//記錄橫的
				}
				if(arrary[i][j] + arrary[i+1][j] == 2)
				{
					a++;//紀錄直的
				}
			}
		}
		System.out.print("total: ");
		System.out.print(a);
		input.close(); //關閉input
   }
}
