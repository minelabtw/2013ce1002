package ce1002.e2.s102502505;

import java.util.Scanner;//匯入套件類別
import java.util.Random;

public class E2 {

	/**
	 * @param args
	 */
	private static Scanner scanner;//宣告輸入套件
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a=0;//定義列數
		int b=0;//定義行數
		int[][] array1;//定義矩陣存亂數
		int a1=0;//定義兩個數來比較是否一樣
		int a2=0;
		int total=0;//定義計算總數
		scanner = new Scanner(System.in);//將輸入變成物件
		Random random1 = new Random();//將亂數書成物件
		array1 = new int[10][10];//將矩陣變成物件
		
		System.out.print("Please input first number (3~10): ");//輸出字串
		a = scanner.nextInt();//輸入數等於列數
		while ( a>10 || a<3 )//用迴圈限制範圍
		{
			System.out.println("Out of range!");
			System.out.print("Please input first number (3~10): ");
			a = scanner.nextInt();
		}
		
		System.out.print("Please input second number (3~10): ");
		b = scanner.nextInt();
		while ( b>10 || b<3 )
		{
			System.out.println("Out of range!");
			System.out.print("Please input second number (3~10): ");
			b = scanner.nextInt();
		}
		
		for (int i=0;i<a;i++)//利用迴圈至早亂數及存到陣列中
		{
			for (int j=0;j<b;j++)
			{
				array1[i][j] = (random1.nextInt(2));
				System.out.print(array1[i][j]);
			}
			System.out.println();
		}
		
		for (int i=0;i<a;i++)//利用迴圈算total值，先算列
		{
			for (int j=0;j<b-1;j++)
			{
				a1=array1[i][j];
				a2=array1[i][j+1];
				if(a1==a2 && a1==1)//如果符合條件，total+1
					total++;
			}
		}
		
		for (int i=0;i<a-1;i++)//再算行
		{
			for (int j=0;j<b;j++)
			{
				a1=array1[i][j];
				a2=array1[i+1][j];
				if(a1==a2 && a1==1)
					total++;
			}
		}
		
		System.out.print("total: "+total);//最後輸出total值
	}

}
