package ce1002.e2.s102502555;

import java.util.Scanner;
import java.util.Random;

public class E2 {

	public static void main(String[] args) {
		
		int l , w;
		int total = 0;
		Scanner input = new Scanner(System.in);
		Random ran = new Random();
		
		//輸入長並檢查有無超出範圍
		do{
			System.out.print("Please input first number (3~10): ");
			l = input.nextInt();
			if(l <3 || l > 10){
				System.out.println("Out of range!");
			}
		}while(l <3 || l > 10);
		
		//輸入寬並檢查有無超出範圍
		do{
			System.out.print("Please input second number (3~10): ");
			w = input.nextInt();
			if(w <3 || w > 10){
				System.out.println("Out of range!");
			}
		}while(w <3 || w > 10);
		
		//宣告存表格的陣列
		int[][] array = new int[l][w];

		//存1和0進表格
		for(int i = 0 ; i < l ; i++){
			for(int j = 0 ; j < w ; j++){
				array[i][j] = ran.nextInt(2);
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
		
		//判斷有無相連
		for(int i = 0 ; i < l ; i++){
			for(int j = 0 ; j < w ; j++){
				if(array[i][j] == 1){
					if(i == 0 && j != 0){  //如果是第一列判斷有無跟左邊相同
						if(array[i][j] == array[i][j - 1]){
							total += 1;
						}
					}else if(i != 0){  //如果不是第一列
						if(array[i][j] == array[i - 1][j]){  //判斷有無跟上面相同
							total += 1;
						}
						if(j != 0){  //如果不是第一個
							if(array[i][j] == array[i][j - 1]){  //判斷有無跟左邊相同
								total += 1;
							}
						}
					}
				}
			}
		}
		System.out.println("total: " + total);  //印出總和
	}
}