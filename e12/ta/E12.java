
public class E12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// initialize a bank 
		Bank bank = new Bank();
		
		// initialize 3 consumer and wrap with java thread
		Thread thread = new Thread(new Consumer(bank,1));
		Thread thread1 = new Thread(new Consumer(bank,2));
		Thread thread2 = new Thread(new Consumer(bank,3));
		
		// start each thread to perform process 
		thread.start();
		thread1.start();
		thread2.start();
	}

}
