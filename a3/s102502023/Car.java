package ce1002.a3.s102502023;

import java.util.Scanner;

class Car {	
	Scanner input = new Scanner(System.in);
	private int id = 0;// 賽車id
	private float maxSpeed = 0;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	//private float MAXSPEED[] = new float[100];
	private int a = 0;
		
	public Car() {}
    public void Setid() {
    	while(id <= 0){
    	System.out.print("Please input the number of cars:\n");
    	id = input.nextInt();
    	if(id <= 0)
    	{
    		System.out.print("Out of range!\n");
    	}    		
    }
    	}
	 
    public void Setmaxspeed(float MAXSPEED[]) {
    	
    	do {
    		System.out.print("Please input the max speed of this car(" + a + "):\n");
    		maxSpeed = input.nextFloat();
    		if(maxSpeed <= 0) {
    			System.out.print("Out of range!\n");
    		}
    		else {
    			MAXSPEED[a] = maxSpeed;
    			a++;
    		}
    	}
    	while(maxSpeed <= 0 || a < id);
    }
    
   public void Getinfo(float MAXSPEED[]) {
	   for(int i = 0; i < id ; i++ )
	   {
		   System.out.print("Car id is " + i + "\n" + "Car max speed is " + MAXSPEED[i] + "\n" +
				   "Car turbo is " + isTurbo + "\n");
	   }
   }
   
   public int getid() {
	   return id;
   }
}
