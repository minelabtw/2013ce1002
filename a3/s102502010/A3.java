package ce1002.a3.s102502010;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int n,i;
		float speed;
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		n=input.nextInt();
		while(n<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=input.nextInt();
		}
		Car[] car = new Car[n]; //�ŧi�}�C
		for(i=0;i<car.length;i++)
		{
			System.out.println("Please input the max speed of this car(" + i + "): ");
			speed=input.nextFloat();
			while(speed<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed=input.nextFloat();
			}
			car[i] = new Car();
			car[i].setMaxSpeed(speed);
			car[i].setid(i);
		}
		System.out.println("Output car status.");
		for(i=0;i<car.length;i++)  //��X
		{
			System.out.println("Car id is " + car[i].getid() + ".");
			System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + car[i].getIsTurbo() + ".");
		}
	}
}
