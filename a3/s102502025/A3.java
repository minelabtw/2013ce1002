package ce1002.a3.s102502025;

import java.util.Scanner;		//開啟input的工具夾

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);		//開啟input

		int number = 0;		//建立汽車數量變數
		float maxspeed = 0;		//建立速度變數

		while (number <= 0) {		//設汽車數量範圍迴圈
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number <= 0) {
				System.out.println("Out of range!");
			}
		}

		Car car[] = new Car[number];		//建立陣列

		for (int id = 0; id < number; id++) {		//輸入車子資料
			car[id] = new Car();
			car[id].setId(id);
		}

		for (int l = 0; l < number; l++) {		//輸入車子速度
			while (maxspeed <= 0) {		//速度範圍
				System.out.println("Please input the max speed of this car("+l+"):");
				maxspeed = input.nextFloat();
				if (maxspeed <= 0) {
					System.out.println("Out of range!");
				}
			}
			car[l].setMaxSpeed(maxspeed);		//記錄資料
			maxspeed = 0;
		}
		
		System.out.println("Output car status.");
		
		for(int n = 0; n < number; n++){		//輸出資料
			System.out.println("Car id is " + car[n].getId() + ".");
			System.out.println("Car max speed is " + car[n].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + car[n].isTurbo() + ".");
		}

		input.close();		//關閉
	}

}