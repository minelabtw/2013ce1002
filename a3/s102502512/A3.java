package ce1002.a3.s102502512;
import java.util.*;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int n=0;												//Line 9-17: let user enter the number of car
		do{
		System.out.println("Please input the number of car:");
		n=input.nextInt();
		if(n<1)
		{
			System.out.println("Out of range!");
		}
		}while(n<1);
		Car rec[]=new Car[n];									//Line 18: create an array
		for(int i=0;i<n;i++)
		{
			rec[i]=new Car(i);									//Line 21: a constructor which translate n to id
			rec[i].enterspd();									//Line 22: call the method in Car class
		}
		System.out.println("Output car status.");				//Line 24-29: print the car status
		for(int j=0;j<n;j++)
		{
			System.out.println("Car id is "+j+".");
			System.out.println("Car max speed is "+rec[j].spd()+".");
			System.out.println("Car turbo is "+rec[j].theturbo()+".");
		}
	}

}

