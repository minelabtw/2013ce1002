package ce1002.a3.s102502512;
import java.util.Scanner;

class Car
{
	Scanner input = new Scanner(System.in);
	private int id;
	private float maxSpeed;
	private boolean isTurbo= true;
	public Car (int i)									//Line 10-13: create a constructor which pass the parameter to id
	{
		id=i;
	}
	
	public void enterspd()								//Line 15-26: create a method to let user enter the maxspeed of each car
	{
		do{
		System.out.println("Please input the max speed of thes car("+id+"):");
		maxSpeed=input.nextFloat();
		if(maxSpeed<=0)
		{
			System.out.println("Out of range!");
		}
		}while(maxSpeed<=0);
		
	}
	public float spd()									//Line 27-30: create a method to return the maxspeed 
	{
		return maxSpeed;
	}
	public boolean theturbo()
	{
		return isTurbo;									//Line 33: return the isturbo
	}
}