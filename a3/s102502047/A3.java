package ce1002.a3.s102502047;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		System.out.println("Please input the number of cars: ");
		Scanner p = new Scanner(System.in);
		int n=p.nextInt();
		for(;n<1;)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=p.nextInt();
		}
		
		Car c[]= new Car [n];//宣告Car的陣列
		for(int i=0;i<n;i++)
		{
			c[i]= new Car(i);//把Car放到陣列裡	
			System.out.println("Please input the max speed of this car("+i+"): ");
			float v=p.nextFloat();
			for(;v<=0;)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"): ");
				v=p.nextFloat();
			}
			c[i].sm(v);
		}
		System.out.println("Output car status.");
		for(int i=0;i<n;i++)
		{
			System.out.println("Car id is "+c[i].gi()+".");
			System.out.println("Car max speed is "+c[i].gm()+".");
			System.out.println("Car turbo is "+c[i].gs()+".");
			
		}
		
	}

}
