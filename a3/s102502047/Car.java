package ce1002.a3.s102502047;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Car(int a)//建構子
	{
		id=a;
		isTurbo=true;
	}
	void sm(float v)//存入速度
	{
		maxSpeed=v;
	}
	int gi()//回傳id
	{
		return id;
	}
	float gm()//回傳最高速
	{
		return maxSpeed;
	}
	boolean gs()//回傳有無渦輪
	{
		return isTurbo;
	}
}
