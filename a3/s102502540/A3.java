package ce1002.a3.s102502540;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int id;
		do {
			System.out.println("Please input the number of cars: "); // 輸出
			id = input.nextInt(); //輸入
			if (id < 1)
				System.out.println("Out of range!");
		} while (id < 1);
		Car[] car = new Car[id]; // 建立car陣列
		for (int a = 0; a < id; a++)
			car[a] = new Car(a);
		float ms;
		for (int a = 0; a < id; a++) {
			do {
				System.out.println("Please input the max speed of this car("
						+ car[a].getid() + "): ");
				ms = input.nextFloat(); 
				car[a].setmaxSpeed(ms); // 存入陣列
				if (ms <= 0)
					System.out.println("Out of range!");
			} while (ms <= 0);
		}
		System.out.println("Output car status.");
		for (int a = 0; a < id; a++) { // 輸出結果
			System.out.println("Car id is " + car[a].getid()
					+ ".\nCar max speed is " + car[a].getmaxSpeed()
					+ ".\nCar turbo is " + car[a].getisTurbo() + ".");
		}
	}
}