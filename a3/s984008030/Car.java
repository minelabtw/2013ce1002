package ce1002.a3.s984008030;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int _id, float _maxSpeed, boolean _isTurbo) {//建構式
		this.id = _id;
		this.maxSpeed = _maxSpeed;
		this.isTurbo = _isTurbo;
	}
	
	int getId() {//取得賽車id
		return this.id;
	}
	
	void setMaxSpeed(float _maxSpeed) {//設定最高速值
		this.maxSpeed = _maxSpeed;
	}
	
	float getMaxSpeed() {//取得最高速值
		return this.maxSpeed;
	}
	
	void setIsTurbo(boolean _isTurbo) {//設定有無渦輪
		this.isTurbo = _isTurbo;
	}
	
	boolean getIsTurbo() {//取得有無渦輪
		return this.isTurbo;
	}
}
