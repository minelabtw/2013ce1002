package ce1002.a3.s984008030;
import ce1002.a3.s984008030.Car;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int numberOfCar = 1;//宣告int形態的車量變數 並初始化為1
		Scanner scanner = new Scanner(System.in);//宣告Scanner形態的scanner物件 並初始化
		Car[] carArray;//宣告形態為Car[]的汽車陣列
		//取得車量
		while (true) {
			System.out.println("Please input the number of cars: ");
			numberOfCar = scanner.nextInt();
			if (numberOfCar <= 0) {
				System.out.println("Out of range!");
			}
			else {
				break;
			}
		}
		//初始化汽車陣列
		carArray = new Car[numberOfCar];
		for (int i = 0; i < carArray.length;) {
			float maxSpeed = (float)1.0;//宣告float形態的車量變數 並初始化為1.0
			System.out.println("Please input the max speed of this car(" + i + "):");
			maxSpeed = scanner.nextFloat();
			if (maxSpeed <= 0.0) {
				System.out.println("Out of range!");
			}
			else {
				carArray[i] = new Car(i, maxSpeed, true);
				i++;
			}
		}
		//輸出汽車陣列的結果
		System.out.println("Output car status.");
		for (int i = 0; i < carArray.length; i++) {
			System.out.println("Car id is " + carArray[i].getId() + ".");
			System.out.println("Car max speed is " + carArray[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + carArray[i].getIsTurbo() + ".");
		}
	}

}
