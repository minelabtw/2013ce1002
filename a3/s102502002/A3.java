package ce1002.a3.s102502002;
import java.util.Scanner; //import scanner

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Scanner input = new Scanner(System.in); //創建物件input
		System.out.println("Please input the number of cars: ");
        int cnum=0;
		cnum=input.nextInt(); //輸入賽車數量
		while(cnum<1){  //判斷是否超出範圍
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			cnum=input.nextInt();
		}
		Car car = new Car(); //創建物件car
		int [] id;  //宣告一整數陣列存放賽車ID
		float [] speed;  //宣告一浮點數陣列存放最大車速
		id = new int [cnum];
		speed = new float [cnum];
		for(int i=0; i<cnum; i++){  //儲存ID
			id[i]=car.getID(i);
		}
		
		float sd=0;
		for(int i=0; i<cnum;i++){
		System.out.println("Please input the max speed of this car("+i+"): ");
		sd=input.nextFloat();  //輸入最大車速
		while(sd<=0){
			System.out.println("Out of range!");
			System.out.println("Please input the max speed of this car("+i+"): ");
			sd=input.nextFloat();
		}
		car.saveSpeed(sd);  //儲存車速
		speed[i]=car.getMaxSpeed();
		}
		car.setTurbo();   //設定渦輪的有無
		System.out.println("Output car status.");  //輸出結果
		for(int i=0;i<cnum;i++){
			System.out.println("Car id is "+id[i]+".");
			System.out.println("Car max speed is "+speed[i]+".");
			System.out.print("Car turbo is ");
			car.getTurbo();
		}
		input.close(); //關閉input
	}

}
