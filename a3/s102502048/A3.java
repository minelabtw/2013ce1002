package ce1002.a3.s102502048;
import java.util.Scanner;
public class A3 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int carnum=0;
		float Mspeed=0;
		do
		{
			System.out.println("Please input the number of cars: ");//顯示
			carnum = input.nextInt();//輸入		
			if(carnum <=0)
				System.out.println("Out of range!");//顯示
		}while(carnum <=0);		
		Car[] car = new Car[carnum];//建構陣列		
		for(int i=0;i<carnum;i++)
		{			
			Car _car = new Car(i);//建構物件
			do
			{
				System.out.println("Please input the max speed of this car("+i+"): ");//顯示
				Mspeed = input.nextFloat();//輸入		
				if(Mspeed <=0)
					System.out.println("Out of range!");//顯示
			}while(Mspeed <=0);
			_car.setspeed(Mspeed);
			_car.setturbo(true);
			car[i]=_car;//丟進陣列			
		}
		System.out.println("Output car status.");//顯示		
		for(int i=0;i<carnum;i++)
		{			
			System.out.println("Car id is "+car[i].displayid()+".");
			System.out.println("Car max speed is "+car[i].displaymaxSpeed()+".");
			System.out.println("Car turbo is "+car[i].displayturbo()+".");
		}
	}
}
