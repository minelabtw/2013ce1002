package ce1002.a3.s102502048;

public class Car 
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int i)//建構子
	{
		id=i;
	}
	public void setspeed(float j)//設置
	{
		maxSpeed=j;
	}
	public void setturbo(boolean k)//設置
	{
		isTurbo=k;
	}
	public int displayid()//顯示
	{
		return id;
	}
	public float displaymaxSpeed()//顯示
	{
		return maxSpeed;
	}
	public boolean displayturbo()//顯示
	{
		return isTurbo;
	}
}
