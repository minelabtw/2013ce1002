﻿package ce1002.a3.s102502044;

import java.util.*;

public class Car
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner scanner;

	Car(int id) //建構子
	{
		scanner = new Scanner(System.in);
		this.id = id;//id初始化
		this.maxSpeed = get_max_speed();
		this.isTurbo = true;
	}

	static List<Car> new_many(int size)//new 一個car陣列，並且放入id
	{
		List<Car> cars = new ArrayList<Car>(size);
		for(int i=0 ; i<size ; i++)
			cars.add(new Car(i));
		return cars;
	}

	int id()//取出id
	{
		return id;
	}
	
	float max_speed()//取出maxSpeed
	{
		return maxSpeed;
	}
	
	boolean is_turbo()//取出isTurbo
	{
		return isTurbo;
	}
	
	void set_max_speed(float value)//設定maxSpeed
	{
		maxSpeed = value;
	}

	void set_is_turbo(boolean value)//設定isTurbo
	{
		isTurbo = value;
	}

	void status()
	{
		System.out.println("Car id is "		   + id()		 + ".");
		System.out.println("Car max speed is " + max_speed() + ".");
		System.out.println("Car turbo is "	   + is_turbo()	 + ".");
	}

	float get_max_speed()
	{
		String input;
		while(true)
		{
			System.out.println("Please input the max speed of this car(" + id() + "): ");
			input = scanner.nextLine();
			if (input.matches("^([1-9]\\d*(\\.\\d+)?|0\\.\\d+)$"))
				break;
			else
				System.out.println("Out of range!");
		}
		return Float.parseFloat(input);
	}
}
