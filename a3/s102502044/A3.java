﻿package ce1002.a3.s102502044;
   
import java.util.*;

public class A3
{
	Scanner scanner;
	List<Car> cars;

	int set_cars_size()//設定cars的數量
	{
		String input;
		while(true)
		{
			System.out.println("Please input the number of cars: ");
			input = scanner.nextLine();
			if (input.matches("^[1-9]\\d*$"))
				break;
			else
				System.out.println("Out of range!");
		}
		return Integer.parseInt(input);
	}

	void show_cars_status()//輸出所有車子
	{
		Iterator<Car> it = cars.iterator();
		while(it.hasNext())
			it.next().status();
	}

	public A3()
	{
		scanner = new Scanner(System.in);//init scanner
		cars = Car.new_many(set_cars_size());//init cars
		System.out.println("Output car status.");
		show_cars_status(); // show all status of cars
	}

	public static void main(String[] argv)//宣告，輸出
	{
		new A3();
	}
}

