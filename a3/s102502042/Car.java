package ce1002.a3.s102502042;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int i)	//constructor
	{
		id = i;
	}
	
	public void setMaxSpeed(float speed)
	{
		maxSpeed = speed;
	}
	
	public void  setTurbo(boolean i)
	{
		isTurbo = i;
	}
	
	public int getId()
	{
		return id;
	}
	
	public float getMaxSpeed()
	{
		return maxSpeed;
	}
	
	public boolean getTurbo()
	{
		return isTurbo;
	}
}
