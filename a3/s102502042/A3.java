package ce1002.a3.s102502042;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		
		Scanner input =  new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int  num;
		while((num = input.nextInt()) <= 0)	//輸入和判斷不符合的狀況 
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
		}
		Car[] c = new Car[num];
		for(int i = 0; i < num; ++i)	//處理陣列裡每個元素
		{
			Car temp = new Car(i);
			float speed;
			System.out.println("Please input the max speed of this car(" + i +"): ");
			while((speed = input.nextFloat()) <= 0)	//輸入和判斷不符
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i +"): ");
			}
			temp.setMaxSpeed(speed);
			temp.setTurbo(true);
			c[i] = temp;
		}
		System.out.println("Output car status.");
		for(int i = 0; i < num; ++i)	//逐一輸出
		{
			System.out.println("Car id is " + c[i].getId() + ".");
			System.out.println("Car max speed is " + c[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + c[i].getTurbo() + ".");
		}
		input.close();	//關掉~~
	}

}
