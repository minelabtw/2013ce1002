package ce1002.a3.s102502525;

public class Car {
	private int id=0;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo=true;// 有無渦輪
	public void setID(int ID){
		id=ID;
	}
	public void setMaxSpeed(float maxspeed){
		maxSpeed=maxspeed;
	}
	public float returnMaxSpeed(){
		return maxSpeed;
	}
	public boolean returnTurbo(){
		return isTurbo;
	}
}
