package ce1002.a3.s102502525;
import ce1002.a3.s102502525.Car;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number=0,ID=0;
		float maxspeed=0;
		do
		{
			System.out.println("Please input the number of cars: ");
			number=input.nextInt();
			if(number<0 || number==0)
			System.out.println("Out of range!");
		}while(number<0 || number==0);//設定一個迴圈讓車子數量大於零且為整數
		float array[]=new float[number];
		do
		{
			System.out.println("Please input the max speed of this car("+ID+ ")"+": ");
			maxspeed=input.nextFloat();
			if(maxspeed<0 || maxspeed==0)
			System.out.println("Out of range!");
			Car car=new Car();
			car.setID(ID);//存入Car屬性
			car.setMaxSpeed(maxspeed);//存入Car屬性
			array[ID]=car.returnMaxSpeed();//將Car屬性傳回，存入陣列
			ID++;
		}while(maxspeed<0 || maxspeed==0 || ID!=number);//設定每台車的最高速度
		System.out.println("Output car status.");
		for(int i=0;i<number;i++)
		{
			Car car=new Car();
			System.out.println("Car id is "+i);
			System.out.println("Car max speed is "+array[i]);
			System.out.println("Car turbo is "+car.returnTurbo());
		}//印出每輛車的屬性
	}

}
