package ce1002.a3.s102502536;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int num;
          //輸出字串
		System.out.println("Please input the number of cars:");
		  //設定範圍
		do
		{
			num = input.nextInt();
			
			if (num < 1)
				System.out.println("Out of range!\nPlease input the number of cars:");
				
		} while (num < 1);     
		  //宣告物件陣列與浮點數陣列
		Car car[] = new Car[num];
		float maxspeed[] = new float[num];
		  //輸入maxspeed和實體化物件 
		for (int i = 0; i < num; i++)
		{	        
	        System.out.println("Please input the max speed of this car(" + i + "):");
	        
	        do
	        {
	        	maxspeed[i] = input.nextFloat();
	        	
	        	if (maxspeed[i] <= 0)
	        		System.out.println("Out of range!\nPlease input the max speed of this car(" + i + "):");
	        	
	        } while (maxspeed[i] <= 0);
	        
	        car[i] = new Car(i , maxspeed[i]);
		} 
		
		System.out.println("Output car status.");
		  //輸出car status
		for (int i = 0; i < num; i++)
		{
			car[i].Status();
		}
		
		input.close();
	}
 
}
