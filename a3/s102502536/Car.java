package ce1002.a3.s102502536;

public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	  //constructor
	public Car(int a , float b){
		
	    id = a;
	    maxSpeed = b;
	    isTurbo = true;
	}
	  //car status
	public void Status(){
		
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.println("Car turbo is " + isTurbo + ".");	
	}
	
}
