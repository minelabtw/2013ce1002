package ce1002.a3.s102502040;
import java.util.Scanner;
public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪;
	Scanner Input=new Scanner(System.in);
	public Car(int name )//設定屬性
	{
		id = name;
		isTurbo=true;
	}
	public void maxSpeed(float i)
	{
		do
		{
			System.out.println("Please input the max speed of this car("+ i +"):");
			maxSpeed = Input.nextInt();
			if(maxSpeed<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+ i +"):");
				maxSpeed = Input.nextFloat();
			}
		}while(maxSpeed<=0);
	}
	public int setid()//回傳值
	{
		return id;
	}
	public float setSpeed()
	{
		return maxSpeed;
	}
	public boolean returnisturbo()
	{
		return isTurbo;
	}
}
