package ce1002.a3.s102502040;
import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int id;
		float maxSpeed[]=new float[100];
		boolean isTurbo;
		Scanner Input = new Scanner(System.in);//執行前段輸入
		do{
			System.out.println("Please input the number of cars:");
			id = Input.nextInt();
			if(id<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the number of cars:");
				id = Input.nextInt();
			}
		}while(id <=0);
		for(int i=0 ; i<id ; i++)
		{
			do
			{
				System.out.println("Please input the max speed of this car("+ i +"):");
				maxSpeed[i] = Input.nextFloat();
				if(maxSpeed[i]<=0)
				{
					System.out.println("Out of range!");
					System.out.println("Please input the max speed of this car("+ i +"):");
					maxSpeed[i] = Input.nextInt();
				}
			}while(maxSpeed[i]<=0);
		}
		System.out.println("Output car status.");//顯示狀態;
		for(int j=0 ; j < id ; j++){
			System.out.println("Car id is " + j);
			System.out.println("Car max speed is " + maxSpeed[j] );
			System.out.println("Car turbo is true");
		}
	}

}
