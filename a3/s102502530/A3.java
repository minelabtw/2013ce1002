package ce1002.a3.s102502530 ;
import java.util.Scanner ;
import ce1002.a3.s102502530.Car ;

public class A3
{
   public static void main(String[] args)
   {
      int m ;
      float n ;
      Scanner scanner = new Scanner(System.in) ;

      do
      {
         System.out.println("Please input the number of cars: ") ;
         m = scanner.nextInt() ;
         if(m <= 0)
            System.out.println("Out of range!") ;
      } while(m <= 0) ;

      Car[] car = new Car[m] ;

      for(int i = 0 ; i != m ; i++)
      {
         do
         {
            System.out.println("Please input the max speed of this car(" + i + "):") ;
            n = scanner.nextFloat() ;
            if(n <= 0)
               System.out.println("Out of range!") ;
         } while(n <= 0) ;
         car[i] = new Car(i) ;
         car[i].setMaxSpeed(n) ;
         car[i].setIsTurbo(true) ;
      }

      scanner.close() ;
      
      System.out.println("Output car status.") ;
      for(int i = 0 ; i != m ; i++)
      {
         System.out.println("Car id is " + car[i].getId() + ".") ;
         System.out.println("Car max speed is " + car[i].getMaxSpeed() + ".") ;
         System.out.println("Car turbo is " + car[i].getIsTurbo() + ".") ;
      }
   }
}
