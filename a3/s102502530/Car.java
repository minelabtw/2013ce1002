package ce1002.a3.s102502530 ;

public class Car
{
   private int id ;
   private float maxSpeed ;
   private boolean isTurbo ;

   public Car(int x)
   {
      id = x ;
      maxSpeed = 0 ;
      isTurbo = true ;
   }

   public void setMaxSpeed(float x)
   {
      maxSpeed = x ;
   }

   public void setIsTurbo(boolean x)
   {
      isTurbo = x ;
   }

   public int getId()
   {
      return id ;
   }

   public float getMaxSpeed()
   {
      return maxSpeed ;
   }

   public boolean getIsTurbo()
   {
      return isTurbo ;
   }
}
