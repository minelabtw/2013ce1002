package ce1002.a3.s102502544;
import java.util.Scanner;
public class Car {
	
	Scanner input = new Scanner(System.in);
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int idd){
		id = idd;
	}
	void getmaxspeed(){
		System.out.println("Please input the max speed of this car(" + id + "): ");
		maxSpeed=input.nextFloat();
		while(maxSpeed<=0){//設定範圍
			System.out.println("Out of range!");
			System.out.println("Please input the max speed of this car(" + id + "): ");
			maxSpeed=input.nextFloat();
		}
	}
	void getisturbo(boolean turbo){
		isTurbo=turbo;
	}
	void Output(){//輸出陣列
		System.out.println("Car id is " + id);
		System.out.println("Car max speed is " + maxSpeed);
		System.out.println("Car turbo is " + isTurbo);
	}

}
