package ce1002.a3.s102502544;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		int number=0;//變數
		float speed=0;
		Car arr[];//陣列
		
		System.out.println("Please input the number of cars: ");//輸出
		number=input.nextInt();//輸入
		while(number<=0){//設定範圍
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number=input.nextInt();
		}
		arr=new Car[number];//陣列大小
		
		for(int i=0; i<number ; i++){//記錄於陣列中
			Car car = new Car(i);
			arr[i]=car;
			arr[i].getmaxspeed();
			arr[i].getisturbo(true);
		}
		
		System.out.println("Output car status.");
		for(int j=0 ; j<number ; j++){//輸出陣列
			arr[j].Output();
		}
	}
}
