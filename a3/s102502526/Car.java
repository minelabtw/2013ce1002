package ce1002.a3.s102502526;

import java.util.Scanner;

public class Car {

		// TODO Auto-generated method stub
		private int id;// 賽車id
		private float maxSpeed;// 最高速
		private boolean isTurbo;// 有無渦輪

		public Car(int ID) {
			id = ID;   //使其可被公開取值
			isTurbo = true;
		}
		public void setMaxSpeed(float speed) { 
			Scanner a = new Scanner(System.in);
			System.out.print("Please input the max speed of this car(" + id + ")\n");
			speed = a.nextInt();
			while (speed <= 0) {
				System.out.print("Out of range!\n");
				System.out.print("Please input the max speed of this car(" + id + ")");
				speed = a.nextInt(); 
			}
			maxSpeed = speed;
		}
		public void condition() { 
			System.out.print("Car id is " + id + ".\n");
			System.out.print("Car max speed is " + maxSpeed + ".\n");
			System.out.printf("Car turbo is %b.\n", isTurbo);
		}
	}
