package ce1002.a3.s102502009;

public class Car {
	private int id = 0;// 賽車id
	private float maxSpeed = 1;// 最高速
	private boolean isTurbo = true;// 有無渦輪

	Car(int setid) {
		id = setid;
	}

	public void setmaxSpeed(float setmaxSpeed) {
		maxSpeed = setmaxSpeed;
	}

	public int getid() {
		return id;
	}

	public boolean getTurbo() {
		return isTurbo;
	}

	public float getmaxSpeed() {
		return maxSpeed;
	}
}
