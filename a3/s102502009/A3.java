package ce1002.a3.s102502009;

import java.util.Scanner;
import ce1002.a3.s102502009.Car;

public class A3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int total = 0;
		do // input total
		{
			System.out.println("Please input the number of cars: ");
			total = input.nextInt();
			if (total <= 0)
				System.out.println("Out of range!");
		} while (total <= 0);

		int[] arr = new int[total]; // set car
		Car Car[] = new Car[total];
		for (int i = 0; i < total; i++) {
			Car[i] = new Car(i);
		}

		float maxSpeed = 0;
		for (int i = 0; i < total; i++) // input maxSpeed
		{
			System.out.println("Please input the max speed of this car(" + i
					+ "):");
			maxSpeed = input.nextFloat();
			if (maxSpeed <= 0) {
				System.out.println("Out of range!");
				i--;
				continue;
			}
			Car[i].setmaxSpeed(maxSpeed);
		}

		System.out.println("Output car status."); // output car status
		for (int i = 0; i < total; i++) {
			System.out.println("Car id is " + Car[i].getid() + ".");
			System.out
					.println("Car max speen is " + Car[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is " + Car[i].getTurbo() + ".");
		}
		input.close();
	}
}
