package ce1002.a3.s101201003;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		int number=0;
		do{
			System.out.println("Please input the number of cars: ");
			number=input.nextInt();
			if(number<1)
				System.out.println("Out of range!");
		}while(number<1);//問賽車數量,如果輸入範圍錯誤就重新輸入

		int[] id=new int[number];//存id的陣列
		float[] maxspeed=new float[number];//存最高速的陣列
		boolean[] isturbo=new boolean[number];//存有無渦輪的陣列

		Car _car=new Car();
		for(int i=0;i<number;i++){
			id[i]=_car.getid(i);
			maxspeed[i]=_car.maxspeed(i);
			isturbo[i]=_car.turbo();			
		}
			
		System.out.println("Output car status.");//顯示Output car status
	
		for(int i=0;i<number;i++){//輸出各個車輛
		System.out.println("Car id is "+id[i]+".\nCar max speed is "+maxspeed[i]+".\nCar turbo is "+isturbo[i]+".");
	    }
		input.close();
	
	}
}

