package ce1002.a3.s101201003;
import java.util.Scanner;
public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo=true;// 有無渦輪
	private Scanner input=new Scanner(System.in);

	
	public int getid(int _id){
		id=_id;
		return id;
	}//設定車輛的id
	public float maxspeed(int i){
		do{
			System.out.println("Please input the max speed of this car("+i+"): ");
			maxSpeed=input.nextFloat();
			if(maxSpeed<=0)
				System.out.println("Out of range!");
		}while(maxSpeed<=0);//問最高速度,如果輸入範圍錯誤就重新輸入
		return maxSpeed;//回傳最高速
	}
	public boolean turbo(){
		return isTurbo;
	}//將每輛車皆設定為有渦輪
}
