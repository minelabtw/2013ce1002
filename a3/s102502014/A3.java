package ce1002.a3.s102502014;

import java.util.Scanner;
import ce1002.a3.s102502014.Car;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a; // 車子數目
		float s; // 車子速度
		Scanner scanner = new Scanner(System.in); // new一個scanner物件
		System.out.println("Please input the number of cars: ");
		a = scanner.nextInt(); // scanner物件使用nextInt()方法
		while (a <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			a = scanner.nextInt();
		}
		Car Car[] = new Car[a];
		for (int i = 0; i < a; i++) {
			Car[i] = new Car(i);
		}
		for (int i = 0; i < a; i++) {
			System.out.println("Please input the max speed of this car(" + i
					+ "): ");
			s = scanner.nextFloat();
			while (s <= 0) {
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("
						+ i + "): ");
				s = scanner.nextFloat();
			}
			Car[i].setmaxSpeed(s);
		}
		System.out.println("Output car status.");
		for (int i = 0; i < a; i++) {
			System.out.println("Car id is " + Car[i].getid() + ".");
			System.out.println("Car max speen is " + Car[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is " + Car[i].getTurbo() + ".");
		}
		scanner.close();
	}
}
