package ce1002.a3.s102502014;

import java.util.Scanner;

public class Car {
	private int id = 0;// 賽車id
	private float maxSpeed = 1;// 最高速
	private boolean isTurbo = true;// 有無渦輪

	Car(int newid) {
		id = newid;
	}

	public void setmaxSpeed(float setMaxspeed) {
		maxSpeed = setMaxspeed;
	}

	public int getid() {
		return id;
	}

	public boolean getTurbo() {
		return isTurbo;
	}

	public float getmaxSpeed() {
		return maxSpeed;
	}
}
