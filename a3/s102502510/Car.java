package ce1002.a3.s102502510;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int n) //建構子
	{
		this.id = n;
	}

	public void setid(int n) {
		this.id = n;
	}

	public void setmaxSpeed(float n) {
		this.maxSpeed = n;
	}

	public void setisTurbo(boolean x) {
		this.isTurbo = x;
	}

	public int getid() {
		return this.id;
	}

	public float getmaxSpeed() {
		return this.maxSpeed;
	}

	public boolean getisTurbo() {
		return this.isTurbo;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
