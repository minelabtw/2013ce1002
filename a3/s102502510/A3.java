package ce1002.a3.s102502510;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);//create a Scanner
		int n = 0;

		while (n <= 0) {
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
			if (n <= 0) {
				System.out.println("Out of range!");
			}
		}
		Car[] c;//create a Car array
		c = new Car[n];
		for (int i = 0; i < n; ++i) {
			c[i] = new Car(i);
			c[i].setisTurbo(true);
			float maxspeed = 0;
			while (maxspeed <= 0) {
				System.out.println("Please input the max speed of this car("
						+ i + ")");
				maxspeed = input.nextFloat();

				if (maxspeed <= 0) {
					System.out.println("Out of range!");
				}
			}
			c[i].setmaxSpeed(maxspeed);
		}
		System.out.println("Output car status.");
		for (int i = 0; i < n; ++i) {
			System.out.println("Car id is " + c[i].getid());
			System.out.println("Car max speed is " + c[i].getmaxSpeed());
			System.out.println("Car turbo is " + c[i].getisTurbo());
		}
	}
}
