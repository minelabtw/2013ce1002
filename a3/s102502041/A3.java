package ce1002.a3.s102502041;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int amount;
		System.out.println("Please input the number of cars: ");
		do{
			amount=cin.nextInt();
			if(amount<=0)System.out.println("Out of range!\nPlease input the number of cars: ");
		}while(amount<=0);
		Car[] car = new Car[amount];
		for(int i=0;i<amount;i++)
		{
			float maxSpeed;
			System.out.println("Please input the max speed of this car("+i+"): ");
			do{
				maxSpeed=cin.nextFloat();
				if(maxSpeed<0)System.out.println("Out of range!\nPlease input the max speed of this car("+i+"): ");
			}while(maxSpeed<0);
			car[i].inputMaxSpeed(maxSpeed);
		}
		System.out.println("Output car status.");
		for(int i=0;i<amount;i++)
		{
			System.out.println("Car id is "+i+".");
			System.out.print("Car max speed is ");
			car[i].maxSpeed();
			System.out.println(".");
			System.out.print("Car turbo is ");
			car[i].isTurbo();
			System.out.println(".");
		}

	}

}
