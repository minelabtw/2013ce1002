package ce1002.e3.s102502554;

import java.util.Scanner;

public class E3 {
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);
		System.out.println("Create rectangle in 2 ways.");
		System.out.println("1)create by constructor parameters,2)create by setter: ");
		int ways = input.nextInt();
		while (ways < 1 || ways > 2){
			System.out.println("Out of range!");
			System.out.println("Create rectangle in 2 ways.");
			System.out.println("1)create by constructor parameters,2)create by setter: ");
			ways = input.nextInt();
		}//選擇方式
		if (ways == 1){
			System.out.println("Input the width: ");
			int width = input.nextInt();
			while (width < 1 || width > 20){
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = input.nextInt();
			}
			
			System.out.println("Input the height: ");
			int height = input.nextInt();
			while (height < 1 || height > 20){
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = input.nextInt();
			}
			
			Rectangle Rectangle = new Rectangle(width,height);
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+Rectangle.getArea());
		}//使用建構子+參數初始化物件
		
	   if (ways == 2){
		   System.out.println("Input the width: ");
			int width = input.nextInt();
			while (width < 1 || width > 20){
				System.out.println("Out of range!");
				System.out.println("Input the width: ");
				width = input.nextInt();
			}
			
			System.out.println("Input the height: ");
			int height = input.nextInt();
			while (height < 1 || height > 20){
				System.out.println("Out of range!");
				System.out.println("Input the height: ");
				height = input.nextInt();
			}
			
			Rectangle rectangle = new Rectangle();
			rectangle.setwidth(width);// set the width
			rectangle.setheight(height);// set the height
			
			System.out.println("The width of this rectangle is "+width);
			System.out.println("The height of this rectangle is "+height);
			System.out.println("The area of this rectangle is "+rectangle.getArea());
		}//使用setter設定物件
	}

}
