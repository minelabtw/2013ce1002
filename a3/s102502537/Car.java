﻿package ce1002.car.s102502537;

import java.util.Scanner;

public class Car {
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	Scanner input = new Scanner(System.in);
	
	//設定id
	Car(int idcar){
		id = idcar;
	}
	
	//設定maxspeed
	void getmaxSpeed(){
		maxSpeed = 0;
		
		//maxspeed範圍<=0
		while(maxSpeed <= 0){
			System.out.println("Please input the max speed of this car(" + id + "):");
			maxSpeed = input.nextFloat();
			if(maxSpeed <= 0){
				System.out.println("Out of range!");
			}
		}
		
	}
	
	//設定turbo
	void getTurbo(boolean t){
		isTurbo = t;
	}
	
	//印出id,maxspeed,turbo
	void outputData(){	
		System.out.println("Car id is " + id + ".");  //輸出編號
		System.out.println("Car max speed is " + maxSpeed + ".");  //輸出最大速度
		
		//輸出有無渦輪
		System.out.print("Car turbo is ");
		if(isTurbo == true){
			System.out.println("true.");
		}else {
			System.out.println("false.");
		}
	}

}
