﻿package ce1002.car.s102502537;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int num = 0;
		Car[] cars;
		Scanner input = new Scanner(System.in);
		
		//設定車數
		while(num <= 0){
			System.out.println("Please input the number of cars:");
			num = input.nextInt();
			if(num <= 0){  
				System.out.println("Out of range!");
			}
		}
		
		//設定陣列大小
		cars = new Car[num];
		
		//設定id,maxspeed,turbo
		for(int i = 0 ; i < num ; i++){
			cars[i] = new Car(i);  //初始化
			cars[i].getmaxSpeed();  //取得最大速度
			cars[i].getTurbo(true);  //設定渦輪為真
		}
		
		System.out.println("Output car status.");
		
		//印出car的資料
		for(int j = 0 ; j < num ; j++){
			cars[j].outputData();
		}
		input.close();
	}

}
