package ce1002.a3.s102502558;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int n;
		float maxSpeed;
		while(true)
		{
			System.out.println("Please input the number of cars: ");
			n = scanner.nextInt();
			if (n > 0)
				break;
			System.out.println("Out of range!");
		}
		
		Car[] c = new Car[n];
		for (int i=0;i<n;i++)
		{
			c[i] = new Car(i);
			while(true)
			{
				System.out.printf("Please input the max speed of this car(%d): \n", i);
				maxSpeed = scanner.nextFloat();
				if (maxSpeed > 0)
					break;
				System.out.println("Out of range!");
			}
			c[i].setMaxSpeed(maxSpeed);
		}
		System.out.println("Output car status.");
		for (int i=0;i<n;i++)
		{
			c[i].print_status();
		}
		scanner.close();
	}

}
