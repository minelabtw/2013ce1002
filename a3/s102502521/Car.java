package ce1002.a3.s102502521;

public class Car {

	private int id;// 賽車id
	private int num;
	private double maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	private double [] arr;
	
	public Car(){
		
	}
	
	//車數
	public void setnum(int num){
		this.num=num;
		arr=new double [num];
	}
	
	//車號
	public void id(int id){
		this.id=id;
	}
	
	//車速
	public void setmaxSpeed(double maxSpeed){
		this.maxSpeed=maxSpeed;
	}
	
	//Turbo
	public void Turbo(){
		isTurbo=true;
	}
	
	//車性能
	public void info(){
		arr[id]=maxSpeed;
	}
	
	//列印
	public void Output(){		
		for(id=0;id<num;id++){
			System.out.println("Car id is "+id+".");
			System.out.println("Car max speed is "+arr[id]+".");
			System.out.println("Car turbo is "+isTurbo+".");	
		}
	}
}
