package ce1002.a3.s102502521;
import java.util.*;

public class A3 {
	private static Scanner sc;
	private static Car car;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		sc=new Scanner(System.in);
		car=new Car();
		int num=0;
		double maxSpeed=0;
		
		//檢查
		do{
			System.out.println("Please input the number of cars: ");
			num=sc.nextInt();
			if(num<=0){
				System.out.println("Out of range!");
			}
		}while(num<=0);
		
		//設定車數與是否有Turbo
		car.setnum(num);
		car.Turbo();
		
		//輸入車速 並將ID與車結合
		for(int id=0;id<num;id++){
			do{
				System.out.println("Please input the max speed of this car("+id+"): ");
				maxSpeed=sc.nextDouble();
				if(maxSpeed<=0){
					System.out.println("Out of range!");
				}
			}while(maxSpeed<=0);
			
			car.id(id);
			car.setmaxSpeed(maxSpeed);
			car.info();
		}
		//印出車子資訊
		System.out.println("Output car status.");
		car.Output();
	}
}
