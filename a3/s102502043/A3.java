package ce1002.a3.s102502043;
import java.util.Scanner;
public class A3 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int num = input.nextInt();
		while(num<=0)      												//限制車輛數目
 		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car x[] = new Car[num];											//宣告一個由N個物件組成的陣列 的類別
		
		for(int i=0;i<num;i++)											//每台車的output
		{
			float s;
			System.out.println("Please input the max speed of this car("+i+"):");
			Car temp = new Car(i); 										//宣告一個站存的物件 來暫時儲存
			s = input.nextFloat();
			while(s<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"):");
				s = input.nextFloat();
			}
			temp.MaxSpeed(s);
			temp.IsTurbo(true);
			x[i] = temp;												//將物件組成陣列的物件
		}
		System.out.println("Output car status.");
		for(int i=0;i<num;i++)
		{
			System.out.println("Car id is "+x[i].Returnid()+".");
			System.out.println("Car max speen is "+x[i].ReturnMaxSpeed()+".");
			System.out.println("Car turbo is "+x[i].ReturnIsTurbo()+".");
		}
		
		input.close();
	}

}
