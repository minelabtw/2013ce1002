package ce1002.a3.s102502561;

import java.util.Scanner;

public class Car {
	private int id;// car id(0~num-1)
	private float maxSpeed;// max speed
	private boolean isTurbo;// turbo ���L

	public Car(int ID) {
		id = ID; // id
		isTurbo = true;
	}

	public void status() { // ��X
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.printf("Car turbo is %b.\n", isTurbo);
	}

	public void setMaxSpeed(float speed) { // �P�_
		Scanner input = new Scanner(System.in);
		System.out
				.println("Please input the max speed of this car(" + id + ")");
		speed = input.nextInt();
		while (speed <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the max speed of this car(" + id
					+ ")");
			speed = input.nextInt();
		}
		maxSpeed = speed;
		input.close();
	}

}