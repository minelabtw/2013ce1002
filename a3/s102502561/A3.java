package ce1002.a3.s102502561;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int num = input.nextInt();//讀入
		while (num <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		float speed = 0;
		Car car[] = new Car[num]; // 每台車
		for (int i = 0; i < num; i++) {
			car[i] = new Car(i); // car class 放進每台car中
			car[i].setMaxSpeed(speed); // run function
		}
		for (int i = 0; i < num; i++) { // Output status
			car[i].status();
		}
		input.close();
	}

}
