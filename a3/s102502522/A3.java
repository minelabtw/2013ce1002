package ce1002.a3.s102502522;
import java.util.Scanner;

import ce1002.a3.s102502522.car;
public class A3 {
	public static void main(String[] args) {
	
	int carnum;
	float carspeed;
	Scanner scanner = new Scanner(System.in);

System.out.println("Please input the number of cars: ");
carnum= scanner.nextInt();
while(carnum<=0)
{
	System.out.println("Out of range!");
	System.out.println("Please input the number of cars: ");
	carnum= scanner.nextInt();
}
 car[] cars=new car[carnum];//建立矩陣

for(int count=0;count<=(carnum-1);count++)
{
	System.out.println("Please input the max speed of this car("+count+"):");
	carspeed=scanner.nextFloat();
	while(carspeed<=0)
	{
		System.out.println("Out of range!");
		System.out.println("Please input the max speed of this car("+count+"):");
		carspeed=scanner.nextFloat();
	}
	cars[count]=new car(count);//將id傳入建構值
	cars[count].getmaxspeed(carspeed);//傳入速度
	cars[count].getturbo(true);//傳入布林值
	
}
System.out.println("Output car status.");
for(int count=0;count<=(carnum-1);count++)//輸出
{
	System.out.println("Car id is "+count);
	System.out.println("Car max speed is "+cars[count].returnmaxspeed());
	System.out.println("Car turbo is "+cars[count].returnturbo());
}
	}
}
