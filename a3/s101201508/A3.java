package ce1002.a3.s101201508;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n=1;//決定有幾台car
		Scanner sc =new Scanner(System.in); 
		System.out.println("Please input the number of cars: ");
		n=sc.nextInt();
		while (n<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=sc.nextInt();
		}
		car cs[]=new car[n];//建n個空間的陣列以存car的形式，但是還沒有放car進去
		for (int i=0;i<n;i++)
		{
			car c=new car(i);//放car進cs的陣列裡
			cs[i]=c;         //也可以寫成 car cs[i]=new car(i);
		}
		for (int i=0;i<n;i++)//存每台car的maxSpeed，再存到物件car裡
		{
			float speed;
			System.out.println("Please input the max speed of this car("+i+"): ");
			speed=sc.nextFloat();
			while (speed<=0)
			{
				System.out.println("Out of ranage!");
				System.out.println("Please input the max speed of this car("+i+"): ");
				speed=sc.nextFloat();
			}
			cs[i].set_speed(speed);
		}
		for (int i=0;i<n;i++)
		{
			cs[i].set_isTurbo_true();
		}
		System.out.println("Output car status.");
		for(int i=0;i<n;i++)//輸出物件car裡的內容
		{
			System.out.println("Car id is "+cs[i].get_id()+".");
			System.out.println("Car max speed is "+cs[i].get_speed()+".");
			System.out.println("Car turbo is "+cs[i].get_isTurbo()+".");
		}
	}

}
