package ce1002.a3.s101201023;

public class Car 
{
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	
	Car()
	{
		
	}
	
	void CarId(int I)
	{
		id=I;
	}
	
	int CarId()
	{
		return id;
	}
	
	void CarMax(float MS)
	{
		maxSpeed=MS;
	}
	
	float CarMax()
	{
		return maxSpeed;
	}
	
	void CarTurbo()
	{
		isTurbo=true;
	}
	
	boolean CarTurbo1()
	{
		return isTurbo;
	}
}
