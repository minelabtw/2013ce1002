package ce1002.a3.s101201023;

import java.util.Scanner;

public class A3 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub

		int sum=0;
		float speed=0;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input the number of cars: ");
		sum = input.nextInt();
		while(sum<1)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			sum = input.nextInt();
		}
		Car CS[] = new Car[sum];         //宣告一個Car型態的陣列
		
		for(int i=0 ; i<sum ; i++)
		{
			Car Cars =new Car();
			CS[i]=Cars;                  //將class.Car傳入陣列裡每個值
			CS[i].CarId(i);              //將i傳入CarId裡
			System.out.println("Please input the max speed of this car("+ i +"): ");
			speed = input.nextInt();
			while(speed<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+ i +"): ");
				speed = input.nextInt();
			}
			CS[i].CarMax(speed);
			CS[i].CarTurbo();
		}
		
		//輸出每台車的資訊
		System.out.println("Output car status.");
		for(int i=0 ; i<sum ; i++)
		{
			System.out.println("Car id is " +CS[i].CarId());
			System.out.println("Car max speed is " + CS[i].CarMax());
			System.out.println("Car turbo is " + CS[i].CarTurbo1());
		}
	}

}
