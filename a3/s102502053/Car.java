package ce1002.a3.s102502053;

public class Car {
	
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	
	Car()
	{
		
	}
	
	Car(int idnumber, float speed, boolean turbo)
	{
		id = idnumber;
		maxSpeed = speed;
		isTurbo = turbo;
	}
	
	int getNumber()
	{
		return id;
	}
	
	float getSpeed()
	{
		return maxSpeed;
	}
	
	boolean getTurbo()
	{
		return isTurbo;
	}
	
	void setSpeed(float speed)
	{
		maxSpeed = speed;
	}
	
	void setTurbo (boolean turbo)
	{
		isTurbo = turbo;
	}
	

}
