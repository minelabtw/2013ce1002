package ce1002.a3.s102502053;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int number;
		float speed;
		
		do
		{
			System.out.print("Please input the number of cars: \n");
			number = input.nextInt();
			if(number<1)
			{
				System.out.print("Out of range!\n");
			}
		}while(number<1);
		

		Car[] arr = new Car[number];
		
		for(int i = 0 ; i<number; i++)
		{
			do
			{
				System.out.print("Please input the max speed of this car(" + i + "): \n");
				speed = input.nextFloat();
				if(speed <= 0)
				{
				System.out.print("Out of range!\n");
				}
			}while(speed <= 0);
			
			Car car = new Car(i, speed, true);

			arr[i] = car;
		}
		
		System.out.print("Output car status.\n");
		for(int i = 0; i<number; i++)
		{
			System.out.print("Car id is " + arr[i].getNumber() + ".\n");
			System.out.print("Car max speed is " + arr[i].getSpeed() + ".\n");
			System.out.print("Car turbo is " + arr[i].getTurbo() + ".\n");
		}
		
	}

}
