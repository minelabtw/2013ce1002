package ce1002.a3.s102502562;

import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner input=new Scanner(System.in);
	
	public Car(int number)//建構子初始化id
	{
		id=number;
	}
	public void setmaxSpeed(int i)//讓使用者輸入maxSpeed並判斷是否再範圍
	{
		do 
		{
			System.out.println("Please input the max speed of this car(" + i + "):");
			maxSpeed=input.nextFloat();
			if(maxSpeed<=0)
			{
				System.out.println("Out of range!");
			}
		}while(maxSpeed<=0);
	}
	public void setisTurbo()//設定isTurbo
	{
		isTurbo=true;
	}
	public int getid()//取得id
	{
		return id;
	}
	public float getmaxSpeed()//取得maxSpeed
	{
		return maxSpeed;
	}
	public boolean getisturbo()//取得isTurbo
	{
		return isTurbo;
	}
}
