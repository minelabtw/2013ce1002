package ce1002.a3.s102502562;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number;
		Car[] cars;
		do//詢問使用者要幾台車並判斷是否符和範圍
		{
			System.out.println("Please input the number of cars:");
			number=input.nextInt();
			if(number<=0)
			{
				System.out.println("Out of range!");
			}
		}while(number<=0);
		cars=new Car[number];
		for(int i=0;i<number;i++)//讓使用者輸入車的屬性並存入陣列裡
		{
			Car car=new Car(i);
			car.setmaxSpeed(i);
			car.setisTurbo();
			cars[i]=car;
		}
		System.out.println("Output car status.");
		for(int i=0;i<number;i++)//輸出車的屬性
		{
			System.out.println("Car id is " + cars[i].getid() + ".");
			System.out.println("Car max speed is " + cars[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].getisturbo() + ".");
		}
	}
}
