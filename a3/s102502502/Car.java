package ce1002.a3.s102502502;

public class Car {         
	private int id;                   // the id of car;
	private float maxspeed;           // the maxspeed of car;
	private boolean isTurbo;          // the isTurbo of car;
	
	public Car(int i)                 // the constructor of Car;
	{
		id =i;
	}
	public int getId()                // get the id;
	{
		return id;
	}
	public void setMaxspeed(float s)  // set max speed;
	{
		maxspeed = s;
	}
	public float getMaxspeed()        // get max speed;
	{
		return maxspeed;
	}
	public void setTurbo(boolean to)  // set  isTurbo;
	{
		 isTurbo = to;
	}
	public boolean getTurbo()         // get  isTurbo;
	{
		return isTurbo;
	}
}
