package ce1002.a3.s102502502;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
	
		System.out.println("Please input the number of cars: ");       //請求輸入數字
		int t = input.nextInt();                                         
		while (t < 1)                                                 //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Please input the number of cars: ");
			t = input.nextInt();
		}
		Car [] cm = new Car[t];                                      //呼叫class  Car
		for (int n = 0; n < t; n++)
		{
		System.out.println("Please input the max speed of this car("+ n +"): ");
		float v = input.nextFloat();
		while (v < 1)                                                 //當數字超出範圍，則顯示超出範圍並要求再次輸入
		{
			System.out.println("Out of range! ");
			System.out.println("Please input the max speed of this car(" + n + "): ");
			v = input.nextFloat();
		}
		cm[n] = new Car(n);                                            //從建構子建立陣列物件，並給予初始值
		cm[n].setMaxspeed(v);
		cm[n].setTurbo(true);		
		}
		System.out.println("Out put car status. ");
		for (int n1 = 0; n1 < t; n1++)                                //輸出所有數值
		{
			System.out.println("Car id is " + cm[n1].getId());
			System.out.println("Car max speed is " + cm[n1].getMaxspeed());
		    System.out.println("Car turbo is " + cm[n1].getTurbo());
		}
		
	}

}
