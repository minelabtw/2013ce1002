package ce1002.a3.s102502532;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Please input the number of cars:");
		int carnumber = input.nextInt();
		while (carnumber <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			carnumber = input.nextInt();
		}
		/* Car carstatus = new Car(carnumber); */
		Car[] cars = new Car[carnumber]; // 物件陣列

		for (int i = 0; i < carnumber; i++) {
			cars[i] = new Car(i); // 要初始化

			System.out.print("Please input the max speed of this car(" + i);
			System.out.println("):");

			float Speed = input.nextFloat();
			cars[i].setmaxSpeed(Speed); // cars物件setmaxSpeed

			while (Speed <= 0) { // 判斷
				System.out.println("Out of range!");
				System.out.print("Please input the max speed of this car(" + i);
				System.out.println("):");
				Speed = input.nextFloat();
				cars[i].setmaxSpeed(Speed);
			}
			cars[i].setisTurbo();
		}

		System.out.println("Output car status.");
		for (int i = 0; i < carnumber; i++) {
			System.out.println("Car id is " + i + ".");
			cars[i].getmaxSpeed(); // 印出
			cars[i].getisTurbo();
		}

	}
}
