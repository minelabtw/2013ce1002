package ce1002.a3.s102502532;

public class Car {

	private int id = 0; // 賽車id
	private float maxSpeed = 0; // 最高速
	private boolean isTurbo; // 有無渦輪

	Car() {

	}

	Car(int number) {
		id = number;
	}

	void setmaxSpeed(float speed) { // void 用法和C++相同
		maxSpeed = speed;
	}

	void setisTurbo() {
		isTurbo = true; // 皆為true
	}

	void getmaxSpeed() {
		System.out.println("Car max speed is " + maxSpeed + ".");
	}

	void getisTurbo() {
		System.out.println("Car turbo is " + isTurbo + ".");
	}

}
