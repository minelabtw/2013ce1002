package ce1002.a3.s102502507;
import java.util.Scanner;
public class A3 {
	public static Scanner input ;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Scanner input= new Scanner(System.in);
      int d;// 賽車id
      float maxS;// 最高速
   
      
      do
      {
    	  System.out.println("Please input the number of cars: ");
    	  d=input.nextInt();
    	  if(d<=0)//設限制
    	  {
    		  System.out.println("Out of range!");
    	  }
      }
      while(d<=0);
      Car [] cars= new Car[d];
      
      for(int i=0;i<d;i++)
      {
    	Car car = new Car(i) ;
    	 
    	  do{
    	  System.out.println("Please input the max speed of this car("+i+"): ");
    	  maxS=input.nextFloat();
    	  if(maxS>0)//如果最高速度成立
    	  {
    		
    		 car.Max(maxS);
    		 car.isT(true);//布林只能跑出true或false
    		 break;
    	  }
    	  else if(maxS<=0)
    	  {
    		  System.out.println("Out of range!");
    	  }
    	 }
    	 while(maxS<=0);

         cars[i]=car;//car存入cars[i]
      }
      System.out.println("Output car status.");
      for(int i=0;i<d;i++)//輸出答案
      {
    	  System.out.println("Car id is "+i+".");
    	  System.out.println("Car max speed is "+cars[i].getMax()+".");
    	  System.out.println("Car turbo is "+cars[i].getTurbo()+".");
      }
      input.close() ;
	}

}
