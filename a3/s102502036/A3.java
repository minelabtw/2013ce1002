package ce1002.a3.s102502036;
import java.util.Scanner ;
public class A3 {

	public static void main(String[] args) {
		int number=1 ;
		float maxspeed ;
		
		Scanner input = new Scanner(System.in) ;
		do{
			if(number<=0){
				System.out.println("Out of range !");
			}
			System.out.println("Please input the number of cars: ");
			number = input.nextInt() ;
		}while(number<=0) ;
		
		
		Car[] speedArray = new Car[number] ;//宣告speedarray陣列用來存放object
         for(int i=0 ;i<number ;i++){
        	 Car car = new Car(i) ;//建立car object
        	 System.out.println("Please input the max speed of this car("+i+"):") ;
        	 maxspeed = input.nextFloat() ; 
        	 while( maxspeed<=0){
        		 System.out.println("Out of range !");
        		 System.out.println("Please input the max speed of this car("+i+"):") ;
        		 maxspeed = input.nextFloat() ; 
        	 }
        	 car.setspeed(maxspeed);
        	   speedArray[i]=car ;//把car object丟到陣列裡面
        	  
         }
		
         System.out.println("Output car status.");
         for(int i=0 ;i<number ;i++){
        	
        	 System.out.println("Car id is "+i);
        	 System.out.println("Car max speed is "+speedArray[i].getspeed());
        	 System.out.println("Car turbo is "+speedArray[i].setisturbo());
         }
         
		
	}

}
