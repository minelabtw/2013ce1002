package ce1002.a3.s101201524;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	// 設定賽車的id,max speed, isTurbo
	public Car(int i, float speed){
		isTurbo = true;
		id = i;
		maxSpeed = speed;
	}
	//輸出賽車的id,max speed, isTurbo
	public void status(){
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.println("Car turbo is " + isTurbo + ".");
	}
}
