package ce1002.a3.s101201524;
import java.util.Scanner;
public class A3 {
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		int number = 0;
		float speed = 0;
		Car[] car = new Car[5];
		//set number of cars
		do{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number < 1)
				System.out.println("Out of range!");
		}while(number < 1);
		//set max speed of cars
		for(int i = 0; i < number; i++){
			do{
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed = input.nextFloat();
				if(speed <= 0)
					System.out.println("Out of range!");
			}while(speed <= 0);
			car[i] = new Car(i, speed);
		}
		//output cars id,max speed, isTurbo
		System.out.println("Output car status.");
		for(int i = 0; i < number; i++){
			car[i].status();
		}
		
	}
}
