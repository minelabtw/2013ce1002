package ce1002.a3.s102502545;

import java.util.Scanner;

public class Car {
	Scanner input = new Scanner(System.in);
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int i) {
		id = i;
	}

	public void getcar() {
          System.out.println("Car id is "+ id);
	}

	public void setspeed() {
		do {/* 限定變數的範圍 */
			System.out.println("Please input the max speed of this car(" + id
					+ "):");
			maxSpeed = input.nextFloat();
			if (maxSpeed <= 0)
				System.out.println("Out of range!");
		} while (maxSpeed <= 0);

	}

	public void getspeed() {//輸出speed
		System.out.println("Car max speed is" + maxSpeed);
	}

	public void setTurbo() {//設定布林
		isTurbo = true;
	}

	public void getTurbo() {//輸出布林
		System.out.println("Car turbo is true.");
	}

}
