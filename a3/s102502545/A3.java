package ce1002.a3.s102502545;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int number = 0;
		
		do {/* 限定變數的範圍 */
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number < 1)
				System.out.println("Out of range!");
		} while (number < 1);

		Car C[] = new Car[number];//建立一個陣列存放資料
		
		for (int i = 0; i < number; i++) {//利用迴圈使用car class 寫入資料
			Car apple = new Car(i);//利用迴圈,使每次都能建立新的物件 EX: Car(1) Car(2)
			C[i] = apple;//將物件資料寫進去陣列儲存
			apple.setspeed();//使用Car class裡的函式去輸入speed
		}
		
		for (int i = 0; i < number; i++) {
			Car apple = new Car(i);
			apple.getcar();
			apple.getspeed();
			apple.getTurbo();
		}

	}
}
