package ce1002.car.s102502555;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int number;
		Scanner input = new Scanner(System.in);
		
		//輸入車子數量
		do{
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			
			//數量要大於零
			if(number <= 0){  
				System.out.println("Out of range!");
			}
		}while(number <= 0);
		
		//汽車陣列
		Car[] cars = new Car[number];
		
		//輸入車子的資訊
		for(int i = 0 ; i < number ; i++){
			cars[i] = new Car(i);  //初始化
			cars[i].getmaxSpeed();  //取得最大速度
			cars[i].getTurbo(true);  //設定渦輪為真
		}
		
		System.out.println("Output car status.");
		
		//輸出車子資訊
		for(int i = 0 ; i < number ; i++){
			cars[i].showStatus();
		}
		
		input.close();
	}

}
