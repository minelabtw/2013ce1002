package ce1002.car.s102502555;

import java.util.Scanner;

public class Car {

		private int id;
		private float maxSpeed;
		private boolean isTurbo;
		
		//傳入車子的編號
		Car(int number){
			id = number;
		}
		
		//取得最大速度
		void getmaxSpeed(){
			Scanner input = new Scanner(System.in);
			
			//最大速度範圍不小於等於零
			do{
				System.out.println("Please input the max speed of this car(" + id + "):");
				maxSpeed = input.nextFloat();
				if(maxSpeed <= 0){
					System.out.println("Out of range!");
				}
			}while(maxSpeed <= 0);
			
		}
		
		//取得有無渦輪
		void getTurbo(boolean turbo){
			isTurbo = turbo;
		}
		
		//輸出車子資訊
		void showStatus(){	
			System.out.println("Car id is " + id + ".");  //輸出編號
			System.out.println("Car max speed is " + maxSpeed + ".");  //輸出最大速度
			
			//輸出有無渦輪
			if(isTurbo == true){
				System.out.println("Car turbo is true.");
			}else {
				System.out.println("Car turbo is false.");
			}
		}

	
}

	
