package ce1002.a3.s992001026;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	// 設定初始值
	public Car ( int i , float j , boolean z ){
		id = i ; 
		maxSpeed = j ;
		isTurbo = z ;
	}
	
	// 取得數度
	public float getspeed(){
		return maxSpeed ;
	}
	//取得狀態 是否有渦輪
	public boolean getstatus (){
		return isTurbo ;
	}
	
}
