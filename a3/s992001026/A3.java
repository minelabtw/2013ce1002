package ce1002.a3.s992001026;
import java.util.*;

public class A3 {

	public static void main(String[] args) {
	    int n , s , id  ;
	    float speed ;
	    boolean bo = true;
	    
		Scanner scn = new Scanner(System.in);
		// 輸入共有幾台車 
	    System.out.println("Please input the number of cars:");
	    n = scn.nextInt();
	    while (n < 1 ){
	    	System.out.println("Out of range1");
	    	System.out.println("Please input the number of cars:");
	 	    n = scn.nextInt();
	    }
	    // 令出car陣列
	    Car[] car = new Car[n];
	    
	    
	    // 設定不同car的最大數度和狀態
	    for ( id = 0 ; id < n ; id++){
	    	
	    	System.out.println("Please input the max speed of this car("+id+"):");
	    	speed = scn.nextFloat();
	    	while ( speed < 0 ){
	    		System.out.println("Out of range!");
	    		System.out.println("Please input the max speed of this car("+id+"):");
		    	speed = scn.nextFloat();
	    	}
	    	
	    	car[id] = new Car( id , speed , bo);
	    }
	    
	    
	    System.out.println("Output car status.");
	    
	    // 輸出 個個賽車的數度及狀態
	    for ( id = 0 ; id < n ; id++){
	    	
	    	System.out.println("Car id is " +id +"." );
	    	System.out.println("Car max speed is "+ car[id].getspeed() +"." );
	    	System.out.println("Car turbo is "+car[id].getstatus()+"." );
	    }
	    
	    
	    
	}

}
