package ce1002.a3.s102502556;

public class Car {
	private int id; //賽車id
	private float maxSpeed; //最高速
	private boolean isTurbo = true; //有無渦輪
	Car( int i ) //建構子，同時傳入id所要設定的值
	{
		id = i;
	}
	int getid () //回傳id的值
	{
		return id;
	}
	void setmaxSpeed ( float i ) //設定車子的最大速度(maxSpeed)
	{
		maxSpeed = i; 
	}
	float getmaxSpeed () //回傳maxSpeed的值
	{
		return maxSpeed;
	}
	void setisTurbo ( boolean i ) //設定車子是否有渦輪(isTurbo)
	{
		isTurbo = i;
	}
	boolean getisTurbo () //回傳isTurbo的值
	{
		return isTurbo;
	}
}
