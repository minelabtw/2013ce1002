package ce1002.a3.s102502556;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int num = 0; //宣告一個int變數，儲存要設定多少台車子的值
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		System.out.println("Please input the number of cars: ");
		num = input.nextInt();
		while ( num <= 0 ) //檢驗使用者的輸入是否合標準，否則要求其重新輸入。
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car[] cars = new Car[num]; //宣告名為cars的Car陣列
		for ( int i = 0 ; i < num ; i++ ) //用for迴圈設定陣列裡每一項元素的值
		{
			Car newcar = new Car(i);
			System.out.println("Please input the max speed of this car("+ i +"): ");
			float speed = 0;
			speed = input.nextFloat();
			while ( speed <= 0 ) //檢驗使用者的輸入是否合標準，否則要求其重新輸入。
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+ i +"): ");
				speed = input.nextFloat();
			}
			newcar.setmaxSpeed(speed);
			cars[i] = newcar;
		}
		System.out.println("Output car status.");
		for( int i = 0 ; i < num ; i++ ) //用for迴圈顯示所有車子設定的狀態
		{
			System.out.println("Car id is "+cars[i].getid()+".");
			System.out.println("Car max speed is "+cars[i].getmaxSpeed()+".");
			System.out.println("Car turbo is "+cars[i].getisTurbo()+".");
		}
		input.close(); //關閉Scanner
	}
}

	