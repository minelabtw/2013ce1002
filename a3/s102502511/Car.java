package ce1002.a3.s102502511;

import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int number) //construtor 參數傳入
	{
		id = number;
	}
	
	void GetSpeed() //函式:用來儲存車子的最高速度 
	{
		Scanner input = new Scanner (System.in); //宣告用來輸出的東西
		do
		{	System.out.println("Please input the max speed of this car(" + id + "): ");		
			maxSpeed = input.nextFloat();//存取數字
			if(maxSpeed <= 0)
			{
				System.out.println("Out of range!");
			}
		}while(maxSpeed <= 0);
	}
	
	
	void GetTurbo(boolean boo) //設定有沒有渦輪
	{
		isTurbo = boo;
	}
	
	void ShowStatus()//函式:用來顯示所有東西
	{
		System.out.println("Car id is " + id + "." );
		System.out.println("Car max speen is " + maxSpeed + ".");
		if(isTurbo == true)
		{
			System.out.println("Car turbo is true.");
		}else
		{
			System.out.println("Car turbo is fasle.");
		}
	}
	
}
