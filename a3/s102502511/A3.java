package ce1002.a3.s102502511;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number;
		Scanner input = new Scanner(System.in);
		
		do
		{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number <= 0)
			{
				System.out.println("Out of range!");
			}
		}while(number <= 0); 
		Car cars [] = new Car [number]; //宣告car陣列裝Car裡的東西
		
		for(int i = 0; i < number ; i++)
		{
			cars[i] = new Car(i); //此陣列存三變數的資料 以及做Car裡面的動作
			cars[i].GetSpeed(); //叫出GetSpeed的函式 並且做下列動作，並且存入maxSpeed的資料
			cars[i].GetTurbo(true);//叫出GetTurbo的函式 並且做下列動作，並且存入isTurbo的資料
		}
		
		System.out.println("Output car status.");
		
		for(int i = 0 ; i < number ; i++)
		{
			cars[i].ShowStatus();//叫出ShowStatus的函式 並做出下列動作
		}
		
		input.close();
	}	
	
}
