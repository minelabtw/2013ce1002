package ce1002.a3.s102502542;

public class Car {
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
    public Car (int i) //建構式+參數初始化
    {
    	id = i;
    	isTurbo = true ;
    }
    public void setmaxSpeed(float a)//設定maxSpeed
    {
    	maxSpeed = a;
    }
    public void setisTurbo(boolean i)//設定turbo
    {
        isTurbo = i ;
    }
    public int getid(int i) 
    {
     return i ;
    }
    public float getmaxSpeed()
    {
    	return maxSpeed;
    }
    public boolean getisTurbo()
    {
    	return isTurbo ;
    }
  }
