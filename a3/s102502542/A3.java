package ce1002.a3.s102502542;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int id = input.nextInt();
		while(id<=0)
		{
			System.out.println("Out of range!");
		    System.out.println("Please input the number of cars: ");
		    id = input.nextInt();
		}
		Car[] car = new Car[id];//設定Car物件陣列大小
		for(int i=0;i<id;i++)
		{
			System.out.println("Please input the max speed of this car("+ i +"): ");
			float a = input.nextFloat();//輸入maxSpeed
			while(a<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+ i +"): ");
				a = input.nextFloat();
			}
			Car t = new Car(id);//利用建構式將id值傳入
			t.setmaxSpeed(a);//將a傳入maxSpeed
			car[i] = t;//設定第i個位置的id和maxSpeed並完成儲存
		}
		System.out.println("Output car status.");
		for(int i=0;i<id;i++)//輸出題目所需
		{
			System.out.println("Car id is " + car[i].getid(i) + ".");
			System.out.println("Car max speed is "+ car[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is "+ car[i].getisTurbo() + ".");
		}
		
		// TODO Auto-generated method stub

	}

}
