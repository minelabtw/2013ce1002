package ce1002.a3.s102502032;

import java.util.Scanner;

public class A3
{
	public static void main(String[] args)
	{
		Scanner jin = new Scanner(System.in);
		int numbers = getSize(0, "number of cars", jin);
		Car carList[] = new Car[numbers];
		carList = setList(numbers);
		printList(carList);
		jin.close();
	}

	// print data
	public static void printList(Car List[])
	{
		System.out.println("Output car status.");
		for (int i = 0; i < List.length; i ++)
		{
			System.out.println("Car id is " + List[i].getID() + ".");
			System.out.println("Car max speed is " + List[i].getMaxSpeed()
					+ ".");
			System.out.println("Car turbo is " + List[i].getIsTurbo() + ".");
		}
	}

	// setting data
	public static Car[] setList(int size)
	{
		Car List[] = new Car[size];
		for (int i = 0; i < size; i ++)
		{
			List[i] = new Car(i);
			List[i].setIsTurbo();
			List[i].setMaxSpeed();
		}
		return List;
	}

	// get size; return int value
	public static int getSize(int min, String str, Scanner jin)
	{
		int size = 0;
		do
		{
			System.out.println("Please input the " + str + ": ");
			size = jin.nextInt();
			if (size <= min)
				System.out.println("Out of range!");
		}
		while (size <= min);
		return size;
	}
}
