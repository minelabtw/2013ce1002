package ce1002.a3.s102502032;

import java.util.Scanner;

public class Car
{
	private int		id;							// 賽車id
	private float	maxSpeed;						// 最高速
	private boolean	isTurbo;						// 有無渦輪
	private Scanner	jin	= new Scanner(System.in);

	Car(int carID)
	{
		id = carID;
	}

	int getID()
	{
		return id;
	}

	float getMaxSpeed()
	{
		return maxSpeed;
	}

	boolean getIsTurbo()
	{
		return isTurbo;
	}

	void setMaxSpeed()
	{
		maxSpeed = getSize(0.0, ("max speed of this car(" + id + ")"));
	}

	void setIsTurbo()
	{
		isTurbo = true;
	}

	// get Turbo; return boolean value
	/**
	 * <pre>
	 * public static boolean getTurbo()
	 * {
	 * 	boolean turbo = true;
	 * 	System.out.print(&quot;Please input the : &quot;);
	 * 	turbo = jin.nextBoolean();
	 * 	return turbo;
	 * }
	 * </pre>
	 */
	// get size; return float value
	private float getSize(Double min, String str)
	{
		float size = 0;
		do
		{
			System.out.println("Please input the " + str + ": ");
			size = jin.nextFloat();
			if (size <= min)
				System.out.println("Out of range!");
		}
		while (size <= min);
		return size;
	}
}
