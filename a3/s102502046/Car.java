package ce1002.a3.s102502046;

public class Car 
{
	private int id;// 賽車ID
	private float maxSpeed;// 最高速
	private boolean isTurbo;//有無渦輪
	public Car(int newid)
	{
		id = newid;
		isTurbo = true;
	}
	
	public void setMaxSpeed(float speed)
	{
		maxSpeed = speed;
	}
	public void print_status()
	{
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.printf("Car turbo is %b.\n", isTurbo);
	}
}
