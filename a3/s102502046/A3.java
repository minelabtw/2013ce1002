package ce1002.a3.s102502046;
import java.util.Scanner;
public class A3 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
			Scanner in = new Scanner(System.in);
			int n;
			float maxSpeed;
			System.out.println("Please input the number of cars: ");
			n = in.nextInt();
			while(n < 1)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the number of cars: ");
				n = in.nextInt();
			}
			
			Car[] c = new Car[n];
			for (int i=0;i<n;i++)
			{
				c[i] = new Car(i);
				System.out.println("Please input the max speed of this car(" + i + "): ");
				maxSpeed = in.nextFloat();
				while(maxSpeed <= 0)
				{
					System.out.println("Out of range!");
					System.out.println("Please input the max speed of this car(" + i + "): ");
					maxSpeed = in.nextFloat();
				}
				c[i].setMaxSpeed(maxSpeed);
			}
			System.out.println("Output car status.");
			for (int i=0;i<n;i++)
			{
				c[i].print_status();
			}
			in.close();
		}

}
