package ce1002.a3.s102502560;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner STDIN=new Scanner(System.in);

		int n;
		
		while(true){
			System.out.println("Please input the number of cars: ");
			n=STDIN.nextInt();
			if(n>0)break;
			System.out.println("Out of range!");
		}
		
		Car[] cars=new Car[n];								//create a Car array
		
		for(int i=0;i<n;i++){
			Car mycar=new Car(i);							//create a Car object
			float maxspeed;
			while(true){
				System.out.println("Please input the max speed of this car("+i+"): ");
				maxspeed=STDIN.nextFloat();
				if(maxspeed>0)break;
				System.out.println("Out of range!");
			}
			mycar.setMaxSpeed(maxspeed); 					//set maxspeed
			mycar.setTurbo(true);							//set turbo
			cars[i]=mycar;									//put this Car object into Car array
		}
		
		STDIN.close();
		
		System.out.println("Output car status.");
		for(int i=0;i<n;i++){
			System.out.println("Car id is "+cars[i].getId()+".");
			System.out.println("Car max speed is "+cars[i].getMaxSpeed()+".");
			System.out.println("Car turbo is "+cars[i].getTurbo()+".");
		}

	}

}
