package ce1002.a3.s102502560;

public class Car {
	
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	
	public Car(int id){
		this.id=id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setMaxSpeed(float speed){
		maxSpeed=speed;
	}
	
	public float getMaxSpeed(){
		return maxSpeed;
	}
	
	public void setTurbo(boolean turbo){
		isTurbo=turbo;
	}
	
	public boolean getTurbo(){
		return isTurbo;
	}
}
