package s102502539;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int num;
		float speed = 0;
		boolean turbo = true;
		Scanner input = new Scanner(System.in);
		
		do
		{
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if ( num < 2 || num > 5 )
				System.out.println("Out of range!");
		} while ( num < 2 || num > 5 );
		
		Car car[] = new Car[num];	//物件陣列
		
		for ( int i = 0 ; i < num ; i++ )
		{
			car[i] = new Car( i );	//再次宣告
			do
			{
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed = input.nextFloat();
				if ( speed <= 0 )
					System.out.println("Out of range!");
			} while ( speed <= 0 );
			car[i].setSpeed( speed );
		}
		
		System.out.println("Output car status.");
		
		for ( int i = 0 ; i < num ; i++ )
		{
			System.out.println("Car id is " + i
							+ ".\nCar max speed is " + car[i].getSpeed() 
							+ ".\nCar turbo is "  + turbo + ".");
		}
		

	}

}
