package ce1002.a3.s102502518;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int num) {//建構函式
		id = num;
	}
	public void SetMaxSpeed(float s) {//設最高速
		maxSpeed = s;
	}
	public void SetTurbo(boolean t) {//設有無渦輪
		isTurbo = t;
	}
	public float GetMaxSpeed() {//獲取最高速
		return maxSpeed;
	}
	public boolean GetTurbo() {//獲取有無渦輪
		return isTurbo;
	}
	

}
