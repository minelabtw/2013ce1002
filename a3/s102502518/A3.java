package ce1002.a3.s102502518;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);	
		System.out.println("Please input the number of cars: ");
		int num = scanner.nextInt();
		while (num <= 0) {//判斷為範圍之內
			System.out.println("Out of range!\n" + "Please input the number of cars: ");
			num = scanner.nextInt();
		}
		Car [] cars = new Car[num];//創造一個物件陣列
		for (int i = 0; i < num; i ++) {
			cars[i] = new Car(i); 
			float speed;
			System.out.print("Please input the max speed of this car(" + i + "): " + "\n");
			speed = scanner.nextFloat();	
			while (speed <= 0) {//判斷為範圍之內
				System.out.print("Out of range!\n" + "Please input the max speed of this car(" + i + "): " + "\n");
				speed = scanner.nextFloat();
			}
			cars[i].SetMaxSpeed(speed);//呼叫	
			cars[i].SetTurbo(true);//呼叫
		}
		System.out.println("Output car status.");
		for (int i = 0; i < num; i ++) {	
			System.out.println("Car id is " + i + ".");
			System.out.println("Car max speed is " + cars[i].GetMaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].GetTurbo() + ".");
		}
	}
}