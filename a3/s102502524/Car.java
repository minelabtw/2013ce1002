package ce1002.a3.s102502524;

public class Car {
	

	private int id;					// 賽車id
	private float maxSpeed;			// 最高速
	private boolean isTurbo;		// 有無渦輪
	
	public Car (int idpass)			//傳入id值
	{
		id = idpass;
	}
	
	void pspeed (float speedpass)	//傳入最高速
	{
		maxSpeed = speedpass;
	}
	
	void pturbo(boolean turbopass)	//傳入渦輪
	{
		isTurbo = turbopass;
	}
	
	float gspeed()					//取得最高速
	{
		return maxSpeed;
	}
	
	boolean gturbo()				//取得渦輪
	{
		return isTurbo;
	}
}
