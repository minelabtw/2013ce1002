package ce1002.a3.s102502524;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		int cou = 0;
		float spe = 0;
			
		System.out.println("Please input the number of cars: ");		//輸入賽車總數
		cou = input.nextInt();
		while(cou<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			cou = input.nextInt();
		}
		
		Car c[]= new Car[cou];											//建立Car陣列物件
		for(int i=0;i<cou;i++)
		{
			c[i] = new Car(i);
			System.out.println("Please input the max speed of this car(" + i + "): ");
			spe = input.nextFloat();									//輸入賽車最高速度
			while(spe<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i + "): ");
				spe = input.nextFloat();
			}
			c[i].pspeed(spe);											//存入陣列
			c[i].pturbo(true);											//TURBO屬性設定
		}
		
		System.out.println("Output car status.");						//輸出所有賽車屬性
		for(int i=0;i<cou;i++)
		{
			System.out.println("Car id is " + i + ".");
			System.out.println("Car max speed is " + c[i].gspeed() + ".");
			System.out.println("Car turbo is " + c[i].gturbo() + ".");
		}
	}
		
}