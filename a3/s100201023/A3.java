package ce1002.a3.s100201023;

import java.util.Scanner;

public class A3
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int number;
		float speed;
		
		//input number of cars
		while(true)
		{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number > 0)
				break;
			System.out.println("Out of range!");
		}
		
		//set array for cars
		Car[] cars = new Car[number];
		
		//set initial car information
		for(int i = 0 ; i < number ; ++i)
		{
			//input maxspeed of car
			while(true)
			{
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed = input.nextFloat();
				if(speed > 0)
					break;
				System.out.println("Out of range!");				
			}
			
			//set car
			cars[i] = new Car(i , speed);
		}
		
		//output status
		for(int i = 0 ; i < number ; ++i)
		{
			System.out.println("Car id is " + cars[i].getid() + ".");
			System.out.println("Car max speed is " + cars[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].getTurbo() + ".");
		}
	}

}
