﻿package ce1002.a3.s100201023;

public class Car
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	//constructure
	public Car(int a , float b)
	{
		id = a;
		maxSpeed = b;
		isTurbo = true;
	}
	
	//get id
	public int getid()
	{
		return id;
	}
	
	//get maxSpeed
	public float getmaxSpeed()
	{
		return maxSpeed;
	}
	
	//get isTurbo
	public boolean getTurbo()
	{
		return isTurbo;
	}
	
}
