package ce1002.a3.s102502037;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");//輸入車子數量
		int a=0,area;
		int put = input.nextInt();
		while(put<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			put=input.nextInt();
		}
		Car[] car = new Car[put];
		for(a=0 ; a<put ; a++)//建立物件
		{
			car[a]=new Car();
		}
		for( a=0 ; a<put ; a++)//設定ID
		{
			car[a].setID(a);
		}
		for(a=0 ; a<put ; a++)//輸入最快速度
		{
			System.out.println("Please input the max speed of this car("+a+"):");
			float max=input.nextFloat();
			while(max<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+a+"):");
				max=input.nextFloat();
			}
			car[a].setmax(max);
		}
		System.out.println("Output car status.");
		for(a=0 ; a<put ; a++)//輸出結果
		{
			System.out.println("Car id is "+car[a].getId()+".");
			System.out.println("Car max speed is "+car[a].getmax()+".");
			System.out.println("Car Turbo id "+car[a].getTurbo()+".");
		}
	
	}
	}
