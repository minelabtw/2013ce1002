package ce1002.a3.s102502037;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	public Car()
	{
	}
	public void setID(int x)//設定ID
	{
		id=x;
	}
	
	public void setmax(float speed)//設定最高速度
	{
		maxSpeed=speed;
	}
	public int getId()//回傳ID
	{
		return id;
	}
	public float getmax()//回傳速度
	{
		return maxSpeed;
	}
	public boolean getTurbo()//回傳是否加速
	{
		return true;
	}

}
