package ce1002.a3.s102502550;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);                                  //輸入物件宣告
		int a=0;
		float b=0;
		
		do{                                                                   //檢查輸入
			System.out.println("Please input the number of cars: ");
		    a = in.nextInt();
		    
		    if(a<=0)
		    	System.out.println("Out of range!");
		}while(a<=0);

		Car[] cars = new Car[a];                                              //Car陣列
		
		
		for(int i = 0 ; i < a ; i++ ){                                        //檢察輸入
			
			do{
				System.out.println("Please input the max speed of this car("+ i +"): ");
			    b = in.nextFloat();
			    
			    if(b<=0)
			    	System.out.println("Out of range!");
			}while(b<=0);
			
			Car temp = new Car(i,b,true);
			cars[i]=temp;                                                     //放入陣列
		}
		
		System.out.println("Output car status.");                             //印出結果
		for(int i = 0 ; i < a ; i++ ){
		    
			System.out.println("Car id is "+ i +".");
			
			System.out.println("Car max speed is "+ cars[i].get_maxSpeed() +".");
			
			System.out.println("Car turbo is "+ cars[i].get_isTurbo() +".");
			
			
		}
		in.close();
		
		
	}
	

}
