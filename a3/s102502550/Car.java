package ce1002.a3.s102502550;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int a,float b,boolean c){                //建構子
		id=a; maxSpeed=b; isTurbo=c;
	}
	
	public boolean set_maxSpeed(float b){               //除id以外的set method
		
		if(b<=0)
			return false;
		else{
		    maxSpeed=b;
		    return true;
		}
	}
	
	public void set_isTurbo(boolean c){
		isTurbo=c;
	}
	
	
	public int get_id(){                                 //各變數的get method
		return id;
	}
	
	public float get_maxSpeed(){
		return maxSpeed;
	}
	
	public boolean get_isTurbo(){
		return isTurbo;
	}
	
}
