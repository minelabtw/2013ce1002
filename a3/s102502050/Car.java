package ce1002.a3.s102502050;

public class Car {
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	public Car()
	{
		
	}
	public Car(int number)
	{
		id=number;
		isTurbo=true;
	}
	public void GetmaxSpeed(float speed)
	{
		maxSpeed=speed;
	}
	public void GetisTurbo(boolean turbo)
	{
		isTurbo=turbo;
	}
	public int valueofid()
	{
		return id;
	}
	public float valueofmaxSpeed()
	{
		return maxSpeed;
	}
	public boolean valueofisTurbo()
	{
		return isTurbo;
	}


}
