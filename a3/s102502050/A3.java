package ce1002.a3.s102502050;
import java.util.Scanner;
public class A3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int carnumber;
		float getspeed;
		Scanner scanner = new Scanner(System.in);
		do
		{
			System.out.println("Please input the number of cars: ");
			carnumber= scanner.nextInt();
			if(carnumber<=0)
				System.out.println("Out of range!");		
		}while(carnumber<=0); //輸入並檢查
		
		Car [] car= new Car[carnumber];
		for(int i=0;i<carnumber;i++)//輸入maxSpeed
		{
			car[i]=new Car(i);
			
			System.out.println("Please input the max speed of this car(" + i + "): ");
			getspeed = scanner.nextFloat();
			while (getspeed<=0)
			{
				System.out.println("Out of range!");		
				System.out.println("Please input the max speed of this car(" + i + "): ");
				getspeed = scanner.nextFloat();
			}//輸入並檢查
			car[i].GetmaxSpeed( getspeed);//將得到的值放入物件
			
			
		}
		
		System.out.println("Output car status.");	
		for(int i=0;i<carnumber;i++)//輸出車子的資訊
		{
			System.out.println("Car id is " + car[i].valueofid() + "." );
			System.out.println("Car max speed is " + car[i].valueofmaxSpeed() + ".");
			System.out.println("Car turbo is " + car[i].valueofisTurbo() + ".");
			
			
		}
		
		
	}

}
