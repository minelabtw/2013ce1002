package ce1002.a3.s102502022;
import java.util.Scanner;
public class A3
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner scn=new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int n=scn.nextInt();
		while(n<1)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n=scn.nextInt();
		}
		
		Car[] car=new Car[n];     //宣告名稱為car的物件陣列
		
		for(int i=0;i<n;i++)
		{
			System.out.println("Please input the max speed of this car("+i+"):");
			float v=scn.nextFloat();
			while(v<0 || v==0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"):");
				v=scn.nextFloat();
			}
			Car temp = new Car(i);//宣告一個暫時變數為class
			temp.setspeed(v);//傳入v
			car[i]=temp;//存入陣列
			
		}
		
		for(int i=0;i<n;i++)
		{
			System.out.println("Output car status.");
			System.out.println("Car id is "+car[i].getid()+".");
			System.out.println("Car max speed is "+car[i].outspeed()+".");
			System.out.println("Car turbo is "+car[i].judge()+".");
		}
	}

}
