package ce1002.a3.s102502022;

public class Car 
{
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	
	public Car(int i)
	{
		id=i;         //將i傳入id  
		isTurbo=true;
	}
	public int getid()
	{
		return id;
	}
	public void setspeed(float v)
	{
		maxSpeed=v;    //將v傳入maxspeed;
	}
	public float outspeed()
	{
		return maxSpeed;
	}
    public boolean judge()
    {
        return isTurbo;
    }

	

}
