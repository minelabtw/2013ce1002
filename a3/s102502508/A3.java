package ce1002.a3.s102502508;
import java.util.Scanner; ;
public class A3 {
    public static Scanner input ;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);  //系統輸入
		
		int noc=0 ;   //使用者的輸入值
		int spd=0 ;   //給予車子速度
		boolean isturbo; //布林值的解果只有兩種(true or false)
		
		
		
		do{//利用迴圈詢問使用者要製造幾輛車並對其輛數加以限制
			
			System.out.println("Please input the number of cars: ");
			noc=input.nextInt() ;
			
			if(noc>0)
			{
				break ;
			}
			else if(noc<=0)
			{
				System.out.println("Out of range!");
			}
			
		}while(noc<=0);
		
		
		Car[] cars =new Car[noc] ;  //建立cars的物件指標(停車場)
		
		
		for(int i=0 ; i<noc ; i=i+1)
		{   
			Car a=new Car(i); //建立新車
			
			
			do{//利用迴圈給予每台車所限制中的速度
				
			System.out.print("Please input the max speed of this car("+i+"): ");
			spd=input.nextInt() ;
			if(spd>0)
			{  
               a.setSpeed(spd);  //給予該台車的速度
               a.setTurbo(true);//給予其該車的屬性
               
				break ;
			}
			else if(spd<=0)
			{
				System.out.println("Out of range!");
			}
		     }while(spd<=0);
			
			cars[i]=a ;//將所新建的新車放到物件指標中(建立在停車場中)
		}
		System.out.println("Output car status.");
		for(int i=0 ; i<noc ; i=i+1)
		{   //利用迴圈將車子的id,速度和有(無)渦輪的情況輸出
			System.out.println("Car id is "+i+".");
			System.out.println("Car max speed is "+cars[i].getSpeed()+".");
			System.out.println("Car turbo is "+cars[i].getTurbo()+".");
		}
		
		
		    input.close() ;
		
		
		
		

	}

}
