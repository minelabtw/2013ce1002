package ce1002.a3.s100204006;

public class Car
{
	private int id;      //賽車id
	private float maxSpeed;     //最高速
	private boolean isTurbo;      //有無渦輪
	
	public Car() 
	{
		this.isTurbo=true;
	}
	public Car(int id, int maxSpeed, boolean isTurbo) 
	{
		this.id = id;
		this.maxSpeed = maxSpeed;
		this.isTurbo = isTurbo;
	}
	
	// setter for ID :)
	public void setid(int id)
	{
		this.id = id;
	}
	// getter for ID
	public int getid()
	{
		return this.id;
	}
	
	public void setmaxSpeed(float maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}
	// setter for maxSpeed
	public float getmaxSpeed()
	{
		return this.maxSpeed;
	}
	// getter for maxSpeed
	public boolean getisTurbo()
	{
		return this.isTurbo;
	}
	//public void setisTurbo(boolean isTurbo)
	//{
	//	this.isTurbo = isTurbo;
	//}
	

}
