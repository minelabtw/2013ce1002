package ce1002.a3.s100204006;

import java.util.Scanner;

public class A3
{
	public static void main(String[] args) 
	{
		while (true)
		{
			int N;
			Scanner scanner = new Scanner(System.in) ;      //System.in用來取得使用者的輸入
			System.out.print("Please input the number of cars:");
			N = scanner.nextInt() ;
			
			if(N>0)
			{
				Car carList[] = new Car[N];		// set parameters 
				for(int i = 0; i < N ; i++)
				{
					carList[i] = new Car();
					// ask user for information
					// get information
					// set car 
					carList[i].setid(i);
					System.out.print("Please input the max speed of this car:");
					
					float maxSpeed;
					scanner = new Scanner(System.in) ;      //System.in用來取得使用者的輸入
					maxSpeed = scanner.nextFloat();
					if(maxSpeed>0)
					{
						carList[i].setmaxSpeed(maxSpeed);	
						System.out.println("Output car status.");
					}
					else
					{
						System.out.println("Out of range!");	
					}									
				}	
				for(int i = 0; i < N ; i++)
				{
					System.out.println("Car id is "+carList[i].getid()+".");
					System.out.println("Car max speed is "+carList[i].getmaxSpeed()+".");
					System.out.println("Car turbo is "+carList[i].getisTurbo()+".");
				}
			}
			else
			{
				System.out.println("Out of range!");	
			}
			
		}	
	}
}
