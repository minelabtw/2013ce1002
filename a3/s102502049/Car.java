package ce1002.a3.s102502049;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(){
		
	}
	
	public Car(int x) { // constructor
		id = x;
		isTurbo = true;
	}

	public void setmaxSpeed(float x) {
		maxSpeed = x;
	}

	public float getmaxSpeed() {
		return maxSpeed;
	}

	public void setisTurbo(boolean x) {
		isTurbo = x;
	}

	public boolean getisTurbo() {
		return isTurbo;
	}

	public int getid() {
		return id;
	}
}
