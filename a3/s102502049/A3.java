package ce1002.a3.s102502049;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number = 1; // car number

		do {
			if (number <= 0) {
				System.out.println("Out of range!");
			}
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		} while (number <= 0); // ask car number

		Car[] car = new Car[number]; // set number of Car objects
		int i = 0; // iteration variable
		for (; i < number; i++) {
			Car car1 = new Car(i);// use constructor set id
			float x = 1; // maxSpeed
			do {
				if (x <= 0) {
					System.out.println("Out of range!");
				}
				System.out.println("Please input the max speed of this car("
						+ i + "):");
				x = input.nextFloat();
			} while (x <= 0); // ask maxSpeed
			car1.setmaxSpeed(x); // set maxSpeed
			car[i] = car1; // put car1 object in car array
		}

		System.out.println("Output car status."); // print the consequences
		for (int j = 0; j < number; j++) {
			System.out.println("Car id is " + car[j].getid() + ".");
			System.out.println("Car max speed is " + car[j].getmaxSpeed() + ".");
			System.out.println("Car turbo is " + car[j].getisTurbo() + ".");
		}

	}

}
