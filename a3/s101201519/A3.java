package ce1002.a3.s101201519;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int ID = 0;
		float speed = 0;
		Car[] car;
		
		while(ID <= 0)
		{
			System.out.println("Please input the number of cars: ");
			ID = input.nextInt();
			if(ID<=0)
			{
				System.out.println("Out of range!");
			}
		}
		
		car = new Car[ID];
		
		for(int i=0; i<ID; i++)
		{
			car[i] = new Car(i);
			
			while(speed<=0)
			{
				System.out.println("Please input the max speed of this car(" +i+ "):");
				speed = input.nextFloat();
				if(speed<=0)
				{
					System.out.println("Out of range!");
				}
			}
			car[i].setmaxSpeed(speed);
			car[i].setisTurbo(true);
			speed = 0;
		}
		System.out.println("Output car status.");
		
		for(int i=0; i<ID ;i++)
		{
			System.out.println("Car id is " + car[i].ID() + ".");
			System.out.println("Car max speed is " + car[i].MaxSpeed() + ".");
			System.out.println("Car turbo is " + car[i].IsTurbo() + ".");
		}
		
		input.close();
	}

}
