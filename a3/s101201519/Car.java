package ce1002.a3.s101201519;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int id)
	{
		this.id = id;
		maxSpeed = 0;
		isTurbo = true;
	}
	
	public void setmaxSpeed(float maxSpeed)
	{
		this.maxSpeed = maxSpeed;
	}
	
	public void setisTurbo(boolean isTurbo)
	{
		this.isTurbo = isTurbo;
	}
	
	public int ID()
	{
		return id;
	}
	
	public float MaxSpeed()
	{
		return maxSpeed;
	}
	
	public boolean IsTurbo()
	{
		return isTurbo;
	}
}
