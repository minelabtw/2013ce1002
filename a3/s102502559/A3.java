package ce1002.a3.s102502559;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //建立一個scanner類別的變數input(新建一個輸入器)
		System.out.println("Please input the number of cars: ");
		int number = input.nextInt();//將輸入的整數值指定給變數number
		//判斷變數number範圍
		while (number <= 0) {
			System.out.println("Out of Range!");
			System.out.print("Please input the number of cars: ");
			number = input.nextInt();
		}
		//新增一個Car型別的陣列,陣列長度為number
		Car[] car = new Car[number];

		float input_speed;
		//新建一個物件時,利用建構子讓使用者輸入的值指定給物件的屬性,並判斷使用者輸入變數值的範圍
		for (int i = 0; i < number; i++) {
			car[i] = new Car(i);
			System.out.println("Please input the max speed of this car(" + i + "): ");
			float speed = input.nextFloat();
			while (speed <= 0) {
				System.out.println("Out of Range!");
				System.out.print("Please input the max speed of this car(" + i + "): \n");
				speed = input.nextFloat();
			}
			car[i].setmaxSpeed(speed);
		}
		//呼叫Class內的method顯示各個物件的屬性值
		for (int j = 0; j < number; j++) {
			car[j].show();
		}
	}

}
