package ce1002.a3.s102502559;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int number) {
		id = number;
		isTurbo = true;
	}
	public void setmaxSpeed(float speed)
	{
		maxSpeed=speed;
	}
	public void show()
	{
		System.out.println("Car id is " + id);
		System.out.println("Car max speen is " + maxSpeed);
		System.out.println("Car turbo is " + isTurbo);
	}
}
