package ce1002.a3.s101201046

import java.util.Scanner;

public class A3 {
	
	public void main (String[] args) {
		Scanner scan =  new Scanner (System.in); // for input
		int num; // the number of cars
		int i;
		float speed;
		
		//input the number of cars
		do {
			System.out.println("Please input the number of cars: ");
			num = scan.nextInt();
			if (num <= 0)
				System.out.println("Out of range!");
		} while (num <= 0);
		
		Car[] cars = new Car[num];
		
		// To set the status of every car
		for (i = 0; i < num; i++) {
			cars[i] = new Car(i);
			
			do {
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed = scan.nextFloat();
				if (speed <= 0.0)
					System.out.println("Out of range!");
			} while(speed <= 0.0);
			
			cars[i].setmaxSpeed(speed);
		}
		
		
		//Output car status
		System.out.println("Output car status.");
		
		for (i = 0; i < num; i++) {
			System.out.println("Car id is " + i + ".");
			System.out.println("Car max speed is " + cars[i].showmaxSpeed() + ".\n" +
							   "Car turbo is true.");
		}
		
	}

}