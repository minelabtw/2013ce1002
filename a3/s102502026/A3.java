package ce1002.a3.s102502026;
import java.util.Scanner;
public class A3 {
	private static Scanner input;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input = new Scanner(System.in);
		int n= 0;
	do	//ask numbers of cars >1
	{
		System.out.println("Please input the number of cars: ");
		n= input.nextInt();
		if(n<1)
		System.out.println("Out of Range!");
	}while(n<1);	//defining n>1
	
	Car []cars = new Car[n];	//defining cars to be array
	float sp=0;  //define maxspeed to be float

	for(int i=0; i<n; i++)	//doing for maxspeed of each car
	{
		Car z= new Car(i);
		do	//ask maxspeed only >0
		{
			System.out.println("Please input the max speed of this car(" + i + "): ");
			sp= input.nextFloat();
			if(sp<=0)
			System.out.println("Out of Range!");
		}while(sp<=0);	//defining sp>0
		z.Msp(sp);	//getting maxspeed
		cars[i]=z;	//put the maxspeed in each array
	}
	System.out.println("Output Car status.");
	for(int x=0; x<n; x++) //printing all cars status
	{
		System.out.println("Car id is "+ cars[x].Id() +"." );
		System.out.println("Car max speed "+ cars[x].MaxSp()+"." );
		System.out.println("Car turbo is "+ cars[x].Turbo() +"." );
	}
	
}
}
