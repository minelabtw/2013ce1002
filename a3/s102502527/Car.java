package ce1002.a3.s102502527;

import java.util.Scanner;

public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public void Car()
	{
		Scanner input = new Scanner(System.in);
		int a = 0;
		float b = 0;
		
		System.out.println("Please input the number of cars:");//叫出字彙並輸入值
		a = input.nextInt();
		while ( a <= 0 )//確認是否大於零
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			a = input.nextInt();
		}
		
		id = a;
		
		float arrayspeed[] = new float [id];//設定陣列
		
		for ( int j = 0 ; j < id ; j++ )//依序叫出字彙並輸入最大素
		{
			System.out.println("Please input the max speed of this car(" + j + "): ");
			b = input.nextInt();
			while ( b <= 0 )
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + j + "): ");
				b = input.nextInt();
			}
			
			arrayspeed[j] = b;//將最大速存入陣列
		}
		
		System.out.println("Output car status.");
		
		for ( int k = 0 ; k < id ; k++ )//依序輸出各項數值
		{
			System.out.println("Car id is " + k +".");
			System.out.println("Car max speed is " + arrayspeed[k] +".");
			isTurbo = arrayspeed[k] > 0;//判斷是否正確
			System.out.println("Car turbo is " + isTurbo +"."); 
		}
	}
	
}
