package ce1002.a3.s101201506;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;
	//constructure
	public Car(int a, float b) {
		id = a;
		maxSpeed = b;
		isTurbo = true;
	}
	//ID
	public int getid() {
		return id;
	}
	//速度
	public float getmaxSpeed() {
		return maxSpeed;
	}
	//有無TURBO
	public boolean getTurbo() {
		return isTurbo;

	}

}
