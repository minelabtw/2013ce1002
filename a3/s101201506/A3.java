package ce1002.a3.s101201506;

import java.util.Scanner;

public class A3 {
	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);

		int x; // 看有要幾台車
		float y; // 最大速度
		// 確認輸入範圍
		do {
			System.out.println("Please input the number of cars:");
			x = scanner.nextInt();
			if (x < 1)
				System.out.println("Out of range!");
		} while (x < 1);
		Car[] cars = new Car[x];
		// 確認輸入範圍
		for (int i = 0; i < x; i++) {
			do {
				System.out.println("Please input the max speed of this car("
						+ i + "): ");
				y = scanner.nextFloat();
				cars[i] = new Car(i, y);
				if (y < 0)
					System.out.println("Out of range!");
			} while (y < 0);
		}
		System.out.println("Output car status.");
		// 輸出
		for (int j = 0; j < x; j++) {
			System.out.println("Car id is " + cars[j].getid() + ".");
			System.out.println("Car max speed is " + cars[j].getmaxSpeed()
					+ ".");
			System.out.println("Car turbo is " + cars[j].getTurbo() + ".");

		}

	}
}
