﻿package ce1002.a3.s102502557;

import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int carname)//constructor 建構元
	{
		id=carname;
		isTurbo = true;
	}
	
	Scanner input = new Scanner(System.in);
	
	public void set_maxSpeed (float v)//設定速度
	{
	do{
		System.out.print("Please input the max speed of this car(" + id +"):");	
		
		v=input.nextFloat();
		if(v<=0)
		{
			System.out.println("Out of range!");
		}
	  }while(v<=0);
	    maxSpeed=v;
	}
	
	public void get_maxSpeed(int v)//輸出速度
	{
		System.out.print("Car max speed is " + v + ".");
		
	}
	
	public void set_turbo( boolean a )//設定加速
	{
		isTurbo = a;
	}
	
	public void get_turbo()//輸出加速
	{
		System.out.print("Car turbo is ");
		System.out.print(isTurbo);
	}
	
	public void print_status()
	{
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.printf("Car turbo is %b.\n", isTurbo);
	}

}
