package ce1002.a3.s101201521;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int numberOfCars = 0;
		//prompt for number of cars
		do{
			System.out.println("Please input the number of cars: ");
			numberOfCars = input.nextInt();
			if(numberOfCars <= 0)
				System.out.println("Out of range!");
		}while(numberOfCars <= 0);
		//create cars and set max speed and turbo or not of the cars
		Car[] cars = new Car[numberOfCars];
		for(int i = 0; i < cars.length; i++){
			cars[i] = new Car(i);
			//prompt for max speed of cars
			do{
				System.out.println("Please input the max speed of this car("
				+ i + "):");
				cars[i].setMaxSpeed(input.nextFloat());
				if(cars[i].getMaxSpeed() <= 0)
					System.out.println("Out of range !");
			}while(cars[i].getMaxSpeed() <= 0);
			cars[i].setIsTurbo(true);
		}
		//print car status: id, max speed ,and turbo or not 
		System.out.println("Output car status.");
		for(int i = 0; i < cars.length; i++){
			System.out.println("Car id is " + cars[i].getId() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].getIsTurbo() + ".");
		}
	}

}
