package ce1002.a3.s101201521;

public class Car {
	private int id;//id of car
	private float maxSpeed;//max speed of car 
	private boolean isTurbo;// is the car turbo or not
	public Car(int carId){
		id = carId;//set id of car
	}
	//set max speed of car
	public void setMaxSpeed(float carMaxSpeed){
		maxSpeed = carMaxSpeed;
	}
	//set the car is turbo or not
	public void setIsTurbo(boolean carIsTurbo){
		isTurbo = carIsTurbo;
	}
	//return id of car
	public int getId(){
		return id;
	}
	//return max speed of car
	public float getMaxSpeed(){
		return maxSpeed;
	}
	//return the car is turbo or not
	public boolean getIsTurbo(){
		return isTurbo;
	}
}
