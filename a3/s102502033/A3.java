package ce1002.a3.s102502033;

import java.util.Scanner;

public class A3
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		int number = 0;//汽車數
		float Mspeed = 0;//暫存速度最大值

		System.out.println("Please input the number of cars: ");
		Scanner input = new Scanner(System.in);
		number = input.nextInt();
		while (number <= 0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}
		Car[] car = new Car[number];//car類別的car array
		for (int i = 0; i < number; i++)//輸入每個車的速度
		{
			car[i] = new Car(i);//初始化
			System.out.println("Please input the max speed of this car("+i+"): ");
			Mspeed = car[i].Maxspeed();
			while (Mspeed <= 0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"): ");
				Mspeed = car[i].Maxspeed();
			}
			
		}

		System.out.println("Output car status.");
		for (int i = 0; i < number; i++)
		{
			System.out.println("Car id is " + car[i].showid() + ".");
			System.out.println("Car max speed is " + car[i].max() + ".");
			System.out.println("Car turbo is " +car[i].showisTurbo()+ ".");

		}

	}

}
