package ce1002.a3.s102302053;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int carid){//constructor of car.設置屬性初始值
		id = carid;	
		maxSpeed = 0;
		isTurbo = true;
	}
	public void setmaxSpeed(float speed){//設定最高速
		maxSpeed = speed;
	}
	public void setTurboStatus( boolean status){//設丁有無渦輪
		isTurbo = status;
	}
	public int getCarid(){//傳回車輛的id
		return id;
	}
	public float getmaxSpeed(){//傳回車輛的最高速
		return  maxSpeed;
	}
	public boolean getTurbo(){//傳回渦輪狀態
		return isTurbo;
	}

}
