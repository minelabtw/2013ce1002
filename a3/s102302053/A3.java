package ce1002.a3.s102302053;

import java.util.Scanner;

public class A3 {
	private static Scanner scanner;

	public static void main(String[] args) {
		int carnum;
		scanner = new Scanner(System.in);
		do//提示使用者輸入車賽車數目,並且檢查範圍是否符合要求
		{
			System.out.println("Please input the number of cars: ");
			carnum = scanner.nextInt();
			if(carnum <= 0){
				System.out.println("Out of range!");
			}			
		}while(carnum <= 0);
		
		Car[] car=new Car[carnum] ;//依賽車數量建立car陣列
		
		float temp;//暫存值
		
		for(int a = 0; a < carnum; a++){//依序輸入賽車的最高速度,並且檢查範圍是否符合標準.符合標準的話存入對應的car物件中
			do{
				System.out.println("Please input the max speed of this car("+ a +"): ");
				temp = scanner.nextFloat();
				if(temp <= 0){
				System.out.println("Out of range!");
				}
			}while(temp <= 0);
			car[a] = new Car(a);
			car[a].setmaxSpeed(temp);
		}
		System.out.println("Output car status.");//依序輸出車輛狀態,id,最高速,有無渦輪
   
	    for(int a = 0; a < carnum; a++){
	    	System.out.println("Car id is " + car[a].getCarid() + ".");
	    	System.out.println("Car max speed is " + car[a].getmaxSpeed() + ".");
	    	System.out.println("Car turbo is " + car[a].getTurbo() + ".");
	    }

	}

}
