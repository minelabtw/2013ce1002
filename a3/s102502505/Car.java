package ce1002.a3.s102502505;
import java.util.Scanner;

public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	public static int caramount(int amount)//設立一個method去詢問賽車數量
	{
		Scanner input = new Scanner( System.in );
		
		System.out.println("Please input the number of cars: ");//輸出字
		amount=input.nextInt();
		while(amount<0)//限定賽車數目為正整數
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			amount=input.nextInt();
		}
		
		return amount;//回傳賽車數
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner( System.in );
		int amount;
		double[][] cararray;//定義陣列
		cararray = new double[10][10];
		amount=caramount(0);//給一個初始值0
		for(int i=0;i<amount;i++)//將賽車id設定
		{
			cararray[0][i]=i;
		}
		for(int i=0;i<amount;i++)//將賽車渦輪設定為true
		{
			cararray[2][i]=1;
		}
		
		for(int i=0;i<amount;i++)//利用迴圈來設定最大速
		{
			System.out.println("Please input the max speed of this car("+i+"): ");
			cararray[1][i]=input.nextDouble();
			while(cararray[1][i]<0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"): ");
				cararray[1][i]=input.nextDouble();
			}
		}
		
		System.out.println("Output car status.");//利用迴圈輸出陣列中的元素，包括id,速度,渦輪
		for(int i=0;i<amount;i++)
		{
			System.out.println("Car id is "+i+".");
			System.out.println("Car max speed is "+cararray[1][i]+".");
			if(cararray[2][i]==1)
				System.out.println("Car turbo is true.");
			else
				System.out.println("Car turbo is false.");
		}
		
		
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isTurbo() {
		return isTurbo;
	}

	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}

}
