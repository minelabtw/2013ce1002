package ce1002.a3.s102502017;
import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	Scanner input = new Scanner(System.in);
	
	Car(int x){
		id = x;
	}
	
	void setMaxSpeed(){
		//set maxspeed(?
		while(true){
			System.out.println("Please input the max speed of this car(" + id + "): ");
			float x = input.nextFloat();
			if(!(x>0))System.out.println("Out of range!");
			else {
				maxSpeed = x;
				break;
			}
		}
	}
	//print the car's status
	void printCarStatus(){
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.println("Car turbo is " + isTurbo + ".");
	}
}
