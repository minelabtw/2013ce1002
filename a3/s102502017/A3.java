package ce1002.a3.s102502017;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int number;
		
		while(true){
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if(number>0)break;
			else System.out.println("Out of range!");
		}
		//Creat a array of Class_Car, the size is the number 
		Car[] cars = new Car[number];
		for(int i=0;i<number;i++){
			//Set constructor
			cars[i] = new Car(i);
			cars[i].setMaxSpeed();
		}
		
		System.out.println("Output car status.");
		
		//print each car's status
		for(int i=0;i<number;i++){
			cars[i].printCarStatus();
		}
		
		input.close();
	}
}
