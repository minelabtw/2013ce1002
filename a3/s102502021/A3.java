package ce1002.a3.s102502021;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int num;
		float v;
		do
		{
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if(num <=0)
			{
				System.out.println("Out of range!");
			}
		}
		while(num <=0);
		
		Car car[]=new Car[num];
		for (int i = 0; i < num; i++)
	    {
	         car[i]=new Car(i); 
	    } 
		for(int i=0;i<num;i++)
		{
			System.out.println("Please input the max speed of this car("+ i + ")");
			do
			{
				v = input.nextInt();
				if(v <= 0)
				{
					System.out.println("Out of range!");
				}
			}
			while(v <= 0);
			car[i].setmaxSpeed(v);
		}
		System.out.println("Output car status."); 
		for(int i=0 ;i < num ; i++)
		{
			System.out.println("Car id is " + car[i].getid() + ".");
			System.out.println("Car max speed is " + car[i].getmaxSpeed() + ".");
	        System.out.println("Car turbo is " + car[i].getTurbo() + ".");
		}
 
		input.close();
	}

}
