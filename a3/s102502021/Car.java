package ce1002.a3.s102502021;

public class Car {
	private int id=0;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	
	public Car(int n)
	{
		id = n ;
	}
	public void setmaxSpeed(float setMaxspeed) 
	{ 
	    maxSpeed = setMaxspeed;
	 } 
	public int getid()
	 {
	     return id; 
	} 
	public boolean getTurbo()
	 { 
	    return isTurbo; 
	}
	 public float getmaxSpeed()
	 {
	     return maxSpeed; 
	}
}
