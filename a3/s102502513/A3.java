package ce1002.a3.s102502513;

import java.util.Scanner;

public class A3
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		
		int number;
		
		do{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			
			if(number<=0)
			{
				System.out.println("Out of range!");
			}	
		}while(number<=0);

		Car car[] = new Car [number];  //運用Car.java宣告car陣列
		
		for(int i=0; i<number; i++)
		{
			car[i] = new Car(i);  //叫出 Car class 以便使用下列函式
			car[i].storemaxSpeed();
			car[i].turbo(true);	
		}
		
		System.out.println("Output car status.");
		
		for(int i=0; i<number; i++)
		{
			car[i].showstatus();
		}
		input.close();	
	}
}
