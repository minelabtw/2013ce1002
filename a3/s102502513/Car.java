package ce1002.a3.s102502513;

import java.util.Scanner;

public class Car 
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	Car (int number)  // 建構子參數傳入
	{
		id = number;
	}
	
	void storemaxSpeed()  //存入使用者所輸入的最快車速
	{
		Scanner input = new Scanner (System.in);
		
		do{
			System.out.println("Please input the max speed of this car(" + id + "): ");
			maxSpeed = input.nextFloat();
			
			if(maxSpeed<=0)
			{
				System.out.println("Out of range!");
			}
		}while(maxSpeed<=0);
	}
	
	void turbo(boolean turbo)  //判斷有無渦輪
	{
		isTurbo = turbo;
	}
	
	void showstatus()  //顯示車子的資料
	{
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed +".");
		
		if(isTurbo == true)
		{
			System.out.println("Car turbo is ture.");
		}
		else
		{
			System.out.println("Car turbo is false.");
		}
	}
}
