package ce1002.a3.s102502018;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		int num = scan.nextInt();
		while(num<1)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = scan.nextInt();
		}
		int carid=0;
		Car car = new Car();				//建立物件
		int [] carsid;
		carsid = new int [num];
		float [] maxspeed;
		maxspeed = new float [num];
		boolean [] turbo;
		turbo = new boolean [num];
		for(carid=0;carid<num;carid++)
		{
			carsid[carid]=car.setid(carid);		//儲存id到陣列裡
			System.out.println("Please input the max speed of this car("+carid+"): ");
			float speed = scan.nextFloat();
			while(speed<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+carid+"): ");
				speed = scan.nextFloat();
			}
			maxspeed[carid]=car.setspeed(speed);//儲存speed到陣列裡
			turbo[carid]=car.setisturbo(true);	//boolean值為true
		}
		System.out.println("Output car status.");
		for(carid=0;carid<num;carid++)
		{			
			System.out.println("Car id is "+carsid[carid]+".");
			System.out.println("Car max speed is "+maxspeed[carid]+".");
			System.out.println("Car turbo is "+turbo[carid]+".");
		}


	}

}
