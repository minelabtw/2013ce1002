package ce1002.a3.s102502031;

import java.util.Scanner;
import ce1002.a3.s102502031.Car;

public class A3 {
	public static void main(String[] args) {
		Scanner jin = new Scanner(System.in);
		int number = promptForNumber(jin);
		Car[] car = new Car[number]; // create array with car type inside
		carProperty(number, car, jin);
		jin.close();
		printout(number, car);
	}

	public static float prompt(String question, Scanner input, boolean integer) {
		float valueToPrompt;
		do {
			System.out.println(question);
			valueToPrompt = input.nextFloat();
			if (integer)
				if ((float) ((int) valueToPrompt) != valueToPrompt)
					valueToPrompt = 0;
			positiveTest(valueToPrompt);
		} while (valueToPrompt <= 0);
		return valueToPrompt;
	}

	public static int promptForNumber(Scanner input) {
		// prompt for the number of cars, it can check if the number is positive integer or not
		float typeTest = prompt("Please input the number of cars: ", input, true);
		return (int) typeTest;
	}

	public static void positiveTest(int value) {
		if (value <= 0)
			System.out.println("Out of range!");
	}

	public static void positiveTest(float value) {
		if (value <= 0)
			System.out.println("Out of range!");
	}
	
	public static void carProperty(int times, Car[] name, Scanner input) {
		for (int i = 0; i < times; i++) {
			name[i] = new Car(); // create car in every array address
			name[i].set_id(i);
			name[i].set_maxSpeed(prompt("Please input the max speed of this car(" + i + "): ", input, false));
			name[i].set_isTurbo(true);
		}
	}
	
	public static void printout(int times, Car[] name) {
		System.out.println("Output car status.");
		for (int i = 0; i < times; i++) {
			name[i].printAllResult();
		}
	}
}