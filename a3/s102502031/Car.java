package ce1002.a3.s102502031;

public class Car {
	private int id;
	private float maxSpeed;
	private boolean isTurbo;

	Car() {
	}

	public void set_id(int new_id) {
		id = new_id;
	}

	public void set_maxSpeed(float new_maxSpeed) {
		maxSpeed = new_maxSpeed;
	}

	public void set_isTurbo(boolean new_isTurbo) {
		isTurbo = new_isTurbo;
	}

	private void printOneLine(String property, int value) {
		System.out.println("Car " + property + " is " + value);
	}

	private void printOneLine(String property, float value) {
		System.out.println("Car " + property + " is " + value);
	}

	private void printOneLine(String property, boolean value) {
		System.out.println("Car " + property + " is " + value);
	}

	public void printAllResult() {
		printOneLine("id", id);
		printOneLine("max speed", maxSpeed);
		printOneLine("turbo", isTurbo);
	}
}
