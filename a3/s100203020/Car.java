package ce1002.a3.s100203020;

public class Car {
	
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	
	public Car(int idNum){
		this.id = idNum;
	}
	

	public Car(int idNum, float maxSpeedNum,boolean Turbo){
		this.id = idNum;
		this.maxSpeed = maxSpeedNum;
		this.isTurbo = Turbo;
	}
	
	public int getId() {
		return id;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public boolean isTurbo() {
		return isTurbo;
	}
	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}


}

