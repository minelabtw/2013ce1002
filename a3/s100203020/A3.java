package ce1002.a3.s100203020;

import java.util.Scanner;

public class A3 {
	
	private static Scanner scanner;
	

	
	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		
		//determine numbers of car
		int carNum;
		
		do{
			System.out.println("Please input the number of cars: ");
			carNum = scanner.nextInt();
			
			//numbers of car must be bigger than 0;
			if (carNum<=0)
				System.out.println("Out of range!");
		}while(carNum<=0);
		
		//declare car 
		Car[] car = new Car[carNum];
		//set  cars
		float speed;
		boolean turbo = true;
		for(int i=0; i < carNum; i++){
			
			Car setcar = new Car(i);//id = i
			do{
				System.out.println("Please input the max speed of this car("+setcar.getId()+"): ");
				speed= scanner.nextFloat();
				//speed of car must be bigger than 0;
				if (speed<=0)
					System.out.println("Out of range!");
			}while(speed<=0);
			
			setcar.setMaxSpeed(speed);
			setcar.setTurbo(turbo);
			car[i]=setcar;//put into car array
			
		}
		
		
		//output car status
		System.out.println("Output car status.");
		for(int i=0; i < carNum; i++){
			System.out.println("Car id is "+car[i].getId()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car turbo is "+car[i].isTurbo()+".");
		}
			
		
		
		
		
		
		
	}

}
