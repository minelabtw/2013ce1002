package ce1002.a3.s102502517;

import java.util.Scanner; //import Scanner
import ce1002.a3.s102502517.Car; //import Car.java

public class A3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberOfcar = 0;
		
		do //輸入 numberOfcar
		{
			System.out.println("Please input the number of cars: ");
			numberOfcar = scanner.nextInt();
			if(numberOfcar<=0)
				System.out.println("Out of range!");
		}
		while(numberOfcar<=0);
		
		Car[] car = new Car[numberOfcar]; //宣告 Car矩陣
		
		for(int i=0;i<=numberOfcar-1;i++) //實體化 car[數字]
			car[i] = new Car();
		
		for(int j=0; j<=numberOfcar-1; j++) //輸入id
			car[j].setId(j);
		
		float maxSpeed = 0;
		
		for(int k=0; k<=numberOfcar-1; k++) //輸入maxSpeed
		{
			do
			{
				System.out.println("Please input the max speed of this car(" + k + "): ");
				maxSpeed = scanner.nextFloat();
				car[k].setMaxSpeed(maxSpeed);
				if(maxSpeed<=0)
					System.out.println("Out of range!");
			}
			while (maxSpeed<=0);
		}
		
		for(int m=0; m<=numberOfcar-1; m++) //輸入isTurbo
			car[m].setTurbo(true);
		
		System.out.println("Output car status.");
		for(int n=0; n<=numberOfcar-1; n++) //輸出結果
		{
			System.out.println("Car id is " + car[n].getId() + ".");
			System.out.println("Car max speed is " + car[n].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + car[n].isTurbo() + ".");
		}
	}

}
