package ce1002.a3.s102502535;

import java.util.Scanner; //import Scanner

public class A3 {

	public static void main(String[] args) {

		int num = 0;
		float maxSpeed = 0; // 設定變數。
		int[] carid;
		float[] maxs; // 設定數列。

		Car car = new Car();

		Scanner input = new Scanner(System.in);

		System.out.println("Please input the number of cars: ");
		num = input.nextInt();
		while (num <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		} // 使輸入數量，並判斷是否合理。

		carid = new int[num];
		maxs = new float[num];

		for (int i = 0; i < num; i++) {
			carid[i] = Car.carid(i);
		} // 存車的id進陣列。

		for (int i = 0; i < num; i++) {
			System.out.println("Please input the max speed of this car(" + i
					+ "): ");
			maxSpeed = input.nextFloat();
			while (maxSpeed <= 0) {
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("
						+ i + "): ");
				maxSpeed = input.nextFloat();
			}
			maxs[i] = Car.setMaxSpeed(maxSpeed);
		} // 使輸入各車車速，並存進maxSpeed陣列。

		System.out.println("Output car status.");
		for (int i = 0; i < num; i++) {
			System.out.println("Car id is " + carid[i] + ".");
			System.out.println("Car max speed is " + maxs[i] + ".");
			System.out.println("Car turbo is " + car.setisTurbo());
		} // 輸出狀態。

		input.close();

	}

}
