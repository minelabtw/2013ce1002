package ce1002.a3.s102502535;

public class Car {

	private static int id; // 賽車id
	private static float maxSpeed; // 最高速
	private boolean isTurbo; // 有無渦輪

	public static int carid(int n) {
		id = n;
		return id;
	} // 回傳id。

	public static float setMaxSpeed(float ms) {
		maxSpeed = ms;
		return maxSpeed;
	} // 回傳maxSpeed。

	public boolean setisTurbo() {
		boolean it = true;
		isTurbo = it;
		return isTurbo;
	} // 設定渦輪都為true，並回傳其值。

}
