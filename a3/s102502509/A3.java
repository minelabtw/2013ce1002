package ce1002.a3.s102502509;

import java.util.Scanner;

public class A3 
{

	public static void main(String[] args) 
	{
		int number;
		Scanner input = new Scanner (System.in);
		do 
		{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if (number <= 0)
			{
				System.out.println("Out of range!");
			}
			
		}while (number <= 0);
		
		Car truck[] = new Car [number];// 運用Car.java的東西宣告truck陣列
		
		for (int i = 0; i < number; i++)
		{
			truck[i] = new Car(i);// 請出Car class以茲叫出下列函式
			truck[i].getmaxSpeed();// 叫出Car中最高速度函式並存入陣列
			truck[i].getturbo(true);// 叫出函式存入isturbo至truck陣列
		}
		System.out.println("Output car status.");
		
		for (int i = 0; i < number; i++)
		{
			truck[i].getstatus();
		}
		input.close();
	}

}
