package ce1002.a3.s102502509;

import java.util.Scanner;

public class Car 
{
		private int id;
		private float maxSpeed;
		private boolean isTurbo;
		
		Car (int number)// 建構子參數傳入
		{
			id = number;
		}
		
		void getmaxSpeed()// 儲存使用者輸入之車子的最高速度的函式
		{
			Scanner input = new Scanner (System.in);
			do
			{
			
				System.out.println("Please input the max speed of this car("+ id + "): ");
				maxSpeed = input.nextFloat();
				if (maxSpeed <= 0)
				{
					System.out.print("Out of range!\n");
				}
			}while (maxSpeed <= 0);
		}
		
		void getturbo(boolean turbo) // 判斷有無渦輪的函式
		{
			isTurbo = turbo;
		}
		void getstatus()// 顯示所有資料的函式
		{
			System.out.println("Car id is " + id + ".");
			System.out.println("Car max speed is " + maxSpeed + "." );
			if (isTurbo == true)
			{
				System.out.println("Car turbo is ture.");
			}
			else
			{
				System.out.println("Car turbo is false.");
			}
		}
}
