package ce1002.a3.s102502005;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int carnumber;
		Car[] carlist;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input the number of cars: ");//要求使用者輸入正確測資。
		carnumber = input.nextInt();
		while(carnumber <= 0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			carnumber = input.nextInt();
		}
			
		carlist = new Car[carnumber];//產生相應長度的陣列。
		
		for(int i=0;i<carnumber;i++)//建立Car物件，並要求使用者輸入各車的maxspeed。
		{
			Car tempcar = new Car(i);//建立一個暫存用的Car物件。
			float tempspeed;
			
			System.out.println("Please input the max speed of this car(" + i + "): ");//要求使用者輸入正確的maxspeed
			tempspeed = input.nextFloat();
			while(tempspeed <= 0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i + "): ");
				tempspeed = input.nextFloat();
			}
			
			tempcar.setmaxspeed(tempspeed);
			tempcar.setisTurbo(true);
			carlist[i] = tempcar;//將暫存區的Car存放到最後要儲存的陣列。	
		}
		
		System.out.println("Output car status.");//印出車輛屬性。
		for(int i=0;i<carnumber;i++)
		{
			System.out.println("Car id is " + carlist[i].getid() + ".");
			System.out.println("Car max speed is " + carlist[i].getmaxspeed() + ".");
			System.out.println("Car turbo is " + carlist[i].getisTurbo() + ".");
		}
	}

}
