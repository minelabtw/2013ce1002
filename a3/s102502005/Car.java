package ce1002.a3.s102502005;

public class Car {
	
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int carid) {	//Constructor
		id = carid;
	}
	
	int getid() {	//傳回id
		return id;
	}
	
	void setmaxspeed(float setmaxspeed) {	//設定maxspeed
		maxSpeed = setmaxspeed;
	}
	
	float getmaxspeed() {	//傳回maxspeed
		return maxSpeed;
	}
	
	void setisTurbo(boolean Turboornot) {	//設定isTurbo
		isTurbo = Turboornot;
	}
	
	boolean getisTurbo() {		//傳回isTurbo
		return isTurbo;
	}
	
}
