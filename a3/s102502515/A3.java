package ce1002.a3.s102502515;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int numCar;
		
		do{//確認輸入值在範圍內，為非零正整數
		System.out.println("Please input the number of cars: ");
		numCar = input.nextInt();
			if(numCar < 1)
			{
				System.out.println("Out of range!");
			}
		}
		while(numCar < 1);
		

		Car cars[] = new Car [numCar];//創造一陣列
		for (int i = 0 ; i < numCar ; i++)//輸入最高速度和是否有渦輪
		{
			cars[i] = new Car(i);//創造一class
			cars[i].setMaxSpeed();//輸入最高速度值
			cars[i].setTurbo(true);//改變渦輪的初始值
			cars[i].getMaxSpeed();//取回最高速度值
			cars[i].getTurbo();//取得是否有渦輪
		}
		
		System.out.println("Output car status.");
		for (int j = 0 ; j < numCar ; j++ )//用for將所有汽車資料輸出
		{
			System.out.println("Car id is " + j + ".");
			System.out.println("Car max speed is " + cars[j].getMaxSpeed() );
			System.out.println("Car turbo is " + cars[j].getTurbo() + "." );
		}
		input.close();//關閉Scanner
	}

}
