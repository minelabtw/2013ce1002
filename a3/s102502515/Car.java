package ce1002.a3.s102502515;
import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car (int number)
	{
		id = number;
	}
	Scanner input = new Scanner(System.in);
	
	public float getMaxSpeed() {//回傳為高速度值
		return maxSpeed;
	}
	
	public void setMaxSpeed() {//設置最高速度值
		
		do{
			System.out.println("Please input the max speed of this car("+ id + "): " );
			maxSpeed = input.nextFloat();
				if(maxSpeed <= 0)
				{
					System.out.println("Out of range!");
				}
			}
			while(maxSpeed <= 0);
	}
	
	public boolean getTurbo() {
		return isTurbo;
	}
	
	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}
	
	
}
