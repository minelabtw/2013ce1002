package ce1002.a3.s102502001;
import java.util.Scanner;
public class A3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int num=0;
		int []id;                  //宣告陣列
		float []spe;
		float speed;
		Car car=new Car();         //宣告一個名稱為car的函式
			
		do{                        //輸入並判斷範圍
			System.out.println("Please input the number of cars: ");
			num = scanner.nextInt();
			if(num<1){
				System.out.println("Out of range!");
			}
	}while(num<1);
		id=new int [num];         //陣列初始化
		spe=new float[num];
		for(int i=0;i<num;i++){   //id[]為其回傳值
			id[i]=car.id(i);
		}
		
		for(int i=0;i<num;i++){
			do{
				System.out.println("Please input the max speed of this car("+i + "): ");
				speed = scanner.nextFloat();
				if(speed<=0){
					System.out.println("Out of range!");
				}
			}while(speed<=0);
			spe[i]=car.maxSpeed(speed);  //spe[]為其回傳值
		}
		
		car.setTurbo();
		
		for(int i=0;i<num;i++){         //輸出字串
		System.out.println("Output car status.");
		System.out.println("Car id is "+id[i]+".");
		System.out.println("Car max speed is "+ spe[i] +".");
	
		if(car.isTurbo()==true){
		System.out.println("Car turbo is true.");
		}
		else{
			System.out.println("Car turbo is false.");
		}
		}
	}
}
