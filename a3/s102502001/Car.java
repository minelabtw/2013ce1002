package ce1002.a3.s102502001;

public class Car {

		private int id;// 賽車id
		private float maxSpeed;// 最高速
		private boolean isTurbo;// 有無渦輪

		public int id(int number){
			id=number;
			return id;
		}
		
		public Car(){
			
		}
		
		public float maxSpeed(float ms){     //將speed放入其中
			maxSpeed=ms;
			return maxSpeed;
		}
		
		public void setTurbo(){             //將所有isTurbo訂為true
			isTurbo=true;
		}
		
		public boolean isTurbo(){
			return isTurbo;
		}
}
