package ce1002.a3.s102502514;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number;
		
		do{
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
			if (number<=0)
			{
				System.out.println("Out of range!");
			}
		}while (number<=0);
		
		Car [] arr = new Car [number];  //創立一個大小為number的陣列arr
		for (int i=0; i<number; i++)
		{
			arr[i] = new Car(i);  //即arr[i]為一個Car物件
			arr[i].setmaxSpeed();  //設定第i台車的最高速度
			arr[i].setisTurbo(true);  //設定第i台車有無渦輪
		}
		
		System.out.println("Output car status.");
		for (int j=0; j<number; j++)
		{
			System.out.println("Car id is " + j + ".");
			System.out.println("Car max speed is " + arr[j].returnmaxSpeed() + ".");  //印出第j台車的最高速度
			System.out.println("Car turbo is " + arr[j].returnisTurbo() + ".");  //印出第j台車有無渦輪
		}
		input.close();
	}
}
