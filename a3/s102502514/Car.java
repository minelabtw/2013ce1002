package ce1002.a3.s102502514;
import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner input = new Scanner(System.in);
	
	Car(int carnumber)
	{
		id = carnumber;
	}
	
	public void setmaxSpeed()
	{
		do{
			System.out.println("Please input the max speed of this car(" + id + "): ");  //請求使用者輸入車輛最高速度
		    maxSpeed = input.nextFloat();  //設定車輛最高速度
		    if (maxSpeed<=0)
		    {
		    	System.out.println("Out of range!");
		    }
		}while (maxSpeed<=0);
	}
	
	public float returnmaxSpeed()
	{
		return maxSpeed;  //回傳最高速度
	}
	
	public void setisTurbo(boolean isTurbo)
	{
		this.isTurbo = isTurbo;  //設定有無渦輪
	}
	
	public boolean returnisTurbo()
	{
		return isTurbo;  //回傳有無渦輪
	}

}
