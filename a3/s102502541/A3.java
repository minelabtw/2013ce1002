package ce1002.a3.s102502541;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numcar = 0;
		Scanner scanner = new Scanner(System.in);
		do{
			System.out.println("Please input the number of cars: ");
			numcar = scanner.nextInt();
			if(numcar<1)
				System.out.println("Out of range !");
		}while(numcar<1);
		Car car[] = new Car[numcar];//宣告一個陣列
		for(int i=0; i<numcar; i++)//設定各台車的maxSpeed和isTurbo
		{
			car[i] = new Car(i);//將物件存入陣列
			float speed = 0;
			do{
			System.out.println("Please input the max speed of this car("+i+"): ");
			speed = scanner.nextFloat();
			car[i].setmaxSpeed(speed);
			if(speed<=0)
					System.out.println("Out of range!");
			}while(speed<=0);
			car[i].setisTurbo();
		}
		System.out.println("Output car status.");
		for(int i=0;i<numcar; i++)//輸出各台車
		{
			System.out.println("Car id is "+car[i].getid());
			System.out.println("Car max speen is "+ car[i].getmaxSpeed());
			System.out.println("Car turbo is "+ car[i].getisTurbo());	
		}
	}
}
