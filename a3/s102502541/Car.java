package ce1002.a3.s102502541;
public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	public Car(int i){ 
		id=i;//初始話id
		
	}
	public void setmaxSpeed(float newms)//更改maxSpeed的值
	{
		maxSpeed = newms;
	}
	public void setisTurbo()//更改isTurbo的值
	{
		isTurbo = true;
	}
	public int getid()//取得id
	{
		return id;
	}
	public float getmaxSpeed()//取得maxSpeed的值
	{
		return maxSpeed;
	}
	public boolean getisTurbo()////取得isTurbo的值
	{
		return isTurbo;
	}

}
