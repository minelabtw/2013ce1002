package ce1002.a3.s995002014;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car(int x){
		id=x;
	}
	public int getID(){
		return id;
	}
	public float getMaxSpeed(){
		return maxSpeed;
	}
	public boolean getIsTurbo(){
		return isTurbo;
	}
	public void setMaxSpeed(float x){
		maxSpeed=x;
	}
	public void setIsTurbo(boolean x){
		isTurbo=x;
	}
	
}
