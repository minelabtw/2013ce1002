package ce1002.a3.s102502034;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	Car(int i) {
		id = i;
	}

	void setspeed(float speed) {
		maxSpeed = speed;
	}

	float getspeed() {

		return maxSpeed;
	}

	void GetIsTurbo() {
		isTurbo = true ;

	}

	boolean SetIsTurbo() {
		return isTurbo ;
	}

	int getId() {
		return id;
	}

}
