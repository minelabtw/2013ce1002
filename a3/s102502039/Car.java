package ce1002.a3.s102502039;

public class Car {
	private int id;// 設立私有變數
	private float maxSpeed;
	private boolean isTurbo;

	public Car() {// 建構子

	}

	public void setId(int x) {// 設定id的值
		id = x;
	}

	public void setSpeed(float y) {// 設定speed的值
		maxSpeed = y;
	}

	public int getId() {// 取得id的值
		return id;
	}

	public float getSpeed() {// 取得speed的值
		return maxSpeed;
	}

	public boolean isIsTurbo() {// isTurbo的值
		return true;
	}

}
