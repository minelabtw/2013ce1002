package ce1002.a3.s102502039;

import java.util.Scanner;
import ce1002.a3.s102502039.Car;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int number;// 設定變數
		System.out.println("Please input the number of cars: ");// 輸出題目
		number = input.nextInt();// 輸入個數
		while (number < 1) {// 進行判斷
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}

		Car[] car = new Car[number];// car在陣列裡的位置
		for (int i = 0; i < number; i++) {
			car[i] = new Car();
		}
		for (int i = 0; i < number; i++) {// 陣列位置
			car[i].setId(i);
		}

		for (int j = 0; j < number; j++) {// 輸入最大速度

			System.out.println("Please input the max speed of this car" + "("
					+ j + "):");
			float speed = input.nextFloat();
			while (speed < 0 || speed == 0) {
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car"
						+ "(" + j + "):");
				speed = input.nextFloat();
			}
			car[j].setSpeed(speed);
		}
		System.out.println("Output car status.");// 輸出結果
		for (int k = 0; k < number; k++) {
			System.out.println("Car id is " + car[k].getId() + ".");
			System.out.println("Car max speed is " + car[k].getSpeed() + ".");
			System.out.println("Car turbo is " + car[k].isIsTurbo() + ".");
		}

	}

}
