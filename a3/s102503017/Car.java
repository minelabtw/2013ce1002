package ce1002.a3.s102503017;

public class Car 
{
	
	private int id;
	private float maxspeed;
	private boolean isTurbo;
	
	Car()
	{
	}
	//default constructor.
	
	Car(float givenspeed, boolean turboDecision, int idSequence)
	{
		maxspeed = givenspeed;
		isTurbo = turboDecision;
		id = idSequence;
	}
	//parameter constructor.
	
	int getId()
	{
		return id;
	}
	//method for getting id of the object.
	
	float getMaxspeed()
	{
		return maxspeed;
	}
	//method for getting the max speed of the object.
	
	boolean getTurbo()
	{
		return isTurbo;
	}
	//method for determine the car require turbo or not.
	
	void setMaxspeed(float incomeData)
	{
		maxspeed = incomeData;
	}
	//method for modifying the speed of the car which was created. 
	
	void setTurbo(boolean incomeData)
	{
		isTurbo = incomeData;
	}
	//method for modifying the turbo system of the car which was created.
	
}//end class
