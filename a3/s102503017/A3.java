package ce1002.a3.s102503017;

import java.util.Scanner;

public class A3 
{
	private static Scanner scanner;
	
	public static void main(String[] args)
	{
		int num;
		float speedSetting;
		
		scanner = new Scanner(System.in);
		System.out.println("Please input the number of cars:");
		do
		{
			num = scanner.nextInt();
			if(num < 1)
			{
				System.out.println("Out of range!\nPlease input the number of cars:");
			}
		}while(num < 1);
		//grabbing quantities of the cars.
		
		Car[] garage = new Car[num];
		//declare an object array named garage.
		
		for(int i = 0; i < num; i++)
		{
			System.out.println("Please input the max speed of this car (" + i +")");
			do
			{
				speedSetting = scanner.nextFloat();
				if(speedSetting <= 0)
				{
					System.out.println("Out of range!\nPlease input the max speed of this car (" + i + ")");
				}
			}while (speedSetting <= 0);
			//grabbing the speed of each car.
			
			Car car = new Car(speedSetting, true,i);
			//create a car object with class Car.
			
			garage[i] = car;
			//put it into the object array.
			
		}//end for
		
		System.out.println("Output car status.");
		for(int i = 0; i < num; i++)
		{
			System.out.println("Car id is " + garage[i].getId() + ".\nCar max speed is " + garage[i].getMaxspeed() + ".\nCar turbo is " + garage[i].getTurbo() + ".");
		}
		//print out the result.
	
	}//end main
}//end class
