package ce1002.a3.s102502035;

import java.util.Scanner;//導入輸入函式庫

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// 建立 scanner class input object
		System.out.println("Please input the number of cars: ");// 提示輸入
		int carnum = input.nextInt();// 輸入
		while (carnum <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");// 再提示輸入
			carnum = input.nextInt();// 輸入
		}
		Car[] cararr = new Car[carnum];// Car class cararr object array
		for (int i = 0; i < carnum; i++) {
			Car car = new Car(i);// Car class car object
			// car[i] = new Car(i);
			System.out.println("Please input the max speed of this car(" + i
					+ "):");// 提示輸入
			float carmax = input.nextFloat();// 輸入
			while (carmax <= 0) {
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("
						+ i + "):");// 提示輸入
				carmax = input.nextFloat();// 輸入
			}
			car.setmax(carmax);// 存最大數
			car.settur();// turbo true
			cararr[i] = car;// 把Car放到陣列裡
		}
		System.out.println("Output car status.");
		for (int i = 0; i < carnum - 1; i++) {
			System.out.println("Car id is " + cararr[i].showid() + ".");// showid
			System.out.println("Car max speen is " + cararr[i].showmax() + ".");// showmaxspeed
			System.out.println("Car turbo is " + cararr[i].showtur() + ".");// showturbo
		}
		System.out.println("Car id is " + cararr[carnum - 1].showid() + ".");// showid
		System.out.println("Car max speen is " + cararr[carnum - 1].showmax()
				+ ".");// showmaxspeed
		System.out.print("Car turbo is " + cararr[carnum - 1].showtur() + ".");// showturbo
	}
}