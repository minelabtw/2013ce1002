package ce1002.a3.s102502035;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int nid) {
		id = nid;
	}

	public int showid() {
		return id;
	}

	public void setmax(float nmax) {
		maxSpeed = nmax;
	}

	public float showmax() {
		return maxSpeed;
	}

	public void settur() {
		isTurbo = true;
	}

	public boolean showtur() {
		return isTurbo;
	}
}
