package ce1002.a3.s102502533;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int carnum;
		Scanner input = new Scanner(System.in);//input how much the car is
		System.out.println("Please input the number of cars:");
		carnum = input.nextInt();
		while (carnum <= 0) {
			System.out.print("Out of range!");
			System.out.println("\nPlease input the number of cars:");
			carnum = input.nextInt();
		}
		Car car = new Car(carnum);//use the class named Car
	}

}
