package ce1002.a3.s102502501;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	Car() {
		id = 0 ;
		maxSpeed = 0 ; //使三者先初始化
		isTurbo = true ;
	}
	
	public void setCar(int castle , float max ) {
		id = castle ;
		maxSpeed = max ;
	}
	
	public int id() {
	   return id;
	}
	
	public float maxSpeed() {
		return maxSpeed ;
	}
	
	public boolean isTurbo() {
		return isTurbo ;
	}
	
	


}