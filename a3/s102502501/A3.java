package ce1002.a3.s102502501;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in) ;
		
		int castle ;
		float max ;
		
		do {
			System.out.println("Please input the number of cars: ") ; //判斷要輸入幾台車,為正整數
			castle = input.nextInt() ;
			if ( castle <= 0 )
				System.out.println("Out of range!") ;		
		}
		while ( castle <= 0 ) ;
		
		Car[] car = new Car[castle] ;
		
		for (int x = 0; x < castle; x++) {
			do {
				System.out.println("Please input the max speed of this car(" + x + "):") ; //判斷每台車的最大速度,為正數
				max = input.nextFloat() ;
				if ( max <= 0 )
					System.out.println("Out of range!") ;		
			}
			while ( max <= 0 ) ;
			
			car[x] = new Car() ; //初始化car[x]  
			car[x].setCar(x , max); //回傳x , max
			
		}
		
		System.out.println("Output car status.") ;
		
		for (int x = 0; x < castle; x++) {
			System.out.println("Car id is " + car[x].id() + "."  ) ;            //輸出結果
			System.out.println("Car max speed is " + car[x].maxSpeed() + "." ) ;
			System.out.println("Car turbo is " + car[x].isTurbo() + "." ) ;  
		}
		

		input.close();

	}
}
