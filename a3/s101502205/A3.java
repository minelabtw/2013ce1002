package ce1002.a3.s101502205;

import java.util.Scanner;

public class A3 {

	private static Scanner input; 
	
	public static void main(String[] args) {
		
		// Create a scanner object
		input = new Scanner( System.in );
		
		int n;
		float speed;
		
		// User input: number of cars
		System.out.println("Please input the number of cars: ");
		n = input.nextInt();
		while(n<=0){
			// Error
			System.out.println("Out of range!");
			// User input
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
		}
		
		Car[] cars = new Car[n];
		
		// User inputs: set all cars
		for(int i=0; i<n; i++){
			// User input: speed of ith car
			System.out.println("Please input the max speed of this car(" + i + "): ");
			speed = input.nextFloat();
			while(speed<=0){
				// Error
				System.out.println("Out of range!");
				// Input again
				System.out.println("Please input the max speed of this car(" + i + "): ");
				speed = input.nextFloat();
			}
			
			// Create ith car
			cars[i] = new Car(i);
			cars[i].setMaxSpeed(speed);
			cars[i].setTurbo(true);
		}
		
		// Output all cars
		System.out.println("Output car status.");
		for(int i=0; i<n; i++){
			System.out.println("Car id is " + cars[i].getID() + ".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].isTurbo() + ".");
		}
		
	}
}