package ce1002.a3.s101502205;

public class Car {
	private int id;// id of car
	private float maxSpeed;// maximum speed
	private boolean isTurbo;// turbo
	
	// Constructor
	public Car(int n){
		id = n;
		isTurbo = true;
	}
	
	// Setters
	public void setMaxSpeed(float v){
		maxSpeed = v;
	}
	public void setTurbo(boolean t){
		isTurbo = t;
	}
	
	// Getters
	public int getID(){
		return id;
	}
	public float getMaxSpeed(){
		return maxSpeed;
	}
	
	public boolean isTurbo(){
		return isTurbo;
	}
}
