package ce1002.a3.s102502519;

public class Car {
	
	private int id;    // 賽車id
	private float maxSpeed;    // 最高速
	private boolean isTurbo;    // 有無渦輪
	public Car(int i){
		id = i;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public boolean isTurbo() {
		return isTurbo;
	}
	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}
	
	
}
