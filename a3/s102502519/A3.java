package ce1002.a3.s102502519;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1=0;
		float num2=0;
		
		Scanner imput = new Scanner(System.in);
		
		do
		{
			System.out.println("Please input the number of cars: ");
			num1 = imput.nextInt();
			if(num1<=0)
				System.out.println("Out of range!");
		}while(num1<=0);
		
		Car car[] = new Car[num1];    //從類別宣告一個car陣列當作很多object
		
		for(int i=0;i<num1;i++){    //請助教幫我解釋........
			car[i] = new Car(i);
		}
		
		for(int j=0;j<num1;j++){
			do
		{
			System.out.println("Please input the max speed of this car(" + j + "): ");
			num2 = imput.nextFloat();
			if(num2<=0)
				System.out.println("Out of range!");
		}while(num2<=0);
			car[j].setMaxSpeed(num2);
			car[j].setTurbo(true);
		}
		System.out.println("Output car status.");
		for(int k=0;k<num1;k++){
			System.out.println("Car id is " + car[k].getId() + ".");
			System.out.println("Car max speed is " + car[k].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + car[k].isTurbo() + "." );
		}
		imput.close();
	}

}
