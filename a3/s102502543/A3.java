package ce1002.a3.s102502543;

import java.util.Scanner;

public class A3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n;
		do {
			System.out.println("Please input the number of cars: "); // 輸出
			n = input.nextInt();
			if (n < 1)
				System.out.println("Out of range!");
		} while (n < 1);
		Car[] car = new Car[n]; // 建立car 陣列
		for (int a = 0; a < n; a++)
			car[a] = new Car(a);
		float ms;
		for (int a = 0; a < n; a++) {
			do {
				System.out.println("Please input the max speed of this car("
						+ car[a].getId() + "): ");
				ms = input.nextFloat(); // 輸入maxspeed
				car[a].setMS(ms); // 存入陣列
				if (ms <= 0)
					System.out.println("Out of range!");
			} while (ms <= 0);
		}
		System.out.println("Output car status.");
		for (int a = 0; a < n; a++) { // 輸出結果
			System.out.println("Car id is " + car[a].getId()
					+ ".\nCar max speed is " + car[a].getMS()
					+ ".\nCar turbo is " + car[a].getIsTurbo() + ".");
		}
	}
}