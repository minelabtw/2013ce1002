package ce1002.a3.s102502543;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	Car(int newId) { //建構
		id = newId;
		isTurbo = true;
	}
	int getId() {
		return id;
	}
	float getMS(){
		return maxSpeed;
	}
	boolean getIsTurbo(){
		return isTurbo;
	}
	void setMS(float newMS){ //setter最高速
		maxSpeed = newMS;
	}
}
