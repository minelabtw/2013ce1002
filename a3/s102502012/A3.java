package ce1002.a3.s102502012;
import java.util.*;

public class A3 {
	
	private static boolean outOfRange(){
		System.out.println("Out of range!");
		return true;
	}
	
	private static void printCars(Car[] cars, int size){ // print all cars of their status respectively
		System.out.println("Output car status.");
		for(int i = 0; i < size; i++){
			System.out.println("Car id is " + cars[i].getID() + ".");
			System.out.println("Car max speen is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].getIsTurbo() + ".");
		}
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num;
		do{
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();

		}while(num <= 0 && outOfRange());
		Car[] cars = new Car[num];
		for(int i = 0; i < num; i++){
			float maxSpeed;
			do{
				System.out.println("Please input the max speed of this car(" + i + "): ");
				maxSpeed = input.nextFloat();
			}while(maxSpeed <= 0 && outOfRange());
			// set all imformation of data
			cars[i].setIsTurbo(true);
			cars[i].setMaxSpeed(maxSpeed);
		}
		printCars(cars, num);
		
		input.close();
	}

}
