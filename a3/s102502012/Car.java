package ce1002.a3.s102502012;

public class Car {
	private int id;
	private float maxSpeed;
	private boolean isTurbo;
	Car(int id){
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
	
	public void setMaxSpeed(float maxSpeed){
		this.maxSpeed = maxSpeed;
	}
	
	public float getMaxSpeed(){
		return maxSpeed;
	}
	
	public void setIsTurbo(boolean isTurbo){
		this.isTurbo = isTurbo;
	}
	
	public boolean getIsTurbo(){
		return isTurbo;
	}
}
