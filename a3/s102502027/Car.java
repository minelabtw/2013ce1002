package ce1002.a3.s102502027;

import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	public Car(int ID) {
		id = ID;  // 使它可以公開取得值
		isTurbo = true;
	}
	public void setMaxSpeed(float speed) { //判斷
		Scanner input1 = new Scanner(System.in);
		System.out
				.println("Please input the max speed of this car(" + id + ")");
		speed = input1.nextInt();
		while (speed <= 0) {
			System.out.println("Out of range!");
			System.out
			.println("Please input the max speed of this car(" + id + ")");
			speed = input1.nextInt(); 
		}
		maxSpeed = speed;
	}
	public void status() { //輸出
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.printf("Car turbo is %b.\n", isTurbo);
	}
}
