package ce1002.a3.s102502027;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int num = 0; //初始
		float speed=0;
		Car c ;
		Scanner input = new Scanner(System.in);
		System.out.println("Please input the number of cars: ");
		num = input.nextInt();
		while (num <= 0) {
			System.out.println("Out of range!"); 
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
		}
		Car car[] = new Car[num]; //宣告陣列
		for(int i=0;i<num;i++){
			car[i]=new Car(i); // 把Car class 東西塞進car陣列
			car[i].setMaxSpeed(speed); //  執行function
		}
		for (int i = 0; i < num; i++) { //輸出
			car[i].status();
		}
	}
}
