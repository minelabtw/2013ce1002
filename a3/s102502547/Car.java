package ce1002.a3.s102502547;

import java.util.Scanner;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner scanner = new Scanner(System.in);

	Car(int x) {
		id = x;
		isTurbo = true;
	}

	void setSpeed() { // 設定速度

		do {
			System.out.println("Please input the max speed of this car(" + id
					+ "):");
			maxSpeed = scanner.nextFloat();
			if (maxSpeed <= 0)
				System.out.println("Out of range!");
		} while (maxSpeed <= 0);
	}

	void setTurbo() { // 設定Turbo
		isTurbo = true;
	}

	float getSpeed() { //取得速度
		return maxSpeed;
	}

	boolean getTurbo() { //取得Turbo
		return isTurbo;
	}

}
