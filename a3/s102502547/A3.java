package ce1002.a3.s102502547;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int a = 0;
		do {
			System.out.println("Please input the number of cars: "); // 輸入車數，範圍不合的話重新輸入
			a = scanner.nextInt();
			if (a < 1)
				System.out.println("Out of range!");
		} while (a < 1);
		
		Car[] x = new Car[a];
		for (int i = 0; i < a; i++) {
			Car car = new Car(i);
			car.setSpeed(); // 設定車速
			car.setTurbo(); // 設定Turbo
			x[i] = car;
		}

		System.out.println("Output car status."); // 輸出
		for (int i = 0; i < a; i++) {
			System.out.println("Car id is " + i + ".");
			System.out.println("Car max speed is " + x[i].getSpeed() + ".");
			System.out.println("Car turbo is " + x[i].getTurbo() + ".");
		}
	}

}
