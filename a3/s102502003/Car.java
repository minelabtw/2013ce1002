package ce1002.a3.s102502003;

public class Car {

	private int id=0;
	private float maxSpeed=0;
	private boolean isTurbo = true;
	
	public Car(){
	}

	public int getId(int numberOfcar) {
		id=numberOfcar;
		return id;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public boolean isTurbo() {
		return isTurbo;
	}

	public void setTurbo(boolean isTurbo) {
		this.isTurbo = isTurbo;
	}
	
	
	
}
