package ce1002.a3.s102502003;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);		
		int quantity = 0;
		int number=0;
		float Max=0;
		boolean turbo=true;
		
		do
		{
			System.out.println("Please input the number of cars: ");
			quantity=scanner.nextInt();
			if(quantity<=0)
				System.out.println("Out of range!");
		}while(quantity<=0);
		
		Car[] c = new Car[quantity];
		
		
		
		for(number=0; number<quantity; number++)
		{
			c[number]=new Car();
			
			do
			{
				System.out.println("Please input the max speed of this car("+number+"): ");
				Max=scanner.nextFloat();
				if(Max<=0)
					System.out.println("Out of range!");
			}while(Max<=0);
			
			c[number].setMaxSpeed(Max);
			c[number].setTurbo(turbo);
			
		}	
				
		System.out.println("Output car status.");
		
		for(number=0; number<quantity; number++)
		{
			System.out.println("Car id is "+c[number].getId(number));
			System.out.println("Car max speed is "+c[number].getMaxSpeed());
			System.out.println("Car turbo is "+c[number].isTurbo());
		}
		
		
		
	}
		
}
