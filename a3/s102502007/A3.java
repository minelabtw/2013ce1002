package ce1002.a3.s102502007;
import java.util.Scanner;

class Car
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	public Car(int number)
	{
		id=number;
	}//constructor
	public void maxSpeed(float maxspeed)
	{
		maxSpeed=maxspeed;
	}//set up a method to store the maxspeed 
	public void isTurbo(boolean turbo)
	{
		isTurbo = turbo;
	}//set up a method to store the turbo attribute
	public void display()
	{
		System.out.println("Car id is " + id +".");
		System.out.println("Car max speed is " + maxSpeed +".");
		System.out.println("Car turbo is " + isTurbo + ".");
	}//diplay the attribute of every object
	public float getMaxSpeed()
	{
		return maxSpeed;
	}//取得maxspeed的method
	public boolean getIsTurbo()
	{
		return isTurbo;
	}//取得isTurbo的method
	public int getID()
	{
		return id;
	}//取得id的method
};

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int amountOfCars;
		float maxspeed;
		do
		{
			System.out.println("Please input the number of cars: ");
			amountOfCars = input.nextInt();
			if(amountOfCars<=0)
				System.out.println("Out of range!");//loop till the correct number is input
		}while(amountOfCars<=0);//get the amount of the cars
		Car[] car = new Car[amountOfCars];//creat a object array
		for(int i=0;i<amountOfCars;i++)
		{
			car[i] = new Car(i);
			System.out.println("Please input the max speed of this car(" + i + "): ");
			maxspeed = input.nextFloat();
			if(maxspeed<=0)
			{
				System.out.println("Out of range!");			
				i--;
				continue;
			}//loop till the correct number is input
			car[i].maxSpeed(maxspeed);
		}
		System.out.println("Output car status.");
		for(int i=0;i<amountOfCars;i++)
		{
			car[i].display();
		}
		input.close();
		}
	}

