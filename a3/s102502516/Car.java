package ce1002.a3.s102502516;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car( int carsNumberPass ) {//建構子
		id = carsNumberPass;
	}
	public void SetMaxSpeed( float speedPass){//設定速度
		maxSpeed = speedPass;	
	}
	public float GetMaxSpeed() {//取得速度
		return maxSpeed;
	}
	public void SetIsTurbo( boolean isTurboPass) {//設定渦輪
		isTurbo = isTurboPass;
	}
	public boolean GetIsTurbo() {//取得渦輪
		return isTurbo;
	}
}
