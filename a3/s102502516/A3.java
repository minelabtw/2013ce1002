package ce1002.a3.s102502516;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
	
		Scanner scanner = new Scanner(System.in); 
		int carsNumber;
		do {
			System.out.println("Please input the number of cars: ");
			carsNumber = scanner.nextInt();
			if( carsNumber <= 0 )
				System.out.println("Out of range!");
		} while ( carsNumber <= 0 );    //決定幾台車

		Car [] cars = new Car[carsNumber];
		float nextMaxSpeed;    //宣告速度
		for (int carsNumberLoop = 0; carsNumberLoop < carsNumber; carsNumberLoop++) {
			cars[carsNumberLoop] = new Car(carsNumberLoop);    //從建構子設定id
			do {
				System.out.println("Please input the max speed of this car(" + carsNumberLoop + "): " );
				nextMaxSpeed = scanner.nextFloat();
				if( nextMaxSpeed <= 0 )
					System.out.println("Out of range!");
			  	} while ( nextMaxSpeed <= 0 );    //決定速度
			
			cars[carsNumberLoop].SetMaxSpeed(nextMaxSpeed);    //設定速度
			cars[carsNumberLoop].SetIsTurbo(true);    //Turbo on!
		}
		
		System.out.println("Output car status.");
		for (int i = 0; i < cars.length; i++) {
			System.out.println("Car id is " + i + ".\nCar max speed is " + cars[i].GetMaxSpeed() + ".\nCar turbo is " + cars[i].GetIsTurbo() + ".");
		}    //印出所有車的屬性
	}
}
