package ce1002.a3.s995002046;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
class A3{
	
	public static void main(String[] args) { 
		int carnum=0;
		Scanner scanner;
		List<Car> cars = new ArrayList<Car>();
		System.out.print("Please input the number of cars:\n");
		while(5>3){//重複迴圈直到輸入範圍的值
			scanner = new Scanner(System.in);
			carnum=scanner.nextInt();
			if(0 < carnum){
				break;
			}
			else
				System.out.print("Out of range !\nPlease input the number of cars:\n");
		}
		for(int i=0; i<=carnum-1 ;i++){//create car and put it in the car list
			cars.add(new Car(i));
		}
		int carid=0;
		float speed=0;
		while(carid<carnum){//input every car's max speed
			System.out.print("input the max speed of this car("+carid+")\n");
			scanner = new Scanner(System.in);
			speed=scanner.nextFloat();
			if(speed>0){
				cars.get(carid).setmaxSpeed(speed);
				carid++;
			}
			else
				System.out.print("Out of range !\nPlease input the max speed of this car("+carid+")\n");
		}
		scanner.close();
		for(Car c:cars){//output all cars's information
			System.out.println("Car id is "+c.getid());
			System.out.println("Car max speed is "+c.getmaxSpeed());
			System.out.println("Car turbo is "+c.isTurbo());
		}
	}

	
}