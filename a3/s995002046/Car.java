package ce1002.a3.s995002046;
class Car{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo=true;// 有無渦輪
	Car(int id){
		this.id=id;
	}
	boolean isTurbo(){//is turbo
		return isTurbo;
	}
	float getmaxSpeed(){//get max speed
		return maxSpeed;
	}
	void setmaxSpeed(float s){//set max speed
		maxSpeed=s;
	}
	int getid(){//get car id
		return id;
	}
}