package ce1002.a3.s102502553;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		int id;
		
		do//確認有幾個ID
		{
		System.out.println("Please input the number of cars:");
		id = input.nextInt();
		if(id <= 0)
			System.out.println("Out of range!");
		}
		while(id <= 0);
		
		Car arr[] = new Car[id];//建立Car物件陣列
		for(int i = 0;i < id;i++)//每個物件的醒能加到該物件
		{
			arr[i] = new Car(i);
			arr[i].MaxSpeed();
			arr[i].turbo();
		}
		
		System.out.println("Output car status.");
		
		for(int j = 0;j < id;j++)//顯示該物件的性能
		{
			arr[j].out();
		}
		
		input.close();//輸入法關閉
		}
}
