package ce1002.a3.s102502549;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {

		int numOfcar;//用來接收輸入的車數
		float maxspeed;//用來接收輸入的車速
		Scanner input = new Scanner(System.in);

		//輸入並檢查numOfcar
		do {

			System.out.println("Please input the number of cars: ");
			numOfcar = input.nextInt();

			if (numOfcar <= 0) {
				System.out.println("Out of range!");
			}

		} while (numOfcar <= 0);

		Car[] cararray = new Car[numOfcar];//建立numOfcar長度的Car物件陣列

		//為每台車設定屬性
		for (int i = 0; i < numOfcar; i++) {

			Car cartemp = new Car(i);

			//輸入並檢查maxspeed
			do {

				System.out.println("Please input the max speed of this car("+ cartemp.getid() + "): ");
				maxspeed = input.nextFloat();

				if (maxspeed <= 0) {
					System.out.println("Out of range!");
				}

			} while (maxspeed <= 0);

			cartemp.setmaxSpeed(maxspeed);//修改車速
			cararray[i] = cartemp;//把物件放進陣列
		}

		System.out.println("Output car status.");
		
		//印出結果
		for (int i = 0; i < numOfcar; i++) {

			System.out.println("Car id is " + cararray[i].getid() + ".");
			System.out.println("Car max speed is " + cararray[i].getmaxSpeed()+ ".");
			System.out.println("Car turbo is " + cararray[i].getisTurbo() + ".");

		}
	}
}
