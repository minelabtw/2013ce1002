package ce1002.a3.s102502549;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	Car(int num) {
		id = num;
		isTurbo = true;
	}

	// 取得id
	public int getid() {
		return id;
	}

	// 設定maxSpeed
	public void setmaxSpeed(float speed) {
		maxSpeed = speed;
	}

	// 取得maxSpeed
	public float getmaxSpeed() {
		return maxSpeed;
	}

	// 設定isTurbo
	public void setisTurbo(boolean b) {
		isTurbo = b;
	}

	// 取得isTurbo
	public boolean getisTurbo() {
		return isTurbo;
	}
}
