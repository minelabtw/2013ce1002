package ce1002.a3.s102502024;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int i)  //建構元
	{
		id=i;
	}
	public void getspeed(float speed)  //設定速度
	{
		maxSpeed=speed;
	}
	public boolean getturbo()  //設定Turbo
	{
		isTurbo=true;
		return isTurbo;
	}
	public float showspeed()  //輸出速度
	{
		return maxSpeed;
	}
	public int showid()
	{
		return id;
	}
		// TODO Auto-generated method stub

}
