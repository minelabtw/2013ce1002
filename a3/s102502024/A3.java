package ce1002.a3.s102502024;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		System.out.println("Please input the number of cars:");  //輸出題目
		Scanner scn=new Scanner(System.in);
		int num;  //宣告變數
		float speed;
		num=scn.nextInt();  //輸入數字
		while(num<=0)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars:");
			num=scn.nextInt();
		}
		Car car[]=new Car[num];  //物件陣列
		for(int i=0;i<num;i++)  
		{
			car[i]=new Car(i);  //存入陣列
		}
		for(int i=0;i<num;i++)  //存入速度
		{
			System.out.println("Please input the max speed of this car("+i+")");
			speed=scn.nextFloat();
			while(speed<0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+")");
				speed=scn.nextFloat();
			}
			car[i].getspeed(speed);
		}
		System.out.println("Output car status.");
		for(int i=0;i<num;i++)  //輸出結果
		{
			System.out.println("Car id is "+car[i].showid()+".");
			System.out.println("Car max speed is "+car[i].showspeed()+".");
			System.out.println("Car turbo is "+car[i].getturbo()+".");
		}
		// TODO Auto-generated method stub
	}

}
