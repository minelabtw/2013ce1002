package ce1002.a3.s102502006;

import java.util.Scanner;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner scanner = new Scanner(System.in);
	
	public Car(int num){
		id = num;
		maxSpeed =0;
		isTurbo =true;
	}
	public void maxSpeed() // 輸入 maxSpeed
	{
		while(maxSpeed<=0){
			System.out.println("Please input the max speed of this car(" +id+ "): ");
			maxSpeed = scanner.nextFloat();
			if(maxSpeed<=0)System.out.println("Out of range!");
		}
	}
	public int getid() // 取得id
	{
		return id;
	}
	public float getmaxSpeed() // 取得maxSpeed
	{
		return maxSpeed;
	}
	public boolean getisTurbo() // 取得isTurbo
	{
		return isTurbo;
	}
}
