package ce1002.a3.s102502006;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		
		int num=0; // 數量
		
		while(num<1){ // 輸入數量
			System.out.println("Please input the number of cars: ");
			num = scanner.nextInt();
			if(num<1)System.out.println("Out of range!");
		}
		
		Car[] car = new Car[num]; // 建立物陣列
		
		for(int i=0;i<num;i++) // 輸入資料
		{
			car[i] = new Car(i);
			car[i].maxSpeed(); // 輸入maxSpeed
		}
		
		System.out.println("Output car status.");
		for(int i=0;i<num;i++){ // 輸出car 資料
			System.out.println("Car id is " + car[i].getid() +".");
			System.out.println("Car max speed is " + car[i].getmaxSpeed() +".");
			System.out.println("Car turbo is " + car[i].getisTurbo() + ".");
		}
	}
}
