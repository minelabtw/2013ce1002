package s102502548;

import java.util.Scanner ;

public class A3 {

	public static void main(String[] args) {
		
		int number=0 ;//宣告變數
		float number2=0 ;
		
		while (true){//用迴圈判斷範圍並輸出
			Scanner sc=new Scanner(System.in) ;
			
			System.out.println("Please input the number of cars:") ;
			
			number=sc.nextInt() ;
			
			if (number<=0){
				System.out.println("Out of range !") ;
			}
			else {
				break ;
			}
		}
		
		
			Car[] car=new Car[number] ;//創造car的物件陣列
		
		
		
		for (int x=0;x<number;x++){//set car的陣列
			car[x]=new Car(x) ;
		
			while (true){
				Scanner sc=new Scanner(System.in) ;
				
				System.out.print("Please input the max speed of this car(") ;
				System.out.print(x) ;
				System.out.println("): ") ;
				
				number2=sc.nextFloat() ;
				
				if (number2<=0){
					System.out.println("Out of range !") ;
				}
				else {
					car[x].maxspeed(number2) ;
					
					break ;
				}
			}
		}
		
		System.out.println("Output car status.") ;
		
		for (int x=0;x<number;x++){//output
				System.out.print("Car id is ") ;
				System.out.println(x) ;
				
				System.out.print("Car max speed is ") ;
				System.out.println(car[x].outputspeed()) ;
				
				System.out.println("Car turbo is "+car[x].outputturbo()) ;
		}

	}

}
