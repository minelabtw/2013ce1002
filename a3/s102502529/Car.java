package ce1002.a3.s102502529;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	public Car(int i){
		id=i;
	}
	public void maxspeed(float s){								//儲存最高速
		maxSpeed=s;
	}
	public float getmaxspeed(){									//取得最高速
		return maxSpeed;
	}
	public boolean getTurbo(){
		isTurbo=true;
		return isTurbo;	
	}

}
