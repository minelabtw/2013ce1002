package ce1002.a3.s102502529;

import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		int num=0;
		do{															//判斷車車
			System.out.println("Please input the number of cars: ");
			num=s.nextInt();
			if(0<num)
				break;
			else
				System.out.println("Out of range! ");
		}while(true);
		Car[] car=new Car[num];													//宣告物件陣列
		for(int i=0;i<num;i++)													//物件實體化
			car[i] = new Car(i);
		for(int i=0;i<num;i++)													//儲存速度
		{
			float speed=0;	
			do{
				System.out.println("Please input the max speed of this car("+i+"): ");
				speed=s.nextFloat();
				if(speed>0)
					break;
				else
					System.out.println("Out of range! ");
			}while(true);
			car[i].maxspeed(speed);
		}
		System.out.println("Output car status.");
		for(int i=0;i<num;i++){												//輸出結果
			System.out.println("Car id is "+i+".");
			System.out.println("Car max speed is "+car[i].getmaxspeed()+".");
			System.out.println("Car turbo is "+car[i].getTurbo()+".");
		}
	}

}
