package ce1002.a3.s102502506;

public class Car {
	private int id=0;//賽車id
	private float maxSpeed;//最高速
	private boolean isTurbo=true;//有無渦輪
	Car(int newid) {
		id = newid;
	}
	public int getId() {
		return id; 
	}
	public float getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(float setmaxSpeed) {
		maxSpeed = setmaxSpeed;
	}
	public boolean isTurbo() {
		return isTurbo;
	}
}
