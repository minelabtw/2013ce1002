package ce1002.a3.s102502506;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		Scanner put = new Scanner(System.in);  //新的Scanner put
		int num;
		float speed;
		do{
			System.out.println("Please input the number of cars: ");
			num = put.nextInt();
			if(num<=0)System.out.println("Out of range! ");
		}while(num<=0);
		int [] arr = new int[num];
		Car Car[] = new Car[num];  //設一個名子為Car Car類別的物件陣列
		for (int i=0;i<num;i++){   //id為此Car在陣列裡的位置(0~num-1)
			Car[i]=new Car(i);
		}
		for (int i=0;i<num;i++){  //從第0台到第num-1台給它它的maxspeed
			do {
				System.out.println("Please input the max speed of this car("+i+"): ");
				speed = put.nextFloat();
				if (speed<=0)
					System.out.println("Out of range!");
			} while(speed<=0);
			Car[i].setMaxSpeed(speed);
		}
		System.out.println("Output car status.");
		for (int i=0;i<num;i++)  //使用函式來輸出輸出id.maxspeed.isturbo
		{
			System.out.println("Car id is "+Car[i].getId()+".");
			System.out.println("Car max speen is "+Car[i].getMaxSpeed()+".");
			System.out.println("Car turbo is "+Car[i].isTurbo()+".");
		}
	}
}
