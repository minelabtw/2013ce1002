package ce1002.a3.s102502015;

import java.util.Scanner;
import ce1002.a3.s102502015.Car;

public class A3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); // 新的SCANNER
		// TODO Auto-generated method stub
		int lenth;			//Car陣列長度
		float speed;		//存速度
		do {
			System.out.println("Please input the number of cars: ");
			lenth = scanner.nextInt();
			if (lenth <= 0)
				System.out.println("Out of range!");
		} while (lenth <= 0);
		Car Car[] = new Car[lenth];			//宣告物件陣列
		for (int i = 0; i < lenth; i++) {	//給定ID
			Car[i]=new Car(i);
		}
		for (int i = 0; i < lenth; i++) {	//設定速度
			do {
				System.out.println("Please input the max speed of this car("+i+"): ");
				speed = scanner.nextFloat();
				if (speed <= 0)
					System.out.println("Out of range!");
			} while (speed <= 0);
			Car[i].setmaxSpeed(speed);		//給car [i] 物件 速度
		}
		System.out.println("Output car status."); //印出
		for (int i=0;i<lenth;i++)
		{
			System.out.println("Car id is "+Car[i].getid()+".");
			System.out.println("Car max speen is "+Car[i].getmaxSpeed()+".");
			System.out.println("Car turbo is "+Car[i].getTurbo()+".");
		}
		scanner.close();
	}

}

