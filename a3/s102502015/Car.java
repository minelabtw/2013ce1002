package ce1002.a3.s102502015;

public class Car {
	private int id=0;// 賽車id
	private float maxSpeed=1;// 最高速
	private boolean isTurbo=true;// 有無渦輪

	Car(int newid) {		//結構子 引導ID
		id = newid;
	}

	public void setmaxSpeed(float setMaxspeed) { //設定速度
		maxSpeed = setMaxspeed;
	}

	public int getid() {						//拿id 渦輪 速度
		return id;
	}
	public boolean getTurbo() {
		return isTurbo;
	}
	public float getmaxSpeed() {
		return maxSpeed;
	}

}
