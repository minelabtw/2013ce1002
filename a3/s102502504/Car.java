package ce1002.a3.s102502504;

import java.util.Scanner;

public class Car 
{
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪

	Car(int num)
	{
		int carname[] = new int [num]; //產生一個型態為int的陣列，名稱叫carname，大小為num
		float carspeed[] = new float [num]; //產生一個型態為flaot的陣列，名稱叫carspeed，大小為num
		
		for(id=0 ; id<num ; id++) 
		{
			do
			{
				carname[id] = id ; //把id存入陣列carname
				System.out.println("Please input the max speed of this car( " + carname[id] + " ): ");
				Scanner speed = new Scanner(System.in); //掃描使用者的輸入
				maxSpeed = speed.nextFloat(); //把掃描的值丟給maxspeed
				if(maxSpeed<=0) //如果maxspeed<=0 則輸出超出範圍
				{
					System.out.println("Out of range !");
				}
			}while(maxSpeed<=0 && id<num); //當maxspeed<=0或是id比num小時 重複執行此迴圈
			carspeed[id] = maxSpeed ; //把每台車的最高速存入陣列carspeed
		}
		
		if(id>=0) //isturbo的值都是true
		{isTurbo = true;}
		
		System.out.println("Output car status.");
		for(id=0 ; id<num ; id++) //依序輸出
		{
			do
			{
				System.out.println("Car id is " + carname[id] + ".");
				System.out.println("Car max speed is " + carspeed[id] + ".");
				System.out.println("Car turbo is " + isTurbo + ".");
			}while(maxSpeed<=0 && id<num);
		}
	}
}
