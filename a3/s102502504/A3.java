package ce1002.a3.s102502504;

import java.util.Scanner;

public class A3 
{
	public static void main(String[] args) 
	{
		int num; //幾台車
		do 
		{
			System.out.println("Please input the number of cars: "); //輸出
			Scanner input = new Scanner(System.in); //掃描使用者的輸入
			num = input.nextInt(); //把掃描到的值丟給num
			if(num<=0) //若num不是飛零整數 則顯示超出範圍
			{
				System.out.println("Out of range!");
			}
		}while(num<=0); //超出範圍就一直重複迴圈
		
		Car cardata = new Car(num); //call car class
	}
}

