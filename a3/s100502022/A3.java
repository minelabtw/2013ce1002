package ce1002.a3.s100502022;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		int numOfCar;
		//number check
		do
		{
			System.out.println("Please input the number of cars: ");
			numOfCar= s.nextInt();
			if(numOfCar<=0)
			{
				System.out.println("Out of range!");
			}
		}while(numOfCar<=0);
		//declare array of objects
		Object[] arrOfCar =new Object[numOfCar];
		//put value to cars one by one
		for(int i=0;i<numOfCar;i++)
		{
			Car car = new Car(i);
			float maxSpeed;
			//speed check
			do
			{
				System.out.println("Please input the max speed of this car("+i+"): ");
				maxSpeed= s.nextFloat();
				if(numOfCar<=0)
				{
					System.out.println("Out of range!");
				}
			}while(maxSpeed<=0);
			// set into value
			car.setmaxSpeed(maxSpeed);
			car.setisTurbo(true);
			//put object into array
			arrOfCar[i]=car;
		}
		//ouput the status
		System.out.println("Output car status.");
		for(int i=0;i<numOfCar;i++)
		{
			System.out.println("Car id is "+((Car) arrOfCar[i]).getId()+".");
			System.out.println("Car max speed is "+((Car) arrOfCar[i]).getmaxSpeed()+".");
			System.out.println("Car turbo is "+((Car) arrOfCar[i]).getisTurbo()+".");
		}
	}

}
