package ce1002.a3.s100502022;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo ;// 有無渦輪
	//constructor
	public Car(){
		this(0);
	}
	public Car(int i){
		this.id=i;
	}
	
	public int getId()
	{
		return id;
	}
	public void setmaxSpeed(float s){
		this.maxSpeed=s;
	}
	public void setisTurbo(boolean is){
		this.isTurbo=is;
	}
	public float getmaxSpeed(){
		return maxSpeed;
	}
	public boolean getisTurbo(){
		return isTurbo;
	}
	
}
