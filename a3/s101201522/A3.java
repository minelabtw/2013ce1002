package ce1002.a3.s101201522;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i,num = 0;//num is the number of cars
		Car[] car;//car array
		float speed = 0;
		do {
			System.out.println("Please input the number of cars: ");
			num = input.nextInt();
			if (num <= 0)
				System.out.println("Out of range!");
		} while (num <= 0);//input car number
		car = new Car[num];
		for(i=0;i<num;i++){
			car[i] = new Car(i);
			do {
				System.out.println("Please input the max speed of this car("+i+"):");
				speed = input.nextFloat();
				if (speed <= 0)
					System.out.println("Out of range!");
			} while (speed <= 0);
			car[i].setmaxSpeed(speed);
			car[i].setisTurbo(true);
		}//set car into array
		System.out.println("Output car status.");//output car status
		for(i=0;i<num;i++){
			System.out.println("Car id is "+car[i].Id()+".");
			System.out.println("Car max speed is "+car[i].MaxSpeed()+".");
			System.out.println("Car turbo is "+car[i].IsTurbo()+".");
		}
		input.close();
	}

}
