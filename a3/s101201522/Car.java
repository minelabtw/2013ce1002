package ce1002.a3.s101201522;

public class Car {
	private int id;//Car id
	private float maxSpeed;//Car max speed
	private boolean isTurbo;//have turbo
	
	Car (int i) {//construct
		id = i;
		maxSpeed = 0;
		isTurbo = false;
	}
	
	public void setmaxSpeed (float speed) {//set car max speed
		maxSpeed = speed;
	}
	
	public void setisTurbo (boolean check) {//set car turbo
		isTurbo = check;
	}
	
	public int Id () {//get car id
		return id;
	}
	
	public float MaxSpeed () {//get car max speed
		return maxSpeed;
	}
	
	public boolean IsTurbo () {//get car turbo
		return isTurbo;
	}
}
