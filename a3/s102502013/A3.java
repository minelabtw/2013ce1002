package ce1002.a3.s102502013;

import java.util.*;

public class A3 {

	public static void main(String[] args) {
		System.out.println("Please input the number of cars: ");
		Scanner input = new Scanner(System.in);
		int n;//declare a variable for car's number
		n = input.nextInt();//input car's number
		while(n<=0){//when the value is not according with condition, output "Out of range!" and repeat input
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			n = input.nextInt();
		}
		Car [] cars = new Car [n];//declare an array 
		float ms = 0;//declare a variable for maxSpeed
		for (int i=0;i<n;i++){
			System.out.println("Please input the max speed of this car(" + i +"): ");
			ms = input.nextFloat();//input maxSpeed
			while(ms<=0){//when the value is not according with condition, output "Out of range!" and repeat input
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i +"): ");
				ms = input.nextFloat();
			}
			cars[i] = new Car(i);//宣告一個實體
			cars[i].setMaxSpeed(ms);//store maxSpeed into array
		}
		System.out.println("Output car status.");
		for(int i =0;i<n;i++){//Output car status
			System.out.println("Car id is "+ i +".");
			System.out.println("Car max speed is " + cars[i].getMaxSpeed() + ".");
			System.out.println("Car turbo is " + cars[i].getIsTurbo() + ".");
		}
	}
}
