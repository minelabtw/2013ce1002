package ce1002.a3.s102502013;

import java.util.Scanner;

public class Car {

	private int id;//declare a variable for car's id
	private float maxSpeed;//declare a variable for car's maxSpeed
	private boolean isTurbo;//declare a variable for car's Turbo
	Car(int carNumber){//constructor for Car class
		id = 0;
		maxSpeed = 0;
		isTurbo = true;
	}
	public void setMaxSpeed(float ms){//set maxSpeed
		maxSpeed = ms;
	}
	public float getMaxSpeed(){//get maxSpeed
		return maxSpeed;
	}
	public boolean getIsTurbo(){//get Turbo
		return isTurbo;
	}
}
