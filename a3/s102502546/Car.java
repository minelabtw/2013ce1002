package ce1002.a3.s102502546;

import java.util.Scanner;

public class Car {

	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	Scanner cin = new Scanner(System.in);

	Car(int i) {//命名id
		id = i;
	}

	void getmaxSpeed() {//讓每台車得到速度
		System.out.println("Please input the max speed of this car(" + id
				+ "): ");
		maxSpeed = cin.nextFloat();
		while (maxSpeed <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the max speed of this car(" + id
					+ "): ");
			maxSpeed = cin.nextFloat();
		}
	}

	void getisTurbo(boolean i) {//確定是否TRUE
		isTurbo = i;
	}

	void showall() {//print出所需
		System.out.println("Car id is " + id + ".");
		System.out.println("Car max speed is " + maxSpeed + ".");
		System.out.println("Car turbo is " + isTurbo + ".");

	}
}
