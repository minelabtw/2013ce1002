package ce1002.a3.s102502546;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);

		int carnum = 0;

		System.out.println("Please input the number of cars: ");
		carnum = cin.nextInt();//需要輸入車數量
		while (carnum <= 0) {
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			carnum = cin.nextInt();
		}

		Car car[] = new Car[carnum];//是讓car[]數量為我輸入的carnum

		for (int i = 0; i < carnum; i++) {//讓每一台車有ID MAXSPEED 跟是否有渦輪
			car[i] = new Car(i);
			car[i].getmaxSpeed();
			car[i].getisTurbo(true);
		}
		System.out.println("Output car status.");
		for (int i = 0; i < carnum; i++) {//顯示出所需!
			car[i].showall();
		}
	}

}
