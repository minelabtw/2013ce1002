package ce1002.a3.s102502503;
import ce1002.a3.s102502503.Car;
import java.util.Scanner;
public class A3 {
	
    private static Scanner scanner;
	public static void main(String[] args) {
		int number;  //宣告整數變數
		scanner = new Scanner( System.in );
		do{  //輸入有幾台車
			System.out.println("Please input the number of cars: ");
			number = scanner.nextInt();
			if (number<=0)
				System.out.println("Out of range!");
		}while (number<=0);
		Car[] car = new Car[number];  //宣告相對應長度的陣列
		
		for (int i=0; i<number; i++)
		{
			float speed;  //宣告變數
			do{  //輸入速度
				System.out.println("Please input the max speed of this car("+i+"): ");
				speed = scanner.nextFloat();
				if (speed<=0)
				{
					System.out.println("Out of range!");
				}
			}while (speed<=0);
			car[i] = new Car(i);  //將id傳入Car建構子
			car[i].setMaxSpeed(speed);  //設定速度值
			car[i].setTurbo(true);  //設定有無渦輪
		}
		System.out.println("Output car status.");
		for (int i=0; i<number; i++)  //輸出每台車屬性
		{
			System.out.println("Car id is "+car[i].getId()+".");
			System.out.println("Car max speed is "+car[i].getMaxSpeed()+".");
			System.out.println("Car turbo is "+car[i].isTurbo()+".");
		}
	}
}