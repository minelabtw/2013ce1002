package ce1002.a3.s102502503;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	public Car(int i){  //建構子
		id = i;
	}
	public Car(){
		
	}
	public int getId() {  //取得id值
		return id;
	}
	public float getMaxSpeed() {  //取得速度值
		return maxSpeed;
	}
	public void setMaxSpeed(float maxSpeed) {  //設定速度值
		this.maxSpeed = maxSpeed;
	}
	public boolean isTurbo() { //取得有無渦輪
		return isTurbo;
	}
	public void setTurbo(boolean isTurbo) {  //設定有無渦輪
		this.isTurbo = isTurbo;
	}
	
}