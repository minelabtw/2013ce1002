package ce1002.a3.s102502552;

import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);//建立掃描機
		int num;//宣告車輛數量的變數
		float Maxspeed = 0;//宣告輸入極速的變數
		
		do{
			System.out.print("Please input the number of cars:");
			num = sc.nextInt();
			if(num < 1)
				System.out.println("Out of range!");
		}while(num < 1);//輸入車輛數，并檢查是否正常
	
		Car car[] = new Car[num];//開始建立車輛物件
		
		for(int i = 0;i < num;i++)
		{
			car[i] = new Car();//利用迴圈輸出多個車輛物件
			car[i].Aboutcar(i);//每輸出一個車輛物件，為其命名并確定是否有渦輪
			car[i].Getmaxspeed(Maxspeed);//每輸出一個車輛物件，設定其極速
		}
		
		System.out.println("Output car status.");
		
		for(int i = 0;i < num;i++)
		{
			car[i].Output();
		}//輸出所有車輛的參數
	
	sc.close();//關閉掃描機
	}
}
