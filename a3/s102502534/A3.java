package ce1002.a3.s102502534;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int number = 0;
		float maxSpeed = 0;
		// 檢查輸入的範圍
		while (number <= 0) {
			System.out.println("Please input the number of cars:");
			number = input.nextInt();
			if (number <= 0) {
				System.out.println("Out of range!");
			}
		}
		//宣告car陣列存放資料
		Car[] car = new Car[number];
		for (int i = 0; i < number; i++) {
			System.out.println("Please input the max speed of this car(" + i
					+ "):");
			maxSpeed = input.nextFloat();
			// 檢查輸入的範圍
			if (maxSpeed <= 0) {
				System.out.println("Out of range!");
				i--;
			} else if (maxSpeed > 0) {
				car[i] = new Car();
				car[i].setCar(i, maxSpeed); // 回傳i和maxSpeed到Car中
			}
		}
		// 輸出結果
		System.out.println("Output car status.");
		for (int i = 0; i < number; i++) {
			System.out.println("Car id is " + car[i].id() + ".");
			System.out.println("Car max speed is " + car[i].maxSpeed() + ".");
			System.out.println("Car turbo is " + car[i].isTurbo() + ".");
		}
		input.close();
	}
}
