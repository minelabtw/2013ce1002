package ce1002.a3.s102502534;

public class Car {
	private int id;// 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
    //初始化各項的值
	Car() {
		id = 0;
		maxSpeed = 0; 
		isTurbo = true;
	}

	public void setCar(int number, float max_Speed) {
		id = number;
		maxSpeed = max_Speed;
	}
	//回傳值
	public int id() {
		return id;
	}

	public float maxSpeed() {
		return maxSpeed;
	}

	public boolean isTurbo() {
		return isTurbo;
	}
}
