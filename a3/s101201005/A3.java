package ce1002.a3.s101201005;
import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		int n=0; // the number of cars
		float m=0; // the max speed of one car
		Scanner input = new Scanner(System.in);	
		System.out.println("Please input the number of cars: ");	
		n=input.nextInt(); //input=n;
		while (n<=0)
		{
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");	
			n=input.nextInt();
		}
		Car []x = new Car [n]; // define a car array 

		for(int i=0; i<n; i++)
		{
			x[i]=new Car(i); //動態產生物件Car，指派給x[i]
			System.out.println("Please input the max speed of this car(" + i + "): ");
			m=input.nextFloat();
			while (m<=0)
			{
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car(" + i + "): ");
				m=input.nextFloat();
			}
			x[i].setmaxSpeed(m);
			x[i].setisTurbo(true);
		}

		
		System.out.println("Output car status.");
		for(int i=0; i<n; i++)
		{
			System.out.println("Car id is "+ x[i].getid() + ".");
			System.out.println("Car max speed is "+ x[i].getmaxSpeed() + ".");
			System.out.println("Car turbo is "+ x[i].getisTurbo() + ".");
		}

	}

}
