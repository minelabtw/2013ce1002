package ce1002.a3.s101201005;

public class Car {
	private int id; // 賽車id
	private float maxSpeed;// 最高速
	private boolean isTurbo;// 有無渦輪
	
	public Car(int n) //建構子過載
	{
		this.id=n;
	}
	public int getid() //其他物件僅能透過此方法得到此物件私有區的(屬性)id值
	{
		return this.id;
	}
	public void setmaxSpeed(float m) //其他物件僅能透過此方法修改此物件私有區的(屬性)maxSpeed
	{
		this.maxSpeed=m;
		return; //need not to return any value
	}
	public float getmaxSpeed()
	{
		return maxSpeed;
	}
	public void setisTurbo(boolean t)
	{
		this.isTurbo=t;
	}
	public boolean getisTurbo()
	{
		return isTurbo;
	}
}
