package ce1002.a3.s102502028;
import java.util.Scanner ;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int number = 1 ;   //宣告
		float maxSpeed = 0 ;
		
		System.out.println("Please input the number of cars: ") ;    //輸入數量
		Scanner cin = new Scanner (System.in) ;
		number = cin.nextInt();
		while (number < 1)
		{
			System.out.println("Out of range!") ;
			System.out.println("Please input the number of cars: ") ;
			number = cin.nextInt();
		}
		
		Car[] carArray = new Car[number] ;  //宣告陣列來儲存車子的屬性
		for (int a = 0 ; a < number ; a++)  //輸入每台車的速度並判斷是否合理
		{
			System.out.println("Please input the max speed of this car("+a+"): ") ;
		    maxSpeed = cin.nextFloat();
		    while (maxSpeed <= 0)
		{
			System.out.println("Out of range!") ;
			System.out.println("Please input the max speed of this car("+a+"): ") ;
			maxSpeed = cin.nextFloat();
        }
		   Car car = new Car(a) ;
		   car.setSpeed(maxSpeed) ;
		   carArray[a] = car ;
		}	
		
		System.out.println("Output car status.") ;
		
		for (int a = 0 ; a < number ; a++)   //輸出每台車的屬性
		{
			System.out.println("Car id is "+carArray[a].getId()+".") ;
			System.out.println("Car max speed is "+carArray[a].getSpeed()+".") ;
			System.out.println("Car turbo is "+carArray[a].getisTurbo()+".") ;
		}	
		
	}

}
