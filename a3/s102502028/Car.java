package ce1002.a3.s102502028;
import java.util.Scanner ;
public class Car {
	
	private int id;// 賽車id
	private float maxSpeed ;// 最高速
	private boolean isTurbo = true;// 有無渦輪
	
	public Car(int a)  //匯入id
	{
		id = a ;
	}
	
	public void setSpeed (float speed)  //設定速度
	{
		maxSpeed = speed ;
	}
	
	public void setisTurbo (boolean a)  //設定渦輪
	{
		isTurbo = a ;
	}
	
	public int getId()  //取得id
	{
		return id ;
	}
	
	public float getSpeed ()  //取得速度
	{
		return maxSpeed ;
	}
	
	public boolean getisTurbo ()  //取得是否有渦輪
	{
		return isTurbo ;
	}
	
}
