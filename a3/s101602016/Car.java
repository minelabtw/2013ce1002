package ce1002.a3.s101602016;

import java.util.Scanner;

public class Car {
	private int id;//賽車id
	private float maxSpeed;//最高速
	private boolean isTurbo=true;//有無渦輪
	private Scanner scanner=new Scanner(System.in);
	
	public int id(int i){//設定車輛的id
			id=i;
			return id;//回傳id
	}
	
	public float maxspeed(int i){
		do{//請使用者輸入車輛的最高速度，如超出範圍則重新輸入
			System.out.println("Please input the max speed of this car("+i+"): ");
			maxSpeed=scanner.nextFloat();
			if(maxSpeed<=0)
				System.out.println("Out of range!");
		}while(maxSpeed<=0);
		return maxSpeed;//回傳最高速
	}
	
	public boolean turbo(){
		return isTurbo;//將每輛車皆設定為有渦輪
	}
	
}
