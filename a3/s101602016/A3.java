package ce1002.a3.s101602016;
import java.util.Scanner;

public class A3 {
	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		Car car=new Car();
		
		int length;
		do{//請使用者輸入車輛，如超出範圍則重新輸入
			System.out.println("Please input the number of cars: ");
			length=scanner.nextInt();
			if(length<1)
				System.out.println("Out of range!");
		}while(length<1);
		//建立三個陣列分別存放id,maxspeed,turbo的數值
		int[] arr=new int[length];
		float[] arr2=new float[length];
		boolean[] arr3=new boolean[length];
		
		for(int i=0;i<length;i++){//將id,maxspeed,turbo的數值存入陣列中
			arr[i]=car.id(i);
			arr2[i]=car.maxspeed(i);
			arr3[i]=car.turbo();
		}
		System.out.println("Output car status.");//顯示輸入結束
		
		for(int j=0;j<length;j++){//輸出車輛的數值
			System.out.println("Car id is "+arr[j]+".\nCar max speed is "+arr2[j]+".\nCar turbo is "+arr3[j]+".");
		}
	}
}
