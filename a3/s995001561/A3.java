package ce1002.a3.s995001561;

import java.util.Scanner;

public class A3 {

	public static void main(String[] args) {
		
		int n;
		float maxspeed;
		
		// input the number of cars
		System.out.println( "Please input the number of cars: " );
		Scanner input = new Scanner(System.in);	
		n = input.nextInt();
		
		while(n<1) {
		System.out.println( "Out of range!\nPlease input the number of cars: " );
		n = input.nextInt();
		}
		
		
		// create car array and assign value
		Car[] carArray = new Car[n];
		
		for(int i=0; i<n; i++) {
			
			carArray[i] = new Car();
			carArray[i].setTurbo(true);
			carArray[i].setId(i);
			
			System.out.println( "Please input the max speed of this car("+ i + "): " );
			maxspeed = input.nextFloat();
			
			while(maxspeed < 0 || maxspeed ==0) {
			System.out.println( "Out of range!\nPlease input the max speed of this car("+ i + "): ");
			maxspeed = input.nextFloat();
			carArray[i].setMaxSpeed(maxspeed);
			}
			
			
			
		}
		
		input.close();
		
		// print output
		System.out.println( "Output car status.");
		
		for(int i=0; i<n; i++) {
			System.out.println( "Car id is " + carArray[i].getId());
			System.out.println( "Car max speed is " + carArray[i].getMaxSpeed());
			System.out.println( "Car turbo is " + carArray[i].isTurbo());
		}
		
		
	}

}
