package ce1002.q1.s102502024;

public class Rectangle extends Shape{
	float width;
	float height;
	public Rectangle()  //建構子
	{
		
	}
	public void setWidth(float width)  //設定寬
	{
		this.width=width;
	}
	public float getWidth()  //回傳寬
	{
		return width;
	}
	public void setHeight(float height)  //設定高
	{
		this.height=height;
	}
	public float getHeight()  //回傳高
	{
		return height;
	}
	public float getArea()  //回傳面積
	{
		return (float)getWidth()*getHeight();
	}
}
