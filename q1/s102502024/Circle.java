package ce1002.q1.s102502024;

public class Circle extends Shape{
	float radius;
	public Circle()  //建構子
	{
		
	}
	public void setRadius(float radius)  //設定半徑
	{
		this.radius=radius;
	}
	public float getRadius()  //回傳半徑
	{
		return radius;
	}
	public float getArea()  //回傳面積
	{
		return (float)(getRadius()*getRadius()*3.14);
	}
}
