package ce1002.q1.s102502024;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		Scanner scn=new Scanner(System.in);
		float height;  //宣告變數
		float width;
		float radius;
		Rectangle r=new Rectangle();  //宣告物件
		Circle c=new Circle();
		System.out.println("Making shapes.");  
		System.out.print("Input the width of the rectangle: ");  //輸出要求
		width=scn.nextFloat();
		while(width<1 || width>20)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width=scn.nextFloat();
		}
		r.setWidth(width);
		System.out.print("Input the height of the rectangle: ");  //輸出要求
		height=scn.nextFloat();
		while(height<1 || height>20)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height=scn.nextFloat();
		}
		r.setHeight(height);
		System.out.print("Input the radius of the circle: ");  //輸出要求
		radius=scn.nextFloat();
		while(radius<1 || radius>20)  //判斷是否符合條件
		{
			System.out.println("Out of range!");
			System.out.print("Input the radius of the rectangle: ");
			radius=scn.nextFloat();
		}
		c.setRadius(radius);
		System.out.println("The area of the rectangle is "+r.getArea());  //輸出結果
		System.out.println("The area of the circle is "+c.getArea());
		System.out.println("The area sum of the shapes is "+(r.getArea()+c.getArea()));
	}
}
