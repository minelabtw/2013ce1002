package ce1002.q1.s101201522;

import java.lang.Math;

public class Circle extends Shape {
	private float radius;
	
	Circle () {
		
	}//construct
	
	public void setRadius (float radius) {//set radius
		this.radius = radius;
	}
	
	public float getRadius () {//get radius
		return radius;
	}
	
	public void setArea () {//calculus area and set
		setArea(radius*radius*(float)Math.PI);
	}
}
