package ce1002.q1.s101201522;

public class Shape {
	private float area;//shape's area
	
	public void setArea (float area) {//set area
		this.area = area;
	}
	
	public float getArea () {//get area
		return area;
	}	
}
