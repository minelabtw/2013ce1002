package ce1002.q1.s101201522;

public class Rectangle extends Shape {
	private float width;
	private float height;
	
	Rectangle () {
		
	}//construct
	
	public void setWidth (float width) {//set width
		this.width = width;
	}
	
	public float getWidth () {//get width
		return width;
	}
	
	public void setHeight (float height) {//set height
		this.height = height;
	}
	
	public float getHeight () {//get height
		return height;
	}
	
	public void setArea () {//calculus area and set
		setArea(width*height);
	}
}
