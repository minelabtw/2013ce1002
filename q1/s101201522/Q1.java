package ce1002.q1.s101201522;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int i,number,choice;
		float width,height,radius,sum;
		System.out.println("Making shapes.");
		
		do {
			System.out.print("Input the number of shapes to be create: ");
			number = input.nextInt();
			if (number<1 || number>10)
				System.out.println("Out of range!");
		} while (number<1 || number>10);//input number
		
		Shape[] shapes = new Shape[number];//set shapes
		
		for (i=0;i<number;i++) {
			do {
				System.out.print("1)Create rectangle, 2)Create circle: ");
				choice = input.nextInt();
				if (choice<1 || choice>2)
					System.out.println("Out of range!");
			} while (choice<1 || choice>2);//input choice of shape
			
			if (choice == 1) {
				Rectangle rectangle = new Rectangle();
				do {
					System.out.print("Input the width of the rectangle: ");
					width = input.nextFloat();
					if (width<1 || width>20)
						System.out.println("Out of range!");
				} while (width<1 || width>20);//input width
				rectangle.setWidth(width);
				
				do {
					System.out.print("Input the height of the rectangle: ");
					height = input.nextFloat();
					if (height<1 || height>20)
						System.out.println("Out of range!");
				} while (height<1 || height>20);//input height
				rectangle.setHeight(height);
				rectangle.setArea();
				
				shapes[i] = rectangle;
			}
			else {
				Circle circle = new Circle();
				do {
					System.out.print("Input the radius of the rectangle: ");
					radius = input.nextFloat();
					if (radius<1 || radius>20)
						System.out.println("Out of range!");
				} while (radius<1 || radius>20);//input radius
				circle.setRadius(radius);
				circle.setArea();
				
				shapes[i] = circle;
			}
		}
		
		sum = 0;
		for (i=0;i<number;i++) {
			System.out.println("The area of the shape is " + shapes[i].getArea());
			sum += shapes[i].getArea();
		}//output area
		System.out.println("The area sum of the shapes is " + sum);

		input.close();
	}

}