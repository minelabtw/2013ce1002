package ce1002.q1.s102502009;

public class Circle extends Shape {

	float radius = 0;

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getRadius() {
		return radius;
	}

	public float getArea() {
		return radius * radius * 3.14f;
	}
}
