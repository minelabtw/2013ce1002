package ce1002.q1.s102502009;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		float width = 0;
		float height = 0;
		float radius = 0;

		Rectangle rectangle = new Rectangle();
		Circle circle = new Circle();

		System.out.println("Making shapes.");

		do { // input
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			if (width < 1 || width > 20) {
				System.out.println("Out of range!");
			}
			rectangle.setWidth(width);
		} while (width < 1 || width > 20);

		do { // input
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			if (height < 1 || height > 20) {
				System.out.println("Out of range!");
			}
			rectangle.setHeight(height);
		} while (height < 1 || height > 20);

		do { // input
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
			if (radius < 1 || radius > 20) {
				System.out.println("Out of range!");
			}
			circle.setRadius(radius);
		} while (radius < 1 || radius > 20);

		// output
		System.out.println("The area of the rectangle is "
				+ rectangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());

		float sum = rectangle.getArea() + circle.getArea();

		System.out.println("The sum of the shapes is " + sum);

		input.close();
	}
}
