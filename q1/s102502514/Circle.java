package ce1002.q1.s102502514;

public class Circle extends Shape{
	private float Radius;
	public void setRadius(float radius){
		Radius = radius;
	}
	public float getRadius(){
		return Radius;
	}
	//Overrides Shape.getArea
	public double getArea(float radius){
		return 3.14* radius* radius;
	}
}
