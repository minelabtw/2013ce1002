package ce1002.q1.s102502514;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.println("Making shapes.");
		//set the width of the rectangle
		System.out.print("Input the width of the rectangle: ");
		float width = input.nextFloat();
		while (width>20 || width<1)
		{
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
		}
		//set the height of the rectangle
		System.out.print("Input the height of the rectangle: ");
		float height = input.nextFloat();
		while (height>20 || height<1)
		{
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
		}
		//set the radius of the circle
		System.out.print("Input the radius of the circle: ");
		float radius = input.nextFloat();
		while (radius>20 || radius<1)
		{
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
		}
		
		Rectangle rectangle = new Rectangle();  //set a Rectangle object
		Circle circle = new Circle();  //set a Circle object
		
		rectangle.setWidth(width);
		rectangle.setHeight(height);
		
		circle.setRadius(radius);
		
		System.out.println("The area of the rectangle is " + rectangle.getArea(rectangle.getWidth(), rectangle.getHeight())); //print rectangle's area
		System.out.println("The area of the circle is " + circle.getArea(circle.getRadius())); //print circle's area
		System.out.print("The area sum of the shapes is " + ( rectangle.getArea(rectangle.getWidth(), rectangle.getHeight()) + circle.getArea(circle.getRadius()) )); //print the area sum of the rectangle and the circle
		
		input.close();
	}

}
