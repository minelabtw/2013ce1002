package ce1002.q1.s102502514;

public class Rectangle extends Shape{
	private float Width;
	private float Height;
	public void setWidth(float width){
		Width = width;
	}
	public float getWidth(){
		return Width;
	}
	public void setHeight(float height){
		Height = height;
	}
	public float getHeight(){
		return Height;
	}
	//Overrides Shape.getArea
	public float getArea(float width, float height){
		return width*height;
	}
	
}
