package ce1002.q1.s101201023;

public class Circle extends Shape
{
	float rad;
	
	public void setRadius(float radius)
	{
		rad = radius;
	}
	
	public float getRadius()
	{
		return rad;
	}
}
