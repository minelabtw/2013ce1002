package ce1002.q1.s101201023;

import java.util.Scanner;

public class Q1 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		float width;
	    float height;
		float radius;
		Scanner input = new Scanner(System.in);
		Rectangle rectangle = new Rectangle();
		Circle circle = new Circle();
		
		System.out.println("Making shapes.");
		System.out.print("Input the width of the rectangle: ");               //輸入矩形長寬及圓半徑
		width = input.nextFloat();
		while(width < 1 || width >20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
		}
		rectangle.setWidth(width);                                            //傳入寬到setWidth
		
		System.out.print("Input the height of the rectangle: ");
		height = input.nextFloat();
		while(height < 1 || height >20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
		}
		rectangle.setHeight(height);                                         //傳入長到setHeight
		
		System.out.print("Input the radius of the circle: ");
		radius = input.nextFloat();
		while(radius < 1 || radius >20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
		}
		circle.setRadius(radius);                                           //傳入半徑到setRadius
		
		System.out.println("The area of the rectangle is " + rectangle.getArea(rectangle.getWidth() , rectangle.getHeight()));                    //回傳值並輸出                
		System.out.println("The area of the circle is " + circle.getArea(circle.getRadius()));
		System.out.println("The area sum of the shapes is " + (rectangle.getArea(rectangle.getWidth() , rectangle.getHeight())+circle.getArea(circle.getRadius())));
	}

}
