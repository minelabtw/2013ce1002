package ce1002.q1.s101201023;

public class Shape 
{
	public float getArea(float width ,float height)
	{
		return width*height;
	}
	
	public double getArea(float radius)
	{
		return radius*radius*3.14;
	}
}
