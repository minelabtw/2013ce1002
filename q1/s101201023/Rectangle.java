package ce1002.q1.s101201023;

public class Rectangle extends Shape
{
	float wid;
	float hei;
	
	public void setWidth(float width)
	{
		wid = width;
	}
	
	public float getWidth()
	{
		return wid;
	}
	
	public void setHeight(float height)
	{
		hei = height;
	}
	
	public float getHeight()
	{
		return hei;
	}
}
