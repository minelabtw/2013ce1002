package ce1002.q1.s102502017;
import java.util.Scanner;


public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		System.out.println("Making shapes.");
		
		Scanner input = new Scanner(System.in);
		//numbers for how many shapes
		int numbers;
		//setting numbers
		while(true){
			System.out.print("Input the number of shapes to be create: ");
			numbers = input.nextInt();
			if(numbers<1 || numbers>10)System.out.println("Out of range!");
			else break;
		}
		
		Shape[] shapes = new Shape[numbers]; 
		
		for(int i=0;i<numbers;i++){
			int _temp;
			//determine whether the shapes are going to create
			while(true){
				System.out.print("1)Create rectngle, 2)Create circle: ");
				_temp = input.nextInt();
				if(_temp<1 || _temp>2)System.out.println("Out of range!");
				else break;
			}
			//creating Rectangle
			if(_temp==1){
				Rectangle rectangle = new Rectangle();
				float temp;
				//set width
				while(true){
					System.out.print("Input the width of the rectangle: ");
					temp = input.nextFloat();
					if(temp<1 || temp >20)System.out.println("Out of range!");
					else {
						rectangle.setWidth(temp);
						break;
					}
				}
				//set height
				while(true){
					System.out.print("Input the height of the rectangle: ");
					temp = input.nextFloat();
					if(temp<1 || temp >20)System.out.println("Out of range!");
					else {
						rectangle.setHeight(temp);
						break;
					}
				}
				shapes[i] = rectangle;
			}
			//creating circle
			else {
				float temp;
				Circle circle = new Circle();
				//set radius
				while(true){
					System.out.print("Input the radius of the circle: ");
					temp = input.nextFloat();
					if(temp<1 || temp >20)System.out.println("Out of range!");
					else {
						circle.setRadius(temp);
						break;
					}
				}
				shapes[i] = circle;
			}
			
		}
		
		float sum=0;
		//out print, counting sum
		for(int i=0;i<numbers;i++){
			System.out.println("The area of the shape is " + shapes[i].getArea());
			sum += shapes[i].getArea();
		}
		
		System.out.println("The area sum of the shapes is " + sum);
		
		
		input.close();
	}

}
