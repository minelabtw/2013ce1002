package ce1002.q1.s102502017;

public class Rectangle extends Shape{
	private float width;
	private float height;
	
	//setting width
	public void setWidth(float x){
		width = x;
	}
	public float getWidth(){
		return width;
	}
	
	//setting height
	public void setHeight(float y){
		height = y;
	}
	
	public float getHeight(){
		return height;
	}
	
	public float getArea(){
		return width*height;
	}
	
}
