package ce1002.q1.s102502555;

public class Rectangle extends Graph{
	
	private double width;  //寬
	private double height;  //長
	
	//初始化長寬
	Rectangle(double weight , double height){
		setEdge(4);
		width = weight;
		this.height = height;
	}
	
	//判斷圖形是否合理
	public boolean Recongnization(){
		if(width < 0 || height < 0){
			return false;
		}else {
			return true;
		}
	}
	
	//回傳面積
	public double getArea(){
		if(Recongnization() == false){
			return 0;
		}else {
			area = width * height;
			return area;
		}
	}
}
