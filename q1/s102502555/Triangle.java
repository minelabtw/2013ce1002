package ce1002.q1.s102502555;

public class Triangle extends Graph{
	double a,b,c;
	
	//初始化三邊
	Triangle(double a , double b , double c){
		setEdge(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	//判斷圖形是否合理
	public boolean Recongnization(){
		if(a < 0 || b < 0 || c < 0){
			return false;
		}else if((a + b) < c || (a + c) < c || (b + c) < a){
			return false;
		}else {
			return true;
		}
	}

	//回傳面積
	public double getArea(){
		if(Recongnization() == false){
			return 0;
		}else {
			double s = (a + b + c) / 2;
			area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
			return area;
		}
	}
}
