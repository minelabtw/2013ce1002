package ce1002.q1.s102502555;

public class Graph {
	
	protected double area;  //面積
	private int edge;  //邊
	
	//設定邊
	public void setEdge(int e){
		edge = e;
	}
	
	//回傳邊
	public int getEdge(){
		return edge;
	}
	
	//判斷圖形是否合理
	public boolean Recongnization(){
		return true;
	}
	
	//回傳面積
	public double getArea(){
		return area;
	}
}
