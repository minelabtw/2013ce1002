package ce1002.q1.s102502016;

public class Rectangle extends Shape {

	Rectangle() {
	}
	private float width;
	private float height;
	
	public void setWidth(float newwidth) {
		width = newwidth;
	}

	public void setHeight(float newheight) {
		height = newheight;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getArea() {
		return width * height;
	}
}