package ce1002.q1.s102502016;


public class Circle extends Shape {
	private float radius;
	Circle() {
	}

	public void setRadius(float newradius) {
		radius = newradius;
	}

	public float getRadius() {
		return radius;
	}

	public float getArea() {
		return (float) (radius * radius * 3.14);
	}
}
