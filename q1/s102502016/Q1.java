package ce1002.q1.s102502016;

import ce1002.q1.s102502016.Shape;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Shape shape[] = new Shape[10];
		int answer, sum;
		System.out.println("Making shapes.");
		System.out.print("Input the number of shapes to be create: ");
		answer = input.nextInt();
		while (answer < 1 || answer > 10) {
			System.out.println("Out of range!");
			System.out.println("Input the number of shapes to be create: ");
			answer = input.nextInt();
		}
		for (int i = 0; i < answer; i++) {
			int choose = 0;
			System.out.print("1)Creat rectangle, 2)Create circle: ");
			choose = input.nextInt();
			while (choose != 1 || choose != 2) {
				System.out.println("Out of range!");
				System.out.print("1)Creat rectangle, 2)Create circle: ");
				choose = input.nextInt();
			}
			if (choose == 1) {
				shape[i] = new Rectangle();
				float width, height;
				System.out.print("Input the width of the rectangle: ");
				width = input.nextFloat();
				while (width < 1 || width > 20) {
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					width = input.nextFloat();
				}
				shape[i].setWidth(width);
				System.out.print("Input the height of the rectangle: ");
				height = input.nextFloat();
				while (height < 1 || height > 20) {
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					height = input.nextFloat();
				}
				shape[i].setHeight(height);
			} else {
				shape[i] = new Circle();
				float radius;
				System.out.print("Input the width of the circle: ");
				radius = input.nextFloat();
				while (radius < 1 || radius > 20) {
					System.out.println("Out of range!");
					System.out.print("Input the radius of the circle: ");
					radius = input.nextFloat();
				}
				shape[i].setRadius();
			}
			for (int i = 0; i < answer; i++) {
				System.out.println("The area of the shape is "
						+ shape[i].getArea());
				sum += shape[i].getArea();
			}
			System.out.println("The area sum of the shapes is " + sum);
		}
		input.close();
	}

}