package ce1002.q1.s102502520;

public class Rectangle extends Shape{
	private float width;
	private float height;
	public Rectangle() //constructor
	{
		 
	}
	public void setWidth(float width) //setter
	{
		this.width = width;
	}
	public float getWidth() //getter
	{
		return width;
	}
	public void setHeight(float height) //setter
	{
		this.height=height;
	}
	public float getHeight() //getter
	{
		return height;
	}
}
