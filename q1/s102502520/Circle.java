package ce1002.q1.s102502520;

public class Circle extends Shape{
	private float radius;
	
	public Circle() //constructor
	{
		
	}
	public void setRadius(float radius) //setter
	{
		this.radius=radius;
	}
	public float getRadius() //getter
	{
		return radius;
	}
}
