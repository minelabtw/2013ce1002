package ce1002.q1.s102502520;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Making Shapes."); //output 
		Scanner cin =new Scanner(System.in);
		float width; //define 
		float height;
		float radius;
		float area;
		Rectangle rectangle=new Rectangle(); //constructor
		do
		{
			System.out.print("Input the width of the rectangle: "); //output
			width=cin.nextFloat(); //input
			if (width>20||width<1) //test
			{
				System.out.println("Out of ranges!");
			}
			rectangle.setWidth(width); 
		}
		while (width>20||width<1);
		do
		{
			System.out.print("Input the height of the rectangle: "); //output
			height=cin.nextFloat(); //input
			if (height>20||height<1) //test
			{
				System.out.println("Out of ranges!");
			}
			rectangle.setHeight(height);
		}
		while (height>20||height<1);
		Circle circle =new Circle(); //constructor
		do
		{
			System.out.print("Input the radius of the circle: "); //output
			radius=cin.nextFloat(); //input
			if (radius>20||radius<1) //test
			{
				System.out.println("Out of ranges!");
			}
			circle.setRadius(radius);
		}
		while (radius>20||radius<1);
		cin.close(); //close Scanner cin
		area = rectangle.getHeight()* rectangle.getWidth(); //calculate the area of rectangle
		rectangle.setArea(area);
		area= (float) (circle.getRadius()*circle.getRadius()*Math.PI); //calculate the area of circle
		circle.setArea(area);
		area = rectangle.getArea()+circle.getArea(); //calculate the area sum
		System.out.println("The area of the rectangle is "+ rectangle.getArea()); //output15
		System.out.println("The area of the circle is "+ circle.getArea());
		System.out.println("The area sum of the shape is "+ area);
	}

}
