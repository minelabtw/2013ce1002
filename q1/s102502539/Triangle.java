package ce1002.q1.s102502539;

import java.math.*;

public class Triangle extends Graph
{
	private double a;
	private double b;
	private double c;
	
	Triangle (double a , double b , double c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge(3);
		Recongnization();
	}
	
	public boolean Recongnization()
	{
		if (a>0 && b>0 && c>0 && a+b>c && b+c>a && a+c>b)
			return super.Recongnization();
		else
			return false;
	}
	
	public double getArea()
	{
		double s = (a+b+c)/2;
		
		if (a>0 && b>0 && c>0 && a+b>c && b+c>a && a+c>b)
			area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		else
			area = 0;
		
		return super.getArea();
	}
	
}
