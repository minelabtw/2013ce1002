package ce1002.q1.s102502018;
import java.util.Scanner;
public class Q1 {

	
	public static void main(String[] args) {
		float w;
		float h;
		float r;
		Scanner scan = new Scanner(System.in);
		System.out.println("Making shapes.");
		do						//set wdith
		{
			System.out.println("Input the width of the rectangle: ");
			w = scan.nextFloat();
			if(w<1||w>20)
			{
				System.out.println("Out of range!");				
			}
		}while(w<1||w>20);
		do						//set height
		{
			System.out.println("Input the height of the rectangle: ");
			h = scan.nextFloat();
			if(h<1||h>20)
			{
				System.out.println("Out of range!");				
			}
		}while(h<1||h>20);
		do						//set radius
		{
			System.out.println("Input the radius of the circle: ");
			r = scan.nextFloat();
			if(r<1||r>20)
			{
				System.out.println("Out of range!");				
			}
		}while(r<1||r>20);
		Rectangle rec;
		rec = new Rectangle();	//建立物件
		rec.setWidth(w);		
		rec.setHeight(h);
		System.out.println("The area of the rectangle is "+rec.getArea(rec.getWidth(), rec.getHeight()));
		Circle cir;
		cir = new Circle();		//建立物件
		cir.setRadius(r);
		System.out.println("The area of the circle is "+cir.getArea(cir.getRadius()));
		Shape shape;
		shape = new Shape();	//建立物件
		System.out.println("The area sum of the shape is "+shape.getsumArea(rec.getArea(w,h),cir.getArea(cir.getRadius())));
		scan.close();	

	}

}
