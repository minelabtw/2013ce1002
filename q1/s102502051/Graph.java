package ce1002.q1.s102502051;

public class Graph {
boolean e;
	protected double area; //存圖的面積
	private int edge; //存圖的邊數
	
	public void setEdge(int t){
		edge=t;
	}    //設定圖的邊數可在建構式就設定
	public int getEdge(){
		return edge;
	}  //回傳圖擁有的邊數
	public boolean Recongnization(){
		if (area<=0 && edge<=0)
		{e=false;}
		else
		{e=true;}
		return e;
	} //回傳該圖是否合理
	public double getArea(){
		return area;
	} //回傳圖的面積大小
}