package ce1002.q1.s102502535;

public class Rectangle extends Graph {

	private double height;
	private double width;

	public Rectangle(double height, double width) {
		setEdge(4);
	} // rectangle's edge is 4

	public boolean Recongnization() {
		if (height <= 0 || width <= 0) {
			return false;
		} else
			return true;
	} // see if the data is reasonable

	public double getArea() {
		return height * width;
	} // return the area
}
