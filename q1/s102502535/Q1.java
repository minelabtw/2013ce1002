package ce1002.q1.s102502535;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in); // create a scanner object and
												// let it parses the standard
												// input.

		double width, height; // variables for rectangle
		double a, b, c; // variables for triangle

		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		width = input.nextDouble(); // input data
		System.out.println("Please input Height: ");
		height = input.nextDouble(); // input data
		Rectangle rectangle = new Rectangle(height, width);

		System.out.println("Second Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble(); // input data
		System.out.println("Please input b: ");
		b = input.nextDouble(); // input data
		System.out.println("Please input c: ");
		c = input.nextDouble(); // input data
		Triangle triangle = new Triangle(a, b, c);

		input.close(); // close input

		System.out.println("Area(0) = " + rectangle.getArea() + " Edge = "
				+ rectangle.getEdge()); // output data
		System.out.println("Area(1) = " + triangle.getArea() + " Edge = "
				+ triangle.getEdge()); // output data
	}

}
