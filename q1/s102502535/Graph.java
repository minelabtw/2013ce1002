package ce1002.q1.s102502535;

public class Graph {

	protected double area;
	private int edge;

	public void setEdge(int e) {
		edge = e;
	} // set the edge

	public int getEdge() {
		return edge;
	} // return the edge

	public double getArea() {
		return this.area;
	} // return the area

}
