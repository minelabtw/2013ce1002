package ce1002.q1.s102502535;

public class Triangle extends Graph {

	private double a;
	private double b;
	private double c;

	Triangle(double a, double b, double c) {
		setEdge(3);
	} // triangle's edge is 3

	public boolean Recongnization() {
		if (a <= 0 || b <= 0 || c <= 0) {
			return false;
		} else if (a + b <= c || a + c <= b || b + c <= a) {
			return false;
		} else
			return true;
	} // see if the data is reasonable

	public double getArea() {
		double s = (a + b + c) / 2;
		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	} // return the area

}
