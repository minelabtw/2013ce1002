package ce1002.q1.s102502531;

public class Rectangle extends Shape { // 繼承
	private float width; // 參數
	private float height;

	// 設定長寬及面積
	public void setWidth(float width) {
		this.width = width;
	}

	public float getWidth() {
		return width;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float setWidth() {
		return height;
	}

	public float getArea() {
		return width * height;
	}
}