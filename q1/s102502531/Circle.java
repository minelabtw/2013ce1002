package ce1002.q1.s102502531;

public class Circle extends Shape { // 繼承
	private float radius; // 參數

	// 設定半徑及面積
	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getRadius() {
		return radius;
	}

	public float getArea() {
		return radius * radius * 3.14f;
	}

}
