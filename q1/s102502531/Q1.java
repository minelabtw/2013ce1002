package ce1002.q1.s102502531;

import java.util.Scanner;

public class Q1 {

	/**
	 * @param args
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float a;
		float b;
		float c;
		System.out.println("Making shapes."); // 程式開始
		Scanner scanner = new Scanner(System.in);
		do // 設定長寬高
		{
			System.out.print("Input the width of the rectangle: ");
			a = scanner.nextFloat();
			if (a < 1.0 || a > 20.0) {
				System.out.println("Out of range!");
			}
		} while (a < 1.0 || a > 20.0);
		do {
			System.out.print("Input the height of the rectangle: ");
			b = scanner.nextFloat();
			if (b < 1.0 || b > 20.0) {
				System.out.println("Out of range!");
			}
		} while (b < 1.0 || b > 20.0);
		do {
			System.out.print("Input the radius of the circle: ");
			c = scanner.nextFloat();
			if (c < 1.0 || c > 20.0) {
				System.out.println("Out of range!");
			}
		} while (c < 1.0 || c > 20.0);
		Circle circle = new Circle(); // 設定class
		Rectangle rectangle = new Rectangle();
		rectangle.setWidth(a); // 傳參數的值
		rectangle.setHeight(b);
		circle.setRadius(c);
		System.out.println("The area of the rectangle is "
				+ rectangle.getArea()); // 印出
		System.out.println("The area of the circle is " + circle.getArea());
		System.out.println("The area sum of the shapes is "
				+ (circle.getArea() + rectangle.getArea()));

	}

}
