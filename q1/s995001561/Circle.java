package ce1002.q1.s995001561;

public class Circle extends Shape{
	
	
	private float Radius;

	// get and set radius
	public float getRadius() {
		return Radius;
	}

	public void setRadius(float radius) {
		Radius = radius;
	}
	
	// get area
	public float getArea() {
		return (float) (Radius * Radius * Math.PI);
		
	}
	

}
