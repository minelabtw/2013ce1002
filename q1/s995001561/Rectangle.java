package ce1002.q1.s995001561;

public class Rectangle extends Shape{
	
	private float Width;
	private float Height;

    // get and set width
	public float getWidth() {
		return Width;
	}

	public void setWidth(float width) {
		Width = width;
	}

	// get and set height
	public float getHeight() {
		return Height;
	}

	public void setHeight(float height) {
		Height = height;
	}
	
	// get Area
	public float getArea() {
		return Width * Height;
	}
	
	
}
