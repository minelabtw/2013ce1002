package ce1002.q1.s995001561;

import java.util.Scanner;

public class Q1 {


	public static void main(String[] args) {
		
		// create variable
		float width;
		float height;
		float radius;
		
		Rectangle rectangle = new Rectangle();
		Circle circle = new Circle();
		
		Scanner input = new Scanner(System.in);
		
		// scan width
		System.out.println("Making shapes.");
		System.out.print("Input the width of the rectangle: ");
		width = input.nextFloat();
		
		while(width<1 || width>20) {
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
		}
		
		// scan height
		System.out.print("Input the height of the rectangle: ");
		height = input.nextFloat();
		
		while(height<1 || height>20) {
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
		}
		
		// scan radius
		System.out.print("Input the radius of the circle: ");
		radius = input.nextFloat();
		
		while(radius<1 || radius>20) {
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
		}
		
		// calculate area
		rectangle.setHeight(height);
		rectangle.setWidth(width);
		circle.setRadius(radius);
		
		// print outcome
		System.out.println("The area of the rectangle is " + rectangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());
		System.out.println("The area sum of the shape is " + (rectangle.getArea() + circle.getArea()) );
		
		input.close();

	}

}
