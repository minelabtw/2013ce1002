package ce1002.q1.s101201508;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//user input the number of Shape 
		System.out.println("Making shapes.");
		Scanner sc=new Scanner(System.in);
		int n;
		System.out.print("Input the number of shapes to be create: ");
		n=sc.nextInt();
		while (n<1 || n>10)
		{
			System.out.println("Out of range!");
			System.out.print("INput the number of shape to be create: ");
			n=sc.nextInt();
		}
		//use a array to Shape
		Shape shape[]=new Shape[n];
		for (int i=0;i<n;i++)
		{
			//user choose the rectangle or the circle
			int choose;
			System.out.print("1)Create rectangle, 2)Create circle: ");
			choose=sc.nextInt();
			while (choose!=1 && choose !=2)
			{
				System.out.println("Out of range!");
				System.out.print("1)Create rectangle, 2)Create circle: ");
				choose=sc.nextInt();
			}
			if (choose==1)//user choose the rectangle
			{
				//use a Rectangle 
				Rectangle R=new Rectangle();
				//input the width 
				float number;
				System.out.print("Input the width of the rectangle: ");
				number=sc.nextFloat();
				while (number<1 || number>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					number=sc.nextFloat();
				}
				R.set_width(number);
				//input height
				System.out.print("Input the height of the rectangle: ");
				number=sc.nextFloat();
				while (number<1 || number>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					number=sc.nextFloat();
				}
				R.set_height(number);
				shape[i]=R;
			}
			else//user choose the circle
			{
				//use a Circle
				Circle C=new Circle();
				//input the radius
				float radius;
				System.out.print("Input the radius of the circle: ");
				radius=sc.nextFloat();
				while (radius<1 || radius>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the radius of the circle: ");
					radius=sc.nextFloat();
				}
				C.set_radius(radius);
				shape[i]=C;
			}
		}
		float sum=0;
		for (int i=0;i<n;i++)//show the area
		{
			System.out.println("The area of the shape is "+shape[i].get_area());
			sum=sum+shape[i].get_area();
		}
		//show the area sum
		System.out.println("The area sum of the shapes is "+sum);
	}

}
