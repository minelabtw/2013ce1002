package ce1002.q1.s101201508;

public class Rectangle extends Shape{
	private float width;
	private float height;
	public void set_width(float in_width)//in width
	{
		width=in_width;
	}
	public float get_width()//out width
	{
		return width;
	}
	public void set_height(float in_height)//in height
	{
		height=in_height;
	}
	public float get_height()//out height
	{
		return height;
	}
	public float get_area()//use the function of Shape and change the method
	{
		return width*height;
	}
}
