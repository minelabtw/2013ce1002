package ce1002.q1.s101201508;

public class Circle extends Shape{
	private float radius;
	public void set_radius(float in_radius)//in radius
	{
		radius=in_radius;
	}
	public float get_radius()//out radius
	{
		return radius;
	}
	public float get_area()//use the function of Shape and change the method
	{
		return (float)(radius*radius*3.14);
	}
}
