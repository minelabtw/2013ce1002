package ce1002.q1.s102502039;

public class Rectangle extends Graph {

	private double height;//declare variable
	private double width;

	public Rectangle() {//constructor

	}

	public Rectangle(double height, double width) {//set variables
		this.height = height;
		this.width = width;
		setEdge(4);
	}

	public boolean Recongnization() {//function
		if (height < 1 || width < 1)
			return false;
		else
			return true;
	}

	public double getArea() {//function
		if (Recongnization() == false)
			return 0;
		else
			return height * width;
	}

}
