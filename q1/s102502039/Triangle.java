package ce1002.q1.s102502039;

public class Triangle extends Graph {

	private double a;//declare variables
	private double b;
	private double c;

	public Triangle() {//constructor

	}

	public Triangle(double a, double b, double c) {//set variable
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge(3);
	}

	public boolean Recongnization() {//function
		if (a < 1 || b < 1 || c < 1)
			return false;
		else if ((a + b) < c || (b + c) < a || (a + c) < b)
			return false;
		else
			return true;
	}

	public double getArea() {//function
		double s = (a + b + c) / 2;
		if (Recongnization() == false)
			return 0;
		else
			return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}

}
