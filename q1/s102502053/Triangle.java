package ce1002.q1.s102502053;

public class Triangle extends Graph {
	
	private double a; //set variables
	private double b;
	private double c;
	private double s;
	
	Triangle(double num1, double num2, double num3) //constructor
	{
		a = num1;
		b = num2;
		c = num3;
	}
	public boolean Recongnization()
	{
		if(a <= 0 || b <= 0 || c <= 0)
		{
			return false;
		}else if((a + b>c && a + c>b && b + c >a))
		{
			return true;
		}else
			return false;
	}
	
	public double getArea() // return area
	{
		if(Recongnization() == true)
		{
			s = (a + b + c) / 2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}else 
			return 0;
	}
	

}
