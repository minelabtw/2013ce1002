package ce1002.q1.s102502053;

public class Rectangle extends Graph {
	
	private double height;
	private double width;
	
	Rectangle(double w, double h) //constructor
	{
		width = w;
		height = h;
	}
	
	public boolean Recongnization() 
	{
		if(width <= 0 || height <= 0)
		{
			return false;
		}else 
			return true;
	}
	
	public double getArea() //return area
	{
		if(Recongnization() == true)
		{
			return width*height;
		}else
			return 0;
	}

}
