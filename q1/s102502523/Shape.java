package ce1001.q1.s102502523;

public class Shape {
	 
	 float height;	//宣告變數
	 float width;	
	 float radius;	
	
	public void setHeight(float height) {//存入值
		this.height = height;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
 
	public float getHeight() {//輸出值
		return this.height;
	}
	public float getWidth() {
		return this.width;
	}
	public float getRadius() {
		return this.radius;
	}	
}
