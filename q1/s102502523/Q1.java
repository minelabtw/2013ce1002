package ce1001.q1.s102502523;
import java.util.*;
public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Making shapes.");
		Scanner input=new Scanner(System.in);
		
		float wid;
		float hei;
		float rad;
		Shape shape=new Shape();
		do {//輸出長寬高
			System.out.println("Input the width of the rectangle: ");
			wid = input.nextFloat();
			if(wid <1 || wid >20){//判斷範圍
				System.out.println("Out of range!");}
		} while (wid <1 || wid >20);
		shape.setWidth(wid);
		do {
			System.out.println("Input the height of the rectangle: ");
			hei = input.nextFloat();
			if(hei <1 || hei >20){//判斷範圍
				System.out.println("Out of range!");}
		} while (hei <1 || hei >20);
		shape.setHeight(hei);
		do {
			System.out.println("Input the radius of the circle: ");
			rad = input.nextFloat();
			if(rad <1 || rad >20){//判斷範圍
				System.out.println("Out of range!");}
		} while (rad <1 || rad >20);
		shape.setRadius(rad);
		Rectangle rec=new Rectangle();
		Circle cir=new Circle();
		System.out.println("The area of the rectangle is "+ rec.getArea());//輸出面積
		System.out.println("The area of the circle is "+ cir.getArea2());
		float sum=rec.getArea()+cir.getArea2();
		System.out.println("The area sum of the shapes is "+sum);
		

	}

}
