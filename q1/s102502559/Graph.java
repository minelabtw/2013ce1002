package ce1002.q1.s102502559;

public class Graph {
	protected double area;
	private int edge;

	public void setEdge(int e) {
		this.edge = e;
	}
//設定邊數
	public int getEdge() {
		return this.edge;
	}
//回傳邊數
	public boolean Recongnization(double a, double b, double c, double weight,double height) {
		if (a <= 0 || b <= 0 || c <= 0 || weight <= 0 || height <= 0)
			return false;
		else if(a+b<=c||a+c<=b||b+c<=a)
			return false;
		else 
			return true;
	}
//判斷輸入值是否正常
	public double getArea() {
		return area;
	};
}
