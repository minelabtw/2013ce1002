package ce1002.q1.s102502559;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// create a rectangle object and a triangle object

		
		// create a scanner object and let it parses the standard input.
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and triangle.
		double weight,height; // variable for rectangle's constructor
		double a,b,c; // variable for triangle's constructor
		
		
		//This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight=input.nextDouble();
		System.out.println("Please input Height: ");
		height=input.nextDouble();
		Graph rectangle = new Graph();//宣告Graph型別的物件
		rectangle =new Rectangle(weight,height);//再宣告此物件型別為Rectangle
		int e =4;
		rectangle.setEdge(e);//設定邊數
		
		
		
		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();
		Graph triangle = new Graph();//宣告型別為Graph的物件
		triangle = new Triangle(a,b,c);//再宣告此物件型別為Triangle\
		int e_2=3;
		triangle.setEdge(e_2);//設定邊數
		System.out.println("Area(0) = "+rectangle.getArea()+" Edge = "+rectangle.getEdge());
		System.out.println("Area(0) = "+triangle.getArea()+" Edge = "+triangle.getEdge());
		
		// print out each area from different shape of graph
				
		
		
	}

}
