package ce1002.q1.s102502547;

public class Rectangle extends Graph {

	private double height;
	private double width;

	Rectangle(double width, double height) {
		this.width = width;
		this.height = height;
		setEdge(4);
		area = width * height; //設定長 寬 邊數 面積
	}

	public boolean Recognization() {
		boolean a = true;
		if (height <= 0 || width <= 0) //回傳該圖是否合理
			a = false;

		return a;
	}

	public double getArea() {
		if (Recognization() == true)
			return area; //如果該圖合理 回傳area
		else
			return 0; //如果不合理 回傳0
	}
}
