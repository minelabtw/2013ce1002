package ce1002.q1.s102502547;

public class Triangle extends Graph {

	private double a;
	private double b;
	private double c;

	Triangle(double a, double b, double c) {
		double s =(a+b+c)/2;
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge(3);
		area = Math.sqrt(s*(s-a)*(s-b)*(s-c)); //設定3邊長 邊數 面積
	}

	public boolean Recognization() {
		boolean x = true;

		if (a + b <= c || a + c <= b || b + c <= a)
			x = false;

		return x; //回傳該圖是否合理
	}

	public double getArea() {
		if (Recognization() == true)
			return area; //如果該圖合理 回傳area
		else
			return 0; //如果不合理 回傳0

	}

}
