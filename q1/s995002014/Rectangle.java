package ce1002.q1.s995002014;

public class Rectangle extends Shape { //�~�� shape
	private float width;
	private float height;
	
	public void setWidth(float width) {
		this.width=width;
	}
	public void setHeight(float height) {
		this.height=height;
	}
	public float getWidth(){
		return this.width;
	}
	public float getHeight(){
		return this.height;
	}
	public float getArea(){ //override
		return width*height;
	}
}
