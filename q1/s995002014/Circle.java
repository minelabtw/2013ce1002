package ce1002.q1.s995002014;

public class Circle extends Shape { //�~��shape
	private float radius;
	public void setRadius(float radius){
		this.radius=radius;
	}
	public float getRadius(){
		return radius;
	}
	public float getArea(){  //override
		return (float) (radius*radius*3.14);
	}
}
