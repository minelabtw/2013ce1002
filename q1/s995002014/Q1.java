package ce1002.q1.s995002014;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		float w,h,r;
		Scanner input = new Scanner(System.in);
		System.out.println("Making Shapes.");
		System.out.print("Input the width of the rectangle: ");
		w=input.nextFloat(); //使用這輸入長方形寬
		while(w<1||w>20){
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			w=input.nextFloat();
		}
		System.out.print("Input the height of the rectangle: ");
		h=input.nextFloat(); //輸入長方形高
		while(h<1||h>20){
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			h=input.nextFloat();
		}
		
		System.out.print("Input the radius of the circle: ");
		r=input.nextFloat(); //輸入圓半徑
		while(r<1||r>20){
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			r=input.nextFloat();
		}
		
		//創建物件
		Rectangle rec = new Rectangle();
		Circle cir = new Circle();
		
		//設定
		rec.setHeight(h);
		rec.setWidth(w);
		cir.setRadius(r);
		
		//輸出
		System.out.println("The area of the rectangle is "+rec.getArea());
		System.out.println("The area of the Circle is "+cir.getArea());
		float sum=rec.getArea()+cir.getArea();
		System.out.println("The area sum of the shapes is "+sum);
		input.close();
	}
}
