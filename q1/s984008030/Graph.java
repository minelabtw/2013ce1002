package ce1002.q1.s984008030;

public class Graph {
	
	protected double area;// Graph's area
	private int edge;// number of graph's edge
	
	public Graph(){
		this.area = 0.0;
		this.edge = 0;
	}
	
	public void setEdge(int e){// setting edge
		this.edge = e;
	}
	
	public int getEdge(){// get edge
		return this.edge;
	}
	
	public boolean Recongnization(){// return whether the graph is reasonable
		// Graph class doesn't have any record of edge measures
		// How could I calculate the area and check whether it is reasonable?
		// Are you kidding me????
		return true;
	}
	
	public double getArea(){// return the area of graph
		return this.area;
	}
	
}
