package ce1002.q1.s984008030;
import ce1002.q1.s984008030.Graph;
import java.lang.Math;

public class Triangle extends Graph{
	
	private double a;
	private double b;
	private double c;// edges of the triangle
	
	public Triangle(double a, double b, double c){
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		double s = (this.a + this.b + this.c) / 2;
		super.area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
		super.setEdge(3);
	}
	
	public boolean Recongnization(){// return whether the triangle is reasonable
		if(a > 0.0 && b > 0.0 && c > 0.0){// all edges is positive
			if(a + b > c && a + c > b && b + c > a){// sum of two edges is bigger then the other one
				return true;
			}
		}
		return false;
	}
	
	public double getArea(){// return the area of Triangle
		if(this.Recongnization()){
			return this.area;
		}
		else{
			return 0.0;
		}
	}
	
}
