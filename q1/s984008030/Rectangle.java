package ce1002.q1.s984008030;
import ce1002.q1.s984008030.Graph;

public class Rectangle extends Graph {
	
	private double height;
	private double width;// height and width of the rectangle
	
	public Rectangle(double width, double height){
		super();
		this.width = width;
		this.height = height;
		super.area = this.width * this.height;
		super.setEdge(4);
	}
	
	public boolean Recongnization(){// return whether the rectangle is reasonable
		if(this.width > 0.0 && this.height > 0.0){// all edges is positive
			return true;
		}
		else{
			return false;
		}
	}
	
	public double getArea(){// return the area of Rectangle
		if(this.Recongnization()){
			return this.area;
		}
		else{
			return 0.0;
		}
	}
	
}
