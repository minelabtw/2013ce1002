package ce1002.q1.s102502007;
import java.util.Scanner;
public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Rectangle rectangle = new Rectangle();
		Circle circle = new Circle();
		float width;
		float height;
		float radius;
		System.out.println("Making shapes.");
		do
		{
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			if(width<1 || width>20)
				System.out.println("Out of range!");
			rectangle.setWidth(width);
		}while(width<1 || width>20);//loop till the correct number is input
		do
		{
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			if(height<1 || height>20)
				System.out.println("Out of range!");
			rectangle.setHeight(height);
		}while(height<1 || height>20);//loop till the correct number is input
		do
		{
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
			if(radius<1 || radius>20)
				System.out.println("Out of range!");
			circle.setRadius(radius);
		}while(radius<1 || radius>20);//loop till the correct number is input
		System.out.println("The area of the rectangle is " + rectangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());
		System.out.print("The area sum of the shapes is " );
		System.out.print(circle.getArea()+rectangle.getArea());
		//display
		input.close();
	}

}
