package ce1002.q1.s102502007;

public class Circle extends Shape {
	public Circle()
	{
		
	}//constructor
	public void setRadius(float radius)
	{
		this.radius=radius;
	}//setter
	public float getRadius()
	{
		return radius;
	}//getter
	public float getArea()
	{
		return radius*radius*(float)Math.PI;
	}
}
