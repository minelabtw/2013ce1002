package ce1002.q1.s102502007;

public class Rectangle extends Shape {
	public Rectangle()
	{
		
	}//constructor
	public void setWidth(float width)
	{
		this.width=width;
	}//setter
	public void setHeight(float height)
	{
		this.height=height;
	}//setter
	public float getWidth()
	{
		return width;
	}//getter
	public float getHeight()
	{
		return height;
	}//getter
	public float getArea()
	{
		return height*width;
	}
}
