package ce1002.q1.s102502041;
import java.lang.Math;
public class Triangle extends Graph {
	Triangle(double x, double y, double z)
	{
		a=x;
		b=y;
		c=z;
		setEdge(3);
	}
	private double a;
	private double b;
	private double c;
	public boolean Recongnization()
	{
		if(a<0||b<0||c<0)return false;
		else if(a+b<c||a+c<b||b+c<a)return false;
		else return true;
	}
	public double getArea()
	{
		if(Recongnization()==true)
		{
			double s=(a+b+c)/2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
		else return 0;
	}
}
