package ce1002.q1.s101201521;

public class Graph {
	protected double area;
	private int edge;
	//constructor with edge
	public Graph(int e){
		setEdge(e);
	}
	//set edge of this graph
	public void setEdge(int e){
		edge = e;
	}
	//return edge of this graph
	public int getEdge(){
		return edge;
	}
	//recongnize if this graph exists
	public boolean Recongnization(){
		return true;
	}
	//return area of this graph
	public double getArea(){
		return 0;
	}
}
