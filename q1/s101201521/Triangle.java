package ce1002.q1.s101201521;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;
	//constructor with the three edge a, b, c of this triangle
	public Triangle(double a, double b, double c){
		super(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	//recongnize if this triangle exists
	public boolean Recongnization(){
		if((a >= 0 && b >= 0 && c >= 0) && (a + b >= c && b + c >= a && a + c >= b))
			return true;
		else
			return false;
	}
	//return area of this triangle
	public double getArea(){
		double s = (a + b + c) / 2.0;
		if(this.Recongnization())
			return Math.sqrt(s * (s - a) * (s - b) * (s - c));
		else
			return 0;
	}
	
}
