package ce1002.q1.s101201521;

public class Rectangle extends Graph{
	private double height;
	private double width;
	//constructor with the Height and Width of this rectangle
	public Rectangle(double height, double width){
		super(4);
		this.height = height;
		this.width = width;
	}
	//recongnize if this rectangle exists
	public boolean Recongnization(){
		if(height >= 0 && width >= 0)
			return true;
		else
			return false;
	}
	//return area of this rectangle
	public double getArea(){
		if(Recongnization())
			return height * width;
		else
			return 0;
		
	}
}
