package ce1002.q1.s102502049;

public class Rectangle extends Graph {
	private double height;
	private double width;
	
	public Rectangle(double x,double y){ // parameter constructor
		width = x;
		height = y;
		super.setEdge(4);
	}
	
	public boolean Recongnization(){
		if (width<0 || height<0)
			return false;
		else
			return true;
	}
	
	public double getArea(){ // override getter
		if(width<0 || height<0){
			return 0;
		}
		else
			return width*height;
	}
}
