package ce1002.q1.s102502049;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c;
	
	public Triangle(double x,double y,double z){ // parameter constructor
		a = x;
		b = y;
		c = z;
		super.setEdge(3);
	}
	
	public boolean Recongnization(){
		if(a<0 || b<0 || c<0)
			return false;
		else if(a+b<c || b+c<a || c+a<b)
			return false;
		else
			return true;
	}
	
	public double getArea(){ // override getter
		if(a+b<c || b+c<a || c+a<b)
			return 0;
		else{
			double s=(a+b+c)/2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));			
		}
		
	}
	
}
