package ce1002.q1.s102502049;

public class Graph {
	protected double area;
	private int edge;
	
	public Graph(){ // default constructor
		
	}
	
	public void setEdge(int e){ // edge setter
		edge = e;
	}
	
	public int getEdge(){ // edge getter
		return edge;
	}
	
	public boolean Recongnization(){ 
		return true;
	}
	
	public double getArea(){ // area getter
		return area;
	}
}
