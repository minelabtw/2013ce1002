package ce1002.q1.s100204006;

public class Circle extends Shape
{
	private float radius;
	
	//setting radius
	public void setRadius(float radius)
	{
		this.radius = radius;
	}
	//getting radius
	public float getRadius()
	{
		return this.radius;
	}
	//getting area
	public float getArea()
	{
		return this.radius*radius;
	}
}
