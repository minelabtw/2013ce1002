package ce1002.q1.s102502023;

public class Rectangle extends Shape {
	private float width; // initialize float width
	private float height; // initialize float height
	public Rectangle() {} // Rectangle constructor
    public void setWidth(float width) {
    	this.width = width;
    }
    
    public float getWidth() {
    	return width;
    }
    
    public void setHeight(float height) {
    	this.height = height;
    }
    
    public float getHeight() {
    	return height;
    }
    
    public float getArea() {
    	return width * height;
    }
}
