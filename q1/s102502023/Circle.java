package ce1002.q1.s102502023;

public class Circle extends Shape {
  private float radius; // initialize float radius
  
  public Circle(){} // Circle constructor
  
  public void setRadius(float radius) {
	  this.radius = radius;
  }
  
  public float getRadius() {
	  return radius;
  }
  
  public float getArea() {
	  return radius * radius * (float)Math.PI;
  }
}
