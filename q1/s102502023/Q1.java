package ce1002.q1.s102502023;

import java.util.Scanner;

public class Q1 extends Shape {
	public static void main(String[] args) {
    Scanner input =  new Scanner(System.in); // initialize Scanner input
    int num; // initialize integer num
    float shape[]; // initialize float array shape
    float sum = 0; // initialize float to 0
  
  System.out.println("Making shapes.");
  do {
	  System.out.print("Input the number of shapes to be create: ");
	  num = input.nextInt();
	  if(num < 1 || num > 10)
		  System.out.println("Out of range!");
  }
  while(num < 1 || num > 10);
  
  shape = new float[num]; // initialize shape's elements
  
  for(int i = 0; i < num; i++) {
	  Rectangle r = new Rectangle();
	  Circle c = new Circle();
	  int num1 = 0; // initialize integer num1
	  do {
		  System.out.print("1)Create rectangle, 2)Create circle: ");
		  num1 = input.nextInt();
		  if(num1 != 1 && num1 != 2)
			  System.out.println("Out of range!");
	  }
	  while(num1 != 1 && num1 != 2);
	  
	  switch (num1) {
	  case 1:
		  float n1 = 0; // initialize float n1
		  do {
			  System.out.print("Input the width of the rectangle: ");
			  n1 = input.nextFloat();
			  if(n1 < 1 || n1 > 20)
				  System.out.println("Out of range!");
		  }
		  while(n1 < 1 || n1 > 20);
		  r.setWidth(n1);
		  
		  float n2 = 0; // initialize float n2
		  do {
			  System.out.print("Input the height of the rectangle: ");
			  n2 = input.nextFloat();
			  if(n2 < 1 || n2 > 20)
				  System.out.println("Out of range!");
		  }
		  while(n2 < 1 || n2 > 20);
		  r.setHeight(n2);
		  
		  shape[i] = r.getArea(); 
		  break;
	  case 2:
		  float r1 = 0; // initialize float r1
		  do {
			  System.out.print("Input the radius of the circle: ");
			  r1 = input.nextFloat();
			  if(r1 < 1 || r1 > 20)
				  System.out.println("Out of range!");
		  }
		  while(r1 < 1 || r1 > 20);
		  c.setRadius(r1);
		  
		  shape[i] = c.getArea();
		  break;
	default:
			  System.out.println("?");
			  break;
	  }
	  
	 
  }
  
  
  for(int i = 0; i < num; i++) {
	  sum += shape[i];
  }
  for(int i = 0; i < num; i++) {
	  System.out.println("The area of the shape is " + shape[i]);
  }
  System.out.println("The area sum of the shapes is " + sum);
  
  input.close(); // close input
}
}