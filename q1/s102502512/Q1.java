package ce1002.q1.s102502512;
import java.util.*;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Making shapes.");
		int n=0;
		int de=0;
		float w=0,h=0,ra=0;
		float total=0;
		do									//let usr to put numder to some shape
		{
			System.out.print("Input the number of shapes to be create: ");
			n = input.nextInt();
			if(n<1||n>10)
			{
				System.out.println("Out of range!");
			}
		}while(n<1||n>10);
		Shape rec[] = new Shape[n];
		for(int i=0;i<n;i++)
		{
		do
		{
			System.out.print("1)Create rectangle, 2)Create circle: ");
			de = input.nextInt();
			if(de!=1 && de!=2)
			
				System.out.println("Out of range!");
			
		}while(de!=1 && de!=2);
		if(de==1)
		{
			rec[i] = new Rectangle();
			do
			{
				System.out.print("Input the width of the rectangle:�@");
				w=input.nextFloat();
				if(w<1||w>20)
				{
					System.out.println("Out of range!");
				}
			}while(w<1||w>20);
			do
			{
				System.out.print("Input the height of the rectangle:�@");
				h=input.nextFloat();
				if(h<1||h>20)
				{
					System.out.println("Out of range!");
				}
			}while(h<1||h>20);
			rec[i].setWidth(w);
			rec[i].setHeight(h);
		}
		else if(de==2)
		{
			rec[i]= new Circle();
			do
			{
				System.out.print("Input the radius of the circle:�@");
				ra=input.nextFloat();
				if(ra<1||ra>20)
				{
					System.out.println("Out of range!");
				}
			}while(ra<1||ra>20);
			rec[i].setRadius(ra);
		}
		}
		for(int i=0;i<n;i++)				//print out the area
		{
			System.out.println("The area of the shape is "+rec[i].getArea());
			total+=rec[i].getArea();
		}
		
		System.out.println("The area sum of the shape is "+total);
	}

}
