package ce1002.q1.s102502512;

public class Circle extends Shape{
	private float radius;
	
	public float getRadius() {			//create some method to calculate
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getArea()
	{
		return super.getArea()+radius*radius;
	}
}
