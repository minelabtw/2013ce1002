package ce1002.q1.s102502512;

public class Rectangle extends Shape{
	private float width;
	private float height;
	public float getWidth() {   //create some method to calculate
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getArea()
	{
		return super.getArea()+width*height;
	}

}
