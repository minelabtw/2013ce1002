package ce1002.q1.s102502544;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;//三角形的三邊
	private double s;
	
	public Triangle(double a,double b,double c){
		this.a=a;
		this.b=b;
		this.c=c;
	}
	public boolean Recognization(){//判斷三個編是否滿足三角形的條件
		if((a<=0 || b<=0 ||c<=0) || (a+b<=c) || (a+c<=b) || (b+c<=a))
			return false;
		else
			return true;
	}
	public double getArea(boolean x){//若不滿足三角形的條件則回傳0，否則回傳三角形的面積
		s=(a+b+c)/2;
		if(x==false){
			return 0;
		}
		else{
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
	}
}
