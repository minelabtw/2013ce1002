package ce1002.q1.s102502544;

public class Rectangle extends Graph{
	private double weight;
	private double height;//矩形的長和寬
	
	public Rectangle(double weight ,double height){
		this.weight=weight;
		this.height=height;
	}
	public boolean Recognization(){//辨別長和寬是否小於0的狀況
		if(weight<=0 || height<=0)
			return false;
		else
			return true;
	}
	public double getArea(boolean x){//若有邊長<0則回傳0，否則回傳矩形面積
		if(x==false){
			return 0;
		}
		else{
			return weight*height;
		}
	}
	
}
