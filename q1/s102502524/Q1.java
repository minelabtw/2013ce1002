package ce1002.q1.s102502524;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		int a = 0;
		
		System.out.println("Making shapes.");		//input how many shapes want to create
		System.out.print("Input the number of shapes to be create: ");
		a = input.nextInt();
		while(a<1 || a>10)
		{
			System.out.println("Out of range!");	//check input in the range
			System.out.print("Input the number of shapes to be create: ");
			a = input.nextInt();
		}
		
		Shape[] s = new Shape[a];
		float[] c = new float[a];					//store the area
		
		for(int i=0;i<a;i++)
		{
			int b = 0;								//choose type
			System.out.print("1)Create rectangle, 2)Create circle: ");
			b = input.nextInt();
			while(b<1 || b>2)
			{
				System.out.println("Out of range!");
				System.out.print("1)Create rectangle, 2)Create circle: ");
				b = input.nextInt();
			}
			
			switch(b)
			{
			case 1:									//rectangle
				s[i] = new Rectangle();
				float width = 0;
				float height = 0;
				System.out.print("Input the width of the rectangle: ");
				width = input.nextFloat();
				while(width<1 || width>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					width = input.nextFloat();
				}
				s[i].setWidth(width);
				
				System.out.print("Input the height of the rectangle: ");
				height = input.nextFloat();
				while(height<1 || height>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					height = input.nextFloat();
				}
				s[i].setHeight(height);
				
				c[i] = s[i].getArea();
				break;
				
			case 2:									//circle
				s[i] = new Circle();
				float radius = 0;
				System.out.print("Input the radius of the rectangle: ");
				radius = input.nextFloat();
				while(radius<1 || radius>20)
				{
					System.out.println("Out of range!");
					System.out.print("Input the radius of the rectangle: ");
					radius = input.nextFloat();
				}
				s[i].setRadius(radius);
				
				c[i] = s[i].getArea();
				break;
			}
		}

		input.close();
		
		float total = 0;
		for(int i=0;i<a;i++)						//print area and count the sum
		{
			System.out.println("The area of the shape is " + c[i]);
			total = total+c[i];
			if(i==(a-1))
			{
				System.out.println("The area sum of the shapes is " + total);
			}
		}
		
	}

}
