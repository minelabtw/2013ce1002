package ce1002.q1.s102502524;

public class Circle extends Shape {

	public void setRadius(float radius)		//set radius
	{
		this.radius = radius;
	}
	
	public float getRadius()				//get radius
	{
		return this.radius;
	}
	
	public float getArea()					//get circle area
	{
		return this.radius*this.radius*(float)Math.PI;
	}
	
}
