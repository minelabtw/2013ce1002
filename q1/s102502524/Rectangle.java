package ce1002.q1.s102502524;

public class Rectangle extends Shape {

	public void setWidth(float width)		//set width
	{
		this.width = width;
	}
	
	public float getWidth()					//get width
	{
		return this.width;
	}
	
	public void setHeight(float height)		//set height
	{
		this.height = height;
	}
	
	public float getHeight()				//get height
	{
		return this.height;
	}
	
	public float getArea()					//get rectangle area
	{
		return this.width*this.height;
	}
}
