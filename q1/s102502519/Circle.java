package ce1002.q1.s102502519;

public class Circle extends Shape{
	Circle()
	{
		
	}
	
	private float radius=0;

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getArea()
	{
		return (float) (radius * radius * 3.14);
	}
}
