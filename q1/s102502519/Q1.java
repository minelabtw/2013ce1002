package ce1002.q1.s102502519;

import java.util.*;

public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Making shapes.");
		float num1=0;
		float num2=0;
		float num3=0;
		
		Scanner scn = new Scanner(System.in);
		
		do
		{
			System.out.print("Input the width of the rectangle: ");
			num1 = scn.nextFloat();
			if(num1<1 || num1>20)
				System.out.println("Out of range!");
		}while(num1<1 || num1>20);    //使值介於1~20
		
		do
		{
			System.out.print("Input the height of the rectangle: ");
			num2 = scn.nextFloat();
			if(num2<1 || num2>20)
				System.out.println("Out of range!");
		}while(num2<1 || num2>20);    //使值介於1~20
		
		do
		{
			System.out.print("Input the radius of the circle: ");
			num3 = scn.nextFloat();
			if(num3<1 || num3>20)
				System.out.println("Out of range!");
		}while(num3<1 || num3>20);    //使值介於1~20
		
		scn.close();
		
		Rectangle rectangle = new Rectangle();    //宣告物件
		rectangle.setWidth(num1);
		rectangle.setHeight(num2);
		
		Circle circle = new Circle();    //宣告物件
		circle.setRadius(num3);
		
		System.out.println("The area of the rectangle is " + rectangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());
		System.out.print("The area sum of the shapes is " + (rectangle.getArea() + circle.getArea()));
	}

}
