package ce1002.q1.s102502536;

public class Rectangle extends Graph {
	
	private double height;  // declare variables
	private double width;
	boolean ans;
	
	public Rectangle(double height , double width) { // constructor
		this.height = height;
		this.width = width;
	}
	
	public boolean Recongnization() {  // find if the rectangle is true or false
		if (height < 0 || width < 0)
			ans = false;
		else
			ans = true;
		return ans;	
	}
	public double getArea() {  // getter of area 
		if(ans == true)
		    return height * width;
		else
			return 0;
	}

}
