package ce1002.q1.s102502536;

import java.lang.Math;

public class Triangle extends Graph {
	
	private double a;  // declare variables , three edges of the triangle
	private double b;
	private double c;
	boolean ans;
	
	public Triangle(double a , double b , double c) { // constructor
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public boolean Recongnization() {  // find if the triangle is true or false		 
		if (a < 0 || b < 0 || c < 0)
			ans = false;
		else if (a >= b && a >= c && a >= b+c)
			ans = false;
		else if (b >= a && b >= c && b >= a+c)
			ans = false;
		else if (c >= a && c >= b && c >= a+b)
			ans = false;
		else
			ans = true;
		return ans;
	}
	public double getArea() { // getter of area
		double s = (a + b + c)/2;
		if(ans == true)
		    return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		else
			return 0;
	}

}
