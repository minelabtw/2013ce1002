package ce1002.q1.s102502026;

class Rectangle extends Shape {
	private float Width;
	private float Height;
	private float Area;

	public Rectangle() {
		
	}

	public void setWidth(float width) {	//get w
		Width = width;
	}

	public void setHeight(float height) {	//get h
		Height = height;
	}

	public float getArea() {	//get area
		Area = Height * Width;
		return Area;
	}
}
