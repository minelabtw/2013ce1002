package ce1002.q1.s102502026;

class Circle extends Shape {
	private float Radius;
	private float Area;

	public Circle() {
		
	}

	public void setRadius(float radius) {	//get r
		Radius = radius;
	}

	public float getArea() {	//get area
		Area = (float) ((Radius*Radius) * (Math.PI));
		return Area;
	}
}
