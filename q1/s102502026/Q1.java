package ce1002.q1.s102502026;

import java.util.Scanner;

public class Q1 {
	public static Scanner input;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Making shapes.");
		input = new Scanner(System.in);
		float w;
		float h;
		float r;
		do {	//asking for weight
			System.out.print("Input the width of the rectangle: ");
			w = input.nextFloat();
			if (w < 1 || w > 20) {
				System.out.println("Out of range!");
			}
		} while (w < 1 || w > 20);
		do {	//asking for height
			System.out.print("Input the height of the rectangle: ");
			h = input.nextFloat();
			if (h < 1 || h > 20) {
				System.out.println("Out of range!");
			}
		} while (h < 1 || h > 20);
		do {	//asking for radius
			System.out.print("Input the radius of the circle: ");
			r = input.nextFloat();
			if (r < 1 || r > 20) {
				System.out.println("Out of range!");
			}
		} while (r < 1 || r > 20);
		Rectangle rect = new Rectangle();
		Circle cir = new Circle();
		rect.setWidth(w);	//set width
		rect.setHeight(h);	//set height
		cir.setRadius(r);	//set radius
		System.out.println("The area of the rectangle is "+rect.getArea());	//output area of rectangle
		System.out.println("The area of the circle is "+cir.getArea());		//output area of circle
		System.out.println("The area sum of the shapes is "+(cir.getArea()+rect.getArea()));	//output sum of the area
		input.close();
	}
}
