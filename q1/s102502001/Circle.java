package ce1002.q1.s102502001;

public class Circle extends Shape{
	private float radius;
	private float pi=314;
	
	
	//get radius
	public void setRadius(float radius){
	this.radius=radius;
	}
	
	//return radius
	public float getRadius(){
	return radius;
	}

	//return area
	public float getArea(){
	return (radius*radius*pi)/100;
	}

}
