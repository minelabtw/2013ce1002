package ce1002.q1.s102502001;

public class Rectangle extends Shape{
	private float width;
	private float height;
	
	//get width and height
	public void setWidth(float width){
		this.width=width;
	}


	public void setHeight(float height){
		this.height=height;
	}

	//return width and height
	public float getWidth(){
		return width;
	}

	public float getHeight(){
		return height;

	}
	//return area
	public float getArea(){
		return width*height;
	}

}
