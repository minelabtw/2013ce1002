package ce1002.q1.s102502001;
import java.util.Scanner;
public class Q1 {

	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		float wid;
		float hei;
		float rad;
		System.out.println("Making shapes.");        //show the text
		
		Rectangle rectangle = new Rectangle();       //declare a new class rectangle
		Circle circle = new Circle();                //declare a new class circle
		do{                                          //judge its range 
			System.out.println("Input the width of the rectangle: ");
			wid=scanner.nextFloat();
			rectangle.setWidth(wid);
			if(wid<1 || wid>20){
				System.out.println("Out of range!");				
			}
		}while(wid<1 || wid>20);

		do{                                          //judge its range
			System.out.println("Input the height of the rectangle: ");
			hei=scanner.nextFloat();
			rectangle.setHeight(hei);
			if(hei<1 || hei>20){
				System.out.println("Out of range!");
			}
		}while(hei<1 || hei>20);
		
		do{                                        //judge its range
			System.out.println("Input the radius of the circle: ");
			rad=scanner.nextFloat();
			circle.setRadius(rad);
			if(rad<1 || rad>20){
				System.out.println("Out of range!");
			}
		}while(rad<1 || rad>20);
		scanner.close();
		//print out the area
		System.out.println("The area of the rectangle is "+rectangle.getArea());
		System.out.println("The area of the circle is "+circle.getArea());
		System.out.println("The area sum of the shapes is "+(rectangle.getArea()+circle.getArea()));
		
		}
	

}
