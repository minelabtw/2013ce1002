package ce1002.q1.s102502534;

public class Triangle extends Graph{

	private double a;
	private double b;
	private double c;
	private double s;
	public Triangle(double a,double b,double c)
	{
		setEdge(3);
		this.a=a;
		this.b=b;
		this.c=c;
	}
	
	/*public boolean Recongnization()
	{
		
	}*/
	public double getArea()
	{
		s=(a+b+c)/2;
		if(a<0 || b<0 ||c<0)
		{
			return 0;
		}
		else if(a+b<=c ||a+c<=b ||b+c<=a)
		{
			return 0;
		}
		else
		{
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
	}
}
