package ce1002.q1.s102502534;

public class Rectangle extends Graph {

	private double height;
	private double width;
	
	public Rectangle(double height,double width)
	{
		setEdge(4);
		this.height=height;
		this.width=width;
	}
	
	/*public boolean Recongnization()
	{
		
	}*/
	public double getArea()
	{
		if(height<0 || width<0)
		{
			return 0;
		}
		else
		{
			return height*width;
		}
	}
}
