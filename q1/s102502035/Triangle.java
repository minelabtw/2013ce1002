package ce1002.q1.s102502035;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c;

	public Triangle(double a, double b, double c) {// constructor
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge(3);// 3
	}

	public boolean Recongnization() {// function Recongnization
		if (a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0)
			return true;
		else
			return false;
	}

	public double getArea() {// function getArea
		double s = (a + b + c) / 2;
		return area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}
}
