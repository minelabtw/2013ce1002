package ce1002.q1.s102502515;
import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		System.out.println("Making shapes.");
		Scanner input = new Scanner(System.in);//輸入
		float w, h, r;
		int num , choice;//形狀數量和選擇
		float sum = 0 ;//總面積總和
		
		do{//確認輸入數量在範圍內
			System.out.print("Input the number of shapes to be create: ");
			num = input.nextInt();
					if (num < 1 || num > 10)
					{
						System.out.println("Out of range!");
					}	
		}
		while(num < 1 || num > 10);
		
		Shape shape[] = new Shape[num];//宣告形狀物件陣列
		
		for (int i = 0 ; i < num ; i++)//陣列中個別去問使用者
		{
			do{//確認輸入在選賊內
				System.out.print("1)Create rectangle, 2)Create circle: ");
				choice = input.nextInt();
				if (choice !=1 && choice!=2)
				{
					System.out.println("Out of range!");
				}
				
			}
			while (choice !=1 && choice!=2);
			
			if (choice == 1)//選擇長方形
			{
				shape[i] = new Rectangle();//改變成長方形物件
				
				do{//確認輸入值在範圍內
				System.out.print("Input the width of the rectangle: ");
				w = input.nextFloat();
				if (w < 1 || w > 20)
				{
					System.out.println("Out of range!");
				}
				}
				while (w < 1 || w > 20);
				
				
				do{//確認輸入值在範圍內
				System.out.print("Input the height of the rectangle: ");
				h = input.nextFloat();
				if (h < 1 || h > 20)
				{
					System.out.println("Out of range!");
				}
				}
				while (h < 1 || h > 20);
				
				shape[i].setWidth(w);//set寬
				shape[i].setHeight(h);//set長
			}
			
			else 
			{
				shape[i] = new Circle();//改變成圓形物件
			
				do{//確認輸入值在範圍內
				System.out.print("Input the radius of the circle: ");
				r = input.nextFloat();
				if (r < 1 || r > 20)
				{
					System.out.println("Out of range!");
				}
				}
				while (r < 1 || r > 20);
				shape[i].setRadius(r);//set半徑
			}
		}
	
		for (int i = 0 ; i < num ; i++)//迴圈，輸出面積
		{
			System.out.println("The area of the shape is " + shape[i].getArea());//輸出面積
			sum = sum + shape[i].getArea();
		}
		System.out.println("The area sum of the shapes is " + sum);//輸出總和
		input.close();//關掉輸入
	}

}
