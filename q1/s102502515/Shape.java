package ce1002.q1.s102502515;

public class Shape {//以下method都會被override
	protected float width;
	protected float height;
	protected float radius;

	Shape(){
	}
	public float getArea(){
		return 0;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
}
