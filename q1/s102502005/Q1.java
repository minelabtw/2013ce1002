package ce1002.q1.s102502005;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		int shapenum;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Making shapes.");//要求使用者輸入欲建立的形狀數。
		System.out.print("Input the number of shapes to be create: ");
		shapenum = input.nextInt();
		while(shapenum<1 || shapenum>10){
			System.out.println("Out of range!");
			System.out.print("Input the number of shapes to be create: ");
			shapenum = input.nextInt();
		}
		
		Shape[] shapelist = new Shape[shapenum];//建立一個Shape型態的陣列。
		
		for(int i=0;i<shapenum;i++){  //將設定完的型態存入。
			int shapetype;
			float a;
			
			System.out.print("1)Create rectangle, 2)Create circle: ");
			shapetype = input.nextInt();
			while(shapetype!=1 && shapetype!=2 ){
				System.out.println("Out of range!");
				System.out.print("1)Create rectangle, 2)Create circle: ");
				shapetype = input.nextInt();
			}
			
			switch (shapetype){
			
			case 1:
				shapelist[i] = new Rectangle();
				
				System.out.print("Input the width of the rectangle: ");
				a =input.nextFloat();
				while(a<1 || a>20 ){
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					a = input.nextFloat();
				}
				((Rectangle) shapelist[i]).setWidth(a);
				
				System.out.print("Input the height of the rectangle: ");
				a =input.nextFloat();
				while(a<1 || a>20 ){
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					a = input.nextFloat();
				}
				((Rectangle) shapelist[i]).setHeight(a);
				break;
				
			case 2:
				shapelist[i] = new Circle();
				
				System.out.print("Input the radius of the circle: ");
				a =input.nextFloat();
				while(a<1 || a>20 ){
					System.out.println("Out of range!");
					System.out.print("Input the radius of the circle: ");
					a = input.nextFloat();
				}
				((Circle) shapelist[i]).setRadius(a);
				break;
				
			default: 
				break;
			}
		}
		
		input.close();
		
		float totalarea=0;
		
		for(int i=0;i<shapenum;i++){//顯示形狀的面積及面積總和。
			System.out.println("The area of the shape is " + shapelist[i].getArea());
			totalarea = totalarea + shapelist[i].getArea();  
		}
		
		System.out.println("The area sum of the shapes is " + totalarea);
	}

}
