package ce1002.q1.s102502005;

public class Circle extends Shape{

	public float radius;
	
	public void setRadius(float radius){
		this.radius = radius;
	}
	
	public float getRadius(){
		return this.radius;
	}
	
	public float getArea(){
		return (float) (radius*radius*3.14);
	}
}
