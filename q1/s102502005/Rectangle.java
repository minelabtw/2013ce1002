package ce1002.q1.s102502005;

public class Rectangle extends Shape{

	public float width;
	public float height;
	
	public void setWidth(float width){
		this.width = width;
	}
	
	public float getWidth(){
		return width;
	}
	
	public void setHeight(float height){
		this.height = height;
	}
	
	public float getHeight(){
		return height;
	}
	
	public float getArea(){
		return this.width*this.height;
	}
	
}
