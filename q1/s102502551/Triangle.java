package ce1002.q1.s102502551;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;
	
	public Triangle(double d,double e,double f){				//set
		a=d;
		b=e;
		c=f;
		setEdge(3);
		Recongnization();
		
	}
	public boolean Recongnization(){							
		if(a<=0){
			return false;
		}
		else if(b<=0){
			return false;
		}
		else if(c<=0){
			return false;
		}
		else if(a+b<=c){
			return false;
		}
		else if(c+b<=a){
			return false;
		}
		else if(a+c<=b){
			return false;
		}
		else{
			return true;
		}
	}
	public double getArea(){								//get
		double s=(a+b+c)/2;
		if(Recongnization()==true){
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
		else{
			return 0;
		}
	}
}
