package ce1002.q1.s102502562;

public class Rectangle extends Graph{
	private double height;
	private double width;//矩形的長和寬
	public Rectangle(double h,double w)//建構子初始化邊長和邊數
	{
		super.setEdge(4);
		height=h;
		width=w;
	}
	public boolean Recongnization()//判斷是否符合矩形的定義
	{
		if(height<0 || width<0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public double getArea(boolean graph)//計算面積和回傳面積
	{
		if(graph==false)
		{
			return area;
		}
		else
		{
			area=height*width;
			return area;
		}
	}
}
