package ce1002.q1.s102502562;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;//三角形的三邊
	public Triangle(double A,double B,double C)//建構子初始化邊長和邊數
	{
		super.setEdge(3);
		a=A;
		b=B;
		c=C;
	}
	public boolean Recongnization()//判斷是否符合三角形的定義
	{
		if(a<0 || b<0 || c<0 || a+b<c || a+c<b || b+c<a)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public double getArea(boolean graph)//計算面積和回傳面積
	{
		double s;
		if(graph==false)
		{
			return area;
		}
		else
		{
			s=(a+b+c)/2;
			area=Math.sqrt(s*(s-a)*(s-b)*(s-c));
			return area;
		}
	}

}
