package ce1002.q1.s992001026;

public class Triangle extends Graph{
	private double a ;
	private double b ; 
	private double c ;
	
	// set a , b & c 
	public Triangle ( double a1 , double b1 , double c1 ){
		a = a1 ;
		b= b1 ;
		c = c1 ;
		
	}
	
	// recongnize that is a triangle or not 
	public boolean Recongnization(){
		if ( a < 0 || b< 0 ||c < 0 ){
			return false ;
		}else if (a > b && a > c ){
			if (a < b+c ) return true ;
			else return false ;
		}else if ( b > a && b > c ){
			if (b < a+c ) return true ;
			else return false ;
		}else if ( c > a && c > b ){
			if (c < b+a ) return true ;
			else return false ;
		}else return false ;
	}
	
	// count the area
	public double getArea(){
		
		boolean bo ; 
		
		bo = Recongnization() ;
		
		if (bo == true ){ 
		double s = ( a+b +c)/2 ;
		area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		return area ;
		}else return 0. ;
		
	}
	
	

}
