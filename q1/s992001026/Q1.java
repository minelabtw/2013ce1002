package ce1002.q1.s992001026;
import java.util.*;

public class Q1 {
	public static void main(String[] args) {
	
			// create a rectangle object and a triangle object
			
			Rectangle rectangle ;
			Triangle triangle ;
		
			// create a scanner object and let it parses the standard input.
			Scanner input = new Scanner(System.in);
			// create temporary variables for creating objects such as rectangle and triangle.
			double weight,height; // variable for rectangle's constructor
			double a,b,c; // variable for triangle's constructor
			
			
			//This part is handling the input to construct the rectangle object
			System.out.println("First Graph is rectangle");
			System.out.println("Please input Weight: ");
			weight=input.nextDouble();
			System.out.println("Please input Height: ");
			height=input.nextDouble();
		    rectangle =new Rectangle(weight,height);
			
			
			//This part is handling the input to construct the triangle object		
			System.out.println("First Graph is triangle");
			System.out.println("Please input a: ");
			a = input.nextDouble();
			System.out.println("Please input b: ");
			b = input.nextDouble();
			System.out.println("Please input c: ");
			c = input.nextDouble();
			triangle = new Triangle(a,b,c);
			
			
			// print out each area from different shape of graph
			rectangle.setEdge(4);
			System.out.println("Area(0) = " +rectangle.getArea()+" Edge = " + rectangle.getEdge());
			triangle.setEdge(3);
			System.out.println("Area(1) = " +triangle.getArea()+" Edge = " + triangle.getEdge());
	}

}
