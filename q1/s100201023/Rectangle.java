package ce1002.q1.s100201023;

public class Rectangle extends Graph
{
	private double height;
	private double width;
	
	//constructure
	public Rectangle(double x , double y)
	{
		//set properties
		height = x;
		width = y;
		super.setEdge(4);
		
		//compute area
		area = height * width;
	}
	
	//method
	public boolean Recongnization()
	{
		if(super.Recongization())
			if(height > 0 && width > 0)
				return true;
		
		return false;
	}
	
	public double getArea()
	{
		if(Recongnization())
			return area;
		else
			return 0;
	}
	
}
