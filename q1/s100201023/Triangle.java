package ce1002.q1.s100201023;

public class Triangle extends Graph
{
	private double a;
	private double b;
	private double c;
	
	//constructure
	public Triangle(double x , double y , double z)
	{
		//set properties
		a = x;
		b = y;
		c = z;
		super.setEdge(3);
		
		//compute area
		double s = (a + b + c) / 2;
		area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}
	
	//method
	public boolean Recongnization()
	{
		if(super.Recongization())
			if(a > 0 && b > 0 && c > 0)
				if((a + b) > c && (a + c) > b && (b + c) > a)
					return true;
		
		return false;
	}
	
	public double getArea()
	{
		if(Recongnization())
			return area;
		else
			return 0;
	}
	
	
}
