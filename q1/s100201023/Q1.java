package ce1002.q1.s100201023;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// create a rectangle object and a triangle object
		Graph[] polygon = new Graph[2];
		
		// create a scanner object and let it parses the standard input.
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and triangle.
		double weight,height; // variable for rectangle's constructor
		double a,b,c; // variable for triangle's constructor
		
		
		//This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight=input.nextDouble();
		System.out.println("Please input Height: ");
		height=input.nextDouble();
		polygon[0] =new Rectangle(weight,height);
		
		
		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();
		polygon[1] = new Triangle(a,b,c);
		
		
		// print out each area from different shape of graph
		for(int i = 0 ; i < 2 ; ++i)
		{
			System.out.print("Area(" + i + ") = " + polygon[i].getArea());
			System.out.println(" Edge = " + polygon[i].getEdge());
		}
		
		
	}

}
