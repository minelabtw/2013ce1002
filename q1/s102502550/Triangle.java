package ce1002.q1.s102502550;

public class Triangle extends Graph{
    private double a;                                              //變數宣告
    private double b;
    private double c;
    
    public Triangle(double a,double b,double c){                   //建構子
    	setEdge(3);
    	this.a = a;
    	this.b = b;
    	this.c = c;
    }
    public boolean Recongnization(){                               //確認府和規則
    	if (a+b>c && b+c>a && a+c>b && a>=0 && b>=0 && c>=0){
    		return true;
    	}
    	else
    		return false;
	}
	public double getArea(){                                       //get面積
		if(Recongnization()){
			double s = (a+b+c)/2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
		else
			return 0;
		
	}
    
}
