package ce1002.q1.s102502008;
import java.util.Scanner ;
import java.util.Random ;
public class Q1 
{
	public static void main(String[] args) 
	{
		Scanner input= new Scanner(System.in) ;
		System.out.println("Making shapes.");
		int number ;
		do
		{
			System.out.println("Input the number of shapes to be creat: ");
			number= input.nextInt();
			if(number<1 || number>10)
				System.out.println("Out of range!");
		}while(number<1 || number>10);
		//get the number
		Shape[] shapes= new Shape[number] ;
		int choose ;
		float vaule ;
		for(int i= 0; i< 3; i++)
		{
			do
			{
				System.out.println("1)Creat rectangle, 2)Creat circle: ");
				choose=input.nextInt() ;
				if(choose<1 || choose >2)
					System.out.println("Out of range!");
			}while(choose<1 || choose >2) ;
			//choose the shape
			if(choose==1)
			{
				Rectangle newOne= new Rectangle() ;
				do
				{
					System.out.print("Input the width of the rectangle: ");
					vaule= input.nextFloat();
					if(vaule<1||vaule>20)
						System.out.println("Out of range!");
				}while(vaule<1||vaule>20);
				//set width
				newOne.setWidth(vaule);
				do
				{
					System.out.print("Input the hight of the rectangle: ");
					vaule= input.nextFloat();
					if(vaule<1||vaule>20)
						System.out.println("Out of range!");
				}while(vaule<1||vaule>20);
				//set hight
				newOne.setHight(vaule);
				shapes[i]=newOne ;
			} //make a rectangle
			else
			{
				Circle newOne= new Circle() ; 
				do
				{
					System.out.print("Input the radius of the circle: ");
					vaule= input.nextFloat();
					if(vaule<1||vaule>20)
						System.out.println("Out of range!");
				}while(vaule<1||vaule>20);
				//setradius
				newOne.setRadius(vaule) ;
				shapes[i]=newOne ;
			}//make a circle
		}
		//creat
		float sum=0f ;
		for(int i= 0; i< 3; i++)
		{
			sum+=shapes[i].getArea() ;
			System.out.println("The area of the shape is "+ shapes[i].getArea());
			
		}
		System.out.println("The area sum of the shape is "+ sum);
		//print
		input.close();
	}

}
