package ce1002.q1.s102502557;

public class Triangle extends Graph{
private double a;
private double b;
private double c;//三角形的三邊

public Triangle(double a, double b, double c)
{
	this.a = a; this.b = b; this.c = c;
	super.setEdge(3);
	super.getEdge();
}

public boolean Recongnization()//判斷三個邊是否滿足三角形的條件 或有邊長<0
{
	boolean T = true;
	if ( a<=0 || b<=0 || c<=0 )
     T = false ;
	else if ( a+b<=c || b+c<=a || a+c<=b )
	 T=false;
	else 
	 T=true;
	
	return T;
}
public double getArea()//若不滿足三角形的條件0 否則回傳三角型的面積
{
	if (Recongnization()==false)
	{
		return 0.0f;
	}
	else
	{
	double s = (a+b+c)/2;
	return Math.sqrt(s*(s-a)*(s-b)*(s-c));
	}
}
	
}
