package ce1002.q1.s102502557;

public class Rectangle extends Graph{
   private double height;
   private double width;//長寬

public Rectangle(double height, double width)//constructor
{
	this.height = height;
	this.width = width;
	super.setEdge(4);
	super.getEdge();
	
}
   
public boolean Recognization()//長寬是否有小餘0
{  
	boolean B = true;
	if (height<=0 || width<=0)
		B = false;
	else
		B = true;
	return B;
}

public double getArea()//若有邊長< 0則回傳0，否則回傳矩形面積
{
	if ( Recognization() == false)
	{
		return 0.0f;
	}
	else 
	{
		return height*width;
	}
}
}
