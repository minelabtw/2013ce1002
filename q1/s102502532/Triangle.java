package ce1002.q1.s102502532;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c;

	public Triangle(double a1, double b1, double c1) {
		a = a1;
		b = b1;
		c = c1;
		super.setEdge(3);
	}

	public boolean Recongnization() { // 判斷

		if (a + b <= c || b + c <= a || a + c <= b) {            //兩邊和大於第三邊
			return false;
		}

		if (a < 0 || b < 0 || c < 0) {
			return false;
		} else {
			return true;
		}
	}

	public double getArea() {

		if (this.Recongnization() == true) {
			double s = 0;
			s = (a + b + c) / 2;
			return Math.sqrt(s * (s - a) * (s - b) * (s - c)); // 面積
		} else {
			return 0;
		}
	}
}
