package ce1002.q1.s102502031;

public class Rectangle extends Graph {
	private double height;
	private double width;

	public Rectangle() {
		this(0, 0);
	}

	public Rectangle(double height, double width) {
		this.height = height;
		this.width = width;
	}

	public boolean Recongnization() {
		if (height <= 0) // height must be positive
			return false;
		else if (width <= 0) // width must be positive
			return false;
		else
			return true;
	}

	public double getArea() {
		if (this.Recongnization())
			return height * width;
		else
			return 0;
	}
}
