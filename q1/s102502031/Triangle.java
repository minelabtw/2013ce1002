package ce1002.q1.s102502031;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c;

	public Triangle() {
		this(0, 0, 0);
	}

	public Triangle(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public boolean Recongnization() {
		if (a <= 0) // a must be positive
			return false;
		else if (b <= 0) // b must be positive
			return false;
		else if (c <= 0) // c must be positive
			return false;
		else if (a + b <= c)
			return false;
		else if (a + c <= b)
			return false;
		else if (b + c <= a)
			return false;
		else
			return true;
	}

	public double getArea() {
		if (this.Recongnization()) {
			double s = (a + b + c) / 2;
			return Math.sqrt(s * (s - a) * (s - b) * (s - c));
		} else {
			return 0;
		}
	}
}
