package ce1002.q1.s102502552;

public class Rectangle extends Graph{
	private double weight;
	private double height;
	
	public Rectangle(double weight,double height)
	{
		this.weight = weight;
		this.height = height;
	}//長方形建構子先傳入長與寬
	
	public int getEdge()
	{
		return 4;
	}//變數為4
	
	public boolean Recongnization()
	{
		if(weight > 0 && height > 0)//長與寬大於0合理
		{
			super.judge = true;
		}
		else
		{
			super.judge = false;
		}
		
		return judge;
	}//判斷是否合理
	
	public double getArea()
	{
		if(judge == false)
		{
			return 0;//不合理為0
		}
		else
		{
			return weight * height;
		}
	}//取得面積
	
	
}
