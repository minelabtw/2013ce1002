package ce1002.q1.s102502552;

public class Triangle extends Graph {
	
	private double a;
	private double b;
	private double c;
	
	public Triangle(double a,double b,double c)
	{
		this.a = a;
		this.b = b;
		this.c = c;
	}//三角形建構子先傳入三個邊長
	
	public int getEdge()
	{
		return 3;
	}//變數為3
	
	
	public boolean Recongnization()
	{
		if((a + b) > c && (a + c) > b && (b + c) > a)//兩邊和大於第三邊為合理
		{
			super.judge = true;
		}
		else
		{
			super.judge = false;
		}
		
		return judge;
	}//判斷是否合理
	
	public double getArea()
	{
		if(judge == false)
		{
			return 0;//不合理傳0
		}
		else
		{
			double s = (a +b + c) / 2;
			return Math.sqrt(s * (s -a) * (s - b) * (s - c));//套用海龍公式
		}
		
	}//取得面積

}
