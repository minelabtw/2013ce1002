package ce1002.q1.s102502552;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and triangle.
		double weight,height; // variable for rectangle's constructor
		double a,b,c; // variable for triangle's constructor
		
		
		//This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight=input.nextDouble();
		System.out.println("Please input Height: ");
		height=input.nextDouble();//輸入長方形參數
		
		
		
		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();//輸入三角形參數

		Graph[] graph = new Graph[2];//建立物件
		
		for(int i = 0;i < 2;i++)
		{
			if(i == 0)
			{
				graph[i] = new Rectangle(weight,height);//第一個是長方形
				graph[i].Recongnization();
				System.out.println("Area(" + i +") = " + graph[i].getArea() + " Edge = " + graph[i].getEdge());//輸出面積與變數
			}
			else
			{
				graph[i] = new Triangle(a,b,c);//第二個是三角形
				graph[i].Recongnization();
				System.out.println("Area(" + i +") = " + graph[i].getArea() + " Edge = " + graph[i].getEdge());//輸出面積與變數
			}
		}
	input.close();
	}

}
