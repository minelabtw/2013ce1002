package ce1002.q1.s102502521;
import java.util.*;

public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float width;
		float height;
		float radius;
		Scanner scanner=new Scanner(System.in);
		Rectangle rectangle=new Rectangle();
		Circle circle=new Circle();
		
		System.out.println("Making shapes.");
		
		//判斷輸入是否合理
		do{			
			System.out.print("Input the width of the rectangle: ");
			width=scanner.nextFloat();
			if(width>20||width<1){
				System.out.println("Out of range!");
			}
		}while(width>20||width<1);
		rectangle.setWidth(width);
		
		do{
			System.out.print("Input the height of the rectangle: ");
			height=scanner.nextFloat();
			if(height>20||height<1){
				System.out.println("Out of range!");
			}
		}while(height>20||height<1);
		rectangle.setHeight(height);
		
		do{
			System.out.print("Input the radius of the circle: ");
			radius=scanner.nextFloat();
			if(radius>20||radius<1){
				System.out.println("Out of range!");
			}
		}while(radius>20||radius<1);
		circle.setRadius(radius);
		
		//關閉
		scanner.close();
		
		//列印
		System.out.println("The area of the rectangle is "+rectangle.getArea());
		System.out.println("The area of the circle is "+circle.getArea());
		System.out.println("The area sum of the shapes is "+(rectangle.getArea()+circle.getArea()));
	}

}
