package ce1002.q1.s102502521;

public class Rectangle extends Shape {
	
	private float width;
	private float height;
	
	public void setWidth(float width){
		this.width=width;    //設定寬度
	}
	
	public float getWidth(){
		return width;        //回傳寬度
	}
	
	public void setHeight(float height){
		this.height=height;  //設定高度
	}
	
	public float getHeight(){
		return height;       //回傳高度
	}
	
	public float getArea(){
		return width*height; //回傳面積
	}

}
