package ce1002.q1.s101201524;

public class Triangle extends Graph{
	private double a; // variable for triangle's constructor
	private double b;
	private double c;
	
	Triangle(){ // construct triangle with no variable
		setEdge(3);
		a = 0;
		b = 0;
		c = 0;
	}
	Triangle(double a, double b, double c){ // construct triangle with variable
		setEdge(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	public boolean Recongnization(){ // check a, b, c is greater than 0 and a + b > c
		if(a < 0 || b < 0 || c < 0)
			return false;
		else
		return a + b > c;
	}
	public double getArea(){ // output area
		if(Recongnization()==true){
			double s = (a+b+c)/2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
		else
			return 0;
	}
}
