package ce1002.q1.s101201524;

public class Rectangle extends Graph{
	private double height; // variable for rectangle's constructor
	private double width;
	
	Rectangle(){ // construct rectangle with no variable
		setEdge(4);
		height = 0;
		width = 0;
	}
	Rectangle(double w, double h){ // construct rectangle with variable
		setEdge(4);
		width = w;
		height = h;
	}
	public boolean Recongnazation(){ // check width and height is greater than 0
		if(width < 0 || height < 0)
			return false;
		else 
			return true;
	}
	public double getArea(){ // output area
		if(Recongnazation() == true)
			return width * height;
		else
			return 0;
		
	}
}
