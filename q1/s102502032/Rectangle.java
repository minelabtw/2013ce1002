package ce1002.q1.s102502032;

public class Rectangle extends Graph
{
	private double	height;
	private double	width;

	// constructor
	Rectangle()
	{
		super.setEdge(4);
	}

	Rectangle(double tempW, double tempH)
	{
		height = tempH;
		width = tempW;
		super.setEdge(4);
	}

	// set
	// none
	// get
	public double getArea()
	{
		if (this.Recongnization() == true)
		{
			super.area = height * width;
		}
		else
		{
			super.area = 0;
		}
		return super.area;
	}

	// method
	public boolean Recongnization()
	{
		return super.Recongnization(height, width);
	}
}
