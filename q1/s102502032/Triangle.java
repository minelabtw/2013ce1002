package ce1002.q1.s102502032;

public class Triangle extends Graph
{
	private double	a;
	private double	b;
	private double	c;

	// constructor
	Triangle()
	{
		super.setEdge(3);
	}

	Triangle(double tempA, double tempB, double tempC)
	{
		a = tempA;
		b = tempB;
		c = tempC;
		super.setEdge(3);
	}

	// set
	// none
	// get
	public double getArea()
	{
		if (this.Recongnization() == true)
		{
			double s = (a + b + c) / 2;
			super.area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
		}
		else
		{
			super.area = 0;
		}
		return super.area;
	}

	// method
	public boolean Recongnization()
	{
		return super.Recongnization(a, b, c);
	}
}
