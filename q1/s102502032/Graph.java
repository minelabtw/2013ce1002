package ce1002.q1.s102502032;

public class Graph
{
	protected double	area	= -1;
	private int			edge;

	// constructor
	Graph()
	{
	}

	Graph(int tempEdge)
	{
		edge = tempEdge;
	}

	// set
	public void setEdge(int e)
	{
		edge = e;
	}

	// get
	public int getEdge()
	{
		return edge;
	}

	public double getArea()
	{
		return area;
	}

	// method
	// for rectangle
	public boolean Recongnization(double a, double b)
	{
		if (a > 0 && b > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// for triangle
	public boolean Recongnization(double a, double b, double c)
	{
		if (a > 0 && b > 0 && c > 0 && a + b > c && a + c > b && b + c > a)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
