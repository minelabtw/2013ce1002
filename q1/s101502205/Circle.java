package ce1002.q1.s101502205;

public class Circle extends Shape{
	
	private float radius;
	
	public void setRadius(float radius){
		if( radius<1 || radius>20 ){
			// Error
			System.out.println("Out of range!");
		}else{
			this.radius = radius;
		}
	}
	public float getRadius(){
		return radius;
	}
	public float getArea(){
		// Override
		return (float)Math.PI*radius*radius;
	}
	
}
