package ce1002.q1.s101502205;

public class Rectangle extends Shape{
	
	private float width;
	private float height;

	public void setWidth(float width){
		if( width<1 || width>20 ){
			// Error
			System.out.println("Out of range!");
		}else{
			this.width = width;
		}
	}
	public float getWidth(){
		return width;
	}

	public void setHeight(float height){
		if( height<1 || height>20 ){
			// Error
			System.out.println("Out of range!");
		}else{
			this.height = height;
		}
	}
	public float getHeight(){
		return height;
	}

	public float getArea(){
		// Override
		return width*height;
	}
	
}
