package ce1002.q1.s101502205;

import java.util.Scanner;

public class Q1 {
	
	// Create a scanner
	private static Scanner input;

	public static void main(String[] args) {
		
		input = new Scanner(System.in);
		
		System.out.println("Making shapes.");
		
		// User input: number of shapes
		System.out.print("Input the number of shapes to be create: ");
		int n = input.nextInt();
		while ( n<1 || n>10){
			System.out.println("Out of range!");
			System.out.print("Input the number of shapes to be create: ");
			n = input.nextInt();
		}
		
		Shape[] shapes = new Shape[n];
		int selection;
		
		// Finish all data
		for(int i=0; i<n; i++){
			// Create rectangle of circle
			System.out.print("1)Create rectangle, 2)Create circle: ");
			selection = input.nextInt();
			while(selection!=1 && selection!=2){
				System.out.println("Out of range!");
				System.out.print("1)Create rectangle, 2)Create circle: ");
				selection = input.nextInt();
			}
			
			if(selection == 1){
				// Create rectangle
				Rectangle rect = new Rectangle();
				float w,h;
				
				// Input the width
				do{
					System.out.print("Input the width of the rectangle: ");
					w = input.nextFloat();
					rect.setWidth(w);
				}while( w<1 || w>20 );
				// Input the height
				do{
					System.out.print("Input the height of the rectangle: ");
					h = input.nextFloat();
					rect.setHeight(h);
				}while( h<1 || h>20 );
				
				// Store the rectangle
				shapes[i] = rect;
				
			}else{
				Circle circle = new Circle();
				float r;
				
				// Input the radius
				do{
					System.out.print("Input the radius of the circle: ");
					r = input.nextFloat();
					circle.setRadius(r);
				}while( r<1 || r>20 );
				
				// Store the circle
				shapes[i] = circle;
			}
		}
		
		float sum = 0;
		for(int i=0; i<n; i++){
			sum += shapes[i].getArea(); // Add area to sum
			// Output the area
			System.out.println("The area of the shape is " + shapes[i].getArea());
		}
		// Output the sum
		System.out.println("The area sum of the shapes is " + sum);
		
	}

}
