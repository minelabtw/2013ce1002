package ce1002.q1.s102502014;
import java.util.Scanner;

import ce1002.q1.s102502014.Shape;
public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		float w, h, r,s;                       //寬、高、半徑
		System.out.println("Making shapes.");
		do {
			System.out.print("Input the width of the rectangle: ");
			w = scanner.nextFloat();
			if (w < 1 || w > 20) {
				System.out.println("Out of range!");
			}
		} while (w < 1 || w > 20);
		do {
			System.out.print("Input the height of the rectangle: ");
			h = scanner.nextFloat();
			if (h < 1 || h > 20) {
				System.out.println("Out of range!");
			}
		} while (h < 1 || h > 20);

		do {
			System.out.print("Input the radius of the circle: ");
			r = scanner.nextFloat();
			if (r < 1 || r > 20) {
				System.out.println("Out of range!");
			}
		} while (r < 1 || r > 20);

        Shape shape[]=new Shape[2];              //物件
        shape[0]=new Rectangle();                //設定值
        shape[0].setWidth(w);
        shape[0].setHeight(h);
        shape[1]=new Circle();
        shape[1].setRadius(r);
        
        System.out.println("The area of the rectangle is "+ shape[0].getArea());
        System.out.println("The area of the circle is "+ shape[1].getArea());
        s=shape[0].getArea()+shape[1].getArea();
        System.out.println("The area sum of the sahpes is "+s);

		scanner.close();
	}

}
