package ce1002.q1.s101201506;

public class Triangle extends Graph {
	private double a;// edge lengh
	private double b;
	private double c;

	public void setArea(double a,double b,double c ) {
		this.a=a;
		this.b=b;
		this.c=c;
	}
	
	public boolean Recongization() {
		return a > 0 && b > 0 && c > 0 && a + b > c && a + c > b && b + c > a;
	}

	public double getArea() {
		if (a < 0 && b < 0 && c < 0 && a + b < c && a + c < b && b + c < a) {
			return area = 0;
		}

		else {
			double s = (a + b + c) / 2;
			return area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
		}
	}
}
