package ce1002.q1.s101201506;

import java.util.Scanner;

class Q1 {
//助教我很努力的把他寫道我會的地步了 為了 讓他可以compile TAT
	public static void main(String[] args) {
		// create a rectangle object and a triangle object

		// create a scanner object and let it parses the standard input.
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and
		// triangle.
		double weight, height; // variable for rectangle's constructor
		double a, b, c; // variable for triangle's constructor

		// This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight = input.nextDouble();
		System.out.println("Please input Height: ");
		height = input.nextDouble();

		Rectangle rectangle = new Rectangle();

		// This part is handling the input to construct the triangle object
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();
		double s = (a + b + c) / 2;
		
		Triangle triangle = new Triangle();

		// print out each area from different shape of graph

		System.out.println("Area(0) = " + weight * height + " Edge = " + "4");
		System.out
				.println("Area(1) = "
						+ Math.sqrt(s * (s - a) * (s - b) * (s - c))
						+ " Edge = " + "3");
	}

}
