package ce1002.q1.s102502046;

public class Triangle extends Graph
{
	private double a; 		//宣告變數
	private double b;
	private double c;
	private double s;
	
	Triangle(double x,double y,double z)//Constructor
	{
		a=x;
		b=y;
		c=z;
		s=(a+b+c)/2;
		area = Math.sqrt(s*(s-a)*(s-b)*(s-c)) ;
		setEdge(3);
	}
	public boolean Recongnization() 
	{
		if(a>0 && b>0 && c>0 && a+b>c && b+c>a && a+c>b)
				return true;
		return false;
	}
	public double getArea() 
	{
		if (Recongnization())
			return super.getarea();
		return 0;
	}
	public int getEdge() {
		return super.getEdge();
	}
	
}
