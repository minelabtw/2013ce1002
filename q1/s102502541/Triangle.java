package ce1002.q1.s102502541;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;
	public Triangle()
	{
		setEdge(3);
	}
	public boolean Recongnization()//判斷正負 三邊長
	{
		if(a<=0 || b<=0 || c<=0)
			return false;
		else if(a+b<c || a+c<b || b+c<a)
			return false;
		else
			return true;
	}
	public double getArea(double a,double b,double c)
	{	
		this.a = a;
		this.b = b;
		this.c = c;
		
		if(this.Recongnization()== false)
			return 0;
		else
			return Math.sqrt((a+b+c)/2*((a+b+c)/2-a)*((a+b+c)/2-b)*((a+b+c)/2-c));
	}
}
