package ce1002.q1.s102502010;

public class Rectangle extends Shape{

	private float width;
	private float height;
	Rectangle()  //constructor
	{
		width=0;
		height=0;
	}
	public void setWidth(float width)
	{
		this.width=width;
	}
	public void setHeight(float height)
	{
		this.height=height;
	}
	public float getWidth()
	{
		return width;
	}
	public float getHeight()
	{
		return height;
	}
	public float getArea()
	{
		return super.getArea()+width*height;
	}
}
