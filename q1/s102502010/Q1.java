package ce1002.q1.s102502010;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		float width;
		float height;
		float radius;
		float sum;
		Scanner input = new Scanner(System.in);
		System.out.println("Making shapes.");
		System.out.print("Input the width of the rectangle: ");
		width=input.nextFloat();  //input width
		while(width<1 || width>20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width=input.nextFloat();
		}
		System.out.print("Input the height of the rectangle: ");
		height=input.nextFloat();  //input height
		while(height<1 || height>20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height=input.nextFloat();
		}
		Rectangle rectangles = new Rectangle();  //rectangle
		rectangles.setWidth(width);
		rectangles.setHeight(height);
		System.out.print("Input the radius of the circle: ");
		radius=input.nextFloat(); //input radius
		while(radius<1 || radius>20)
		{
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			radius=input.nextFloat();
		}
		Circle circles = new Circle();  //circle
		circles.setRadius(radius);
		System.out.println("The area of the shape is " + rectangles.getArea());
		System.out.println("The area of the shape is " + circles.getArea());
		sum=rectangles.getArea()+circles.getArea();
		System.out.println("The area sum of the shape is " + sum);
		input.close();
	}
}
