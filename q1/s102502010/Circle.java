package ce1002.q1.s102502010;

public class Circle extends Shape{

	private float radius;
	Circle()  //constructor
	{
		radius=0;
	}
	public void setRadius(float radius)
	{
		this.radius=radius;
	}
	public float getRadius()
	{
		return radius;
	}
	public float getArea()
	{
		return super.getArea()+(float) (radius*radius*3.14);
	}
}
