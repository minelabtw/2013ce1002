package ce1002.q1.s102502560;

public class Triangle extends Graph{
	
	private double a;
	private double b;
	private double c;
	
	public Triangle(double a, double b, double c) {
		setA(a);
		setB(b);
		setC(c);
		setEdge(3);
	}
	
	public double getA() {
		return a;
	}
	
	public double getB() {
		return b;
	}
	
	public double getC() {
		return c;
	}
	
	public void setA(double a) {
		this.a = a;
	}
	
	public void setB(double b) {
		this.b = b;
	}
	
	public void setC(double c) {
		this.c = c;
	}
	
	@Override
	public boolean Recongnization() {
		return a>=0 && b>=0 && c>=0 && a+b>c && b+c>a && a+c>b;
	}
	
	@Override
	public double getArea() {
		if(Recongnization()){
			double s=(a+b+c)/2;
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}else{
			return 0;
		}
		
	}
	
}
