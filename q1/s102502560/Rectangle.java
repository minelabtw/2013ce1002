package ce1002.q1.s102502560;

public class Rectangle extends Graph{
	
	private double width;
	private double height;
	
	public Rectangle(double width, double height) {
		setWidth(width);
		setHeight(height);
		setEdge(4);
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	@Override
	public boolean Recongnization() {
		return height>=0 && width>=0;
	}
	
	@Override
	public double getArea() {
		if(Recongnization()){
			return height*width;
		}else{
			return 0;
		}
	}
	
}
