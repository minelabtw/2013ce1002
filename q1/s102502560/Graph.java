package ce1002.q1.s102502560;

public abstract class Graph {
	
	//protected double area;	//useless variable
	private int edge;
	
	public void setEdge(int edge) {
		this.edge = edge;
	}
	
	public int getEdge() {
		return edge;
	}
	
	public abstract boolean Recongnization();
	public abstract double getArea();
	
}
