package ce1002.q1.s102502560;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		Graph[] graphs=new Graph[2];		// create a graph array
		
		Scanner STDIN = new Scanner(System.in);

		double width, height;				// variable for rectangle's constructor
		double a,b,c;						// variable for triangle's constructor
		
		
		//This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");	//typo
		width=STDIN.nextDouble();
		System.out.println("Please input Height: ");
		height=STDIN.nextDouble();
		graphs[0] = new Rectangle(width,height);
		
		
		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");	//typo
		System.out.println("Please input a: ");
		a = STDIN.nextDouble();
		System.out.println("Please input b: ");
		b = STDIN.nextDouble();
		System.out.println("Please input c: ");
		c = STDIN.nextDouble();
		graphs[1] = new Triangle(a,b,c);
		
		STDIN.close();
		
		// print out each area from different shape of graph
		for(int i=0;i<graphs.length;i++){
			System.out.println("Area("+i+") = "+graphs[i].getArea()+" Edge = "+graphs[i].getEdge());
		}
				
	}
	
}
