package ce1002.q1.s102502050;

public class Rectangle extends Graph{
	private double height;
	private double width;//長方形邊長
	public Rectangle(double height,double width)
	{
		this.height=height;
		this.width=width;
	}
	public boolean Recongnization()//判斷圖形是否合理
	{
		return (height>0 && width >0);//判斷邊長是否皆大於0
	}
	public double getArea()//回傳面積
	{
		if(Recongnization() == false)//
		{
			area=0;
		}
		else
		{
			area=height*width;
		}
		return super.getArea();
	}
	
}
