package ce1002.q1.s102502050;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;//三個邊的邊長
	public Triangle(double a, double b, double c)
	{
		this.a=a;
		this.b=b;
		this.c=c;
	}
	public boolean Recongnization()//判斷圖形是否合理
	{
		double max=0,sum=a+b+c;
		if(a>=b && a>=c)//判斷最長的邊
		{
			max=a;
				
		}
		else if(b>=a && b>=c)
		{
			max=b;
		}
		else if(c>=a && c>=b)
		{
			max=c;
		}
		
		if( (max >= (sum-max) )|| a<=0 || b<=0 || c<=0 )//判斷兩邊之和大於第三邊，邊長大於0
			return false;
		else
			return true;
		
	}
	public double getArea()//回傳面積
	{
		if(Recongnization()==false)//不滿足三角形條件，面積為0
		{
			area=0;
		}
		else//計算面積
		{
			double s= (a+b+c)/2;
			area= Math.sqrt(s*(s-a)*(s-b)*(s-c));
		}
		return super.getArea();
		
	}
}
