package ce1002.q1.s102502538;

public class Triangle extends Graph{
	private double a;
	private double b;
	private double c;//三角形的三邊
	
	public void setData(double x,double y,double z){
		a = x;
		b = y;
		c = z;
	}
	
	public boolean Recongnization(){//判別三個編是否滿足三角形的條件。或有邊長<0
		if(a<0||b<0||c<0){
			x = false;
		}
		else if(a+b<c||a+c<b||b+c<a){
			x = false;
		}
		else{
			x = true;
		}
		return x;	
	}
	
	public double getArea(){//若不滿足條件則回傳0，否則回傳面積
		if(x==false){
			return 0;
		}
		else{
			double s,w;
			s = (a+b+c)/2;
			w =  Math.sqrt(s*(s-a)*(s-b)*(s-c));
			return w;
		}	
	}
}
