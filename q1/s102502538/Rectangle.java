package ce1002.q1.s102502538;

public class Rectangle extends Graph{
	private double height;
	private double width;//矩形的長和寬
	
	public void setData(double x,double y){
		width = x;
		height = y;
	}
	
	public boolean Recongnization(){//判別長和寬是否<0
		if(height<0||width<0){
			x = false;
		}
		else{
			x = true;
		}
		return x;	
	}
	
	public double getArea(){//若不滿足條件則回傳0，否則回傳面積
		double A;
		if(x == false){
			A = 0;
		}	
		else {
			A = height * width;
		}	
		return A;
	}
}
