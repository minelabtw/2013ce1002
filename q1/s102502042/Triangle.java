package ce1002.q1.s102502042;

public class Triangle extends Graph{
	
	private double a;
	private double b;
	private double c;
	
	Triangle()	//建構子
	{
		setEdge(3);
	}
	Triangle(double a,double b,double c)	//有參數的建構子
	{
		setEdge(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	public boolean Recongnization()
	{
		if(a<0||b<0||c<0)return false;
		if((a+b>c) && (b+c>a) && (a+c>b))return true;
		return false;
	}
	public double getArea()	//return area
	{
		if(!this.Recongnization())return 0;
		double s = (a+b+c)/2;
		return Math.sqrt(s*(s-a)*(s-b)*(s-c));
	}
	
}
