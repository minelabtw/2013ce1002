package ce1002.q1.s102502042;

public class Rectangle extends Graph{
	
	private double height;
	private double width;
	
	Rectangle()	//建構子
	{
		setEdge(4);
	}
	Rectangle(double width,double height)	//有參數的建構子
	{
		setEdge(4);
		this.width = width;
		this.height = height;
	}
	public boolean Recongnization() 
	{
		if(height<0 || width<0)return false;
		return true;
	}
	public double getArea()	//reurn area
	{
		if(!this.Recongnization())return 0;
		return this.height*this.width;
	}
}
