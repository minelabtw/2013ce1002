package ce1002.q1.s102502002;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		float wid=0;
		float hit=0;
		float rad=0;
		int n=0;
		Shape []shape = new Shape[2];
		Rectangle rec = new Rectangle();
		Circle circle = new Circle();
		shape[0]=rec;
		shape[1]=circle;
		Scanner input = new Scanner(System.in);
		System.out.println("Making Shapes.");
		System.out.print("Input the number of shapes to be create: ");
		n=input.nextInt();
		while(n<1||n>10){
			System.out.println("Out of range!");
			System.out.print("Input the number of shapes to be create: ");
			n=input.nextInt();
		}
		float []a= new float[n];
		for(int j=0;j<n;j++){
			int c=0;
			System.out.print("1)Create rectangle, 2)Create circle: ");
			c=input.nextInt();
			while(c<1||c>2){
				System.out.println("Out of range!");
				System.out.print("1)Create rectangle, 2)Create circle: ");
				c=input.nextInt();
			}
			switch (c){
			case 1:
				System.out.print("Input the width of the rectangle: ");
				wid=input.nextFloat();
				while(wid<1||wid>20){
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					wid=input.nextFloat();
				}
				rec.setWidth(wid);
				System.out.print("Input the height of the rectangle: ");
				hit=input.nextFloat();
				while(hit<1||hit>20){
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					hit=input.nextFloat();
				}
				rec.setHeight(hit);
				a[j]=shape[0].getArea();
				break;
			case 2:
				System.out.print("Input the radius of the circle: ");
				rad=input.nextFloat();
				while(rad<1||rad>20){
					System.out.println("Out of range!");
					System.out.print("Input the radius of the circle: ");
					rad=input.nextFloat();
				}
				circle.setRadius(rad);
				a[j]=shape[1].getArea();
				break;
			}
		}
		for(int i=0; i<n; i++){
			System.out.println("The area of the shape is: "+a[i]);
		}
		float k=0;
		for(int i=0; i<n; i++){
			k=a[i]+a[i+1];
			a[i]=a[i+1];
		}
		System.out.println("The sum of the shapes is: "+k);
		
		input.close();
	}

}
