package ce1002.q1.s102502002;

public class Circle extends Shape {
	Circle(){
		super.setArea((float)(Radius*Radius*3.14));
	}
	private float Radius=0;
	public void setRadius(float radius){
		Radius=radius;
	}
	public float getRadius(){
		return Radius;
	}
}
