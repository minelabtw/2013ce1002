package ce1002.q1.s102502509;

public class Shape 
{
	protected float width;
	protected float height;
	protected float radius;
	
	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getArea()
	{
		return 0;
	}
}
