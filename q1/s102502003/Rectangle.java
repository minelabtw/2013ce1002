package ce1002.q1.s102502003;

public class Rectangle extends Shape {
	
	private float width;
	private float height;

	//set sections
	public void setWidth(float width) {
		this.width = width;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	
	//get information of rectangle
	public float getHeight() {
		return height;
	}
	public float getWidth() {
		return width;
	}
	
	//calculate the area
	public float getArea(){
		float area=0;
		area=height * width;
		return area;
	}
	
	

}
