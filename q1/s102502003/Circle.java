package ce1002.q1.s102502003;

public class Circle extends Shape {
	
	private float radius;

	//get the radius
	public float getRadius() {
		return radius;
	}

	//set the radius
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	//calculate the area
	public float getArea()
	{
		float area=0;
		area=(float)(radius * radius * Math.PI);  //change type from double to float
		return area;
	}

}
