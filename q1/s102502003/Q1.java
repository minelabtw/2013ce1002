package ce1002.q1.s102502003;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		float width=0;
		float height=0;
		float radius=0;
		
		Scanner scanner = new Scanner(System.in);
		Rectangle r = new Rectangle();  //create a rectangle object
		Circle c = new Circle();  //create a circle object
		
		System.out.println("Making shapes.");
		do  //set the width
		{
			System.out.print("Input the width of the rectangle: ");
			width=scanner.nextFloat();
			if(width<1||width>20)
				System.out.println("Out of range!");
		}while(width<1||width>20);
		r.setWidth(width);
		
		do  //set the height
		{
			System.out.print("Input the height of the rectangle: ");
			height=scanner.nextFloat();
			if(height<1||height>20)
				System.out.println("Out of range!");
		}while(height<1||height>20);
		r.setHeight(height);
		
		do  //set the radius
		{
			System.out.print("Input the radius of the rectangle: ");
			radius=scanner.nextFloat();
			if(radius<1||radius>20)
				System.out.println("Out of range!");
		}while(radius<1||radius>20);
		c.setRadius(radius);
				
		scanner.close();  //close the Scanner
		
		//print the result
		System.out.println("The area of the rectangle is "+r.getArea());
		System.out.println("The area of the circle is "+c.getArea());
		float area = r.getArea() + c.getArea();
		System.out.println("The area sum of the shape is "+area);

	}

}
