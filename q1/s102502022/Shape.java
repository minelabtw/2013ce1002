package ce1002.q1.s102502022;

public class Shape {
	public float width;
    public float height;
    public float radius;
	public void setWidth(float width){
		this.width=width;
	}
	public void setHeight(float height){
		this.height=height;
	}
	public void setRadius(float radius){
		this.radius=radius;
	}
	public float getArea(){
		return 0;
	}

}
