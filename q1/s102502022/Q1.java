package ce1002.q1.s102502022;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner scn= new Scanner(System.in);
		System.out.println("Making shapes.");
		System.out.print("Input the number of shapes to be create: ");
		int n1= scn.nextInt();     //變數
		while(n1<1 || n1>10){
			System.out.println("Out of range!");
			System.out.print("Input the number of shapes to be create: ");
			n1= scn.nextInt();
		}
		Shape[] shape= new Shape[n1];     //宣告陣列
		for(int t=0;t<n1;t++)    //for迴圈
		{
			shape[t]= new Shape();
			System.out.print("1)Create rectangle, 2)Create circle: ");
			int n2= scn.nextInt();    //變數
			while(n2<1 || n2>2){
				System.out.println("Out of range!");
				System.out.println("1)Create rectangle, 2)Create circle: ");
			}
			if(n2==1){
				System.out.print("Input the width of the rectangle: ");
				float width= scn.nextFloat();
				while(width<1 || width>20){
					System.out.println("Out of range!");
					System.out.print("Input the width of the rectangle: ");
					width= scn.nextFloat();
				}
				shape[t].setWidth(width);    //呼叫函式
				System.out.print("Input the height of the rectangle: ");
				float height= scn.nextFloat();
				while(height<1 || height>20){
					System.out.println("Out of range!");
					System.out.print("Input the height of the rectangle: ");
					height= scn.nextFloat();
				}
				shape[t].setHeight(height);
				
			}
			if(n2==2){
				System.out.print("Input the radius of the circle: ");
				float radius=scn.nextFloat();
				while(radius<1 || radius>20){
					System.out.println("Out of range!");
					System.out.print("Input the radius of the rectangle: ");
					radius= scn.nextFloat();
				}
				shape[t].setRadius(radius);
				
			}
		}
		for(int t=0;t<n1;t++){
			System.out.println("The area of the shape is "+shape[t].getArea());
		}
        float sum=shape[0].getArea()+shape[1].getArea()+shape[2].getArea();
		System.out.println("The area sum of the shapes is"+sum);
		scn.close();
	}

}
