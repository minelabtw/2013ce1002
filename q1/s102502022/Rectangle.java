package ce1002.q1.s102502022;

class Rectangle extends Shape{
	public float width;
    public float height;	
	public void setWidth(float width){
		this.width=width;
	}
	public void setHeight(float height){
		this.height=height;
	}
    public float getArea(){    //override
    	return width*height;
    }
	

}
