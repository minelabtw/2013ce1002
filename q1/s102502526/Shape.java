package ce1002.q1.s102502526;

public class Shape {
private float width;
private float height;
private float radius;

Shape(){    //constructor

}

public void setWidth(float width){     //設定private裡的變數
	this.width = width;
}
public void setHeight(float height){
	this.height = height;
}
public void setRadius(float radius){
	this.radius = radius;
}

public float getWidth(){               //get private裡的變數值
	return this.width;
}
public float getHeight(){
	return this.height;
}
public float getRadius(){
	return this.radius;
}

public float getRectangleArea(){   //做矩形的面積運算並且回傳
	return this.width * this.height;
}
public float getCircleArea(){      //做圓形的面積運算並且回傳
	return this.radius * 3.14f;
}
public float getAreaSum(){         //將兩面積相加
	return getRectangleArea() + getCircleArea();
}
}
