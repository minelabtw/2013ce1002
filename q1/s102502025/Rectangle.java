package ce1002.q1.s102502025;

public class Rectangle extends Shape {

	public void Rectangle(float height,float width) {
		setHeight(height);
		setWidth(width);
	}

	public void setWidth(float width) {
		width = width;
	}

	public float getWidth() {
		return width;
	}

	public void setHeight(float height) {
		height=height;
	}

	public float getheight() {
		return height;
	}
}
