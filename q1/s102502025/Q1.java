package ce1002.q1.s102502025;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Making shape.");
		float width = 0;
		float height = 0;
		float radius = 0;
		float rectangle = 0;
		float circle = 0;
		float sum = 0;
		do {
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			if (width < 1 || width > 20) {
				System.out.println("Out of range!");
			}
		} while (width < 1 || width > 20);

		do {
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			if (height < 1 || height > 20) {
				System.out.println("Out of range!");
			}
		} while (height < 1 || height > 20);

		do {
			System.out.print("Input the radius of the rectangle: ");
			height = input.nextFloat();
			if (height < 1 || height > 20) {
				System.out.println("Out of range!");
			}
		} while (height < 1 || height > 20);
		
		rectangle = (width * height);
		circle = (float)((float)(Math.PI) * radius *radius);
		sum = rectangle + circle;
		
		System.out.println("The area of the rectangle is " + rectangle);
		System.out.println("The area of the circle is "
				+ circle);
		System.out.println("The area sum of the shape is "
				+ sum);
		input.close();
	}
}
