package ce1002.q1.s102502034;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c;

	Triangle(double a, double b, double c) {
		setEdge(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}
	//set a b c  edge

	public boolean Recongnization() {
		if (a + b < c || c + b < a || a + c < b)
			return false;
		else if (a<0 || b<0 || c<0)
			return false ;
		else
			return true;

	}
	//Check if inputs are right

	public double getArea() {
		if (Recongnization() == false)
			return 0;
		double s = (a + b + c) / 2;
		area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
		return area;

	}
	//calculate area
}
