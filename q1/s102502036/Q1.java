package ce1002.q1.s102502036;

import java.util.Scanner ;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// create a rectangle object and a triangle object

		        Graph[] Grapharray = new Graph[2]  ;
		       
		        
				// create a scanner object and let it parses the standard input.
				Scanner input = new Scanner(System.in);
				// create temporary variables for creating objects such as rectangle and triangle.
				double weight,height; // variable for rectangle's constructor
				double a,b,c; // variable for triangle's constructor
				
				
				//This part is handling the input to construct the rectangle object
				System.out.println("First Graph is rectangle");
				System.out.println("Please input Weight: ");
				weight=input.nextDouble();
				System.out.println("Please input Height: ");
				height=input.nextDouble();
				
				Grapharray[0] = new Rectangle(weight,height) ;
				Grapharray[0].setEdge(4) ;
				//This part is handling the input to construct the triangle object		
				System.out.println("First Graph is triangle");
				System.out.println("Please input a: ");
				a = input.nextDouble();
				System.out.println("Please input b: ");
				b = input.nextDouble();
				System.out.println("Please input c: ");
				c = input.nextDouble();
				Grapharray[1] = new Triangle(a,b,c) ;
				Grapharray[1].setEdge(3) ;
				for(int i = 0 ; i<2 ;i++){
					Grapharray[i].Recongnization() ;
					System.out.println("Area("+i+") = "+Grapharray[i].getArea()+"Edge = "+Grapharray[i].getEdge());
				}
				
				// print out each area from different shape of graph
						
	}

}
