package ce1002.q1.s102502006;

import java.util.Scanner;

public class Q1 {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		float r=0;
		float sum=0;
		int num=0;
		int method=0;
		float area=0;
		System.out.println("Making shapes.");
		
		
		while(num<1 || num>10){
			System.out.print("Input the number of shapes to be create: ");
			num = scanner.nextInt();
			if(num<1 || num>10)System.out.println("Out of range!");
		}
		
		// Car[] cars = new Car[num]; 
		Rectangle[] rectangles = new Rectangle[num];
		Circle[] circles = new Circle[num];
		
		for(int i=0;i<3;i++){
			
			while(method!=1 && method!=2){
				System.out.print("1)Create rectangle, 2)Create circle: ");
				method = scanner.nextInt();
				if(method!=1 && method!=2)System.out.println("Out of range!");
			}
			
			if(method==1){
				rectangles[i] = new Rectangle();
				while(r<1 || r>20){
					System.out.print("Input the width of the rectangle: ");
					r = scanner.nextFloat();
					if(r<1 || r>20)System.out.println("Out of range!");
				}
				rectangles[i].setWidth(r);
				r=0;
				
				while(r<1 || r>20){
					System.out.print("Input the height of the rectangle: ");
					r = scanner.nextFloat();
					if(r<1 || r>20)System.out.println("Out of range!");
				}
				rectangles[i].setHeight(r);
				r=0;
			}
			else{ 
				circles[i] = new Circle();
				while(r<1 || r>20){
					System.out.print("Input the radius of the circle: ");
					r = scanner.nextFloat();
					if(r<1 || r>20)System.out.println("Out of range!");
				}
				circles[i].setRadius(r);
				r=0;
			}
		}
		scanner.close();
		for(int i=0;i<3;i++){
			// System.out.println(circles[i].getArea());
			area = rectangles[i].getArea()==0 ? circles[i].getArea() : rectangles[i].getArea();
			System.out.println("The area of the shape is " + area);
			sum+=area;
		}
		
		
		
		
		
		/*sum = rectangle.getArea() +  circle.getArea();
		
		System.out.println("The area of the rectangle is " + rectangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());*/
		System.out.println("The area sum of the shapes is " + sum);
		
	}
	
}
