package ce1002.q1.s102502006;

public class Circle extends Shape{
	private float radius;
	public Circle(){
		radius =0;
	}
	public void setRadius(float radius){
		this.radius = radius;
	}
	public float getRadius(){
		return radius;
	}
	public float getArea(){
		return radius*radius*31415926/10000000;
	}

}
