package ce1002.q1.s102502006;

public class Rectangle extends Shape{
	
	private float width;
	private float height;
	public Rectangle(){
		width =0;
		height =0;
	}
	public void setWidth(float width){
		this.width = width;
	}
	public float getWidth(){
		return width;
	}
	public void setHeight(float height){
		this.height = height;
	}
	public float getHeight(){
		return height;
	}
	public float getArea(){
		return width*height;
	}
}
