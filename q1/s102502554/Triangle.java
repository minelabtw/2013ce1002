package ce1002.q1.s102502554;

public class Triangle extends Graph{
	
	private double a;
	private double b;
	private double c;
	private boolean i;
	
	public Triangle (){
	
	}
	
	Triangle(double a , double b , double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}//set the length of sides
	
	public boolean Recongnization(){
		if(a < 0 || b < 0 || c < 0){
			i = false;
		}
		if( a + b < c || a + c < b || b + c < a){
			i = false;
		}
		else{
			i = true;
		}
		return i;
		
	}//judge the shape is possible
	
	public double getArea(){
		double s = (a + b + c)/2;
		double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		if ( a < 0 || b < 0 || c < 0 || a + b < c || a + c < b || b + c < a){
			area = 0.0 ;
		}
		return area;
	}//print out the area
	
	public int getEdge(){
		return 3;
	}

}
