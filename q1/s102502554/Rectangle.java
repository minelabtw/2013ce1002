package ce1002.q1.s102502554;

public class Rectangle extends Graph{
	
	private double height;
	private double width;
	private boolean i;
	
	public Rectangle (){
		
	}
	
	Rectangle(double w, double h){
		this.width = w;
		this.height = h;
	}//set the length of sides
	
	public boolean Recongnization(){
		if ( height < 0 || width < 0 ){
			i = false;
		}
		else{
			i = true;
		}
		return i;
	}//judge the shape is possible
	
	public double getArea(){
		double area = width * height;
		if ( area < 0 ){
			area = 0.0 ;
		}
		return area;
		
	}
	
	public int getEdge(){
		return 4;
	}

}
