package ce1002.q1.s102502508;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		float w = 0;
		float h = 0;
		float r = 0;

		System.out.println("Making shapes.");

		do {// 利用後測試迴圈使使用者輸入規定中的長度範圍
			System.out.print("Input the width of the rectangle: ");
			w = input.nextFloat();

			if (w < 1 || w > 20) {
				System.out.println("Out of range!");
			}
		} while (w < 1 || w > 20);

		do {// 利用後測試迴圈使使用者輸入規定中的長度範圍
			System.out.print("Input the height of the rectangle: ");
			h = input.nextFloat();

			if (h < 1 || h > 20) {
				System.out.println("Out of range!");
			}
		} while (h < 1 || h > 20);

		do {// 利用後測試迴圈使使用者輸入規定中的長度範圍
			System.out.print("Input the radius of the circle: ");
			r = input.nextFloat();

			if (r < 1 || r > 20) {
				System.out.println("Out of range!");
			}
		} while (r < 1 || r > 20);

		// 建立物件
		Rectangle Re = new Rectangle();
		Circle Ce = new Circle();
		Shape Se = new Shape();
		// 利用物件呼叫類別中的函式
		Re.setHeight(h);
		Re.setWidth(w);
		System.out.println("The area of the ractangle is " + Re.getArea());

		Ce.setRadius(r);
		System.out.println("The area of the circle is " + Ce.getArea());

		System.out.println("The area sum of the shapes is "
				+ (Re.getArea() + Ce.getArea()));

		input.close();

	}

}
