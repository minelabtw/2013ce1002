package ce1002.q1.s102502508;

public class Circle extends Shape {

	public Circle()
	{
		
	}
	
	public void setRadius(float radius)
	{
		this.radius=radius ;
	}
	public float getRadius()
	{
		return radius ;
	}
	public float getArea()
	{
		return (float) (radius*radius*(double)Math.PI) ;
	}
}
