package ce1002.q1.s102502508;

public class Rectangle extends Shape {
   
	public Rectangle()
	{
		
	}
	
	public void setWidth(float width)
	{
		this.width=width ;
	}
	public float getWidth()
	{
		return width ;
	}
	public void setHeight(float height)
	{
		this.height=height ;
	}
	public float getHeight()
	{
		return height ;
	}
	public float getArea()
	{
		return height*width ;
	}
}
