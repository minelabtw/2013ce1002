package ce1002.q1.s102502015;

import java.util.Scanner;
import ce1002.q1.s102502015.Circle;

public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Making shapes.");
		int mode;			//個階段需要變數
		int num;
		float w;
		float h;
		float r;
		do {
			System.out.println("Input the number of shapes to be create: ");
			num = scanner.nextInt();
			if (num > 10 || num < 1) {
				System.out.println("Out of range!");
			}
		} while (num > 10 || num < 1);
		Shape shape[] = new Shape[num];		//創陣列
		for (int i = 0; i < num; i++) {
			do {
				System.out.println("1)Creat rectangle, 2)Creat circle: ");
				mode = scanner.nextInt();
				if (mode > 2 || mode < 1) {
					System.out.println("Out of range!");
				}
			} while (mode > 2 || mode < 1);
			if (mode == 1) {
				shape[i] = new Rectangle();		//多形
				do {
					System.out.println("Input the width of the rectangle: ");
					w = scanner.nextFloat();
					if (w > 20 || w < 1) {
						System.out.println("Out of range!");
					}
				} while (w > 20 || w < 1);
				do {
					System.out.println("Input the heighth of the rectangle: ");
					h = scanner.nextFloat();
					if (h > 20 || h < 1) {
						System.out.println("Out of range!");
					}
				} while (h > 20 || h < 1);
				shape[i].setHeight(h);
				shape[i].setWidth(w);
			} else {
				shape[i] = new Circle();		//多形
				do {
					System.out.println("Input the radius of the circle: ");
					r = scanner.nextFloat();
					if (r > 20 || r < 1) {
						System.out.println("Out of range!");
					}
				} while (r > 20 || r < 1);
				shape[i].setRadius(r);
			}
		}
		float sum = 0;
		for (int i = 0; i < num; i++) {		//計算SUM 兼 印出面積
			System.out
					.println("The area of the shape is " + shape[i].getArea());
			sum = sum + shape[i].getArea();
		}
		System.out.println("The area sum of the shapes is " + sum);
		scanner.close();
	}

}
