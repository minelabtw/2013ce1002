package ce1002.q1.s102502015;

public class Rectangle extends Shape{
	private float w;
	private float h;
	Rectangle()
	{
		
	}
	public void setWidth(float width)
	{
		w=width;
	}
	public void setHeight(float height)
	{
		h=height;
	}
	public float getWidth()
	{
		return w;
	}
	public float getHeight()
	{
		return h;
	}
	public float getArea()
	{
		return w*h;
	}

}
