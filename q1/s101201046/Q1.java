package ce1002.q1.s101201046;

import java.util.Scanner;

public class Q1 {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in); //build a scanner
		Shape[] ss;
		int num, i, opt;
		float width, height, radius, total;
		
		System.out.println("Makeing shapes."); //start making shapes
		
		do { //read the number of shapes
			System.out.print("Input the number of shapes to create: ");
			num = in.nextInt();
			
			if (num < 1 || num > 10)
				System.out.println("Out of range!");
		} while(num < 1 || num > 10);
		
		ss = new Shape[num];
		
		for (i = 0; i < num; i++) {
			do { //determine the type of the shape
				System.out.print("1)Create rectangle, 2)Create circle: ");
				opt = in.nextInt();
				
				if (opt != 1 && opt != 2)
					System.out.println("Out of range!");
			} while(opt != 1 && opt != 2);
			
			if (opt == 1) { //when the shpae is rectangle
				ss[i] = new Rectangle();
				
				do { //read width
					System.out.print("Input the width of the rectangle: ");
					width = in.nextFloat();
					
					if (width > 20.0 || width < 1.0)
						System.out.println("Out of range!");
				} while(width > 20.0 || width < 1.0);
				
				do { //read height
					System.out.print("Input the height of the rectangle: ");
					height = in.nextFloat();
					
					if (height > 20.0 || height < 1.0)
						System.out.println("Out of range!");
				} while(height > 20.0 || height < 1.0);
				
				((Rectangle)ss[i]).setWidth(width);
				((Rectangle)ss[i]).setHeight(height);
			
 			}
			else { //when the shape is circle
				ss[i] = new Circle();
				
				do { //read radius
					System.out.print("Input the radius of the circle: ");
					radius = in.nextFloat();
					
					if (radius > 20.0 || radius < 1.0)
						System.out.println("Out of range!");
				} while(radius > 20.0 || radius < 1.0);
				
				((Circle)ss[i]).setRadius(radius);
			}
		}
		
		for (i = 0, total = 0.0f; i < num; i++) { //output the areas of shapes
			System.out.println("The area of the shape is " + ss[i].getArea());
			total += ss[i].getArea();
		}

		System.out.println("The area sum of the shapes is " + total); //output the total area
		
		in.close();
	}

}
