package ce1002.q1.s101201046;

public class Rectangle extends Shape {
	private float width;
	private float height;
	private int method;
	
	Rectangle() {
		method = 1;
	}
	
	public void setWidth(float _width) { //set width
		width = _width;
	}
	
	public void setHeight(float _height) { //set height
		height = _height;
	}
	
	public float getWidth() { //get width
		return width;
	}
	
	public float getHeight() { //get height
		return height;
	}
	
	public float getArea() { // get the area of rectangle
		return height * width;
	}
}
