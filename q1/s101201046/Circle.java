package ce1002.q1.s101201046;

public class Circle extends Shape{
	private float radius;
	private int method;
	
	Circle() { //construct circle
		method = 2;
	}
	
	public void setRadius(float _radius) { //set radius
		radius = _radius;
	}
	
	public float getRadius() { //get radius
		return radius;
	}
	
	public float getArea() { //get the area of circle
		return radius * radius * (float)java.lang.Math.PI;
	}
}
