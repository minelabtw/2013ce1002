package ce1002.q1.s102502530 ;
import java.util.Scanner ;

public class Q1
{
   public static void main(String[] args)
   {
      Scanner scanner = new Scanner(System.in) ;   //declare
      Shape[] shape = new Shape[10] ;
      int n, m ;
      float t, area = 0 ;

      System.out.println("Making shapes") ;  //Start

      do    //input the number of shapes
      {
         System.out.print("Input the number of shapes to be create: ") ;
         n = scanner.nextInt() ;
         if(n < 1 || n > 10)
            System.out.println("Out of range!") ;
      } while(n < 1 || n > 10) ;

      for(int i = 0 ; i != n ; i++)
      {
         do    //input which shape to create
         {
            System.out.print("1)Create rectangle, 2)Create circle: ") ;
            m = scanner.nextInt() ;
            if(m != 1 && m != 2)
               System.out.println("Out of range!") ;
         } while(m != 1 && m != 2) ;

         if(m == 1)  //create rectangle
         {
            shape[i] = new Rectangle() ;

            do    //input the width
            {
               System.out.print("Input the width of the rectangle: ") ;
               t = scanner.nextFloat() ;
               if(t < 1 || t > 20)
                  System.out.println("Out of range!") ;
            } while(t < 1 || t > 20) ;
            shape[i].setWidth(t) ;

            do    //input the height
            {
               System.out.print("Input the height of the rectangle: ") ;
               t = scanner.nextFloat() ;
               if(t < 1 || t > 20)
                  System.out.println("Out of range!") ;
            } while(t < 1 || t > 20) ;
            shape[i].setHeight(t) ;
         }
         else  //create circle
         {
            shape[i] = new Circle() ;

            do    //input radius
            {
               System.out.print("Input the radius of the circle: ") ;
               t = scanner.nextFloat() ;
               if(t < 1 || t > 20)
                  System.out.println("Out of range!") ;
            } while(t < 1 || t > 20) ;
            shape[i].setRadius(t) ;
         }
      }

      for(int i = 0 ; i != n ; i++)    //output
      {
         t = shape[i].getArea() ;
         System.out.println("The area of the shape is " + t) ;
         area += t ;
      }

      System.out.println("The area sum of the shapes is " + area) ;
   }
}
