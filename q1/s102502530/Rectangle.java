package ce1002.q1.s102502530 ;

public class Rectangle extends Shape
{
   private float width ;   //declare
   private float height ;

   public void setWidth(float width)   //set width
   {
      this.width = width ;
   }
   
   public void setHeight(float height)    //set height
   {
      this.height = height ;
   }

   public float getWidth()    //get width
   {
      return width ;
   }

   public float getHeight()   //get height
   {
      return height ;
   }

   public float getArea()  //get area
   {
      return width * height ;
   }
}
