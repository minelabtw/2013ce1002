package ce1002.q1.s102502530 ;

public class Circle extends Shape
{
   private float radius ;  //declare

   public void setRadius(float radius)    //set radius
   {
      this.radius = radius ;
   }

   public float getRadius()   //get radius
   {
      return radius ;
   }

   public float getArea()  //get area
   {
      return (float) Math.PI * radius * radius ;
   }
}

