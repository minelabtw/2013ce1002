package ce1002.q1.s102502528;

public class Circle extends Shape{
	private float radius;
	public void setRadius(float radius) {
		this.radius = radius;
	}
	public float getRadius() {
		return radius;
	}
	@Override
	public float getArea(){
		return radius * radius * 314/100;  //override and return Area 
	}

}
