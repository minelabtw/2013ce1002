package ce1002.q1.s102502528;

public class Rectangle extends Shape{
	private float width;
	private float height;
	public void setWidth(float width) {
		this.width = width;
	}
	public float getWidth() {
		return width;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getHeight() {
		return height;
	}
	@Override
	public float getArea(){
		return width * height;  //override and return Area 
	}
	

}
