package ce1002.q1.s102502528;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		float width,height;
		float radius;
		Rectangle r = new Rectangle();
		Circle c = new Circle();
		Scanner input = new Scanner(System.in); 
		System.out.println("Making shapes.");
		do{              //judge the input
			System.out.println("Input the width of the rectangle: ");
			width = input.nextFloat();
			if(width < 1 || width > 20){
				System.out.println("Out of range!");
			}
		}while(width < 1 || width > 20);
		do{               //judge the input
			System.out.println("Input the height of the rectangle: ");
			height = input.nextFloat();
			if(height < 1 || height > 20){
				System.out.println("Out of range!");
			}
		}while(height < 1 || height > 20);
		do{              //judge the input
			System.out.println("Input the radius of the circle: ");
			radius = input.nextFloat();
			if(radius < 1 || radius > 20){
				System.out.println("Out of range!");
			}
		}while(radius < 1 || radius > 20);
		input.close();   //close the scanner
		r.setWidth(width); //set the width of the rectangle
		r.setHeight(height);//set the height of the rectangle 
		c.setRadius(radius);//set the radius of the circle
		System.out.println("The area of the rectangle is " + r.getArea());  //output
		System.out.println("The area of the circle is " + c.getArea());    //output
		System.out.println("The area sum of the shapes is " + (r.getArea()+c.getArea()));  //output
		//why should we set method "getWidth""getHeight"...etc ?
	}
}
