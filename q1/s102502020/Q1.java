package ce1002.q1.s102502020;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float width = 0;                                                       //宣告長、寬、半徑，介於1~20之間
		float height = 0;
		float radius = 0;
		Scanner input = new Scanner(System.in);
		System.out.println("Making shapes.");                                  //建矩形和圓形
		do{
		    System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			if(width < 1 || width > 20){
				System.out.println("Out of range!");
			}
		}while(width < 1 || width > 20);
		do{
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			if(height < 1 || height > 20){
				System.out.println("Out of range!");
			}
		}while(height < 1 || height > 20);
		do{
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
			if(radius < 1 || radius > 20){
				System.out.println("Out of range!");
			}
		}while(radius < 1 || radius > 20);
		input.close();
		Rectangle rectangle = new Rectangle();                                  //呼叫CLASS
		Circle circle = new Circle(); 
        rectangle.setWidth(width);
        rectangle.setHeight(height);
        circle.setRadius(radius);
		System.out.println("The area of the rectangle is " + rectangle.getArea());                           //矩形面積
		System.out.println("The area of the circle is " + circle.getArea());                                 //圓形面積
		System.out.println("The area sum of the shapes is " + (rectangle.getArea() + circle.getArea()));     //總面積
	}

}
