package ce1002.q1.s102502529;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s=new Scanner(System.in);
		System.out.println("Making shapes.");		
		Rectangle re=new Rectangle();								//宣告物件
		do{															//判斷
			System.out.print("Input the width of the rectangle: ");
			float width=s.nextFloat();
			if(1<=width&&width<=20){
				re.setWidth(width);
				break;
			}						
			else
				System.out.println("Out of range!");
		}while(true);
		do{																		//判斷
			System.out.print("Input the height of the rectangle: ");
			float height=s.nextFloat();
			if(1<=height&&height<=20){
				re.setHeight(height);
				break;
			}
		}while(true);
		Circle cir=new Circle();
		do{																		//判斷
			System.out.print("Input the radius of the circle: ");
			float radius=s.nextFloat();
			if(1<=radius&&radius<=20){
				cir.setRadius(radius);
				break;
			}
			else
				System.out.println("Out of range!");
		}while(true);
		float sum=cir.getArea()+re.getArea();									//儲存輸入面積的和
		System.out.println("The area of the rectangle is "+re.getArea());		//輸出
		System.out.println("The area of the circle is "+cir.getArea());
		System.out.println("The area sum of the shapes is "+sum);
		s.close();
		
		
	}
	

}
