package ce1002.q1.s102502033;

public class Triangle extends Graph
{
	private double a;//三邊
	private double b;
	private double c;
	
	Triangle(double e,double f,double g)
	{
		a=e;
		b=f;
		c=g;
		setEdge(3);		
	}
	
	public boolean Recongnization()
	{
		if(a+b<c || b+c<a || a+c<b || a<=0 || b<=0 || c<=0)//判別圖形是否合理
		{
			return false;
		}
		else
			return true;
		
	}
	public double getArea()
	{
		double s;
		s=(a+b+c)/2;
		if(Recongnization()==false)
		{
			return 0;
		}
		else 
			return Math.sqrt(s*(s-a)*(s-b)*(s-c));//三角形面積
	}

}
