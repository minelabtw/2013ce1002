package ce1002.q1.s102502545;

import java.util.Scanner;

public class Triangle extends Graph {
	private double a;//設立變數
	private double b;
	private double c;// 三角形三邊

	public Triangle(double a, double b, double c) {
		this.a = a;//建構函式
		this.b = b;
		this.c = c;
	}

	public boolean Recongnization() {
		if ((a + b) <= c || (a + c) <= b || (b + c) <= a)//利用布林代數檢查邊長是否有問題
			return false;
		else
			return true;
	}

	public double getArea() {//計算面積
		if ((a + b) <= c || (a + c) <= b || (b + c) <= a)
			return 0;
		else {
			double s = (a + b + c) / 2;
			return Math.sqrt(s * (s - a) * (s - b) * (s - c));
		}
	}
}
