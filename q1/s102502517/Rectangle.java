package ce1002.q1.s102502517;

import ce1002.q1.s102502517.Shape;

public class Rectangle extends Shape{ //繼承
	private float width;
	private float height;
	
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	
	public float getArea()
	{
		return width*height;
	}
}
