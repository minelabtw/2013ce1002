package ce1002.q1.s102502517;

import ce1002.q1.s102502517.*;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		int numberofshape = 0;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Making shapes.");
		do //輸入數量
		{
		System.out.print("Input the number of shapes to be create: ");
		numberofshape = scanner.nextInt();
		if(numberofshape<1 || numberofshape>10)
			System.out.println("Out of range!");
		}
		while(numberofshape<1 || numberofshape>10);
		
		Shape[] shape = new Shape[numberofshape]; //宣告Shape class
		int choice = 0;
		
		for(int i=0; i<=numberofshape-1; i++)
		{
			System.out.print("1)Create rectangle, 2)Create circle: "); //選擇
			choice = scanner.nextInt();
			if(choice==1)
			{
				shape[i] = new Rectangle(); //實體化shape[i]為Rectangle class
				do
				{
					System.out.print("Input the width of the rectangle: ");
					((Rectangle) shape[i]).setWidth(scanner.nextFloat());
					if(((Rectangle) shape[i]).getWidth()<1 || ((Rectangle) shape[i]).getWidth()>20)
						System.out.println("Out of range!");
				}
				while(((Rectangle) shape[i]).getWidth()<1 || ((Rectangle) shape[i]).getWidth()>20);
				
				do
				{
				System.out.print("Input the height of the rectangle: ");
				((Rectangle) shape[i]).setHeight(scanner.nextFloat());
				if(((Rectangle) shape[i]).getHeight()<1 || ((Rectangle) shape[i]).getHeight()>20)
					System.out.println("Out of range!");
				}
				while(((Rectangle) shape[i]).getHeight()<1 || ((Rectangle) shape[i]).getHeight()>20);
			}
			
			else if(choice==2)
			{
				shape[i] = new Circle(); //實體化shape[i]為Circle class
				do
				{
					System.out.print("Input the radius of the circle: ");
					((Circle) shape[i]).setRadius(scanner.nextFloat());
					if(((Circle) shape[i]).getRadius()<1 || ((Circle) shape[i]).getRadius()>20)
						System.out.println("Out of range!");
				}
				while(((Circle) shape[i]).getRadius()<1 || ((Circle) shape[i]).getRadius()>20);
			}
			
			else
			{
				System.out.println("Out of range!");
				i--;
			}
		}
		
		float sum = 0;
		
		for(int j=0; j<=numberofshape-1; j++) //輸出結果
		{
			System.out.println("The area of the shape is " + shape[j].getArea());
			sum += shape[j].getArea();
		}
		
		System.out.println("The area sum of the shapes is " + sum);
	}

}
