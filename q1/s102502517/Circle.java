package ce1002.q1.s102502517;

import ce1002.q1.s102502517.Shape;

public class Circle extends Shape{ //繼承
	private float radius;

	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getArea()
	{
		return (float) (radius*radius*Math.PI);
	}
}
