package ce1002.q1.s102502011;

public class Circle extends Shape {
	private float radius ;
	
	public void setRadius(float radius) { //setter
		this.radius = radius;
		
		this.area = (float) (radius * radius *3.14) ;
	}
	
	public float getRadius() { //getter
		return radius;
	}
	

}
