package ce1002.q1.s102502011;

public class Rectangle extends Shape {
	private float width ;
	private float height ;

	public void setWidth(float width) { //setter
		this.width = width;
	}
	
	public void setHeight(float height) {
		this.height = height;
	}
	
	public void setArea(float width , float height) {
		this.area = width*height;
	}
	
	public float getWidth() { //getter
		return width;
	}
	
	public float getHeight() {
		return height;
	}
	
	

}
