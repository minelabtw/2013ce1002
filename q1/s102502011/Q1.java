package ce1002.q1.s102502011;
import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in) ;
		
		System.out.println("Making shapes.") ;
		
		//int create , choose ;
		float width = 0 , height = 0 , radius = 0 ;
		
		/*do {
			System.out.print("Input the number of shapes to be create: ") ; //input number
			create = input.nextInt() ;
			if (create < 1 || create > 10 )
				System.out.println("Out of range!") ;
		}
		while (create < 1 || create > 10 ) ;
		
		Shape[] shapes = new Shape[(int) create] ;
		
		do {
			System.out.print("1)Create rectangle, 2)Create circle: ") ; //choose
			choose = input.nextInt() ;
			if (choose!=1 || choose!=2 )
				System.out.println("Out of range!") ;
		}
		while (choose!=1 || choose!=2 ) ;*/
		
		do {
			System.out.print("Input the width of the rectangle: ") ; //input width
			width = input.nextFloat() ;
			if (width < 1 || width > 20 )
				System.out.println("Out of range!") ;
		}
		while (width < 1 || width > 20 ) ;
	     	     
	     do {
				System.out.print("Input the height of the rectangle: ") ; //input height
				height = input.nextFloat() ;
				if (height < 1 || height > 20 )
					System.out.println("Out of range!") ;
			}
			while (height < 1 || height > 20 ) ;
	     
	     do {
				System.out.print("Input the radius of the rectangle: ") ; //input radius
				radius = input.nextFloat() ;
				if (radius < 1 || radius > 20 )
					System.out.println("Out of range!") ;
			}
			while (radius < 1 || radius > 20 ) ;
	     
	     Rectangle rectangle = new Rectangle() ; //
	     rectangle.setWidth(width);
	     rectangle.setHeight(height); 
	     Circle circle = new Circle() ;
	     circle.setRadius(radius);
	     rectangle.setArea(width, height);
	        
	     System.out.println("The area of the rectangle is " + rectangle.getArea() ) ; //output
		 System.out.println("The area of the circle is " + circle.getArea() ) ;
		 System.out.println("The area sum of the shapes is " + (rectangle.getArea() + circle.getArea()) ) ;
		
		
		input.close();
	}

}
