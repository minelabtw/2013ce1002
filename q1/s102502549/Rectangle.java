package ce1002.q1.s102502549;

public class Rectangle extends Graph {

	// member
	private double height;
	private double weight;

	// costructor
	public Rectangle(double height, double weight) {
		super.setEdge(4);
		this.height = height;
		this.weight = weight;
	}

	// method
	public boolean Recognization() {
		return (height > 0 && weight > 0);
	}

	public double getArea() {
		if (Recognization() == true) {
			return height * weight;
		} else {
			return 0;
		}
	}
}
