package ce1002.q1.s102502549;

public class Triangle extends Graph {

	// member
	private double a;
	private double b;
	private double c;

	// costructor
	public Triangle(double a, double b, double c) {
		super.setEdge(3);
		this.a = a;
		this.b = b;
		this.c = c;
	}

	// method
	public boolean Recognization() {
		return (a + b > c && a + c > b && b + c > a && a > 0 && b > 0 && c > 0);
	}

	public double getArea() {

		if (Recognization() == true) {
			double s = (a + b + c) / 2;
			return Math.sqrt(s * (s - a) * (s - b) * (s - c));
		} else {
			return 0;
		}
	}
}
