package ce1002.q1.s102502549;

public class Graph {

	// constructor
	public Graph() {
	}

	// member
	protected double area;
	private int edge;

	// method
	public void setEdge(int e) {
		this.edge = e;
	}

	public int getEdge() {
		return edge;
	}

	public boolean Recognization() {
		return true;
	}

	public double getArea() {
		return area;
	}
}
