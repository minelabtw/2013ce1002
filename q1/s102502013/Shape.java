package ce1002.q1.s102502013;

public class Shape {
	private float Width;
	private float Radius;
	private float Height;
	private float Area;
	Shape(){
		Width = 0;
		Radius = 0;
		Height = 0;
		Area = 0;
	}
	public void setWidth(float w){
		Width = w;
	}
	public void setRadius(float r){
		Radius = r;
	}
	public void setHeight(float h){
		Height = h;
	}
	public float getWidth(){
		return Width;
	}
	public float getRadius(){
		return Radius;
	}
	public float getHeight(){
		return Height;
	}
	public float getArea(){
		return 0;
	}
}
