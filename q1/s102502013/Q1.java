package ce1002.q1.s102502013;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		float w = 0;
		float h = 0;
		float r = 0;
		float sum;
		Scanner input = new Scanner(System.in);
		Retangle retangle = new Retangle();
		Circle circle = new Circle();
		System.out.println("Making shapes.");
		System.out.print("Input the width of the retangle: ");
		w = input.nextFloat();
		while(w<1||w>20){
			System.out.println("Out of range!");
			System.out.print("Input the width of the retangle: ");
			w = input.nextFloat();
		}
		retangle.setWidth(w);
		System.out.print("Input the height of the retangle: ");
		h = input.nextFloat();
		while(h<1||h>20){
			System.out.println("Out of range!");
			System.out.print("Input the height of the retangle: ");
			h = input.nextFloat();
		}
		retangle.setHeight(h);
		System.out.print("Input the radius of the circle: ");
		r = input.nextFloat();
		while(r<1||r>20){
			System.out.println("Out of range!");
			System.out.print("Input the radius of the circle: ");
			r = input.nextFloat();
		}
		circle.setRadius(r);
		System.out.println("The area of the retangle is " + retangle.getArea());
		System.out.println("The area of the circle is " + circle.getArea());
		sum = retangle.getArea() + circle.getArea();
		System.out.println("The area sum of the shapes is " + sum);
	}
	

}
