package ce1002.q1.s102502013;

public class Circle extends Shape{
	private float PI;
	Circle(){
		PI = (float) 3.14;
	}
	public void setRadius(float r){
		super.setRadius(r);
	}
	public float getRadius(){
		return super.getRadius();
	}
	public float getArea(){
		return PI * super.getRadius() * super.getRadius();
	}
}
