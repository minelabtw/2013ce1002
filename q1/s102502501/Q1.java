package ce1002.q1.s102502501;
import java.util.Scanner;
public class Q1 {
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int w ;
		System.out.println("Making shapes.");
		System.out.print("Input the width of the rectangle: ");
		w = input.nextInt();
	while( w < 1 || w > 20) 
	{
		System.out.println("Out of range!");
		System.out.print("Input the width of the rectangle: ");
		w = input.nextInt();	
	} 
	
}
}
