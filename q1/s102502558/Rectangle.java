package ce1002.q1.s102502558;

public class Rectangle extends Graph {

	private double height;
	private double width;
	Rectangle(double w, double h)
	{
		// set the parameter
		this.height = h;
		this.width = w;
		setEdge(4);
	}
	@Override
	public boolean Recongnization()
	{
		// must be greater than zero
		return height >= 0 && width >= 0;
	}
	@Override
	public double getArea()
	{
		// if not reasonable return 0
		if (Recongnization() == false)
			return super.getArea();
		// else return area
		return width * height;
	}
}
