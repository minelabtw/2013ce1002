package ce1002.q1.s102502558;

public class Triangle extends Graph {

	private double a;
	private double b;
	private double c;
	Triangle(double a, double b, double c)
	{
		// set three edge
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge(3);
	}
	@Override
	public boolean Recongnization()
	{
		// check reasonable
		return (a+b>c) && (b+c>a) && (a+c>b) && a >= 0 && b >= 0 && c >= 0;
	}
	@Override
	public double getArea()
	{
		// if not reasonable then return 0
		if (Recongnization() == false)
			return super.getArea();
		// else we calculate the area
		double s = (a+b+c) / 2;
		return Math.sqrt(s * (s-a) * (s-b) * (s-c));
	}

}
