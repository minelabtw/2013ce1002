package ce1002.q1.s102502558;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// create a rectangle object and a triangle object
		Graph[] graph = new Graph[2];
		
		// create a scanner object and let it parses the standard input.
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and triangle.
		double weight,height; // variable for rectangle's constructor
		double a,b,c; // variable for triangle's constructor
		
		
		//This part is handling the input to construct the rectangle object
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight=input.nextDouble();
		System.out.println("Please input Height: ");
		height=input.nextDouble();
		graph[0] =new Rectangle(weight,height);
		
		
		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();
		graph[1] = new Triangle(a,b,c);
		
		
		// print out each area from different shape of graph
		for (int i=0;i<2;i++)
			System.out.println("Area(" + i + ") = " + graph[i].getArea() + " Edge = " + graph[i].getEdge());
		input.close();
	}

}
