package ce1002.q1.s102502516;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		float temp, totalArea = 0; // temp暫存、totalArea計算總面積
		int number, choose; // number圖形數、choose使用者選擇

		System.out.println("Making shapes.");
		do {
			System.out.print("Input the number of shapes to be create: ");
			number = scanner.nextInt();
			if (number < 1 || number > 10)
				System.out.println("Out of range!");
		} while (number < 1 || number > 10);
		Shape[] shapes = new Shape[number]; // 詢問並設定圖形數量

		for (int i = 0; i < shapes.length; i++) { // 逐一設定
			do {
				System.out.print("1)Create rectangle, 2)Creat circle: ");
				choose = scanner.nextInt();
				if (choose < 1 || choose > 2)
					System.out.println("Out of range!");
			} while (choose < 1 || choose > 2);
			if (choose == 1) {
				shapes[i] = new Rectangle();
				do {
					System.out.print("Input the width of the rectangle: ");
					temp = scanner.nextFloat();
					if (temp < 1 || temp > 20)
						System.out.println("Out of range!");
				} while (temp < 1 || temp > 20);
				shapes[i].setWidth(temp);
				do {
					System.out.print("Input the height of the rectangle: ");
					temp = scanner.nextFloat();
					if (temp < 1 || temp > 20)
						System.out.println("Out of range!");
				} while (temp < 1 || temp > 20);
				shapes[i].setHeight(temp); // 矩型設定
			} else {
				shapes[i] = new Circle();
				do {
					System.out.print("Input the radius of the rectangle: ");
					temp = scanner.nextFloat();
					if (temp < 1 || temp > 20)
						System.out.println("Out of range!");
				} while (temp < 1 || temp > 20);
				shapes[i].setRadius(temp); // 圓形設定
			}
		}
		scanner.close(); // 完成設定

		for (int i = 0; i < shapes.length; i++) {
			System.out.println("The area of the shape is "
					+ shapes[i].getArea());
			totalArea += shapes[i].getArea();
		} // 印出每一圖形面積並累計
		System.out.println("The area sum of the shapes is " + totalArea); // 印出總面積
	}
}
