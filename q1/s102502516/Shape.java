package ce1002.q1.s102502516;

public class Shape {
	protected float area, width, height, radius;

	// 以下是對於各種變數的設定和回傳函式
	public float getArea() {
		return area;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
}
