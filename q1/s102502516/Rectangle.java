package ce1002.q1.s102502516;

public class Rectangle extends Shape { // 繼承自shape
	// 設定長寬
	@Override
	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getArea() {
		return super.getHeight() * super.getWidth();
	} // 利用override回傳矩型面積

}
