package ce1002.q1.s102502516;

public class Circle extends Shape {// 繼承自shape
	// 設定半徑
	@Override
	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getArea() {
		return (float) (Math.pow(super.getRadius(), 2) * Math.PI);
	} // 利用override回傳圓面積

}
