package ce1002.q1.s102502028;

public class Triangle extends Graph {
	private double a ;
	private double b ;
	private double c ;
	
    Triangle(double a , double b , double c)  //初始化長寬 邊數
    {
    	this.a = a ;
    	this.b = b ;
    	this.c = c ;
    	setEdge(3) ;
    }
	public boolean Recongnization()  //判斷是否合理
	{

		if (a <= 0 || b <= 0 || c <= 0 || (a+b < c) || (a+c < b ) || (b+c < a))
		return  false ;
		else
			return true ;
	}
	
	public double getArea()  //回傳area
	{
		if (Recongnization() == false)
			return 0 ;
		else
		{
		double s = (a+b+c)/2 ;
		area = Math.sqrt(s*(s-a)*(s-b)*(s-c)) ;
		return super.getArea() ;
		}		
	}
}
