package ce1002.q1.s102502028;

public class Rectangle extends Graph {
	private double height ;  //宣告
	private double width ;
	
	Rectangle(double width , double height)  //初始化長寬 邊數
	{
		this.width = width ;
		this.height = height ;
		setEdge(4) ;
	}
	
	public boolean Recongnization()
	{
		if (height <= 0 || width <= 0)
		return  false ;
		else
			return true ;
	}
	
	public double getArea() //回傳面積
	{
		if (Recongnization() == false)
		return 0 ;
		else
			return height*width ;
			  
	}
}
