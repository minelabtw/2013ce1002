package ce1002.q1.s102502028;

public class Graph {
	protected double area ; //存圖的面積
	private int edge ; //存圖的邊數
	
	public void setEdge(int e) //設定邊長
	{
		this.edge = e ;
	}
	
	public int getEdge()  //回傳邊長
	{
		return  edge ;
	}
	
	public boolean Recongnization()
	{
		return  true ;
	}
	
	public double getArea()  //回傳面積
	{
		return area ;	
	}
	
}
