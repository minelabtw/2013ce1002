package ce1002.q1.s101303504;

public class Rectangle extends Graph{
	
	private double height;
	private double width;//矩形長寬
	
	public Rectangle(){
		
	}
	
	public Rectangle(double height,double width){
		this.height = height;
		this.width = width;
	}
	
	public boolean Recongnization(){//判斷正負
		boolean rec;
		if(height<=0 || width<=0){
			rec = false;
			return rec;
		}
		else{
			rec = true;
			return rec;
		}
	}
	
	public double getArea(){//輸出面積
		double area;
		if(height<=0 || width<=0){
			area = 0;
		}
		else{
			area = height * width ;
		}
		return area;
	}
}
