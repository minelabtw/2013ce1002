package ce1002.q1.s101303504;

public class Triangle extends Graph{

	private double a;
	private double b;
	private double c;//三角形三邊
	
	public Triangle(){
		
	}
	
	public Triangle(double a,double b,double c){
		this.a=a;
		this.b=b;
		this.c=c;
	}
	
	public boolean Recongnization(){//判斷邊長正負以及兩編之和大於第三邊
		boolean rec=false;
		if(a<=0 || b<=0 || c<=0){
			rec = false;
		}
		if(a>0 && b>0 && c>0){
			if(a>=b && a>=c){
				if((b+c)<=a)
					rec = false;
				else
					rec = true;
			}
			if(b>=a && b>=c){
				if((a+c)<=b)
					rec = false;
				else
					rec = true;
			}
			if(c>=a && c>=b){
				if((a+b)<=c)
					rec = false;
				else
					rec = true;
					
			}
		}
		return rec;
	}
	public double getArea(){//面積
		double area;
		double s;
		if(a>0 && b>0 && c>0)
		{
			s=(a+b+c)/2;
			area=Math.sqrt(s*(s-a)*(s-b)*(s-c));
			if(a>b && a>c)
			{
				if(a>(b+c))
					area=0;
			}
			if(b>a && b>c)
			{
				if(b>(a+c))
					area=0;
			}
			if(c>a && c>b)
			{
				if(c>(a+b))
					area = 0;
			}
		}
		else{
			area = 0;
		}
		return area;
	}
}
