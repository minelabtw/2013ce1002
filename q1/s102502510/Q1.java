package ce1002.q1.s102502510;
import java.util.Scanner;
public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);//create a new Scanner
		System.out.println("Making shapes.");
		int n;
		do 
		{
			System.out.print("Input the number of shapes to be create: ");
			n=input.nextInt();
			if(n>10||n<1)
			{
				System.out.println("Out of range!");
			}
		}while(n>10||n<1);
		Shape s[]=new Shape[n];// create a array of Shapes
		for(int i=0;i<n;++i)
		{
			int choice;
			do 
			{
				System.out.print("1)Create rectangle, 2)Create circle: ");
				choice=input.nextInt();
				if(choice>2||choice<1)
				{
					System.out.println("Out of range!");
				}
			}while(choice>2||choice<1);
			if(choice==1)
			{
				Rectangle r=new Rectangle();
				float wid;
				float hei;
				do
				{
					System.out.print("Input the width of the rectangle: ");
					wid=input.nextFloat();
					if(wid<1||wid>20)
					{
						System.out.println("Out of range!");
					}
					
				}while(wid<1||wid>20);
				r.setWidth(wid);
				do
				{
					System.out.print("Input the height of the rectangle: ");
					hei=input.nextFloat();
					if(hei<1||hei>20)
					{
						System.out.println("Out of range!");
					}
				}while(hei<1||hei>20);
				r.setHeight(hei);
				s[i]=r;
			}
			else
			{
				Circle c=new Circle();
				float r;
				do
				{
					System.out.print("Input the radius of circle: ");
					r=input.nextFloat();
					if(r<1||r>20)
					{
						System.out.println("Out of range!");
					}
				}while(r<1||r>20);
				c.setRadius(r);
				s[i]=c;
			}
		}
		float sumArea=0;
		for(int i=0;i<n;++i)
		{
			sumArea+=s[i].getArea();
			System.out.println("The area of the shape is "+s[i].getArea());
		}
		System.out.print("The area sum of the shapes is "+sumArea);
	}

}
