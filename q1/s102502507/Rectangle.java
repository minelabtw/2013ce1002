package ce1002.q1.s102502507;

public class Rectangle extends Shape{
	public void setWidth(float width)
	{
		this.Width=width;
	}
	public float getWidth()
	{
		return Width;
	}
	public void setHeight(float height)
	{
		 this.Height=height;
	}
	public float getHeight()
	{
		return Height;
	}
	public float getArea()
	{
		return Height*Width;
	}
}
