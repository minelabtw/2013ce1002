package ce1002.q1.s102502507;

public class Shape {

	public float Width;
	public float Height;
	public float Radius;
	public float Area;

	public void setWidth(float width)
	{
		this.Width=width;
	}
	public float getWidth()
	{
		return Width;
	}
	public void setHeight(float height)
	{
		 this.Height=height;
	}
	public float getHeight()
	{
		return Height;
	}

	public void setRadius(float radius)
	{
		this.Radius=radius;
	}
	public float getRadius()
	{
		return Radius;
	}

	public float getArea()
  {
	  return 0;
  }


}
