package ce1002.q1.s102502507;
import java.util.Scanner;
public class Q1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Scanner input = new Scanner(System.in);
      float width;
      float height;
      float radius;
  Shape shapes= new Shape();
	System.out.println("Making shapes.");
	do
	{
	System.out.println("Input the width of the rectangle: ");
	width= input.nextFloat();
	if(width>20 || width<1)
	{
		System.out.println("Out of range!");
	}
	}
	while(width>20 || width<1);
	do
	{
	System.out.println("Input the height of the rectangle: ");
	height = input.nextFloat();
	if(height>20 || height<1)
	{
		System.out.println("Out of range!");
	}
	}
	while(height>20 || height<1);
	
	do
	{
	System.out.println("Input the radius of the circle: ");
	radius= input.nextFloat();
	if(radius>20 || radius<1)
	{
		System.out.println("Out of range!");
	}
	}
	while(radius>20 || radius<1);
	input.close();
	
	System.out.println("The area of the rectangle is "+shapes.getArea());
	System.out.println("The area of the circle is "+shapes.getArea());
	System.out.println("The area of the shapes is "+shapes.getArea());
	
	}

}
