package ce1002.q1.s102502030;

public class Rectangle extends Graph {

	private double height;
	private double width;
	
	public Rectangle( double w, double h )
	{
		this.width = w;
		this.height = h;
		setEdge( 4 );
	}
	public boolean Recongnization()
	{
		if( width<0 || height<0 ) {
			return false;
		}
		else {
			return true;
		}
	}
	public double getArea()
	{
		boolean result;
		result = Recongnization();
		if( result == false ) {
			return 0;
		}
		else {
			return height*width;
		}
	}
}