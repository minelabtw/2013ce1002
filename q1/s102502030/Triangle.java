package ce1002.q1.s102502030;

public class Triangle extends Graph {

	private double a;
	private double b;
	private double c;
	
	public Triangle( double a, double b, double c )
	{
		this.a = a;
		this.b = b;
		this.c = c;
		setEdge( 3 );
	}
	public boolean Recongnization()
	{
		if( a<0 || b<0 || c<0 || a+b<c || a+c<b || b+c<a ) {
			return false;
		}
		else {
			return true;
		}
	}
	public double getArea()
	{
		boolean result;
		double s = (a+b+c)/2;
		result = Recongnization();
		if( result == false ) {
			return 0;
		}
		else {
			return Math.sqrt(s*(s-a)*(s-b)*(s-c) );
		}
	}
	
}