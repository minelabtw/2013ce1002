package ce1002.q1.s102502511;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		
		System.out.println("Making shapes.");
		float width; //寬
		float height; //高
		float radius; //半徑
		int type; //型態
		int number; //個數
		float sum = 0; //計算總面積
		
		Scanner input = new Scanner(System.in);
		
		do{ //判斷個數是否正確
			System.out.print("Input the number of shapes to be create: ");
			number = input.nextInt();
			if(number < 1 || number > 10){
				System.out.println("Out of range!");
			}
		}while(number < 1 || number > 10);
		
		Shape [] shapes = new Shape [number]; //製造陣列來使用Shape類別
		
		for(int i = 0 ; i < number ; i ++)
		{
			
			shapes[i] = new Shape(); //陣列等於Shape類別的建構值
			
			do{ //判斷是否type符合條件
				System.out.print("1)Create rectangle, 2)Create circle: ");
				type = input.nextInt();
				if(type != 1 && type != 2){
					System.out.println("Out of range!");
				}
			}while(type != 1 && type != 2);
			
			switch(type){
				case 1:{
					
					shapes[i] = new Rectangle(); //當type等於1時 使用Rectangle();
					
					do{ //判斷寬是否正確
						System.out.print("Input the width of the rectangle: ");
						width = input.nextFloat();
						if(width < 1 || width >20){
							System.out.println("Out of range!");
						}
					}while(width < 1 || width >20);
					shapes[i].setWidth(width); //用陣列去存取width
					
					do{ //判斷高是否正確
						System.out.print("Input the height of the rectangle: ");
						height = input.nextFloat();
						if(height < 1 || height >20){
							System.out.println("Out of range!");
						}
					}while(height < 1 || height >20);
					shapes[i].setHeight(height); //用陣列存取高
					
					break; //使用完跳出
				}
				case 2:{
					
					shapes[i] = new Circle(); //當type等於2時 陣列等於Circle()
					
					do{ //判斷半徑是否正確
						System.out.print("Input the radius of the rectangle: ");
						radius = input.nextFloat();
						if(radius < 1 || radius >20){
							System.out.println("Out of range!");
						}
					}while(radius < 1 || radius >20);
					shapes[i].setRadius(radius); //用陣列存取半徑
					
					break;
				}
			}
		}
		
		for(int i = 0 ; i < number ; i ++){
			System.out.println("The area of the shape is " + shapes[i].getArea()); //用多型將所有面積輸出
			sum +=(float)(shapes[i].getArea()); //面積總和等於每次面積連加
		}
		
		System.out.println("Ther area sum of the shapes is " + sum); //輸出總面積
		
		input.close();	
	}

}
