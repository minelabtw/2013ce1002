package ce1002.q1.s102502511;

import java.math.*;

public class Circle extends Shape{
	private float Radius;
	
	Circle(){
		
	}

	public float getRadius() { //回傳radius
		return Radius;
	}

	public void setRadius(float radius) { //儲存外來輸入的radius
		Radius = radius;
	}
	
	public float getArea(){ //回傳面積
		return (float) ( Radius * Radius * Math.PI); //用float形式
	}

}
