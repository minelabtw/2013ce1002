package ce1002.q1.s102502511;

public class Rectangle extends Shape {
	private float Width;
	private float Height;
	
	Rectangle(){
		
	}
	
	public float getWidth() { //回傳width
		return Width;
	}
	public void setWidth(float width) { //儲存外來的輸入的width
		Width = width;
	}
	public float getHeight() { //回傳height
		return Height;
	}
	public void setHeight(float height) { //儲存外來的輸入的height
		Height = height;
	}
	
	public float getArea(){ //回傳面積
		return (float) Width * Height; //用float形式
	}
}
