package ce1002.q1.s100502022;

public class Rectangle extends Shape{
	
	//class member
	private float width;
	private float height;
	//set,get
	public void setHeight(float h){
		this.width=h;
	}
	public void setWidth(float w){
		this.height=w;
	}
	public float getWidth(){
		return width;
	}
	public float getHeight(){
		return height;
	}
	//override
	public float getArea(){
		return width*height;
	}
}
