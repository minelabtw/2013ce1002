package ce1002.q1.s100502022;
import java.util.Scanner;
public class Q1 {
	public static void main(String a[]){
		Scanner input = new Scanner(System.in);
		System.out.println("Making shapes.");
		int number;
		//quantity of shapes
		do{
			System.out.print("Input the number of shapes to be create: ");
			number= input.nextInt();
			if(number<1||number>10){
				System.out.println("Out of range!");
			}
		}while(number<1||number>10);
		//make shape array
		Shape[] shape = new Shape[number];
		//loop for shapes
		for(int i=0;i<number;i++){
			int s;
			//shapes check
			do{
				System.out.print("1)Create rectangle, 2)Create circle: ");
				s= input.nextInt();
				if(s<1||s>2){
					System.out.println("Out of range!");
				}
			}while(s<1||s>2);
			//shape choose
			if(s==1){
				float width,height;
				//value check
				do{
					System.out.print("Input the width of the rectangle: ");
					width= input.nextFloat();
					if(width<1||width>20){
						System.out.println("Out of range!");
					}
				}while(width<1||width>20);
				do{
					System.out.print("Input the height of the rectangle: ");
					height= input.nextFloat();
					if(height<1||height>20){
						System.out.println("Out of range!");
					}
				}while(height<1||height>20);
				//new a object put it into shape array
				Rectangle r = new Rectangle();
				r.setHeight(height);
				r.setWidth(width);
				shape[i]=r;
			}
			else{
				float radius;
				do{
					System.out.print("Input the radius of the circle: ");
					radius= input.nextFloat();
					if(radius<1||radius>20){
						System.out.println("Out of range!");
					}
				}while(radius<1||radius>20);
				Circle c = new Circle();
				c.setRadius(radius);
				shape[i]=c;
			}
		}
		//close input
		input.close();
		float total=0;
		for(int i=0;i<number;i++){
			System.out.println("The are of the shape is "+shape[i].getArea());
			total+=shape[i].getArea();
		}
		System.out.println("The area sum of the shapes is "+total);
	}
}
