package ce1002.q1.s100502022;

public class Circle extends Shape{
	//class member
	private float radius;
	//set,get
	public void setRadius(float r){
		this.radius=r;
	}
	public float getRadius(){
		return radius;
	}
	//override
	public float getArea(){
		return (float)Math.PI*radius*radius;
	}
}
