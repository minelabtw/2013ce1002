package ce1002.q1.s102502513;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		Shape shape = new Shape();
		
		float width, height, radius;
		
		System.out.println("Making shapes.");
		
		do{                                                          //��J�x�μe��
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			
			if(width<1||width>20)
			{
				System.out.println("Out of range!");	
			}
		}while(width<1||width>20);
		
		do{
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			
			if(height<1||height>20)
			{
				System.out.println("Out of range!");	
			}
		}while(height<1||height>20);
		
		do{
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
			
			if(radius<1||radius>20)
			{
				System.out.println("Out of range!");	
			}
		}while(radius<1||radius>20);
		
	}

}
