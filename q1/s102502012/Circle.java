package ce1002.q1.s102502012;

public class Circle extends Shape{
	private float radius;

	// override method in Shape
	public float getArea(){
		return (radius * radius * (float)Math.PI);
	}
	
	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
}
