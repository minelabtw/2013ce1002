package ce1002.q1.s102502012;

public class Rectangle extends Shape{
	private float width;
	private float height;
	
	public float getArea(){
		return width * height;
	}
	
	// override method in Shape
	public float getWidth() {
		return width;
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	
	public float getHeight() {
		return height;
	}
	
	public void setHeight(float height) {
		this.height = height;
	}
}
