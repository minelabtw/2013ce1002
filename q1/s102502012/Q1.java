package ce1002.q1.s102502012;

import java.util.*;

public class Q1 {

	public static boolean outOfRange(){
		System.out.println("Out of range!");
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println("Making shapes.");
		Scanner input = new Scanner(System.in);
		int n;
		do{
			System.out.print("Input the number of shapes to be create: ");
			n = input.nextInt();
		}while((n < 1 || n > 10) && outOfRange());
		Shape[] shapes = new Shape[n];
		for(int i = 0; i < n; i++){
			int type;
			float radius, width, height;
			do{
				System.out.print("1)Create rectangle, 2)Create circle: ");
				type = input.nextInt();
			}while((type < 1 || type >2) && outOfRange());
			if(type == 1){ // input rectangle
				do{
					System.out.print("Input the width of the rectangle: ");
					width = input.nextFloat();
				}while((width < 1 || width > 20) && outOfRange());
				do{
					System.out.print("Input the height of the rectangle: ");
					height = input.nextFloat();
				}while((height < 1 || height > 20) && outOfRange());
				Rectangle rect = new Rectangle();
				rect.setHeight(height);
				rect.setWidth(width);
				shapes[i] = rect;
			}
			else{ // input circle
				do{
					System.out.print("Input the radius of the circle: ");
					radius = input.nextFloat();
				}while((radius < 1 || radius > 20) && outOfRange());
				Circle circ = new Circle();
				circ.setRadius(radius);
				shapes[i] = circ;
			}
		}
		float sum = 0;
		for(int i = 0; i < n; i++){
			float tmpArea = shapes[i].getArea();
			System.out.println("The area of the shape is " + tmpArea);
			sum += tmpArea;
		}
		System.out.println("The area sum of the shapes is " + sum);
		input.close();
	}

}
