package ce1002.q1.s102502503;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int num,choose;
		float w,h,r;  //寬度 高度 半徑
		System.out.println("Making shapes.");
		do{  //選擇幾個形狀
			System.out.print("Input the mumber of shapes to be create: ");
			num=input.nextInt();
			if (num>10 || num<1)
				System.out.println("Out of range!");
		}while (num>10 || num<1);
		Shape[] shape = new Shape[num];  //創陣列
		
		for (int i=0; i<num; i++){  
			Rectangle rectangle = new Rectangle();
			Circle circle = new Circle();
		do{  //選擇矩形或圓形
			System.out.print("1)Create rectangle, 2)Create circle: ");
			choose=input.nextInt();
			if (choose>2 || choose<1)
				System.out.println("Out of range!");
		}while (choose>2 || choose<1);
		if (choose==1)  //若選擇矩形
		{
			do{
				System.out.print("Input the width of the rectangle: ");
				w=input.nextFloat();
				if (w>20 || w<1)
					System.out.println("Out of range!");
			}while (w>20 || w<1);
			rectangle.setWidth(w);  //設定寬度
			
			do{
				System.out.print("Input the height of the rectangle: ");
				h=input.nextFloat();
				if (h>20 || h<1)
					System.out.println("Out of range!");
			}while (h>20 || h<1);
			rectangle.setHeight(h);  //設定高度
			shape[i]=rectangle;  //存入陣列
		}
		
		else if (choose==2)  //若選擇圓形
		{
			do{
				System.out.print("Input the radius of the circle: ");
				r=input.nextFloat();
				if (r>20 || r<1)
					System.out.println("Out of range!");
			}while (r>20 || r<1);
			circle.setRadius(r);  //設定半徑
			shape[i]=circle;
		}
	}
		input.close();  //關閉input
		float area=0;
		for (int j=0; j<num; j++){  //輸出面積
			System.out.println("The area of the shape is "+shape[j].getArea());
			area+=shape[j].getArea();  //面積加總
		}
		System.out.print("The area sum of the shapes is "+area);  //輸出總面積
	}
}
