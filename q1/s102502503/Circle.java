package ce1002.q1.s102502503;

public class Circle extends Shape{
	private float radius;
	private static float PI=(float) 3.14;
	public float getRadius() {  //取得
		return radius;
	}
	public void setRadius(float radius) {  //設定
		this.radius = radius;
	}
	public float getArea(){  //取得面積
		return radius*radius*PI;
	}
}
