package ce1002.q1.s102502503;

public class Rectangle extends Shape{
	private float width;
	private float height;
	public float getWidth() {  //取得
		return width;
	}
	public void setWidth(float width) {  //設定
		this.width = width;
	}
	public float getHeight() {  //取得
		return height;
	}
	public void setHeight(float height) {  //設定
		this.height = height;
	}
	public float getArea(){  //取得面積
		return width*height;
	}
}
