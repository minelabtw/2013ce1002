package ce1002.q1.s100203020;

public class Rectangle extends Shape{
	private float width;
	private float height;
	
	public Rectangle(){
		setType("Rectangle");
	}
	public void setWidth(float width){
		this.width = width;
	}
	public float getWidth(){
		return width;
	}
	

	public void setHeight(float height){
		this.height = height;
	}
	public float getHeight(){
		return height;
	}
	public float setArea(){
		return area=width*height;
	}
	
}
