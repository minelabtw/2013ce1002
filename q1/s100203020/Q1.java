package ce1002.q1.s100203020;

import java.util.Scanner;
public class Q1 {

	private static Scanner scan; 

	public static void main(String[] args) {

		scan =new Scanner(System.in);
		System.out.println("Making shapes.");
		float width, height, radius,sum; 
		
		Rectangle rec=new Rectangle();
		Circle cir=new Circle();
		//Rectangle
		do{
			System.out.print("Input the width of the rectangle: ");
			
			width=scan.nextFloat();
			if( width<1|| width>20)
				System.out.println("Out of range!");
			else
				rec.setWidth(width);
		}while ( width<1|| width>20);
		
		
		do{
			System.out.print("Input the height of the rectangle: ");
			
			height=scan.nextFloat();
			if( height<1|| height>20)
				System.out.println("Out of range!");
			else
				rec.setHeight(height);
		}while ( height<1|| height>20);
		//Circle
		do{
			System.out.print("Input the radius of the circle: ");
			
			radius=scan.nextFloat();
			if( radius<1|| radius>20)
				System.out.println("Out of range!");
			else
				cir.setRadius(radius);
		}while ( radius<1|| radius>20);
			
		rec.setArea();
		cir.setArea();
		//output
		System.out.println("The area of the rectangle is "+rec.getArea());
		System.out.println("The area of the circle is "+cir.getArea());
		sum=cir.getArea()+rec.getArea();
		System.out.println("The area sum of the shapes is "+sum);
		
		
		
		
		
		
	
		
		
		
	
		
		
		
		
	}

}
