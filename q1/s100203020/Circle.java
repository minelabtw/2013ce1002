package ce1002.q1.s100203020;

public class Circle extends Shape{
	private float radius;
	public Circle(){
		setType("Circle");
	}
	
	public void setRadius(float radius){
		this.radius = radius;
	}
	public float getRadius(){
		return radius;
	}
	public float setArea(){
		return area=(float) (radius*radius*3.14);
	}
	
	
}
