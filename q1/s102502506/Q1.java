package ce1002.q1.s102502506;
import java.util.Scanner;
public class Q1 {

	public static void main(String[] args){
		Rectangle R =new Rectangle();  //把Rectangle簡化叫R
		Circle C =new Circle();  //把Circle簡化叫C
		float w,h,r;
		Scanner put=new Scanner(System.in);  //新的Scanner叫做put
		System.out.println("Making shapes.");  //照題輸出
		do{
			System.out.print("Input the width of the rectangle: ");
			w=put.nextFloat();
			if(w<1||w>20)System.out.println("Out of range!");  //判斷輸入的數是否超出範圍超出就輸出Out of range!
		}while(w<1||w>20);  //重複執行直到w在範圍內  
		do{
			System.out.print("Input the height of the rectangle: ");
			h=put.nextFloat();
			if(h<1||h>20)System.out.println("Out of range!");
		}while(h<1||h>20);
		do{
			System.out.print("Input the radius of the circle: ");
			r=put.nextFloat();
			if(r<1||r>20)System.out.println("Out of range!");
		}while(r<1||r>20);
		R.setWidth(w);  //用Rectangle的setWidth函式將值紀錄下來
		R.setHight(h);
		C.setRadius(r);
		System.out.println("The area of the rectangle is "+R.getArea());  //輸出函式回傳值
		System.out.println("The area of the circle is "+C.getArea());
		System.out.println("The areasum of the shapes is "+(R.getArea()+C.getArea()));
		}
	}
