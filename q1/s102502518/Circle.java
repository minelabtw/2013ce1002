package ce1002.q1.s102502518;

public class Circle extends Shape {
	
    public Circle() {
		
	}
 
    public void SetRadius(float radius) {
    	Radius = radius;
	}
	public float GetRadius() {
		return Radius;
	}
	public double GetCircleArea() {
		return Radius*Radius*Math.PI;
	}
}
