package ce1002.q1.s102502518;

public class Shape {
	
	protected float Radius;
	protected float Width;
	protected float Height;
	
	public float GetRectangleArea() {
		return Width*Height;
	}
	public double GetCircleArea() {
		return Radius*Radius*Math.PI;
	}
	public double GetArea() {
		return Width*Height + Radius*Radius*Math.PI;
	}
	
}
