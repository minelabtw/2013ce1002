package ce1002.q1.s102502518;

import java.util.Scanner;

public class Q1 {
	
	
	public static void main(String[] args) {
		Shape shapes = new Shape();
		
		Scanner input = new Scanner(System.in);	
		System.out.println("Makeing shapes.");
		
		System.out.print("Input the width of the rectangle: ");
		float w = input.nextFloat();
		while (w < 1 || w >20) {
			System.out.print("Out of range!\n" + "Input the width of the rectangle: ");
			w = input.nextFloat();
		}
		Rectangle rectangles = new Rectangle();
		rectangles.SetWidth(w);
		System.out.print("Input the height of the rectangle: ");
		float h = input.nextFloat();
		while (h < 1 || h >20) {
			System.out.print("Out of range!\n" + "Input the height of the rectangle: ");
			h = input.nextFloat();
		}
		rectangles.SetHeight(h);
		System.out.print("Input the radius of the circle: ");
		float r = input.nextFloat();
		while (r < 1 || r >20) {
			System.out.print("Out of range!\n" + "Input the radius of the circle: ");
			r = input.nextFloat();
		}
		Circle circles = new Circle();
		circles.SetRadius(r);	
		input.close();
		System.out.println("The area of the rectangle is "+rectangles.GetRectangleArea());
		System.out.println("The area of the circle is "+circles.GetCircleArea());
		System.out.println("The area sum of the shapes is "+shapes.GetArea());
		
		
		
		
			
	}

}
