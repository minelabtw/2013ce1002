package ce1002.q1.s102502518;

public class Rectangle extends Shape {
	
    public Rectangle() {
		
	}
 
	public void SetWidth(float width) {
		Width = width;
	}
	public void SetHeight(float height) {
		Height = height;
	}
	public float GetWidth() {
		return Width;
	}
	public float GetHeight() {
		return Height;
	}
	public float GetRectangleArea() {
		return Width*Height;
	}
	
}
