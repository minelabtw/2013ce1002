package ce1002.q1.s102502019;

public class Rectangle extends Shape{
	private float width;
	private float height;
	
	public void setWidth(float x)
	{
		width = x;
	}
	public void setHeight(float y)
	{
		height = y;
	}
	public float getWidth()
	{
		return width;
	}
	public float getHeight()
	{
		return height;
	}
    public float getArea()
    {
    	return width*height;
    }
}
