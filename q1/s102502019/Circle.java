package ce1002.q1.s102502019;

public class Circle extends Shape{
private float radius;
public void setRadius(float z)
{
	radius = z;
}
public float getRadius()
{
	return radius;
}
public float getArea()
{
	return radius*radius*(float)3.14;
}
}
