package ce1002.q1.s102502504;

import java.util.Scanner;

public class Q1 
{
	public static void main(String[] args)
	{
		float width; //寬
		float height;//高
		float radius;//半徑
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Making shapes.");
		
		do //使用者輸入寬
		{
			System.out.print("Input the width of the rectangle: ");
			width = input.nextFloat();
			if(width<1 || width>20)
			{
				System.out.println("Out of range!");
			}
		}while(width<1 || width>20);
		
		do //使用者輸入高
		{
			System.out.print("Input the height of the rectangle: ");
			height = input.nextFloat();
			if(height<1 || height>20)
			{
				System.out.println("Out of range!");
			}
		}while(height<1 || height>20);
		
		do //使用者輸入半徑
		{
			System.out.print("Input the radius of the circle: ");
			radius = input.nextFloat();
			if(radius<1 || radius>20)
			{
				System.out.println("Out of range!");
			}
		}while(radius<1 || radius>20);
		
		input.close(); //用不到了 要關掉
		
		Rectangle rec = new Rectangle(); //新增一個rec物件
		Circle cir = new Circle(); //新增一個cir物件
		rec.setWidth(width); //call rec的setwidth
		rec.setHeight(height); //call rec的setheight
		cir.setRadius(radius); //call cir的setradius
		System.out.println("The area of the rectangle is " + rec.getArea());
		System.out.println("The area of the circle is " + cir.getArea());
		System.out.println("The area sum of the shapes is " + (rec.getArea()+cir.getArea()));
	}
	

}
