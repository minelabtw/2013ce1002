package ce1002.q1.s102502504;

public class Circle extends Shape
{
	public float radius=0;

	public Circle(){
		
	}

	//半徑的getter和setter
	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	public float getArea() //override
	{
		return (float) (radius * radius * Math.PI);
	}
}
