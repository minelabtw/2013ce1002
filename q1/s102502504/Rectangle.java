package ce1002.q1.s102502504;


public class Rectangle extends Shape
{
	public float width;
	public float height;
	
	public Rectangle(){
		
	}
	
	//長跟寬的setter和getter
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	
	public float getArea() //override
	{
		return height*width;
	}
	
}
