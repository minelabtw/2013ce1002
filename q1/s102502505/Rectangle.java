package ce1002.q1.s102502505;

public class Rectangle extends Shape{
	
	private float width;//定義寬
	private float height;//定義高
	@Override
	public float getArea(float width,float height)//override父類別中的面積方法
	{
		return (width*height);
	}
	
	public void setWidth(float width) {
		this.width = width;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	
	public float getWidth() {
		return width;
	}
	public float getHeight() {
		return height;
	}

	

}
