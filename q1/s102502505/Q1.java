package ce1002.q1.s102502505;

import java.util.Scanner;

public class Q1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Rectangle rectangle = new Rectangle();//定義rectangle,circle物件
		Circle circle = new Circle();
		Scanner input = new Scanner(System.in);
		float width;//定義寬,高,半徑
		float height;
		float radius;
		double pi=3.14;
		
		System.out.println("Making shapes.");//輸出字串
		
		System.out.print("Input the width of the rectangle: ");
		width=input.nextFloat();
		while(width>20||width<1)//定義範圍
		{
			System.out.println("Out of range!");
			System.out.print("Input the width of the rectangle: ");
			width=input.nextFloat();
		}
		rectangle.setWidth(width);//將寬傳入rectangle.java
		
		System.out.print("Input the height of the rectangle: ");
		height=input.nextFloat();
		while(height>20||height<1)
		{
			System.out.println("Out of range!");
			System.out.print("Input the height of the rectangle: ");
			height=input.nextFloat();
		}
		rectangle.setHeight(height);//將高傳入rectangle.java
		
		System.out.print("Input the radius of the rectangle: ");
		radius=input.nextFloat();
		while(radius>20||radius<1)
		{
			System.out.println("Out of range!");
			System.out.print("Input the radius of the rectangle: ");
			radius=input.nextFloat();
		}
		circle.setRadius(radius);
		//輸出字串，並呼叫被override的rectangle,circle面積，還有輸出面積總和
		System.out.println("The area of the rectangle is "
							+rectangle.getArea(rectangle.getWidth(),rectangle.getHeight()));
		System.out.println("The area of the circle is "
							+(circle.getArea(circle.getRadius())*pi));
		System.out.println("The area sum of the shapes is "+
				(rectangle.getArea(width,height)+(circle.getArea(circle.getRadius()))*pi));
		
	}

}
