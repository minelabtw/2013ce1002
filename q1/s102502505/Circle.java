package ce1002.q1.s102502505;

public class Circle extends Shape{
	
	private float radius;
	@Override
	public float getArea(float radius)//override父類別中的面積方法，return圓面積
	{
		return (radius*radius);
	}
	
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}

}
