package ce1002.q1.s102502561;

import java.util.Scanner;

public class Q1 {
	public static void main(String[] args) {
		
		// create a scanner object and let it parses the standard input.
		Scanner input = new Scanner(System.in);
		// create temporary variables for creating objects such as rectangle and triangle.
		double weight,height; // variable for rectangle's constructor
		double a,b,c; // variable for triangle's constructor
		
		System.out.println("First Graph is rectangle");
		System.out.println("Please input Weight: ");
		weight=input.nextDouble();
		System.out.println("Please input Height: ");
		height=input.nextDouble();
		Graph graph[] = new Graph[2];
		graph[0]  = new Rectangle(weight,height);
		graph[0].setEdge(4);
		

		//This part is handling the input to construct the triangle object		
		System.out.println("First Graph is triangle");
		System.out.println("Please input a: ");
		a = input.nextDouble();
		System.out.println("Please input b: ");
		b = input.nextDouble();
		System.out.println("Please input c: ");
		c = input.nextDouble();
		graph[1] = new Triangle(a,b,c);
		graph[1].setEdge(3);
		
		
		// print out each area from different shape of graph
		//��X���G
		
		System.out.println("Area(0) = "+graph[0].getArea() +" Edge = "+graph[0].getEdge());
		System.out.println("Area(1) = "+graph[1].getArea() +" Edge = "+graph[1].getEdge());
		
		input.close();
		
	}

}
