package ce1002.q1.s102502561;

public class Triangle extends Graph{
	
	private double a;
	private double b;
	private double c;//三角形的三邊
	


	public Triangle(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;

	}

	public boolean Recongnization(){
		if(a>0&&b>0&&c>0&&a+b>c&&b+c>a&&a+c>b)
		return true;
		else
			return false;
	}
	
	public double getArea(){//回傳面積
		double s = (a+b+c)/2;
		double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		if(area<0||a+b<c||a+c<b||b+c<a||a<0||b<0||c<0)
			return 0;
		else
		return area;
	}
}