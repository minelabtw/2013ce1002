package ce1002.q1.s102502556;

public class Triangle extends Graph {
	private double a;
	private double b;
	private double c; //三角形的三邊
	Triangle ( double a , double b , double c ) //constructor
	{
		this.a = a;
		this.b = b;
		this.c = c; //先初始化三個邊的大小
		setEdge(3); //設定邊數為3
		double s = ( a + b + c ) / 2;
		area = Math.sqrt(s * (s - a) * (s - b) * (s - c)); //計算該三角形的面積，並存入area變數之中
	}
	public boolean Recognization () //判斷該三角形是否合理
	{
		if ( ( (a + b) > c ) && ( (a + c) > b ) && ( (b + c) > a )  && a > 0 && b > 0 && c > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public double getArea () //回傳該三角形的面積，先判斷該三角形是否存在，如果存在，則正確回傳面積，否則回傳0
	{
		boolean check = Recognization();
		if ( check == true )
		{
			return super.getArea();
		}
		else
		{
			return 0;
		}
	}
	
}
