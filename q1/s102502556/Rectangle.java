package ce1002.q1.s102502556;

public class Rectangle extends Graph {
	private double height;
	private double weight; //長方形的寬和高
	Rectangle ( double weight , double height ) //constructor
	{
		this.weight = weight;
		this.height = height; //初始化長方形的寬和高
		setEdge(4); //設定邊數為4
		area = weight * height; //計算該長方形的面積大小，並存入area之中
	}
	public boolean Recognization () //判斷該長方形是否合理
	{
		if ( height > 0 && weight > 0 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public double getArea () //回傳該長方形的面積，先判斷該長方形是否存在，如果存在，則正確回傳面積，否則回傳0
	{
		boolean check = Recognization();
		if ( check == true )
		{
			return super.getArea();
		}
		else
		{
			return 0;
		}
		
	}

}
