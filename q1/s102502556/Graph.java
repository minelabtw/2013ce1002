package ce1002.q1.s102502556;

public class Graph {
		protected double area; //存圖的面積
		private int edge; //存圖的邊數
		Graph() //constructor
		{
			
		}
		public void setEdge(int e) //設定圖的邊數
		{
			edge = e;
		}
		public int getEdge() //回傳圖擁有的邊數
		{
			return edge;
		}
		public boolean Recognization () //回傳該圖是否合理
		{
			return false;
		}
		public double getArea () //回傳圖的面積大小
		{
			return area;
		}

}
