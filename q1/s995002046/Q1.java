package ce1002.q1.s995002046;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Q1{
	public static void main(String[] args){
		Scanner scanner;
		int num=0;
		int num1=0;
		float w=0;
		float h=0;
		float r=0;
		List<Shape> mys= new ArrayList<>();
		System.out.print("Making Shapes.\n");
		while(5>3){
			System.out.print("Input the number of shapes to be create: ");
			scanner = new Scanner(System.in);
			num=scanner.nextInt();
			if(num>=1 && num <=10){
				for(int i=0; i<num; i++){
					System.out.print("1)Create rectangle, 2(Create circle: ");
					scanner = new Scanner(System.in);
					num1=scanner.nextInt();
					if(num1!=1 && num1!=2){
						i=i-1;
						System.out.print("Out of range!\n");
						continue;
					}
					else if (num1==1){
						Rectangle R1 = new Rectangle();
						while(5>3){
							System.out.print("Input the width of the rectangle: ");
							scanner = new Scanner(System.in);
							w=scanner.nextFloat();
							if(w>=1 && w<=20){
								R1.setWidth(w);
								break;
								}
							System.out.print("Out of range!\n");
							}
						while(5>3){
							System.out.print("Input the height of the rectangle: ");
							scanner = new Scanner(System.in);
							h=scanner.nextFloat();
							if(h>=1 && h<=20){
								R1.setHeight(h);
								break;
							}
							System.out.print("Out of range!\n");
						}
						mys.add(R1);
					
					}
					else if (num1==2){
							Circle C1 = new Circle();
							while(5>3){
								System.out.print("Input the radius of the circle: ");
								scanner = new Scanner(System.in);
								r=scanner.nextFloat();
								if(r>=1 && r<=20){
									C1.setRadius(r);
									break;
								}
								System.out.print("Out of range!\n");
							}
							mys.add(C1);			
					}
			}
				break;
		}
			System.out.print("Out of range!\n");
	}
		scanner.close();
		float total=0;
		for(Shape s: mys){
			System.out.print("The area of the shape is "+s.getArea()+"\n");
			total=total+s.getArea();
		}
		System.out.print("The area sum of the shapes is "+total+"\n");
}
}