package ce1002.q1.s995002046;

class Circle extends Shape{
	private float r;
	Circle(){
		
	}
	@Override
	float getArea(){
		return (float) (r*r*3.14);
	}
	void setRadius(float radius){
		r=radius;
	}
	float getRadius(){
		return r;
	}
}