package ce1002.q1.s995002046;

class Rectangle extends Shape{
	private float w;
	private float h;
	Rectangle(){
		
	}
	@Override
	float getArea(){
		return w*h;
	}
	void setWidth(float width){
		w=width;
	}
	void setHeight(float height){
		h=height;
	}
	float getWidth(){
		return w;
	}
	float getHeight(){
		return h;
	}
}