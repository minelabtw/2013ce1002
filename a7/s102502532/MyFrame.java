package ce1002.a7.s102502532;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame() {
		
		setLayout(null); // 板子顯現 不打不行
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {

		panel.setRoleState(hero); // 角色
		panel.setLocation(x, y); //座標
		this.add(panel); // 加上
	}
}
