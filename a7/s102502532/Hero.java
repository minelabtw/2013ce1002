package ce1002.a7.s102502532;

public class Hero {
	private String heroName;
	private double HP =30.0;
	private double MP =30.0;
	private double PP =30.0;
	
	public Hero(){
		
	}
	
	public void setHeroName(String heroName) {         //Getter�PSetter
		this.heroName = heroName;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	
	public String getHeroName() {                 //Getter�PSetter
		return heroName;
	}
	public double getHP() {
		return HP;
	}
	public double getMP() {
		return MP;
	}
	public double getPP() {
		return PP;
	}
	
}
