package ce1002.a7.s102502532;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] heros = new Hero[3]; // 物件陣列
		heros[0] = new Wizard(); // 不同型態(父,子)
		heros[1] = new Swordsman();
		heros[2] = new Knight();

		heros[0].setHeroName("Wizard");
		heros[1].setHeroName("Swordsman");
		heros[2].setHeroName("Knight");

		MyFrame frame = new MyFrame(); // 建立板子
		MyPanel[] panel = new MyPanel[3]; // 建立

		frame.setSize(490, 740); // frame寫在 main 裡
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		for (int i = 0; i < 3; i++) {
			panel[i] = new MyPanel(); // 三個畫布
			frame.newRolePos(heros[i], panel[i], 20, 10 + 230 * i);
		}

		frame.setVisible(true); // 擺最後
	}

}
