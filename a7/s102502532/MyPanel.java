package ce1002.a7.s102502532;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.LineBorder; //LineBorder

public class MyPanel extends JPanel {

	public MyPanel() {

		setSize(440, 220); // 畫布大小
		setBorder(new LineBorder(Color.BLACK, 4)); // 畫布邊框 (用setBorder函式)
	}

	public void setRoleState(Hero hero) {

		JLabel name = new JLabel(hero.getHeroName() + " HP: " + hero.getHP()
				+ " MP: " + hero.getMP() + " PP: " + hero.getPP());
		ImageIcon image = new ImageIcon(hero.getHeroName() + ".jpg"); // 不用給絕對路徑讀取圖片
		StatusPanel newPanel = new StatusPanel();
		JLabel icon = new JLabel(image);
		setLayout(new BorderLayout(15, 0)); // 用 BorderLayout

		this.add(name, BorderLayout.NORTH); // 在這塊 畫布
		// this.add(name);
		newPanel.setState(hero);

		// this.add(newPanel);
		// this.add(icon);
		this.add(newPanel, BorderLayout.CENTER);
		this.add(icon, BorderLayout.WEST);
	}
}
