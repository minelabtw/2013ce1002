package ce1002.a7.s102502532;

import javax.swing.*;
import java.awt.GridLayout;

public class StatusPanel extends JPanel{

	public StatusPanel() {
		
		setSize(150, 220);    // �j�p
		//setLocation(300, 0);
	}
	
	public void setState(Hero hero){
		setLayout(new GridLayout(3,2,10,5) );      //�� GridLayout
		
		add(new JLabel("HP"));
		add(new JLabel("" + hero.getHP() ));         //�r��
		add(new JLabel("MP"));
		add(new JLabel("" + hero.getMP() ));
		add(new JLabel("PP"));
		add(new JLabel("" + hero.getPP() ));
	}
}
