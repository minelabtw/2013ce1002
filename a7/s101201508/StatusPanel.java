package ce1002.a7.s101201508;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{

	StatusPanel()
	{
		setLayout(new GridLayout(3,2));//use the chess board range
		setVisible(true);
	}
	public void setState(Hero in_hero)
	{
		//set the data to the table
		add(new JLabel("HP"));
		add(new JLabel(Double.toString(in_hero.get_HP())));
		add(new JLabel("MP"));
		add(new JLabel(Double.toString(in_hero.get_MP())));
		add(new JLabel("PP"));
		add(new JLabel(Double.toString(in_hero.get_PP())));
	}
}
