package ce1002.a7.s101201508;

public class Wizard extends Hero{
	public Wizard()
	{
		//input the data of this class
		set_name("Wizard");
		set_image("src\\ce1002\\a7\\s101201508\\Wizard.jpg");
	}
	public double  get_HP()
	{
		return HP*0.2;
	}
	public double  get_MP()
	{
		return MP*0.7;
	}
	public double  get_PP()
	{
		return PP*0.1;
	}
}
