package ce1002.a7.s101201508;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame{
	protected MyPanel panel[]=new MyPanel[3];
	/*
	protected MyPanel panel1=new MyPanel();
	protected MyPanel panel2=new MyPanel();
	protected MyPanel panel3=new MyPanel();
	*/
	protected Hero hero[]=new Hero[3];
	MyFrame(Hero in_hero[])
	{
		//input data
		this.hero=in_hero;
		for (int i=0;i<3;i++)
		{
			panel[i]=new MyPanel();
		}
		//set the exterior data
		setLayout(null);
		setSize(490,680) ;
		setLocationRelativeTo(null) ;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		//input the data to newRolePos function
		for (int i=0;i<3;i++)
		{
			newRolePos(hero[i],panel[i],10,210*i+10);
		}
		/*
		newRolePos(hero[0],panel1,10,10);
		newRolePos(hero[1],panel2,10,220);
		newRolePos(hero[2],panel3,10,430);
		*/
	}
	public void newRolePos(Hero in_hero , MyPanel P , int x , int y)
	{
		P.setBounds(x,y,445,200);
		P.setRoleState(in_hero);
		P.setBorder(new LineBorder(Color.BLACK,2));//set the borders
		add(P);
	}
}
