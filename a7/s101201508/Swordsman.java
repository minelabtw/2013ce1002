package ce1002.a7.s101201508;

public class Swordsman extends Hero{
	public Swordsman()
	{
		//input the data of this class
		set_name("Swordsman");
		set_image("src\\ce1002\\a7\\s101201508\\Swordsman.jpg");
	}
	public double  get_HP()
	{
		return HP*0.1;
	}
	public double  get_MP()
	{
		return MP*0.1;
	}
	public double  get_PP()
	{
		return PP*0.8;
	}
}
