package ce1002.a7.s101201508;

public class Knight extends Hero{
	public Knight()
	{
		//input the data of this class
		set_name("Knight");
		set_image("src\\ce1002\\a7\\s101201508\\Knight");
	}
	public double  get_HP()
	{
		return HP*0.8;
	}
	public double  get_MP()
	{
		return MP*0.1;
	}
	public double  get_PP()
	{
		return PP*0.1;
	}
}
