package ce1002.a7.s101201508;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MyPanel extends JPanel{
	protected JLabel label1=new JLabel();
	protected JLabel label2=new JLabel();
	protected StatusPanel panel=new StatusPanel();
	MyPanel()
	{
		setLayout(null);
		//set the location and the size in the panel
		label1.setBounds(5,0,220,20);
		label2.setBounds(5,15,240,180);
		panel.setBounds(255,5,180,190);
		//input to the panel
		add(label1);
		add(label2);
		add(panel);
	}
	public void setRoleState(Hero in_hero)
	{
		label1.setText(in_hero.get_name()+" HP: "+in_hero.get_HP()+" MP: "+in_hero.get_MP()+" PP: "+in_hero.get_PP());//set the word in the label
		label2.setIcon(in_hero.get_image());//input the image from class hero
		panel.setState(in_hero);//set the table of HP, MP, PP
	}
}
