package ce1002.a7.s102502501;
import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3]; //宣告Hero
		hero[0] = new Wizard(); 
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		MyFrame frame = new MyFrame();  //板子
		MyPanel[] panel = new MyPanel[3]; //畫框
		
		frame.setSize(500,500); //板子大小
		frame.setLocationRelativeTo(null); // 初始位置
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 關閉板子
		
		for(int i=0; i<3;i++){
			panel[i] = new MyPanel(); // 三個畫布
			frame.newRolePos(hero[i],panel[i],20,10+240*i);
		}
		frame.setVisible(true); // 顯示板子
	}
}
