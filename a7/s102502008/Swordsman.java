package ce1002.a7.s102502008;
import javax.swing.ImageIcon;

public class Swordsman extends Hero
{
	public Swordsman()
	{
		name= "Swordsman" ; 
		image= new ImageIcon("src/ce1002/a7/s102502008/image/Swordsman.jpg");
	}
	public String nameGetter()
	{
		return name ;
	}
	public float hpGetter()
	{
		return hp*0.1f ;
	}
	public float mpGetter()
	{
		return mp*0.1f ;
	}
	public float ppGetter()
	{
		return pp*0.8f ;
	}
}
