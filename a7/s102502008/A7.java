package ce1002.a7.s102502008;
import javax.swing.JFrame;

public class A7 
{
	public static void main(String args[])
	{
		MyFrame frame= new MyFrame() ;
		
		StatusPanel statu1= new StatusPanel() ;
		StatusPanel statu2= new StatusPanel() ;
		StatusPanel statu3= new StatusPanel() ;

		
		MyPanel knight= new MyPanel() ;
		MyPanel swordsman= new MyPanel() ;
		MyPanel wizard= new MyPanel() ;
		knight.setState(new Knight()) ;
		swordsman.setState(new Swordsman()) ;
		wizard.setState(new Wizard()) ;
		
		MyPanel[] arrays= new MyPanel[3] ;
		arrays[0]= wizard ;
		arrays[1]= swordsman ;
		arrays[2]= knight ;
		
		frame.setHero(arrays) ;
		
		frame.setSize(447,686);
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true);
	}
}
