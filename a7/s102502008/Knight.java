package ce1002.a7.s102502008;
import javax.swing.ImageIcon;
public class Knight extends Hero
{
	
	public Knight()
	{
		name= "Knight" ; 
		image= new ImageIcon("src/ce1002/a7/s102502008/image/Knight.jpg");
	}
	
	public String nameGetter()
	{
		return name ;
	}
	public float hpGetter()
	{
		return hp*0.8f ;
	}
	public float mpGetter()
	{
		return mp*0.1f ;
	}
	public float ppGetter()
	{
		return pp*0.1f ;
	}
}
