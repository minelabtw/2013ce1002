package ce1002.a7.s102502008;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel; 

public class StatusPanel extends JPanel
{
	protected JLabel hp= new JLabel("HP") ;
	protected JLabel hpv= new JLabel() ;
	protected JLabel mp= new JLabel("MP") ;
	protected JLabel mpv= new JLabel() ;
	protected JLabel pp= new JLabel("PP") ;
	protected JLabel ppv= new JLabel() ;
	public StatusPanel()
	{
		setLayout(new GridLayout(3,2,100,0)) ;
	
		add(hp) ;
		add(hpv) ;
		add(mp) ;
		add(mpv) ;
		add(pp) ;
		add(ppv) ;

	}
	public void setStatu(Hero hero)
	{
		hpv.setText(""+hero.hpGetter()) ;
		mpv.setText(""+hero.mpGetter()) ;
		ppv.setText(""+hero.ppGetter()) ;
	}
}
