package ce1002.a7.s102502008;
import javax.swing.ImageIcon;


public class Wizard extends Hero
{
	public Wizard()
	{
		name= "Wizard" ; 
		image= new ImageIcon("src/ce1002/a7/s102502008/image/Wizard.jpg");
	}
	public String nameGetter()
	{
		return name ;
	}
	public float hpGetter()
	{
		return hp*0.2f ;
	}
	public float mpGetter()
	{
		return mp*0.7f ;
	}
	public float ppGetter()
	{
		return pp*0.1f ;
	}
}
