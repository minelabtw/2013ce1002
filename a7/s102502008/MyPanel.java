package ce1002.a7.s102502008;
import javax.swing.JPanel;
import javax.swing.JLabel ; 
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
public class MyPanel extends JPanel
{
	protected JLabel text= new JLabel() ; 
	protected JLabel image= new JLabel() ;
	protected StatusPanel statu= new StatusPanel() ;
	public MyPanel()
	{
		setLayout(new BorderLayout()) ;
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(text,BorderLayout.NORTH) ;
		add(image,BorderLayout.CENTER) ;
		add(statu,BorderLayout.EAST) ;
	}
	public void setState(Hero hero)
	{
		text.setText(hero.nameGetter()+" HP: "+hero.hpGetter()+" MP: "+hero.mpGetter()+" PP: "+hero.ppGetter());
		image.setIcon(hero.imageGetter());
		statu.setStatu(hero) ;
		this.statu = statu ;
	}
}
