package ce1002.a7.s102502526;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;


public class MyPanel extends JPanel{    //by A6
	JLabel info;  //建立一個框框
	JLabel img;
	ImageIcon icon; 
	StatusPanel SP = new StatusPanel();
	MyPanel(){
		this.setSize(400, 230);
		this.setBorder(new LineBorder(Color.black, 5));
	}
	
	public void setRoleState(Hero hero){
		icon = new ImageIcon(hero.getHeroname() + ".jpg");  //召喚圖片
		info = new JLabel(hero.getHeroname() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		img = new JLabel(icon);
		SP.setState(hero);
		this.add(info);
		this.add(img);
		this.add(SP);
	}   
}
