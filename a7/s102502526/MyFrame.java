package ce1002.a7.s102502526;

import javax.swing.JFrame;

public class MyFrame extends JFrame{    //by A6
	MyFrame(){
		this.setLayout(null);
		setBounds(500,200,500,800);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){  //呼叫A6裡宣告的hero、panel，宣告並呼叫x、y變數
		this.add(panel);
		panel.setRoleState(hero);
		panel.setLocation(x, y);  //xy用以定位圖片
	}
}
