package ce1002.a7.s102502526;

public class Hero {    //by A6
	private String heroname;
	private float HP;
	private float MP;
	private float PP;

	Hero(){     //constructor
		HP = 30;
		MP = 30;
		PP = 30;
	}

	public void setHeroname(String heroname){
		this.heroname = heroname;
	}
	public void setHP(float HP){
		this.HP = HP;
	}
	public void setMP(float MP){
		this.MP = MP;
	}
	public void setPP(float PP){
		this.PP = PP;
	}

	public String getHeroname(){
		return this.heroname;
	}
	public float getHP(){
		return this.HP;
	}
	public float getMP(){
		return this.MP;
	}
	public float getPP(){
		return this.PP;
	}
}
