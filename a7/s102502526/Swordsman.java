package ce1002.a7.s102502526;

public class Swordsman extends Hero{   //by A6
	@Override   
	public float getHP(){
		return super.getHP() * 0.1f;
	}
	@Override
	public float getMP(){
		return super.getMP() * 0.1f;
	}
	@Override
	public float getPP(){
		return super.getPP() * 0.8f;
	}
}
