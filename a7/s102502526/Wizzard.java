package ce1002.a7.s102502526;

public class Wizzard extends Hero{   //by A6
	@Override
	public float getHP(){
		return super.getHP() * 0.2f;
	}
	@Override
	public float getMP(){
		return super.getMP() * 0.7f;
	}
	@Override
	public float getPP(){
		return super.getPP() * 0.1f;
	}

}
