package ce1002.a7.s101201521;

public class Wizard extends Hero{
	//create wizard with hero's name and initialize status
	public Wizard(String name){
		super(name);
	}
	public String getName(){
		return super.getName();
	}
	//return hp of this hero
	public double getHp(){
		return super.getHp() * 0.2;
	}
	//return mp of this hero
	public double getMp(){
		return super.getMp() * 0.7;
	}
	//return pp of this hero
	public double getPp(){
		return super.getPp() * 0.1;
	}
}
