package ce1002.a7.s101201521;

public class Swordsman extends Hero{
	//create swordsman with hero's name and initialize status
	public Swordsman(String name){
		super(name);
	}
	public String getName(){
		return super.getName();
	}
	//return hp of this hero
	public double getHp(){
		return super.getHp() * 0.1;
	}
	//return mp of this hero
	public double getMp(){
		return super.getMp() * 0.1;
	}
	//return pp of this hero
	public double getPp(){
		return super.getPp() * 0.8;

	}
}