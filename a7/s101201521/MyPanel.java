package ce1002.a7.s101201521;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
public class MyPanel extends JPanel{
	//label for hero status
	private JLabel textLbl = new JLabel();
	//label for hero image
	private JLabel imageLbl = new JLabel();
	//panel for status matrix
	private JPanel rightPanel = new JPanel();
	//labels for entries of status matrix
	private JLabel[] labels = new JLabel[6];
	public MyPanel(){
		for(int i = 0; i < labels.length; i++){
			labels[i] = new JLabel();
		}
		setLayout(null);
		textLbl.setBounds(5, 0, 220, 20);
		imageLbl.setBounds(5, 20, 240, 180);
	}
	public void setRoleState(Hero hero){
		//set text of text label
		textLbl.setText(hero.getName() + 
						" HP: " + hero.getHp() + 
						" MP: " + hero.getMp() + 
						" PP: " + hero.getPp());
		//set image of image label
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg");
		imageLbl.setIcon(icon);
		add(textLbl);
		add(imageLbl);
		//setting rightPanel
		rightPanel.setLayout(new GridLayout(3,2));
		rightPanel.setBounds(250, 20, 140, 180);
		add(rightPanel);
		//setting status matrix
		labels[0].setText("HP");
		labels[1].setText(hero.getHp() + "");
		labels[2].setText("MP");
		labels[3].setText(hero.getMp() + "");
		labels[4].setText("PP");
		labels[5].setText(hero.getPp() + "");
		for(int i = 0; i < labels.length; i++){
			rightPanel.add(labels[i]);
		}
	}
}
