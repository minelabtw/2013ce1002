package ce1002.a7.s101201521;

public class Knight extends Hero{
	//create knight with hero's name
	public Knight(String name){
		super(name);
	}
	public String getName(){
		return super.getName();
	}
	//return hp of this hero
	public double getHp(){
		return super.getHp() * 0.8;
	}
	//return mp of this hero
	public double getMp(){
		return super.getMp() * 0.1;
	}
	//return pp of this hero
	public double getPp(){
		return super.getPp() * 0.1;

	}
}