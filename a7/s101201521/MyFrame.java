package ce1002.a7.s101201521;
import javax.swing.JFrame;
import javax.swing.border.*;
import java.awt.Color;
public class MyFrame extends JFrame{
	//panels for heros
	private MyPanel[] panels = new MyPanel[3];
	//heros
	private Hero[] heros = new Hero[3];
	public MyFrame(){
		for(int i = 0; i < panels.length; i++){
			panels[i] = new MyPanel();
		}
		heros[0] = new Wizard("Wizard");
		heros[1] = new Swordsman("Swordsman");
		heros[2] = new Knight("Knight");
		//setting hero panels
		for(int i = 0; i < heros.length; i++) {
			newRolePos(heros[i], panels[i], 10, 10+i*210);
		}
		setLayout(null);
		setLocation(0,0);
		setSize(435, 680);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	//method to set hero status
	public void newRolePos(Hero hero, MyPanel panel, int x, int y){
		panel.setBounds(x, y, 400, 205);
		panel.setRoleState(hero);
		panel.setBorder(new LineBorder(Color.BLACK, 2));
		add(panel);
	}
}
