package ce1002.a7.s102502020;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.*;

public class StatusPanel extends JPanel{

	protected JLabel label1 = new JLabel();        //宣告6個Label
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel();
	protected JLabel label6 = new JLabel();
	
	StatusPanel(){
		setLayout(new GridLayout(3,2,45,44));      //用GridLayout排版
		add(label1);                               //6個Label加進GridLayout
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
	}
	public void setState(Hero hero) {              //設定角色的各項屬性
		label1.setText(" HP ");
		label2.setText(hero.getHp()+"");
		label3.setText(" MP ");
		label4.setText(hero.getMp()+"");
		label5.setText(" PP");
		label6.setText(hero.getPp()+"");
	}

}
