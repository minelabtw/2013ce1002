package ce1002.a7.s102502020;

import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();         //宣告1個面板和2個Label
	protected JLabel imagelabel = new JLabel();
	protected JPanel panel = new JPanel();
	StatusPanel status = new StatusPanel();
	
	MyPanel()
	{
		setLayout(null);                           //用預設的排版
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		panel.setBounds(225, 23, 150, 150);
		add(label);                                //加入能力值Label
		add(imagelabel);                           //加入圖片Label
		panel.add(status);
		add(panel);                                //加入StatusPanel
	}
	
	public void setRoleState(Hero hero)            //設定角色能力值和圖片和StatusPanel的能力值
	{
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		status.setState(hero);
	}
}
