package ce1002.a7.s102502018;
import javax.swing.ImageIcon;
public class Hero {
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;

	public Hero()
	{
		sethp(30);
		setmp(30);
		setpp(30);
	}
	public void setname(String n)
	{
		this.name = n;
	}
	public void sethp(float hp)
	{
		this.hp = hp;
	}
	public void setmp(float mp)
	{
		this.mp = mp;
	}
	public void setpp(float pp)
	{
		this.pp = pp;
	}
	public String getname()
	{
		return name;
	}
	public double gethp()
	{
		return hp;
	}
	public double getmp()
	{
		return mp;
	}
	public double getpp()
	{
		return pp;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}

}
