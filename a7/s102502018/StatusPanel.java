package ce1002.a7.s102502018;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.GridLayout;
public class StatusPanel extends JPanel{
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel();
	protected JLabel label6 = new JLabel();
	
	public StatusPanel()
	{
		setLayout(new GridLayout(3,2));  //排版方式
		setSize(100,150);				 //panel大小
		add(label1);
		add(label4);
		add(label2);
		add(label5);
		add(label3);
		add(label6);	
	}
	public void setState(Hero hero)
	{
		label1.setText("HP");
		label2.setText("MP");		
		label3.setText("PP");
		label4.setText(""+hero.gethp());
		label5.setText(""+hero.getmp());
		label6.setText(""+hero.getpp());
	}

}
