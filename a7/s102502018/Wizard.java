package ce1002.a7.s102502018;


public class Wizard extends Hero {
	public Wizard()
	{
		super.setname("Wiazrd");
		super.setImage("Wizard.jpg");
	}
	public double gethp()
	{
		return super.hp*0.2;
	}
	public double getmp()
	{
		return super.mp*0.7;
	}
	public double getpp()
	{
		return super.pp*0.1;
	}
}
