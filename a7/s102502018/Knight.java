package ce1002.a7.s102502018;

public class Knight extends Hero {
	public Knight()
	{
		super.setname("Knight");
		super.setImage("Knight.jpg");
	}
	public double gethp()
	{
		return super.hp*0.8;
	}
	public double getmp()
	{
		return super.mp*0.1;
	}
	public double getpp()
	{
		return super.pp*0.1;
	}	
}
