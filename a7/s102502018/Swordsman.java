package ce1002.a7.s102502018;

public class Swordsman extends Hero {
	public Swordsman()
	{
		super.setname("Swordsman");	
		super.setImage("Swordsman.jpg");
	}
	public double gethp()
	{
		return super.hp*0.1;
	}
	public double getmp()
	{
		return super.mp*0.1;
	}
	public double getpp()
	{
		return super.pp*0.8;
	}
	
}
