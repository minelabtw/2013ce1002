package ce1002.a7.s102502018;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
	
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel newpanel = new StatusPanel();
	public MyPanel()
	{		
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,17,240,180);		//設置圖片起始位置及panel大小
		add(label);
		add(imagelabel);
		newpanel.setLocation(255, 30);			//設置panel起始位置
		add(newpanel);
		
	}
	public void setRoleState(Hero hero)
	{
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getname()+" HP: "+hero.gethp()+" MP: "+hero.getmp()+" PP: "+hero.getpp());
		newpanel.setState(hero);			//將英雄資訊傳給newpanel
	}
	
	
}
