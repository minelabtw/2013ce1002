package ce1002.a7.s102502018;
import java.awt.Color;

import javax.swing.*;
public class MyFrame extends JFrame{
	
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;
	public MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		setLayout(null);
		setBounds(300 , 50 , 440 , 680);            //視窗出現位置及出現大小
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 400, 200);		
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));		//在panel邊加上邊框
		panel.setRoleState(hero);                     						//將英雄資訊帶入panel
		add(panel);
	}

}
