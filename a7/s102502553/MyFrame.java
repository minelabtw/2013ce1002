package ce1002.a7.s102502553;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class MyFrame extends JFrame{
	
	    MyPanel p1 = new MyPanel();//建立面板
		MyPanel p2 = new MyPanel();
		MyPanel p3 = new MyPanel();
		
	MyFrame()
	{
		setSize(450,1000);//設長寬
		setLayout(null);//排版
		
        Hero arr[] = new Hero[3];//宣告Hero陣列
		
		arr[0] = new Wizard();//轉為Wizard		
		arr[1] = new Swordsman();//轉為Swordsman		
		arr[2] = new Knight();//轉為Knight
		
		for(int i = 0;i < 3;i++)//處事畫HP MP PP為30
		{
			arr[i].setHP(30);
			arr[i].setMP(30);
			arr[i].setPP(30);
		}
		
		newRolePos(arr[0] , p1 , 10 , 10);
		newRolePos(arr[1] , p2 , 10 , 250);
		newRolePos(arr[2] , p3 , 10 , 490);
		
		add(p1);//貼上面板
		add(p2);
		add(p3);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setRoleState(hero);
		panel.setLocation(x, y);//設初始位置
		panel.setBorder(new LineBorder(Color.BLACK,3));//設面板邊界為黑色寬度3
	}
}

