package ce1002.a7.s102502553;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	
	StatusPanel()
	{
		setLayout(new GridLayout(3 , 2 , 40 , 10));//y軸3個，x軸2個，x軸格子差40格，y軸格子差10格
		setSize(50,200);
	}

     public void setState(Hero hero )//傳入StatusPanel的值 
	{
    	 JLabel l1 = new JLabel("HP");
    	 JLabel l2 = new JLabel("" + hero.getHP());
    	 JLabel l3 = new JLabel("MP");
    	 JLabel l4 = new JLabel("" + hero.getMP());
    	 JLabel l5 = new JLabel("PP");
    	 JLabel l6 = new JLabel("" + hero.getPP());
    	 
    	 add(l1);
    	 add(l2);
    	 add(l3);
    	 add(l4);
    	 add(l5);
    	 add(l6);
	}
}
