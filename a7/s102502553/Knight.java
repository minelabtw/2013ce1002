package ce1002.a7.s102502553;

import ce1002.a7.s102502553.Hero;

public class Knight extends Hero{
	
	Knight(){//constructor
		super.setname("Knight");
	}
	
	public float getHP(){//回傳Knight的值
		return super.getHP() * (float) 0.8;
	}
	public float getMP(){
		return super.getMP() * (float) 0.1;
	}
	public float getPP(){
		return super.getPP() * (float) 0.1;
	}
}