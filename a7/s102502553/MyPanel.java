package ce1002.a7.s102502553;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{

	MyPanel()
	{
		setLayout(new BorderLayout(5 , 5));//排版
		setSize(350,200);//設長寬
	}

	public void setRoleState(Hero hero)
	{
		 ImageIcon image = new ImageIcon(hero.getnames() + ".jpg");//建立圖片 
		 
		 JLabel l1 = new JLabel(hero.getnames() + " HP: " + hero.getHP() + " MP: "
				 + hero.getMP() + " PP: " + hero.getPP());//建立鱉籤，並存入值
		 JLabel l2 = new JLabel(image);//建立標籤，並存入圖片
		 
		 l1.setSize(274,19);//設長寬
		 l1.setLocation(10,0);//設初始位置
		
		 l2.setSize(274,175);//設長寬
		 l2.setLocation(0,19);//設初始位置
		 
		 add(l1 , BorderLayout.NORTH);//貼上標籤，並擺在北邊
		 add(l2 , BorderLayout.WEST);
		 
		 StatusPanel pp = new StatusPanel();//建立標籤
		 pp.setState(hero);
	   	 add(pp , BorderLayout.EAST);//在panel上貼上StatusPanel
	   	 	 
	}
}
