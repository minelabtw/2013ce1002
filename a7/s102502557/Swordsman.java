package ce1002.a7.s102502557;

import ce1002.a7.s102502557.Hero;

public class Swordsman extends Hero {
	public Swordsman()
	{
		set_id("Swordsman");
	}
	
	public float get_hp()
    {
   	  //另一種方法：get_hp有引數 x=(float) ;//強制轉型
       return super.get_hp() * 0.1f;
    }
	
	public float get_mp()
	{
		return super.get_mp() * 0.1f;
	}
	
	public float get_pp()
	{
		return super.get_pp() * 0.8f;
	}

}
