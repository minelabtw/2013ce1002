package ce1002.a7.s102502557;

public class Hero {
	private String ID;
	public float HP, MP, PP ;//private的話 在wizard上就無法呼叫
	
	public Hero(){          //建構元
		HP=30;MP=30;PP=30;
	}
	
	public void set_id(String ID)
	{
		this.ID=ID;
	}
	public String get_id()
	{
		return ID;
	}
	
	public void set_hp(float HP)
	{
	 this.HP = HP;//這裡的this.HP就像是HP  HP就像_hp
	}
    public float get_hp()
    {
      return HP;
    }
    
    public void set_mp(float MP)
    {
    	this.MP = MP;
    }
    public float get_mp()
    {
    	return MP;
    }
    
    public void set_pp(float PP)
    {
    	this.PP = PP;
    }
    public float get_pp()
    {
    	return PP;
    }
}
