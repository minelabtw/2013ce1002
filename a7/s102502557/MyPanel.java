package ce1002.a7.s102502557;

import java.awt.Color;
import java.awt.Component;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	protected JLabel label = new JLabel();
	protected JLabel image = new JLabel();
	JLabel info = new JLabel();
	ImageIcon icon = new ImageIcon();
	JLabel img = new JLabel();
	StatusPanel statuspanel = new StatusPanel();
	
	MyPanel()
	{
		//setLayout(null);//打了這行 就會預設大小為 0 0 ;
	}
	
	public void setRoleState(Hero hero)
	{
	   info = new JLabel(hero.get_id() + " HP: " + hero.get_hp() + " MP: " + hero.get_mp() + " PP: " + hero.get_pp() );
	   icon = new ImageIcon(hero.get_id() + ".jpg");
	   img = new JLabel(icon);  //把icon放入 img 這個標籤
	   statuspanel.setState(hero);//呼叫statuspanel裡面的函式
	   add(info);
	   add(img);
	   add(statuspanel);
	}

}
