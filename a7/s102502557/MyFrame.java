package ce1002.a7.s102502557;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame
{
	
   MyFrame()
   {
	 this.setLayout(null);
	 setSize(430,700);//設定frame的大小
	 setLocationRelativeTo(null);//視窗置中
	 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//讓x有功能
	 
   }
   public void newRolePos (Hero hero, MyPanel panel, int x, int y)
   {
	  
	  panel.setBounds(x, y, 400 , 230);   //x y讓我們在A7改變
		/*setBound= set location 跟setsize */
	  panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
	  panel.setRoleState(hero);		
	  this.add(panel);  
   }
}
