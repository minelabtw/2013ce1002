package ce1002.a7.s102502557;

import java.awt.GridLayout;
import javax.swing.*;

public class StatusPanel extends JPanel {
		
	JLabel grid[];
	StatusPanel()
	{
		setLayout(new GridLayout(3,2,50,50) );
	  
	}

	public void setState(Hero hero) 
	{
		grid = new JLabel[6];
	    grid[0]= new JLabel("HP");                    //要用JLabel
	    grid[1]= new JLabel(  "" + hero.get_hp() ) ;  //前面用一個空字串 讓後面強致轉型
	    grid[2]= new JLabel("MP");
	    grid[3]= new JLabel( "" + hero.get_mp() ) ;
	    grid[4]= new JLabel("PP");
	    grid[5]= new JLabel("" + hero.get_pp() ) ;
	    for(int i=0;i<6;i++)
    	{
    	add(grid[i]);
    	}//設定六個grid 並且都加進StatusPanel
	}
	

}
