package ce1002.a7.s102502557;

import java.util.Scanner;

public class A7 {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) 
	{
		Hero[] hero;//宣告一個Hero形態的hero陣列
		hero=new Hero[3];//設定大小   一行解：Hero[] hero = new Hero[3]
		hero[0]= new Wizard();//hero[0]變成wizard形態而不再是hero
		hero[1]= new Swordsman();
		hero[2]= new Knight();		
		
		MyPanel[] panel = new MyPanel[3];
		MyFrame frame = new MyFrame();		
		for(int i =0; i<3;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i],panel[i],10,10+(240*i));
		}
		frame.setVisible(true);
	}

}
