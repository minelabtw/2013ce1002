package ce1002.a7.s102502539;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.*;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel panel = new StatusPanel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(panel);
		//add(panel);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		imagelabel.setIcon(hero.getImage());
		panel.setState(hero);
		
	}
}