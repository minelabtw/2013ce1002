package ce1002.a7.s102502539;

import java.awt.GridLayout;
import javax.swing.*;

public class StatusPanel extends JPanel
{
	//表格
	public StatusPanel()
	{	
		setLayout(new GridLayout(3, 2, 50, 50));
	}
	
	//設定屬性
	public void setState(Hero hero) 
	{
		JLabel j1 = new JLabel("HP");
		JLabel j2 = new JLabel(""+hero.getHp());
		JLabel j3 = new JLabel("MP");
		JLabel j4 = new JLabel(""+hero.getMp());
		JLabel j5 = new JLabel("PP");
		JLabel j6 = new JLabel(""+hero.getPp());
		add(j1);
		add(j2);
		add(j3);
		add(j4);
		add(j5);
		add(j6);
		setVisible(true);
	}
}

