package ce1002.a7.s102502527;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel
{
	JLabel info;//標示出標題
	JLabel img;//標示出圖像
	ImageIcon icon;//暫存圖像
	
	MyPanel()
	{
		this.setSize(280, 230);//圖像大小
		this.setBorder(new LineBorder(Color.black, 5));//方框大小及顏色
	}
	
	public void setRoleState(Hero hero)
	{
		icon = new ImageIcon(hero.getId() + ".jpg");//圖像標題檔及副檔名路徑
		info = new JLabel(hero.getId() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		img = new JLabel(icon);
		this.add(info);
		this.add(img);
	}
}
