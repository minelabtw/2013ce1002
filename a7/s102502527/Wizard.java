package ce1002.a7.s102502527;

import ce1002.a7.s102502527.Hero;

public class Wizard extends Hero {
	
	Wizard(){
		super();
		setId("Wizard");
	}
	
	@Override
	
	float getHp(){
		return super.getHp() * 0.2f;
	}
	
	@Override
	
	float getMp(){
		return super.getMp() * 0.7f;
	}
	
	@Override
	
	float getPp(){
		return super.getPp() * 0.1f;
	}
}

