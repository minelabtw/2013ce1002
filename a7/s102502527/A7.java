package ce1002.a7.s102502527;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		
		MyPanel[] panel = new MyPanel[3];
		
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		
		hero[1] = new Swordsman();
		
		hero[2] = new Knight();
		
		frame.setSize(300,770);//開始的大小
		
		frame.setLocationRelativeTo(null);//置中or置左or置右
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//將右上角叉叉的符號用作跳開視窗
		
		for (int i=0;i<3;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i], panel[i], 10 , 10 + 240 * i );
		}
		frame.setVisible(true);
	}

}
