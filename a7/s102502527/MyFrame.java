package ce1002.a7.s102502527;

import javax.swing.JFrame;

import ce1002.a7.s102502527.Hero;
import ce1002.a7.s102502527.MyPanel;


public class MyFrame extends JFrame
{
	MyFrame()
	{
		this.setLayout(null);//把排版的方式用座標表示
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		this.add(panel);
		panel.setRoleState(hero);
		panel.setLocation(x, y);
	}
}

