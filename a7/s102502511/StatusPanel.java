package ce1002.a7.s102502511;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	protected JLabel slab1 = new JLabel();
	protected JLabel slab2 = new JLabel();
	protected JLabel slab3 = new JLabel();
	protected JLabel slab4 = new JLabel();
	protected JLabel slab5 = new JLabel();
	protected JLabel slab6 = new JLabel();
	
	StatusPanel(){
		setLayout(new GridLayout(3,2));		
		add(slab1); //照順序加入
		add(slab4);
		add(slab2);
		add(slab5);
		add(slab3);
		add(slab6);
	}
	
	public void setStatus(Hero hero){
		slab1.setText("HP");
		slab2.setText("MP");
		slab3.setText("PP");
		slab4.setText(" "+hero.getHp());
		slab5.setText(" "+hero.getMp());
		slab6.setText(" "+hero.getPp());
	}

}
