package ce1002.a7.s102502511;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	StatusPanel Spanel = new StatusPanel();
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		Spanel.setBounds(260 , 5 , 130 , 180); //設定Spanel的位置和大小
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(Spanel);
		
		
		
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		Spanel.setStatus(hero); //放入內容
	}
}