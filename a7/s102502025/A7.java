package ce1002.a7.s102502025;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] heroes = new Hero[3]; //列三個矩陣

		heroes[0] = new Wizard(); //輸入英雄
		heroes[1] = new Swordsman(); 
		heroes[2] = new Knight();

		MyFrame frame = new MyFrame();//列一個Frame
		MyPanel[] panel = new MyPanel[3];//列一個Panel
		frame.setSize(440, 740);//定下Frame的大小
		frame.setLocationRelativeTo(null);//顯示
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//基礎設定

		for (int k = 0; k < 3; k++) {//輸出英雄範圍
			panel[k] = new MyPanel();
			frame.newRolePos(heroes[k], panel[k], 10, 10 + 230 * k);
		}
		frame.setVisible(true); //允許顯示
	}
}
