package ce1002.a7.s102502025;

public class Knight extends Hero {
	public Knight() {
		setHero("Knight");//設定名字
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {//計算能力值
		Hp = (float) (.8 * 30);
	}

	public void setMp() {
		Mp = (float) (.1 * 30);
	}

	public void setPp() {
		Pp = (float) (.1 * 30);

	}
}
