package ce1002.a7.s102502025;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	StatusPanel Sp;

	public MyPanel() {
		setSize(400, 220);//panel大小
		setLayout(new BorderLayout());//輸出框框
		setBorder(new LineBorder(Color.black, 3));//設定黑色邊框
		Sp = new StatusPanel();
	}

	public void setRoleState(Hero hero) {//輸出圖形
		String str = "src/ce1002/a7/s102502025/" + hero.getHero() + ".jpg";
		ImageIcon icon = new ImageIcon(str);
		JLabel state = new JLabel(hero.getHero() + " HP: " + hero.getHp()
				+ " MP: " + hero.getMp() + " PP: " + hero.getPp());
		Sp.setState(hero);
		add(state, BorderLayout.NORTH);//輸出位置
		add(new JLabel(icon), BorderLayout.WEST);//輸出圖形的位置
		add(Sp, BorderLayout.CENTER);
	}
}
