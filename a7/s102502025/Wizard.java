package ce1002.a7.s102502025;

public class Wizard extends Hero {
	public Wizard() {
		setHero("Wizard");//設定名字
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {//計算能力值
		Hp = (float) (.2 * 30);
	}

	public void setMp() {
		Mp = (float) (.7 * 30);
	}

	public void setPp() {
		Pp = (float) (.1 * 30);
	}
}
