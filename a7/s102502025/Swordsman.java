package ce1002.a7.s102502025;

public class Swordsman extends Hero {
	public Swordsman() {
		setHero("Swordsman"); //設定名字
		setHp();
		setMp();
		setPp();
	}

	public void setHp() {//計算能力值
		Hp = (float) (.1 * 30);
	}

	public void setMp() {
		Mp = (float) (.1 * 30);
	}

	public void setPp() {
		Pp = (float) (.8 * 30);
	}
}
