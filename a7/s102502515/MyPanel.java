package ce1002.a7.s102502515;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel status = new StatusPanel();
	//protected StatusPanel statusSW = new StatusPanel();
	//protected StatusPanel statusKN = new StatusPanel();
	
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,190);
		status.setBounds(255, 25 , 160,180);//set statuspanel 的位置和大小
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(status);//add statuspanel
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		status.setState(hero);
	}
}
