package ce1002.a7.s102502515;

import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{
	/*Create three role panel*/
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 452 , 700);//frame的位置
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*
		 * set role position and image
		 */
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 225);
		newRolePos(heros[2] , panel3 , 10 , 440);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 420 , 210);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
}
