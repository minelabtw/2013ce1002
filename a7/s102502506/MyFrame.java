package ce1002.a7.s102502506;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.border.LineBorder;
import java.awt.GridLayout;

public class MyFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		setLayout(null);
		setBounds(300 , 50 , 385 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 350 , 200);//設定Panel的size
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//設定Panel的border
		panel.setRoleState(hero);
		panel.setStatus(hero);
		add(panel);//把Panel加進來
	}
}