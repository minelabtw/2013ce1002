package ce1002.a7.s102502506;

public class A7 {
	 
		public static void main(String[] args) {
			Hero[] heros = new Hero[3];
			heros[0] = new Wizard();
			heros[1] = new Swordsman();
			heros[2] = new Knight();
			for(Hero hero : heros){
				System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
			}
			MyFrame frame = new MyFrame(heros);
		}
}
