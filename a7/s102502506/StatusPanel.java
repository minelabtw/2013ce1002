package ce1002.a7.s102502506;
import java.awt.GridLayout;
import java.awt.TextArea;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class StatusPanel extends JPanel{
	private JLabel Hp = new JLabel("HP");
	private JLabel Mp = new JLabel("MP");
	private JLabel Pp = new JLabel("PP");
	private JLabel Hpvalue = new JLabel();
	private JLabel Mpvalue = new JLabel();
	private JLabel Ppvalue = new JLabel();
	public StatusPanel(){
		setLayout(new GridLayout(3,2));
	}
	public void setState(Hero hero) {
		Hpvalue.setText(String.valueOf(hero.getHp()));
		Mpvalue.setText(String.valueOf(hero.getMp()));
		Ppvalue.setText(String.valueOf(hero.getPp()));
		add(Hp);
		add(Hpvalue);
		add(Mp);
		add(Mpvalue);
		add(Pp);
		add(Ppvalue);
	}
}
