package ce1002.a7.s102502544;

import javax.swing.*;

public class MyPanel extends JPanel{
	
	MyPanel(){
		setLayout(null);  //設定排版
		setBounds(0, 0, 384, 211);  //設定邊界
	}
	
	public void setRoleState(Hero hero){
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");  //宣告圖案
		JLabel l1 = new JLabel(hero.getName() + " HP: " + hero.getHP()+ " MP: " + hero.getMP() + " PP:" + hero.getPP());  //設定能力標籤
		l1.setSize(260,10);  //設定標籤大小
		l1.setLocation(6, 5);  //設定標籤在panel的位置
		
		JLabel l2 = new JLabel(image);  //設定圖片標籤
		l2.setSize(260,190);  //設定標籤大小
		l2.setLocation(2, 15);  //設定標籤在panel的位置
		
		add(l1);  //把l1放到panel上
		add(l2);  //把l2放到panel上
		
		StatusPanel statuspanel=new StatusPanel(); //建立新的panel
		statuspanel.setState(hero); //設定panel裡的東西
		add(statuspanel); //加入新的panel
	}

}
