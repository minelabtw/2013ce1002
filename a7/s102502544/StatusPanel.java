package ce1002.a7.s102502544;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;

public class StatusPanel extends JPanel{
	JLabel l1=new JLabel("HP"); //建立新的label
	JLabel l2=new JLabel("MP");
	JLabel l3=new JLabel("PP");
	JLabel l4=new JLabel();
	JLabel l5=new JLabel();
	JLabel l6=new JLabel();
	
	StatusPanel(){
		setLayout(new GridLayout(3,2)); //設定排版
		setBounds(266,5,100,200); //設定位置大小
		add(l1); //將label加入panel
		add(l4);
		add(l2);
		add(l5);
		add(l3);
		add(l6);
	}
	public void setState(Hero hero) {
		l4.setText(""+hero.getHP()); //設定標籤裡的數字
		l5.setText(""+hero.getMP());
		l6.setText(""+hero.getPP());
	}

}
