package ce1002.a7.s102502544;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MyFrame extends JFrame{
	MyPanel p1 = new MyPanel();  //建立新的panel
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();
	
	MyFrame(){	
		
		setLayout(null);  //設定排版
		setSize(420, 730);  //設定frame的大小
		Hero[] hero = new Hero[3];//初始化三個英雄
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		newRolePos(hero[0] , p1 , 10 , 15);//設定panel
		newRolePos(hero[1] , p2 , 10 , 240);
		newRolePos(hero[2] , p3 , 10 , 465);
		
		add(p1);//把panel放到frame上
		add(p2);
		add(p3);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setBorder(new LineBorder(Color.BLACK,3)); //設定邊線
		panel.setRoleState(hero);  //設定panel裡的東西
		panel.setLocation(x, y);  //設定panel在frame裡的位置
	}

}
