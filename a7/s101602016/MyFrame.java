package ce1002.a7.s101602016;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
public class MyFrame extends JFrame{
	/*Create three role panel*/
	protected MyPanel panel1 = new MyPanel();//建構三種職業的panel
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 485 , 680);//設定視窗大小，因為增加了英雄的型態，所以寬度必須增加
		setVisible(true);//將預設顯示改為true
		setDefaultCloseOperation(EXIT_ON_CLOSE);//關閉視窗時關閉程式
		
		/*
		 * set role position and image
		 */
		newRolePos(heros[0] , panel1 , 10 , 10);//將三種職業的職業、panel、以及顯示位置傳到newRolePos函數中
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 400, 200);//設定標籤大小，因為增加了英雄的型態，所以寬度必須增加
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//畫寬度為3的黑色邊框
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);//將設定完的panel加入
	}
}