package ce1002.a7.s101602016;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Initialized hero array with length of 3
		Hero[] heros = new Hero[3];//建構型態為Hero的陣列，並拿來存放巫師，劍士，騎士
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		MyFrame frame = new MyFrame(heros);//設定新的英雄視窗		
	}
}
