package ce1002.a7.s101602016;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class StatusPanel extends JPanel{
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));//增加一個3*2的表格
	}
	public void setState(Hero hero)
	{
		/*將各職業的HP,MP,PP顯示出來*/
		add(new JLabel("HP"));
		add(new JLabel(Double.toString(hero.getHp())));//因為getHp出來的型態是double,所以必須將其型態轉為string
		add(new JLabel("MP"));
		add(new JLabel(Double.toString(hero.getMp())));//同上
		add(new JLabel("PP"));
		add(new JLabel(Double.toString(hero.getPp())));//同上
	}
}
