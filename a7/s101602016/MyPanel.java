package ce1002.a7.s101602016;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel statuspanel = new StatusPanel();//新增一個型態為StatusPanel的板panel	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		statuspanel.setBounds(255,5,140,190);//設定此panel的大小
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(statuspanel);//將此panel加入
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image and state*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		statuspanel.setState(hero);//設定英雄型態
	}
}