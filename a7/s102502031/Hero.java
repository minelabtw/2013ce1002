package ce1002.a7.s102502031;

public class Hero {
	private String name;
	private double HP;
	private double MP;
	private double PP;

	public Hero() { // default constructor
		setHP(30);
		setMP(30);
		setPP(30);
		setName("Hero");
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setHP(double HP) {
		this.HP = HP;
	}

	public void setMP(double MP) {
		this.MP = MP;
	}

	public void setPP(double PP) {
		this.PP = PP;
	}

	public String getName() {
		return name;
	}

	public double getHP() {
		return HP;
	}

	public double getMP() {
		return MP;
	}

	public double getPP() {
		return PP;
	}
}
