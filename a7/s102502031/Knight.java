package ce1002.a7.s102502031;

public class Knight extends Hero {
	public Knight() {
		super.setName("Knight"); // default name is its title Knight
	}

	public double getHP() {
		return super.getHP() * 0.8;
	}

	public double getMP() {
		return super.getMP() * 0.1;
	}

	public double getPP() {
		return super.getPP() * 0.1;
	}
}