package ce1002.a7.s102502031;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	public StatusPanel(Hero hero) {
		this.setLayout(new GridLayout(3, 2, 10, 10));
		setStatus(hero);
	}

	public void setStatus(Hero hero) {
		JLabel[] labels = new JLabel[6];
		labels[0] = new JLabel("HP");
		labels[1] = new JLabel("" + hero.getHP());
		labels[2] = new JLabel("MP");
		labels[3] = new JLabel("" + hero.getMP());
		labels[4] = new JLabel("PP");
		labels[5] = new JLabel("" + hero.getPP());
		for (int i = 0; i < labels.length; i++) {
			labels[i].setSize(100, 100);
			this.add(labels[i]);
		}
	}

}
