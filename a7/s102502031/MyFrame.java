package ce1002.a7.s102502031;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.FlowLayout;

public class MyFrame extends JFrame {
	private MyPanel[] panels = new MyPanel[3];
	private Hero[] heros = new Hero[3];

	MyFrame() {
		this.setLayout(new FlowLayout());
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		for (int i = 0; i < heros.length; i++) {
			panels[i] = new MyPanel();
			this.newRolePos(heros[i], panels[i]); // get the left part of the panel
			this.add(panels[i]);
		}
	}

	private void newRolePos(Hero hero, MyPanel panel) {
		panel.setRoleState(hero); // get property and figure
		panel.setBorder(new LineBorder(Color.black, 3));
		this.add(panel); // add panel to frame
	}
}
