package ce1002.a7.s102502031;

public class Wizard extends Hero {
	public Wizard() {
		super.setName("Wizard"); // default name is its title Wizard
	}

	public double getHP() {
		return super.getHP() * 0.2;
	}

	public double getMP() {
		return super.getMP() * 0.7;
	}

	public double getPP() {
		return super.getPP() * 0.1;
	}
}