package ce1002.a7.s102502031;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private JLabel property;
	private JLabel figure;

	public MyPanel() {
		this.setLayout(new BorderLayout(10, 10));
	}

	public void setRoleState(Hero hero) {
		// set figure
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg");
		this.figure = new JLabel(icon);

		// set property
		this.property = new JLabel(getHeroProperty(hero));

		// set StatusPanel
		StatusPanel status = new StatusPanel(hero);

		// little adjusting
		JPanel panelLeft = new JPanel();
		panelLeft.setLayout(new BorderLayout(10, 10));
		panelLeft.add(property, BorderLayout.NORTH);
		panelLeft.add(figure, BorderLayout.CENTER);
		panelLeft.add(new JLabel(""), BorderLayout.SOUTH); // a space between the lineBorder and figure
		JPanel panelRight = new JPanel();
		panelRight.setLayout(new BorderLayout(10, 10));
		panelRight.add(status, BorderLayout.CENTER);
		panelRight.add(new JLabel(""), BorderLayout.EAST);// a space between the lineBorder and figure

		// add to panel
		this.add(new JLabel(""), BorderLayout.WEST); // a space between the lineBorder and figure
		this.add(panelLeft, BorderLayout.CENTER);
		this.add(panelRight, BorderLayout.EAST);
	}

	private String getHeroProperty(Hero hero) { // get the string in label property
		String a = new String();
		a = hero.getName() + " ";
		a = a + "HP: " + hero.getHP() + " ";
		a = a + "MP: " + hero.getMP() + " ";
		a = a + "PP: " + hero.getPP();
		return a;
	}

}
