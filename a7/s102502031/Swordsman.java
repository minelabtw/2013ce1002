package ce1002.a7.s102502031;

public class Swordsman extends Hero {
	public Swordsman() {
		super.setName("Swordsman"); // default name is its title Swordsman
	}

	public double getHP() {
		return super.getHP() * 0.1;
	}

	public double getMP() {
		return super.getMP() * 0.1;
	}

	public double getPP() {
		return super.getPP() * 0.8;
	}
}