package ce1002.a7.s102502023;

import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel SP = new StatusPanel();
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		SP.setBounds(250, 15, 100, 180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(SP);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		SP.setState(hero);
	}
}
