package ce1002.a7.s102502023;

//import javax.swing.JLabel;
import javax.swing.*;

import java.awt.*;
public class StatusPanel extends JPanel {
	protected JLabel p = new JLabel();
    protected JLabel p1 = new JLabel();
    protected JLabel p2 = new JLabel();
    protected JLabel p3 = new JLabel();
    protected JLabel p4 = new JLabel();
    protected JLabel p5 = new JLabel();
    
	public StatusPanel() {
		  setLayout(new GridLayout(3, 2));
	}
  public void setState(Hero hero) {	 
	  p.setText("HP");
	  p1.setText("" + hero.getHp());
	  p2.setText("MP");
	  p3.setText("" + hero.getMp());
	  p4.setText("PP");
	  p5.setText("" + hero.getPp());
	  add(p);
	  add(p1);
	  add(p2);
	  add(p3);
	  add(p4);
	  add(p5);
  } // set strings and add them
}
