package ce1002.a7.s102502561;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	
	JLabel[] name;
	JLabel[] value;
	StatusPanel(Hero hero) {
		setLayout(new GridLayout(3,2,30,60));//��,�C,�e���j,�����j
		setState(hero);
		for(int i=0;i<3;i++){
			add(name[i]);
			add(value[i]);
		}
	}
	
	public void setState(Hero hero) {
		name = new JLabel[3];
		name[0] = new JLabel("HP");
		name[1] = new JLabel("MP");
		name[2] = new JLabel("PP");
		value = new JLabel[3];
		value[0] = new JLabel(""+hero.getHP());
		value[1] = new JLabel(""+hero.getMP());
		value[2] = new JLabel(""+hero.getPP());
	}
}
