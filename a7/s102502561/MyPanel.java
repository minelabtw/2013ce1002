package ce1002.a7.s102502561;

import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{

	JLabel info;
	JLabel img;
	ImageIcon icon;
	StatusPanel SP;
	
	MyPanel()
	{
		setLayout(new FlowLayout(FlowLayout.LEFT,2,0));//靠左,然後從左邊,上面各空多少
		this.setSize(360, 220);//職業框框大小寬,高
		this.setBorder(new LineBorder(Color.black, 5));	
	}
	
	public void setRoleState(Hero hero)//這會顯示英雄的能力值,讀入圖檔
	{
		
		SP = new StatusPanel(hero);
		icon = new ImageIcon(hero.getjob() + ".jpg");
		info = new JLabel(hero.getjob() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());//跟A5的class差不多
		img = new JLabel(icon);//指令
		this.add(info);
		this.add(img);
		this.add(SP);
	}
}
