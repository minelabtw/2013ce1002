package ce1002.a7.s101303504;
import java.awt.*;
import javax.swing.*;
public class StatusPanel extends JPanel{
	
	protected JLabel l1 = new JLabel();
	protected JLabel l2 = new JLabel();
	protected JLabel l3 = new JLabel();
	protected Hero []heros;
	
	StatusPanel(){
		//set panel's size and layout
		setLayout(null);
		setSize(200,180);
		l1.setSize(50,20);
		l2.setSize(50,20);
		l3.setSize(50,20);
		add(l1);
		add(l2);
		add(l3);
		add(new Button("ok"));
	}
	
	public void setState(Hero hero){
		
		
		l1.setText("HP:" + hero.getHP());
		l2.setText("MP:" + hero.getMP());
		l3.setText("PP:" + hero.getPP());
	}
}
