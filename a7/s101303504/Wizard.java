package ce1002.a7.s101303504;

public class Wizard extends Hero{

public Wizard(){
		
	super.setImage("image/Wizard.jpg");
	super.setName("Wizard");
	}
	
	// give each function value
	
	public double getHP(){
		return HP = 30*0.2 ;
	}
	
	public double getMP(){
		return MP = 30*0.7 ;
	}
	
	public double getPP(){
		return PP = 30*0.1 ;
	}
}