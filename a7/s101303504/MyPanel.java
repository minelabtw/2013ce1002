package ce1002.a7.s101303504;
import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{

	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel spl = new StatusPanel();
	MyPanel(){
		//set panel's size and layout
		setLayout(null);
		label.setBounds(10,10,220,20);
		imagelabel.setBounds(5,15,240,180);
		add(label);
		add(imagelabel);
		add(spl,BorderLayout.EAST);
	}
	
	public void setRoleState(Hero hero){
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP:"+hero.getHP()
				+ " MP:"+hero.getMP()+" PP:"+hero.getPP());
	}
	
}
