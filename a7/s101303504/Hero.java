package ce1002.a7.s101303504;
import javax.swing.ImageIcon;

public class Hero {

	protected String name;
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	
	public Hero(){
		//constructor
	}
	// store each of protected stuff
	public void setName(String name){
		this.name = name;
	}
	
	public void setHP(double HP){
		this.HP = HP ;
	}
	
	public void setMp(double MP){
		this.MP = MP;
	}
	
	public void setPP(double PP){
		this.PP = PP;
	}
	
	public void setImage(String imagename){
		this.image = new ImageIcon(imagename);
	}
	
	// print each of protected stuff
	public String getName(){
		return name;
	}
	
	public double getHP(){
		return HP=30;
	}
	
	public double getMP(){
		return MP=30;
	}
	
	public double getPP(){
		return PP=30;
	}
	
	public ImageIcon getImage(){
		return image;
	}
}