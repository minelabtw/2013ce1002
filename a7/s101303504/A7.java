package ce1002.a7.s101303504;


public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

Hero[] heros = new Hero[3];
		
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		for(Hero hero : heros){
			System.out.println( hero.getName() + " HP:"+ hero.getHP() 
					+ " MP:" + hero.getMP() + " PP:" + hero.getPP() );
		}
		
		/*
		 * Create a new frame
		 */
		MyFrame frame = new MyFrame(heros);
	}

}
