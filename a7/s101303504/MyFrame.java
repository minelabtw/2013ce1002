package ce1002.a7.s101303504;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame{

	protected MyPanel p1 = new MyPanel();
	protected MyPanel p2 = new MyPanel();
	protected MyPanel p3 = new MyPanel();
	protected Hero[] heros;
	
	MyFrame(Hero[] heros){
		this.heros = heros;
		setLayout(null);
		setBounds(300,50,500,700);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//set role position and image
		newRolePos(heros[0] , p1 , 10 , 10);
		newRolePos(heros[1] , p2 , 10 , 220);
		newRolePos(heros[2] , p3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 450 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
		
	}
}