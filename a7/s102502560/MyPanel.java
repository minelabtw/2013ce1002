package ce1002.a7.s102502560;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	Hero hero;
	JLabel statuslabel;
	JLabel picturelabel;
	StatusPanel spanel;
	
	public MyPanel() {
		statuslabel=new JLabel();
		picturelabel=new JLabel();
		spanel=new StatusPanel();
	}
	
	public void setRoleState(Hero hero){
		statuslabel.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		picturelabel.setIcon(new ImageIcon(hero.getName()+".jpg"));
		this.hero=hero;
		spanel.setState(hero);
		setLayout(null);
		statuslabel.setBounds(10, 0, 250, 20);
		picturelabel.setBounds(10, 20, 250, 190);
		spanel.setBounds(270, 10, 160, 200);
		add(statuslabel);
		add(picturelabel);
		add(spanel);
		setBorder(new LineBorder(Color.BLACK, 2));
	}
}
