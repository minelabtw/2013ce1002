package ce1002.a7.s102502560;

public class A7 {
	public static void main(String[] args) {
		Hero[] heros=new Hero[]{new Wizard(),new Swordsman(),new Knight()};		//superclass type array with different subclass object
		
		MyFrame frame=new MyFrame();
		for(Hero hero: heros){
			frame.newRolePos(hero);
		}
		frame.setSize(500,40+frame.panels.size()*230);
		frame.setVisible(true);
	}
}
