package ce1002.a7.s102502560;

public class Swordsman extends Hero{
	public Swordsman() {
		setName("Swordsman");
	}
	
	@Override
	public double getHP() {
		return super.getHP()*0.1;
	}
	
	@Override
	public double getMP() {
		return super.getMP()*0.1;
	}
	
	@Override
	public double getPP() {
		return super.getPP()*0.8;
	}
}
