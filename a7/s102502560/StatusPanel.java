package ce1002.a7.s102502560;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.TreeMap;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	TreeMap<String, JLabel> namelabels=new TreeMap<String, JLabel>();
	TreeMap<String, JLabel> valuelabels=new TreeMap<String, JLabel>();
	Hero hero;
	
	public StatusPanel() {
		
		namelabels.put("hp",new JLabel("HP"));
		namelabels.put("mp",new JLabel("MP"));
		namelabels.put("pp",new JLabel("PP"));
		valuelabels.put("hp",new JLabel());
		valuelabels.put("mp",new JLabel());
		valuelabels.put("pp",new JLabel());
		
		setLayout(new GridLayout(0,2));
		
		for(String labelname: new String[]{"hp","mp","pp"}){
			//nl.setPreferredSize(getPreferredSize());
			add(namelabels.get(labelname));
			add(valuelabels.get(labelname));
		}
		
	}
	
	public void setState(Hero hero) {
		valuelabels.get("hp").setText(""+hero.getHP());
		valuelabels.get("mp").setText(""+hero.getMP());
		valuelabels.get("pp").setText(""+hero.getPP());
	}
	
}
