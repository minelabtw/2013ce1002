package ce1002.a7.s102502516;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel myStatusPanel = new StatusPanel();

	MyPanel() {
		/* set panel's size and layout */
		setLayout(null);
		label.setBounds(5, 0, 220, 20);
		imagelabel.setBounds(5, 15, 240, 180);
		myStatusPanel.setBounds(260, 15, 180, 180 ); //設定右方狀態的位置
		/* add label to panel */
		add(label);
		add(imagelabel);
		add(myStatusPanel);//增加狀態
	}

	public void setRoleState(Hero hero) {
		/* set label's text and image */
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP:" + hero.getPP());
		myStatusPanel.setState(hero); //以hero呼叫函式設定狀態內容

	}
}