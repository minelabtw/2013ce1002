package ce1002.a7.s102502516;

public class A7 {

	public static void main(String[] args) {

		Hero[] heros = new Hero[3]; // 宣告物件陣列
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight(); // 初始化

		MyFrame frame = new MyFrame(heros);

	}
}
