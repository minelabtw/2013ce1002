package ce1002.a7.s102502516;

import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	StatusPanel() {
		setLayout(new GridLayout(3, 2)); // 產生3*2表格放置內容
	}

	public void setState(Hero hero) {
		add(new Label("HP"));
		add(new Label("" + hero.getHP()));
		add(new Label("MP"));
		add(new Label("" + hero.getMP()));
		add(new Label("PP"));
		add(new Label("" + hero.getPP()));// 依序加入內容
		setVisible(true);// 設定為顯示
	}
}
