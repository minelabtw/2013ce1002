package ce1002.a7.s102502516;

public class Knight extends Hero {
	public String getName() { // 名字寫死
		return "Knight";
	}

	public Knight() {
		// setup the title for this character's name
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}

	// 利用override，回傳加權後的屬性值
	@Override
	public float getHP() {
		return (float) (super.getHP() * 0.8);
	}

	public float getMP() {
		return (float) (super.getMP() * 0.1);
	}

	public float getPP() {
		return (float) (super.getPP() * 0.1);
	}
}
