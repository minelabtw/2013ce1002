package ce1002.a7.s101201522;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel {
	private JLabel[] labels = new JLabel[6];
	
	StatusPanel () {
		setLayout(new GridLayout(3,2));
		for (int i=0;i<6;i++) {
			labels[i] = new JLabel();
			add(labels[i]);
		}
	}
	
	public void setState (Hero hero) {
		labels[0].setText("HP");
		labels[1].setText(hero.getHp()+"");
		labels[2].setText("MP");
		labels[3].setText(hero.getMp()+"");
		labels[4].setText("PP");
		labels[5].setText(hero.getPp()+"");
	}	
}
