package ce1002.a7.s101201522;

public class Wizard extends Hero {
	 
	public Wizard () {//initial
		setName("Wizard");
		setImage("Wizard.jpg");
	}
	
	/*override every getter in hero*/
	public double getHp () {
		return hp*0.2;
	}
	
	public double getMp () {
		return mp*0.7;
	}
	
	public double getPp () {
		return pp*0.1;
	}
}