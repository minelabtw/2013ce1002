package ce1002.a7.s101201522;

public class Swordsman extends Hero {
	
	public Swordsman ()	{//initial
		setName("Swordsman");
		setImage("Swordsman.jpg");
	}
	
	/*override every getter in hero*/
	public double getHp () {
		return hp*0.1;
	}
	
	public double getMp () {
		return mp*0.1;
	}
	
	public double getPp () {
		return pp*0.8;
	}
}