package ce1002.a7.s101201522;

public class Knight extends Hero {
	
	public Knight () {//initial
		setName("Knight");
		setImage("Knight.jpg");
	}
	
	/*override every getter in hero*/
	public double getHp () {
		return hp*0.8;
	}
	
	public double getMp () {
		return mp*0.1;
	}
	
	public double getPp () {
		return pp*0.1;
	}
}