package ce1002.a7.s101201522;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	private MyPanel[] heros = new MyPanel[3];//three kinds of hero's Panel
	
	MyFrame () {//initial
		/*set MyFrame's size and position in windows*/
		setLayout(null);
		setBounds(0,0,440,680);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		/*initial MyPanel[]*/
		heros[0] = new MyPanel();
		heros[1] = new MyPanel();
		heros[2] = new MyPanel();
		/*set every MyPanel's size and position*/
		newRolePos(new Wizard(),heros[0],10,10);
		newRolePos(new Swordsman(),heros[1],10,220);
		newRolePos(new Knight(),heros[2],10,430);
		setVisible(true);
	}
	
	public void newRolePos (Hero hero, MyPanel panel, int x, int y) {//set panel's size, position, border and labels in panel
		panel.setBounds(x,y,400,200);
		panel.setRoleState(hero);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel);//add panel to MyFrame
	}
}