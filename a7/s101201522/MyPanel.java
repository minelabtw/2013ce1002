package ce1002.a7.s101201522;

import javax.swing.*;
 
public class MyPanel extends JPanel {
	private JLabel label = new JLabel();//hero's status
	private JLabel imagelabel = new JLabel();//hero;s image
	private StatusPanel status = new StatusPanel();//hero's status2
	
	MyPanel () {//initial
		/*set labels' and subPanel's size and position in MyPanel*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		status.setBounds(255,5,140,190);
		/*add labels and subPanel to MyPanel*/
		add(label);
		add(imagelabel);
		add(status);
	}
	
	public void setRoleState (Hero hero) {//set labels and subPanel
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName() + " HP: " + hero.getHp() + " MP: " + hero.getMp() + " PP:" + hero.getPp());
		status.setState(hero);
	}
}