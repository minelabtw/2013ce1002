package ce1002.a7.s102502548;

public class Hero {
	private String Name ;//宣告變數
	private double HP ;
	private double MP ;
	private double PP ;
	
	Hero(){//建構子~初始化各變數
		HP=30.0 ;
		MP=30.0 ;
		PP=30.0 ;
	}
	
	public void setName(String s){//設定名字、HP、MP、PP
		this.Name=s ;
	}
	public void setHP(double h){
		this.HP=h ;
	}
	public void setMP(double m){
		this.MP=m ;
	}
	public void setPP(double p){
		this.PP=p ;
	}
	
	public String getName(){//回傳名字、HP、MP、PP
		return Name ;
	}
	public double getHP(){
		return HP ;
	}
	public double getMP(){
		return MP ;
	}
	public double getPP(){
		return PP ;
	}
}
