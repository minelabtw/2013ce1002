package ce1002.a7.s102502548;

import javax.swing.* ;
import javax.swing.border.LineBorder;

import java.awt.* ;

public class MyPanel extends JPanel {
	JLabel[] label=new JLabel[2] ;
	
	public MyPanel(Hero hero) {
		ImageIcon image1 = new ImageIcon(hero.getName()+".jpg") ;//宣告圖片物件
		StatusPanel statuspanel=new StatusPanel(hero) ;
		
		label[0]=new JLabel() ;
		label[1]=new JLabel() ;
		
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK, 3)) ;
		
		label[0].setBounds(5,5,300,10) ;//設定邊界大小
		label[1].setBounds(5,20,300,180) ;
		statuspanel.setBounds(275,35,100,100) ;
		
		add(label[0]) ;//加上標籤
		add(label[1]) ;
		add(statuspanel) ;
		
		label[0].setText(hero.getName()+" HP:"+hero.getHP()+" MP:"+hero.getMP()+" PP:"+hero.getPP()) ;//在標籤上輸出
		label[1].setIcon(image1) ;//輸出圖片
	}
}
