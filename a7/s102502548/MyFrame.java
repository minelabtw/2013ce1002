package ce1002.a7.s102502548;

import javax.swing.* ;

import java.awt.* ; 

public class MyFrame extends JFrame {
	 MyPanel[]  mypanel=new  MyPanel[4] ;//宣告一個大小為4的MyPanel陣列
	 
	 MyFrame(){
		Knight k=new Knight() ;//創立物件
		Swordsman s=new Swordsman() ;
		Wizard w=new Wizard() ;
		Hero[] hero=new Hero[3] ;
		
		    hero[0]=w ;//使用多型
			hero[1]=s ;
			hero[2]=k ;
		 
		 mypanel[0]=new MyPanel(hero[0]) ;//把MyPanel丟進我的mypanel陣列
		 mypanel[1]=new MyPanel(hero[1]) ;
		 mypanel[2]=new MyPanel(hero[2]) ;
		 
		 mypanel[0].setBounds(30, 30, 400, 200) ;
		 mypanel[1].setBounds(30, 250, 400, 200) ;
		 mypanel[2].setBounds(30, 470, 400, 200) ;
		 
		 setLayout(null) ;//弄掉預設
		 
		 add(mypanel[0]) ;//在視窗上加上面板
		 add(mypanel[1]) ;
		 add(mypanel[2]) ;
		 
		 setSize(600,1000) ;//設定大小
		 
		 setVisible(true) ;//設定看的見
	 }

}
