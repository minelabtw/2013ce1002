package ce1002.a7.s102502548;

import javax.swing.* ;

import java.awt.* ;

public class StatusPanel extends JPanel{
	protected JLabel[] labels=new JLabel[6] ;//創立陣列
	
	public StatusPanel (Hero hero){
		for (int x=0;x<6;x++){
			labels[x]=new JLabel() ;//把JLabel丟進陣列
		}
	
		setState(hero) ;
	}
	
	public void setState(Hero hero) {
		setLayout(new GridLayout(3,2,5,5)) ;//印一個表格式的面板
		
		for (int x=0;x<6;x++){
			add(labels[x]) ;//貼上標籤
		}
			
		labels[0].setText("HP") ;//輸出
		labels[1].setText(""+hero.getHP()) ;
		labels[2].setText("MP") ;
		labels[3].setText(""+hero.getMP()) ;
		labels[4].setText("PP") ;
		labels[5].setText(""+hero.getPP());
	}
}
