package ce1002.a7.s102502513;

public class Swordsman extends Hero
{
	public Swordsman()
	{
		// setup the title for this character's name 
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	
	public double getHP()
	{
		return HP*0.1;
	}
		
	public double getMP()
	{
		return MP*0.1;
	}
	
	public double getPP()
	{
		return PP*0.8;
	}
	
}
