package ce1002.a7.s102502513;

import javax.swing.*;

public class Hero //父類別，其setter，getter可被竄改
{
	protected String NAME;  //用protected使其他子類別可使用
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	
	public String getName()
	{
		return NAME;
	}
	
	public void setName(String name)
	{
		NAME = name;
	}
	
	public double getHP()
	{
		return HP;
	}
	
	public void setHP(double hp)
	{
		HP = hp;
	}
	
	public double getMP()
	{
		return MP;
	}
	
	public void setMP(double mp)
	{
		MP = mp;
	}
	
	public double getPP()
	{
		return PP;
	}
	
	public void setPP(double pp)
	{
		PP = pp;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}
