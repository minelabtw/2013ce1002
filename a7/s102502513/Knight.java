package ce1002.a7.s102502513;

public class Knight extends Hero  //Knight子類別　繼承　Hero父類別
{
	public Knight()
	{	
		// setup the title for this character's name
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	public double getHP()  //竄改父類別
	{
		return HP*0.8;
	}
	
	
	public double getMP()
	{
		return MP*0.1;
	}
	
	
	public double getPP()
	{
		return PP*0.1;
	}
}
