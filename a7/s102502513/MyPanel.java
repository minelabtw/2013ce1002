package ce1002.a7.s102502513;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel
{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel status = new StatusPanel();
	
	MyPanel()
	{
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,190);
		status.setBounds(255, 25 , 160,180);//set statuspanel 的位置和大小
		/*加入label到panel*/
		add(label);
		add(imagelabel);
		add(status);
	}
	
	public void setRoleState(Hero hero)
	{
		/*設置 label's 內容跟圖檔*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
		status.setState(hero);
	}
}
