package ce1002.a7.s102502513;

public class A7
{
	public static void main(String[] args) 
	{
		Hero heros[] = new Hero[3];  //宣告Hero物件陣列 
		
		heros[2] = new Knight();  //將第三項陣列改為Knight物件
		heros[1] = new Swordsman();
		heros[0] = new Wizard();
		
		heros[2].setName("Knight");  //傳入名稱
		heros[1].setName("Swordsman");
		heros[0].setName("Wizard");
		
		for(int i=0; i<=2; i++)  //傳入ＨＰ,ＭＰ,ＰＰ=３０
		{
			heros[i].setHP(30.0);
			heros[i].setMP(30.0);
			heros[i].setPP(30.0);	
		}
		
		/*
		 * Use polymorphism to print out the result of different 
		 * hero's name,hp,mp,pp
		 */
		for(Hero hero : heros)
		{
			System.out.println( hero.getName() + " HP:"+ hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP() );
		}
		
		
		/*
		 * Create a new frame
		 */
		MyFrame frame = new MyFrame(heros);
	}
}
