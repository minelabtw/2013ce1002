package ce1002.a7.s102502513;

import java.awt.*;  //載入AWT類別庫
import javax.swing.*;  //載入Swing類別庫

public class MyFrame extends JFrame
{
	/*Create three role panel*/
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero heros[];
	
	MyFrame(Hero heros[])
	{
		this.heros = heros;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 452 , 700);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*
		 * set role position and image
		 */
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 225);
		newRolePos(heros[2] , panel3 , 10 , 440);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*設置位置和大小*/
		panel.setBounds(x, y, 420 , 210);
		/*設置 panel's 面板*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*設置 label於 panel裡*/
		panel.setRoleState(hero);
		/*將 panel 放入 frame*/
		add(panel);
	}
}
