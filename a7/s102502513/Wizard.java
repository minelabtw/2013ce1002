package ce1002.a7.s102502513;

public class Wizard extends Hero 
{
	public Wizard()
	{
		// setup the title for this character's name
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	
	public double getHP()
	{
		return HP*0.2;
	}
	
	public double getMP()
	{
		return MP*0.7;
	}
	
	public double getPP()
	{
		return PP*0.1;
	}
}
