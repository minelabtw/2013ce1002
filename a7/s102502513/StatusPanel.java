package ce1002.a7.s102502513;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel 
{
	protected JLabel lblHP = new JLabel();
	protected JLabel lblMP = new JLabel();
	protected JLabel lblPP = new JLabel();
	protected JLabel lblgetHP = new JLabel();
	protected JLabel lblgetMP = new JLabel();
	protected JLabel lblgetPP = new JLabel(); 
	
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));
		lblHP.setBounds(220,0,232,75);//set label的位置與大小
		lblMP.setBounds(220,20,232,75);
		lblPP.setBounds(220,40,232,75);
		lblgetHP.setBounds(390, 0, 232,75);

		add(lblHP);//add label
		add(lblgetHP);
		add(lblMP);
		add(lblgetMP);
		add(lblPP);
		add(lblgetPP);
	}
	public void setState(Hero hero)
	{
		 lblHP.setText("HP");
		 lblMP.setText("MP");
		 lblPP.setText("PP");
		 lblgetHP.setText(" " + hero.getHP());
		 lblgetMP.setText(" " + hero.getMP());
		 lblgetPP.setText(" " + hero.getPP());	 
	}
}
