package ce1002.a7.s102502523;
import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel
{
	JLabel[] line= new JLabel[6];//define
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));
		setSize(100,120);
	}
	public void setState(Hero hero)
	{ 
		line [0] = new JLabel("HP: ");//setting
		line [1] = new JLabel(hero.getHp()+" ");
		line [2] = new JLabel("MP: ");
		line [3] = new JLabel(hero.getMp()+" ");
		line [4] = new JLabel("PP: ");
		line [5] = new JLabel(hero.getPp()+" ");
		for(int x=0 ;x<6;x++)//add
		{
			add(line[x]);
		}
	}
}