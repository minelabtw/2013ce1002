package ce1002.a7.s102502523;

import javax.swing.*;
import java.awt.*;

 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel sta=new StatusPanel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		sta.setBounds(250,15,200,180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(sta);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		sta.setState(hero);
	}
}