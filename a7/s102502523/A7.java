package ce1002.a7.s102502523;

public class A7 {
 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Initialized hero array with length of 3
		Hero[] heros = new Hero[3];
		
		/*
		 * Initialized different hero with different index
		 */
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		
		new MyFrame(heros);
		
	}
 
}
