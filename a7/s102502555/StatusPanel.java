package ce1002.a7.s102502555;

import javax.swing.*;
import java.awt.*;

public class StatusPanel extends JPanel{
	JLabel hp = new JLabel("HP");  //hp標籤
	JLabel mp = new JLabel("MP");  //mp標籤
	JLabel pp = new JLabel("PP");  //pp標籤
	JLabel hpValue = new JLabel();  //hp值的標籤
	JLabel mpValue = new JLabel();  //mp值的標籤
	JLabel ppValue = new JLabel();  //pp值的標籤
	
	StatusPanel(){
		setLayout(new GridLayout(3 , 2));  //用表格排版
		setSize(130 , 190);  //設能力值panel大小
		add(hp);  //把hp加到panel
		add(hpValue);  //把hp值加到panel
		add(mp);  //把mp加到panel
		add(mpValue);  //把mp值加到panel
		add(pp);  //把pp加到panel
		add(ppValue);  //把pp值加到panel
	}
	
	public void setState(Hero hero){
		hpValue.setText("" + hero.getHp());  //設定HP值
		mpValue.setText("" + hero.getMp());  //設定MP值
		ppValue.setText("" + hero.getPp());  //設定PP值
	}
}
