package ce1002.a7.s102502555;

public class Swordsman  extends Hero{
	Swordsman(double h , double m , double p){
		super(h , m , p);  //初始化生命,魔法,能力
		setName("Swordsman");  //設定名稱
	}
	
	//取得加權後的HP
	public double getHp() {
		return super.getHp() * 0.1;
	}
	
	//取得加權後的MP
	public double getMp() {
		return super.getMp() * 0.1;
	}
	
	//取得加權後的PP
	public double getPp() {
		return super.getPp() * 0.8;
	}
}
