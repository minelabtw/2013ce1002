package ce1002.a7.s102502555;

import javax.swing.*;

public class MyPanel extends JPanel{
	JLabel l1 , l2;
	StatusPanel sp;
	
	MyPanel(){
		setLayout(null);  //設定排版
		setSize(400 , 210);  //設定大小
	}
	
	public void setRoleState(Hero hero){
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg");  //宣告圖案
		l1 = new JLabel(hero.getName() + " HP: " + hero.getHp()
							+ " MP: " + hero.getMp() + " PP:" + hero.getPp());  //設定能力標籤
		l1.setSize(250,10);  //設定標籤大小
		l1.setLocation(5, 5);  //設定標籤在panel的位置
		
		l2 = new JLabel(icon);  //設定圖片標籤
		l2.setSize(250,190);  //設定標籤大小
		l2.setLocation(5, 16);  //設定標籤在panel的位置
		
		sp = new StatusPanel();  //設定顯示能力的panel
		sp.setState(hero);  //設定StatusPanel裡的hero數值
		sp.setLocation(265, 5);  //設定panel的位置
		
		add(l1);  //把l1放到panel上
		add(l2);  //把l2放到panel上
		add(sp);  //把StatusPanel放到panel上
	}
}
