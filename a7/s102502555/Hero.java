package ce1002.a7.s102502555;

public class Hero {
	private String name;
	private double hp;
	private double mp;
	private double pp;
	
	//初始化生命,魔法,能力
	Hero(double h , double m , double p){
		hp = h;
		mp = m;
		pp = p;
	}
	
	//取得名稱
	public String getName() {
		return name;
	}

	//設定名稱
	public void setName(String name) {
		this.name = name;
	}

	//取得HP
	public double getHp() {
		return hp;
	}

	//設定HP
	public void setHp(double hp) {
		this.hp = hp;
	}

	//取得MP
	public double getMp() {
		return mp;
	}

	//設定MP
	public void setMp(double mp) {
		this.mp = mp;
	}

	//取得PP
	public double getPp() {
		return pp;
	}

	//設定PP
	public void setPp(double pp) {
		this.pp = pp;
	}
}
