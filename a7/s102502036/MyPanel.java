package ce1002.a7.s102502036;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
public class MyPanel extends JPanel{
	private String picture ;
	public MyPanel(String hero){
		picture = hero;
		
	}
	public void setRoleState(Hero hero){
		StatusPanel panel4 = new StatusPanel() ;
		panel4.setState(hero) ;
		ImageIcon image = new ImageIcon(picture) ;
		JLabel Label1  ;
		JLabel Label2 ; 
		Label1 = new JLabel(image) ;	//把圖片放到label
		Label2 = new JLabel("Hp: "+hero.hpgetter()+"Mp: "+hero.mpgetter()+"Pp: "+hero.ppgetter()) ;	//把文字放到label
		add(Label2);
		add(Label1);
		add(panel4) ;	//把panel4(用來顯示數值)加進去 
	}

}
