package ce1002.a7.s102502036;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;


public class MyFrame extends JFrame  {
	
	public MyFrame(Hero hero[]){
		setSize(400,900);
		MyPanel panel1 = new MyPanel("Wizard.jpg") ;//panel1來存Wizard物件
		MyPanel panel2 = new MyPanel("Swordsman.jpg") ; //panel2來存Swordsman物件
		MyPanel panel3 = new MyPanel("Knight.jpg") ; //panel3來存Knight物件		
		newRolePos(hero[0] , panel1 ,0 ,10);
		newRolePos(hero[1],panel2,0,230) ;
		newRolePos(hero[2],panel3,0, 450);		
		setVisible(true);
	}
	 public void newRolePos(Hero hero  , MyPanel panel , int x , int y){
		 panel.setVisible(true);
		 panel.setRoleState(hero) ;
		 setLayout(null);
		 panel.setBounds(x ,y , 350 , 230);//替panel設定大小
		 panel.setBorder(BorderFactory.createLineBorder(Color.black,2));//設定邊框
		 add(panel);
		 
	 }
	
	 
	 
	 
}
