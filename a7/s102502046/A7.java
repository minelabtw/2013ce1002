package ce1002.a7.s102502046;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] h = new Hero[3] ;
		h[0] = new Wizard();
		h[1] = new Swordsman();
		h[2] = new Knight();
		
		MyFrame frame = new MyFrame();//
		frame.setSize(400, 770);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MyPanel[] panel = new MyPanel[3]; //

		for (int a = 0; a < 3; a++) //
		{
			panel[a] = new MyPanel();
			frame.newRolePos(h[a], panel[a], 10, 10 + 240 * a);
		}
		frame.setVisible(true);
	}

}
