package ce1002.a7.s102502013;

import javax.swing.JPanel;
import javax.swing.JLabel;

public class StatusPanel extends JPanel{
	private JLabel status[];
	StatusPanel(){
		status = new JLabel[6];
	}
	public void setRoleStatus(Hero hero){
		status[0] = new JLabel("HP");
		status[1] = new JLabel(hero.getHP() + "");
		status[2] = new JLabel("MP");
		status[3] = new JLabel(hero.getMP() + "");
		status[4] = new JLabel("PP");
		status[5] = new JLabel(hero.getPP() + "");
		for(int i = 0; i < 6; i++){
			add(status[i]);
		}
	}
}
