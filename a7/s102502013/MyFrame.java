package ce1002.a7.s102502013;

import javax.swing.BorderFactory;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
public class MyFrame extends JFrame{
	private MyPanel myPanel[];
	
	MyFrame(){
		myPanel = new MyPanel[3];
		Hero hero[] = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		JPanel contentOfRole = new JPanel();
		//set picture's frame
		for(int i=0;i<3;i++){
			myPanel[i] = new MyPanel();
			newRolePos( hero[i], myPanel[i] , 5 ,5+215*i);
			contentOfRole.add(myPanel[i]);
			contentOfRole.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
			contentOfRole.setLayout(new BorderLayout());
			setContentPane(contentOfRole);
		}
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setRoleState(hero);
		panel.setLayout(null);
		panel.setBounds(x, y, 475, 220);
		panel.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panel.add(panel.getRoleSituation());
		panel.add(panel.getRolePicture());
		panel.add(panel.getStatus());
	}
}
