package ce1002.a7.s102502013;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
	private JLabel situation;
	private JLabel picture;
	private StatusPanel status;
	MyPanel(){
		
	}
	public void setRoleState(Hero hero){
		situation = new JLabel(hero.getName() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		picture = new JLabel(new ImageIcon(hero.getName() + ".jpg"));
		status = new StatusPanel();
		status.setRoleStatus(hero);
		status.setLayout(new GridLayout(3, 2, 5, 5));
		status.setSize(200, 175);//show situationlabel's size
		status.setLocation(256, 20);//set statuspanel's location
		situation.setSize(225, 20);//show statuspanel's size
		picture.setSize(250,200);//show picturelabel's size
		situation.setLocation(5, 0);//set situationlabel's location
		picture.setLocation(5, 20);//set picturelabel's location
		
	}
	public JLabel getRoleSituation(){
		return situation;
	}
	public JLabel getRolePicture(){
		return picture;
	}
	public StatusPanel getStatus(){
		return status;
	}
}
