package ce1002.a7.s102502013;

public class Wizard extends Hero{
	private final double hpCoefficient = 0.2;//declare a variable for hpCoefficient
	private final double mpCoefficient = 0.7;//declare a variable for mpCoefficient
    private final double ppCoefficient = 0.1;//declare a variable for ppCoefficient
    Wizard(){
    	super.setName("Wizard");
    }
    public double getHP(){//overwrite hero' getHP function
		return hpCoefficient * super.getHP();
	}
	public double getMP(){//overwrite hero' getMP function
		return mpCoefficient * super.getMP();
	}
	public double getPP(){//overwrite hero' getPP function
		return ppCoefficient * super.getPP();
	}
}
