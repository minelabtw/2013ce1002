package ce1002.a7.s102502005;

import java.awt.GridLayout;

import javax.swing.*;

public class StatusPanel extends JPanel{
	
	public void setState(Hero hero) {
		
		//將StatusPanel的Layout改為GridLayout(上下格數,左右格數)。
		this.setLayout(new GridLayout(3, 2));
		
		//依序將元件放上去(由左到右再由上到下)。
		this.add(new JLabel("HP"));
		this.add(new JLabel("" + hero.getHP()));
		this.add(new JLabel("MP"));
		this.add(new JLabel("" + hero.getMP()));
		this.add(new JLabel("PP"));
		this.add(new JLabel("" + hero.getPP()));
		
	}
}
