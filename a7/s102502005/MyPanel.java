package ce1002.a7.s102502005;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	
	String IconPath;
	String HeroStatus;
	StatusPanel statusPanel = new StatusPanel();
	
	MyPanel() {
	}
	
	public void setRoleState(Hero hero){
		
		//做好左半邊的文字及圖形。
		this.IconPath = hero.getIconPath();
		this.HeroStatus = hero.getHeroname() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP();
		
		ImageIcon HeroIcon = new ImageIcon(IconPath); 
		JLabel Status = new JLabel(HeroStatus);
		JLabel Icon = new JLabel(HeroIcon);
		
		//做好包含右半邊三個JLable的JPanel。
		statusPanel.setState(hero);
		
		
		//設定好各元件的位置與大小。
		this.setLayout(null);
		Status.setBounds(5,3,250,15);
		Icon.setBounds(5,18,250,190);
		statusPanel.setBounds(267, 3, 151, 207);
		
		//把元件加上去並設立邊界。
		this.add(Status);
		this.add(Icon);
		this.add(statusPanel);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}

}
