package ce1002.a7.s102502005;

public class Knight extends Hero{
	
	Knight(){
		this.setHeroname("Knight");
		this.setIconPath("Knight.jpg");
	}
	
	String getIconPath(){
		return "Knight.jpg";
	}
	
	double getHP() //getter
	{
		return this.HP * 0.8;
	}
	
	double getMP()
	{
		return this.MP * 0.1;
	}
	
	double getPP()
	{
		return this.PP * 0.1;
	}

}
