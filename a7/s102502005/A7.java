package ce1002.a7.s102502005;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
	
		//建立三個Hero。
		Wizard wizard = new Wizard();
		Swordsman swordsman = new Swordsman();
		Knight knight = new Knight();
		
		//生成Frame。
		MyFrame frame = new MyFrame();
		frame.setLayout(null);
		frame.newRolePos(wizard,frame.WizardPanel,15,15);
		frame.newRolePos(swordsman,frame.SwordsManPanel,15,235);
		frame.newRolePos(knight,frame.KnightPanel,15,455);
		
		frame.setSize(467, 720);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
