package ce1002.a7.s102502005;

public class Wizard extends Hero {

	Wizard() {
		this.setHeroname("Wizard");
		this.setIconPath("Wizard.jpg");
	}

	String getIconPath() {
		return "Wizard.jpg";
	}

	double getHP() // getter
	{
		return this.HP * 0.2;
	}

	double getMP() {
		return this.MP * 0.7;
	}

	double getPP() {
		return this.PP * 0.1;
	}

}
