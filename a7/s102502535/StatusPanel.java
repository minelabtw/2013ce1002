package ce1002.a7.s102502535;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel();
	protected JLabel label6 = new JLabel();

	protected JPanel statuspanel = new JPanel();

	StatusPanel() {
		setLayout(null);
		setLayout(new GridLayout(3, 2)); // set table's height and width
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6); // add labels into the table
		setVisible(true);
	}

	public void setState(Hero hero) {
		label1.setText("HP");
		label2.setText(hero.getHp() + "");
		label3.setText("MP");
		label4.setText(hero.getMp() + "");
		label5.setText("PP");
		label6.setText(hero.getPp() + ""); // set each label
	}
}
