package ce1002.a7.s102502535;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();

	protected StatusPanel statuspanel = new StatusPanel();

	MyPanel() {
		setLayout(null);
		label.setBounds(5, 0, 220, 20);
		imagelabel.setBounds(5, 15, 240, 180);
		statuspanel.setBounds(250, 15, 180, 180);
		add(label); // add label to panel
		add(imagelabel); // add label to panel
		add(statuspanel); // add statuspanel to panel
	}

	public void setRoleState(Hero hero) {
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName() + " HP: " + hero.getHp() + " MP: "
				+ hero.getMp() + " PP:" + hero.getPp()); // set label
		statuspanel.setState(hero); // set statuspanel
	}

}
