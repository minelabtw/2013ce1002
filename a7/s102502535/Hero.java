package ce1002.a7.s102502535;

import javax.swing.ImageIcon;

public class Hero {

	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;

	public Hero() {
		setHp(30);
		setMp(30);
		setPp(30);
	}

	// getter and setter for every variable
	public void setName(String n) {
		this.name = n;
	}

	public String getName() {
		return this.name;
	}

	public double getHp() {
		return this.hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public double getMp() {
		return this.mp;
	}

	public void setMp(int mp) {
		this.mp = mp;
	}

	public double getPp() {
		return this.pp;
	}

	public void setPp(int pp) {
		this.pp = pp;
	}

	public void setImage(String imagename) {
		this.image = new ImageIcon(imagename);
	}

	public ImageIcon getImage() {
		return this.image;
	}
}
