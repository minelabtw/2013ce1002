package ce1002.a7.s102502534;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame()
	{
		setLayout(null);
		setSize(460, 780);//設定frame大小
		setLocationRelativeTo(null);//跳出視窗時置中
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;//關閉視窗
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setRoleState(hero);
		panel.setBounds(x,y,430,220);
		add(panel);
	}
}
