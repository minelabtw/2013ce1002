package ce1002.a7.s102502534;

public class Knight extends Hero{
	Knight() {
		HeroName ="Knight";
	}
	// ��gHeroName.HP.MP.PP
	public String getHeroName() {
		return super.getHeroName();
	}

	public double getHP() { 
		return super.getHP() * 0.8;
	}

	public double getMP() {
		return super.getMP() * 0.1;
	}

	public double getPP() {
		return super.getPP() * 0.1;
	}
}