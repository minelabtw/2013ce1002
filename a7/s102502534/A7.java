package ce1002.a7.s102502534;

public class A7 {

	public static void main(String[] args) {
		Hero [] hero = new Hero [3];
		//第0個位置存wizard的HP.MP.PP
		hero [0]=new Wizard();
		//第1個位置存swordsman的HP.MP.PP
		hero [1]=new Swordsman();
		//第2個位置存knight的HP.MP.PP
		hero[2]=new Knight();
		
		MyFrame myFrame = new MyFrame();
		
		MyPanel[] myPanel = new MyPanel[3];
				
		for(int i=0; i<3; i++){
			myPanel[i] = new MyPanel() ;
			myFrame.newRolePos(hero[i], myPanel[i], 10, 10+240*i);
		}
		myFrame.setVisible(true);//讓視窗跳出
	}

}
