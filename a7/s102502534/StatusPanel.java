package ce1002.a7.s102502534;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel {

	StatusPanel() {
		setSize(100,150);
		setLayout(new GridLayout(3, 2, 0, 0));//3*2表格
	}

	public void setState(Hero hero) {
		//依序由左到右上到下加到panel中
		add(new JLabel("HP"));
		add(new JLabel("" + hero.getHP()));
		add(new JLabel("MP"));
		add(new JLabel("" + hero.getMP()));
		add(new JLabel("PP"));
		add(new JLabel("" + hero.getPP()));
	}
}
