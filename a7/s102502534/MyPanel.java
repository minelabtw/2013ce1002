package ce1002.a7.s102502534;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class MyPanel extends JPanel {

	// 宣告2個 JLabel 使用，一個用來顯示角色的各項屬性狀態
	JLabel state = new JLabel();
	JLabel image = new JLabel();
	StatusPanel statuspanel = new StatusPanel();

	MyPanel() {
		setBorder(new LineBorder(Color.black, 3));
	}

	public void setRoleState(Hero hero) {
		setLayout(new BorderLayout(10, 0));

		state = new JLabel(hero.getHeroName() + " HP: " + hero.getHP()+ " MP: " + hero.getMP() + " PP: " + hero.getPP());
		ImageIcon image2 = new ImageIcon(hero.getHeroName() + ".jpg");
		image = new JLabel(image2);
		
		// 記得要setState!!!
		statuspanel.setState(hero);
		
		// 加到版面中順便設定位置
		add(state, BorderLayout.NORTH);
		add(image, BorderLayout.WEST);
		add(statuspanel, BorderLayout.CENTER);
	}
}
