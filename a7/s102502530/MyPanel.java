package ce1002.a7.s102502530 ;
import javax.swing.JPanel ;
import javax.swing.JLabel ;
import javax.swing.ImageIcon ;
import java.awt.BorderLayout ;
import java.awt.GridLayout ;
import java.awt.Dimension ;

public class MyPanel extends JPanel
{
   public void setRoleState(Hero hero)
   {
      JPanel jpanel = new JPanel() ;
      jpanel.setLayout(new BorderLayout()) ;
      jpanel.setPreferredSize(new Dimension(230, 200)) ;
      jpanel.add(new JLabel(hero.getName() + " HP: " + hero.getHp() + " MP: " + hero.getMp() + " PP: " + hero.getPp()), BorderLayout.NORTH) ;
      jpanel.add(new JLabel(new ImageIcon(hero.getName() + ".jpg")), BorderLayout.SOUTH) ;
      this.add(jpanel, BorderLayout.WEST) ;
      StatusPanel statusPanel = new StatusPanel() ;
      statusPanel.setPreferredSize(new Dimension(140,200)) ;
      statusPanel.setLayout(new GridLayout(3, 2)) ;
      statusPanel.setState(hero) ;
      this.add(statusPanel, BorderLayout.EAST) ;
   }
}
