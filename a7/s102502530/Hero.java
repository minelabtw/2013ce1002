package ce1002.a7.s102502530 ;

public class Hero
{
   private String name ;   //declare
   private double hp ;
   private double mp ;
   private double pp ;

   public Hero()  //constructor
   {
      hp = 30 ;
      mp = 30 ;
      pp = 30 ;
   }

   public String getName()    //get name
   {
      return name ;
   }

   public double getHp()   //get hp
   {
      return hp ;
   }

   public double getMp()   //get mp
   {
      return mp ;
   }

   public double getPp()   //get pp
   {
      return pp ;
   }

   public void setName(String name)    //set name
   {
      this.name = name ;
   }

   public void setHp(double hp)  //set hp
   {
      this.hp = hp ;
   }

   public void setMp(double mp)  //set mp
   {
      this.mp = mp ;
   }

   public void setPp(double pp)  //set pp
   {
      this.pp = pp ;
   }
}
