package ce1002.a7.s102502530 ;

public class Knight extends Hero
{
   public Knight()   //constructor
   {
      super.setName("Knight") ;
   }

   public double getHp()   //get hp
   {
      return super.getHp() * 0.8 ;
   }

   public double getMp()   //get mp
   {
      return super.getMp() * 0.1 ;
   }

   public double getPp()   //get pp
   {
      return super.getPp() * 0.1 ;
   }
}
