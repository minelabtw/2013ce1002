package ce1002.a7.s102502530 ;

public class Swordsman extends Hero
{
   public Swordsman()   //constructor
   {
      super.setName("Swordsman") ;
   }

   public double getHp()   //get hp
   {
      return super.getHp() * 0.1 ;
   }

   public double getMp()   //get mp
   {
      return super.getMp() * 0.1 ;
   }

   public double getPp()   //get pp
   {
      return super.getPp() * 0.8 ;
   }
}

