package ce1002.a7.s102502530 ;
import javax.swing.JPanel ;
import javax.swing.JLabel ;

public class StatusPanel extends JPanel
{
   public void setState(Hero hero)
   {
      add(new JLabel("HP")) ;
      add(new JLabel(hero.getHp() + "")) ;
      add(new JLabel("MP")) ;
      add(new JLabel(hero.getMp() + "")) ;
      add(new JLabel("PP")) ;
      add(new JLabel(hero.getPp() + "")) ;
   }
}
