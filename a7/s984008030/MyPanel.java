package ce1002.a7.s984008030;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	private JLabel status;//狀態
	private JLabel picture;//圖片
	private StatusPanel statusPanel;//右邊狀態
	
	public MyPanel() {
		//初始化成員變數
		status = new JLabel("", 2);
		picture = new JLabel();
		statusPanel = new StatusPanel();
		//設定MyPanel的狀態
		setLayout(new BorderLayout());
		status.setBounds(0, 0, 450, 20);
		picture.setBounds(0, 20, 250, 190);
		statusPanel.setBounds(250, 20, 200, 190);
		//將三個component加入
		add(status, BorderLayout.NORTH);
		add(picture,BorderLayout.WEST);
		add(statusPanel,BorderLayout.CENTER);
	}
	
	public void setRoleState(Hero hero) {
		//設定圖片
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");
		picture.setIcon(image);
		//設定文字
		status.setText(hero.getName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP());
		statusPanel.setState(hero);
	}
	
}
