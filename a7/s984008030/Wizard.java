package ce1002.a7.s984008030;

public class Wizard extends Hero {
	
	public Wizard(){
		super();
		super.setName("Wizard");
	}
	
	public float getHP() {//取得生命點數(HP)
		return super.getHP() * (float)0.2;
	}
	
	public float getMP() {//取得魔法點數(MP)
		return super.getMP() * (float)0.7;
	}
	
	public float getPP() {//取得能力點數(PP)
		return super.getPP() * (float)0.1;
	}
	
}
