package ce1002.a7.s984008030;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	private MyPanel wizardPanel;
	private MyPanel swordsmanPanel;
	private MyPanel knightPanel;
	private Hero[] heros;
	
	public MyFrame(Hero[] _heros) {
		wizardPanel = new MyPanel();
		swordsmanPanel = new MyPanel();
		knightPanel = new MyPanel();
		heros = _heros;
		
		setLayout(null);
		setBounds(300 , 50 , 460 , 640);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(heros[0], wizardPanel, 5, 5);
		newRolePos(heros[1], swordsmanPanel, 5, 210);
		newRolePos(heros[2], knightPanel, 5, 415);
		
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y) {
		/*set panel's position and size*/
		panel.setBounds(x, y, 450 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
	
}
