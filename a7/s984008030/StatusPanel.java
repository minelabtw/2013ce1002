package ce1002.a7.s984008030;
import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	
	private JLabel[][] labels;//英雄狀態文字陣列
	
	public StatusPanel() {
		//設定排版為 3*2 GridLayout
		setLayout(new GridLayout(3, 2));
		//初始化成員變數
		this.labels = new JLabel[3][2];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 2; j++) {
				this.labels[i][j] = new JLabel();
				add(this.labels[i][j]);
			}
		}
	}
	
	public void setState(Hero hero) {
		this.labels[0][0].setText("HP");
		this.labels[0][1].setText(String.valueOf(hero.getHP()));
		this.labels[1][0].setText("MP");
		this.labels[1][1].setText(String.valueOf(hero.getMP()));
		this.labels[2][0].setText("PP");
		this.labels[2][1].setText(String.valueOf(hero.getPP()));
	}
	
}
