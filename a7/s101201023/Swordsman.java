package ce1002.a7.s101201023;

import javax.swing.ImageIcon;

public class Swordsman extends Hero
{
	public double getHP()
	{
		return super.getHP() * 0.1;
	}
	
	public double getMP()
	{
		return super.getMP() * 0.1;
	}
	
	public double getPP()
	{
		return super.getPP() * 0.8;
	}
	
	
}
