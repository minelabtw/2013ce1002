package ce1002.a7.s101201023;

import javax.swing.*;
import javax.swing.ImageIcon;

public class MyPanel extends JPanel
{
	private JLabel rolekind=new JLabel();
	private JLabel rolepict=new JLabel();
	private StatusPanel panels=new StatusPanel();
	public MyPanel()
	{
		//set labels & set StatusPanel
		setLayout(null);
		rolekind.setBounds(5,0,220,20);
		rolepict.setBounds(5,15,240,180);
		panels.setBounds(250,15,150,180);
		add(rolekind);
		add(rolepict);
		add(panels);
	}
	
	public void setRoleState(Hero hero)
	{
		//input message
		rolekind.setText(hero.getHeroName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP());
		rolepict.setIcon(hero.getImage());
		panels.setState(hero);
	}
}