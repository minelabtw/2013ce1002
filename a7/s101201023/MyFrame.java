package ce1002.a7.s101201023;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;

public class MyFrame extends JFrame
{
	private MyPanel wizard=new MyPanel();                      
	private MyPanel swordsman=new MyPanel();
	private MyPanel knight=new MyPanel();
	
	public MyFrame(Hero[] heros)
	{
		//set frame
		setLayout(null);
		setBounds(300 , 50 , 480 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//output
		newRolePos(heros[0] , wizard , 10 , 10);
		newRolePos(heros[1] , swordsman , 10 , 220);
		newRolePos(heros[2] , knight , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		//set panel
		panel.setBounds(x, y, 420 , 200);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		add(panel);
	}
}
