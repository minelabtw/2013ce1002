package ce1002.a7.s101201023;

import javax.swing.*;

import java.awt.GridLayout;

public class StatusPanel extends JPanel
{	
	public StatusPanel()
	{
		setLayout(new GridLayout(3 , 2 , 10 , 10));
	}
	
	public void setState(Hero hero)
	{
		//set panel
		add(new JLabel("HP"));
		add(new JLabel(Double.toString(hero.getHP())));
		add(new JLabel("MP"));
		add(new JLabel(Double.toString(hero.getMP())));
		add(new JLabel("PP"));
		add(new JLabel(Double.toString(hero.getPP())));
	}
}