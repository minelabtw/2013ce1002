package ce1002.a7.s101201023;

public class Wizard extends Hero
{
	public double getHP()
	{
		return super.getHP() * 0.2;
	}
	
	public double getMP()
	{
		return super.getMP() * 0.7;
	}
	
	public double getPP()
	{
		return super.getPP() * 0.1;
	}
}
