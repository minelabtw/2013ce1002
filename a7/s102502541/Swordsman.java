package ce1002.a7.s102502541;
import javax.swing.*;
public class Swordsman extends Hero{
	public Swordsman(){
		//super(0.1,0.1,0.8);
		setheroid("Swordsman");
		image = new ImageIcon("Swordsman.jpg");
	}
	public double getHP()
	{
		return HP*0.1;
	}
	public double getMP(){
		return MP*0.1;
	}
	public double getPP(){
		return PP*0.8;
	}

}
