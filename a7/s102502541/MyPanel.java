package ce1002.a7.s102502541;
import javax.swing.*;

import java.awt.*;

import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	protected JLabel state = new JLabel();
	protected JLabel icon = new JLabel();
	protected StatusPanel StatusPanel = new StatusPanel();
	public MyPanel()
	{
		setBorder(new LineBorder(Color.black,3));//邊線
		setLayout(new BorderLayout(5,10));
		add(state,BorderLayout.NORTH);
		add(icon,BorderLayout.WEST);
		add(StatusPanel,BorderLayout.CENTER);
	}
	public void setRoleState(Hero hero)
	{
		state.setText(hero.getheroid()+" "+"HP:"+hero.getHP()+" "+"MP:"+hero.getMP()+" "+"PP:"+hero.getPP());
		icon.setIcon(hero.image);
		StatusPanel.setState(hero);
		
	}//在label貼上 state and icon  and StatusPanel
}
