package ce1002.a7.s102502541;
import javax.swing.*;
import java.awt.GridLayout;
public class StatusPanel extends JPanel{
	protected JLabel s0 = new JLabel();
	protected JLabel s1 = new JLabel();
	protected JLabel s2 = new JLabel();
	protected JLabel s3 = new JLabel();
	protected JLabel s4 = new JLabel();
	protected JLabel s5 = new JLabel();
	public StatusPanel()
	{
		setLayout(new GridLayout(3,2,0,0));//棋盤排列
		add(s0);
		add(s1);
		add(s2);
		add(s3);
		add(s4);
		add(s5);
	}
	 public void setState(Hero hero) 
	 {
		 s0.setText("HP");
		 s1.setText("" +hero.getHP());
		 s2.setText("MP");
		 s3.setText("" +hero.getMP());
		 s4.setText("PP");
		 s5.setText("" +hero.getPP());
	 }//將state輸入label
}
