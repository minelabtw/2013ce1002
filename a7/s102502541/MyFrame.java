package ce1002.a7.s102502541;
import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame{
	
	MyPanel p0 = new MyPanel();
	MyPanel p1 = new MyPanel();
	MyPanel p2 = new MyPanel();
	public MyFrame(Hero hero[])
	{
		newRolePos(hero[0] ,p0 , 10 , 10);
		newRolePos(hero[1] ,p1 , 10 , 240);
		newRolePos(hero[2] ,p2 , 10 , 480);
		setSize(480,750);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setRoleState(hero);//設定panel
		panel.setBounds(x,y,450,220);
		add(panel);
		 
	}

}
