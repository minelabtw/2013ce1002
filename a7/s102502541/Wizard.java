package ce1002.a7.s102502541;
import javax.swing.*;
public class Wizard extends Hero{
	public Wizard(){
		//super(0.2,0.7,0.1);
		setheroid("Wizard");
		image = new ImageIcon("Wizard.jpg");
	}
	public double getHP()
	{
		return HP*0.2;
	}
	public double getMP(){
		return MP*0.7;
	}
	public double getPP(){
		return PP*0.1;
	}
	
}
