package ce1002.a7.ta;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel statsPanel = new StatusPanel();

	MyPanel() {
		/* set panel's size and layout */
		setLayout(null);
		label.setBounds(5, 0, 220, 20);
		imagelabel.setBounds(5, 15, 240, 180);
		statsPanel.setBounds(255, 5, 140, 190);
		/* add label to panel */
		add(label);
		add(imagelabel);
		add(statsPanel);
	}

	public void setRoleState(Hero hero) {
		/* set label's text and image */
		imagelabel.setIcon(hero.getImage());
		statsPanel.setState(hero);// set state table
		label.setText(hero.getName() + " HP: " + hero.getHp() + " MP: "
				+ hero.getMp() + " PP:" + hero.getPp());
	}
}
