package ce1002.a7.ta;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	private JLabel hpLabel = new JLabel("undefine");// hp value
	private JLabel mpLabel = new JLabel("undefine");// mp value
	private JLabel ppLabel = new JLabel("undefine");// pp value

	public StatusPanel() {
		// table with 3 rows, 2 columns
		GridLayout gridLayout = new GridLayout(3, 2);
		setLayout(gridLayout);

		// 1st row
		this.add(new JLabel("HP"));
		this.add(hpLabel);
		// 2nd row
		this.add(new JLabel("MP"));
		this.add(mpLabel);
		// 3rd row
		this.add(new JLabel("PP"));
		this.add(ppLabel);
	}

	public void setState(Hero hero) {// set each value
		this.hpLabel.setText("" + hero.getHp());
		this.mpLabel.setText("" + hero.getMp());
		this.ppLabel.setText("" + hero.getPp());
	}
}
