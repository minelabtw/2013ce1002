package ce1002.a7.s102502006;

public class Swordsman extends Hero{
	public Swordsman(){
		super.setName("Swordsman");
	}
	
	public String getName()
	{
	  return super.getName();
	}
	public float getHp()
	{
	  return super.getHp()*0.1f;
	}
	public float getMp()
	{
	  return super.getMp()*0.1f;
	}
	public float getPp()
	{
	  return super.getPp()*0.8f;
	}
}
