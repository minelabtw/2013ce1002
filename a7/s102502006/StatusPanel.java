package ce1002.a7.s102502006;

import java.awt.GridLayout;
import javax.swing.JLabel;

import javax.swing.JPanel;

public class StatusPanel extends JPanel{

	public StatusPanel(){
		setSize(50,100);
		setLayout(new GridLayout(3, 1, 0, 40)); // �O�l��{	
	}
	
	public void setState(Hero hero) {
		add(new JLabel("HP        " + hero.getHp()));
		add(new JLabel("MP        " + hero.getMp()));
		add(new JLabel("PP        " + hero.getPp()));
	}

}
