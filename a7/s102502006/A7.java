package ce1002.a7.s102502006;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		MyFrame myframe = new MyFrame();
		MyPanel[] panel = new MyPanel[3];

		for(int i=0; i<3; i++)
		{
			panel[i] = new MyPanel();
			myframe.newRolePos(hero[i], panel[i], 10, 10+205*i);
		}
		myframe.setVisible(true);
	}
}
