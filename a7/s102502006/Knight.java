package ce1002.a7.s102502006;

public class Knight extends Hero{
	public Knight(){
		super.setName("Knight");
	}
	
	public String getName()
	{
	  return super.getName();
	}
	public float getHp()
	{
	  return super.getHp()*0.8f;
	}
	public float getMp()
	{
	  return super.getMp()*0.1f;
	}
	public float getPp()
	{
	  return super.getPp()*0.1f;
	}
}
