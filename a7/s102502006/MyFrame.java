package ce1002.a7.s102502006;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	public MyFrame(){
		setBounds(300, 10, 400, 670); // 視窗大小
		setLayout(null); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 關閉板子
	}
	public void newRolePos(Hero hero, MyPanel panel, int x, int y)
	{
		panel.setBounds(x, y, 360, 200); // 畫布位置
	    panel.setRoleState(hero); // 畫布樣式
		this.add(panel); // 加上畫布
	}
}
