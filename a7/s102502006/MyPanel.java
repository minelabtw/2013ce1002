package ce1002.a7.s102502006;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder; // 邊框
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class MyPanel extends JPanel{
	private JLabel infor; // 資料
	private JLabel pic; // 圖片標籤
	private ImageIcon icon; // 圖片
	private StatusPanel spanel; // 子畫布
	
	public MyPanel(){
		setLayout(null);
		setBorder(new LineBorder(Color.black, 4)); // 畫布邊框
	}
	public void setRoleState(Hero hero)
	{
		infor = new JLabel(hero.getName() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp()); // 放入資訊
		icon = new ImageIcon(hero.getName() + ".jpg");
		pic = new JLabel(icon);
		spanel = new StatusPanel();
		spanel.setState(hero);
		
		infor.setBounds(5, 5, 200, 10);
		pic.setBounds(5,15,240,180);
		spanel.setBounds(250, 15, 100, 150);
		
		this.add(infor);
		this.add(pic);
		this.add(spanel);
	}
}
