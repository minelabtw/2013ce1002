package ce1002.a7.s102502006;

public class Hero {
	protected String name;
	protected float hp;
	protected float mp;
	protected float pp;
	
	Hero(){
		hp = mp = pp =30.0f;
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return name;
	}
	
	public void setHp(float hp){
		this.hp = hp;
	}
	public float getHp(){
		return hp;
	}
	public void setMp(float mp){
		this.mp = mp;
	}
	public float getMp(){
		return mp;
	}
	public void setPp(float pp){
		this.pp = pp;
	}
	public float getPp(){
		return pp;
	}
}
