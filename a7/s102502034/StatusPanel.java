package ce1002.a7.s102502034;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	public void setState(Hero hero) {

		setLayout(new GridLayout(3, 2));//做3*2的格子
		JLabel Label1 = new JLabel(" HP");
		JLabel Label2 = new JLabel(" MP");
		JLabel Label3 = new JLabel(" PP");
		JLabel Label4 = new JLabel(" " + hero.getHp());
		JLabel Label5 = new JLabel(" " + hero.getMp());
		JLabel Label6 = new JLabel(" " + hero.getPp());
		//設定JLabel
		add(Label1);
		add(Label4);
		add(Label2);
		add(Label5);
		add(Label3);
		add(Label6);
		//加入panel
		setVisible(true);


	}

}
