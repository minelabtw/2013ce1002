package ce1002.a7.s102502034;

import ce1002.a7.s102502034.Hero;

public class Swordsman extends Hero {
	public double getMp() {

		return Mp * 0.1;
	}

	public double getHp() {
		return Hp * 0.1;
	}

	public double getPp() {
		return Pp * 0.8;
	}

	public String getHeroName() {
		return this.HeroName="Swordsman";
	}
	// 回傳乘上劍士比例後的數值

}