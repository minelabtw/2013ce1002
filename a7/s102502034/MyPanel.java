package ce1002.a7.s102502034;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MyPanel extends JPanel {
	private String temp;

	MyPanel(String hero) {

		temp = hero;
		// set image full name " .... .jpg"
	}

	public void setRoleState(Hero hero) {
		setLayout(new BorderLayout());
		StatusPanel sta = new StatusPanel();

		ImageIcon image = new ImageIcon(temp);
		JLabel lb1;
		lb1 = new JLabel(image);
		// set lb1 = image
		JLabel lb2;
		lb2 = new JLabel(hero.getHeroName() + " HP: " + hero.getHp() + " MP: "
				+ hero.getMp() + " PP: " + hero.getPp());
		// set lb2 = status
		sta.setState(hero);
		add(lb2,BorderLayout.NORTH);
		add(lb1,BorderLayout.WEST);
		
		add(sta,BorderLayout.CENTER);

		// add label to panel
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
		// set border
		this.setVisible(true);
		// let panel visible

	}

}
