package ce1002.a7.s102502034;

import ce1002.a7.s102502034.Hero;

public class Wizard extends Hero {
	public double getMp() {

		return Mp * 0.7;
	}

	public double getHp() {
		return Hp * 0.2;
	}

	public double getPp() {
		return Pp * 0.1;
	}

	public String getHeroName() {
		return this.HeroName = "Wizard";
	}
	// 回傳乘上巫師比例後的數值

}