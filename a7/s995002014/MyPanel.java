package ce1002.a7.s995002014;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel  {
	private JLabel herotext;
	MyPanel(){
		setBorder(BorderFactory.createLineBorder(Color.black)); //邊界黑線
	}
	
	//設定hero文字圖片
	public void setRoleState(Hero hero){
		JPanel temp = new JPanel();
		temp.setLayout(new BoxLayout(temp, BoxLayout.Y_AXIS));
		herotext=new JLabel(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP: "+hero.getPp());
		temp.add(herotext);
		JLabel picLabel = new JLabel(new ImageIcon(hero.getName()+".jpg"));
		temp.add(picLabel);
		add(temp);
		StatusPanel state = new StatusPanel(hero); //加入statusPanel
		add(state);
	}
}
