package ce1002.a7.s995002014;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	StatusPanel(Hero hero) {
		setLayout(new GridLayout(3, 2)); //�ϥ�gridLayout
		setState(hero);
	}
	public void setState(Hero hero) {
		JLabel l1 = new JLabel("HP");
		JLabel l2 = new JLabel(Float.toString(hero.getHp()));
		JLabel l3 = new JLabel("MP");
		JLabel l4 = new JLabel(Float.toString(hero.getMp()));
		JLabel l5 = new JLabel("PP");
		JLabel l6 = new JLabel(Float.toString(hero.getPp()));
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
		add(l6);
	}
}
