package ce1002.a7.s995002014;

public class Knight extends Hero {
	
	Knight() {
		setName("Knight");
	}
	
	//Override
	public float getHp() {
		return (float) (this.hp*0.8);
	}
	public float getMp() {
		return (float) (mp*0.1);
	}
	public float getPp() {
		return (float) (pp*0.1);
	}
	
}
