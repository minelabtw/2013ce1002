package ce1002.a7.s995002014;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;

public class A7 {

	public static void main(String[] args) {
		//hero�}�C
				Hero[] herolist = new Hero[3] ;
				
				//�U�۪�l��
				herolist[0]=new Wizard();
				herolist[1]=new Swordsman();
				herolist[2]=new Knight();
				
				//new JFrame
				MyFrame frame = new MyFrame();
				
				frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
				/*frame.add(new MyPanel());
				MyPanel sec=new MyPanel();
				MyPanel fir=new MyPanel();
				frame.add(fir);
				frame.add(sec,BorderLayout.PAGE_END);*/
				
				//create three panel for each hero
				MyPanel fir=new MyPanel();
				MyPanel sec=new MyPanel();
				MyPanel thi=new MyPanel();
				
				//set panel by method in Myframe
				frame.newRolePos(herolist[0], fir, 4, 9);
				frame.newRolePos(herolist[1], sec, 8, 9);
				frame.newRolePos(herolist[2], thi, 10, 9);
				frame.add(fir);
				frame.add(Box.createRigidArea(new Dimension(0,10)));
				frame.add(sec);
				frame.add(Box.createRigidArea(new Dimension(0,10)));
				frame.add(thi);
				
				frame.pack();
				frame.setVisible(true);
	}

}
