package ce1002.a7.s102502050;

import javax.swing.*;
import java.awt.Color;

public class MyFrame extends JFrame{
	protected MyPanel panel;
	Hero []hero;
	public MyFrame(Hero [] hero , int heronumber)
	{
		this.hero=hero;
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(300,20,700,heronumber*250+50);
		
		for(int i=0;i<heronumber;i++)
		{
			panel = new MyPanel(this.hero[i]);
			newRolePos( hero[i] , panel , 10 , i*250);
		}
		setVisible(true);
		//setVisible要放在最後面
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 650 , 260);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		add(panel);
	}

}
