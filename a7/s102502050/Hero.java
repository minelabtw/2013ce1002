package ce1002.a7.s102502050;

public class Hero {
	private String name; 
	private double HP=30;
	private double MP=30;
	private double PP=30;
	private String imagename;

	public Hero()
	{	

	}
	// setter of name,HP,MP,PP
	public void Setname(String name)
	{
		this.name=name;
	}
	public void Setimagename(String imagename)
	{
		this.imagename=imagename;
	}
	public void SetHP(int HP)
	{
		this.HP=HP;
	}
	public void SetMP(int MP)
	{
		this.MP=MP;
	}
	public void SetPP(int PP)
	{
		this.PP=PP;
	}
	//getter of name,HP,MP,PP
	public String Getname()
	{
		return name;
	}
	public String Getimagename()
	{
		return imagename;
	}
	public double GetHP()
	{
		return HP;
	}
	public double GetMP()
	{
		return MP;
	}
	public double GetPP()
	{
		return PP;
	}
}


