package ce1002.a7.s102502050;
import javax.swing.*;
public class MyPanel extends JPanel{
	protected JLabel label;
	protected JLabel imagelabel;
	protected ImageIcon imageicon;
	protected StatusPanel statuspanel;
	Hero panelhero;
	public MyPanel(Hero hero)
	{
		setLayout(null);
		setRoleState( hero);
		
		//設定圖片
		imageicon = new ImageIcon(hero.Getimagename());
		imagelabel = new JLabel();
		imagelabel.setIcon(imageicon);

		//設定文字
		label = new JLabel(hero.Getname()+" HP: "+hero.GetHP()+" MP: "+hero.GetMP()+" PP:"+hero.GetPP());
		
		statuspanel= new StatusPanel(panelhero);
		statuspanel.setBounds(300,10,50,100);
		
		label.setBounds(5,10,250,20);
		imagelabel.setBounds(10,20,240,250);
		add(label);
		add(imagelabel);
		add(statuspanel);
		
	}
	public void setRoleState(Hero hero)
	{
		this.panelhero=hero;
	}

}
