package ce1002.a7.s102502010;

import java.awt.FlowLayout;
import java.awt.GridLayout;

public class A7 {

	public static void main(String[] args) {
        Hero[] heros = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		for(Hero hero : heros){
			System.out.println( hero.getname() + " HP:"+ hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP() );
		}
		MyFrame frame = new MyFrame(heros);
	}

}
