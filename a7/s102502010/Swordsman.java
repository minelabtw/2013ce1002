package ce1002.a7.s102502010;

public class Swordsman extends Hero{
	Swordsman()  //constructor swordsman
	{
		super.setname("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	public double getHP()
	{
		return super.getHP()*0.1;
	}
	public double getMP()
	{
		return super.getMP()*0.1;
	}
	public double getPP()
	{
		return super.getPP()*0.8;
	}
}
