package ce1002.a7.s102502010;

import javax.swing.ImageIcon;

public class Hero {
	private double HP;
	private double MP;
	private double PP;
	private String name;
	private ImageIcon image;
	Hero()  //constructor hero
	{
		setHP(30);
		setMP(30);
		setPP(30);
		name="";
	}
	public void setHP(double HP)
	{
		this.HP=HP;
	}
	public void setMP(double MP)
	{
		this.MP=MP;
	}
	public void setPP(double PP)
	{
		this.PP=PP;
	}
	public void setname(String name)
	{
		this.name=name;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
	public double getHP()
	{
		return HP;
	}
	public double getMP()
	{
		return MP;
	}
	public double getPP()
	{
		return PP;
	}
	public String getname()
	{
		return name;
	}
}
