package ce1002.a7.s102502010;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;

public class StatusPanel extends JPanel{
	
	protected JLabel[] label = new JLabel[6];
	
	StatusPanel()
	{
		setBounds(250,15,100,160);  //setBounds
		setLayout(new GridLayout(3, 2));  //setLayout
	}
	
	public void setState(Hero hero) //setState
	{
		for(int i=0; i<6; i++)
		{
			label[i] = new JLabel();
		}
		label[0].setText("HP: ");  //setText
		label[1].setText(hero.getHP() + "");
		label[2].setText("MP: ");
		label[3].setText(hero.getMP() + "");
		label[4].setText("PP:");
		label[5].setText(hero.getPP() + "");
		for(int i=0; i<6; i++)
		{
			add(label[i]);
		}
	}
}
