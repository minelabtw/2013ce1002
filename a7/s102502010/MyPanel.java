package ce1002.a7.s102502010;

import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel Statuspanel = new StatusPanel();
	
	MyPanel()
	{
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		add(label);  //add
		add(imagelabel);
		add(Statuspanel);
	}
	
	public void setRoleState(Hero hero)  //setRoleState
	{
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getname()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
		Statuspanel.setState(hero);
	}
}

