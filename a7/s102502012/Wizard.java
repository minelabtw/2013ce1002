package ce1002.a7.s102502012;

public class Wizard extends Hero{
	static final private double hpFix = 0.2;
	static final private double mpFix = 0.7;
	static final private double ppFix = 0.1;
	
	Wizard(){
		setName("Wizard");
	}
	
	// override methods in Hero.
	public double getHp(){
		return super.getHp() * hpFix;
	}
	
	public double getMp(){
		return super.getMp() * mpFix;
	}
	
	public double getPp(){
		return super.getPp() * ppFix;
	}
}
