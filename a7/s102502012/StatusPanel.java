package ce1002.a7.s102502012;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ce1002.a7.s102502012.Hero;

public class StatusPanel extends JPanel{
	private JLabel[] status;
	
	public StatusPanel(){
		setLayout(new GridLayout(3, 2, 20, 20));
		status = new JLabel[6];
		for(int i = 0; i < 6; i++){
			status[i] = new JLabel();
			add(status[i]);
		}
	}
	
	public void setState(Hero hero){
		status[0].setText("HP");
		status[1].setText("" + hero.getHp());
		status[2].setText("MP");
		status[3].setText("" + hero.getMp());
		status[4].setText("PP");
		status[5].setText("" + hero.getPp());
	}
}
