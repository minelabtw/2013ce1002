package ce1002.a7.s102502012;

public class Swordsman extends Hero{
	static final private double hpFix = 0.1;
	static final private double mpFix = 0.1;
	static final private double ppFix = 0.8;
	
	Swordsman(){
		setName("Swordsman");
	}
	
	// override methods in Hero.
	public double getHp(){
		return super.getHp() * hpFix;
	}
	
	public double getMp(){
		return super.getMp() * mpFix;
	}
	
	public double getPp(){
		return super.getPp() * ppFix;
	}
}
