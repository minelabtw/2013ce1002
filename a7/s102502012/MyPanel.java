package ce1002.a7.s102502012;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private JLabel status;
	private JLabel image;
	private StatusPanel statuspanel;
	
	public void setRoleState(Hero hero){
		status = new JLabel(hero.getName() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		image = new JLabel(new ImageIcon(hero.getName() + ".jpg"));
		statuspanel = new StatusPanel();
		statuspanel.setState(hero);
		
		// configure display effect
		status.setSize(220, 20);
		image.setSize(250,190);
		statuspanel.setSize(100, 180);
		status.setLocation(5, 0);
		image.setLocation(5, 20);
		statuspanel.setLocation(300, 20);
	}
	
	public JLabel getStatus(){
		return status;
	}
	
	public JLabel getImage(){
		return image;
	}
	
	public StatusPanel getStatusPanel(){
		return statuspanel;
	}
}
