package ce1002.a7.s102502012;

import java.awt.FlowLayout;
import java.awt.GridLayout;

public class A7 {

	public static void main(String[] args) {
		MyFrame myFrame = new MyFrame();
		myFrame.setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		myFrame.setLocationRelativeTo(null);
		myFrame.setTitle("s102502012");
		myFrame.setSize(475, 700);
		myFrame.setResizable(false);
	}

}
