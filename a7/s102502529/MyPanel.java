package ce1002.a7.s102502529;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel
{
	JLabel info;
	JLabel img;
	ImageIcon icon;
	JLabel Status ;
	MyPanel()
	{
		this.setSize(400, 230);
		this.setBorder(new LineBorder(Color.black, 5));
	}
	
	public void setRoleState(Hero hero)						//導入狀態
	{
		icon = new ImageIcon(hero.getName() + ".jpg");
		info = new JLabel(hero.getName() + " HP:" + hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		img = new JLabel(icon);
		this.add(info);
		this.add(img);		
		StatusPanel StatusRight = new StatusPanel(hero);  						//放入新寫的Status
		this.add(StatusRight);
	}
}