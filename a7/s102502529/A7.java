package ce1002.a7.s102502529;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyFrame frame = new MyFrame();
		MyPanel[] panel = new MyPanel[3];								//宣告
		Hero[] hero = new Hero[3];										//物件化  多形
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		frame.setSize(450,770);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for (int i=0;i<3;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i], panel[i], 10, 10 + 240 * i);
		}

		frame.setVisible(true);
	}
}

