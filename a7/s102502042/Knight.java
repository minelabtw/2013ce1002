package ce1002.a7.s102502042;

public class Knight extends Hero{
	//constructor
	Knight()
	{
		super();
		setName("Knight");
	}
	//override getter
	float getHP()
	{
		return this.HP * (float)0.8;
	}
	float getMP()
	{
		return this.MP * (float)0.1;
	}
	float getPP()
	{
		return this.PP * (float)0.1;
	}
}