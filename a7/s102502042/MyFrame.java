package ce1002.a7.s102502042;

import javax.swing.*;

public class MyFrame extends JFrame {
	//三個panel
	MyPanel p1 = new MyPanel();
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();

	MyFrame()
	{
		/*三個英雄*/
		Hero knight = new Knight();
		Hero swordsman = new Swordsman();
		Hero wizard = new Wizard();
		//設定大小、英雄、位置
		this.newRolePos(wizard,this.p3,450,200);
		this.p3.setLocation(10,10);
		this.newRolePos(swordsman, this.p2, 450, 200);
		this.p2.setLocation(10, 220);
		this.newRolePos(knight,this.p1,450,200);
		this.p1.setLocation(10,430);
		StatusPanel status = new StatusPanel();
	}
	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setSize(x,y);	//設定大小
		panel.setRoleState(hero);
		this.add(panel);
	}
}
