package ce1002.a7.s102502042;
import javax.swing.*;
import java.awt.*;
public class StatusPanel extends JLabel{
	//英雄屬性表格
	JLabel label1=new JLabel();
	JLabel label2=new JLabel();
	JLabel label3=new JLabel();
	JLabel label4=new JLabel();
	JLabel label5=new JLabel();
	JLabel label6=new JLabel();
	StatusPanel()
	{
		//設定排版
		setLayout(new GridLayout(3,2,10,10));
	}
	public void setState(Hero hero)
	{
		//設定屬性
		label1.setText("HP");
		label2.setText(""+(hero.getHP()));
		label3.setText("MP");
		label4.setText(""+(hero.getMP()));
		label5.setText("PP");
		label6.setText(""+(hero.getPP()));
		this.add(label1);
		this.add(label2);
		this.add(label3);
		this.add(label4);
		this.add(label5);
		this.add(label6);
	}
}
