package ce1002.a7.s102502042;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	StatusPanel status = new StatusPanel();
	public void setRoleState(Hero hero) {
		setLayout(null);
		this.setBorder(BorderFactory.createLineBorder(Color.black, 3));	//線邊框
		label1.setText(hero.getName()+" HP:"+hero.getHP()+" MP:"+hero.getMP()+" PP:"+hero.getPP());
		label2.setIcon(new ImageIcon(hero.getPath()));
		//放進panel
		label1.setBounds(10,10,260,10);
		this.add(label1);
		label2.setBounds(10, 20, 260, 160);
		this.add(label2);
		//設定STATUS PANEL
		status.setBounds(280, 30, 150, 150);
		status.setState(hero);
		this.add(status);
	}
}
