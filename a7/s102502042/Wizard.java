package ce1002.a7.s102502042;

public class Wizard extends Hero{
	//constructor
	Wizard()
	{
		super();
		setName("Wizard");
	}
	//override getter
	float getHP()
	{
		return this.HP * (float)0.2;
	}
	float getMP()
	{
		return this.MP * (float)0.7;
	}
	float getPP()
	{
		return this.PP * (float)0.1;
	}
}
