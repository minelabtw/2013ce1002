package ce1002.a7.s102502035;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();// use constructor
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame(hero);// MyFrame class frame object
		frame.setSize(447, 720);// frame size
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);// to see
	}

}
