package ce1002.a7.s102502035;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	JLabel label6 = new JLabel();

	public StatusPanel() {// constructor
		setLayout(new GridLayout(3, 2));
		setSize(50, 180);
	}

	public void setState(Hero hero) {// setText
		label1.setText("HP");
		label2.setText("" + hero.getHP());
		label3.setText("MP");
		label4.setText("" + hero.getMP());
		label5.setText("PP");
		label6.setText("" + hero.getPP());
		finaladd();// add Label
	}

	public void finaladd() {// final add Label
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
	}
}
