package ce1002.a7.s102502035;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	String labelcontent;
	String picture;
	ImageIcon image;
	StatusPanel statusPanel = new StatusPanel();

	public MyPanel() {// construct
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}

	public void setRoleState(Hero hero) {
		labelcontent = hero.getname() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP: " + hero.getPP();// save content
		picture = hero.getname() + ".jpg";
		image = new ImageIcon(picture);
		statusPanel.setState(hero);// pass hero object to StatusPanel

	}

	public void finaladd() {// final add Panel
		add(new JLabel(labelcontent), BorderLayout.NORTH);
		add(new JLabel(image), BorderLayout.WEST);
		add(statusPanel, BorderLayout.CENTER);
	}

}