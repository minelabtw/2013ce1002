package ce1002.a7.s102502035;

public class Wizard extends Hero {
	String name;
	double HPrate = 0.2;
	double MPrate = 0.7;
	double PPrate = 0.1;

	public Wizard() {
		// TODO Auto-generated constructor stub
		name = "Wizard";

	}

	public String getname() {
		return name;
	}

	public double getHP() {// return and calculate
		return HP * HPrate;
	}

	public double getMP() {// return and calculate
		return MP * MPrate;
	}

	public double getPP() {// return and calculate
		return PP * PPrate;
	}
}
