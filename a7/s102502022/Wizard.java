package ce1002.a7.s102502022;

public class Wizard extends Hero{

	public Wizard()
	{
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	
	public double getHp()
	{
		return super.hp*0.2;
	}
	public double getMp()
	{
		return super.mp*0.7;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}

}
