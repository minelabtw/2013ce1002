package ce1002.a7.s102502022;

import javax.swing.*;

public class Hero {
	public String name;
	public double hp;
	public double mp;
	public double pp;
	public ImageIcon image;
	
	public Hero(){
		setHp(30);
		setMp(30);
		setPp(30);
	}
	
	public void setName(String n)
	{
		this.name=n;
	}
	public String getName()
	{
		return name;
	}
	public double getHp()
	{
		return hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
	public void setImage(String imagename)//傳入圖片
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()//回傳圖片
	{
		return image;
	}

}