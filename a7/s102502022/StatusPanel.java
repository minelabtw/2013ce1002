package ce1002.a7.s102502022;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel{
	StatusPanel(){
		setLayout(new GridLayout(3,2,20,20));
		setVisible(true);
		setSize(100,180);
		setLocation(245,10);
	}
	
	public void setState(Hero hero){
		JLabel[] grid = new JLabel[6];//創造陣列作為格子
		
	    grid[0] = new JLabel("HP");//陣列第幾個位置放什麼東西
	    grid[1] = new JLabel(""+hero.getHp());
	    grid[2] = new JLabel("MP");
	    grid[3] = new JLabel(""+hero.getMp());
	    grid[4] = new JLabel("PP");
	    grid[5] = new JLabel(""+hero.getPp());
	    for(int i =0;i<6;i++)//迴圈依序填入我要填的東西
		{
			add(grid[i]);
		}
	}
	

	
}
