package ce1002.a7.s102502022;

public class Knight extends Hero{

	public Knight()
	{	
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	public double getHp()
	{
		return super.hp*0.8;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}

}
