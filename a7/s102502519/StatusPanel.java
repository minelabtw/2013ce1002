package ce1002.a7.s102502519;

import javax.swing.*;
import java.awt.*;

public class StatusPanel  extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel[] state = new JLabel[6];
	
	StatusPanel(){
		setLayout(new GridLayout(3,2));    //設定3,2的位置
		setSize(60,50);
		
	}
	
	public void setState(Hero hero) {    //將值存入state陣列中
		state[0] = new JLabel("HP:");
		state[1] = new JLabel(" " + hero.getHp());
		state[2] = new JLabel("MP:");
		state[3] = new JLabel(" " + hero.getMp());
		state[4] = new JLabel("PP:");
		state[5] = new JLabel(" " + hero.getPp());
		for(int i=0;i<6;i++){
			add(state[i]);
		}
		
	}
}
