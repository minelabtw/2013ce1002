package ce1002.a7.s102502519;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel state = new StatusPanel();
	
	MyPanel()
	{
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		state.setBounds(255,10,150,180);
		add(label);
		add(imagelabel);
		add(state);
	}
	
	public void setRoleState(Hero hero)
	{
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		state.setState(hero);
	}
}
