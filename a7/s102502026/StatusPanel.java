package ce1002.a7.s102502026;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	
	public StatusPanel(){
		setLayout(new GridLayout(3,2)); //layout of 3 rows 2 columns
		
	}
	public void setState(Hero hero) {
		//putting 6 labels and output it
		JLabel a= new JLabel("    HP");	
		JLabel b= new JLabel("");
		JLabel c= new JLabel("    MP");
		JLabel d= new JLabel("");
		JLabel e= new JLabel("    PP");
		JLabel f= new JLabel("");
		b.setText(""+hero.getHp());
		d.setText(""+hero.getMp());
		f.setText(""+hero.getPp());
		add(a);
		add(b);
		add(c);
		add(d);
		add(e);
		add(f);
	}
}
