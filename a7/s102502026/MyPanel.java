package ce1002.a7.s102502026;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	StatusPanel Sp;
	public MyPanel(){	//panel
		setSize(400, 220);
		setLayout(new BorderLayout());
		setBorder(new LineBorder(Color.black, 3));
		Sp= new StatusPanel();
	}
	public void setRoleState(Hero hero){
		String str= "src/ce1002/a7/s102502026/" + hero.getHero() +".jpg" ;	//location of the picture
		ImageIcon icon= new ImageIcon(str);
		JLabel state= new JLabel(hero.getHero() + " HP: " + hero.getHp()
				+ " MP: " + hero.getMp() + " PP: " + hero.getPp());
		Sp.setState(hero);
		add(state, BorderLayout.NORTH);	//output state
		add(new JLabel(icon), BorderLayout.WEST);	//output picture
		add(Sp, BorderLayout.CENTER);
	}
}
