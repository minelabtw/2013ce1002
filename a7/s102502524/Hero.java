package ce1002.a7.s102502524;

import javax.swing.ImageIcon;

public class Hero {
	
	Hero(){}					//setting basic abilities to all heroes
	
	protected String HeroName;
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	
	public void setName()
	{
		setHP();
		setMP();
		setPP();
	}
	
	public void setHP()
	{
		this.HP = 30.0;
	}
	
	public void setMP()
	{
		this.MP = 30.0;
	}
	
	public void setPP()
	{
		this.PP = 30.0;
	}
	
	public String getName()
	{
		return this.HeroName;
	}
	
	public double getHP()
	{
		return this.HP;
	}
	
	public double getMP()
	{
		return this.MP;
	}
	
	public double getPP()
	{
		return this.PP;
	}

	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}
