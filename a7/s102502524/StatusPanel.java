package ce1002.a7.s102502524;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel{
	
	JLabel[] item;
	JLabel[] value;
	
	StatusPanel()
	{
		setSize(200,200);
		setLocation(280,10);
		setLayout(new GridLayout(3,2,30,30));
	}
	
	public void setState(Hero hero)
	{
		item = new JLabel[3];
		item[0]  = new JLabel("HP");
		item[1]  = new JLabel("MP");
		item[2]  = new JLabel("PP");
		value    = new JLabel[3];
		value[0] = new JLabel("" + hero.getHP());
		value[1] = new JLabel("" + hero.getMP());
		value[2] = new JLabel("" + hero.getPP());
		for(int i=0 ; i<3 ; i++)
		{
			add(item[i]);
			add(value[i]);
		}
	}

}
