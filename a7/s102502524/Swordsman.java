package ce1002.a7.s102502524;

public class Swordsman extends Hero {
	
	public Swordsman()
	{
		super.setImage("Swordsman.jpg");
	}
	
	public void setName()			//setting abilities to swordsman
	{
		this.HeroName = "Swordsman";
	}
	
	public double getHP()
	{
		return this.HP*0.1;
	}

	public double getMP()
	{
		return this.MP*0.1;
	}
	
	public double getPP()
	{
		return this.PP*0.8;
	}

}
