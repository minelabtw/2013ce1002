package ce1002.a7.s102502524;

public class Knight extends Hero {
	
	public Knight()
	{
		super.setImage("Knight.jpg");
	}
	
	public void setName()			//setting abilities to knight
	{
		this.HeroName = "Knight";
	}
	
	public double getHP()
	{
		return this.HP*0.8;
	}

	public double getMP()
	{
		return this.MP*0.1;
	}
	
	public double getPP()
	{
		return this.PP*0.1;
	}

}
