package ce1002.a7.s102502545;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame {

	MyPanel A = new MyPanel();
	MyPanel B = new MyPanel();
	MyPanel C = new MyPanel();

	public MyFrame() {// 建構子
		Hero knight = new Knight();
		Hero swordsman = new Swordsman();
		Hero wizard = new Wizard();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 關閉視窗後結束程式
		setLayout(null);

		newRolePos(wizard,A, 450, 200);
		A.setLocation(15, 15);
		newRolePos(swordsman, B, 450, 200);
		B.setLocation(15, 225);
		newRolePos(knight,C, 450, 200);
		C.setLocation(15, 435);

		setSize(500, 690);// 利用frame設定外框大小
		setVisible(true);

	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {

		panel.setRoleState(hero);// 設定英雄
		panel.setSize(x, y);// 設定大小
		this.add(panel);// 增加圖片

	}
}
