package ce1002.a7.s102502540;

public class Swordsman extends Hero {
	Swordsman() {

	}

	String getName() {
		return "Swordsman";
	}

	double getHp() {
		return hp * 0.1;
	}

	double getMp() {
		return mp * 0.1;
	}

	double getPp() {
		return pp * 0.8;
	}

}