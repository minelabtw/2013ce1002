package ce1002.a7.s102502540;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	JLabel ch = new JLabel();
	JLabel pic = new JLabel();
	StatusPanel SP = new StatusPanel();

	MyPanel() {
		this.setSize(350, 198);
		this.setLayout(new BorderLayout());
	}

	public void setRoleState(Hero hero) { //設定圖片與角色狀態
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");
		ch.setText(hero.getName() + " HP: " + hero.getHp() + " MP: "
				+ hero.getMp() + " PP: " + hero.getPp());
		pic.setIcon(image);
		SP.setState(hero);
		this.add(ch, BorderLayout.NORTH);
		this.add(pic, BorderLayout.WEST);
		this.add(SP, BorderLayout.CENTER);
		this.setVisible(true);
	}
}