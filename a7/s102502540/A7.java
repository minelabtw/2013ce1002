package ce1002.a7.s102502540;

import java.awt.*;
import java.awt.event.*;

public class A7 {
	public static void main(String[] args) {
		Hero[] hero = new Hero[3]; // 創建Hero物件陣列並取名hero
		hero[0] = new Wizard(); // 將Wizard class儲存在hero陣列的第0個位置
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame(hero);
	}

}

