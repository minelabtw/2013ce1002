package ce1002.a7.s102502540;

public class Knight extends Hero {
	Knight() {

	}

	String getName() {
		return "Knight";
	}

	double getHp() {
		return hp * 0.8;
	}

	double getMp() {
		return mp * 0.1;
	}

	double getPp() {
		return pp * 0.1;
	}
}
