package ce1002.a7.s102502540;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel {

	StatusPanel() {
		this.setLayout(new GridLayout(3, 2)); // 設定表格
	}

	public void setState(Hero hero) {
		JLabel HP = new JLabel(" HP ");
		JLabel hp = new JLabel("" + hero.getHp());
		JLabel MP = new JLabel(" MP ");
		JLabel mp = new JLabel("" + hero.getMp());
		JLabel PP = new JLabel(" PP ");
		JLabel pp = new JLabel("" + hero.getPp());
		this.add(HP);
		this.add(hp);
		this.add(MP);
		this.add(mp);
		this.add(PP);
		this.add(pp);
		this.setVisible(true); // 顯示表格
	}
}