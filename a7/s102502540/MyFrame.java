package ce1002.a7.s102502540;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame {
	protected MyPanel p1 = new MyPanel();
	protected MyPanel p2 = new MyPanel();
	protected MyPanel p3 = new MyPanel();
	protected Hero[] heros;

	MyFrame(Hero[] heros) {
		setSize(500, 720);
		setLayout(null); // 設定排版方向
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		newRolePos(heros[0], p1, 10, 10);
		newRolePos(heros[1], p2, 10, 235);
		newRolePos(heros[2], p3, 10, 460);
	}

	// 此函式用傳進來的x和y值設定panel位置與大小
	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setLocation(x,y);
		panel.setRoleState(hero);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		panel.setVisible(true);
		this.add(panel);
		this.setVisible(true);
	}
}

