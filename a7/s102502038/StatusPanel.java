package ce1002.a7.s102502038;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	private JLabel[][] labelArray = new JLabel[3][2];
	StatusPanel(){
	    setLayout(new GridLayout(3, 2));//set grid layout
	    setBounds(250,3,247,194);
	    for(int i = 0;i<3;i++){
	    	for(int j = 0;j<2;j++){//label setting loop
		    	labelArray[i][j] = new JLabel();
		    	labelArray[i][j].setBounds(5,5,20,20);
		    	add(labelArray[i][j]);
	    	}
	    }
	}
	public void setState(Hero hero){
		labelArray[0][0].setText("HP");
		labelArray[1][0].setText("MP");
		labelArray[2][0].setText("PP");
		labelArray[0][1].setText(String.valueOf(hero.getHp()));
		labelArray[1][1].setText(String.valueOf(hero.getMp()));
		labelArray[2][1].setText(String.valueOf(hero.getPp()));
	}
}
