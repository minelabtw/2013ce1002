package ce1002.a7.s102502040;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();//做框框
		MyPanel[] panel = new MyPanel[3];
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();//顯示順序
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		frame.setSize(300,770);
		frame.setLocationRelativeTo(null);//最中間
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//按叉叉,就可以關掉他
		for (int i=0 ; i<3 ; i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i], panel[i], 10, 10 + 240 * i);
		}
		frame.setVisible(true);//我看得到程式
	}

}