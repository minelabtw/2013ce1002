package ce1002.a7.s102502024;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	 
	MyPanel[] MyPanel = new MyPanel[3];
	public MyFrame(Hero[] hero)
	 {
		setSize(800,1200);  //設定視窗大小
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //設定關閉鍵
		setLayout(null);  //不用預設排版
		for(int i=0;i<3;i++)  //放上3個panel
		{
			MyPanel[i]=new MyPanel();
			newRolePos(hero[i], MyPanel[i], 10, 10 + 250 * i);
		}
		setVisible(true);  //使視窗看的見
	 }
	 public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	 {
		 panel.setLocation(x,y);  
		 panel.setRoleState(hero);
		 add(panel);
	 }
}