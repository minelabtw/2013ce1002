package ce1002.a7.s102502024;
import java.awt.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JLabel title;
	JLabel image;
	ImageIcon con;
	StatusPanel st=new StatusPanel();
	public MyPanel()
	{
		setSize(600,200);  //設定panel大小
		setLayout(null);  //不用預設排版
		setBorder(new LineBorder(Color.black, 2));  //設定邊框
	}
	public void setRoleState(Hero hero)
	{
		con=new ImageIcon(hero.getname()+".jpg");  //載入圖片
		image=new JLabel(con);  //存放圖片
		title=new JLabel(hero.getname()+" HP:"+hero.gethp()+" MP:"+hero.getmp()+" PP:"+hero.getpp());  //腳色的title
		title.setSize(210,20);  //title的位置與大小
		title.setLocation(30,10);
		image.setSize(200,150);  //圖片的位置與大小
		image.setLocation(0,30);
		st.setState(hero);  //設定數據的label
		st.setSize(240,150);  //數據的位置與大小
		st.setLocation(300,10);
		add(title);
		add(image);
		add(st);
	}
}