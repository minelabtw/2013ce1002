package ce1002.a7.s102502024;
import javax.swing.JLabel;
import java.awt.GridLayout;
public class StatusPanel extends JLabel{
	protected JLabel hp=new JLabel();  //宣告各項資料的label
	protected JLabel mp=new JLabel();
	protected JLabel pp=new JLabel();
	protected JLabel hps;
	protected JLabel mps;
	protected JLabel pps;
	
	public StatusPanel()
	{
		setLayout(new GridLayout(3,2));  //棋盤式排列
		setSize(100,100);  //label的大小
	}
	public void setState(Hero hero)
	{
		hp.setText("HP");
		mp.setText("MP");
		pp.setText("PP");
		hps=new JLabel(hero.gethp()+"");
		mps=new JLabel(hero.getmp()+"");
		pps=new JLabel(hero.getpp()+"");
		add(hp);
		add(hps);
		add(mp);
		add(mps);
		add(pp);
		add(pps);
	}
}
