package ce1002.a7.s102502009;

import java.awt.*;
import javax.swing.*;

class StatusPanel extends JPanel {

	StatusPanel() {
		setSize(200, 200);
		setLayout(new GridLayout(3, 1, 50, 50));
	}

	public void setState(Hero hero) {
		add(new JLabel("Hp               " + hero.getHp()));
		add(new JLabel("Mp               " + hero.getMp()));
		add(new JLabel("Pp               " + hero.getPp()));
	}
}