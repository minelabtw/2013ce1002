package ce1002.a7.s102502009;

public class Swordsman extends Hero {

	public Swordsman() {
		// setup the title for this character's name
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}

	/*
	 * This is the key part of polymorphism. We overrided the each point's
	 * getter and multiply it with weight from the table.
	 */
	public double getHp() {
		return super.hp * 0.1;
	}

	public double getMp() {
		return super.mp * 0.1;
	}

	public double getPp() {
		return super.pp * 0.8;
	}
}