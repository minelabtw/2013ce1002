package ce1002.a7.s101201003;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	StatusPanel(){
		setLayout(new GridLayout(3,2));
		setVisible(true);
	}

	public void setState(Hero hero){
		add(new JLabel("HP"));
		add(new JLabel(Double.toString(hero.getHp())));
		add(new JLabel("MP"));
		add(new JLabel(Double.toString(hero.getMp())));
		add(new JLabel("PP"));
		add(new JLabel(Double.toString(hero.getPp())));				
	}
}
