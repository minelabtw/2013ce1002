package ce1002.a7.s100502022;

import java.awt.Color;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel statuspanel = new  StatusPanel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,237,180);
		statuspanel.setBounds(245,5,100,190);
		//statuspanel.setBorder(new LineBorder(Color.black,2));
		
		/*add label to panel*/
		add(label);
		add(imagelabel);
		
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
	}
	public void setStatus(Hero hero){
		//write value and add status panel on my panel
		statuspanel.setState(hero);
		add(statuspanel);
	}
}
