package ce1002.a7.s102502011;

import javax.swing.ImageIcon;

/*
 * This is the father class of the all different heros.
 * It contains 3 main ability points, which are health point,magic point,power point
 * and a title (name) for the hero. 
 * */
public class Hero{
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;
	
	public Hero()
	{
		// we initialized these 3 variables value in hero's constructor
		// because it's called before it's child's constructor
		// so that we don't need to call it again in every children's constructor
		setHp(30);
		setMp(30);
		setPp(30);
	}
	
	/*
	 * Getter and setter for every variables
	 * */
	public void setName(String n)
	{
		this.name=n;
	}
	public String getName()
	{
		return this.name;
	}
	public double getHp()
	{
		return this.hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return this.pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}