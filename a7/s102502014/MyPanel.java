package ce1002.a7.s102502014;

import java.awt.*;
import javax.swing.*;
import ce1002.a7.s102502014.StatusPanel;

public class MyPanel extends JPanel {
	public JLabel L1;
	public JLabel L2;
	
	public StatusPanel PANEL =new StatusPanel();

	public MyPanel() {
		this.setLayout(null);        //非預設排版
		this.setBounds(0,0,550,200); //邊界
		this.setSize(500,200);       //大小
	}
	public void setRoleState(Hero hero) {
		ImageIcon icon = new ImageIcon(hero.getNAME() + ".jpg"); //圖片物件
		L1 = new JLabel(icon); //LABEL 1 讀圖片 設置大小 位置
		L1.setSize(250, 190);
		L1.setLocation(0, 10);
		this.add(L1); //加到Panel�堶�
		L2 = new JLabel(); //LABEL 2 放文字 設置大小位置
		L2.setText(hero.getNAME() + " HP:" + hero.getHP() + " MP:"
		+ hero.getMP() + " PP:" + hero.getPP());
		L2.setSize(250, 10);
		L2.setLocation(0, 0);
		this.add(L2); //加到Panel�堶�
		PANEL.setLocation(275, 0);
		PANEL.setState(hero);
		this.add(PANEL);
	}
}
