package ce1002.a7.s102502014;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;

public class StatusPanel extends JPanel {

	public JLabel l1 = new JLabel();
	public JLabel l2 = new JLabel();
	public JLabel l3 = new JLabel();
	public JLabel l4 = new JLabel();
	public JLabel l5 = new JLabel();
	public JLabel l6 = new JLabel();

	public StatusPanel() {
		this.setSize(250, 200); // 大小
		this.setLayout(new GridLayout(3, 2, 5, 5)); // 非預設排版
	}

	public void setState(Hero hero) {
		l1.setText(" HP");
		l2.setText(hero.getHP() + "");
		l3.setText(" MP");
		l4.setText(hero.getMP() + "");
		l5.setText(" PP");
		l6.setText(hero.getPP() + "");
		this.add(l1);
		this.add(l2);
		this.add(l3);
		this.add(l4);
		this.add(l5);
		this.add(l6);
	}
}
