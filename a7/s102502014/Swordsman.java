package ce1002.a7.s102502014;

import ce1002.a7.s102502014.Hero;

public class Swordsman extends Hero {
	public Swordsman() {
		// TODO Auto-generated constructor stub
	}

	public double getHP() {
		return HP * 0.1;
	}

	public double getMP() {
		return MP * 0.1;
	}

	public double getPP() {
		return PP * 0.8;
	}
}
