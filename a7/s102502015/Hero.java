package ce1002.a7.s102502015;

public class Hero {
	public String name;
	public float hp;
	public float mp;
	public float pp;

	Hero() {
	}

	public void setname(String _name) {
		name = _name;
	}

	public void sethp(float _hp) {
		hp = _hp;
	}

	public void setmp(float _mp) {
		mp = _mp;
	}

	public void setpp(float _pp) {
		pp = _pp;
	}
	public String getname(){
		return name;
	}
	public float gethp() {
		return hp;
	}

	public float getmp() {
		return mp;
	}

	public float getpp() {
		return pp;
	}

}
