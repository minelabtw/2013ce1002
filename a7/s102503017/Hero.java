package ce1002.a7.s102503017;

import javax.swing.ImageIcon;

public class Hero 
{
	protected double hp, mp, pp;
	protected String name;
	protected ImageIcon image;
	//Default constructor for initialize values. hp,mp,pp for 30, name,imageName for Null. 
	Hero()
	{
		setHp(30);
		setMp(30);
		setPp(30);
	}
	//setter and getter for each value.
	void setHp(double hp)
	{
		this.hp = hp;
	}
	
	void setMp(double mp)
	{
		this.mp = mp;
	}
	
	void setPp(double pp)
	{
		this.pp = pp;
	}
	
	void setName(String name)
	{
		this.name = name;
	}
	
	void setImage(String imageName)
	{
		this.image = new ImageIcon(imageName);
	}
	
	double getHp()
	{
		return this.hp;
	}
	
	double getMp()
	{
		return this.mp;
	}
	
	double getPp()
	{
		return this.pp;
	}
	
	String getName()
	{
		return this.name;
	}
	
	ImageIcon getImage()
	{
		return this.image;
	}
	
	
	
	
}
