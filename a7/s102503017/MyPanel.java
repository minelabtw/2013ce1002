package ce1002.a7.s102503017;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JLabel
{

	protected JLabel label = new JLabel();
	protected JLabel imageLabel = new JLabel();
	protected FlowLayout flow = new FlowLayout();
	protected StatusPanel status = new StatusPanel();
	
	//setting the parameters of MyPanel, including StatusPanel object.
	MyPanel()
	{
		setLayout(null);
		label.setBounds(5, 0, 220, 17);
		imageLabel.setBounds(5, 17, 240, 180);
		status.setBounds(260, 17, 170, 180);
		add(label);
		add(imageLabel);
		add(status);
	}
	
	
	//method for setting the parameters from the class Hero.
	public void setRoleState(Hero heros)
	{
		label.setText(heros.getName() + ": hp: " + heros.getHp() + " mp: " + heros.getMp() + " pp: " + heros.getPp());
		imageLabel.setIcon(heros.getImage());
		status.setState(heros);
		//method of the StatusPanel.
	}
	
	
}
