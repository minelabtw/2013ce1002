package ce1002.a7.s102503017;

public class Knight extends Hero
{
	//Default constructor for Knight, specifying the name and the image of the Knight.
	Knight()
	{
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	//getter which override its father class, which means polymorphism, assigning different
	//values to the values which it inherit from the father class.
	
	double getHp()
	{
		return super.hp * 0.8;
	}
	
	double getMp()
	{
		return super.mp * 0.1;
	}
	
	double getPp()
	{
		return super.pp * 0.1;
	}
	
}
