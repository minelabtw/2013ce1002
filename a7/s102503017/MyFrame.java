package ce1002.a7.s102503017;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame
{
	
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;
	
	//setting the parameters of the frame.
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		setLayout(null);
		setBounds(300, 50, 500, 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		newRolePos(heros[0], panel1, 10, 10 );
		newRolePos(heros[1], panel2, 10, 220 );
		newRolePos(heros[2], panel3, 10, 430 );
		
		
	}	

	//method for outputting the result.
	public void newRolePos(Hero heros, MyPanel panel, int x, int y)
	{
		panel.setBounds(x, y, 450, 200);
		panel.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panel.setRoleState(heros);
		add(panel);
		
	}
}