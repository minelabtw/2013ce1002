package ce1002.a7.s102503017;

public class Wizard extends Hero
{
	//same as class Knight.
	Wizard()
	{
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	
	double getHp()
	{
		return super.hp * 0.2;
	}
	
	double getMp()
	{
		return super.mp * 0.7;
	}
	
	double getPp()
	{
		return super.pp * 0.1;
	}
	
}
