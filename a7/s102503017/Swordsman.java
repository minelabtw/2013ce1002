package ce1002.a7.s102503017;

public class Swordsman extends Hero
{
	//same as class Knight.
	Swordsman()
	{
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	
	double getHp()
	{
		return super.hp * 0.1;
	}
	
	double getMp()
	{
		return super.mp * 0.1;
	}
	
	double getPp()
	{
		return super.pp * 0.8;
	}
	
	
}
