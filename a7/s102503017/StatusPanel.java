package ce1002.a7.s102503017;

import javax.swing.*;
import java.awt.*;


public class StatusPanel extends JPanel
{
	//incarnate an object grid with GridLayout class, which is use for layout the panel.
	protected GridLayout grid = new GridLayout(3,0);
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel();
	protected JLabel label6 = new JLabel();
	
	
	StatusPanel()
	{
		//using GridLayout for the StatusPanel.
		setLayout(grid);
		setBounds(0, 0, 240, 180);
		//setBounds for the labels1~6 and add them into the panel.
		label1.setBounds(0, 0, 120, 60);
		label2.setBounds(0, 0, 120, 60);
		label3.setBounds(0, 0, 120, 60);
		label4.setBounds(0, 0, 120, 60);
		label5.setBounds(0, 0, 120, 60);
		label6.setBounds(0, 0, 120, 60);
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
	}

	public void setState(Hero heros)
	{
		//method for setting the details of the label.
		label1.setText("Hp: ");
		label2.setText("" + heros.getHp());
		label3.setText("Mp: ");
		label4.setText("" + heros.getMp());
		label5.setText("Pp: ");
		label6.setText("" + heros.getPp());

		
	}
	
	
	
}

