package ce1002.a7.s102502032;

import javax.swing.ImageIcon;

public class Hero
{
	private double		HP;
	private double		MP;
	private double		PP;
	private String		name;
	private ImageIcon	icon;

	// construstor
	public Hero()
	{
	}

	// set
	public void setHP(double valueHP)
	{
		HP = valueHP;
	}

	public void setMP(double valueMP)
	{
		MP = valueMP;
	}

	public void setPP(double valuePP)
	{
		PP = valuePP;
	}

	public void setIcon(String path)
	{
		icon = new ImageIcon(path);
	}

	public void setName(String str)
	{
		name = str;
	}

	// get
	public String getName()
	{
		return name;
	}

	public double getHP()
	{
		return HP;
	}

	public double getMP()
	{
		return MP;
	}

	public double getPP()
	{
		return PP;
	}

	public ImageIcon getIcon()
	{
		return icon;
	}
}
