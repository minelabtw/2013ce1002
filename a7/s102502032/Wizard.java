package ce1002.a7.s102502032;

import javax.swing.ImageIcon;

public class Wizard extends Hero
{
	private ImageIcon	icon	= new ImageIcon("Wizard.jpg");

	// construstor
	public Wizard()
	{
		setHP(30);
		setMP(30);
		setPP(30);
		super.setIcon("Wizard.jpg");
		super.setName("Wizard");
	}

	// get
	public double getHP()
	{
		return super.getHP() * 0.2;
	}

	public double getMP()
	{
		return super.getMP() * 0.7;
	}

	public double getPP()
	{
		return super.getPP() * 0.1;
	}

	public ImageIcon getIcon()
	{
		return icon;
	}
}
