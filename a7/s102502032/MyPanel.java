package ce1002.a7.s102502032;

import java.awt.*;

import javax.swing.*;

public class MyPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private JLabel				properity			= new JLabel();
	private JLabel				img					= new JLabel();
	private StatusPanel			status				= new StatusPanel();

	// constructor
	public MyPanel()
	{
		this.setVisible(true);
		this.add(properity);
		this.add(img);
		this.add(status);
		this.setLayout(null);
		this.properity.setBounds(5, 3, 250, 20);
		this.img.setBounds(3, 22, 250, 190);
	}

	// setter
	// getter
	/**
	 * TEST REPOTER
	 * 
	 * <pre>
	 * 	private int			testCC;
	 * 	private static int	testCount	= 0;
	 * 
	 * 	public void getReport()
	 * 	{
	 * 		if (this.testCount <= 2)
	 * 		{
	 * 			this.testCC = this.testCount;
	 * 		}
	 * 		System.out.println("testCC = " + this.testCC + " count = " + this.testCount);
	 * 		System.out.println(this.getVisibleRect());
	 * 		System.out.println(this.getPreferredSize());
	 * 		this.testCount ++;
	 * }
	 * 
	 * <pre>
	 */
	// methods
	// show the propities, icon, and the border
	public void setRoleState(Hero hero)
	{
		properity.setText(hero.getName() + " HP:" + hero.getHP() + " MP:"
				+ hero.getMP() + " PP:" + hero.getPP());
		img.setIcon(hero.getIcon());
		status.setState(hero);
		properity.setVisible(true);
		img.setVisible(true);
		status.setVisible(true);
		this.setBorder(BorderFactory.createLineBorder(Color.black, 2, true));
	}
}
