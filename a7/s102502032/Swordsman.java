package ce1002.a7.s102502032;

import javax.swing.ImageIcon;

public class Swordsman extends Hero
{
	private ImageIcon	icon	= new ImageIcon("Swordsman.jpg");

	// construstor
	public Swordsman()
	{
		setHP(30);
		setMP(30);
		setPP(30);
		super.setIcon("Swordsman.jpg");
		super.setName("Swordsman");
	}

	// get
	public double getHP()
	{
		return super.getHP() * 0.1;
	}

	public double getMP()
	{
		return super.getMP() * 0.1;
	}

	public double getPP()
	{
		return super.getPP() * 0.8;
	}

	public ImageIcon getIcon()
	{
		return icon;
	}
}
