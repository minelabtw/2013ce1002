package ce1002.a7.s102502032;

//import javax.swing.ImageIcon;

public class Knight extends Hero
{
	// private ImageIcon icon = new ImageIcon("Knight.jpg");
	// construstor
	public Knight()
	{
		setHP(30);
		setMP(30);
		setPP(30);
		super.setIcon("Knight.jpg");
		super.setName("Knight");
	}

	// get
	public double getHP()
	{
		return super.getHP() * 0.8;
	}

	public double getMP()
	{
		return super.getMP() * 0.1;
	}

	public double getPP()
	{
		return super.getPP() * 0.1;
	}
}
