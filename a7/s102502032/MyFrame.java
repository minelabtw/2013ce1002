package ce1002.a7.s102502032;

import javax.swing.*;

public class MyFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private MyPanel				panelList[]			= new MyPanel[3];
	private Hero				heroList[]			= new Hero[3];

	// constructor
	public MyFrame()
	{
		for (int i = 0; i < 3; i ++)
		{
			panelList[i] = new MyPanel();
			heroList[i] = new Hero();
		}
		this.setLayout(null);
		this.setTitle(null);
		this.setSize(490, 730);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	// setter
	public void setHero(Hero[] hero)
	{
		heroList = hero;
	}

	public void setPanel()
	{
		for (int i = 0; i < 3; i ++)
		{
			panelList[i].setRoleState(heroList[i]);
			panelList[i].setBounds(10, 10 + 230 * i, 456, 215);
			this.add(panelList[i]);
		}
	}
	// getter
	/**
	 * TEST REPOTER
	 * 
	 * <pre>
	 * 	private int			testCC;
	 * 	private static int	testCount	= 0;
	 * 
	 * 	public void getReport()
	 * 	{
	 * 		if (this.testCount <= 2)
	 * 		{
	 * 			this.testCC = this.testCount;
	 * 		}
	 * 		System.out.println("testCC = " + this.testCC + " count = " + this.testCount);
	 * 		System.out.println(this.getVisibleRect());
	 * 		System.out.println(this.getPreferredSize());
	 * 		this.testCount ++;
	 * }
	 * 
	 * <pre>
	 */
	// methods
}
