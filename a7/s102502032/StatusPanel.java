package ce1002.a7.s102502032;

import java.awt.*;

import javax.swing.*;

public class StatusPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private JLabel				properities[]		= new JLabel[6];

	// constructor
	public StatusPanel()
	{
		this.setLocation(256, 3);
		this.setLayout(new GridLayout(3, 2));
		this.setSize(200, 210);
		this.setVisible(true);
		for (int i = 0; i < 6; i ++)
		{
			properities[i] = new JLabel();
			properities[i].setVisible(true);
			this.add(properities[i]);
		}
	}

	// setter
	public void setState(Hero hero)
	{
		properities[0].setText("HP");
		properities[1].setText("" + hero.getHP());
		properities[2].setText("MP");
		properities[3].setText("" + hero.getMP());
		properities[4].setText("PP");
		properities[5].setText("" + hero.getPP());
	}
	// getter
	/**
	 * TEST REPOTER
	 * 
	 * <pre>
	 * 	private int			testCC;
	 * 	private static int	testCount	= 0;
	 * 
	 * 	public void getReport()
	 * 	{
	 * 		if (this.testCount <= 2)
	 * 		{
	 * 			this.testCC = this.testCount;
	 * 		}
	 * 		System.out.println("testCC = " + this.testCC + " count = " + this.testCount);
	 * 		System.out.println(this.getVisibleRect());
	 * 		System.out.println(this.getPreferredSize());
	 * 		this.testCount ++;
	 * }
	 * 
	 * <pre>
	 */
	// methods
}
