package ce1002.a7.s102502542;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel
{
	StatusPanel SPanel = new StatusPanel();
	MyPanel () 
	{
	    this.setSize(350,199);
		this.setLayout(new BorderLayout());//設定排版
	}
	public void setRoleState(Hero hero)//設定圖片與角色狀態
	{
		this.setBorder(new LineBorder(Color.black,3));	//設定邊界
		ImageIcon image = new ImageIcon(hero.getname()+".jpg");
		//設定標籤
		JLabel jlbl = new JLabel(hero.getname() + " HP: " + hero.getHP()
				 + " MP: " + hero.getMP() + " PP: " + hero.getPP());
		JLabel jlbl1 = new JLabel(image);
		SPanel.setState(hero);
		this.add(jlbl,BorderLayout.NORTH);
		this.add(jlbl1,BorderLayout.WEST);
		this.add(SPanel,BorderLayout.CENTER);
	}

}