package ce1002.a7.s102502542;

public class Wizard extends Hero {
	public String getname() {
		return "Wizard";
	}

	public double getHP()// 回傳加權值
	{
		return HP * 0.2;
	}

	public double getMP() {
		return MP * 0.7;
	}

	public double getPP() {
		return PP * 0.1;
	}
}
