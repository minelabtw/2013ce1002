package ce1002.a7.s102502542;

import java.util.Scanner;

import javax.swing.*;

public class A7 {

	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		Hero[] hero = new Hero[3];// 創建Hero物件陣列並取名hero
		hero[0] = new Wizard();// 將Wizard class儲存在hero陣列的第0個位置
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame myframe = new MyFrame();
		MyPanel mypanel = new MyPanel();
		MyPanel mypanel1 = new MyPanel();
		MyPanel mypanel2 = new MyPanel();
		myframe.newRolePos(hero[0], mypanel, 10, 10);
		myframe.newRolePos(hero[1], mypanel1, 10, 220);
		myframe.newRolePos(hero[2], mypanel2, 10, 430);
    }

}
