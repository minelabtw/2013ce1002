package ce1002.a7.s102502542;

import javax.swing.JFrame;

public class MyFrame extends JFrame 
{
	MyFrame()
	{
		setSize(447,686);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);//設定排版方向
	}
	//此函式用傳進來的x和y值設定panel位置與大小
	public void newRolePos(Hero hero,MyPanel panel,int x,int y)
	{
		panel.setLocation(x,y);
		panel.setRoleState(hero);
		this.add(panel);
		setVisible(true);
	}
}

