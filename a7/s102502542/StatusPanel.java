package ce1002.a7.s102502542;

import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;

public class StatusPanel extends JPanel
{
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));//設定表格
	}
	public void setState(Hero hero) //設定角色的各項屬性
	{
		JLabel label = new JLabel(" HP ");
		JLabel label1 = new JLabel("" + hero.getHP());
		JLabel label2 = new JLabel(" MP ");
		JLabel label3 = new JLabel("" + hero.getMP());
		JLabel label4 = new JLabel(" PP ");
		JLabel label5 = new JLabel("" + hero.getPP());
		this.add(label);
		this.add(label1);
		this.add(label2);
		this.add(label3);
		this.add(label4);
		this.add(label5);
		this.setVisible(true);//顯示表格
	}
}
