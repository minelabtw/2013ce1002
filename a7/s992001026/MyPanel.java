package ce1002.a7.s992001026;
import javax.swing.JLabel;
import java.awt.*;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel sp = new StatusPanel();
	
	public MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		sp.setBounds(245 + 10, 5, 120, 190);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(sp);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		sp.setState(hero);
	}
}
