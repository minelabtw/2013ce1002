package ce1002.a7.s992001026;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	public StatusPanel() {
		super(new GridLayout(3 , 2));
	}

	public void setState(Hero hero) {
		
		String strs[] = {"HP", "" + hero.getHp(), "MP", "" + hero.getMp(), "PP", "" + hero.getPp()};
		for (int i = 0; i < 6; ++i) {
			JLabel jl1 = new JLabel();
			jl1.setText(strs[i]);
			add(jl1);
		}
	}
}
