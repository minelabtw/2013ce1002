package ce1002.a7.s102502536;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	
	protected JLabel label1 = new JLabel("HP");
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel("MP");
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel("PP");
	protected JLabel label6 = new JLabel();
	
	StatusPanel() {
        
		setLayout(null);
		setLayout(new GridLayout(3,2));
		/*add label to status panel*/
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
		setVisible(true);
		
	}
	
	public void setState(Hero hero) {
		/* set label's text */
		label2.setText(hero.getHp() + "");
		label4.setText(hero.getMp() + "");
		label6.setText(hero.getPp() + "");
			
	}

}
