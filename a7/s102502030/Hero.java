package ce1002.a7.s102502030;

import javax.swing.ImageIcon;

public class Hero {

	protected String name;
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	
	public Hero() {
		HP = 30.0;
		MP = 30.0;
		PP = 30.0;
	}

	//SetGetName
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//SetGet HP
	public double getHP() {
		return HP;
	}

	public void setHP(double hP) {
		HP = hP;
	}

	//SetGetMP
	public double getMP() {
		return MP;
	}

	public void setMP(double mP) {
		MP = mP;
	}

	//SetGetPP
	public double getPP() {
		return PP;
	}

	public void setPP(double pP) {
		PP = pP;
	}
	
	//SetGetImage
	public ImageIcon getImage() {
		return this.image;
	}

	public void setImage( String imgname ) {
		this.image = new ImageIcon( imgname );
	}
	
}
