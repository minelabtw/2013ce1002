package ce1002.a7.s102502030;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//設三個英雄丟進陣列
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		//執行MyFrame
		MyFrame frame = new MyFrame( hero );
	}

}
