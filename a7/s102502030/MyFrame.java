package ce1002.a7.s102502030;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.BorderFactory;

public class MyFrame extends JFrame {

	protected MyPanel K = new MyPanel();
	protected MyPanel S = new MyPanel();
	protected MyPanel W = new MyPanel();
	protected Hero[] hero;
	
	public MyFrame ( Hero[] hero ) {
		//設定視窗並傳出三個Panel資料
		this.hero = hero;
		setLayout( null );
		setBounds( 300, 50, 600, 680 );
		setVisible( true );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		newRolePos( hero[0], W, 10, 10 );
		newRolePos( hero[1], S, 10, 220 );
		newRolePos( hero[2], K, 10, 430 );
	}
	
	public void newRolePos( Hero hero, MyPanel panel , int x, int y ) {
		//設定三個panel資料並呼叫label 最後把panel加入frame
		panel.setBounds( x, y, 550, 200 );
		panel.setBorder( BorderFactory.createLineBorder( Color.BLACK, 3 ) );
		panel.setRoleState( hero );
		add( panel );
	}
}
