package ce1002.a7.s102502030;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	protected JLabel HP = new JLabel();
	protected JLabel HPdata = new JLabel();
	protected JLabel MP = new JLabel();
	protected JLabel MPdata = new JLabel();
	protected JLabel PP = new JLabel();
	protected JLabel PPdata = new JLabel();
	
	public StatusPanel() {
		//設定六個Label
		setLayout( new GridLayout( 3, 2 ) );
		add( HP );
		add( HPdata );
		add( MP );
		add( MPdata );
		add( PP );
		add( PPdata );
	}
	
	public void setState( Hero hero ) {
		//設定Label上顯示的資料
		HP.setText( "HP" );
		HPdata.setText( "" + hero.getHP() );
		MP.setText( "MP" );
		MPdata.setText( "" + hero.getMP() );
		PP.setText( "PP" );
		PPdata.setText( "" + hero.getPP() );
	}
}
