package ce1002.a7.s102502030;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	protected JLabel data = new JLabel();
	protected JLabel img = new JLabel();
	protected StatusPanel status = new StatusPanel();
	
	public MyPanel () {
		//設定label的資料並加到panel中
		setLayout( null );
		data.setBounds( 5, 0, 220, 20 );
		img.setBounds( 5, 15, 240, 180 );
		status.setBounds( 255, 15, 240, 180 );
		add( data );
		add( img );
		add( status );
	}
	
	public void setRoleState( Hero hero ) {
		//設定每個Panle中的兩個label
		img.setIcon( hero.getImage() );
		data.setText( hero.getName() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP:"+hero.getPP() );
		status.setState( hero );
	}
}
