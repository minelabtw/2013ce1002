package ce1002.a7.s102502049;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	
	private JLabel[] labels = new JLabel[6]; // create 6 labels
	
	public StatusPanel(){
		setLayout(new GridLayout(3, 2)); // grid layout
		
		for(int i = 0; i<6; i++){
			labels[i] = new JLabel();
			labels[i].setSize(220, 20); // set label size
			add(labels[i]); // add 6 labels
		}
		
	}
	
	public  void setState(Hero hero) { //set hero state
		labels[0].setText("HP");
		labels[1].setText(""+hero.getHP());
		labels[2].setText("MP");
		labels[3].setText(""+hero.getMP());
		labels[4].setText("PP");
		labels[5].setText(""+hero.getPP());
	}

}
