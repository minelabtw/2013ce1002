package ce1002.a7.s102502049;
 
public class A7 {
 
	public static void main(String[] args) {
		
		Hero[] heros = new Hero[3]; // create Hero array 3
		
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight(); // construct three role
		
		MyFrame frame = new MyFrame(heros); // show frame
		
	}
 
}
