package ce1002.a7.s102502049;

public class Swordsman extends Hero {
	public Swordsman(){ // default constructor
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	
	@Override
	public float getHP(){
		return super.getHP()*1/10;
	}
	
	public float getMP(){
		return super.getMP()*1/10;
	}
	
	public float getPP(){
		return super.getPP()*8/10;
	}
}
