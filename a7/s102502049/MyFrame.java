package ce1002.a7.s102502049;

import java.awt.Color;
 
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
 
public class MyFrame extends JFrame{
	
	private MyPanel panel1;
	private MyPanel panel2;
	private MyPanel panel3; // three MyPanel
	
	private Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros; // set heros
		
		panel1 = new MyPanel(heros[0]);
		panel2 = new MyPanel(heros[1]);
		panel3 = new MyPanel(heros[2]); // use constructor to pass hero to StatusPanel
		
		setLayout(null);
		setBounds(400 , 30 ,440 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); // Frame setting
		
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430); // MyPanel setting and add
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 400 , 200);	// set the position of the panels
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3)); // border line color=>black size=>3
		panel.setRoleState(hero); // call MyPanel function
		add(panel);
	}
}
