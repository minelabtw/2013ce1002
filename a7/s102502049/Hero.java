package ce1002.a7.s102502049;

import javax.swing.ImageIcon;

public class Hero {
	String name;
	private float HP=30;
	private float MP=30;
	private float PP=30;
	private ImageIcon image; // save ImageIcon
	
	public Hero(){ // default constructor
		
	}
	
	public void setName(String x) { // setter
		name = x;
	}
	
	public void setHP(float x){
		HP = x;
	}
	
	public void setMP(float x){
		MP = x;
	}
	
	public void setPP(float x){
		PP = x;
	}
	
	public String getName(){ // getter
		return name;
	}
	
	public float getHP(){
		return HP;
	}
	
	public float getMP(){
		return MP;
	}
	
	public float getPP(){
		return PP;
	}
	
	public void setImage(String imagename) // set Image Icon
	{
		this.image = new ImageIcon(imagename);
	}
	
	public ImageIcon getImage()
	{
		return this.image;
	}
	
}
