package ce1002.a7.s102502049;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	private JLabel label = new JLabel();
	private JLabel imagelabel = new JLabel(); // two label component in MyPanel

	private StatusPanel spanel1 = new StatusPanel();
	private StatusPanel spanel2 = new StatusPanel();
	private StatusPanel spanel3 = new StatusPanel(); // three StatusPanel
	
	private Hero hero; // save heros array
	
	MyPanel(Hero hero)
	{
		this.hero = hero;
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180); // set position and size
		add(label);
		add(imagelabel); //add two labels
		
		CallsetState(hero, spanel1, 260, 3);
		CallsetState(hero, spanel2, 260, 3);
		CallsetState(hero, spanel3, 260, 3); // add StatusPanel
		
	}
	
	public void setRoleState(Hero hero)
	{
		imagelabel.setIcon(hero.getImage()); // set image-label
		label.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP()); // set text-label
	}
	
	public void CallsetState(Hero hero, StatusPanel panel, int x, int y){
		panel.setBounds(x, y, 130 , 180);	// set the size and position of the panels
		panel.setState(hero); // call setState function
		add(panel);
	}
}
