package ce1002.a7.s101502205;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;

public class StatusPanel extends JPanel{
	
	protected JLabel hpLabel, mpLabel, ppLabel, hpValLabel, mpValLabel, ppValLabel;
	
	public StatusPanel() {
		
		// Set Layout as GridLayout
		setLayout(new GridLayout(3, 2, 5, 5));
		
		// Create all Labels
		hpLabel = new JLabel();
		mpLabel = new JLabel();
		ppLabel = new JLabel();
		hpValLabel = new JLabel();
		mpValLabel = new JLabel();
		ppValLabel = new JLabel();
		
		// Set text
		hpLabel.setText("HP");
		mpLabel.setText("MP");
		ppLabel.setText("PP");
		
		// Add all labels to panel orderly
		add(hpLabel);
		add(hpValLabel);
		add(mpLabel);
		add(mpValLabel);
		add(ppLabel);
		add(ppValLabel);
	}
	
	public void setState(Hero hero) {
		// Set text
		hpValLabel.setText( String.valueOf(hero.getHp()) );
		mpValLabel.setText( String.valueOf(hero.getMp()) );
		ppValLabel.setText( String.valueOf(hero.getPp()) );
	}
}
