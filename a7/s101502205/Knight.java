package ce1002.a7.s101502205;

public class Knight extends Hero {

	public Knight() {
		// Set the name
		name = "Knight";
	}
	
	// Overridings
	public double getHp() {
		return 0.8*hp;
	}
	public double getMp() {
		return 0.1*mp;
	}
	public double getPp() {
		return 0.1*pp;
	}
	
}
