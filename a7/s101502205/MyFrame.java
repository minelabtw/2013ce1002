package ce1002.a7.s101502205;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
 
public class MyFrame extends JFrame{
	
	// 3 panel for 3 hero
	protected MyPanel panel1;
	protected MyPanel panel2;
	protected MyPanel panel3;
	// hero information
	protected Hero[] heros;
	
	public MyFrame(){
		
		// Create 3 new panel
		panel1 = new MyPanel();
		panel2 = new MyPanel();
		panel3 = new MyPanel();
		
		// Create heros
		heros = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		// Set Layout, size, location, ......
		setLayout(null);
		setSize(609 , 689);
		/* Faith of 609 and 689
		        $$       $$$$      $$$$$               $$       $$$$     $$$$$ 
		       $$       $$  $$   $$    $$             $$       $$  $$  $$    $$   
		      $$ $$    $$    $$  $$    $$            $$ $$      $$$$   $$    $$ 
		     $$    $$  $$    $$    $$ $$            $$    $$   $$  $$    $$ $$
		     $$    $$   $$  $$       $$             $$    $$  $$    $$     $$
		      $$$$$      $$$$       $$               $$$$$     $$$$$$     $$
		 */
		
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		// Set heros' position and status
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		// Set panel location
		panel.setBounds(x, y, 380 , 200);
		// Set border
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		// Set hero status
		panel.setRoleState(hero);
		// add panel to frame
		add(panel);
	}
}