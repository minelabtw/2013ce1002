package ce1002.a7.s101502205;

public class Wizard extends Hero {

	public Wizard() {
		// Set the name
		name = "Wizard";
	}
	
	// Overridings
	public double getHp() {
		return 0.2*hp;
	}
	public double getMp() {
		return 0.7*mp;
	}
	public double getPp() {
		return 0.1*pp;
	}
	
}
