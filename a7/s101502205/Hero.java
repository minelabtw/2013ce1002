package ce1002.a7.s101502205;

public class Hero {
	
	protected String name;
	protected double hp, mp, pp;
	
	public Hero() {
		// Initialize
		hp = 30;
		mp = 30;
		pp = 30;
	}
	
	// Getters and Setters
	// Name
	public void setName (String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	
	// HP
	public void setHp (float hp) {
		this.hp = hp;
	}
	public double getHp() {
		return hp;
	}
	
	// MP
	public void setMp(float mp) {
		this.mp = mp;
	}
	public double getMp() {
		return mp;
	}
	
	// PP
	public void setPp(float pp) {
		this.pp = pp;
	}
	public double getPp() {
		return pp;
	}
}
