package ce1002.a7.s101502205;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class MyPanel extends JPanel{
	
	// Two label and a status panel
	protected JLabel textLabel;
	protected JLabel imageLabel;
	protected StatusPanel sPanel;
	
	MyPanel()
	{
		// Create labels and panel
		textLabel = new JLabel();
		imageLabel = new JLabel();
		sPanel = new StatusPanel();
		
		// setLayout
		setLayout(null);
		
		// set Location and size
		textLabel.setBounds(5,0,220,20);
		imageLabel.setBounds(5,15,240,180);
		sPanel.setBounds(250, 15, 120, 180);
		
		// Add them to panel
		add(textLabel);
		add(imageLabel);
		add(sPanel);
	}
	
	public void setRoleState(Hero hero)
	{
		// Set text and image
		textLabel.setText(hero.getName() + " HP: " + hero.getHp() + " MP: " + hero.getMp() + " PP: " + hero.getPp());
		imageLabel.setIcon(new ImageIcon(hero.getName() + ".jpg"));
		sPanel.setState(hero);
	}
}
