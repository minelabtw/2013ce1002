package ce1002.a7.s101502205;

public class Swordsman extends Hero {
	
	public Swordsman() {
		// Set the name
		name = "Swordsman";
	}
	
	// Overridings
	public double getHp() {
		return 0.1*hp;
	}
	public double getMp() {
		return 0.1*mp;
	}
	public double getPp() {
		return 0.8*pp;
	}
	
}
