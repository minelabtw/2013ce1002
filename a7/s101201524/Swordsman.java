package ce1002.a6.s101201524;

public class Swordsman extends Hero{
	//change hero's data with swordsman's coefficient
		Swordsman(){
			setName("Swordsman");
			setImage("Swordsman.jpg");
			setHp(getHp()*0.1);
			setMp(getMp()*0.1);
			setPp(getPp()*0.8);
		}
}
