package ce1002.a6.s101201524;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	private MyPanel wizard; //wizard's panel
	private MyPanel swordsman; //swordsman's panel
	private MyPanel knight; //knight's panel
	private void newRolePos(Hero hero, MyPanel panel, int x, int y){ //set panel a hero class, it's size 
		panel.setBounds(x, y, 260, 230);
		panel.setRoleState(hero);
		panel.setBorder(BorderFactory.createLineBorder(Color.black,2));
		add(panel); // add to frame
	}
	
	MyFrame(){
		setLayout(null);
		setSize(295,770);
		wizard = new MyPanel();
		newRolePos(new Wizard(), wizard, 10, 10); // set wizard(panel) to be wizard(hero)
		swordsman = new MyPanel();
		newRolePos(new Swordsman(), swordsman, 10, 250); // set swordsman(panel) to be swordsman(hero)
		knight = new MyPanel();
		newRolePos(new Knight(), knight, 10, 490); // set knight(panel) to be knight(hero)
		setVisible(true);
	}
}
