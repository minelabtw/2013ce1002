package ce1002.a7.s102502562;

public class Hero {
	private String name;
	//初始化HP,MP,PP為30.0
	private float HP=(float)30.0;
	private float MP=(float)30.0;
	private float PP=(float)30.0;
	//設定名字,HP,MP,PP
	public void setname(String name)
	{
		this.name=name;
	}
	public void setHP(float hp)
	{
		HP=hp;
	}
	public void setMP(float mp)
	{
		this.MP=mp;
	}
	public void setPP(float pp)
	{
		this.PP=pp;
	}
	//取得名字,HP,MP,PP
	public String getname()
	{
		return name;
	}
	public float getHP()
	{
		return HP;
	}
	public float getMP()
	{
		return MP;
	}
	public float getPP()
	{
		return PP;
	}
}
