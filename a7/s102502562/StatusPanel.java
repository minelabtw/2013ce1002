package ce1002.a7.s102502562;

import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.xml.soap.Text;

public class StatusPanel extends JPanel{
	JLabel HP;
	JLabel MP;
	JLabel PP;
	JLabel HPinfo;
	JLabel MPinfo;
	JLabel PPinfo;
	public void setState(Hero hero)
	{
		setLayout(new GridLayout(3,2,60,65));//設定表格
		HP=new JLabel("HP");//設定info
		MP=new JLabel("MP");
		PP=new JLabel("PP");
		HPinfo=new JLabel(""+hero.getHP());
		MPinfo=new JLabel(""+hero.getMP());
		PPinfo=new JLabel(""+hero.getPP());
		this.add(HP);//新增info進去表格裡
		this.add(HPinfo);
		this.add(MP);
		this.add(MPinfo);
		this.add(PP);
		this.add(PPinfo);
	}
}
