package ce1002.a7.s102502562;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	JLabel info;
	JLabel img;
	ImageIcon icon;
	MyPanel()
	{
		this.setSize(390,230);//設定Panel大小
		this.setBorder(new LineBorder(Color.black, 5));
	}
	public void setRoleState(Hero hero)
	{
		StatusPanel SP=new StatusPanel();
		icon=new ImageIcon(hero.getname() + ".jpg");
		info=new JLabel(hero.getname() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
		img=new JLabel(icon);
		SP.setState(hero);
		this.add(info);//新增info,圖片,表格裡的info
		this.add(img);
		this.add(SP);
	}
}
