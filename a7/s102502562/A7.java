package ce1002.a7.s102502562;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		MyPanel[] panel = new MyPanel[3];
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[0].setname("Wizard");
		hero[1] = new Swordsman();
		hero[1].setname("Swordsman");
		hero[2] = new Knight();
		hero[2].setname("Knight");
		frame.setSize(450,770);//設定frame的大小
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		for (int i=0;i<3;i++)//呼叫frame的newRolePos
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i], panel[i], 10, 10 + 240 * i);
		}
		frame.setVisible(true);
	}
}
