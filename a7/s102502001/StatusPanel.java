package ce1002.a7.s102502001;
import javax.swing.*;
import java.awt.*;
public class StatusPanel extends JPanel{   //declare label
	protected JLabel label = new JLabel();
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	protected JLabel label5 = new JLabel();
	protected JLabel label6 = new JLabel();
	protected JPanel statuspanel = new JPanel();
	
	StatusPanel(){        //add label by gridlayout
		setLayout(null);
		setLayout(new GridLayout(3,2));
		add(label);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
		setVisible(true);
	}
	
	public void setState(Hero hero) {    //set text
		label.setText("HP");
		label2.setText(""+hero.getHp());
		label3.setText("MP");
		label4.setText(""+hero.getMp());
		label5.setText("PP");
		label6.setText(""+hero.getPp());
	}
}
