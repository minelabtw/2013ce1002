package ce1002.a7.s102502021;

public class Knight extends Hero{
	public Knight() {
		// setup the title for this character's name
		super.setname("Knight");
		super.setImage("Knight.jpg");
	}

	public float gethp() {
		return _hp * 8 / 10;
	}

	public float getmp() {
		return _mp / 10;
	}

	public float getpp() {
		return _pp / 10;
	}

}
