package ce1002.a7.s102502021;

import java.awt.GridLayout;
import javax.swing.*;

public class StatusPanel extends JPanel {
	public JLabel lb1 = new JLabel();
	public JLabel lb2 = new JLabel();
	public JLabel lb3 = new JLabel();
	public JLabel lb4 = new JLabel();
	public JLabel lb5 = new JLabel();
	public JLabel lb6 = new JLabel();

	public StatusPanel() {
		this.setSize(250, 200); // �j�p
		this.setLayout(new GridLayout(3, 2)); 
	}

	public void setState(Hero hero) {
		lb1.setText(" HP");
		lb2.setText(hero.gethp() + "");
		lb3.setText(" MP");
		lb4.setText(hero.getmp() + "");
		lb5.setText(" PP");
		lb6.setText(hero.getpp() + "");
		this.add(lb1); 
		this.add(lb2); 
		this.add(lb3);
		this.add(lb4); 		
		this.add(lb5);
		this.add(lb6); 
	}

}
