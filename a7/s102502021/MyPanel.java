package ce1002.a7.s102502021;

import javax.swing.JLabel;
import javax.swing.JPanel;


	public class MyPanel extends JPanel{
		protected JLabel label = new JLabel();
		protected JLabel imagelabel = new JLabel();
		public StatusPanel sp1;

		MyPanel() {
			
			setLayout(null);
			label.setBounds(5, 0, 220, 20);
			imagelabel.setBounds(5, 15, 240, 180);
			
			add(label);
			add(imagelabel);
			
		}

		public void setRoleState(ce1002.a7.s102502021.Hero hero) {
			
			imagelabel.setIcon(hero.getImage());
			label.setText(hero.getname() + " HP: " + hero.gethp() + " MP: "
					+ hero.getmp() + " PP:" + hero.getpp());
			this.add(sp1); 
			sp1 =new StatusPanel();
			sp1.setState(hero);
			sp1.setLocation(275, 0);
		}
	}
	


