package ce1002.a7.s102502021;

public class Wizard extends Hero{
	public Wizard() {
		// setup the title for this character's name
		super.setname("Wizard");
		super.setImage("Wizard.jpg");
	}

	public float gethp() {
		return _hp * 2 / 10;
	}

	public float getmp() {
		return _mp * 7 / 10;
	}

	public float getpp() {
		return _pp / 10;
	}

}
