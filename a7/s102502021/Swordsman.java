package ce1002.a7.s102502021;

public class Swordsman extends Hero{
	public Swordsman() {
		// setup the title for this character's name
		super.setname("Swordsman");
		super.setImage("Swordsman.jpg");
	}

	public float gethp() {
		return _hp / 10;
	}

	public float getmp() {
		return _mp / 10;
	}

	public float getpp() {
		return _pp * 8 / 10;
	}

}
