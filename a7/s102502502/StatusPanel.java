package ce1002.a7.s102502502;
import java.awt.*;

import javax.swing.*;
@SuppressWarnings("serial")
public class StatusPanel extends JPanel{
	protected JLabel abn0 = new JLabel();
	protected JLabel abn1 = new JLabel();
	protected JLabel abn2 = new JLabel();
	protected JLabel abv0 = new JLabel();
	protected JLabel abv1 = new JLabel();
	protected JLabel abv2 = new JLabel();
	
	 StatusPanel(Hero hero){
	  JFrame.setDefaultLookAndFeelDecorated(true);
	  // use GridLayout to arrange our value form
	 setLayout( new GridLayout(3, 2, 50, 50));   
	    setVisible(true);
	    // add the six labels to the panel
	    	add(abn0);
	    	add(abn1);
	    	add(abn2);
	    	add(abv0);
	    	add(abv1);
	    	add(abv2);
	    }
	  public void setState(Hero hero) {
		  //set labels'texts
		  abn0.setText(" HP ");
		  abn1.setText(" MP ");
		  abn2.setText(" PP");
		  abv0.setText(""+hero.getHP());
		  abv1.setText(""+hero.getMP());
		  abv2.setText(""+hero.getPP());
		}		   		 
	}

