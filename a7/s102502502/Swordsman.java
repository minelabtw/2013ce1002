package ce1002.a7.s102502502;

public class Swordsman extends Hero {

	public Swordsman() {
		// setup the title for this character's name 
				super.setHeroName("Swordsman");
				super.setImage("Swordsman.jpg");
	}
	public double getHP()
	{
		return super.HP*0.1;
	}
	public double getMP()
	{
		return super.MP*0.1;
	}
	public double getPP()
	{
		return super.PP*0.8;
	}
}
