package ce1002.a7.s102502502;

import javax.swing.ImageIcon;

public class Hero {
	protected String HeroName;
	protected double HP;         
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	public Hero()
	{
		// we initialized these 3 variables value =30 in hero'e constructor
		// because it's called before it's child's constructor
		// so that we don't need to call it again in every children's constructor
		setHP(30.0);
		setMP(30.0);
		setPP(30.0);
	}
	public void setHeroName(String heroName) {
		HeroName = heroName;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	public void setImage(String imagename) {
		this.image = new ImageIcon(imagename);
	}	
	public String getHeroName() {
		return HeroName;
	}
	public double getHP() {
		return HP;
	}
	public double getMP() {
		return MP;
	}
	public double getPP() {
		return PP;
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}


