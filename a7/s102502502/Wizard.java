package ce1002.a7.s102502502;

public class Wizard extends Hero {

	public Wizard() {
		// setup the title for this character's name
				super.setHeroName("Wizard");
				super.setImage("Wizard.jpg");
	}
	public double getHP()
	{
		return super.HP*0.2;
	}
    public double getMP()
	{
		return super.MP*0.7;
	}
    public double getPP()
	{
		return super.PP*0.1;
	}
}
