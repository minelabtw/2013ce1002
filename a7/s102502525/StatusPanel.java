package ce1002.a7.s102502525;
import java.awt.GridLayout;

import javax.swing.*;
public class StatusPanel extends JPanel{
	 public StatusPanel(){
		 setLayout(new GridLayout(3,2,50,50));
	 }
	 public void setState(Hero hero) {
		 JLabel label1=new JLabel("HP");
		 JLabel label2=new JLabel(""+hero.getHp());
		 JLabel label3=new JLabel("MP");
		 JLabel label4=new JLabel(""+hero.getMp());
		 JLabel label5=new JLabel("PP");
		 JLabel label6=new JLabel(""+hero.getPp());
		 add(label1);
		 add(label2);
		 add(label3);
		 add(label4);
		 add(label5);
		 add(label6);
		 setVisible(true);
	 }
}
