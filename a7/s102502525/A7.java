package ce1002.a7.s102502525;
import javax.swing.*;
public class A6 {
	public static void main(String[] args) {
		Hero[] heros = new Hero[3];//建立名為hero的陣列
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		for(int i=0;i<3;i++)//印出名稱與能力
		System.out.println(heros[i].getName() + " HP: " + heros[i].getHp() + " MP: " + heros[i].getMp() + " PP: " + heros[i].getPp() );
		MyFrame frame=new MyFrame(heros);
	}		
}

