package ce1002.a7.s102502525;
public class Swordsman extends Hero {
	Swordsman(){
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	@Override
	public double getHp(){
	  return super.getHp() * 0.1;
	}
	@Override
	public double getMp(){
		return super.getMp() * 0.1;
	}
	@Override
	public double getPp(){
		return super.getPp() * 0.8;
	}
}
