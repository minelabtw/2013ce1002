package ce1002.a7.s102502525;
public class Wizard extends Hero {
	Wizard(){
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	@Override
	public double getHp(){
	  return super.getHp() * 0.2;
	}
	@Override
	public double getMp(){
		return super.getMp() * 0.7;
	}
	@Override
	public double getPp(){
		return super.getPp() * 0.1;
	}
}
