package ce1002.a7.s102502550;

public class Knight extends Hero{
   
	public Knight(){
    	super.setName("Knight");
    	super.setImage("Knight.jpg");
    }
    
    public float getHP(){                           //�S����get method
    	return (0.8f * super.getHP());
    }
    public float getMP(){
    	return (0.1f * super.getMP());
    }
    public float getPP(){
    	return (0.1f * super.getPP());
    }
}
