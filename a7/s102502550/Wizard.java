package ce1002.a7.s102502550;

public class Wizard extends Hero{
	public Wizard(){
    	super.setName("Wizard");
    	super.setImage("Wizard.jpg");
    }
    
    public float getHP(){                           //�S����get method
    	return (0.2f * super.getHP());
    }
    public float getMP(){
    	return (0.7f * super.getMP());
    }
    public float getPP(){
    	return (0.1f * super.getPP());
    }
}
