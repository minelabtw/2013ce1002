package ce1002.a7.s102502550;

import javax.swing.ImageIcon;

public class Hero {                          //�����O
    private String name;
    private int HP;
    private int MP;
    private int PP;
    protected ImageIcon image; 
    
    public Hero(){
        HP=30; MP=30; PP=30;	             //��l��
    }
    
    public void setName(String name){        //set && get method
  		this.name = name;
  	}
    public void setHP(int HP) {
		this.HP = HP;
	}
    public void setMP(int MP){
  		this.MP = MP;
  	}
    public void setPP(int PP){
  		this.PP = PP;
  	}
    
    public String getName(){
    	return name;
    }
    public float getHP(){
    	return HP;
    }
    public float getMP(){
    	return MP;
    }
    public float getPP(){
    	return PP;
    }
   
    public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}
