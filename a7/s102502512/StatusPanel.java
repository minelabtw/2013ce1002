package ce1002.a7.s102502512;
import javax.swing.*;

import java.awt.*;

public class StatusPanel extends JPanel{		
	protected JLabel label1 = new JLabel();			//Line 7-12: create six labels
	protected JLabel label2 = new JLabel();	
	protected JLabel label3 = new JLabel();	
	protected JLabel label4 = new JLabel();	
	protected JLabel label5 = new JLabel();	
	protected JLabel label6 = new JLabel();	
	StatusPanel()									//Line 13-16: sort labels by GridLayout
	{
		setLayout(new GridLayout(3,2));
	}
	
	public void setState(Hero hero)					//Line 18-33: pass hero to labels
	{
		label1.setText(" HP ");
		label2.setText(""+hero.getHp());
		label3.setText(" MP ");
		label4.setText(""+hero.getMp());
		label5.setText(" PP ");
		label6.setText(""+hero.getPp());
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
	
	}
	
}
