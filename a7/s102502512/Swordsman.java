package ce1002.a7.s102502512;

public class Swordsman extends Hero{
	Swordsman(String a)					//make some function to set and get heros' abilities
	{
		super.setName(a);				//override the function
		super.setIm("Swordsman.jpg");
	}
	public double getHp()
	{
		return super.getHp()*0.1;
	}
	public double getMp()
	{
		return super.getMp()*0.1;
	}
	public double getPp()
	{
		return super.getPp()*0.8;
	}
	public String getName() 
	{
		return super.getName();
	}

}
