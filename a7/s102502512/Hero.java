package ce1002.a7.s102502512;
import java.awt.*;
import javax.swing.*;

public class Hero {
	private String name;
	private double hp;
	private double mp;
	private double pp;
	private ImageIcon im;
	
	public String getName() {				//make some function to set and get heros' abilities
		return name;
	}
	public ImageIcon getIm() {
		return im;
	}
	public void setIm(String imname) {
		this.im = new ImageIcon(imname);
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getHp() {
		return hp;
	}
	public void setHp(double hp) {
		this.hp = hp;
	}
	public double getMp() {
		return mp;
	}
	public void setMp(double mp) {
		this.mp = mp;
	}
	public double getPp() {
		return pp;
	}
	public void setPp(double pp) {
		this.pp = pp;
	}

}
