package ce1002.a7.s102502512;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero [] rec = new Hero[3];				//set the array by using Polymorphism
		rec[0] = new Wizard("Wizard");
		rec[1] = new Swordsman("Swordsman");
		rec[2] = new Knight("Knight");
		
		for(int i=0;i<3;i++)					//print the heros abilities
		{
			rec[i].setHp(30);
			rec[i].setMp(30);
			rec[i].setPp(30);
		}
		MyFrame anyway = new MyFrame(rec);
		
	}

}
