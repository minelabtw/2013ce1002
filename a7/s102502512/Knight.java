package ce1002.a7.s102502512;

public class Knight extends Hero{
	Knight(String a)					//make some function to set and get heros' abilities
	{
		super.setName(a);
		super.setIm("Knight.jpg");
	}
	public double getHp()
	{
		return super.getHp()*0.8;		//override the function
	}
	public double getMp()
	{
		return super.getMp()*0.1;
	}
	public double getPp()
	{
		return super.getPp()*0.1;
	}
	public String getName() 
	{
		return super.getName();
	}

}
