package ce1002.a7.s102502512;
import java.awt.*;

import javax.swing.*;

public class MyFrame extends JFrame{
	private MyPanel p1 = new MyPanel();
	private MyPanel p2 = new MyPanel();
	private MyPanel p3 = new MyPanel();
	private Hero [] rec;
	public MyFrame(Hero[] rec){												//Line 11-20: show panels in the frame
		this.rec = rec;
		setLayout(null);
		setBounds(300 , 50 , 500 , 670);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		
		newRolePos(rec[0] , p1 , 10 , 10);
		newRolePos(rec[1] , p2 , 10 , 220);
		newRolePos(rec[2] , p3 , 10 , 430);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)		//Line 22-28: to set the panel's location and size
	{
		panel.setBounds(x, y, 450 , 200);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		add(panel);
	}
	
}
