package ce1002.a7.s102502512;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	private StatusPanel may = new StatusPanel();
	
	MyPanel()										//Line 10-18: create a panel with setting its location and size
	{
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		may.setBounds(270, 15, 150, 150);
		add(label);
		add(imagelabel);
		add(may);
	}
	
	public void setRoleState(Hero hero)				//Line 21-26: pass the hero to the panel
	{
		imagelabel.setIcon(hero.getIm());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		may.setState(hero);
	}
}
