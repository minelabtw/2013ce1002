package ce1002.a7.s102502037;


public class Swordsman extends Hero{
	Swordsman(){//設定與改變值
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	public double getHP() {
		return super.getHP() * 0.1;
	}
	public double getMP() {
		return super.getMP() * 0.1;
	}
	public double getPP() {
		return super.getPP() * 0.8;
	}
}
