package ce1002.a7.s102502037;
import java.awt.*;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.*;
public class MyPanel extends JPanel{
	protected JLabel Label1 = new JLabel();
	protected JLabel Label2 = new JLabel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		Label1.setBounds(5,0,220,20);
		Label2.setBounds(5,15,240,180);
		/*add label to panel*/
		add(Label1);
		add(Label2);
		
		StatusPanel SP = new StatusPanel();
		add(SP);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		Label2.setIcon(hero.getImage());
		Label1.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
		SP.setState(hero);
		

	}

}
