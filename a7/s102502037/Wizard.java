package ce1002.a7.s102502037;

public class Wizard extends Hero{
	Wizard(){//設定與改變值
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	public double getHP() {
		return super.getHP() * 0.2;
	}
	public double getMP() {
		return super.getMP() * 0.7;
	}
	public double getPP() {
		return super.getPP() * 0.1;
	}
}
