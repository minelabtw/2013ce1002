package ce1002.a7.s102502037;

public class Knight extends Hero {
	Knight(){//設定與改變值
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	public double getHP() {
		return super.getHP() * 0.8;
	}
	public double getMP() {
		return super.getMP() * 0.1;
	}
	public double getPP() {
		return super.getPP() * 0.1;
	}

}