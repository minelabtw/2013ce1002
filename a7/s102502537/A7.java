package ce1002.a7.s102502537;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3];
		hero[0] = new Wizard();// use construct
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		MyFrame frame = new MyFrame(hero[0], hero[1], hero[2]);
		frame.setSize(450, 800);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
