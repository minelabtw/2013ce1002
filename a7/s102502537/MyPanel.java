package ce1002.a7.s102502537;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	String a;
	String b;
	StatusPanel statusPanel = new StatusPanel();

	public MyPanel() {
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}

	public void setRoleState(Hero hero) {
		a = hero.getname() + " HP: " + hero.getHP() + " MP: " + hero.getMP()
				+ " PP: " + hero.getPP();
		b = hero.getname() + ".jpg";
		statusPanel.setState(hero);
	}

	public void realadd() {
		setLayout(new BorderLayout());
		add(new JLabel(a), BorderLayout.NORTH);
		ImageIcon image = new ImageIcon(b);
		add(new JLabel(image), BorderLayout.WEST);
		add(statusPanel, BorderLayout.CENTER);
	}
}
