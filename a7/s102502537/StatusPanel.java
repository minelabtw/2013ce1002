package ce1002.a7.s102502537;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	public StatusPanel() {
	}

	public void setState(Hero hero) {
		setLayout(new GridLayout(3, 2));

		// add
		add(new JLabel("HP"));
		add(new JLabel("" + hero.getHP()));
		add(new JLabel("MP"));
		add(new JLabel("" + hero.getMP()));
		add(new JLabel("PP"));
		add(new JLabel("" + hero.getPP()));
	}

}
