package ce1002.a7.s102502537;

public class Knight extends Hero {
	String name = "Knight";

	public Knight() {
		super();
	}

	public String getname() {
		// 得name
		return name;
	}

	public double getHP() {
		// 得HP
		return HP * 0.8;
	}

	public double getMP() {
		// 得MP
		return MP * 0.1;
	}

	public double getPP() {
		// 得PP
		return PP * 0.1;
	}
}
