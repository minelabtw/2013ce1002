package ce1002.a7.s102502002;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		Hero[] heros = new Hero[3]; // initialize a class array heros
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		for(Hero hero : heros){
			System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp());
		}
		MyFrame frame = new MyFrame(heros); //new a frame
		
	}
}
