package ce1002.a7.s102502002;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel {
	
	protected JLabel label = new JLabel(); //new a label to put the status of each character
	protected JLabel imagelabel = new JLabel(); //new a label to put the image
	protected StatusPanel stp = new StatusPanel(); //new a status panel
	MyPanel(){
		setLayout(null); //no layout
		setVisible(true); //set visibility
		//set the bounds
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		//add the labels to the panel
		add(label);
		add(imagelabel);
		stp.setBounds(250, 15, 140, 180);
		add(stp);
	}
	public void setRoleState(Hero hero){
		imagelabel.setIcon(hero.getImage()); //add the icon to the label
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp()); //add text to the label
		stp.setState(hero);
	}
}
