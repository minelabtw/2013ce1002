package ce1002.a7.s102502002;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;

public class StatusPanel extends JPanel {

	//new six labels to put the name of each status and the data
	protected JLabel lb1 = new JLabel();
	protected JLabel lb2 = new JLabel();
	protected JLabel lb3 = new JLabel();
	protected JLabel lb4 = new JLabel();
	protected JLabel lb5 = new JLabel();
	protected JLabel lb6 = new JLabel();
	
	StatusPanel(){
		setVisible(true);
		setLayout(new GridLayout(3, 2)); //set the layout
		//add all six labels to the panel
		add(lb1);
		add(lb2);
		add(lb3);
		add(lb4);
		add(lb5);
		add(lb6);
	}
	public void setState(Hero hero) {
		//set the text of each label
		lb1.setText("HP");
		lb2.setText(" "+hero.getHp());
		lb3.setText("MP");
		lb4.setText(" "+hero.getMp());
		lb5.setText("PP");
		lb6.setText(" "+hero.getPp());
		
	}
	
}
