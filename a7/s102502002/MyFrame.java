package ce1002.a7.s102502002;
import javax.swing.BorderFactory;
import javax.swing.JFrame;

import java.awt.Color;

public class MyFrame extends JFrame {
	
	protected MyPanel pw = new MyPanel();
	protected MyPanel ps = new MyPanel();
	protected MyPanel pk = new MyPanel();
	protected Hero[] heros;
	
	MyFrame(Hero[] heros){
		this.heros=heros;
		//set the size, visibility and the layout of the frame
		setSize( 450, 700);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		newRolePos(heros[0], pw, 10, 10);
		newRolePos(heros[1], ps, 10, 220);
		newRolePos(heros[2], pk, 10, 430);
	}
	public void newRolePos(Hero hero , MyPanel panel, int x , int y){
		panel.setBounds(x, y, 400 , 200); // set the bounds of the panel
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		add(panel); // add the panel to the frame
	}
	
}
