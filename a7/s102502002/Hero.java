package ce1002.a7.s102502002;
import javax.swing.ImageIcon;

public class Hero{
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;
	
	public Hero()
	{
		setHp(30);
		setMp(30);
		setPp(30);
	}
	
	//setters and getters for name, hp, mp, pp and image
	public void setName(String Name)
	{
		this.name=Name;
	}
	public String getName()
	{
		return this.name;
	}
	public double getHp()
	{
		return this.hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return this.pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename); //new an icon
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}