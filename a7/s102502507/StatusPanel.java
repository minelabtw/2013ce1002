package ce1002.a7.s102502507;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.GridLayout;

public class StatusPanel extends JPanel {

	protected JLabel s1;
	protected JLabel s2;
	protected JLabel s3;
	protected JLabel s4;
	protected JLabel s5;
	protected JLabel s6;

	public StatusPanel() {
		setLayout(new GridLayout(3, 2));
		s1 = new JLabel("HP");
		s2 = new JLabel("");
		s3 = new JLabel("MP");
		s4 = new JLabel("");
		s5 = new JLabel("PP");
		s6 = new JLabel("");
		
		this.add(s1);
		this.add(s2);
		this.add(s3);
		this.add(s4);
		this.add(s5);
		this.add(s6);
	}
	public void setState(Hero hero){
		s2.setText("" + hero.getHP());
		s4.setText("" + hero.getMP());
		s6.setText("" + hero.getPP());
	}
}
