package ce1002.a7.s102502507;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import java.awt.BorderLayout;
import java.awt.Color;

public class MyPanel extends JPanel {
	StatusPanel J;//將在StatusPanel中的Panel模型用J代表

	public MyPanel() { // 設定Panel的格式
		setSize(263, 230);
		setLayout(new BorderLayout());
		setBorder(new LineBorder(Color.BLACK, 3));
		J = new StatusPanel();
	}

	public void setRoleState(Hero hero) {

		// 將Label的資料放進Panel裡並調整其相對位子
		JLabel text = new JLabel(hero.getHerosName() + " HP:" + hero.getHP()
				+ " MP:" + hero.getMP() + " PP:" + hero.getPP());
		add(text, BorderLayout.NORTH);
		JLabel image = new JLabel(new ImageIcon( hero.getHerosName() + ".jpg"));
		add(image, BorderLayout.WEST);
		J.setState(hero);
		add(J, BorderLayout.CENTER);

	}

}
