package ce1002.a7.s102502558;
import javax.swing.* ;
import java.awt.Color ;

import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JLabel state = new JLabel() ;
	JLabel image = new JLabel() ;
	StatusPanel statuspanel;
	
	MyPanel()  //
	{
		setSize(380,235) ;
		setBorder(new LineBorder(Color.black, 5)) ;
	}
	
	public void setRoleState(Hero hero){
		//new MyPanel();
		statuspanel = new StatusPanel(hero);
		state = new JLabel(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP()) ;
		ImageIcon image2 = new ImageIcon(hero.getName()+".jpg") ;
		image = new JLabel(image2) ;
		add(state);
		add(image);
		add(statuspanel);
	}
}
