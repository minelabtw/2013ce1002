package ce1002.a7.s102502558;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3]; //
		hero[0] = new Wizard(); //
		hero[1] = new Swordsman();
		hero[2] = new Knight();

		MyFrame frame = new MyFrame();//
		frame.setSize(400, 770);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MyPanel[] panel = new MyPanel[3]; //

		for (int a = 0; a < 3; a++) //
		{
			panel[a] = new MyPanel();
			frame.newRolePos(hero[a], panel[a], 10, 10 + 240 * a);
			frame.add(panel[a]);
		}
		frame.setVisible(true);
	}

}
