package ce1002.a7.s102502528;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	JLabel[] labels = new JLabel[3];  //sorry that I can't think up a name for it
	JLabel[] state = new JLabel[3];

	StatusPanel(Hero hero) {
		setSize(200, 200);
		setLayout(new GridLayout(3, 2, 60, 60)); // set layout
		setState(hero);
		for (int i = 0; i < 3; i++) {
			add(labels[i]);
			add(state[i]);
		}
	}

	public void setState(Hero hero) {
		labels = new JLabel[3];
		labels[0] = new JLabel("HP"); // set status
		labels[1] = new JLabel("MP");
		labels[2] = new JLabel("PP");
		state = new JLabel[3];
		state[0] = new JLabel("" + hero.getHP());
		state[1] = new JLabel("" + hero.getMP());
		state[2] = new JLabel("" + hero.getPP());
	}

}
