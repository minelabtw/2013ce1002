package ce1002.a7.s102502528;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {

	JLabel[] herolLabel = new JLabel[2];
	JPanel status;

	public MyPanel(Hero hero, int x, int y, int width, int height) {
		setLocation(x, y);        //set location
		setSize(width, height);   //set size
        Border border = new LineBorder(Color.black, 5);  //set border
        setBorder(border);  
        setRoleState(hero);  //put state into label
		add(herolLabel[0]);  //add labels to panel
		add(herolLabel[1]);
		add(status);
	}

	public void setRoleState(Hero hero) {
		ImageIcon image = new ImageIcon(hero.getname() + ".jpg");
		herolLabel[0] = new JLabel(hero.getname() + " HP: " + hero.getHP()
				+ " MP: " + hero.getMP() + " PP: " + hero.getPP());
		herolLabel[1] = new JLabel(image);
		status = new StatusPanel(hero);
	}
}
