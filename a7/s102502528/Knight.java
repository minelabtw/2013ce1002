package ce1002.a7.s102502528;

public class Knight extends Hero{
	//override
	Knight() {
        name = "Knight";
    }

	public float getHP() {
		return super.getHP() * 0.8f;
	}

	public float getMP() {
		return super.getMP() * 0.1f;
	}
	
	public float getPP() {
		return super.getPP() * 0.1f;
	}
}