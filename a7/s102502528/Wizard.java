package ce1002.a7.s102502528;

public class Wizard extends Hero {
	// override
	Wizard() {
		name = "Wizard";
	}

	public float getHP() {
		return super.getHP() * 0.2f;
	}

	public float getMP() {
		return super.getMP() * 0.7f;
	}

	public float getPP() {
		return super.getPP() * 0.1f;
	}
}
