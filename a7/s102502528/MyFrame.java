package ce1002.a7.s102502528;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	Hero heros[] = new Hero[3];

	MyFrame() {
		heros[0] = new Wizard();        //new heros
		MyPanel panel = new MyPanel(heros[0], 5, 5, 400, 230);  
		this.add(panel);          //add them to frame
		heros[1] = new Swordsman();
		panel = new MyPanel(heros[1], 5, 240, 400, 230);
		this.add(panel);
		heros[2] = new Knight();
		panel = new MyPanel(heros[2], 5, 475, 400, 230);
		this.add(panel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
		setSize(430, 760);          //set size
		setLayout(null);            //set layout
		setLocationRelativeTo(null);//set location
		setVisible(true);           //set visible
	}

}
