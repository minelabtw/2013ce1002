package ce1002.a7.s102502554;

import javax.swing.*;

import java.awt.*;

public class StatusPanel extends JPanel{
	
	protected JLabel sa = new JLabel();
	protected JLabel sb = new JLabel();
	protected JLabel sc = new JLabel();
	protected JLabel sd = new JLabel();
	protected JLabel se = new JLabel();
	protected JLabel sf = new JLabel();//set 6 Label to save the status 
	
	StatusPanel (){
		setLayout(new GridLayout(3,2,0,0));
		add(sa);
		add(sb);
		add(sc);
		add(sd);
		add(se);
		add(sf);//use GridLayout to add 6 status 
	}
	
	public void getStatus(Hero hero){
		sa.setText("HP: ");
		sb.setText(""+hero.getHp());
		sc.setText("MP: ");
		sd.setText(""+hero.getMp());
		se.setText("PP: ");
		sf.setText(""+hero.getPp());//get the status from hero
	}
	

}
