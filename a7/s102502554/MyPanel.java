package ce1002.a7.s102502554;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	
	StatusPanel SP = new  StatusPanel();
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(new BorderLayout(5,10));
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		/*add label to panel*/
		add(label,BorderLayout.NORTH);
		add(imagelabel,BorderLayout.WEST);
		add(SP,BorderLayout.CENTER);//add the status panel near the image
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		SP.getStatus(hero);//set the status into StatusPanel
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
	}

}
