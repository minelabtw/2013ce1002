package ce1002.a7.s102502007;
import javax.swing.ImageIcon;
public class Hero {
	public Hero()
	{
		setHP(30);
		setMP(30);
		setPP(30);
	}
	private String heroName;
	protected float HP;
	protected float MP;
	protected float PP;
	protected ImageIcon image;
	
	public void setHeroName(String heroname)
	{
		this.heroName = heroname;
	}//setter
	public String getHeroName()
	{
		return heroName;
	}//getter
	public void setHP(float HP)
	{
		this.HP = HP;
	}//setter
	public float getHP()
	{
		return HP;
	}//getter
	public void setMP(float MP)
	{
		this.MP = MP;
	}//setter
	public float getMP()
	{
		return MP;
	}//getter
	public void setPP(float PP)
	{
		this.PP = PP;
	}//setter
	public float getPP()
	{
		return PP;
	}//getter
	public void setImage(String imageName)
	{
		this.image = new ImageIcon(imageName);
	}//setter
	public ImageIcon getImage()
	{
		return this.image;
	}//getter
	
}
