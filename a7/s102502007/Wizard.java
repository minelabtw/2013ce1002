package ce1002.a7.s102502007;

public class Wizard extends Hero {
	public Wizard()
	{
		setHeroName("Wizard");
		setImage("Wizard.jpg");
	}//constructor
	public float getHP()
	{
		return HP*0.2f;
	}//overriding
	public float getMP()
	{
		return MP*0.7f;
	}//overriding
	public float getPP()
	{
		return PP*0.1f;
	}//overriding

}
