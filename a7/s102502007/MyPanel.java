package ce1002.a7.s102502007;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel {
	protected StatusPanel panel1 ;
	public MyPanel()
	{
		setLayout(new BorderLayout());
		textLabel.setBounds(5,0,220,20);
		imageLabel.setBounds(5,15,240,180);
		
		add(textLabel,BorderLayout.NORTH);
		add(imageLabel,BorderLayout.WEST);
		panel1=new StatusPanel();
		
		add(panel1,BorderLayout.CENTER);
	}	
	protected JLabel imageLabel = new JLabel();
	protected JLabel textLabel = new JLabel();
	public void setRoleState(Hero hero)
	{
		panel1.setState(hero);
		imageLabel.setIcon(hero.getImage());
		textLabel.setText(hero.getHeroName() + " HP:" + hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP());
	}
	
}
