package ce1002.a7.s102502007;
import javax.swing.*;

import java.awt.*;
public class MyFrame extends JFrame {
	protected Hero[] hero;
	protected MyPanel p1 = new MyPanel();
	protected MyPanel p2 = new MyPanel();
	protected MyPanel p3 = new MyPanel();
	public MyFrame(Hero[] hero)
	{
		this.hero = hero;
		
		setLayout(null);
		setBounds(400 , 100 , 500 ,680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(hero[0] , p1 , 10 , 10);
		newRolePos(hero[1] , p2 , 10 , 220);
		newRolePos(hero[2] , p3 , 10 , 430);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x , y , 450 , 200);
		//set panel's position and size
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		//set panel's border
		panel.setRoleState(hero);
		//set label which in the panel
		add(panel);
	}
}
