package ce1002.a7.s102502007;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.*;
public class StatusPanel extends JPanel {
	public StatusPanel()
	{
		setLayout(new GridLayout(3 , 2 , 50 , 60));
		
		add(HP);
		add(HPState);
		add(MP);
		add(MPState);
		add(PP);
		add(PPState);
	}
	protected JLabel HP = new JLabel();
	protected JLabel MP = new JLabel();
	protected JLabel PP = new JLabel();
	protected JLabel HPState = new JLabel();
	protected JLabel MPState = new JLabel();
	protected JLabel PPState = new JLabel();
	
	public void setState(Hero hero)
	{
		HP.setText("HP");
		MP.setText("MP");
		PP.setText("PP");
		HPState.setText("" + hero.getHP());
		MPState.setText("" + hero.getPP());
		PPState.setText("" + hero.getPP());
	}
		
	

}
