package ce1002.a7.s102502007;

public class Knight extends Hero {
	public Knight()
	{
		setHeroName("Knight");
		setImage("Knight.jpg");
	}//constructor
	public float getHP()
	{
		return HP*0.8f;
	}//overriding
	public float getMP()
	{
		return MP*0.1f;
	}//overriding
	public float getPP()
	{
		return PP*0.1f;
	}//overriding

}
