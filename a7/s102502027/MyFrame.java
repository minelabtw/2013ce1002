package ce1002.a7.s102502027;

import javax.swing.*;

public class MyFrame extends JFrame {

	MyPanel pnl1 = new MyPanel(); //設變數
	MyPanel pnl2 = new MyPanel();
	MyPanel pnl3 = new MyPanel();
	
	MyFrame() {
		setLayout(null);
		newRolePos(new Wizard((float)30.0,(float)30.0,(float)30.0,"Wizard"),pnl1,5,0); //呼叫
		newRolePos(new Swordsman((float)30.0,(float)30.0,(float)30.0,"Swordsman"),pnl2,5,240);
		newRolePos(new Knight((float)30.0,(float)30.0,(float)30.0,"Knight"),pnl3,5,480);
		add(pnl1); //存入
		add(pnl2);
		add(pnl3);
		setSize(550,800);
		setDefaultCloseOperation(EXIT_ON_CLOSE);//關
		setVisible(true);//顯示
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {

		panel.setRoleState(hero); //呼叫
		panel.setBounds(x,y,360,215); //設位置
	}

}
