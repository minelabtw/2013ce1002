package ce1002.a7.s102502027;

public class Hero {

	public String name;
	public float HP;
	public float MP;
	public float PP;

	Hero(float HP, float MP, float PP, String name) {

		setname(name);
		sethp(HP);
		setpp(MP);
		setmp(PP);
	}

	public void setname(String name) {
		this.name = name;
	}

	public void sethp(float HP) { // ��l
		this.HP = HP;
	}

	public void setmp(float MP) {
		this.MP = MP;
	}

	public void setpp(float PP) {
		this.PP = PP;
	}

	public String name() { // get
		return name;
	}

	public float hp() {
		return HP;
	}

	public float mp() {
		return MP;
	}

	public float pp() {
		return PP;
	}
}
