package ce1002.a7.s102502027;

import javax.swing.*;
import java.awt.GridLayout;

public class StatusPanel extends JPanel {
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	JLabel label6 = new JLabel();

	StatusPanel(Hero hero) {
		setSize(200, 200);
		setLayout(new GridLayout(3, 2, 50, 50)); //�榡
		setState(hero); //�I�s
	}

	public void setState(Hero hero) {
		label1.setText("" + hero.hp()); //read
		label2.setText("" + hero.pp());
		label3.setText("" + hero.mp());
		label4.setText("HP");
		label5.setText("PP");
		label6.setText("MP");
		add(label4); // add
		add(label1);
		add(label5);
		add(label2);
		add(label6);
		add(label3);
	}

}
