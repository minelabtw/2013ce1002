package ce1002.a7.s102502027;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;

public class MyPanel extends JPanel {

	public void setRoleState(Hero hero) {

		JPanel Status;
		JLabel label1 = new JLabel(); // 設變數
		JLabel label2 = new JLabel();

		label1.setText(hero.name() + " HP:" + hero.hp() + " MP:" + hero.mp()
				+ " PP:" + hero.pp());
		label1.setPreferredSize(new Dimension(340, 10));

		ImageIcon icon = new ImageIcon(hero.name() + ".jpg"); // 讀取圖片
		label2.setIcon(icon);
		label2.setPreferredSize(new Dimension(240, 188));

		Status = new StatusPanel(hero);
		
		add(label1); // 存入
		add(label2);
		add(Status);
		Border Line = new LineBorder(Color.black,3);//black line
		setBorder(Line); 
		setVisible(true);

	}


}
