package ce1002.a7.s102502053;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.*;

public class StatusPanel extends JPanel{
	
	
	
	public StatusPanel()
	{
		
		setLayout(null);
		setBounds(250, 10, 145, 180);
		setVisible(true);
		setLayout(new GridLayout(3, 2, 0, 0)); //set GridLayout
		
	}
	

	
	 public void setState(Hero hero) 
	 {
		 	
			add(new JLabel("HP"));
			add(new JLabel(""+hero.getHP()));
			add(new JLabel("MP"));
			add(new JLabel(""+hero.getMP()));
			add(new JLabel("PP"));
			add(new JLabel(""+hero.getPP()));


	 }
	 

}
