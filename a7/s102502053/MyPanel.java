package ce1002.a7.s102502053;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.*;

public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel sl = new StatusPanel(); // new label for A7
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5, 0, 220, 20);
		imagelabel.setBounds(5, 15, 240, 180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(sl);
		
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
		sl.setState(hero);
		setVisible(true);
		
	}
	
	
}