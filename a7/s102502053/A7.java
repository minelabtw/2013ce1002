package ce1002.a7.s102502053;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//create object arrays
		Hero [] hero = new Hero[3];
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		//print out result
		for(int a = 0; a<3; a++)
		{
			System.out.print(hero[a].getName() + " HP: " + hero[a].getHP() + " MP: " + hero[a].getMP() + " PP: " + hero[a].getPP() + "\n");
		}
		
		MyFrame frame = new MyFrame(hero);
	}

}
