package ce1002.a7.s102502505;

public class Swordsman extends Hero{

	public Swordsman()
	{
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}
	
	public double getHp()
	{
		return super.hp*0.1;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.8;
	}

}
