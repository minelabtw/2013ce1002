package ce1002.a7.s102502505;

import javax.swing.*;

public class Hero {

	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;
	
	public Hero()
	{
		setHp(30);//給訂初始值30
		setMp(30);
		setPp(30);
	}
	
	public void setName(String n)//定義名稱
	{
		this.name=n;
	}
	public String getName()
	{
		return this.name;
	}
	public double getHp()
	{
		return this.hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return this.pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
	public void setImage(String imagename)//取得圖片
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()//回傳圖片
	{
		return this.image;
	}

}
