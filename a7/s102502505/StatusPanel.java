package ce1002.a7.s102502505;

import javax.swing.*;

public class StatusPanel extends JPanel{
	
	protected double hp;//定義所需的panel元素
	protected double mp;
	protected double pp;
	
	public void setState(Hero hero) {
		
		hp = hero.getHp();//先根據hero值去得取相對應的值
		mp = hero.getMp();
		pp = hero.getPp();
		
		add(new JLabel("HP"));//再創建按鈕
		add(new JLabel(""+hp));//用此方法即可創建也變數之label或button
		add(new JLabel("MP"));
		add(new JLabel(""+mp));
		add(new JLabel("PP"));
		add(new JLabel(""+pp));
		
	}
	


}
