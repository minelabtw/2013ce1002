package ce1002.a7.s102502505;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;

public class MyPanel extends JPanel{

	protected JLabel label = new JLabel();//定義第一個輸出label
	protected JLabel imagelabel = new JLabel();//定義第二個輸出邊框
	protected StatusPanel statuspanel = new StatusPanel();//定義第三個輸出邊框
	
	MyPanel()
	{
		setLayout(new BorderLayout(5,0));//定義label版面管理員的類型和規格(水平間距,垂直間距)
		label.setBounds(5,0,220,20);//設定label1初始位值及大小
		imagelabel.setBounds(5,15,240,180);//設定label2位置及大小
		statuspanel.setLayout(new GridLayout(3,2,5,5));//設定子panel的版面管理員和規格
		statuspanel.setSize(200,150);//設定子panel的大小

		add(label,BorderLayout.NORTH);//將label1放置北方
		add(imagelabel,BorderLayout.WEST);//放置西方
		add(statuspanel,BorderLayout.CENTER);//子panel放置中間
	}
	
	public void setRoleState(Hero hero)//定義method
	{
		imagelabel.setIcon(hero.getImage());//去根據hero值取得圖片
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		statuspanel.setState(hero);//去呼叫setState，根據hero，定義panel中需要用到的值
	}

}
