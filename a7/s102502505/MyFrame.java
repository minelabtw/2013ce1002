package ce1002.a7.s102502505;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{

	protected MyPanel panel1 = new MyPanel();//建立3個panel
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;//建立陣列
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		setLayout(null);
		setBounds(300 , 50 , 435 , 680);//改變frame的大小
		setVisible(true);//輸出frame
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(heros[0] , panel1 , 10 , 10);//分別呼叫三次newRolePos函式，並傳入不同陣列位置及panel初始位置
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)//定義method
	{
		panel.setBounds(x, y, 400 , 200);//定義初始位置及大小
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//建立邊框，並設顏色為黑色，粗細為3
		panel.setRoleState(hero);//呼叫在MyPanel.cpp中的method
		add(panel);//將panel加入frame中
	}

}
