package ce1002.a7.s102502505;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
				
				Hero[] heros = new Hero[3];//建立陣列
				
				heros[0] = new Wizard();//將矩陣設為Wizard.cpp
				heros[1] = new Swordsman();
				heros[2] = new Knight();
				
				for(Hero hero : heros){//輸出下列字串，並呼叫hero函式
					System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
				}
				
				MyFrame frame = new MyFrame(heros);//呼叫MyFrame檔案
	}
	
}
