package ce1002.a7.s100201023;

public class Knight extends Hero
{
	//override getter method
	public double getHP()
	{
		return super.getHP() * 0.8;
	}
	
	public double getMP()
	{
		return super.getMP() * 0.1;
	}
	
	public double getPP()
	{
		return super.getPP() * 0.1;
	}
	
}
