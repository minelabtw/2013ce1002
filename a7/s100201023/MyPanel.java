package ce1002.a7.s100201023;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	//properties
	private JLabel roletype;
	private JLabel roleimage;
	private StatusPanel rightstatus;
	
	//constructures
	public MyPanel()
	{
		roletype = new JLabel();
		rightstatus = new StatusPanel();
		setLayout(null);
	}
	
	//method
	public void setRoleState(Hero hero)
	{
		//set status text
		roletype.setText(hero.getname() + " HP: " + hero.getHP() + " MP: " + hero.getMP() + " PP: " + hero.getPP());
		roletype.setBounds(6, 6, 250, 20);
		add(roletype);
		
		//set image
		ImageIcon image;
		if(hero.getname() == "Wizard")
			image = new ImageIcon("Wizard.jpg");
		else if(hero.getname() == "Swordsman")
			image = new ImageIcon("Swordsman.jpg");
		else
			image = new ImageIcon("Knight.jpg");
		
		roleimage = new JLabel(image);
		roleimage.setBounds(6, 27, image.getIconWidth(), image.getIconHeight());
		add(roleimage);
		
		//set right status
		rightstatus.setState(hero);
		rightstatus.setBounds(16 + image.getIconWidth(), 27, 100, image.getIconHeight());
		add(rightstatus);
	}
}
