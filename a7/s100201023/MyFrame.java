package ce1002.a7.s100201023;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	//properties
	private MyPanel knight;
	private MyPanel swordsman;
	private MyPanel wizard;
	
	//constructures
	public MyFrame()
	{
		knight = new MyPanel();
		swordsman = new MyPanel();
		wizard = new MyPanel();
		
		setSize(800, 800);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	//method
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		 panel.setRoleState(hero);
		 panel.setBorder(BorderFactory.createLineBorder(Color.black, 5));
		 panel.setBounds(x, y, 400, 225);
		 add(panel);
		 
	}
	
	public MyPanel getpanel(Hero hero)
	{
		if(hero.getname() == "Wizard")
			return wizard;
		else if(hero.getname() == "Swordsman")
			return swordsman;
		else
			return knight;
	}
}
