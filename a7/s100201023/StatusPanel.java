package ce1002.a7.s100201023;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel
{
	//properties
	private JLabel hp;
	private JLabel mp;
	private JLabel pp;
	private JLabel hpvalue;
	private JLabel mpvalue;
	private JLabel ppvalue;
	
	//constructure
	public StatusPanel()
	{
		hp = new JLabel("HP");
		mp = new JLabel("MP");
		pp = new JLabel("PP");
		hpvalue = new JLabel();
		mpvalue = new JLabel();
		ppvalue = new JLabel();
		
		setLayout(new GridLayout(3, 2));
	}
	
	//method
	public void setState(Hero hero)
	{
		//set text
		hpvalue.setText("" + hero.getHP());
		mpvalue.setText("" + hero.getMP());
		ppvalue.setText("" + hero.getPP());
		
		//add component
		add(hp);
		add(hpvalue);
		add(mp);
		add(mpvalue);
		add(pp);
		add(ppvalue);
	}
}
