package ce1002.a7.s100201023;

public class Hero
{
	private String name;
	private double HP;
	private double MP;
	private double PP;
	
	public Hero()
	{
		HP = MP = PP = 30.0;
	}
	
	//setter method
	public void setname(String in)
	{
		name = in;
	}
	
	public void setHP(double in)
	{
		HP = in;
	}
	
	public void setMP(double in)
	{
		MP = in;
	}
	
	public void setPP(double in)
	{
		PP = in;
	}
	
	//getter method
	public String getname()
	{
		return name;
	}
	
	public double getHP()
	{
		return HP;
	}
	
	public double getMP()
	{
		return MP;
	}
	
	public double getPP()
	{
		return PP;
	}

}
