/**
 *  @name MyPanel.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/12/2014
 *  MyPanel inherit JPanel, and have two Labels, one is image and the other is its name.
 *
 */
package ce1002.a7.s100203020;
import javax.swing.*;
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel StaPan = new StatusPanel(); 
	
	public MyPanel(){
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		StaPan.setBounds(250,15,190,180);

		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(StaPan);

		}
	
	public void setRoleState(Hero hero){
		/* icon */
		imagelabel.setIcon(hero.getImage());
		/* point */
		label.setText(hero.getName() +" HP: " + hero.getHP()
				+ " MP: " + hero.getMP() + " PP: " + hero.getPP());
		StaPan.setState(hero);

	}
	
	
}
