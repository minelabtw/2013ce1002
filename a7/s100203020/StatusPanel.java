/**
 *  @name StatusPanel.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/12/2014
 *  StatusPanel inherit JPanel, and have six Labels.
 *
 */
package ce1002.a7.s100203020;
import java.awt.GridLayout;
import javax.swing.*;

public class StatusPanel extends JPanel{
	//Build six labels
	protected JLabel hpLb = new JLabel();
	protected JLabel hpLbvalue = new JLabel();
	protected JLabel mpLb = new JLabel();
	protected JLabel mpLbvalue = new JLabel();
	protected JLabel ppLb = new JLabel();
	protected JLabel ppLbvalue = new JLabel();

	StatusPanel(){
		setLayout(null);
		setLayout(new GridLayout(3, 2));
	
	}
	
	public void setState(Hero hero) {
		
		/* setting text of labels */
		String value;
		hpLb.setText("HP");
		mpLb.setText("MP");
		ppLb.setText("PP");
		value=""+hero.getHP();
		hpLbvalue.setText(value);
		value=""+hero.getMP();
		mpLbvalue.setText(value);
		value=""+hero.getPP();
		ppLbvalue.setText(value);

		
		/* add labels */
		add(hpLb);
		add(hpLbvalue);
		add(mpLb);
		add(mpLbvalue);
		add(ppLb);
		add(ppLbvalue);
	}
	  
	
}
