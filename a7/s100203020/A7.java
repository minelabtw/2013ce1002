/**
 *  @name A7.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/12/2014
 *  This program is to show the images of Wizard, Swordsman, and Knight.
 *  And show the value of Hero. 
 *
 */
package ce1002.a7.s100203020;

public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] heros = new Hero[3];
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		MyFrame frame = new MyFrame(heros);
	
		

	}

}
