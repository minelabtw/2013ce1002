/**
 *  @name Swordsman.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/12/2014
 *  This class is to build class Swordsman which has weight point of Hp, Mp, Pp, and inherit class Hero.
 *  And override getHP, getMp, getPP.
 */
package ce1002.a7.s100203020;

public class Swordsman extends Hero{
	/* weight point of Hp,Mp,Pp for Swordsman */
	protected float weightOfHP= (float) 0.1;
	protected float weightOfMP= (float) 0.1;
	protected float weightOfPP= (float) 0.8;
	
	/** Swordsman constructor*/
	public Swordsman(){
		super.setName("Swordsman");
		super.setImage(getName()+".jpg");
	}
	
	/** Overriding getHP, getMP, getPP */
	@Override
	public float getHP(){
		return super.getHP()*weightOfHP;
	}
	@Override
	public float getMP(){
		return super.getMP()*weightOfMP;
	}
	@Override
	public float getPP(){
		return super.getPP()*weightOfPP;		
	}
}



	