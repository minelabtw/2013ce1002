/**
 *  @name MyFrame.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/8/2014
 *  
 *  
 */
package ce1002.a7.s100203020;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;

public class MyFrame extends JFrame{
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected Hero[] heros;
	
	public MyFrame(Hero[] heros){
		 
		  this.heros = heros;
		  setLayout(null);

		  setBounds(200,20,485, 680);
		  setVisible(true);
		  setDefaultCloseOperation(EXIT_ON_CLOSE);
		 
		  newRolePos(heros[0], panel1, 10, 10);
		  newRolePos(heros[1], panel2, 10, 220);
		  newRolePos(heros[2], panel3, 10, 430);
		 
	}

	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		this.add(panel);
		//Border
		Border blackline;               
		blackline = BorderFactory.createLineBorder(Color.BLACK,3);
		panel.setBorder(blackline);
		
		panel.setBounds(x, y, 450, 200);//set bound
		panel.setRoleState(hero);
		add(panel);//add panel to frame
		
	}	
	
}
