/**
 *  @name Hero.java 
 *  @author jackyhobingo  <jackyhobingo@gmail.com>
 *  @date 4/12/2014
 *  This class is to build class Hero having name, HP, MP, PP, and image.
 *  
 */

package ce1002.a7.s100203020;
import javax.swing.ImageIcon;
public class Hero {
	
	protected String name; // name of hero
	/* initial HP, MP, PP are 30.0 */
	protected float HP = (float) 30.0;
	protected float MP = (float) 30.0;
	protected float PP = (float) 30.0;
	protected ImageIcon image;
	/** Setter and Getter   */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getHP() {
		return HP;
	}
	public void setHP(float hP) {
		HP = hP;
	}
	public float getMP() {
		return MP;
	}
	public void setMP(float mP) {
		MP = mP;
	}
	public float getPP() {
		return PP;
	}
	public void setPP(float pP) {
		PP = pP;
	}
	public ImageIcon getImage() {
		return image;
	}
	public void setImage(String  imagelocated) {
		this.image = new ImageIcon(imagelocated);
	}
}
