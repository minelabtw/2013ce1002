package ce1002.a7.s102502016;

import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel {

	public JLabel lb1;
	public JLabel lb2;
	public StatusPanel sp;

	public MyPanel() {
		this.setLayout(null); // 非預設排版
		this.setBounds(0, 0, 600, 200); // 邊界
		this.setSize(600, 200); // 大小
	}

	public void setRoleState(Hero hero) {
		ImageIcon icon = new ImageIcon(hero.getName() + ".jpg"); // 讀取圖片物件
		lb1 = new JLabel(icon); // LABEL 1 讀圖片 設置大小 位置
		lb1.setSize(260, 200);
		lb1.setLocation(0, 15);
		this.add(lb1); // 加到Panel�堶�
		lb2 = new JLabel(); // LABEL 2 放文字 設置大小位置
		lb2.setText(hero.getName() + " HP:" + hero.getHP() + " MP:"//文字內容
				+ hero.getMP() + " PP:" + hero.getPP());
		lb2.setSize(250, 10);
		lb2.setLocation(0, 0);
		this.add(lb2); // 加到Panel�堶�
		sp = new StatusPanel(hero);
		sp.setLocation(300,0);
		this.add(sp);
	}
}