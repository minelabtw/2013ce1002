package ce1002.a7.s102502016;
import ce1002.a7.s102502016.Hero;
public class A7 {

	public static void main(String[] args) {
	Hero hero[] = new Hero[3];
	hero[0] = new Wizard();
	hero[1] = new Swordsman();
	hero[2] = new Knight();
	
	hero[0].setName("Wizard");
	hero[1].setName("Swordsman");
	hero[2].setName("Knight");
	
	for (int i=0;i<=2;i++){
		hero[i].setHP();
		hero[i].setMP();
		hero[i].setPP();
		System.out.println(hero[i].getName()+" HP: "+hero[i].getHP()+" MP: "+hero[i].getMP()+" PP: "+hero[i].getPP());
	}
	MyFrame frame=new MyFrame(); //創GUI
	frame.newRolePos(hero[0], frame.panel1, 0, 0); //設置GUI(開始位置)
	frame.newRolePos(hero[1], frame.panel2, 0, 200);
	frame.newRolePos(hero[2], frame.panel3, 0, 400);
	}

}
