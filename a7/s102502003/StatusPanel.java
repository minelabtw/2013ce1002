package ce1002.a7.s102502003;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	
	JLabel label = new JLabel();
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	
	StatusPanel()
	{
		setLayout(new GridLayout(2,3));
		add(label);
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
	}
	
	public void setState(Hero hero) 
	{
		label.setText("HP");
		label1.setText(""+hero.getHp());
		label2.setText("MP");
		label3.setText(""+hero.getMp());
		label4.setText("PP");
		label5.setText(""+hero.getPp());
	}

}
