package ce1002.a7.s100204006;

public class Wizard extends Hero
{
	public Wizard(String Name, float HP, float MP, float PP) 
	{
		super("Wizard", HP, MP, PP);
		// TODO Auto-generated constructor stub
	}
	
	public float getHP()
	{
		return (float) (super.getHP()*0.2);
	}
	public float getMP()
	{
		return (float) (super.getMP()*0.7);
	}
	public float getPP()
	{
		return (float) (super.getPP()*0.1);
	}
}
