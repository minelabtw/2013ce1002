package ce1002.a7.s100204006;

public class Hero 
{
	private String Name;  //英雄名稱
	private float HP = (float) 30.0;     //生命點數
	private float MP = (float) 30.0;     //魔法點數
	private float PP = (float) 30.0;     //能力點數
	
	
	public Hero(String Name,float HP,float MP,float PP)
	{
		this.Name = Name;
		this.HP = HP;
		this.MP = MP;
		this.PP =PP;
	}
	
	//setting Name,HP,MP,PP
	public void setName(String Name)
	{
		this.Name = Name;
	}
	public void setHP(float HP)
	{
		this.HP = HP;
	}
	public void setMP(float MP)
	{
		this.MP = MP;
	}
	public void setPP(float PP)
	{
		this.PP = PP;
	}
	
	
	
	//getting Name,HP,MP,PP
	public String getName()
	{
		return this.Name;
	}
	public float getHP()
	{
		return this.HP;
	}
	public float getMP()
	{
		return this.MP;
	}
	public float getPP()
	{
		return this.PP;
	}
}
