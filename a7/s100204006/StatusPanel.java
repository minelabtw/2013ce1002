package ce1002.a7.s100204006;

import java.awt.*;

import javax.swing.*;

public class StatusPanel extends JPanel
{
	JLabel a1,a2,a3,a4,a5,a6;
	private Color preserveBackgroundColor;
	public StatusPanel(Hero hero) 
	{
		setRoleState(hero);
		setLayout(new GridLayout(3,2));
		setBounds(206,20,190,190);
		setBorder(BorderFactory.createLineBorder(preserveBackgroundColor));
	}
	private void setRoleState(Hero hero) 
	{
		
	}
	public void setState(Hero hero) 
	{
		
		a1 = new JLabel("HP");
	    a2 = new JLabel(String.valueOf(hero.getHP()));
	    a3 = new JLabel("MP");
	    a4 = new JLabel(String.valueOf(hero.getMP()));
	    a5 = new JLabel("PP");
	    a6 = new JLabel(String.valueOf(hero.getPP()));
	   

	    this.add(a1);
	    this.add(a2);
	    this.add(a3);
	    this.add(a4);
	    this.add(a5);
	    this.add(a6);
	}
}
