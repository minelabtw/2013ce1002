package ce1002.a7.s100204006;

import java.util.*;
import javax.swing.*;

public class A7 
{
	public static void main(String[] args )
	{
		Hero[] heros= new Hero [3];
				
		
		heros[0] = new Wizard("Wizard", (float) 30.0, (float) 30.0, (float) 30.0);
		System.out.println(heros[0].getName()+" HP: "+heros[0].getHP()+" MP: "+heros[0].getMP()+" PP: "+heros[0].getPP());
		
		heros[1] = new Swordsman("Swordsman", (float) 30.0, (float) 30.0, (float) 30.0);
		System.out.println(heros[1].getName()+" HP: "+heros[1].getHP()+" MP: "+heros[1].getMP()+" PP: "+heros[1].getPP());
		
		heros[2] = new Knight("Knight", (float) 30.0, (float) 30.0, (float) 30.0);
		System.out.println(heros[2].getName()+" HP: "+heros[2].getHP()+" MP: "+heros[2].getMP()+" PP: "+heros[2].getPP());
		
		JFrame frame = new MyFrame(heros);
		
		frame.setSize(450, 685);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle(null);
		frame.setVisible(true);
	}
}