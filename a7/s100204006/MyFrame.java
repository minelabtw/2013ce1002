package ce1002.a7.s100204006;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	 private Hero[] heros = new Hero[3];
	 private MyPanel panel1;
	 private MyPanel panel2;
	 private MyPanel panel3;
	 
	 public MyFrame(Hero[] heros) 
	 {
		 this.heros = heros;
		 
		 //JPanel panel = new JPanel();
		 this.setLayout(null);
		 
		 newRolePos(heros[0], panel1, 0, 0);
		 newRolePos(heros[1], panel2, 0, 215);
		 newRolePos(heros[2], panel3, 0, 428);
		 //add(new MyPanel());
	 }
		  
	 public void newRolePos(Hero hero, MyPanel panel, int x, int y) 
	 {
		 panel = new MyPanel(hero);
		 panel.setBounds(x, y, 406, 220);
		 this.add(panel);
	 } 
}
