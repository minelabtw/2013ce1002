package ce1002.a7.s100204006;

public class Knight extends Hero
{

	public Knight(String Name, float HP, float MP, float PP) 
	{
		super("Knight", HP, MP, PP);
		// TODO Auto-generated constructor stub
	}
	public float getHP()
	{
		return (float) (super.getHP() *0.8);
	}
	public float getMP()
	{
		return (float) (super.getMP()*0.1);
	}
	public float getPP()
	{
		return (float) (super.getPP()*0.1);
	}	 
}
