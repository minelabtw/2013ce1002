package ce1002.a7.s102502556;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	JLabel state = new JLabel(); //英雄狀態
	JLabel image = new JLabel(); //圖片資料
	StatusPanel status = new StatusPanel(); //狀態列表

	MyPanel () {
		state.setBounds(10, 5, 220, 20); //設定Label和Panel的預設位置和大小
		image.setBounds(10, 30, 240, 180);
		status.setBounds(260, 40, 100, 170);
		setLayout(null); //設定排版方式
	}
	//設定Label的屬性值
	public void setRoleState(Hero hero) {
		state.setText(hero.getId()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp()); //設定state的屬性
		image.setIcon(new ImageIcon(hero.getId()+".jpg"));  //設定image的屬性		
		status.setState(hero); //呼叫setState函數來設定status的屬性值
		this.add(state); //把設定好的Label和Panel增加到Panel裡
		this.add(image);
		this.add(status);
	}
}
