package ce1002.a7.s102502556;

public class A7 {

	public static void main(String[] args) {
		Hero[] character = new Hero[3]; //宣告一個名為character的Hero型態陣列
		character[0] = new Wizard(); //宣告character的第一個元素為Wizard型態的Object
		character[1] = new Swordsman(); //宣告character的第二個元素為Swordsman型態的Object
		character[2] = new Knight(); //宣告character的第三個元素為Knight型態的Object
		MyFrame frame = new MyFrame(character); //建立一個名為frame的MyFrame型態的Object
	}
}
