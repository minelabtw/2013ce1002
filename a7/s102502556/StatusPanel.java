package ce1002.a7.s102502556;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	JLabel Hp = new JLabel(); //宣告六個分別儲存不同內容的JLabel變數
	JLabel Mp = new JLabel();
	JLabel Pp = new JLabel(); 
	JLabel ValueOfHp = new JLabel();
	JLabel ValueOfMp = new JLabel(); 
	JLabel ValueOfPp = new JLabel();
	StatusPanel () {
		setLayout(new GridLayout(3,2,50,50)); //設定排版方式
	}
	public void setState(Hero hero) //使用setText來設定Label的屬性值
	{
		Hp.setText("HP");
		Mp.setText("MP");
		Pp.setText("PP");
		ValueOfHp.setText(""+hero.getHp());
		ValueOfMp.setText(""+hero.getMp());
		ValueOfPp.setText(""+hero.getPp());
		this.add(Hp); //把設定好的Label增加到Panel裡
		this.add(ValueOfHp);
		this.add(Mp);
		this.add(ValueOfMp);
		this.add(Pp);
		this.add(ValueOfPp);
	}
}
