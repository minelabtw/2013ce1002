package ce1002.a7.s102502556;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	protected MyPanel[] panel = new MyPanel[3]; //宣告一個名為panel的MyPanel型態的陣列
	
	MyFrame (Hero[] heros) {
		this.setSize(415, 770); //設定視窗大小
		this.setLocationRelativeTo(null); //設定視窗預設位置
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定按"X"會完整關閉視窗
		this.setLayout(null); //設定排版方式
		for ( int i = 0 ; i < 3 ; i++ ) //用for迴圈呼叫newRolePos函數來設定Panel裡的屬性值
		{
			panel[i] = new MyPanel();
			newRolePos(heros[i] , panel[i] , 10 , 10 + 240 * i);
		}
		this.setVisible(true); //使視窗可顯示在螢幕上
	}
	//設定Panel的屬性值
	public void newRolePos (Hero hero, MyPanel panel, int x, int y) {
		panel.setBounds(x, y, 380, 230); //設定預設位置和大小
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3)); //設定邊界
		panel.setRoleState(hero); //設定panel的屬性值
		this.add(panel); //將設定好的panel增加到frame裡
	}
}
