package ce1002.a7.s102502556;

public class Swordsman extends Hero {
	Swordsman () //constructor，並重新設定英雄名稱
	{
		setId("Swordsman");
	}
	float getHp () //回傳加權後的生命點數
	{	
		return super.getHp() * 0.1f;
	}
	float getMp () //回傳加權後的魔法點數
	{
		return super.getMp() * 0.1f;
	}
	float getPp () //回傳加權後的能力點數
	{
		return super.getPp() * 0.8f;
	}
}
