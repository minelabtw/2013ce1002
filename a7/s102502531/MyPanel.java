package ce1002.a7.s102502531;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;


public class MyPanel extends JPanel { //圖片左邊部分

		 
		     MyPanel(Hero hero) {
		    	 setRoleState(hero);
		     }
		 
		     public void setRoleState(Hero hero) {
		    	 setLayout(null);
		    	 JLabel jlabel_1 = new JLabel();
		    	 jlabel_1.setBounds(5,10,220,20);
		    	 
		    	 jlabel_1.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		         ImageIcon imageicon = new ImageIcon(hero.getName()+".jpg");
		         JLabel jlabel_2= new JLabel(imageicon);
		         jlabel_2.setBounds(5,30,240,180);
		         StatusPanel statuspanel_1 = new StatusPanel(hero);
		         statuspanel_1.setBounds(250,30,100,180);
		         add(jlabel_1);
		         add(jlabel_2);
		         
		         add(statuspanel_1);
		     }
		 
		 }

