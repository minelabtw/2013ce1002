package ce1002.a7.s102502531;


public class Knight extends Hero {

	public String getName() { // Knight hp mp pp
		return "Knight";
	}

	public float getHp() {
		return super.getHp() * 0.8f;
	}

	public float getMp() {
		return super.getMp() * 0.1f;
	}

	public float getPp() {
		return super.getPp() * 0.1f;
	}

}
