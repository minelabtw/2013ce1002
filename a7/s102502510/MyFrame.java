package ce1002.a7.s102502510;

import java.awt.Color;
import javax.swing.*;

public class MyFrame extends JFrame{
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected Hero[] heros;
	protected StatusPanel sp1=new StatusPanel();
	protected StatusPanel sp2=new StatusPanel();
	protected StatusPanel sp3=new StatusPanel();
	MyFrame(Hero[] h)
	{
		heros = h;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 500 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*
		 * set role position and image
		 */
		newstatus(h[0],sp1,310,10);
		newstatus(h[1],sp2,310,220);
		newstatus(h[2],sp3,310,430);
		newRolePos(h[0] , panel1 , 10 , 10);
		newRolePos(h[1] , panel2 , 10 , 220);
		newRolePos(h[2] , panel3 , 10 , 430);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 250 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
	public void newstatus(Hero h,StatusPanel sp,int x,int y)
	{
		sp.setBounds(x, y, 200, 200);
		sp.setState(h);
		add(sp);
	}
}
