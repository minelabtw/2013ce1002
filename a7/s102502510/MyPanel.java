package ce1002.a7.s102502510;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		
	}
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getimage());
		label.setText(hero.getname()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
	}
}