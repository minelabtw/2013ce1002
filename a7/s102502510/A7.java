package ce1002.a7.s102502510;



public class A7 {

	public static void main(String[] args) {
		Hero[] heros = new Hero[3];

		/*
		 * Initialized different hero with different index
		 */
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();

		// Create a new frame
		MyFrame frame = new MyFrame(heros);
	}

}
