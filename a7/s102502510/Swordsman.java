package ce1002.a7.s102502510;

public class Swordsman extends Hero{
	Swordsman()
	{
		setname("Swordsman");
		setimage("Swordsman.jpg");
	}
	public double getHP()
	{
		return HP*0.1;
	}
	public double getMP()
	{
		return MP*0.1;
	}
	public double getPP()
	{
		return PP*0.8;
	}
}