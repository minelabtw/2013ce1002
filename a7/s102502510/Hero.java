package ce1002.a7.s102502510;

import javax.swing.ImageIcon;

public class Hero {
	Hero()
	{
		setHP(30.0);
		setMP(30.0);
		setPP(30.0);
	}
	protected String name;
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon icon;
	//setter
	public void setname(String s)
	{
		name=s;
	}
	public void setHP(double x)
	{
		HP=x;
	}
	public void setMP(double x)
	{
		MP=x;
	}
	public void setPP(double x)
	{
		PP=x;
	}
	public void setimage(String image)
	{
		icon=new ImageIcon(image);
	}
	//getter
	public String getname()
	{
		return name;
	}
	public double getHP()
	{
		return HP;
	}
	public double getMP()
	{
		return MP;
	}
	public double getPP()
	{
		return PP;
	}
	public ImageIcon getimage()
	{
		return icon;
	}
}

