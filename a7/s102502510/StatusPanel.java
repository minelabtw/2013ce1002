package ce1002.a7.s102502510;

import java.awt.GridLayout;
import javax.swing.*;

public class StatusPanel extends JPanel{
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));
	}
	public void setState(Hero h) 
	{
		JLabel j1=new JLabel();
		JLabel j2=new JLabel();
		JLabel j3=new JLabel();
		JLabel j4=new JLabel();
		JLabel j5=new JLabel();
		JLabel j6=new JLabel();
		j1.setText("HP");
		j2.setText(""+h.getHP());
		j3.setText("MP");
		j4.setText(""+h.getMP());
		j5.setText("PP");
		j6.setText(""+h.getPP());
		add(j1);
		add(j2);
		add(j3);
		add(j4);
		add(j5);
		add(j6);
	}
}
