package ce1002.a7.s102502510;

public class Wizard extends Hero{
	Wizard()
	{
		setname("Wizard");
		setimage("Wizard.jpg");
	}
	public double getHP()
	{
		return HP*0.2;
	}
	public double getMP()
	{
		return MP*0.7;
	}
	public double getPP()
	{
		return PP*0.1;
	}
}