package ce1002.a7.s102502552;

import java.awt.Color;
import javax.swing.border.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	
	public MyFrame()
	{
		Hero hero[] = new Hero[3];//開始建立英雄物件
		MyPanel mypanel[] = new MyPanel[3];//建立面板物件
		
		setLayout(null);//排版
		
		for(int i = 0;i < 3;i++)
		{
			mypanel[i] = new MyPanel();
			
			switch(i)
			{
			case 0:
				hero [i] = new Wizard();
				break;//第一個創立巫師
			case 1:
				hero [i] = new Swordsman();
				break;//第二個創立劍士
			case 2:
				hero [i] = new Knight();
				break;//第三個創立騎士
			}
		}
		newRolePos(hero[0],mypanel[0],10,5);//把對應參數與圖片放入面板
		newRolePos(hero[1],mypanel[1],10,250);//把對應參數與圖片放入面板
		newRolePos(hero[2],mypanel[2],10,500);//把對應參數與圖片放入面板
		add(mypanel[0]);
		add(mypanel[1]);
		add(mypanel[2]);//把面板放入方框
	}
	
	public void newRolePos(Hero hero,MyPanel panel,int x,int y)
	{
		panel.setBorder(new LineBorder(Color.BLACK,2));
		panel.setLocation(x, y);//面板定位
		panel.setRoleState(hero);//英雄屬性
	}

}
