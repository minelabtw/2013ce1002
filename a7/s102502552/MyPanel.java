package ce1002.a7.s102502552;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	private String career;//職業名稱
	private float hp = 30;//血量
	private float mp = 30;//魔力
	private float pp = 30;//能力
	private ImageIcon image;//圖片
	
	public MyPanel()
	{
		setSize(400,210);//面板大小
	}

	public void setRoleState(Hero hero)
	{
	 career = hero.getCareer();//職業名稱
	 hp = hero.getHp();//血量
	 mp = hero.getMp();//魔力
	 pp = hero.getPp();//能力
	 image = hero.getimage();//圖片
	 
	 setLayout(null);
	 
	 JLabel abouthero = new JLabel(career + " HP: " + hp + " MP: " + mp + " PP: " + pp);//第一個標籤顯示屬性   
	 abouthero.setSize(250,10);//標籤大小
	 abouthero.setLocation(5, 5);
		add(abouthero);//將標籤加入面板
	
	 JLabel heroimage = new JLabel(image);//第二個標籤顯示圖片
		heroimage.setSize(250,190);
		heroimage.setLocation(5,15);
		add(heroimage);
		
	 StatusPanel newpanel = new StatusPanel();//第三個面板
		newpanel.setSize(120,180);
		newpanel.setLocation(265, 15);
		newpanel.setState(hero);//英雄資料匯入第三個面板
		add(newpanel);
	}
}
