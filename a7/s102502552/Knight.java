package ce1002.a7.s102502552;

import javax.swing.ImageIcon;

public class Knight extends Hero{
	public Knight()
	{
		super.setCareer("Knight");//職業名稱為騎士
	}//騎士建構子
	
	public float getHp()
	{
	    float hp = (float) (super.getHp() * 0.8);//取出原始血量點數并加權	
	    return hp;
	}//加權體力函式
	
	public float getMp()
	{
	    float mp = (float) (super.getMp() * 0.1);//取出原始魔力點數并加權	
	    return mp;
	}//加權魔力函式
	
	public float getPp()
	{
	    float pp = (float) (super.getPp() * 0.1);//取出原始能力點數并加權	
	    return pp;
	}//加權能力函式
	
	public ImageIcon getimage()
	{
		ImageIcon knight = new ImageIcon("knight.jpg");
		return knight;
	}//騎士圖片

}
