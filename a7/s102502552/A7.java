package ce1002.a7.s102502552;

import javax.swing.JFrame;

public class A7 {
	public static void main(String[] args) {
		
		MyFrame myframe = new MyFrame();//建立方框
		myframe.setSize(450, 800);//方框大小
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉按鈕
		myframe.setVisible(true);//可見
	}
}
