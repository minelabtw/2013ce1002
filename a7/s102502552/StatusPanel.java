package ce1002.a7.s102502552;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	private JLabel label1 = new JLabel("HP");
	private JLabel label2 = new JLabel();
	private JLabel label3 = new JLabel("MP");
	private JLabel label4 = new JLabel();
	private JLabel label5 = new JLabel("PP");
	private JLabel label6 = new JLabel();//六個標籤
	
	StatusPanel()
	{
	}
	
	public void setState(Hero hero)
	{
		
		label2.setText("" + hero.getHp());
		label4.setText("" + hero.getMp());
		label6.setText("" + hero.getPp());//獲取各項參數
		setLayout(new GridLayout(3,2,50,40));//進行表格排版
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);//添加標籤
			
	}
}
