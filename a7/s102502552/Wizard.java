package ce1002.a7.s102502552;

import javax.swing.ImageIcon;

public class Wizard extends Hero{
	
	public Wizard()
	{
		super.setCareer("Wizard");//職業名稱為巫師
	}//巫師建構子

	public float getHp()
	{
	    float hp = (float) (super.getHp() * 0.2);//取出原始血量點數并加權	
	    return hp;
	}//加權體力函式
	
	public float getMp()
	{
	    float mp = (float) (super.getMp() * 0.7);//取出原始魔力點數并加權	
	    return mp;
	}//加權魔力函式
	
	public float getPp()
	{
	    float pp = (float) (super.getPp() * 0.1);//取出原始能力點數并加權	
	    return pp;
	}//加權能力函式
	
	public ImageIcon getimage()
	{
		ImageIcon wizard = new ImageIcon("Wizard.jpg");
		return wizard;
	}//巫師圖片
}
