package ce1002.a7.s102502552;

import javax.swing.ImageIcon;

public class Hero {
	private String career;//職業名稱
	private float hp = 30;//血量（初始值30）
	private float mp = 30;//魔力（初始值30）
	private float pp = 30;//能力（初始值30）
	private ImageIcon image;
	
	public Hero()
	{
	}//英雄建構子
	
	public void setCareer(String career)
	{
		this.career = career;
	}//設定職業名稱
	
	public void setHp(float hp)
	{
	}//設定血量

	public void setMp(float mp)
	{
	}//設定魔力
	
	public void setPp(float pp)
	{
	}//設定能力
	
	public String getCareer()
	{
		return career;
	}//提取職業名稱
	
	public float getHp()
	{
		return hp;
	}//提取血量
	
	public float getMp()
	{
		return mp;
	}//提取魔力
	
	public float getPp()
	{
		return pp;
	}//提取能力
	
	public ImageIcon getimage()
	{
		return image;
	}//回傳對應圖片
}
