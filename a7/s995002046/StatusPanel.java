package ce1002.a7.s995002046;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
@SuppressWarnings("serial")
class StatusPanel extends JPanel{
	private List<JLabel>stLabs=new ArrayList<JLabel>();//status labels
	StatusPanel(){
		this.setLayout(new GridLayout(3, 2));//set GridLayout
		for(int i =0;i<6;i++){
			JLabel temp=new JLabel();
			stLabs.add(temp);//add to list
			this.add(temp);//add to panel

		}
	}
	public void setState(Hero hero) {//set status
		stLabs.get(0).setText("HP");
		stLabs.get(1).setText(""+hero.getHP());
		stLabs.get(2).setText("MP");
		stLabs.get(3).setText(""+hero.getMP());
		stLabs.get(4).setText("PP");
		stLabs.get(5).setText(""+hero.getPP());
	}
}