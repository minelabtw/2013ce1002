package ce1002.a7.s995002046;
import javax.swing.ImageIcon;
class Swordsman extends Hero{
	Swordsman(){
		setname("Swordsman");
		icon = new ImageIcon("Swordsman.jpg");//read picture
	}
	@Override
	public double getHP(){
		return HP*0.1;
	}
	@Override
	public double getMP(){
		return MP*0.1;
	}
	@Override
	public double getPP(){
		return PP*0.8;
	}
}