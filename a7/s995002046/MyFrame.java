package ce1002.a7.s995002046;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MyFrame extends JFrame{
	MyPanel s_panel=new MyPanel();
	MyPanel w_panel=new MyPanel();
	MyPanel k_panel=new MyPanel();
	MyFrame(){
        this.add(w_panel);//add panel to frame and set up panels
        newRolePos(new Wizard(), w_panel, 2, 0);
        this.add(s_panel);
        newRolePos(new Swordsman(), s_panel, 2, 230);
        this.add(k_panel);
        newRolePos(new Knight(), k_panel, 2, 460);
        this.setLayout(null);//set layout
		this.setSize(480,750); //set size
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		panel.setRoleState(hero);
		panel.setBounds(x, y, 450, 225);//set panel location and size
	}
}