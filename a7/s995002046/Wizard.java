package ce1002.a7.s995002046;
import javax.swing.ImageIcon;
class Wizard extends Hero{
	Wizard(){
		setname("Wizard");
		icon = new ImageIcon("Wizard.jpg");//read picture
	}
	@Override
	public double getHP(){
		return HP*0.2;
	}
	@Override
	public double getMP(){
		return MP*0.7;
	}
	@Override
	public double getPP(){
		return PP*0.1;
	}
}