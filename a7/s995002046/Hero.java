package ce1002.a7.s995002046;

import javax.swing.ImageIcon;

public class Hero{
	protected String name="Hero";
	protected ImageIcon icon;
	protected double HP=30.0;
	protected double MP=30.0;
	protected double PP=30.0;
	Hero(){
		
	}
	public String getname(){//getter
		return name;
	}
	public ImageIcon getImage(){
		return icon;
	}
	public double getHP(){
		return HP;
	}
	public double getMP(){
		return MP;
	}
	public double getPP(){
		return PP;
	}
	public void setname(String s){//setter
		name=s;
	}
	public void setHP(double i){
		HP=i;
	}
	public void setMP(double i){
		MP=i;
	}
	public void setPP(double i){
		PP=i;
	}
	
}