package ce1002.a7.s995002046;
import javax.swing.ImageIcon;
class Knight extends Hero{
	Knight(){
		setname("Knight");
		icon = new ImageIcon("Knight.jpg");//read picture
	}
	@Override
	public double getHP(){
		return HP*0.8;
	}
	@Override
	public double getMP(){
		return MP*0.1;
	}
	@Override
	public double getPP(){
		return PP*0.1;
	}
}