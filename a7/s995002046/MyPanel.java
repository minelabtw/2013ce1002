package ce1002.a7.s995002046;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	JLabel status=new JLabel();//status
	JLabel picture=new JLabel();//picture
	StatusPanel stPanel=new StatusPanel();
	MyPanel(){
		this.setLayout(new BorderLayout());//set BorderLayout add component and set their location
		this.add(status,BorderLayout.NORTH);
		this.add(picture,BorderLayout.WEST);
		this.add(stPanel,BorderLayout.CENTER);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));//set border
	}
	public void setRoleState(Hero hero){
		status.setText(hero.getname()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());//show status in label
		picture.setIcon(hero.getImage());
		stPanel.setState(hero);
	}
}