package ce1002.a7.s102502048;
import java.awt.*;
import javax.swing.*;

public class A7 
{
	public static void main(String[] args) 
	{		
		Hero[] heros = new Hero[3];// Initialized hero array with length of 3		 
		//Initialized different hero with different index
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();		
		//Use polymorphism to print out the result of different hero's name,hp,mp,pp
		for(Hero hero : heros)
		{
			System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
		}
		
		MyFrame frame = new MyFrame(heros);//Create a new frame
	}
}
