package ce1002.a7.s102502048;
import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel
{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel p1 = new StatusPanel();
	MyPanel()
	{		
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		p1.setBounds(255,10,180,180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(p1);
	}	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		p1.setlll(hero);		
	}
}
