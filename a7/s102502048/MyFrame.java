package ce1002.a7.s102502048;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame 
{
	/*Create six role panel*/
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 485 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
		/*set role position and image*/
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 ,  10 , 430);
	}	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 450 , 200);//set panel's position and size
		panel.setRoleState(hero);//set label which in the panel
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//set panel's border
		add(panel);//add panel to frame		
	}
}
