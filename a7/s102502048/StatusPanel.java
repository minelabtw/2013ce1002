package ce1002.a7.s102502048;
import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel
{
	public StatusPanel()
	{
		/*set panel's layout*/
		setLayout(new GridLayout(3,2));
	}
	
	public void setlll(Hero hero)
	{
		/*add label to panel*/
		add(new Label("HP:"));
		add(new Label(""+hero.getHp()));
		add(new Label("MP:"));
		add(new Label(""+hero.getMp()));
		add(new Label("PP:"));
		add(new Label(""+hero.getPp()));
	}
}
