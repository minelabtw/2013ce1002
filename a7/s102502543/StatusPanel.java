package ce1002.a7.s102502543;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel {

	StatusPanel() {
		setLayout(new GridLayout(3, 2, 10, 20));
	}

	public void setState(Hero hero) {
		JLabel HP = new JLabel("   HP");
		JLabel hp = new JLabel("" + hero.getHp());
		JLabel MP = new JLabel("   MP");
		JLabel mp = new JLabel("" + hero.getMp());
		JLabel PP = new JLabel("   PP");
		JLabel pp = new JLabel("" + hero.getPp());
		add(HP);
		add(hp);
		add(MP);
		add(mp);
		add(PP);
		add(pp);
		setVisible(true);
	}
}
