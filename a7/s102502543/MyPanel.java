package ce1002.a7.s102502543;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	JLabel ch = new JLabel();
	JLabel pic = new JLabel();
	StatusPanel SP = new StatusPanel();

	MyPanel() {
		setLayout(new BorderLayout());
	}

	public void setRoleState(Hero hero) {
		ImageIcon image = new ImageIcon(hero.getName() + ".jpg");
		ch.setText(hero.getName() + " HP: " + hero.getHp() + " MP: "
				+ hero.getMp() + " PP:" + hero.getPp());
		pic.setIcon(image);
		SP.setState(hero);
		this.add(ch, BorderLayout.NORTH); 
		this.add(pic, BorderLayout.WEST); 
		this.add(SP, BorderLayout.CENTER); 
		setVisible(true);
	}
}
