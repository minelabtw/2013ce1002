package ce1002.a7.s102502041;

public class Wizard  extends Hero{
	 
	public Wizard()
	{
		// setup the title for this character's name
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	
	/*
	 * This is the key part of polymorphism.
	 * We overrided the each point's getter
	 * and multiply it with weight from the table.
	 * */
	public double getHp()
	{
		return super.hp*0.2;
	}
	public double getMp()
	{
		return super.mp*0.7;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}