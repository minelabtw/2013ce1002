package ce1002.a7.s102502041;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	
	protected JLabel labelhp = new JLabel();
	protected JLabel labelhpv = new JLabel();
	protected JLabel labelmp = new JLabel();
	protected JLabel labelmpv = new JLabel();
	protected JLabel labelpp = new JLabel();
	protected JLabel labelppv = new JLabel();
	
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));
		add(labelhp);
		add(labelhpv);
		add(labelmp);
		add(labelmpv);
		add(labelpp);
		add(labelppv);
	}
	
	public void setState(Hero hero)
	{
		/*set label's text*/
		labelhp.setText("HP");
		labelhpv.setText(""+hero.getHp());
		labelmp.setText("MP");
		labelmpv.setText(""+hero.getMp());
		labelpp.setText("pp");
		labelppv.setText(""+hero.getPp());
	}
}
