package ce1002.a7.s102502041;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
 
public class MyFrame extends JFrame{
	/*Create three role panel*/
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected StatusPanel panel11 = new StatusPanel();
	protected StatusPanel panel12 = new StatusPanel();
	protected StatusPanel panel13 = new StatusPanel();
	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 285 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*
		 * set role position and image
		 */
		newPos(heros[0], panel11, 260, 10);
		newPos(heros[1], panel12, 260, 220);
		newPos(heros[2], panel13, 260, 430);
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);

	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 400 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
	public void newPos(Hero hero, StatusPanel panell, int x, int y)
	{
		panell.setBounds(x, y, 150, 200);
		panell.setState(hero);
		add(panell);
	}
}