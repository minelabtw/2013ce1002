package ce1002.a7.s102502518;

import javax.swing.ImageIcon;

public class Hero {
	protected String Name;
	protected float HP, MP, PP;
	protected ImageIcon image;

	public Hero() {
		HP = MP = PP = (float) 30; //set 30
	}

	//setter, getter
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public float getHP() {
		return HP;
	}

	public void setHP(float hP) {
		HP = hP;
	}

	public float getMP() {
		return MP;
	}

	public void setMP(float mP) {
		MP = mP;
	}

	public float getPP() {
		return PP;
	}

	public void setPP(float pP) {
		PP = pP;

	}

	public void setImage(String imagename) {
		this.image = new ImageIcon(imagename);
	}

	public ImageIcon getImage() {
		return this.image;
	}
}
