package ce1002.a7.s102502518;

public class Swordsman extends Hero {
	public String getName() { 
		return "Swordsman";
	}

	public Swordsman() {
		//setup the title for this character's name
		super.setName("Swordsman");
		super.setImage("Swordsman.jpg");
	}

	//use override
	@Override
	public float getHP() {
		return (float) (super.getHP() * 0.1);
	}

	public float getMP() {
		return (float) (super.getMP() * 0.1);
	}

	public float getPP() {
		return (float) (super.getPP() * 0.8);
	}
}
