package ce1002.a7.s102502518;

import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	StatusPanel() {
		setLayout(new GridLayout(3, 2)); //3*2
	}

	public void setState(Hero hero) {
		add(new Label("HP"));
		add(new Label("" + hero.getHP()));
		add(new Label("MP"));
		add(new Label("" + hero.getMP()));
		add(new Label("PP"));
		add(new Label("" + hero.getPP()));//add contain
		setVisible(true);//display
	}
}
