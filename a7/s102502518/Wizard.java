package ce1002.a7.s102502518;

public class Wizard extends Hero {
	public String getName() { 
		return "Wizard";
	}

	public Wizard() {
		//setup the title for this character's name
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}

	//use override
	@Override
	public float getHP() {
		return (float) (super.getHP() * 0.2);
	}

	public float getMP() {
		return (float) (super.getMP() * 0.7);
	}

	public float getPP() {
		return (float) (super.getPP() * 0.1);
	}
}

