package ce1002.a7.s102502503;
import javax.swing.ImageIcon;
 
public class Hero{
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;
	
	public Hero()
	{
		setHp(30);  //設定初始值
		setMp(30);
		setPp(30);
	}
	
	public void setName(String n)  //設定
	{
		this.name=n;
	}
	public String getName()  //取得
	{
		return this.name;
	}
	public double getHp()  //取得
	{
		return this.hp;
	}
	public void setHp(int hp) //設定
	{
		this.hp=hp;
	}
	public double getMp()  //取得
	{
		return this.mp;
	}
	public void setMp(int mp) //設定
	{
		this.mp=mp;
	}
	public double getPp()  //取得
	{
		return this.pp;
	}
	public void setPp(int pp) //設定
	{
		this.pp=pp;
	}
	public void setImage(String imagename) //設定
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()  //取得
	{
		return this.image;
	}
}