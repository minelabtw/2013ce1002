package ce1002.a7.s102502503;
import java.awt.GridLayout;
import javax.swing.*;
public class StatusPanel extends JPanel {
	protected JLabel labhp=new JLabel();  //宣告三個JLabel物件
	protected JLabel labmp=new JLabel();
	protected JLabel labpp=new JLabel();
	StatusPanel(){
		setLayout(new GridLayout(3, 2));  //設定版面配置
		add(new JLabel("HP"));  //將label加到statuspanel
		add(labhp);
		add(new JLabel("MP"));
		add(labmp);
	    add(new JLabel("PP"));
	    add(labpp);
	}
	public void setState(Hero hero) {
	    labhp.setText(hero.getHp()+"");  //設定字串
	    labmp.setText(hero.getMp()+"");
	    labpp.setText(hero.getPp()+"");
	}
}
