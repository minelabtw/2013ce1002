package ce1002.a7.s102502503;
 
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel status = new StatusPanel();
	
	MyPanel()
	{
		setLayout(null);
		label.setBounds(5,0,220,20);  //設定邊界
		imagelabel.setBounds(5,15,230,180);
		status.setBounds(250,5,100,180);
		add(label);  //將label加到panel
		add(imagelabel);
		add(status);
	}
	
	public void setRoleState(Hero hero)
	{
		status.setState(hero);  //呼叫函式
		imagelabel.setIcon(hero.getImage());  //載入圖片
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());  //設定字串
	}
}