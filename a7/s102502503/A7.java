package ce1002.a7.s102502503;
 
public class A7 {
 
	public static void main(String[] args) {
		
		Hero[] heros = new Hero[3];  //宣告陣列
		
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();

		MyFrame frame = new MyFrame(heros);  //建立MyFrame物件
		
	}
 
}