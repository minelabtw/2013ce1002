package ce1002.a7.s102502503;
 
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	protected MyPanel panel1 = new MyPanel();  //宣告三個Mypanel物件
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		setLayout(null);
		setBounds(300 , 50 , 400 , 680);
		setVisible(true);  //將視窗設為可見
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(heros[0] , panel1 , 10 , 10);  //呼叫函式
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		
		panel.setBounds(x, y, 365 , 200);  //設定panel邊界
		
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));  //黑色邊框
		
		panel.setRoleState(hero);
		
		add(panel);  //將panel加到frame
	}
}
