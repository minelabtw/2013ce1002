package ce1002.a7.s102502503;

public class Wizard  extends Hero{  //繼承Hero
	 
	public Wizard()
	{
		super.setName("Wizard");
		super.setImage("Wizard.jpg");
	}
	
	public double getHp()  //複寫
	{
		return super.hp*0.2;
	}
	public double getMp()
	{
		return super.mp*0.7;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}