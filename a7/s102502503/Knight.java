package ce1002.a7.s102502503;
 
public class Knight  extends Hero{ //繼承Hero
	
	public Knight()
	{	
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	public double getHp()  //複寫
	{
		return super.hp*0.8;
	}
	public double getMp()
	{
		return super.mp*0.1;
	}
	public double getPp()
	{
		return super.pp*0.1;
	}
}