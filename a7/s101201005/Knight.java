package ce1002.a7.s101201005;

public class Knight extends Hero{
	public Knight()
	{	
		// setup the title for this character's name
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	/*
	 * This is the key part of polymorphism.
	 * We overrided the each point's getter
	 * and multiply it with weight from the table.
	 * */
	public double getHP()
	{
		return super.HP*0.8;
	}
	public double getMP()
	{
		return super.MP*0.1;
	}
	public double getPP()
	{
		return super.PP*0.1;
	}
}
