package ce1002.a7.s101201005;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame {
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected StatusPanel panel4 = new StatusPanel();
	protected StatusPanel panel5 = new StatusPanel();
	protected StatusPanel panel6 = new StatusPanel();
	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 570 , 1360);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/*
		 * set role position and image
		 */
		newRolePos(heros[0] , panel1 , 10 , 10);
		setStatus(heros[0] ,panel4, 295, 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		setStatus(heros[1] ,panel5, 295, 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
		setStatus(heros[2] ,panel6, 295, 430);
		
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 250 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
	public void setStatus(Hero hero , StatusPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 250 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.set6Status(hero);
		/*set label which in the panel*/
		add(panel);
	}
}