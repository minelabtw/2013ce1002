package ce1002.a7.s101201005;

public class A7 {

	public static void main(String[] args) {
		// Initialized hero array with length of 3
		Hero[] heros = new Hero[3];
		
		/*
		 * Initialized different hero with different index
		 */
		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		/*
		 * Use polymorphism to print out the result of different 
		 * hero's name,hp,mp,pp
		 */
		for(Hero hero : heros){
			System.out.println( hero.getName() + " HP:"+ hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP() );
		}
		
		/*
		 * Create a new frame
		 */
		MyFrame frame = new MyFrame(heros);
	}

}
