package ce1002.a7.s101201005;
import javax.swing.*;
public class MyPanel extends JPanel {
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel StatusPanel = new StatusPanel();
	protected JLabel label1=new JLabel();
	protected JLabel label2=new JLabel();
	protected JLabel label3=new JLabel();
	protected JLabel label4=new JLabel();
	protected JLabel label5=new JLabel();
	protected JLabel label6=new JLabel();
	
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		add(StatusPanel);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
	}

}

