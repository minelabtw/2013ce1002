package ce1002.a7.s101201005;

import javax.swing.ImageIcon;

public class Hero {
	private String name;
	protected double HP=0;
	protected double MP=0;
	protected double PP=0;
	protected ImageIcon image;
	public Hero()
	{
		// we initialized these 3 variables value in hero's constructor
		// because it's called before it's child's constructor
		// so that we don't need to call it again in every children's constructor
		setHP(30);
		setMP(30);
		setPP(30);
	}
	public void setName(String name)//set name
	{
		this.name=name;
	}
	public String getName()//use get name
	{
		return name;
	}
	public void setHP(double HP)//set HP
	{
		this.HP=HP;
	}
	public double getHP()//return HP
	{
		return HP;
	}
	public void setMP(double MP)//set MP
	{
		this.MP=MP;
	}
	public double getMP()//return MP
	{
		return MP;
	}
	public void setPP(double PP)//set PP
	{
		this.PP=PP;
	}
	public double  getPP()//return PP
	{
		return PP;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}
