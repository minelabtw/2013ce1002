package ce1002.a7.s101201005;

import java.awt.*;

import javax.swing.*;

public class StatusPanel extends JPanel {
	private static final LayoutManager GridLayout = new GridLayout(3,2);
	protected JLabel label1=new JLabel();
	protected JLabel label2=new JLabel();
	protected JLabel label3=new JLabel();
	protected JLabel label4=new JLabel();
	protected JLabel label5=new JLabel();
	protected JLabel label6=new JLabel();

	StatusPanel()
	{
		setLayout(GridLayout);
		/*set panel's size and layout*/
		label1.setBounds(205,200,100,125);
		label2.setBounds(255,200,100,125);
		label3.setBounds(205,400,100,125);
		label4.setBounds(255,400,100,125);
		label5.setBounds(205,600,100,125);
		label6.setBounds(255,600,100,125);
		/*add label to panel*/
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);
		//add(StatusPanel);
	}

	public void set6Status(Hero hero)
	{
		this.label1.setText("HP");
		//this.label1.setText(" HP: "+hero.getHP());
		this.label2.setText(hero.getHP()+"");
		//this.label2.setText(" HP: "+hero.getHP());
		this.label3.setText("MP");
		//this.label3.setText(" MP: "+hero.getMP());
		this.label4.setText(hero.getMP()+"");
		//this.label4.setText(" MP: "+hero.getMP());
		this.label5.setText("PP");
		//this.label5.setText(" PP: "+hero.getPP());
		this.label6.setText(hero.getPP()+"");
		//this.label6.setText(" PP: "+hero.getPP());
	}
}
