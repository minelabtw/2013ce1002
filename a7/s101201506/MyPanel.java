package ce1002.a7.s101201506;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel panel=new StatusPanel();
	
	MyPanel()
	{
	
		setLayout(null);
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
		panel.setBounds(255,5,180,190);
		
		add(label);
		add(imagelabel);
		add(panel);
	}
	
	public void setRoleState(Hero hero)
	{
	
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		panel.setState(hero);
	}
}