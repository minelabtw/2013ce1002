package ce1002.a7.s101201506;

public class Knight extends Hero {

	public Knight() {

		setName("Knight");
		setImage("src\\ce1002\\a7\\s101201506\\Knight.jpg");
	}

	public double getHp() {//get hp mp pp  
		return super.hp * 0.8;
	}

	public double getMp() {
		return super.mp * 0.1;
	}

	public double getPp() {
		return super.pp * 0.1;
	}
}