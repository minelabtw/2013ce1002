package ce1002.a7.s101201506;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame {
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();

	protected Hero[] heros;
	protected MyPanel panel[]=new MyPanel[3];

	MyFrame(Hero[] heros) { //建構 hero
		this.heros = heros;
		
		for (int i=0;i<3;i++)
		{
			panel[i]=new MyPanel();
		}
		//顯示出的格式
		setLayout(null);
		setBounds(300, 50, 285, 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		for (int i=0;i<3;i++)
		{
			newRolePos(heros[i],panel[i],10,210*i+10);
		}
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setBounds(x,y,250,200	);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		add(panel);
	}

}
