package ce1002.a7.s101201506;

import javax.swing.ImageIcon;


public class Hero{
	protected String name;
	protected double hp;
	protected double mp;
	protected double pp;
	protected ImageIcon image;
	
	public Hero()
	{
		
		setHp(30);
		setMp(30);
		setPp(30);
	}
	

	public void setName(String n) //set name 
	{
		this.name=n;
	}
	public String getName() //get name  以下以此類推
	{
		return this.name;
	}
	public double getHp()
	{
		return this.hp;
	}
	public void setHp(int hp)
	{
		this.hp=hp;
	}
	public double getMp()
	{
		return this.mp;
	}
	public void setMp(int mp)
	{
		this.mp=mp;
	}
	public double getPp()
	{
		return this.pp;
	}
	public void setPp(int pp)
	{
		this.pp=pp;
	}
	
	public void setImage(String image_address)
	{
		image=new ImageIcon(image_address);
	}
	public ImageIcon getImage()
	{
		return image;
	}
}