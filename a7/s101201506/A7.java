package ce1002.a7.s101201506;
import javax.swing.JFrame;
public class A7 {
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Initialized hero array with length of 3
		Hero[] heros = new Hero[3]; 

		heros[0] = new Wizard();
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		
		MyFrame frame = new MyFrame(heros);
		
	}
 
}