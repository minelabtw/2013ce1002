package ce1002.a7.s101201506;

public class Swordsman extends Hero {

	public Swordsman() {

		setName("Swordsman");
		setImage("src\\ce1002\\a7\\s101201506\\Swordsman.jpg");
	}

	public double getHp() { //get hp mp pp  
		return super.hp * 0.1;
	}

	public double getMp() { 
		return super.mp * 0.1;
	}

	public double getPp() {
		return super.pp * 0.8;
	}
}