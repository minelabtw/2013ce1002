package ce1002.a7.s101201506;

public class Wizard extends Hero {

	public Wizard() {
		// setup the title for this character's name
		setName("Wizard");
		setImage("src\\ce1002\\a7\\s101201506\\Wizard.jpg");
	}

	public double getHp() // get hp mp pp
	{
		return super.hp * 0.2;
	}

	public double getMp() {
		return super.mp * 0.7;
	}

	public double getPp() {
		return super.pp * 0.1;
	}
}
