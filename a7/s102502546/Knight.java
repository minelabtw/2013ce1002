package ce1002.a5.s102502546;

public class Knight extends Hero {
	Knight() {
		setName("Knight");
	}// 建構子鎖死自己的Name
		// 下面即是多型 讓每項回傳屬於自己的加權數值

	public double getHP() {
		return HP * 0.8;
	}

	public double getMP() {
		return MP * 0.1;
	}

	public double getPP() {
		return PP * 0.1;
	}

}
