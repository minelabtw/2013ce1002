package ce1002.a5.s102502546;

public class Hero {
	private String name;// protected 型態 可以讓繼承的使用
	protected double HP;
	protected double MP;
	protected double PP;
//set+get Name HP MP PP 
	public void setName(String name) {
		this.name = name;
	}

	public void setHP(double HP) {
		this.HP = 30.0;
	}

	public void setMP(double MP) {
		this.MP = 30.0;
	}

	public void setPP(double PP) {
		this.PP = 30.0;
	}

	public String getName() {
		return name;
	}

	public double getHP() {
		return HP;
	}

	public double getMP() {
		return MP;
	}

	public double getPP() {
		return PP;
	}

}
