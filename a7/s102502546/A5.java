package ce1002.a5.s102502546;

public class A5 {

	public static void main(String[] args) {
		Hero hero[] = new Hero[3];//令hero[]大小為3
		hero[0] = new Wizard();//依序確定編號
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		for (int i = 0; i < 3; i++) {//輸出所需
			System.out.println(hero[i].getName() + " HP: " + hero[i].getHP()
					+ " MP: " + hero[i].getMP() + " PP: " + hero[i].getPP());
		}
	}

}
