package ce1002.a7.s102502520;

public class Knight extends Hero{
	
	public Knight()
	{	
		super.setName("Knight");
		super.setImage("Knight.jpg");
	}
	
	public double getHP()
	{
		return super.HP*0.8;
	}
	public double getMP()
	{
		return super.MP*0.1;
	}
	public double getPP()
	{
		return super.PP*0.1;
	}
}