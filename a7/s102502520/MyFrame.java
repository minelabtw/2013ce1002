package ce1002.a7.s102502520;
import java.awt.*;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
 
public class MyFrame extends JFrame{

	protected MyPanel panel1 = new MyPanel();//define panel
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected StatusPanel StatusPanel = new StatusPanel();//define StatusPanel
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;//read heros
		
		setLayout(null);//setting
		setBounds(300 , 50 , 500 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		newRolePos(heros[0] , panel1 , 15 , 10);//call newRolePos
		newRolePos(heros[1] , panel2 , 15 , 220);
		newRolePos(heros[2] , panel3 , 15 , 430);
	}

	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 400 , 200);
		panel.setBorder(BorderFactory.createLineBorder(Color.black,3));
		panel.setRoleState(hero);//call setRoleState
		add(panel);
	}
}