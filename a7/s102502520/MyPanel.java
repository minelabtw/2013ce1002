package ce1002.a7.s102502520;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.*; 
public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();//define
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel Status = new StatusPanel();

	MyPanel()
	{
		setLayout(null);//setting
		label.setBounds(5,0,250,20);
		imagelabel.setBounds(5,15,240,180);
		Status.setBounds(280,15,90,150);

		add(label);//add
		add(imagelabel);
		add(Status);
	}
	
	public void setRoleState(Hero hero)
	{
		imagelabel.setIcon(hero.getImage());//read image
		label.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());//read String
		Status.setState(hero);
	}
}