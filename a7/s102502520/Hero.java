package ce1002.a7.s102502520;
import javax.swing.ImageIcon;
public class Hero{
	protected String name;
	protected double HP;
	protected double MP;
	protected double PP;
	protected ImageIcon image;
	
	public Hero()
	{
		setHP(30);
		setMP(30);
		setPP(30);
	}
	
	public void setName(String n)
	{
		this.name=n;
	}
	public String getName()
	{
		return this.name;
	}
	public double getHP()
	{
		return this.HP;
	}
	public void setHP(double HP)
	{
		this.HP=HP;
	}
	public double getMP()
	{
		return this.MP;
	}
	public void setMP(double MP)
	{
		this.MP=MP;
	}
	public double getPP()
	{
		return this.PP;
	}
	public void setPP(double PP)
	{
		this.PP=PP;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}
}