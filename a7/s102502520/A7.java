package ce1002.a7.s102502520;

public class A7 {

	public static void main(String[] args) {
		Hero[] heros = new Hero[3];//define array
		
		heros[0] = new Wizard();//read
		heros[1] = new Swordsman();
		heros[2] = new Knight();
		
		for(Hero hero : heros){//output
			System.out.println( hero.getName() + " HP:"+ hero.getHP() + " MP:" + hero.getMP() + " PP:" + hero.getPP() );
		}
		MyFrame frame = new MyFrame(heros);//output frame
	}
}
