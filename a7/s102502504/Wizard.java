package ce1002.a7.s102502504;

public class Wizard extends Hero
{
	public Wizard(String heroname,double hp,double mp,double pp)
	{
		setHeroname(heroname); //直接繼承
		setImageIcon("Wizard.jpg");
		setHp(hp);
		setMp(mp);
		setPp(pp);
	}
	
	public double getHp() //修改父函式的內容-->覆寫overriding
	{
		return hp*0.2;
	}
	
	public double getMp() //overriding
	{
		return mp*0.7;
	}
	
	public double getPp() //overriding
	{
		return pp*0.1;
	}
}
