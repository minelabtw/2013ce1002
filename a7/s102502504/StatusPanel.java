package ce1002.a7.s102502504;

import java.awt.*;
import javax.swing.*;

public class StatusPanel extends JPanel
{
	Hero [] heroes;
	protected JLabel hp = new JLabel(); //宣告三個JLabel物件
	protected JLabel mp = new JLabel();
	protected JLabel pp = new JLabel();
	
	
	public StatusPanel()
	{
		setLayout(new GridLayout(3,2)); //三列兩行的版面配置
		add(new JLabel("HP"));  //將label加到statuspanel
		add(hp);
		add(new JLabel("MP"));
		add(mp);
	    add(new JLabel("PP"));
	    add(pp);
	}
	
	public void setState(Hero hero) 
	{
		hp.setText(" "+ hero.getHp()); //強制變成字串
		mp.setText(" "+ hero.getMp());
		pp.setText(" "+ hero.getPp());
		
	}
}
