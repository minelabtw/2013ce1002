package ce1002.a7.s102502504;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel sp  = new StatusPanel();
	
	public MyPanel()
	{
		setLayout(null); //為何不可以打setLayout(new GridLayout(3,1))
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,20,240,170);
		sp.setBounds(250,5,100,180);
		add(label); //將label加到panel
		add(imagelabel); //把圖片加到panel
		add(sp); //把status加到panel
		
	}
	
	public void setRoleState(Hero hero)
	{
		sp.setState(hero); 
		imagelabel.setIcon(hero.getImage()); //載入圖片
		label.setText(hero.getHeroname()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp()); //設定字串
	}
	
}
