package ce1002.a7.s102502504;

import javax.swing.*;

public class Hero 
{
	protected String heroname; //英雄名稱
	protected double hp; //生命點數
	protected double mp; //魔法點數
	protected double pp; //能力點數
	
	protected ImageIcon image;
	
	//四個變數對應的Getter與Setter
	public String getHeroname() {
		return heroname;
	}
	public void setHeroname(String heroname) {
		this.heroname = heroname;
	}
	
	public double getHp() {
		return hp;
	}
	public void setHp(double hp) {
		this.hp = hp;
	}
	
	public double getMp() {
		return mp;
	}
	public void setMp(double mp) {
		this.mp = mp;
	}
	
	public double getPp() {
		return pp;
	}
	public void setPp(double pp) {
		this.pp = pp;
	}

	public ImageIcon getImage() {
		return image;
	}
	public void setImageIcon(String imagename) {
		this.image = new ImageIcon(imagename); //新增icon
	}
}
