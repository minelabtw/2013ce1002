package ce1002.a7.s102502504;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame
{
	protected MyPanel p1 = new MyPanel(); //3個 MyPanel 變數存放3個角色用
	protected MyPanel p2 = new MyPanel();
	protected MyPanel p3 = new MyPanel();
	
	protected Hero [] heroes;
	
	public MyFrame(Hero[] heroes)
	{
		this.heroes = heroes;
		
		setLayout(null);
		setBounds(0,0,400,680);
		setVisible(true); //設為可見
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null); //置中
		
		newRolePos(heroes[0] , p1 , 10 , 10);
		newRolePos(heroes[1] , p2 , 10 , 220);
		newRolePos(heroes[2] , p3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel p , int x , int y)
	{
		p.setBounds(x,y,370,200); //設定panel邊界
		p.setBorder(BorderFactory.createLineBorder(Color.BLACK,3)); //黑色邊框
		p.setRoleState(hero);
		add(p); //將panel加到frame
	}
	
}
