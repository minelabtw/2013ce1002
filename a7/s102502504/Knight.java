package ce1002.a7.s102502504;

public class Knight extends Hero
{
	public Knight(String heroname,double hp,double mp,double pp)
	{
		setHeroname(heroname); //直接繼承
		setImageIcon("Knight.jpg");
		setHp(hp);
		setMp(mp);
		setPp(pp);
	}
	
	public double getHp() //overriding
	{
		return hp*0.8;
	}
	
	public double getMp() //overriding
	{
		return mp*0.1;
	}
	
	public double getPp() //overriding
	{
		return pp*0.1;
	}
}
