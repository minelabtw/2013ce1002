package ce1002.a7.s102502549;

public class Hero {
	
	private String Name;
	private float HP;
	private float MP;
	private float PP;

	//初始三能力為30
	public Hero() {
		setHP(30);
		setMP(30);
		setPP(30);
	}

	// 四變數getter
	public String getName() {
		return Name;
	}

	public float getHP() {
		return HP;
	}

	public float getMP() {
		return MP;
	}

	public float getPP() {
		return PP;
	}

	// 四變數setter
	public void setName(String Name) {
		this.Name = Name;
	}

	public void setHP(float HP) {
		this.HP = HP;
	}

	public void setMP(float MP) {
		this.MP = MP;
	}

	public void setPP(float PP) {
		this.PP = PP;
	}
}
