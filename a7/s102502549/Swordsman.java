package ce1002.a7.s102502549;

public class Swordsman extends Hero{
	
	//初始名稱為Swordsman
	public Swordsman()
	{
		super.setName("Swordsman");
	}
	
	//覆寫getter
	public float getHP() {
		return super.getHP()*0.1f;
	}

	public float getMP() {
		return super.getMP()*0.1f;
	}

	public float getPP() {
		return super.getPP()*0.8f;
	}

}
