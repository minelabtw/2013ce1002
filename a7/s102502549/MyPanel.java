package ce1002.a7.s102502549;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {

	// 兩個label，一個狀態，一個圖片
	private JLabel lable1 = new JLabel();
	private JLabel lable2 = new JLabel();

	public void setRoleState(Hero hero) {
		// 設定邊界，設定label，加入label
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK, 3));

		lable1.setText(hero.getName() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP: " + hero.getPP());
		lable1.setBounds(5, 0, 250, 20);
		lable2.setIcon(new ImageIcon(hero.getName() + ".jpg"));
		lable2.setBounds(5, 17, 250, 190);

		add(lable1);
		add(lable2);
		// -------------------------------------以上為a6內容，以下為a7

		StatusPanel sp = new StatusPanel(hero);
		sp.setBounds(265, 20, 150, 170);
		add(sp);
	}
}
