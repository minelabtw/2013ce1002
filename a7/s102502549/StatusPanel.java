package ce1002.a7.s102502549;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {

	private JLabel[] lb = new JLabel[6];

	public StatusPanel(Hero hero) {
		setLayout(new GridLayout(3, 2));
		setState(hero);

		for (int i = 0; i < 6; i++) {
			add(lb[i]);// 將六個lable加入板板
		}
	}

	public void setState(Hero hero) {
		// 設定六個lable的內容
		lb[0] = new JLabel("HP");
		lb[1] = new JLabel("" + hero.getHP());
		lb[2] = new JLabel("MP");
		lb[3] = new JLabel("" + hero.getMP());
		lb[4] = new JLabel("PP");
		lb[5] = new JLabel("" + hero.getPP());
	}
}
