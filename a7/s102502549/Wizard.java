package ce1002.a7.s102502549;

public class Wizard extends Hero{
	
	//初始名稱為Wizard
	public Wizard()
	{
		super.setName("Wizard");
	}
	
	//覆寫getter
	public float getHP() {
		return super.getHP()*0.2f;
	}

	public float getMP() {
		return super.getMP()*0.7f;
	}

	public float getPP() {
		return super.getPP()*0.1f;
	}

}
