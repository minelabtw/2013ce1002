package ce1002.a7.s102502028;
import javax.swing.* ;
import java.awt.* ;
import java.awt.GridLayout ;
public class StatusPanel extends JPanel {
	
	JLabel label = new JLabel() ;  //建立6個標籤
	JLabel label2 = new JLabel() ;
	JLabel label3 = new JLabel() ;
	JLabel label4 = new JLabel() ;
	JLabel label5 = new JLabel() ;
	JLabel label6 = new JLabel() ;
		
	StatusPanel() //建構子設定標籤大小及位置及排版
	{
		setBounds(300,30,110,180) ;
		setLayout(new GridLayout(3,2,55,55)) ;
	}
	 
	public void setState(Hero hero) {	  //修改標籤並加到版面中
		 label.setText("HP");
		 label2.setText(""+hero.getHP());
		 label3.setText("MP");
		 label4.setText(""+hero.getMP());
		 label5.setText("PP");
		 label6.setText(""+hero.getPP());
		 add(label) ;
		 add(label2) ;
		 add(label3) ;
		 add(label4) ;
		 add(label5) ;
		 add(label6) ;
	 }
}
