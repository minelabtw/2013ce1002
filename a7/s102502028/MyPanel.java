package ce1002.a7.s102502028;
import javax.swing.* ;
import javax.swing.border.LineBorder;
import java.awt.* ;
public class MyPanel extends JPanel{
	JLabel state = new JLabel() ;
	JLabel image = new JLabel() ;
	StatusPanel status = new StatusPanel() ;
	
	MyPanel()  //設定版面
	{
		setSize(430,225) ;
		setBorder(new LineBorder(Color.black, 5)) ;
		setLayout(null) ;
	}
	
	public void setRoleState(Hero hero){ //設定屬性還有圖片
		//new MyPanel();
		state = new JLabel(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP()) ;
		state.setBounds(10,5,250,20) ;
		ImageIcon image2 = new ImageIcon(hero.getName()+".jpg") ;
		image = new JLabel(image2) ;
		image.setBounds(10,25,250,190) ;
		add(state) ; //加到版面中
		add(image) ;
		status.setState(hero) ;
		add(status) ;
	}
}
