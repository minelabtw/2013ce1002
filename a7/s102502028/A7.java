package ce1002.a7.s102502028;
import javax.swing.*;
import java.awt.* ;
public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Hero[] hero = new Hero[3] ; //宣告物件陣列
        hero[0] = new Wizard() ;  //初始化物件陣列
        hero[1] = new Swordsman() ;
        hero[2] = new Knight() ;
        
        MyFrame frame = new MyFrame() ;//設定框架
        frame.setSize(530,810) ;
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;	
		frame.setLayout(null);
        MyPanel[] panel = new MyPanel[3] ; //3個 MyPanel 變數存放3個角色用
        
        for (int a = 0 ; a < 3 ; a++)  //印出結果
        {
        	panel[a] =  new MyPanel();
        	frame.newRolePos(hero[a],panel[a],10,10+230*a) ;  
        	//System.out.println(hero[a].getName()+" HP: "+hero[a].getHP()+" MP: "+hero[a].getMP()+" PP: "+hero[a].getPP()) ;
        }
        
        frame.setVisible(true);
	}

}
