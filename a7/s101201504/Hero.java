package ce1002.a7.s101201504;

import javax.swing.ImageIcon;
public class Hero {
	private String id;
	protected double hp=0;
	protected double mp=0;
	protected double pp=0;
	protected ImageIcon image;
	public Hero()
	{
		sethp(30);
		setmp(30);
		setpp(30);
	}
	public void setid(String id){    //setup name
		this.id=id;
	}
	public void sethp(double hp){              //setup hp
		this.hp=hp;
	}
	public void setmp(double mp){     //setup mp
		this.mp=mp;
		}
	public void setpp(double pp){      //setup pp
		this.pp=pp;
	}
	public String getid(){
		return id ;
	}
	public double gethp(){
		return hp;
	}
	public double getmp(){
		return mp;
	}
	public double getpp(){
		return pp;
	}
	public void setimage(String imageaddress)//input the image by the address
	{
		image=new ImageIcon(imageaddress);
	}
	public ImageIcon getimage()//get the image
	{
		return image;
	}

}
