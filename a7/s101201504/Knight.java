package ce1002.a7.s101201504;

public class Knight  extends Hero {
	public Knight()
	{
		//input the data 
		setid("Knight");
		setimage("src\\ce1002\\a7\\s101201504\\Knight.jpg");
	}
	public double  gethp()
	{
		return hp*0.8;
	}
	public double  getmp()
	{
		return mp*0.1;
	}
	public double  getpp()
	{
		return pp*0.1;
	}

}
