package ce1002.a7.s101201504;

public class Swordsman extends Hero  {

	public Swordsman()
	{
		//input the data 
		setid("Swordsman");
		setimage("src\\ce1002\\a7\\s101201504\\Swordsman.jpg");
	}
	public double  gethp()
	{
		return hp*0.1;
	}
	public double  getmp()
	{
		return mp*0.1;
	}
	public double  getpp()
	{
		return pp*0.8;
	}
}
