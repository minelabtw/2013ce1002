package ce1002.a7.s101201504;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
public class MyPanel  extends JPanel {
	protected JLabel label1=new JLabel();
	protected JLabel label2=new JLabel();
	protected StatusPanel panel=new StatusPanel();
	MyPanel()
	{
		setLayout(null);
		//set the location and the size in the panel
		label1.setBounds(5,0,220,20);
		label2.setBounds(5,15,240,180);
		panel.setBounds(255,5,180,190);
		//input to the panel
		add(label1);
		add(label2);
		add(panel);
	}
	public void setRoleState(Hero hero)
	{
		label1.setText(hero.getid()+" HP: "+hero.gethp()+" MP: "+hero.getmp()+" PP: "+hero.getpp());//set the word in the label
		label2.setIcon(hero.getimage());//input the image from class hero
		panel.setState(hero);//set the table of HP, MP, PP
	}

}
