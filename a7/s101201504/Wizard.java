package ce1002.a7.s101201504;

public class Wizard extends Hero {
	public Wizard()
	{
		//input the data 
		setid("Wizard");
		setimage("src\\ce1002\\a7\\s101201504\\Wizard.jpg");
	}
	public double  gethp()
	{
		return hp*0.2;
	}
	public double  getmp()
	{
		return mp*0.7;
	}
	public double  getpp()
	{
		return pp*0.1;
	}

}
