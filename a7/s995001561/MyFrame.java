package ce1002.a7.s995001561;


import javax.swing.JFrame;
import ce1002.a7.s995001561.Hero;
import ce1002.a7.s995001561.Knight;
import ce1002.a7.s995001561.MyPanel;
import ce1002.a7.s995001561.Swordsman;
import ce1002.a7.s995001561.Wizard;

@SuppressWarnings("serial")
public class MyFrame extends JFrame{

	MyPanel MyPanel0 = new MyPanel();
	MyPanel MyPanel1 = new MyPanel();
	MyPanel MyPanel2 = new MyPanel();

	
	MyFrame(){
		
		//add panel to frame and set up panels
        this.add(MyPanel0);
        newRolePos(new Wizard(), MyPanel0, 2, 0);
        this.add(MyPanel1);
        newRolePos(new Swordsman(), MyPanel1, 2, 230);
        this.add(MyPanel2);
        newRolePos(new Knight(), MyPanel2, 2, 460);

        
        
        // set layout and size
        this.setLayout(null);
		this.setSize(420,730);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		//set panel location and size
		panel.setRoleState(hero);
		panel.setBounds(x, y, 400, 225);
	}
	


}
