package ce1002.a7.s995001561;

import javax.swing.ImageIcon;

import ce1002.a7.s995001561.Hero;


public class Knight extends Hero{
	
	protected double HP = 30.0;
	protected double MP = 30.0;
	protected double PP = 30.0;
	
	//read picture
	Knight(){
		setName("Knight");
		icon = new ImageIcon("Knight.jpg");
	}
	
	// modify getHP method
	public double getHP() {
		return HP*0.8;
	}
	
	// modify getMP method
	public double getMP() {
		return MP*0.1;
	}
	
	// modify getPP method
	public double getPP() {
		return PP*0.1;
	}

}