package ce1002.a7.s995001561;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	
	// create status and picture
	JLabel status = new JLabel();
	JLabel picture = new JLabel();
	StatusPanel StatusPanel = new StatusPanel();

	// set border
	public MyPanel() {
		
		this.setLayout(new BorderLayout());
		this.add(status,BorderLayout.NORTH);
		this.add(picture,BorderLayout.WEST);
		this.add(StatusPanel,BorderLayout.CENTER);
		
		}
	
	
	public void setRoleState(Hero hero){
		
		//show status in label
		status.setText(hero.getName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP: "+hero.getPP());
		picture.setIcon(hero.getIcon());
		StatusPanel.setState(hero);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}
	

}