package ce1002.a7.s995001561;

import javax.swing.ImageIcon;

import ce1002.a7.s995001561.Hero;


public class Wizard extends Hero{
	
	protected double HP = 30.0;
	protected double MP = 30.0;
	protected double PP = 30.0;
	
	//read picture
	Wizard(){
		setName("Wizard");
		icon = new ImageIcon("Wizard.jpg");
	}
	
	// modify getHP method
	public double getHP() {
		return HP*0.2;
	}
	
	// modify getMP method
	public double getMP() {
		return MP*0.7;
	}
	
	// modify getPP method
	public double getPP() {
		return PP*0.1;
	}

}