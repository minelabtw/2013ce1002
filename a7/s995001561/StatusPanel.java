package ce1002.a7.s995001561;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class StatusPanel extends JPanel{
	
	// create label
	JLabel HP = new JLabel();
	JLabel MP = new JLabel();
	JLabel PP = new JLabel();
	JLabel hp = new JLabel();
	JLabel mp = new JLabel();
	JLabel pp = new JLabel();
	
	// add label
	public StatusPanel() {

		this.setLayout(new GridLayout(3, 2));
		this.add(HP);
		this.add(hp);
		this.add(MP);
		this.add(mp);
		this.add(PP);
		this.add(pp);
		
		}
	
	public void setState(Hero hero){
		
		//show status in label
		HP.setText("HP");
		hp.setText(" "+hero.getHP());
		MP.setText("MP");
		mp.setText(" "+hero.getMP());
		PP.setText("PP");
		pp.setText(" "+hero.getPP());
		
	}

}
