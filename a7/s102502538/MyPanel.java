package ce1002.a7.s102502538;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel  {
	
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	
	MyPanel()
	{
		setLayout(null);//設定panel數值
		label.setBounds(5,0,220,20);
		imagelabel.setBounds(5,15,240,180);
	
		add(label);
		add(imagelabel);
	}
	
	public void setRoleState(Hero hero)
	{	 
		imagelabel.setIcon(hero.getImage());//get標籤的文字和圖片
		label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
	}

}
