package ce1002.a7.s102502538;

public class Wizard extends Hero{
	
	public Wizard()
	{
		super.setName("Wizard");//設定角色和圖片
		super.setImage("Wizard.jpg");
	}
	
	public double getHp()//回傳加成後的數值
	{
		return super.HP*0.2;
	}
	public double getMp()
	{
		return super.MP*0.7;
	}
	public double getPp()
	{
		return super.PP*0.1;
	}
}
