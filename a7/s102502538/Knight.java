package ce1002.a7.s102502538;

public class Knight extends Hero {
	
	public Knight()
	{	
		super.setName("Knight");//設定角色和圖片
		super.setImage("Knight.jpg");
	}
	
	public double getHp()//回傳加成後的數值
	{
		return super.HP*0.8;
	}
	public double getMp()
	{
		return super.MP*0.1;
	}
	public double getPp()
	{
		return super.PP*0.1;
	}
}

