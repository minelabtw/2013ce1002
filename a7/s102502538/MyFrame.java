package ce1002.a7.s102502538;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{
	
	protected MyPanel panel1 = new MyPanel();//創造3個panel
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected StatusPanel panel4 = new StatusPanel();
	protected StatusPanel panel5 = new StatusPanel();
	protected StatusPanel panel6 = new StatusPanel();

	
	protected Hero[] heros;
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		setLayout(null);//設定frame的各項數值
		setBounds(300 , 50 , 485 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		
		newRolePos(heros[0] , panel1 , 10 , 10);//設定圖片位置
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);

		newpanel(heros[0] , panel4 , 300 , 100);
		newpanel(heros[1] , panel5 , 300 , 300);
		newpanel(heros[2] , panel6 , 300 , 500);

		
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{

		panel.setBounds(x, y, 250 , 200);//設定panel的位置和大小

		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//設定邊界

		panel.setRoleState(hero);//設定標籤
		
		add(panel);
	}
	public void newpanel(Hero hero , StatusPanel panel , int x , int y)
	{
		panel.setBounds(x, y, 100 , 70);//設定panel的位置和大小
		
		panel.setState(hero);//設定標籤
		
		add(panel);
				
	}
	
	
}
