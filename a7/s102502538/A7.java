package ce1002.a7.s102502538;

public class A7 {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			// Initialized hero array with length of 3
			Hero[] heros = new Hero[3];
			

			heros[0] = new Wizard();//用陣列儲存角色數值
			heros[1] = new Swordsman();
			heros[2] = new Knight();
			

			for(Hero hero : heros){//輸出各項數值
				System.out.println( hero.getName() + " HP:"+ hero.getHp() + " MP:" + hero.getMp() + " PP:" + hero.getPp() );
			}			

			MyFrame frame = new MyFrame(heros);//輸出圖片
		
	}
}
