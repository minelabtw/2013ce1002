package ce1002.a7.s102502538;

public class Swordsman extends Hero{

	public Swordsman()
	{
		super.setName("Swordsman");//設定角色和圖片
		super.setImage("Swordsman.jpg");
	}
	
	public double getHp()//回傳加成後的數值
	{
		return super.HP*0.1;
	}
	public double getMp()
	{
		return super.MP*0.1;
	}
	public double getPp()
	{
		return super.PP*0.8;
	}
}
