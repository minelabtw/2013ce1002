package ce1002.a7.s102502517;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
 
public class MyPanel extends JPanel{
	private JLabel showstate = new JLabel();
	private JLabel showimage = new JLabel();
	
	public MyPanel() //設定格式
	{
		setLayout(new BorderLayout());
		showstate.setBounds(5,0,220,20);
		showimage.setBounds(5,20,240,180);
		add(showstate,BorderLayout.NORTH);
		add(showimage,BorderLayout.CENTER);
	}
	
	public void setRoleState(Hero hero) //設定label內容
	{
		showstate.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
		showimage.setIcon(hero.getImage());
	}
}
