package ce1002.a7.s102502517;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel{
	
	public StatusPanel()
	{
		setLayout(new GridLayout(3, 2));
	}
	
	public void setStatus(Hero hero) //加入label
	{
		add(new JLabel("HP"));
		add(new JLabel("" + hero.getHp()));
		add(new JLabel("MP"));
		add(new JLabel("" + hero.getMp()));
		add(new JLabel("PP"));
		add(new JLabel("" + hero.getPp()));
	}
}
