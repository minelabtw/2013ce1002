package ce1002.a7.s102502517;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
 
public class MyFrame extends JFrame{
	private MyPanel panel1 = new MyPanel();
	private MyPanel panel2 = new MyPanel();
	private MyPanel panel3 = new MyPanel();
	private StatusPanel status1 = new StatusPanel();
	private StatusPanel status2 = new StatusPanel();
	private StatusPanel status3 = new StatusPanel();
	private Hero[] hero;
	
	public MyFrame(Hero[] hero) //設定frame內容
	{
		this.hero = hero;
		
		setLayout(null);
		setBounds(300 , 50 , 500 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		newRolePos(hero[0],panel1,status1,10,10);
		newRolePos(hero[1],panel2,status2,10,220);
		newRolePos(hero[2],panel3,status3,10,430);
		
	}
	
	public void newRolePos(Hero hero,MyPanel panel,StatusPanel status,int x ,int y) //設定格式
	{
		JPanel bigpanel = new JPanel();
		bigpanel.setLayout(new BorderLayout());
		bigpanel.setBounds(x, y, 450 , 200);
		bigpanel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		status.setStatus(hero);
		bigpanel.add(panel,BorderLayout.WEST);
		bigpanel.add(status,BorderLayout.CENTER);
		add(bigpanel);
	}
}
