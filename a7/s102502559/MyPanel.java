package ce1002.a7.s102502559;

import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.FlowLayout;

public class MyPanel extends JPanel{
	ImageIcon icon;
	JLabel info;
	JLabel image;
	public MyPanel()
	{
		this.setLayout(new FlowLayout(FlowLayout.LEFT));
		this.setSize(400,250);
		LineBorder LB = new LineBorder(Color.black,5);
		this.setBorder(LB);
	}
	public void setRoleState(Hero hero,int i)
	{
		icon = new ImageIcon(hero.getName()+".jpg");
		info = new JLabel(hero.getName() + " HP:" + hero.getHP() +" MP:" + hero.getMP() + " PP:"+ hero.getPP());
		image = new JLabel(icon);
		this.add(info);
		this.add(image);
		StatusPanel[] statuspanel = new StatusPanel[3];//建立用來顯示英雄能力值得panel陣列
		statuspanel[i] = new StatusPanel(hero);//呼叫建構子
		this.add(statuspanel[i]);//將panel加入frame
	}

}
