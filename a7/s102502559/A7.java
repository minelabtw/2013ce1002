package ce1002.a7.s102502559;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		Hero[] hero = new Hero[3]; //宣告型別為Hero的陣列,陣列長度為3
		//宣告陣列的三項分別為Knight,Swordsman,Wizard類別的物件
		hero[0] = new Wizard();
		hero[1] = new Swordsman();
		hero[2] = new Knight();
		
		MyPanel[] panel = new MyPanel[3];
		
		MyFrame frame = new MyFrame();
		frame.setSize(430,900);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		for(int i = 0;i<3;i++)
		{
			panel[i] = new MyPanel();
			frame.newRolePos(hero[i],panel[i],10,10+240*i,i);
		}
		frame.setVisible(true);
	}

}
