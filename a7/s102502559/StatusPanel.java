package ce1002.a7.s102502559;

import java.awt.Color;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class StatusPanel extends JPanel {
	public double hp;
	public double mp;
	public double pp;
	StatusPanel()
	{
		this.setSize(100,240);
		this.setLayout(new GridLayout(3,2));
		
	}
	//建構子內用來初始化panel的大小,設定為排版為gridlayout,然後呼叫其他函式
	StatusPanel(Hero hero) {
		this.setSize(100,240);
		this.setLayout(new GridLayout(3,2,50,50));
		this.setState(hero);
		this.setLabel();
	}
//取得英雄個能力值
public void setState(Hero hero)
{
	hp = hero.getHP();
	mp = hero.getMP();
	pp = hero.getPP();
	}
//將能力值加入label還有將label加入panel
public void setLabel()
{
	JLabel status1 = new JLabel("HP");
	JLabel status2 = new JLabel(""+hp);
	JLabel status3 = new JLabel("MP");
	JLabel status4 = new JLabel(""+mp);
	JLabel status5 = new JLabel("PP");
	JLabel status6 = new JLabel(""+pp);
	this.add(status1);
	this.add(status2);
	this.add(status3);
	this.add(status4);
	this.add(status5);
	this.add(status6);
}
}
