package ce1002.a7.s102302053;


public class Knight extends Hero {
	Knight(){
		setHeroName("Knight");
		setHp();
		setMp();
		setPp();
		setPic("Knight.jpg");
	}
	public double getHp(){//回傳加權後的HP
		return hp*0.8;
	}
	public double getMp(){//回傳加權後的MP
		return mp*0.1;
	}
	public double getPp(){//回傳加權後的PP
		return pp*0.1;
	}

}
