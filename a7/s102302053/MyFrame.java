package ce1002.a7.s102302053;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame {	
	MyPanel[] panel = new MyPanel[3];
	MyPanel p1 = new MyPanel();//frame上有三個panel
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();
	MyFrame(){
		JFrame frame = new JFrame();//宣告frame物件
		Hero w = new Wizard();//宣告三個不同hero
		Hero s = new Swordsman();
		Hero k = new Knight();
		newRolePos(w , p1 , 50 , 100);//呼喚method進行設定
		frame.add(p1);//將panel加到frame上
		newRolePos(s , p2 , 50 , 350);
		frame.add(p2);//將panel加到frame上
		newRolePos(k , p3 , 50 , 600);
		frame.add(p3);//將panel加到frame上
		frame.setLayout(new GridLayout(3, 1, 5, 5 ));//frame分為三塊依序排列
		frame.setSize(450,730);//設定frame的大小
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){//依傳入的hero, panel, 位置, 進行設置
		panel.statusPanel.setState(hero);	
		panel.setRoleState(hero);//設定每個panel裡面的資料	
		panel.setBorder(new LineBorder(Color.BLACK,3));//設定黑色的邊框
		setLayout(null);
		panel.setBounds(x, y, 400, 250);//設置panel的位置和大小
		
		
		
		
	}
	
	

}
