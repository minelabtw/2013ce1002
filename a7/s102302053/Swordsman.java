package ce1002.a7.s102302053;


public class Swordsman extends Hero {
	Swordsman(){
		setHeroName("Swordsman");
		setHp();
		setMp();
		setPp();
		setPic("Swordsman.jpg");
	}
	public double getHp(){//回傳加權後的HP
		return hp*0.1;
	}
	public double getMp(){//回傳加權後的MP
		return mp*0.1;
	}
	public double getPp(){//回傳加權後的PP
		return pp*0.8;
	}


}
