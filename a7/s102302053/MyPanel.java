package ce1002.a7.s102302053;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	JLabel l1 = new JLabel();//每個panel上有兩個label
	JLabel l2 = new JLabel();
	StatusPanel statusPanel = new StatusPanel();//每個panel上有一個子panel
	
	MyPanel(){
		
	
	}
	public void setRoleState(Hero hero){
		
		String n = hero.getPic();//對應的hero圖檔名稱
		ImageIcon image = new ImageIcon(n);
		l1 = new JLabel(hero.getHeroName() + " HP: " + hero.getHp() +" MP: " + hero.getMp() + " PP: " + hero.getPp());//l1上為對應的hero各項數值
		l2 = new JLabel(image);//l2上為對應的hero圖檔
		add(l1);//將label加到panel上
		add(l2);//將label加到panel上
		add(statusPanel);
		setLayout(null);
		l1.setBounds(5,0,220,30);
		l2.setBounds(5,5,240,230);
		statusPanel.setBounds(260,20,125,200);
		
		
	
	}

}
