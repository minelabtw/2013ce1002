package ce1002.a7.s102302053;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class StatusPanel extends JPanel {
	JLabel hp = new JLabel("HP");//宣告六個label顯示資料
	JLabel mp = new JLabel("MP");
	JLabel pp = new JLabel("PP");
	JLabel hpValue = new JLabel();
	JLabel mpValue = new JLabel();
	JLabel ppValue = new JLabel();
	
	StatusPanel(){
		setLayout(new GridLayout(3,2,9,10));//status分為六塊來放置panel
		add(hp);//將各個label由左而右,由上而下,放置到statuspanel上
		add(hpValue);
		add(mp);
		add(mpValue);
		add(pp);
		add(ppValue);
	}
	public void setState(Hero hero) {
		hpValue.setText(""+hero.getHp() );//依照需要的資料呼叫hero裡的method
		hpValue.setHorizontalTextPosition(JLabel.CENTER);//將文字置中
		mpValue.setText(""+hero.getMp());//依照需要的資料呼叫hero裡的method
		mpValue.setHorizontalTextPosition(JLabel.CENTER);//將文字置中
		ppValue.setText(""+hero.getPp());//依照需要的資料呼叫hero裡的method
		ppValue.setHorizontalTextPosition(JLabel.CENTER);//將文字置中
		
	}

}
