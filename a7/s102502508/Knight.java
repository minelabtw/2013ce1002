package ce1002.a7.s102502508;

public class Knight extends Hero{
	
	public Knight()
	{
		setHerosName("Knight");
		setHP(HP);
		setMP(MP);
		setPP(PP);
	}

	
	public void setHP(float hP) {
         HP = (float) (hP*0.8);
	}

	

	public void setMP(float mP) {
		
		MP = (float) (mP*0.1);
	}

	

	public void setPP(float pP) {
	    PP = (float)(pP*0.1);
	}

}
