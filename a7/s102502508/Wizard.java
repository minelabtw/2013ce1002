package ce1002.a7.s102502508;

public class Wizard extends Hero{

	public Wizard()
	{
		setHerosName("Wizard");
		setHP(HP);
		setMP(MP);
		setPP(PP);
	}

	
	public void setHP(float hP) {
		HP = (float) (hP*0.2);
	}

	

	public void setMP(float mP) {
		
		MP = (float) (mP*0.7);
	}

	

	public void setPP(float pP) {
		PP = (float)(pP*0.1);
	}
}
