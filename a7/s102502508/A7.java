package ce1002.a7.s102502508;

import javax.swing.JFrame; 
public class A7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//建立物件
		MyFrame frame =new MyFrame();
		MyPanel panel1=new MyPanel();
		MyPanel panel2=new MyPanel();
		MyPanel panel3=new MyPanel();
		MyPanel[] c = new  MyPanel[3];
		//利用一維陣列存入個角色的照片和文字敘述
		panel1.setRoleState(new Knight());
		c[0]= panel1 ;
		panel2.setRoleState(new Swordsman());
		c[1]= panel2 ;
		panel3.setRoleState(new Wizard());
		c[2]= panel3 ;
		frame.newRolePos(c,400,600);//設定視窗的長與寬並將Panel放入Frame中
		frame.setLocationRelativeTo(null) ;
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		frame.setVisible(true);  //讓使用者能看到視窗
	}
}
		
		
