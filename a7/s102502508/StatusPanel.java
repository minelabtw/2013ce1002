package ce1002.a7.s102502508;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.GridLayout;

public class StatusPanel extends JPanel {

	protected JLabel j1;
	protected JLabel j2;
	protected JLabel j3;
	protected JLabel j4;
	protected JLabel j5;
	protected JLabel j6;

	public StatusPanel() { // 建立Panel的模型
		setLayout(new GridLayout(3, 2));
		j1 = new JLabel("HP");
		j2 = new JLabel("");
		j3 = new JLabel("MP");
		j4 = new JLabel("");
		j5 = new JLabel("PP");
		j6 = new JLabel("");
		this.add(j1);
		this.add(j2);
		this.add(j3);
		this.add(j4);
		this.add(j5);
		this.add(j6);

	}

	public void setState(Hero hero) { // 設定欲在Panel上顯示的資料
		j2.setText("" + hero.getHP());
		j4.setText("" + hero.getMP());
		j6.setText("" + hero.getPP());

	}

}
