package ce1002.a7.s102502039;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JFrame;

public class StatusPanel extends JPanel {//set object
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	JLabel label4 = new JLabel();
	JLabel label5 = new JLabel();
	JLabel label6 = new JLabel();

	public StatusPanel() {//add to panel

		setBounds(5, 0, 220, 20);
		setLayout(new GridLayout(3, 2, 3, 3));
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		add(label5);
		add(label6);

	}

	public void setState(Hero hero) {//out numbers
		label1.setText("HP");
		label2.setText("" + hero.getHP());
		label3.setText("MP");
		label4.setText("" + hero.getMP());
		label5.setText("PP");
		label6.setText("" + hero.getPP());
	}

}
