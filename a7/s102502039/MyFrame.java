package ce1002.a7.s102502039;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{
	
	protected Hero[] heros;
	
	protected MyPanel panel1; 
	protected MyPanel panel2;
	protected MyPanel panel3;
	
	
	MyFrame(Hero[] heros)
	{
		this.heros = heros;
		
		panel1 = new MyPanel(heros[0]);
		panel2 = new MyPanel(heros[1]);
		panel3 = new MyPanel(heros[2]);
		
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 285 , 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		/*
		 * set role position and image
		 */
		newRolePos(heros[0] , panel1 , 10 , 10);
		newRolePos(heros[1] , panel2 , 10 , 220);
		newRolePos(heros[2] , panel3 , 10 , 430);
	}
	
	public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 500 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);

	}
}
