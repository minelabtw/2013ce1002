package ce1002.a7.s102502039;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	
	MyPanel(Hero hero) {
		/* set panel's size and layout */
		setLayout(null);
		label.setBounds(5, 0, 220, 20);
		imagelabel.setBounds(5, 15, 240, 180);
		/* add label to panel */
		add(label);
		add(imagelabel);
		StatusPanel p = new StatusPanel();
		p.setBounds(250, 10, 240, 180);
		p.setState(hero);
		add(p);
	}

	public void setRoleState(Hero hero) {
		/* set label's text and image */
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getHeroName() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP:" + hero.getPP());
	}

}
