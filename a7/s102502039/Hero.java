package ce1002.a7.s102502039;
import javax.swing.ImageIcon;
public class Hero {
	private String heroname;//declare variable
	private double HP;
	private double MP;
	private double PP;
	private ImageIcon image;
public Hero(){//constructor
	setHp(30);
	setMP(30);
	setPP(30);
}
	public void set(String heroname, double HP, double MP, double PP) {//set variable
		setHeroname(heroname);
		setHp(HP);
		setMP(MP);
		setPP(PP);
	}

	public void setHeroname(String heroname) {//set variable
		this.heroname = heroname;
	}

	public void setHp(double HP) {//set variable
		this.HP = HP;
	}

	public void setMP(double MP) {//set variable
		this.MP = MP;
	}

	public void setPP(double PP) {//set variable
		this.PP = PP;
	}

	public String getHeroName() {//function
		return heroname;
	}

	public double getHP() {//function
		return HP;
	}

	public double getMP() {//function
		return MP;
	}

	public double getPP() {//function
		return PP;
	}
	public void setImage(String imagename)
	{
		this.image = new ImageIcon(imagename);
	}
	public ImageIcon getImage()
	{
		return this.image;
	}

		
	
}
