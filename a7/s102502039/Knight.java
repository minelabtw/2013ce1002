package ce1002.a7.s102502039;


public class Knight extends Hero {
	public Knight() {// constructor
		super.setHeroname("Knight");
		super.setImage("Knight.jpg");

	}

	public double getHP() {// function
		return super.getHP() * 0.8;
	}

	public double getMP() {// function
		return super.getMP() * 0.1;
	}

	public double getPP() {// function
		return super.getPP() * 0.1;
	}

}
