package ce1002.a7.s102502039;

public class Swordsman extends Hero {
	public Swordsman() {// constructor
		super.setHeroname("Swordsman");
		super.setImage("Swordsman.jpg");

	}

	public double getHP() {// function
		return super.getHP() * 0.1;
	}

	public double getMP() {// function
		return super.getMP() * 0.1;
	}

	public double getPP() {// function
		return super.getPP() * 0.8;
	}
}
