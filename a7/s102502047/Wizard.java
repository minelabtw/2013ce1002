package ce1002.a7.s102502047;

import ce1002.a7.s102502047.Hero;

public class Wizard 
	extends Hero{
	
	public Wizard() {
		double hp=gh()*0.2;
		double mp=gm()*0.7;
		double pp=gp()*0.1;
		sh(hp);
		sm(mp);
		sp(pp);
		sn("Wizard");
	}

}
