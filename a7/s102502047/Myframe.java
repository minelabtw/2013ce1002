package ce1002.a7.s102502047;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

public class Myframe  extends JFrame{

	public Myframe() {
		Mypanel p1=new Mypanel();
		Mypanel p2=new Mypanel();
		Mypanel p3=new Mypanel();
		
		Hero h[]=new Hero[3];
		h[0] = new Wizard();
		h[1] = new Swordsman();
		h[2] = new Knight();
		
		setLayout(null);
		
	    newRolePos(h[0] , p1 , 10 , 10);
		newRolePos(h[1] , p2 , 10 , 240);
		newRolePos(h[2] , p3 , 10 , 470);
		
		add(p1);
		add(p2);
		add(p3);
	}
	public void newRolePos(Hero hero , Mypanel panel , int x , int y){
		panel.setBorder(new LineBorder(Color.BLACK,3));
		panel.setRoleState(hero);
		panel.setLocation(x, y);
	}
}
