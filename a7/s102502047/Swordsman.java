package ce1002.a7.s102502047;

import ce1002.a7.s102502047.Hero;

public class Swordsman 
	extends Hero{

	public Swordsman() {
		double hp=gh()*0.1;
		double mp=gm()*0.1;
		double pp=gp()*0.8;
		sh(hp);
		sm(mp);
		sp(pp);	
		sn("Swordsman");
	}

}
