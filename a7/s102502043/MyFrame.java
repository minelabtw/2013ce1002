package ce1002.a7.s102502043;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
public class MyFrame extends JFrame
{
	protected MyPanel panel1 = new MyPanel();//建立3個MyPanel物件
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	MyFrame(Hero hero[])
	{
		setLayout(null);//自訂排板框架
		setBounds(300 , 50 , 390 , 800);//設定框架大小
		newRolePos(hero[0],panel1,15,15);//將3個MyPanel物件傳入並設定位置
		newRolePos(hero[1],panel2,15,260);
		newRolePos(hero[2],panel3,15,505);
	}
	 public void newRolePos(Hero hero , MyPanel panel , int x , int y)
	 {
		panel.setBounds(x,y,330,230);//規定panel大小
		panel.setRoleState(hero);//將hero物件傳入panel method
		add(panel);//將panel加入框架
	 }
}
