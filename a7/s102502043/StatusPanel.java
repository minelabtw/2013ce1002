package ce1002.a7.s102502043;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JLabel;
public class StatusPanel extends JPanel
{
	protected JLabel l0 = new JLabel("HP");//建立6個label
	protected JLabel l1 = new JLabel();
	protected JLabel l2 = new JLabel("MP");
	protected JLabel l3 = new JLabel();
	protected JLabel l4 = new JLabel("PP");
	protected JLabel l5 = new JLabel();
	StatusPanel()
	{
		setLayout(new GridLayout(3,2));//設定排版
		add(l0);//將label加入面板
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
	}
	public void setState(Hero hero)
	{
		l1.setText(""+hero.gethp());//將method傳入text
		l3.setText(""+hero.getmp());
		l5.setText(""+hero.getpp());
	}
}
