package ce1002.a7.s102502043;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyPanel extends JPanel 
{
	protected JLabel label = new JLabel();//設定2個label一個存自一個存圖
	protected JLabel imagelabel = new JLabel();
	protected StatusPanel status = new StatusPanel();//建立Status物件
	MyPanel()
	{
		setLayout(null);//設定自訂排版
		label.setBounds(10,10,220,20);//設定文字大小與位置
		imagelabel.setBounds(10,20,220,200);//設定圖片大小與位置
		status.setBounds(250,20,50,200);//設定status物件大小與位置
		add(label,BorderLayout.NORTH);//將文字加入面板
		add(imagelabel,BorderLayout.WEST);//將圖片加入面板
		add(status,BorderLayout.CENTER);//將表格加入面板
		setBorder(BorderFactory.createLineBorder(Color.BLACK));//設定外圍黑框
	}
	public void setRoleState(Hero hero)
	{
		label.setText(hero.getName()+" HP:"+hero.gethp()+" MP:"+hero.getmp()+" PP:"+hero.getpp());//設定各個英雄文字
		imagelabel.setIcon(hero.getImage());//設定各個hero傳入圖片
		status.setState(hero);//將各個hero傳入method
	}
	
	
}
