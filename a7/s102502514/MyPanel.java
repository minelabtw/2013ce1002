package ce1002.a7.s102502514;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	protected JLabel label = new JLabel();
	protected JLabel imagelabel = new JLabel();
	
	StatusPanel statuspanel = new StatusPanel();
	MyPanel()
	{
		/*set panel's size and layout*/
		setLayout(null);
		label.setBounds(5,0,240,20);
		imagelabel.setBounds(5,15,240,180);
		/*add label to panel*/
		add(label);
		add(imagelabel);
		/*add statuspanel to panel*/
		add(statuspanel);
	}
	
	public void setRoleState(Hero hero)
	{
		/*set label's text and image*/
		imagelabel.setIcon(hero.getImage());
		label.setText(hero.getHeroName()+" HP: "+hero.getHP()+" MP: "+hero.getMP()+" PP:"+hero.getPP());
		/*set statuspanel's  text*/
		statuspanel.setState(hero);
	}

}
