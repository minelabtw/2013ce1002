package ce1002.a7.s102502514;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
    protected MyPanel knightpanel = new MyPanel();
	protected MyPanel swordsmanpanel = new MyPanel();
	protected MyPanel wizardpanel = new MyPanel();
	protected Hero[] heros = new Hero[3];
	
	MyFrame(Hero[] heros){
		this.heros = heros;
		setLayout(null);
		setBounds(300, 50, 460, 680);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		newRolePos(heros[0], wizardpanel, 10, 10);
		newRolePos(heros[1], swordsmanpanel, 10, 220);
		newRolePos(heros[2], knightpanel, 10, 430);
	}
	public void newRolePos(Hero hero , MyPanel panel , int x , int y){
		/*set panel's position and size*/
		panel.setBounds(x, y, 420, 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*set label which in the panel*/
		panel.setRoleState(hero);
		/*add panel to frame*/
		add(panel);
	}
}
