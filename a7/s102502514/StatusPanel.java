package ce1002.a7.s102502514;

import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.*;

public class StatusPanel extends JPanel{
	private JLabel jlb1 = new JLabel();
	private JLabel jlb2 = new JLabel();
	private JLabel jlb3 = new JLabel();
	private JLabel jlb4 = new JLabel();
	private JLabel jlb5 = new JLabel();
	private JLabel jlb6 = new JLabel();
	StatusPanel(){
		/*set statuspanel's bounds and layout*/
		setLayout(new GridLayout(3, 2));
		setBounds(260, 15, 150, 180);
		add(jlb1);
		add(jlb2);
		add(jlb3);
		add(jlb4);
		add(jlb5);
		add(jlb6);
	}
	public void setState(Hero hero){
		jlb1.setText("HP");
		jlb2.setText(""+hero.getHP());
		jlb3.setText("MP");
		jlb4.setText(""+hero.getMP());
		jlb5.setText("PP");
		jlb6.setText(""+hero.getPP());
	}
}
