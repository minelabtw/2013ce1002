package ce1002.a7.s102502514;

import javax.swing.ImageIcon;

public class Hero {
	private String HeroName;
	private double HP;
	private double MP;
	private double PP;
	private ImageIcon image;
	
	Hero(){
		//初始值
		setHP(30);
		setMP(30);
		setPP(30);
	}
	public String getHeroName() {
		return HeroName;
	}
	public void setHeroName(String heroName) {
		HeroName = heroName;
	}
	public double getHP() {
		return HP;
	}
	public void setHP(double hP) {
		HP = hP;
	}
	public double getMP() {
		return MP;
	}
	public void setMP(double mP) {
		MP = mP;
	}
	public double getPP() {
		return PP;
	}
	public void setPP(double pP) {
		PP = pP;
	}
	//讀取圖片
	public void setImage(String imagename){
		this.image = new ImageIcon(imagename);
	}
	//回傳圖片
	public ImageIcon getImage(){
		return this.image;
	}
}
