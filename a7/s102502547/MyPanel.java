package ce1002.a7.s102502547;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {

	JLabel state =new JLabel(); // 英雄狀態
	JLabel image =new JLabel(); // 圖片資料
	ImageIcon icon; // 儲存圖片路徑
	StatusPanel s =new StatusPanel(); //表格

	MyPanel() {
		setSize(380, 240); // 設定大小
		setBorder(new LineBorder(Color.black, 3)); // 設定邊界屬性
		

	}

	public void setRoleState(Hero hero) {
		state = new JLabel(hero.getName() + " HP: " + hero.getHP() + " MP: "
				+ hero.getMP() + " PP: " + hero.getPP()+"                                  "); // 設定state屬性
		icon = new ImageIcon(hero.getName() + ".jpg"); // 設定icon為圖片路徑
		image = new JLabel(icon); // 設定image屬性
		this.add(state); // 添加到Panel
		this.add(image);
		s.setState(hero);
		add(s);
		
	}
}
