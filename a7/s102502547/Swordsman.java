package ce1002.a7.s102502547;

public class Swordsman extends Hero {
	
	Swordsman() {
		name = "Swordsman"; //命名
	}

	public double getHP() {
		return HP * 0.1; //加權
	}

	public double getMP() {
		return MP * 0.1; //加權
	}

	public double getPP() {
		return PP * 0.8; //加權
	}
}