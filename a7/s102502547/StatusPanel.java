package ce1002.a7.s102502547;

import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JLabel;

public class StatusPanel extends JPanel {

	StatusPanel() {
		setLayout(new GridLayout(3, 2, 30, 50)); //3�C2�� ���Z��50
	}

	public void setState(Hero hero) {

		JLabel a1 = new JLabel("HP: ");
		JLabel a2 = new JLabel(hero.getHP() + "");
		JLabel a3 = new JLabel("MP: ");
		JLabel a4 = new JLabel(hero.getMP() + "");
		JLabel a5 = new JLabel("PP: ");
		JLabel a6 = new JLabel(hero.getPP() + "");

		add(a1);
		add(a2);
		add(a3);
		add(a4);
		add(a5);
		add(a6);

	}

}
