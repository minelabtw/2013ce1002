package ce1002.a7.s102502547;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	MyFrame() {
		setLayout(null); // 設定各物件在視窗中的布局方式
		setSize(410, 780); // 設定視窗大小
		setLocationRelativeTo(null); // 設定視窗位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 按視窗右上角的"X"會關閉程序
	}

	public void newRolePos(Hero hero, MyPanel panel, int x, int y) {
		panel.setRoleState(hero); // 設定panel的資料、圖片
		panel.setLocation(x, y); // 設定panel的位置
		this.add(panel); // 把設定好的panel添加到視窗裡
	}
}
