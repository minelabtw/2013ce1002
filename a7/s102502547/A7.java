package ce1002.a7.s102502547;

import javax.swing.JFrame;

public class A7 {

	public static void main(String[] args) {
		Hero[] character = new Hero[3];
		character[0] = new Wizard();
		character[1] = new Swordsman();
		character[2] = new Knight();

		MyFrame frame = new MyFrame(); // 宣告一個名為frame的MyFrame型態的變數

		MyPanel[] panel = new MyPanel[3]; // 宣告一個名為panel的MyPanel型態陣列
		for (int i = 0; i < 3; i++) // 設定panel陣列裡的元素
		{
			panel[i] = new MyPanel();
			frame.newRolePos(character[i], panel[i], 5, 5 + 245 * i);
		}
		frame.setVisible(true); // 使視窗能顯示在螢幕上
	}
}
