package ce1002.a7.s102502547;

public class Wizard extends Hero {
	Wizard() {
		name = "Wizard"; //命名
	}

	public double getHP() {
		return HP * 0.2; //加權
	}

	public double getMP() {
		return MP * 0.7; //加權
	}

	public double getPP() {
		return PP * 0.1; //加權
	}

}
