package ce1002.a7.s102502509;

import java.awt.GridLayout;

import javax.swing.*;

public class StatusPanel extends JPanel
{
	StatusPanel()//panel(容器)建構子 建立範圍
	{
		setLayout(new GridLayout(3, 2)); // manage
		setSize(100, 236); // 容器大小
	}
	
	public void setState(Hero hero)
	{
		JLabel l1 = new JLabel();
		JLabel l2 = new JLabel();
		JLabel l3 = new JLabel();
		JLabel l4 = new JLabel();
		JLabel l5 = new JLabel();
		JLabel l6 = new JLabel();
		
		l1.setText("HP");
		l2.setText("" + hero.getHP());
		l3.setText("MP");
		l4.setText("" + hero.getMP());
		l5.setText("PP");
		l6.setText("" + hero.getPP());
		
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
		add(l6);
		
	}
}
