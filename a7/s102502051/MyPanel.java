package ce1002.a7.s102502051;

import java.awt.Event;

import javax.swing.JLabel;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
protected JLabel label = new JLabel();
protected JLabel imagelabel = new JLabel();
protected JLabel StatusPanel = new JLabel();
protected JLabel StatusPanel1 = new JLabel();
protected JLabel StatusPanel2 = new JLabel();
MyPanel()
{
/*set panel's size and layout*/
setLayout(null);
label.setBounds(5,0,220,20);
imagelabel.setBounds(5,15,240,180);
StatusPanel.setBounds(150,0,240,180);
StatusPanel1.setBounds(150,40,240,180);
StatusPanel2.setBounds(150,80,240,180);
/*add label to panel*/
add(label);
add(imagelabel);
add(StatusPanel);
add(StatusPanel1);
add(StatusPanel2);
}
public void setRoleState(Hero hero)
{
/*set label's text and image*/
imagelabel.setIcon(hero.getImage());
label.setText(hero.getName()+" HP: "+hero.getHp()+" MP: "+hero.getMp()+" PP:"+hero.getPp());
StatusPanel.setText(" HP:             "+hero.getHp());
StatusPanel1.setText(" MP:             "+hero.getMp());
StatusPanel2.setText(" PP:             "+hero.getPp());
}
}