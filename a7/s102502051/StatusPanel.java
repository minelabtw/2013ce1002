package ce1002.a7.s102502051;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class StatusPanel extends JPanel {
public void setState(Hero hero) {
 
    JFrame.setDefaultLookAndFeelDecorated(true);
    JFrame frame = new JFrame(" ");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setLayout(new GridLayout(3, 1));
    frame.add(new JLabel("Hp:"));
    frame.add(new JLabel(" "+ hero.getHp()));
    frame.add(new JLabel("Mp: "));
    frame.add(new JLabel(" "+ hero.getMp()));
    frame.add(new JLabel("Pp: "));
    frame.add(new JLabel(" "+ hero.getPp()));
    frame.pack();
    frame.setVisible(true);

  }
}