package ce1002.a10.s101201524;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	private MyPanel panel = new MyPanel();
	
	MyFrame(){
		//set frame's status and add a panel
		setTitle("A10-101201524");
		setLayout(null);
		setBounds(400, 150, 516, 538);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(panel);
	}
}
