package ce1002.a10.s101201524;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyPanel extends JPanel{
	private JButton save = new JButton("Save");
	private JTextField name = new JTextField();
	private JTextArea letter = new JTextArea();
	
	MyPanel(){
		//set panel's status
		setLayout(null);
		setBounds(0, 0, 500, 500);
		//add the save button and give it a action listener
		save.setBounds(0, 0, 80, 30);
		save.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				//if file name is empty -> show the message
				if(name.getText().isEmpty())
					JOptionPane.showMessageDialog(null, "File name can��t be empty!", "�T��", JOptionPane.PLAIN_MESSAGE);
				//create a file with "file name.txt"
				else{
					java.io.File file = new java.io.File(name.getText() + ".txt");
					java.io.PrintWriter output;
					Scanner input = new Scanner(letter.getText());
					try {
						output = new java.io.PrintWriter(file);
						//write the words
						while(input.hasNext()){
							output.print(input.next());
						}
						output.close();
					} catch (FileNotFoundException e1){
					}
				}
			}
		});
		//set name and letter's status
		name.setBounds(80, 0, 420, 30);
		name.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		letter.setBounds(0, 30, 500, 470);
		letter.setBorder(BorderFactory.createLineBorder(Color.gray, 1));
		letter.setLineWrap(true);
		//add them on panel
		add(save);
		add(name);
		add(letter);
	}
}