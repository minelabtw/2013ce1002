package ce1002.a10.s102502037;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.*;

public class MyFrame
  extends JFrame
{
  private static final long serialVersionUID = 1L;
  private JPanel MyPanel;
  private JSplitPane splitpane;
  private JTextField text;
  private JButton btn;
  private JTextArea TextArea;
  public MyFrame()
  {
    setTitle("A10-102502037");
    setBounds(100, 100, 450, 300);
    this.MyPanel = new JPanel();
    this.MyPanel.setLayout(new BorderLayout(0, 0));
    setContentPane(this.MyPanel);
    
    this.TextArea = new JTextArea();
    this.TextArea.setLineWrap(true);
    this.MyPanel.add(this.TextArea, "Center");
    
    this.splitpane = new JSplitPane();
    this.MyPanel.add(this.splitpane, "North");
    
    this.text = new JTextField();
    this.splitpane.setRightComponent(this.text);
    
    this.btn = new JButton("Save");
    this.btn.addActionListener(new SaveButton(this));
    this.splitpane.setLeftComponent(this.btn);
  }
  class SaveButton implements ActionListener
  {
    JFrame frame;
    public SaveButton(JFrame frame)
    {
      this.frame = frame;
    }
    public void actionPerformed(ActionEvent e)
    {
      if (MyFrame.this.text.getText().isEmpty())
      {
        Empty();
      }
      else
      {
        String mainText = MyFrame.this.TextArea.getText();
        String fileName = MyFrame.this.text.getText() + ".txt";
        try
        {
          PrintWriter out = new PrintWriter(fileName);
          out.println(mainText);
          out.close();
          Saved(fileName);
        }
        catch (FileNotFoundException e1)
        {
          e1.printStackTrace();
        }
      }
    }
    
    private void Empty()
    {
      JOptionPane.showMessageDialog(this.frame, "File name can't be empty!");
    }
    private void Saved(String name)
    {
      JOptionPane.showMessageDialog(this.frame, "File " + name + " saved!");
    }
  }
}