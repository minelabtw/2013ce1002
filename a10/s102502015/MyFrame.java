package ce1002.a10.s102502015;

import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {
	JButton Save = new JButton("Done");
	JTextField Title = new JTextField();
	JTextArea Area = new JTextArea();

	public MyFrame() {
		Save.addActionListener(this);//Δγγ
		Save.setSize(100, 30);
		Save.setLocation(0, 0);
		Title.setSize(400, 30);
		Title.setLocation(105, 0);
		Area.setSize(500, 460);
		Area.setLocation(0, 35);
		Area.setLineWrap(true);//·s
		add(Save);
		add(Title);
		add(Area);
		this.setTitle("A10-102502015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null); // ρaέrΕ
		setSize(500, 500); // έuε¬
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {//
		save();
	}

	public void save() {
		if ("".equals(Title.getText().trim())) {//σ
			JFrame b = new JFrame();
			JLabel a = new JLabel("File name can't be empty!");
			JOptionPane.showMessageDialog(b, a);
		} else {//ρσ
			JFrame b = new JFrame();
			JLabel a = new JLabel("File " + Title.getText() + ".txt saved!");
			JOptionPane.showMessageDialog(b, a);
			FileWriter out;//Ao
			try {
				out = new FileWriter(Title.getText() + ".txt", true);
				out.write(Area.getText());
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
