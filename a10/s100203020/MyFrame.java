package ce1002.a10.s100203020;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import javax.swing.border.*;

public class MyFrame extends JFrame{
	private JPanel Panel = new JPanel();;
	private JButton button = new JButton("Save");
	private JSplitPane splitPane = new JSplitPane();
	private JTextField textFile = new JTextField();
	private JTextArea textArea = new JTextArea();
  
	
	//construct
	public MyFrame()
	{
		int x =150, y=100;
		int sizeX = 600, sizeY = 400;
		
		this.setTitle("A10");
		this.setBounds(x, y, sizeX, sizeY);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setContentPane(Panel);
		Panel.setLayout(new BorderLayout(0, 0));
		
		//add textArea, splitpanel
		Panel.add(textArea, "Center"); 
		Panel.add(splitPane, "North"); 
		
		textArea.setLineWrap(true);
		
		splitPane.setRightComponent(textFile);	    
		splitPane.setLeftComponent(button);
		
		button.addActionListener(new Save(this));

	}
class Save implements ActionListener{
    JFrame frame;
    public Save(JFrame frame)
    {
    	this.frame = frame;
    }
    
    public void actionPerformed(ActionEvent e)
    {
    	
    	//empty
    	if (MyFrame.this.textFile.getText().isEmpty())
    	{
        	JOptionPane.showMessageDialog(frame, "File name can't be empty!"); 
    	}
    	
    	//not empty
    	else
    	{
    		String text = MyFrame.this.textArea.getText();
    		String name = MyFrame.this.textFile.getText() + ".txt";
    		try
    		{
    			PrintWriter output = new PrintWriter(name);
    			output.println(text);
		        output.close();
		    	JOptionPane.showMessageDialog(frame, "File " + name + " saved!"); //saved
    		}
    		catch (FileNotFoundException ex)
    		{
    			ex.printStackTrace(); 
    		}
    	}
    }     
  }
}