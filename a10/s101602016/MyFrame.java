package ce1002.a10.s101602016;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JButton save = new JButton("Save");
	private JTextField field = new JTextField();
	private JTextArea area = new JTextArea();
	MyFrame() {
		setTitle("A10-101602016");//標題
		setSize(450,300);//視窗大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉視窗時程式關閉
		setLayout(null);//排版
		setVisible(true);//顯示
		area.setLineWrap(true);
		save.setBounds(5,5,65,27);//save按鈕大小
		field.setBounds(74,5,364,28);//標題輸入區域大小
		area.setBounds(5,36,433,225);//文字輸入區域大小
		//加入sava按鈕，標題，文字
		add(save);
		add(field);
		add(area);
		Savelistener save_listener = new Savelistener();//設定listener
		save.addActionListener(save_listener);//將listener放入save按鈕中
	}
	public class Savelistener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFrame frame = new JFrame("訊息");
			String text = new String();
			try {
				if (field.getText() == null){//如果使用者未輸入文字
					throw new Exception();
				} else {//如果有輸入
					text = "File " + field.getText() + ".txt saved!";//顯示已儲存
					File file = new File(field.getText());//打開文件
					PrintWriter output = new PrintWriter(file);
					output.print(area.getText());//將文字寫入文件中
					output.close();
				}
			} catch (Exception g){//顯示"File name can't be empty!"
				text = "File name can't be empty!";
			}
			JOptionPane.showMessageDialog(frame,text);
		}
	}
}