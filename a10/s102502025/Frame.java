package ce1002.a10.s102502025;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame implements ActionListener {
	JTextField txt = new JTextField(28);// 開txt輸入指令
	JButton button = new JButton();// 開botton
	JTextArea txtarea = new JTextArea("", 10, 35);// 設框
	JPanel panel = new JPanel();

	public Frame() {
		Container c = this.getContentPane();
		button.setText("Save"); // button名
		txtarea.setLineWrap(true); // 換行

		panel.add(button);
		panel.add(txt);
		panel.add(txtarea);

		c.add(panel);
		button.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ("".equals(txt.getText())) { // 當txt是空白時
			final JFrame fr = new JFrame("提示"); // 提示
			fr.setLayout(new BorderLayout());
			ImageIcon icon = new ImageIcon(
					"src/ce1002/a10/s102502025/images.jpg");
			JButton button = new JButton("確定"); // 加button
			JPanel pn = new JPanel();
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();
			pn1.setSize(100, 50);
			JLabel label = new JLabel();
			fr.setSize(250, 135);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("File name can't be empty!"); // label output
			pn.add(new JLabel(icon));
			pn.add(label);
			pn1.add(button);
			fr.add(pn, BorderLayout.CENTER);
			fr.add(pn1, BorderLayout.SOUTH);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose(); // 當按下button時關閉提示
				}
			});
		} else {
			try {
				File file = new File(txt.getText() + ".txt"); // 創意txt檔
				BufferedWriter output = new BufferedWriter(new FileWriter(file)); // save下寫的東西
				output.write(txtarea.getText()); // save textarea output
				output.close();
			} catch (IOException e) {
				e.printStackTrace(); // 輸入
			}
			final JFrame fr = new JFrame("提示");
			fr.setLayout(new BorderLayout());
			ImageIcon icon = new ImageIcon(
					"src/ce1002/a10/s102502025/images.jpg");
			JButton button = new JButton("確定");
			JPanel pn = new JPanel();
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();
			pn1.setSize(100, 50);
			JLabel label = new JLabel();
			fr.setSize(250, 135);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("File " + txt.getText() + ".txt is saved!");
			pn.add(new JLabel(icon));
			pn.add(label);
			pn1.add(button);
			fr.add(pn, BorderLayout.CENTER);
			fr.add(pn1, BorderLayout.SOUTH);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();
				}
			});
		}
	}
}