package ce1002.a10.s102502526;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JTextField text = new JTextField();
	JTextArea textA = new JTextArea();
	String title;
	String content;
	MyPanel(){
		
		setBorder(new LineBorder(Color.white,5));
		setLayout(null);
		setBounds(0, 0, 350, 400);
		
		
	}
	public void bt(){																//set a button
		JButton bt = new JButton("Save");
		bt.setBounds(20, 20, 70, 30);
		bt.addActionListener(new ActionListener() {									//不是很了解這行
			
			@Override
			public void actionPerformed(ActionEvent e) {						
				// TODO Auto-generated method stub
				title=text.getText();
				if(text.getText().isEmpty())										
				JOptionPane.showMessageDialog(null, "File name can’t be empty!");	
				else {															
				JOptionPane.showMessageDialog(null,"File "+ title+".txt saved");
				PrintWriter out;
				try {
					
					out = new PrintWriter(title+".txt");							//output
					content=textA.getText();
					out.println(content);
					out.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				}
			}
		});
		add(bt);
	}
	

	public void textF (){															//textField
		text.setBounds(100, 20, 200, 30);
		setVisible(true);
		add(text);
		
}
	public void textA(){															//textArea
		textA.setLineWrap(true);
		textA.setBounds(20, 70, 300, 300);
		add(textA);
	}
}
