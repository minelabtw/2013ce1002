package ce1002.a10.s102502555;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class MyPanel extends JPanel{
	JButton button = new JButton("Save");
	JTextArea textArea = new JTextArea();
	JTextField textField = new JTextField();
	
	MyPanel(){
		setLayout(null);
		setSize(500, 500);
		
		//設定邊界
		button.setBounds(5, 5, 75, 20);
		textField.setBounds(80, 5, 325, 20);
		textArea.setBounds(5, 25, 400, 350);
		textArea.setLineWrap(true);
		
		//加上去
		button.addActionListener(new SaveListener());
		add(button);
		add(textArea);
		add(textField);
		
	}  //end MyPanel
	
	class SaveListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			DataOutputStream outputStream;
			
			if(textField.getText().isEmpty() == true){  //檔名不能是空的
				JOptionPane.showMessageDialog(null,"File name can't be empty");
			}else{  //檔名不是空的就存起來
				try{
					JOptionPane.showMessageDialog(null, ("File " + textField.getText() + ".txt saved!"));
					outputStream = new DataOutputStream(new FileOutputStream(textField.getText() + ".txt"));
					outputStream.writeBytes(textArea.getText());
				}catch(Exception e1){
					
				}
			}
				
		}  //end actionPerformed
		
	}  //end SaveListener
	
	
	
}
