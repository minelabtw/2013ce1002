package ce1002.a10.s102502542;

import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;

public class MyFrame extends JFrame implements ActionListener
{
	MyFrame()
	{
		setTitle("A10-102502542");
		setSize(487,395);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);//將frame視窗固定在center
		setLayout(null);//設定排版
		
	    final JTextField textfield = new JTextField();
		textfield.setBounds(70,0,400,30);
		
		final JTextArea textarea = new JTextArea();
        textarea.setBounds(3,35,465,310);
		textarea.setLineWrap(true);
		
		JButton button = new JButton("Save");
		button.addActionListener( new ActionListener()//監聽使用者輸入事件
		{
			public void actionPerformed(ActionEvent e) 
			 {			 
				 if(textfield.getText().isEmpty())//如果textfield內容為empty
					 
					 JOptionPane.showMessageDialog(null,"File name can’t be empty!");
				 
				 else
				 {
					 JOptionPane.showMessageDialog(null,"File " + textfield.getText() +".txt saved!");
				 
				     java.io.File file = new java.io.File(textfield.getText()+".txt");//創txt檔並根據輸入的textfield命名
					 java.io.PrintWriter output = null;
					 
					try 
					{
						output = new java.io.PrintWriter(file);
					} 
					catch (FileNotFoundException a) 
					{
						a.printStackTrace();
					}
					 output.print(textarea.getText());//在創建的txt檔上輸出textarea內容
					 output.close();
				 }
			 }
		});
		button.setBounds(0,0,65,30);
		button.addActionListener(this);
		
		add(button);
		add(textarea);
		add(textfield);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) //override函式
	{
		
	}
}
