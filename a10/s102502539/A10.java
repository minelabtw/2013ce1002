package ce1002.a10.s102502539;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class A10 
{
	public static void main(String[] args) 
	{
		MyFrame frame = new MyFrame();
	}

}

class MyFrame extends JFrame
{
	JButton button = new JButton("Save");
	JTextField field = new JTextField();
	JTextArea area = new JTextArea();
	
	public MyFrame()
	{
		setLayout(null);
		setVisible(true);
		setBounds(200,200,300,300);
		
		/*setLocationRelativeTo(null);	//置中 */
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setTitle("A10-102502539");	//視窗標題
		
		button.setBounds(10,10,80,40);
		field.setBounds(95,10,180,40);
		area.setBounds(10,60,265,195);
		area.setLineWrap(true);	//自動換行
		
		Save save = new Save();
		button.addActionListener(save);
		add(button);
		add(field);
		add(area);
	}
	
	class Save implements ActionListener
	{
		public void actionPerformed(ActionEvent a)
		{
			JLabel label = new JLabel();
			JFrame frame = new JFrame();
			
			if ( field.getText().length() == 0 )
				label.setText("File name can't be empty!");
			else
			{
				try
				{
					DataOutputStream print= new DataOutputStream(new FileOutputStream(field.getText()+".txt"));
					print.writeUTF(field.getText());
					print.close();
					label.setText("File "+field.getText()+ ".txt saved!");
				}
				catch(Exception ex) { }
			}
			
			JOptionPane.showMessageDialog(frame, label);		
		}
	}
}
