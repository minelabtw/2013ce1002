package ce1002.a10.s102502557;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.*;


class frame  implements ActionListener
{

public void actionPerformed(ActionEvent e)
{
    if(MyFrame.access$0(MyFrame.this).getText().isEmpty())
    {
        showEmptyFileNameMsg();
    } else
    {
        String mainText = MyFrame.access$1(MyFrame.this).getText();
        String fileName = (new StringBuilder(String.valueOf(MyFrame.access$0(MyFrame.this).getText()))).append(".txt").toString();
        try
        {
            PrintWriter out = new PrintWriter(fileName);  
            out.println(mainText);
            out.close();
            showFileSavedMsg(fileName);
        }
        catch(FileNotFoundException e1)
        {
            e1.printStackTrace();
        }
    }
}

private void showEmptyFileNameMsg()
{
    JOptionPane.showMessageDialog(frame, "File name can't be empty!");//如果沒有甜任何東西進去 就會有這個訊息
}

private void showFileSavedMsg(String name)
{
    JOptionPane.showMessageDialog(frame, (new StringBuilder("File ")).append(name).append(" saved!").toString());//有填內容就會有這個訊息產生
}

JFrame frame;
final MyFrame this$0;

public (JFrame frame)
{
    this$0 = MyFrame.this;
    super();
    this.frame = frame;
}
}
