package ce1002.a10.s102502522;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame implements ActionListener{
	
	private  JOptionPane op=new JOptionPane();
	private FileWriter f;
	private JTextArea textarea = new JTextArea(15,28);
    private JTextField textfield = new JTextField(20);
    private JButton button = new JButton("Save");  //建立按鈕
	static JFrame frame=new JFrame("A10-102502522");
	public MyFrame(String title){
    	//setTitle(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout(FlowLayout.CENTER));
        frame.add(button);  //加入按鈕
        frame.add(textfield); 
        frame.add(textarea);
        frame.setBounds(500,200,350,350);
        button.addActionListener(this);  //使用者輸入事件
        textfield.addActionListener(this); //使用者輸入事件
        textarea.setLineWrap(true);  //超過就換行
        frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e) {
	    
		if(textfield.getText().equals(""))  //檔名的地方空白
			op.showConfirmDialog(this,"File name can’t be empty!","訊息",JOptionPane.CLOSED_OPTION);  
		
		else{
			try 
			{  //文字寫入txt檔
				f = new FileWriter(textfield.getText()+".txt");
				 f.write(textarea.getText());
	                f.close();
	                op.showConfirmDialog(this,"File "+textfield.getText()+".txt saved!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
			} 
			catch (IOException e1)
			{
				e1.printStackTrace();
			} 
		}	
	}
}
