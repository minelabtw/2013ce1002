package ce1002.a10.s102502528;

import java.io.*;
import java.awt.event.*;

import javax.swing.*;

public class A10 extends JFrame {
	JButton save;
	JTextField filename;
	JTextArea message;

	// configure
	A10() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("A10-102502528");

		save = new JButton("Save");
		save.setBounds(5, 5, 75, 30);
		ActionListener saving = new Saving();
		save.addActionListener(saving);
		add(save);

		filename = new JTextField();
		filename.setBounds(85, 5, 355, 30);
		filename.setColumns(10);
		add(filename);

		message = new JTextArea();
		message.setBounds(5, 40, 435, 215);
		message.setLineWrap(true);
		add(message);

		setSize(450, 300);
		setVisible(true);
		setLocationRelativeTo(null);
		setLayout(null);
	}

	public static void main(String[] args) {
		new A10();
	}

	// button's listener
	class Saving implements ActionListener {

		JFrame frame;
		String name;
		String content;

		Saving() {
		}

		// button's function
		public void actionPerformed(ActionEvent event) {
			name = filename.getText();
			content = message.getText();
			if (name.isEmpty()) {
				JOptionPane.showMessageDialog(null,
						"File name can't be empty!", "A10",
						JOptionPane.ERROR_MESSAGE);
			} else {
				name += ".txt";
				;
				save();
				JOptionPane.showMessageDialog(null, "File " + name + " saved!",
						"A10", JOptionPane.PLAIN_MESSAGE);
			}
		}

		// 存檔
		void save() {
			try {
				FileWriter fw = new FileWriter(name);
				fw.write(content);
				fw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
}
