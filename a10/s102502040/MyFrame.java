package ce1002.a10.s102502040;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame
{
    class SaveButtonActionListener
        implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            if(fileNameArea.getText().isEmpty())
            {
                showEmptyFileNameMsg();
            } else
            {
                String mainText = mainTextArea.getText();
                String fileName = (new StringBuilder(String.valueOf(fileNameArea.getText()))).append(".txt").toString();
                try
                {
                    PrintWriter out = new PrintWriter(fileName);
                    out.println(mainText);
                    out.close();
                    showFileSavedMsg(fileName);
                }
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
            }
        }

        private void showEmptyFileNameMsg()
        {
            JOptionPane.showMessageDialog(frame, "File name can't be empty!");
        }

        private void showFileSavedMsg(String name)
        {
            JOptionPane.showMessageDialog(frame, (new StringBuilder("File ")).append(name).append(" saved!").toString());
        }

        JFrame frame;
        final MyFrame this$0;

        public SaveButtonActionListener(JFrame frame)
        {
            this$0 = MyFrame.this;
            this.frame = frame;
        }
    }

    public MyFrame()
    {
        setTitle("A10-TA");
        setDefaultCloseOperation(3);
        setBounds(0, 0, 500, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        mainTextArea = new JTextArea();
        mainTextArea.setLineWrap(true);
        contentPane.add(mainTextArea, "Center");
        splitPane = new JSplitPane();
        contentPane.add(splitPane, "North");
        fileNameArea = new JTextField();
        splitPane.setRightComponent(fileNameArea);
        fileNameArea.setColumns(10);
        btnSave = new JButton("Save");
        btnSave.addActionListener(new SaveButtonActionListener(this));
        splitPane.setLeftComponent(btnSave);
    }
    
    private JPanel contentPane;
    private JSplitPane splitPane;
    private JTextField fileNameArea;
    private JButton btnSave;
    private JTextArea mainTextArea;
	public static Object access$0(MyFrame myFrame) {
		return null;
	}


}