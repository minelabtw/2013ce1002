package ce1002.a10.s100502022;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	public MyFrame() {
		setTitle("A10");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		//fileName input design
		final JTextField textfield = new JTextField();
		textfield.setBounds(10, 10, 375, 30);
		contentPane.add(textfield);
		//fileData input design
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 52, 450, 250);
		contentPane.add(textArea);
		//Button design
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				//get Data
				String fileName=textfield.getText();
				String Data = textArea.getText();
				try {
					// if we have fileName 
					if(!fileName.isEmpty()){
						FileWriter fw = new FileWriter(fileName+".txt", false);
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(Data);
						JOptionPane.showMessageDialog(null, "File "+fileName+".txt saved!", "�T��", JOptionPane.PLAIN_MESSAGE);
						bw.close();
						fw.close();
					}//have no fileName
					else{
						JOptionPane.showMessageDialog(null, "File name can't be empty!", "�T��", JOptionPane.PLAIN_MESSAGE);
					}
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(394, 10, 75, 30);
		contentPane.add(btnNewButton);
	}
}
