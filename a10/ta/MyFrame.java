package ce1002.a10.ta;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JSplitPane splitPane;
	private JTextField fileNameArea;
	private JButton btnSave;
	private JTextArea mainTextArea;

	/**
	 * Create the frame.
	 */
	public MyFrame() {
		setTitle("A10-TA");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		mainTextArea = new JTextArea();
		mainTextArea.setLineWrap(true);
		contentPane.add(mainTextArea, BorderLayout.CENTER);

		splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.NORTH);

		fileNameArea = new JTextField();
		splitPane.setRightComponent(fileNameArea);
		fileNameArea.setColumns(10);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new SaveButtonActionListener(this));
		splitPane.setLeftComponent(btnSave);
	}

	// handle save action
	class SaveButtonActionListener implements ActionListener {
		JFrame frame;

		public SaveButtonActionListener(JFrame frame) {
			this.frame = frame;
		}

		// invoke when button be clicked
		@Override
		public void actionPerformed(ActionEvent e) {
			if (fileNameArea.getText().isEmpty())// file name must be non-empty
				showEmptyFileNameMsg();
			else {
				String mainText = mainTextArea.getText();
				String fileName = fileNameArea.getText() + ".txt";
				try {
					PrintWriter out = new PrintWriter(fileName);
					out.println(mainText);
					out.close();
					showFileSavedMsg(fileName);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}

		// dialog
		private void showEmptyFileNameMsg() {
			JOptionPane.showMessageDialog(frame, "File name can't be empty!");
		}

		private void showFileSavedMsg(String name) {
			JOptionPane.showMessageDialog(frame, "File " + name + " saved!");
		}
	}

}
