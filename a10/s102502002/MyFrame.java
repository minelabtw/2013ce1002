package ce1002.a10.s102502002;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener {

	protected JButton button = new JButton();
	protected JTextField field = new JTextField();
	protected JTextArea area = new JTextArea();
	protected JFrame frame = new JFrame();
	protected JLabel label = new JLabel();
	protected String strf; // string to save text field
	protected String stra; // string to save text area
	
	MyFrame(){
		setButton();
		setField();
		setArea();
		// set frame title, visibility, layout and bounds
		setTitle("A10-102502002");
		setVisible(true);
		setLayout(null);
		setBounds(300,200,750,500);
		add(button); // add button, text field and text area
		add(field);
		add(area);
	}
	
	private void setButton(){ // set button
		button.setText("Save");
		button.setBounds(0, 0, 90, 45);
		button.setVisible(true);
		button.setLayout(null);
		button.addActionListener(this);
	}
	
	private void setField(){ // set text field
		field.setBounds(92, 0, 635, 45);
		field.setVisible(true);
		field.setLayout(null);
	}
	
	private void setArea(){ // set text area
		area.setBounds(0, 45, 725, 395);
		area.setVisible(true);
		area.setLayout(null);
		area.setLineWrap(true);
	}

	private void setFrame(){ // set message frame
		frame.setTitle("Message");
		frame.setVisible(true);
		frame.setLayout(null);
		frame.setBounds(500,350,350,250);
		frame.add(label);
	}
	
	private void setLabel(){ // set label
		label.setVisible(true);
		label.setLayout(null);
	}
	
	@Override
	public void actionPerformed(ActionEvent ac) { // action performed due to pressing button
		// TODO Auto-generated method stub
		
		try {
			strf = field.getText(); // get text
			stra = area.getText();
			if(strf.isEmpty())
				throw new Exception(); // throw exception if text field is empty
			else{
				PrintWriter pnt = new PrintWriter(new FileWriter(strf+".txt"));//create an print writer for writing to a file
				pnt.println(stra);//output to the file string in the text area
				pnt.close();
				setLabel();
				label.setBounds(110, 90, 300, 20);
				label.setText("File "+strf+".txt saved!");
				setFrame();
			}
				
			
		}
		catch(Exception e) { // catch exception and output the error
			setLabel();
			label.setBounds(110, 90, 300, 20);
			label.setText("File name can't be empty!");
			setFrame();
	   }
	      
	}
}

