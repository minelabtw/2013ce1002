package ce1002.a10.s102502007;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.FileNotFoundException;
public class MyFrame extends JFrame
{
	JPanel panel = new JPanel();
	JButton button = new JButton();
	JTextField field = new JTextField();//title
	JTextArea area = new JTextArea();//content
	ButtonListener listener = new ButtonListener();
		public MyFrame()
		{
			//set up frame attributes
			super("A10-102502007");
			setLayout(new BorderLayout());
			setLocationRelativeTo(null);
			setDefaultCloseOperation(EXIT_ON_CLOSE);
			setResizable(false);
			setSize(500,500);
			setVisible(true);
			
			//set up button attributes 
			button.setText("Save");
			button.setSize(50, 50);
			button.addActionListener(listener);
			
			//set up panel layout
			panel.setLayout(new BorderLayout());
			panel.add(button, BorderLayout.WEST) ;
			panel.add(field, BorderLayout.CENTER) ;
			
			area.setLineWrap(true);
			add(panel, BorderLayout.NORTH);
			add(area, BorderLayout.CENTER) ;
		}
		
		//inner class
		class ButtonListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				/**
				 * 標題空白時，跳出新視窗
				 */
				if(field.getText().equals(""))
				{
					JPanel panel = new JPanel();  
					JLabel empty = new JLabel("File name can't be empty!");
				    panel.add(empty);
				    JOptionPane.showMessageDialog((new JFrame()), panel);
				}
				else
				{
					//build up a new file
					java.io.File file = new java.io.File(field.getText()+".txt");
					try 
					{
						java.io.PrintWriter output = new java.io.PrintWriter(file) ;
						output.println(area.getText());//寫入檔案
						JPanel panel = new JPanel();  
						JLabel saved = new JLabel("File "+ field.getText()+".txt saved!");
					    panel.add(saved);
					    //once the file is built successfully, then create a new frame to display information
					    JOptionPane.showMessageDialog((new JFrame()), panel);
					    output.close();
					} 
					catch (FileNotFoundException ex) 
					{
						ex.printStackTrace();
					}
					finally
					{
						
					}
				}
			}
		}
}
