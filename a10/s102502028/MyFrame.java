package ce1002.a10.s102502028;
import javax.swing.* ;
import java.awt.* ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class MyFrame extends JFrame{
	JButton save = new JButton("Save") ;  //建立按鈕及輸入
    JTextField jtf = new JTextField() ; 
    JTextArea jta = new JTextArea() ;
    
    MyFrame()
    {
    	setTitle("A10-102502028") ; //設定框架
    	setSize(620,410); 
    	setLayout(null) ; 
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
              
    	save.setBounds(5,5,80,40) ;   //save按鈕
    	add(save) ; 
         
    	jtf.setBounds(90,5,500,40) ;  //檔案名稱
    	add(jtf) ; 
              
    	jta.setBounds(5,50,585,310) ;  //檔案內容
    	jta.setLineWrap(true) ; 
    	add(jta) ; 
      
    	save.addActionListener(new saveListener()) ;  //按鍵事件
                       
    	setVisible(true) ;
    }
    
    class saveListener implements ActionListener{ 
        public void actionPerformed(ActionEvent a) { 
        try{ 
            if(jtf.getText().isEmpty())   //檔案名稱不能是空的
            {
            	JOptionPane.showMessageDialog(null, "File name can't be empty!", "A10", JOptionPane.ERROR_MESSAGE);
            }
            else   //建立一格txt檔存起來
            {
            	java.io.File file = new java.io.File(jtf.getText()+".txt") ; 
            	java.io.PrintWriter output = new java.io.PrintWriter(file) ;         
            	output.print(jta.getText());
            	JOptionPane.showMessageDialog(null, "File " +jtf.getText() + ".txt  saved!", "A10", JOptionPane.PLAIN_MESSAGE);
            	output.close() ;
            }           
        } 
        catch(Exception ex){              
        } 
        } 
                           
    } 
      
    
}
