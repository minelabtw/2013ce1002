package ce1002.a10.s102502502;

 import java.awt.BorderLayout;
 import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
 import javax.swing.JButton;
 import javax.swing.JFrame;
 import javax.swing.JOptionPane;
 import javax.swing.JPanel;
 import javax.swing.JSplitPane;
 import javax.swing.JTextArea;
 import javax.swing.JTextField;
 import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame {
   private static final long serialVersionUID = 1L; //to assure compatibility among different versions   
   private JPanel contentPane;                      //set contentPane to place mainTextArea
   private JSplitPane splitPane;                    //set splitPane to place fileNameArea
   private JTextField fileNameArea;
   private JButton btnSave;
   private JTextArea mainTextArea;

   public MyFrame() {
     setTitle("A10-102502502");
     setDefaultCloseOperation(EXIT_ON_CLOSE);
     setBounds(100, 100, 450, 300);
     this.contentPane = new JPanel();
     this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
     this.contentPane.setLayout(new BorderLayout(0, 0));
     setContentPane(this.contentPane);
     this.mainTextArea = new JTextArea();
     this.mainTextArea.setLineWrap(true);                      //to assure the context not to be edited out of the area 
     this.contentPane.add(this.mainTextArea, "Center");        //add the mainTextArea to the center of the panel
     this.splitPane = new JSplitPane();
     this.contentPane.add(this.splitPane, "North");            //add the splitPane to the North part of the panel
     this.fileNameArea = new JTextField();
     this.splitPane.setRightComponent(this.fileNameArea);
     this.fileNameArea.setColumns(10);

     this.btnSave = new JButton("Save");
     this.btnSave.addActionListener(new SaveButtonActionListener(this));  // invoke the listener of the button
     this.splitPane.setLeftComponent(this.btnSave);
   }

   class SaveButtonActionListener implements ActionListener {            // set the customer listener 
         JFrame frame;
         public SaveButtonActionListener(JFrame frame){                  // the constructor
        this.frame = frame;
     }
     public void actionPerformed(ActionEvent e){                         // override the interface implemented
      if (MyFrame.this.fileNameArea.getText().isEmpty()){                // call the function
        showEmptyFileNameMsg();
      }
      else 
      {
    	 String mainText = MyFrame.this.mainTextArea.getText();
         String fileName = MyFrame.this.fileNameArea.getText() + ".txt";
          try
          {
            PrintWriter out = new PrintWriter(fileName);        // put fileName into the file
           out.println(mainText);
            out.close();
            showFileSavedMsg(fileName);                         // call the function to show save message
         }
          catch (FileNotFoundException e1)
          {
          e1.printStackTrace();
          }
        }
      }
 
     private void showEmptyFileNameMsg() {                                       // set the dialog function of EmptyFileNameMsg
        JOptionPane.showMessageDialog(this.frame, "File name can't be empty!");
     }
     private void showFileSavedMsg(String name){                                       // set the dialog function of FileSavedMsg
        JOptionPane.showMessageDialog(this.frame, "File " + name + " saved!");
      }
   }
 }
