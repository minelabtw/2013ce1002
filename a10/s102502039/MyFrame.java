package ce1002.a10.s102502039;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.*;

public class MyFrame extends JFrame {//set frame
	public MyFrame(){
		setLayout(new BorderLayout());
		JButton jbtSave = new JButton("Save");
		JTextField text = new JTextField();
		JTextArea mainText = new JTextArea();
		mainText.setLineWrap(true);
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,2,5,5));
		jbtSave.setSize(60, 10);
		p1.add(jbtSave);
		p1.add(text);
		add(p1,BorderLayout.NORTH);
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1,1));
	    p2.add(mainText);
	    add(p2,BorderLayout.CENTER);
	    SaveListenerClass listener1 = new  SaveListenerClass();
	    jbtSave.addActionListener(listener1);
	}
}
class  SaveListenerClass implements ActionListener//listener
{
	@Override
	public void actionPerformed(ActionEvent e){
		JPanel panel = new JPanel();
		JLabel jbtwarnButton = new JLabel("File name can’t be empty!");
		panel.add(jbtwarnButton);
		 JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
		 JOptionPane.showMessageDialog(frame, panel);
	}

}