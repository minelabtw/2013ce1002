package ce1002.a10.s102502509;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class saveside extends JPanel implements ActionListener // implement the listener
{
	private JButton bt = new JButton("Save"); 
	private JTextField tf = new JTextField();
	private JTextArea ta = new JTextArea();
	private JLabel lab = new JLabel(); // afford the message
	
	saveside()
	{
		// traditional setting 
		setLayout(null);
		setBounds(0, 0, 450, 400);
		
		bt.setBounds(5, 0, 80, 30);
		
		tf.setBounds(90, 0, 370, 30);
		tf.setText(null);
		
		ta.setBounds(5, 35, 450, 370);
		ta.setLineWrap(true);
		
		lab.setBounds(60, 79, 225, 40);
		
		add(bt);
		add(tf);
		add(ta);
		add(lab);
		
		bt.addActionListener(this); // call the event, this is to be a parameter
	}

		public void actionPerformed(ActionEvent e) // must have to create the event action
		{
			try // exception method
			{
				check(tf.getText()); // call the check tf's method
				JOptionPane.showMessageDialog(lab, ("File " + tf.getText() + ".txt saved!"), "訊息",
						                      JOptionPane.INFORMATION_MESSAGE);
				// print the message with label 
				// save the ta's text with tf's name
				FileOutputStream FOS = new FileOutputStream(new File((tf.getText() + ".txt")));

				FOS.write(ta.getText().getBytes());
				FOS.close();
			}
			catch(Exception excep) // encounter the empty situation
			{
				// print the message with label
				JOptionPane.showMessageDialog(lab,  "File name can't be empty!", "訊息",
	                      JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
		public boolean check (String file) throws Exception
		{
			// check method; check the file's name is empty or not
			if(tf.getText().isEmpty())
			{
				throw new Exception(); // false = empty => exception
			}
			else
			{
				return true; // true = exist => do the "try" thing
			}
		}
}
