package ce1002.a10.s102502540;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Text extends JFrame {
	JButton jb = new JButton("Save");
	JTextField jtf = new JTextField();
    JTextArea jta = new JTextArea();

	public Text() { //創造視窗
		setVisible(true);
		setSize(450, 450);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("A10-102502540");
		jta.setLineWrap(true);
		jb.setBounds(0, 0, 100, 50);
		jtf.setBounds(100, 0, getWidth() - 100, 50);
		jta.setBounds(0, 50, getWidth(), getHeight() - 50);
		SaveListener save = new SaveListener();
		jb.addActionListener(save);
		add(jb);
		add(jtf);
		add(jta);
	}

	class SaveListener implements ActionListener { //實作ActionListener
		public void actionPerformed(ActionEvent a) {
			JLabel l = new JLabel();
			JFrame f = new JFrame();
			if (jtf.getText().length() == 0) //如果沒有輸入檔名就按下存檔，要跳出對話框，對話框中顯示File name can't be empty!
				l.setText("File name can't be empty!");
			else {
				try {
					DataOutputStream print = new DataOutputStream(
							new FileOutputStream(jtf.getText() + ".txt"));
					print.writeUTF(jtf.getText());
					print.close();
					l.setText("File " + jtf.getText() + ".txt saved!"); //按下存檔時要跳出對話框，對話框中顯示File 檔名.txt saved!
					} catch (IOException e) {
				}
			}
			JOptionPane.showMessageDialog(f, l);
		}
	}
}
