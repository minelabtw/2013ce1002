package ce1002.a10.s102502042;
import javax.swing.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class MyFrame extends JFrame{
	private JTextField field = new JTextField();
	private JButton btn = new JButton();
	private JTextArea text = new JTextArea();
	private JPanel panel = new JPanel();
	
	//constructor
	MyFrame()
	{
		text.setLineWrap(true);
		setTitle("A10-102502042");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(500,500);
		setLayout(null);
		
		//set panel
		panel.setLayout(null);
		panel.setSize(490,490);
		
		//set button
		btn.setLocation(0,0);
		btn.setSize(80,50);
		btn.setText("Save");
		btn.addActionListener(new Save(this));
		
		//set field
		field.setLocation(80,0);
		field.setSize(410,50);
		
		//set text
		text.setLocation(0,60);
		text.setSize(480,430);
		
		panel.add(btn);
		panel.add(field);
		panel.add(text);
		
		this.add(panel);
	}
	
	//實作listener
	class Save implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			if (field.getText().isEmpty())
		    {
		        Empty();
		    }
		    else
		    {
		        String Text = text.getText();
		        String fileName = field.getText() + ".txt";
		        try
		        {
		        	PrintWriter out = new PrintWriter(fileName);
		        	out.println(Text);
		        	out.close();
		          	Success(fileName);
		        }
		        catch (IOException e1)
		        {
		        	e1.printStackTrace();
		        }
		    }
		}
		
	    public Save(JFrame frame)
	    {
	    }
	    
	    //檔名為空的狀況
	    private void Empty()
	    {
	    	JOptionPane.showMessageDialog(null, "File name can't be empty!");
	    }
	    
	    //成功儲存
	    private void Success(String name)
	    {
	    	JOptionPane.showMessageDialog(null, "File " + name + " saved!");
	    }
	}
}
