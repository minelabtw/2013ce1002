package ce1002.a10.s102502512;
import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.io.FileNotFoundException;

import javax.swing.event.*;
import javax.swing.event.*;


public class MainFrame extends JFrame{
	private JButton save = new JButton("save");
	private JTextField title = new JTextField(35);
	private JTextArea conta = new JTextArea();
	private String warring ;
	private String message;
	
	MainFrame()
	{	
		setLayout(null);								//Line 21-29: 排版面
		save.setBounds(2,2,65,30);
		this.add(save);
		title.setBounds(80,2,400,30);
		this.add(title);
		conta.setBounds(2,40,480,290);
		this.add(conta);
		conta.setLineWrap(true);						//自動換行
		save.addActionListener(new hello());
	}
	
	public void MessageBox(String message)				//Line 32-41: 設一個會跳出
	{	
		this.message = message;
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(200, 200));
		JFrame frame = new JFrame("訊息");
		JLabel label = new JLabel(message);
		panel.add(label);
		JOptionPane.showMessageDialog(frame, panel);
	  }
	
	class hello implements ActionListener				//!!!!!!! Line 43-46: except hello, other things "must" be liking this: class *** implements ActionListener%n @Override%n public void actionPerformed(ActionEvent e){}
	{
		@Override
		public void actionPerformed(ActionEvent e)		//!!!!!!! this line must be exits
		{
			try
			{	
				if(title.getText().isEmpty())			//new!!!! text isn't the title empty or not
				{
					throw new Exception("File name can't be empty!");
				}
				MessageBox("File "+title.getText()+".txt saved!");
				java.io.File file = new java.io.File(title.getText()+".txt");	//new!!!!!! Line 55-58: save the file to .txt type
				java.io.PrintWriter output = new java.io.PrintWriter(file);
				output.print(conta.getText());
				output.close();
			}
			catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();											//new!!!!!! printStackTrace()方法的意思是：在命令行打印異常信息在程序中出錯的位置及原因。
				warring = e1.getMessage();
				MessageBox(warring);
			}
		}
	}//class savelistner's end
}
