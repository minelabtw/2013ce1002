package ce1002.a10.s995002046;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

class A10{
	JFrame myframe;
	JButton mybutton;
	JTextField myfield;
	JTextArea myarea;
	A10(){
		createGui(); 
	}

	private void createGui() {
		// TODO Auto-generated method stub
		//create frame and initialize it
		myframe=new JFrame("A10-995002046");
		myframe.setLayout(null);
		myframe.setSize(500, 300);
		myframe.setVisible(true);
        myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //create button and initialize it
        mybutton= new JButton("Save");
		mybutton.addActionListener(buttonclick);
		//add button and set bound
        myframe.add(mybutton);
        mybutton.setBounds(0, 0, 80, 30);
        //create text field and initialize it
        myfield=new JTextField();
        //add text field and set bound
        myframe.add(myfield);
        myfield.setBounds(80, 0, 200, 30);
        //create text area and initialize it
        myarea=new  JTextArea();
        myarea.setLineWrap(true);
        //add text area and set bound
        myframe.add(myarea);
        myarea.setBounds(0, 35, 280, 200);
	}
	//save button click event
	ActionListener buttonclick= new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(myfield.getText().contentEquals("")){//text field is empty then show dialog
				JOptionPane.showMessageDialog(myframe, "File name can��t be empty!");
			}
			else{//text field is not empty then create file and save it
				try {						 
					File file = new File("src/ce1002/a10/s995002046/"+myfield.getText()+".txt");
		 
					// if file doesnt exists, then create it
					if (!file.exists()) {
						file.createNewFile();
					} 
					FileWriter fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(myarea.getText());//write text of text area
					bw.close();
		 
				} catch (IOException e) {
					e.printStackTrace();
				}
				//show dialog that saved
				JOptionPane.showMessageDialog(myframe, "File"+ myfield.getText()+".txt saved");
			}
		}
		
	};
	
	public static void main(String[] args) {
		new A10();
	}
}