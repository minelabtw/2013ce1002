package ce1002.a10.s102502006;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyPanel extends JPanel{
	
	JTextField textF = new JTextField(); // 檔名區
	JTextArea textA = new JTextArea(); // 內文曲
	String title; // 檔名
	String content; // 內文
	MyPanel(){
		setLayout(null);
		setBounds(0, 0, 350, 400);
	}
	
	public void bt(){														
		JButton bt = new JButton("Save");
		bt.setBounds(20, 20, 70, 30);
		bt.addActionListener(
		new ActionListener() 
		{									//button  actionlistener
			
			public void actionPerformed(ActionEvent e) {							// 執行事件

				title=textF.getText();
				if(textF.getText().isEmpty())										
				JOptionPane.showMessageDialog(null, "File name can’t be empty!");
				else {																
				JOptionPane.showMessageDialog(null,"File "+ title+".txt saved");
					try {
						PrintWriter out = new PrintWriter(title+".txt");					
						content=textA.getText();
						out.println(content);
						out.close();
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		add(bt);
	}
	
	public void textF (){															//textField
		textF.setBounds(100, 20, 200, 30);
		add(textF);
	}
	
	public void textA(){															//textArea
		//textA.setLineWrap(true);
		textA.setBounds(20, 70, 300, 300);
		add(textA);
	}
}
