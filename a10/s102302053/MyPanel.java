package ce1002.a10.s102302053;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class MyPanel extends JPanel{
	JTextField FileName = new JTextField();//儲存檔名的欄位
	JTextArea textArea = new JTextArea();//文字輸入的位置
	JButton saveButton = new JButton();//儲存檔案鈕
	
	MyPanel(){//加入各項元件 並且執行actionlistner
		setLayout(null);
		setSplitPane();
		setTextArea();
		setActionListner();
		
	}
	private void setSplitPane(){//分隔FileName和SaveButton並讓使用者可拖拉兩者長度
		setSaveButton();
		setTextField();
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,false,saveButton,FileName);
		splitPane.setBounds(20, 10, 550, 35);
		splitPane.setDividerSize(10);
		add(splitPane);
	}
    private void setTextArea(){//設置內容輸入欄位
    	textArea.setBorder(BorderFactory.createLineBorder(Color.black));
    	textArea.setLineWrap(true);   
    	textArea.setBounds(20,50,550,400);
		add(textArea);
	}
 
	private void setTextField(){//設置檔名輸入欄位
		FileName.setColumns(100);
		
	}
	
	private void setSaveButton(){//儲存鈕設置
		saveButton.setText("Save");
	}
	private void setActionListner(){//當按下save後 執行saveFile()
		saveButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				saveFile();
			}
		});
	}
	private void saveFile() {//按下save後執行
		 if (FileName.getText().equals("")) {//當檔名欄位是空的的時候 輸出提示訊息
				JFrame savefail = new JFrame("訊息");
				JLabel text = new JLabel("File name can't be empty!");
				JOptionPane.showMessageDialog(savefail, text);//輸出儲存失敗的提示訊息
				
			}
		 else{//當檔名欄位不是空的 就儲存檔案 並輸出提示訊息
			File file = new File(FileName.getText()+".txt");//以檔名欄位裡的文字作為檔名
			try {
				BufferedWriter buf = new BufferedWriter(new FileWriter(file));//儲存文字輸入區的內容
				buf.write(textArea.getText());
				buf.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			JFrame savesucceed = new JFrame("訊息");
			JLabel text = new JLabel("File " + FileName.getText() +".txt saved!");
			JOptionPane.showMessageDialog(savesucceed, text);//輸出儲存成功的提示訊息
		}	   
	}

}
