package ce1002.a10.s102502556;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
		JButton submit = new JButton("Save"); //宣告Button、TextField和TextArea
		JTextField name = new JTextField();
		JTextArea content = new JTextArea();
		MyFrame() {
			setTitle("A10-102502556"); //設定標題、大小、位置、關閉、排版
			setSize(420,400);
			setLocationRelativeTo(null);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setLayout(null);
			submit.setSize(80, 20); //設定三種物件的大小、位置
			submit.setLocation(0, 0);
			name.setSize(320, 20);
			name.setLocation(80, 0);
			content.setSize(400, 340);
			content.setLocation(0, 20);
			content.setLineWrap(true); //設定自動換行
			add(submit);
			add(name);
			add(content);
			submit.addActionListener(new SaveListener()); //設定處理按按鈕的事件
			setVisible(true); //設定視窗可被顯示
		}
		boolean fileOutput () {
			 
			try {
				
				if ( name.getText().isEmpty() ) //檢查檔名是否為空
				{
					new MessageBox("File name can’t be empty!");
					return false;		
				}
				java.io.File file = new java.io.File(name.getText()+".txt");
				java.io.PrintWriter output = new java.io.PrintWriter(file);
				output.print(content.getText()); //印出內容到txt檔
				output.close(); //關閉output
			}
			catch(Exception e)
			{
				return false;
			}
			return true;
		}
		class SaveListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				if(fileOutput()) {
					new MessageBox("File " + name.getText() + ".txt saved!");
				}
			}
		}
}
