package ce1002.a10.s102502501;
import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class MyFrame extends JFrame implements ActionListener
{
	private JOptionPane JO=new JOptionPane();
	private FileWriter FW;//先宣告下面再實作
	private JTextArea Textarea = new JTextArea(15,28);//建立打檔名的地方
    private JTextField Textfield = new JTextField(20);//建立打內容的地方
    private JButton button = new JButton("Save");  //建立按鈕物件
	
	public MyFrame(String title)
	{
    	setTitle(title);
    	setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new FlowLayout(FlowLayout.CENTER));
        getContentPane().add(button);  //加入按鈕
        getContentPane().add(Textfield); 
        getContentPane().add(Textarea);
        setBounds(500,200,350,350);
        button.addActionListener(this);  //監聽使用者輸入事件
        Textfield.addActionListener(this); //監聽使用者輸入事件
        Textarea.setLineWrap(true);  //超過空白區域就換行
    }
	
	public void actionPerformed(ActionEvent e)
	{
	    
		if(Textfield.getText().equals(""))  //若輸入檔名的地方空白
			JO.showConfirmDialog(this,"File name can’t be empty!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
		//如果有輸入檔名
		else
		{
			try
			{  //將文字寫入txt檔
				FW = new FileWriter(Textfield.getText()+".txt");
				 FW.write(Textarea.getText());
	                FW.close();
	                JO.showConfirmDialog(this,"File "+Textfield.getText()+".txt saved!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
           
		}	
	}
} 

