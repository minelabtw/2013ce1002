package ce1002.a10.s102502014;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener { // 實作ActionListener介面
	JButton Save = new JButton("Save"); // 按鈕
	JTextField Title = new JTextField(); // 標題
	JTextArea Area = new JTextArea(); // 內容

	public MyFrame() {
		Save.addActionListener(this);
		Save.setSize(100, 40);
		Save.setLocation(0, 0);
		Title.setSize(400, 40);
		Title.setLocation(100, 0);
		Area.setSize(500, 560);
		Area.setLocation(0, 35);
		Area.setLineWrap(true);
		add(Save);
		add(Title);
		add(Area);
		this.setTitle("A10-102502014");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null); // 非預設排版
		setSize(520, 650); // 設置大小
		this.setVisible(true);
	}
    @Override
	public void actionPerformed(ActionEvent e) {   
		save();
	}

	public void save() {
		if ("".equals(Title.getText().trim())) { //trim去除空白
			JFrame b = new JFrame();   //空白輸入時跳出的視窗
			JLabel a = new JLabel("File name can't be empty!");
			JOptionPane.showMessageDialog(b, a);
		} else {
			JFrame b = new JFrame();   //成功輸入時跳出的視窗
			JLabel a = new JLabel("File " + Title.getText() + ".txt saved!");
			JOptionPane.showMessageDialog(b, a);
			FileWriter out;
			try {                      //檔案輸出
				out = new FileWriter(Title.getText() + ".txt", true);
				out.write(Area.getText());
				out.close();
			} catch (IOException e_) {
				e_.printStackTrace();
			}
		}
	}
}
