package ce1002.a10.s102502049;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyPanel extends JPanel{
	private JButton jbtSave = new JButton("Save");
	private JTextField jtf = new JTextField();
	private JTextArea jta = new JTextArea();
	MyPanel(){
		jta.setLineWrap(true);

		jbtSave.addActionListener(new ActionListener() { // add ActionListener by anonymous inner class
			@Override
			public void actionPerformed(ActionEvent e){
				try{ // try to write file
					String fileName="";
					if(jtf.getText().length() != 0) // check empty to create FileNotFoundException
						fileName = jtf.getText()+".txt";
					File file = new File(fileName); // create file named after fileName
					PrintWriter output = new PrintWriter(file);
					output.print(jta.getText()); // write the file
					JFrame opFrame = new JFrame();	// JOptionPane
					JOptionPane.showMessageDialog(opFrame, "File "+ fileName +" saved!");
					output.close();
				}
				catch(FileNotFoundException exception){ // file not found
					JFrame opFrame = new JFrame();	// JOptionPane
					JOptionPane.showMessageDialog(opFrame, "File name can��t be empty!");
				}
			}
		});// end inner-class
		
		add(jbtSave);
		add(jta);
		add(jtf); // add all components
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		jbtSave.setBounds(0, 0, (int) (getWidth() / 8),	(int) (getHeight() / 12)); // set component size and position
		jtf.setBounds((int) (getWidth() / 8), 0,	(int) (getWidth() / 8 * 7)+6, (int) (getHeight() / 12));
		jta.setBounds(0, (int) (getHeight() / 12), getWidth(),(int) (getHeight() / 12 * 11));
	}
}
