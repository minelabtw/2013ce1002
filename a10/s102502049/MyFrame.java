package ce1002.a10.s102502049;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	private MyPanel panel= new MyPanel();

	MyFrame() {
		setTitle("A10-102502049");
		setSize(700, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		add(panel);
	} // end constructor
}
