package ce1002.a10.s101201003;
import javax.swing.JFrame;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JTextField;



import java.awt.event.ActionEvent;
import java.io.FileWriter;


public class MyFrame extends JFrame implements ActionListener{
	
	private JTextField textfield;
	private JTextArea textarea;
	private JButton save;
	
	public MyFrame() {
		//set the data
		setTitle("A10-101201003");
		setVisible(true);
		setSize(new Dimension(450, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	
		
		
		textarea=new JTextArea();
		getContentPane().add(textarea,BorderLayout.CENTER);
		
		JSplitPane splitpane = new JSplitPane();
		getContentPane().add(splitpane,BorderLayout.NORTH);
		
		save=new JButton("Save");
		splitpane.setLeftComponent(save);
		save.addActionListener(null);
		
		textfield=new JTextField();
		splitpane.setRightComponent(textfield);
		textfield.setColumns(12);
		
	
	}

	@Override
	public void actionPerformed(ActionEvent e){
		// TODO Auto-generated method stub
		if(textfield.getText().isEmpty())
			JOptionPane.showMessageDialog(null, "File name can't be empty!","�T��",JOptionPane.WARNING_MESSAGE);
		else{
			try{
				FileWriter filewriter=new FileWriter(textfield.getText()+".txt");
				filewriter.write(textarea.getText());
				filewriter.close();
				JOptionPane.showMessageDialog(null,"File "+textfield.getText()+".txt saved!","�T��",JOptionPane.INFORMATION_MESSAGE);		
			}
			catch(Exception e2){
				JOptionPane.showMessageDialog(null, "File name can't be empty!","�T��",JOptionPane.WARNING_MESSAGE);			
		
			}
		}
	}
}
	
	
	
	
	
	
	


