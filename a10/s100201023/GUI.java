package ce1002.a10.s100201023;

import javax.swing.JFrame;

import java.awt.Dimension;

import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.AbstractAction;

import java.awt.event.ActionEvent;
import java.io.FileWriter;

import javax.swing.Action;

public class GUI extends JFrame implements ActionListener
{
	private JTextField textField;
	private JTextArea textArea;
	private JButton btnSave;

	public GUI() 
	{
		setTitle("A10-100201023");
		setVisible(true);
		setSize(new Dimension(450, 300));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		textArea = new JTextArea();
		getContentPane().add(textArea, BorderLayout.CENTER);
		
		JSplitPane splitPane = new JSplitPane();
		getContentPane().add(splitPane, BorderLayout.NORTH);
		
		btnSave = new JButton("Save");
		splitPane.setLeftComponent(btnSave);
		btnSave.addActionListener(this);
		
		textField = new JTextField();
		splitPane.setRightComponent(textField);
		textField.setColumns(10);

	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		if(textField.getText().isEmpty())
			JOptionPane.showMessageDialog(null, "File name can’t be empty!", "Warning!!", JOptionPane.WARNING_MESSAGE);
		else 
		{
			try
			{
				FileWriter fileWriter = new FileWriter(textField.getText() + ".txt");
				fileWriter.write(textArea.getText());
				fileWriter.close();
				JOptionPane.showMessageDialog(null, "File " + textField.getText() + ".txt saved!", "Saving success!!", JOptionPane.INFORMATION_MESSAGE);
			}
			catch (Exception e2)
			{
				// TODO: handle exception
				JOptionPane.showMessageDialog(null, "File name can’t be empty!", "Warning!!", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
