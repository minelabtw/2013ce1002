
package ce1002.a10.s102502051;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame
{
    class SaveButtonActionListener implements ActionListener
    {

        public void actionPerformed(ActionEvent e)
        {
            if(fileNameArea.getText().isEmpty()) //獲得檔案名
            {
                showEmptyFileNameMsg();
            } else 
            {
                String mainText = mainTextArea.getText();   //
                String fileName = (new StringBuilder(String.valueOf(fileNameArea.getText()))).append(".txt").toString();
                try
                {
                    PrintWriter out = new PrintWriter(fileName);  
                    out.println(mainText);
                    out.close();
                    showFileSavedMsg(fileName);
                }
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
            }
        }

        private void showEmptyFileNameMsg()    //檔案名不能為空
        {
            JOptionPane.showMessageDialog(frame, "File name can't be empty!");
        }

        private void showFileSavedMsg(String name) //提醒你檔案已儲存
        {
            JOptionPane.showMessageDialog(frame, (new StringBuilder("File ")).append(name).append(" saved!").toString());
        }

        JFrame frame;
        final MyFrame this1;

        public SaveButtonActionListener(JFrame frame)
        {
            this1 = MyFrame.this;
  
            this.frame = frame;
        }
    }


    public MyFrame()
    {    
        setTitle("A10-S102502051");      //設定
        setDefaultCloseOperation(3);
        setBounds(200, 200, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);//把contentPane對象設置成為frame的內容面板
        mainTextArea = new JTextArea();
        mainTextArea.setLineWrap(true);
        contentPane.add(mainTextArea, "Center");
        splitPane = new JSplitPane();
        contentPane.add(splitPane, "North");
        fileNameArea = new JTextField();
        splitPane.setRightComponent(fileNameArea);
        fileNameArea.setColumns(10);
        btnSave = new JButton("Save");
        btnSave.addActionListener(new SaveButtonActionListener(this));
        splitPane.setLeftComponent(btnSave);
    }

   
    private JPanel contentPane;
    private JSplitPane splitPane;
    private JTextField fileNameArea;
    private JButton btnSave;
    private JTextArea mainTextArea;
	

}
