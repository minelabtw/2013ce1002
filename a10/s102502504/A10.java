package ce1002.a10.s102502504;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;


public class A10 extends JFrame implements ActionListener
{
	private JButton btn = new JButton(); //按鈕
	private JTextField filename = new JTextField(); //檔名
	private JTextArea text = new JTextArea(); //內容
	
	String name;
    String content;
	
	public static void main(String[] args)  //有關frame的東西
	{
		A10 a10 = new A10(); 
		a10.setTitle("A10-102502504"); //設標題
		a10.setSize(450,300); 
		a10.setLocationRelativeTo(null); //置中
		a10.setDefaultCloseOperation(A10.EXIT_ON_CLOSE);
		a10.setVisible(true); //設為可見
	}

	public A10()
	{
		setLayout(null); //不使用版面配置
		
		btn.setBounds(5,5,80,30); //button的大小
		btn.setText("Save"); //名為save的button
		ActionListener saving = new Saving(this);
		btn.addActionListener(saving);
		
		filename.setBounds(90,5,340,30); //標題名稱大小
		filename.setEditable(true);//可編輯
		
		text.setBounds(5, 40, 420, 290); //輸入文字處的大小
		text.setLineWrap(true); //可換行
		
		add(btn);
		add(filename);
		add(text);
	}
	
	 class Saving implements ActionListener 
	 {
		 JFrame frame;
		 String name;
		 String content;
		
		 Saving(JFrame frame) 
		 {
			 this.frame = frame;
		 }
	
		 public void actionPerformed(ActionEvent event)
		 {
			 name = filename.getText() + ".txt";
			 name = filename.getText();
			 content =text.getText();
			 if(name.isEmpty())  //如果filename是空的話
			 {
				 JOptionPane.showMessageDialog(frame, "File name can't be empty!");
			 } 
			 else //如果filename不是空的話
			 {
				 name += ".txt";
				 save_into_file();
				 JOptionPane.showMessageDialog(frame, "File " + name + " saved!");
			 }
		 }
		 
		 void save_into_file() 
		 {
			 try 
			 {
				 FileWriter f = new FileWriter(name);
				 f.write(content);
				 f.flush();
			 } 
			 catch (IOException e)
			 {
				 e.printStackTrace();
			 }
		}
	 }

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
}

	
	