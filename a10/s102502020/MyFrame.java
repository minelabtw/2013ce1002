package ce1002.a10.s102502020;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;

public class MyFrame extends JFrame {
	
	private JButton button = new JButton("Save");                                 //宣告一個JButton
	private JTextArea textarea = new JTextArea();                                 //宣告一個JTextArea
	private JTextField textfield = new JTextField();                              //宣告一個JTextField

	public MyFrame() {                                                            //建構子
		// TODO Auto-generated constructor stub
		setTitle("A10-102502020");                                                //標頭
		setSize(450,300);                                                         //大小
		setLocationRelativeTo(null);                                              //位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                           //可關閉視窗
		setLayout(null);                                                          //預設排版
		setVisible(true);                                                         //看得到
		
		button.setBounds(5, 5, 65, 30);                                           //設定位置和大小
		button.addActionListener(new SaveFile());                                 //會觸發下面的函式
		add(button);
		
		textfield.setBounds(75, 5, this.getWidth()-95, 30);                       //可隨著視窗調整大小
		add(textfield);
		
		textarea.setBounds(5, 35, this.getWidth()-25, this.getHeight()-75);
		textarea.setLineWrap(true);                                               //會自動換行
		add(textarea);
		repaint();                                                                //重新繪圖
	}
	class SaveFile implements ActionListener {
		String filename;                                                          //宣告參數
		String ext = "txt";
		SaveFile(){	
		}
		public void actionPerformed(ActionEvent event) {
			filename = textfield.getText();                                       //得到檔名
			if (filename.isEmpty()) {                                             //如果檔名是空的
				JOptionPane.showMessageDialog(null, "File name can't be empty!", "訊息", JOptionPane.INFORMATION_MESSAGE);      //跳出視窗
			}
			else {
				try {
					FileWriter file = new FileWriter(filename + "." + ext);       //儲存檔案
	                file.write(textarea.getText());
	                file.flush();
					JOptionPane.showMessageDialog(null, "File " + filename + "." + ext + " saved!", "訊息", JOptionPane.INFORMATION_MESSAGE);
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		
	}

}
