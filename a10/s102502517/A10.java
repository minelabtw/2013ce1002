package ce1002.a10.s102502517;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JTextArea;

public class A10 extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField textField;
	private JTextArea textArea;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A10 frame = new A10();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public A10() {
		setTitle("A10-102502517");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("Save");
		btnNewButton.setBounds(5, 5, 67, 23);
		btnNewButton.addActionListener(this);
		contentPane.add(btnNewButton);

		textField = new JTextField();
		textField.setBounds(82, 6, 342, 21);
		contentPane.add(textField);
		textField.setColumns(10);

		textArea = new JTextArea();
		textArea.setBounds(0, 41, 434, 220);
		contentPane.add(textArea);
	}

	public void actionPerformed(ActionEvent e) {
		if (textField.getText().equals(""))
			JOptionPane.showMessageDialog(new JFrame(),
					"File name can't be empty!"); //若textField內為空白則跳出訊息
		else {
			File file = new File(textField.getText());
			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}

			FileWriter dataFile = null;
			try {
				dataFile = new FileWriter(file);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			BufferedWriter input = new BufferedWriter(dataFile);
			try {
				input.write(textArea.getText());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				input.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			JOptionPane.showMessageDialog(new JFrame(),
					"File " + textField.getText() + ".txt saved!"); //儲存後跳出訊息
		}
	}
}
