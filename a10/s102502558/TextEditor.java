package ce1002.a10.s102502558;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TextEditor extends JFrame
{
	JButton btn_save;
    JTextField txt_filename;
    JTextArea txt_content;
    
	TextEditor()
	{
		// basic setup
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(null);
        setTitle("A10-102502558");
        setSize(450, 300);
        
        // build user interface
        buildUI();
	}
	
	private void buildUI()
	{
		// add a button with click event
		btn_save = new JButton("Save");
		btn_save.setBounds(5, 5, 75, 30);
		btn_save.addActionListener(new SaveFile());
		add(btn_save);
		
		// filename
		txt_filename = new JTextField();
		txt_filename.setBounds(85, 5, 355, 30);
		txt_filename.setColumns(10);
		add(txt_filename);
		
		// content
		txt_content = new JTextArea();
		txt_content.setBounds(5, 40, 435, 215);
		txt_content.setLineWrap(true);
		add(txt_content);
	}
	
	class SaveFile implements ActionListener
	{
		String filename;
		String ext = "txt";
		SaveFile(){}
		
		@Override
		public void actionPerformed(ActionEvent event)
		{
			// get filename
			filename = txt_filename.getText();
			if (filename.isEmpty())
			{
				// if no filename then show error
				JOptionPane.showMessageDialog(null, "File name can't be empty!", "A10", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				try
				{
					// save file
					FileWriter fout = new FileWriter(filename + "." + ext);
	                fout.write(txt_content.getText());
	                fout.flush();
					JOptionPane.showMessageDialog(null, "File " + filename + "." + ext + " saved!", "A10", JOptionPane.PLAIN_MESSAGE);
				}
				catch(IOException ex)
				{
					ex.printStackTrace();
				}
			}
		}
	}
}
