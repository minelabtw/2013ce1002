package ce1002.a10.s102502026;

import javax.swing.JFrame;

public class A10 {

	public static void main(String[] args) {
		//add the frame and open it
		Frame frame = new Frame();
		frame.setTitle("A10-102502026");	//name of the frame
		frame.setSize(415, 260);	//size of the frame
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);		//visible it
	}
}
