package ce1002.a10.s102502026;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame implements ActionListener {
	JTextField txt = new JTextField(28);	//set textfield and its large for frame
	JButton button = new JButton();			//set button	for frame
	JTextArea txtarea = new JTextArea("", 10, 35);	//set textarea	for frame
	JPanel panel = new JPanel();		//set panel	for frame	

	public Frame() {
		Container c = this.getContentPane();	//for container
		button.setText("Save");					//type save on button
		txtarea.setLineWrap(true);				//for auto change line
		// add to panel
		panel.add(button);					
		panel.add(txt);
		panel.add(txtarea);
		// add panel in container
		c.add(panel);
		button.addActionListener(this);	//do actionperformed when press button
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if ("".equals(txt.getText())) {	//when textfield is null
			final JFrame fr = new JFrame("�T��");		//open another frame
			fr.setLayout(new BorderLayout());	
			ImageIcon icon= new ImageIcon("src/ce1002/a10/s102502026/a.png");	//add picture
			JButton bt = new JButton("�T�w");	  //add button
			JPanel pn = new JPanel();			//add panel
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();			//add panel2
			pn1.setSize(100, 50);
			JLabel label = new JLabel();		//add label
			fr.setSize(200, 100);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("File name can't be empty!");		//label output
			//add to panel
			pn.add(new JLabel(icon));		
			pn.add(label);
			pn1.add(bt);
			fr.add(pn,BorderLayout.CENTER);
			fr.add(pn1,BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();		//close frame when press button
				}
			});
		} else {
			try {
				File file = new File(txt.getText() + ".txt");	//create txt file
				BufferedWriter output = new BufferedWriter(new FileWriter(file));	//for writing
				output.write(txtarea.getText());	//save textarea output
				output.close();						
			} catch (IOException e) {
				e.printStackTrace();			//write 
			}
			final JFrame fr = new JFrame("�T��");		//add frame
			  fr.setLayout(new BorderLayout());
			  ImageIcon icon= new ImageIcon("src/ce1002/a10/s102502026/a.png");	//add picture
			JButton bt = new JButton("�T�w");		//add button
			JPanel pn = new JPanel();		//add panel
			pn.setSize(100, 50);
			JPanel pn1 = new JPanel();		//add panel
			pn1.setSize(100, 50);
			JLabel label = new JLabel();	//add label
			fr.setSize(200, 100);
			fr.setLocationRelativeTo(null);
			fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fr.setVisible(true);
			label.setText("File "+txt.getText() + ".txt is saved!");	//label output
			//add to panel
			pn.add(new JLabel(icon));
			pn.add(label);
			pn1.add(bt);
			fr.add(pn,BorderLayout.CENTER);
			fr.add(pn1,BorderLayout.SOUTH);
			bt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					fr.dispose();	//close frame when press button
				}
			});
		}
	}
}
