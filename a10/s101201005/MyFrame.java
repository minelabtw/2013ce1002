package ce1002.a10.s101201005;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {

	JTextField text = new JTextField();
	JTextArea textArea = new JTextArea();
	JButton button = new JButton("Save");

	public MyFrame() {
		setLayout(null);
		setBounds(0, 0, 550, 300);
		setTitle("A10-101201005");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		button.setBounds(0, 0, 100, 30);
		text.setBounds(100, 0, 450, 30);
		textArea.setBounds(0, 30, 550, 270);
		add(button);
		button.addActionListener(this); //按下按鈕後可儲存檔案
		add(text);
		textArea.setLineWrap(true);//可自動換行
		textArea.setWrapStyleWord(true);
		add(textArea);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String str1 = text.getText();
		String str2 = textArea.getText();
		if (str1.isEmpty()) //如果沒有輸入檔名
		{
			JOptionPane.showMessageDialog(this, "File name can't be empty!");
		}
		else
		{
			if (!str1.endsWith(".txt")) //如果檔名不是以txt結尾
				str1 = str1 + ".txt";
			try
			{
				BufferedWriter bw = new BufferedWriter(new PrintWriter(str1));
				bw.write(str2);
				bw.close();
				JOptionPane.showMessageDialog(this, "File" + str1 + " saved!");
			}
			catch (Exception e)
			{
				JOptionPane.showMessageDialog(this, e.getMessage(), "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
