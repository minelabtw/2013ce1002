package ce1002.a10.s102502516;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JTextField textField = new JTextField();
	private JButton saveButton = new JButton("Save");
	private JTextArea textArea = new JTextArea();

	MyFrame() {
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(450, 400, 500, 300);
		setVisible(true);
		setTitle("A10-102502516"); // 視窗設定

		textField.setBounds(100, 10, 375, 30);
		add(textField); // 加入檔名欄位

		saveButton.setBounds(10, 10, 80, 30);
		saveButton.addActionListener(new SaveButtonListener());
		add(saveButton); // 加入按鈕

		textArea.setBounds(10, 50, 460, 195);
		textArea.setLineWrap(true);
		add(textArea); // 加入內文欄位
	}

	class SaveButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			if (textField.getText().isEmpty()) {
				JOptionPane
						.showMessageDialog(null, "File name can't be empty!");
				// 未輸入檔名時
			} else {
				try {
					FileWriter fileSaved = new FileWriter(textField.getText()
							+ ".txt");
					fileSaved.write(textArea.getText());
					fileSaved.close();
					JOptionPane.showMessageDialog(null,
							"File " + textField.getText() + ".txt" + " saved!");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} // 存檔
			}
		}
	}
}
