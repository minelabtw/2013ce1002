

package ce1002.a10.s102502550;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame
{
	private JPanel contentPane;                            
    private JSplitPane splitPane;
    private JTextField fileNameArea;
    private JButton btnSave;
    private JTextArea mainTextArea;

    public MyFrame()
    {
        setTitle("A10-s102502550");                              //設定GUI介面
        setDefaultCloseOperation(3);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        mainTextArea = new JTextArea();
        mainTextArea.setLineWrap(true);
        contentPane.add(mainTextArea, "Center");
        splitPane = new JSplitPane();
        contentPane.add(splitPane, "North");
        fileNameArea = new JTextField();
        splitPane.setRightComponent(fileNameArea);
        fileNameArea.setColumns(10);
        btnSave = new JButton("Save");
        btnSave.addActionListener(new SaveButtonActionListener());
        splitPane.setLeftComponent(btnSave);
    }

    
    class SaveButtonActionListener implements ActionListener              //間聽事件
    {

        public void actionPerformed(ActionEvent e)
        {
            if(fileNameArea.getText().isEmpty())
            {
                showEmptyFileNameMsg();
            } else
            {
                String mainText = mainTextArea.getText();
                String fileName = (new StringBuilder(String.valueOf(fileNameArea.getText()))).append(".txt").toString();
                try
                {
                    PrintWriter out = new PrintWriter(fileName);   //存檔
                    out.println(mainText);
                    out.close();
                    showFileSavedMsg(fileName);
                }
                catch(FileNotFoundException e1)
                {
                    e1.printStackTrace();
                }
            }
        }

        private void showEmptyFileNameMsg()                 //定義顯示示窗內容
        {
            JOptionPane.showMessageDialog(null, "File name can't be empty!");
        }

        private void showFileSavedMsg(String name)
        {
            JOptionPane.showMessageDialog(null, (new StringBuilder("File ")).append(name).append(" saved!").toString());
        }

        
    }


   
    

}
