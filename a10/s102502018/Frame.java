package ce1002.a10.s102502018;

import java.awt.Font;
import java.awt.event.*;
import java.io.*;
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Frame extends JFrame{
	String txtname;	
	public Frame()
	{
		setLayout(null);
		setBounds(30,30,500,500);
		setLocationRelativeTo(null);			//將視窗置中
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("A10-102502018");				//視窗名稱
		JButton bot = new JButton("Save");		
		bot.setBounds(10, 10, 80, 30);
		final JTextField tf = new JTextField();
		tf.setBounds(100, 10, 380, 30);
		final JTextArea ta = new JTextArea();
		ta.setLineWrap(true);					//當字串長度超過TextArea時，自動換行
		ta.setBounds(10, 50, 463, 400);
		add(bot);
		add(tf);
		add(ta);
		
		bot.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e)
			{
				File f = new File(tf.getText()+".txt");
				if(f.getName().equals(".txt"))		//檔名不能為空
				{
					Panel panel = new Panel();				
				}
				else
				{					
					try {					
						f.createNewFile();			//建立檔案
						txtname = tf.getText();
						Panel panel = new Panel(txtname);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						FileWriter write = new FileWriter(tf.getText()+".txt");
						write.write(ta.getText());			//將TextArea內容寫入檔案中
						write.close();
						
						
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}		
				}
			}
		});	    
		}
	
	
}
