package ce1002.a10.s101201521;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.*;
public class A10 extends JFrame{
	//button for save
	private JButton saveBt = new JButton("Save");
	//text field for file name
	private JTextField fileTf = new JTextField();
	//text area for file content
	private JTextArea contentTa = new JTextArea(14, 38);
	//splitPane for saveBt and fileTf
	private JSplitPane p1= new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,saveBt, fileTf);
	//panel for contentTa
	private JPanel p2 = new JPanel();
	//constructor with title
	public A10(String title){
		super(title);
		//layout setting
		this.add(p1, BorderLayout.NORTH);
		saveBt.addActionListener(new btListener());
		
		this.add(p2, BorderLayout.CENTER);
		p2.add(contentTa);
		contentTa.setLineWrap(true);
		
		this.setSize(450,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	public static void main(String[] args){
		A10 frame = new A10("A10-101201521");
	}
	class btListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			String filename = fileTf.getText();
			String content = contentTa.getText();
			//check whether there exist a file name
			if(filename.length() == 0){
				JOptionPane.showMessageDialog(null, "File name can't be empty!");
			}
			else{
				//save or create a txt file with filename and content
				File file = new File(filename + ".txt");
				PrintWriter output;
				try {
					output = new PrintWriter(file);
					output.print(content);
					output.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "File " + filename + ".txt saved");
			}
		}
		
	}
}
