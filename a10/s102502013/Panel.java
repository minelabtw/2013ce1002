package ce1002.a10.s102502013;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JLabel;

import java.awt.Dimension;
import java.io.IOException;

import javax.swing.JFrame;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class Panel extends JPanel{
	protected JButton save;
	public JTextField filename;
	public JTextArea filecontent;
	Panel(){
		setBounds(5, 5, 495, 295);
		setLayout(new FlowLayout(FlowLayout.LEFT,  5, 5));
		
		save = new JButton("Save");
		save.setBounds(0, 0, 25, 15);
		save.setVisible(true);
		save.addActionListener(new SAVEListenerClass());
		add(save);
		
		filename = new JTextField(36);
		add(filename);
		
		filecontent = new JTextArea(13,42);
		filecontent.setBackground(Color.white);
		filecontent.setLineWrap(true);
		add(filecontent);
	}
	class SAVEListenerClass implements ActionListener{
		
	@Override
	public void actionPerformed(ActionEvent arg0) {
		try{
		  if(filename.getText().length() != 0 ){
				writeFile();
				//訊息視窗
				JPanel panel = new JPanel();
				JLabel message = new JLabel("File " + filename.getText() + ".txt Saved!");
				panel.add(message);
			    panel.setMinimumSize(new Dimension(200,200));
			    JFrame frame = new JFrame("訊息");
			    JOptionPane.showMessageDialog(frame, panel);
			}
		  else {
			    JPanel panel1 = new JPanel();
				JLabel message = new JLabel("File name can’t be empty!");
				panel1.add(message);
			    panel1.setMinimumSize(new Dimension(200,200));
			    JFrame frame1 = new JFrame("訊息");
			    JOptionPane.showMessageDialog(frame1, panel1);
		  }
		}
		catch(Exception e){
			
		}
	}
	//output txt file
	public void writeFile()throws IOException{
		java.io.File file = new java.io.File(filename.getText() + ".txt");
		if(file.exists()){
			System.out.println("File already exists");
			System.exit(1);
		}
		java.io.PrintWriter output = new java.io.PrintWriter(file);
		output.print(filecontent.getText());
		output.close();
		}
	
	}
}
