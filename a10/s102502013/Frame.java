package ce1002.a10.s102502013;
import javax.swing.JFrame;
import javax.swing.JPanel;
public class Frame extends JFrame{
	Frame(){
		Panel panel = new Panel();
		setTitle("A10-102502013");
		setLayout(null);
		setSize(500, 300);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(panel);
	}
}
