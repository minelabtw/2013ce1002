package ce1002.a10.s102502016;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {
	JButton jb = new JButton("Save");
	JTextField jtf = new JTextField();
	JTextArea jta = new JTextArea();

	public MyFrame() {
		jb.addActionListener(this);//加入動作監控
		jb.setSize(100, 30);
		jb.setLocation(10, 5);
		jtf.setSize(380, 30);
		jtf.setLocation(130, 5);
		jta.setSize(500, 460);
		jta.setLocation(10, 50);
		jta.setLineWrap(true);
		add(jb);
		add(jtf);
		add(jta);
		this.setTitle("A10-102502016");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null); // 非預設排版
		setSize(600, 600); // 設置大小
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {//執行的動作
		save();
	}

	public void save() {
		if ("".equals(jtf.getText())) {//標題為空的話
			JFrame b = new JFrame();
			JLabel a = new JLabel("File name can't be empty!");
			JOptionPane.showMessageDialog(b, a);
		} else {
			JFrame b = new JFrame();
			JLabel a = new JLabel("File " + jtf.getText() + ".txt saved!");
			JOptionPane.showMessageDialog(b, a);
			FileWriter out;
			try {
				out = new FileWriter(jtf.getText() + ".txt", true);//可能產生的例外
				out.write(jta.getText());
				out.close();
			} catch (IOException e) {//例外發生時產生的動作
				e.printStackTrace();
			}
		}
	}
}