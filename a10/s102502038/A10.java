package ce1002.a10.s102502038;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class A10 {
	public static void main(String[] args){
		GUI gui = new GUI();//build up GUI
	}
}
class GUI{
	JFrame frame;
	JPanel northPanel;
	JTextField smallText;
	JTextArea largeText;
	JButton button;
	GUI(){//GUI up pack
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("A10-102502038");
		frame.setVisible(true);
		northPanel = new JPanel();
		northPanel.setLayout(new BoxLayout(northPanel,0));
		smallText = new JTextField();
		largeText = new JTextArea();
		largeText.setLineWrap(true);
		button = new JButton("Save it");
		button.addActionListener(new Listener(smallText,largeText));//add action listener
		northPanel.add(button);
		northPanel.add(smallText);
		frame.getContentPane().add(northPanel,BorderLayout.NORTH);
		frame.getContentPane().add(largeText,BorderLayout.CENTER);
		frame.setSize(300, 200);
	}
}
class Listener implements ActionListener{//listener
	private JTextField name;
	private JTextArea text;
	private JOptionPane alert;
	Listener(JTextField name,JTextArea text){
		this.name = name;
		this.text = text;
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		alert = new JOptionPane();
		if(!name.getText().isEmpty()){//check if file name empty
			try {
				FileWriter file = new FileWriter(new File(name.getText() + ".txt"));
				file.write(text.getText().replace("\n", "\r\n"));
				file.close();
				alert.showMessageDialog(null, "File " + name.getText() + ".txt saved!",null,0);
			} catch (IOException e) {
				alert.showMessageDialog(null, e.getMessage());
			}
		}else{
			alert.showMessageDialog(null, "File name can't be empty!",null,0);//alert when file name empty
		}
	}
	
}