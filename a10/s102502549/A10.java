package ce1002.a10.s102502549;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class A10 extends JFrame {

	private JSplitPane p = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,true);//一個jsplitpanel
	private JButton savebutton = new JButton("Save");//save按鈕
	static private JTextField textField = new JTextField(10);//textfield
	private JTextArea textArea = new JTextArea();//textarea

	public A10() {
		savebutton.addActionListener(new SaveListener());//將按鈕加個監聽器
		//把按鈕和textfield加入jsplitpane，再放入frame中
		p.add(savebutton);
		p.add(textField);
		add(p, BorderLayout.NORTH);

		//設定textarea的一些屬性並加入這個frame
		textArea.setBorder(new LineBorder(Color.BLACK, 2));
		textArea.setPreferredSize(new Dimension(300, 300));
		textArea.setLineWrap(true);
		textArea.setFont(new Font("微軟正黑體", Font.PLAIN, 20));
		add(textArea);

		//設定這個frame的一些屬性
		setTitle("A10-102502549");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}

	public static void main(String args[]) {
		A10 frame = new A10();
	}

	class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//檢查textfield是否有輸入
			if (textField.getText().trim().length()==0) {
				JOptionPane
						.showMessageDialog(null, "File name can’t be empty!");
			} else {
				
				//建立file
				File file = new File(textField.getText().trim() + ".txt");
				
				//寫入資料
				try {
					PrintWriter print = new PrintWriter(file);
					print.print(textArea.getText());
					print.close();
					JOptionPane.showMessageDialog(null, "File "+textField.getText().trim()+".txt saved!");
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}