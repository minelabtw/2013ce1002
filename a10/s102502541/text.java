package ce1002.a10.s102502541;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
public class text extends JFrame{
	JTextField jt = new JTextField();
	JButton jb = new JButton("Save");
	JTextArea jta = new JTextArea();
	public text()
	{
		setVisible(true);
		setSize(500,500);
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("A10-102502541");
		jta.setLineWrap(true);
		jb.setBounds(0, 0,100,50);
		jt.setBounds(100 ,0 , getWidth()-100, 50);
		jta.setBounds(0 ,50 , getWidth(), getHeight()-50);
		SaveListener save = new SaveListener();
		jb.addActionListener(save);//把動作加到jbutton上
		add(jb);
		add(jt);
		add(jta);
	}
	class SaveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent a)
		{
			JLabel l = new JLabel();
			JFrame f = new JFrame();
			if(jt.getText().length()==0)//判斷沒有輸入檔名
				l.setText("File name can’t be empty!");
			else
			{
				try
				{
					DataOutputStream print= new DataOutputStream(new FileOutputStream(jt.getText()+".txt"));//new一個檔案
					print.writeUTF(jt.getText());//寫入檔案
					print.close();//輸出檔案
					l.setText("File "+jt.getText()+ ".txt saved!");
				}
				catch(IOException e)
				{
				}
			}
			JOptionPane.showMessageDialog(f, l);		
		}
	}
}
