package ce1002.a10.s102502521;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.*;

public class MyPanel extends JPanel implements ActionListener {
	protected JButton button = new JButton("save");
	protected JTextField textField = new JTextField();
	protected JTextArea textArea = new JTextArea();
	private String text;
	private String name;

	MyPanel() {
		setLayout(null);
		button.setBounds(5, 5, 100, 30);
		textField.setBounds(150, 5, 400, 30);
		textArea.setBounds(5, 45, 545, 300);
		textArea.setLineWrap(true);

		//替按鈕加入action listener
		button.addActionListener(this);

		add(button);
		add(textField);
		add(textArea);
	}

	@Override
	public void actionPerformed(ActionEvent a) {
		// TODO Auto-generated method stub
		name = textField.getText();
		text = textArea.getText();

		try {
			if (!name.isEmpty()) {
				//新增檔案
				File txtFile = new File(name + ".txt");
				txtFile.createNewFile();
				//寫入
				FileWriter writer = new FileWriter(name + ".txt");
				writer.write(text);
				writer.close();
				//創建檔案成功
				JOptionPane.showMessageDialog(null, "File " + name
						+ ".txt saved!");
			} else {
				//沒有檔名
				JOptionPane
						.showMessageDialog(null, "File name can't be empty!");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
