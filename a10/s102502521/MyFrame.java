package ce1002.a10.s102502521;

import javax.swing.*;

public class MyFrame extends JFrame {

	protected MyPanel p1 = new MyPanel();

	MyFrame() {
		setTitle("A10-102502521");
		setBounds(300, 200, 575, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		p1.setBounds(10, 10, 550, 30);
		add(p1);
	}
}
