package ce1002.a10.s102502562;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.omg.CORBA.PUBLIC_MEMBER;

public class MyFrame extends JFrame{
	JButton savebutton=new JButton("Save");//宣告Button、TextField和TextArea
	JTextField textname=new JTextField();
	JTextArea message=new JTextArea();
	MyFrame()
	{
		setTitle("A10-102502562");//設定標題,大小,位置
		setSize(420,400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		savebutton.setSize(100,25);//設定物件大小
		savebutton.setLocation(0,0);
		textname.setSize(320,25);
		textname.setLocation(100,0);
		message.setSize(420,350);
		message.setLocation(0,25);
		message.setLineWrap(true);//設定自動換行
		add(savebutton);
		add(textname);
		add(message);
		savebutton.addActionListener(new SaveText());//設定處理按按鈕的事件
		setVisible(true);
	}
	boolean newFile()
	{
		try
		{
			if(textname.getText().isEmpty())//檢查檔名是否為空值
			{
				JOptionPane.showMessageDialog(null,"File name can't be empty!","訊息",JOptionPane.ERROR_MESSAGE);
				return false;
			}
			java.io.File file=new java.io.File(textname.getText()+".txt");
			java.io.PrintWriter output=new java.io.PrintWriter(file);
			output.print(message.getText());//輸出內容到txt檔
			output.close();//關閉
		}
		catch (Exception e)
		{
			return false;
		}
		return true;
	}
	class SaveText implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(newFile())
			{
				JOptionPane.showMessageDialog(null,"File " + textname.getText() + ".txt" + " saved!","訊息",JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
}
