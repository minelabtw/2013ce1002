	package ce1002.a10.s102502506;

	import java.awt.event.*;
	import java.io.*;
	import javax.swing.*;

	public class MyFrame extends JFrame implements ActionListener {
	 JButton D = new JButton("Done");  //創造一個按鈕D
	 JTextField Filename = new JTextField();  //創造一個JTextField區域Filename
	 JTextArea A = new JTextArea();  

	 public MyFrame() {
	setTitle("A10-102502506");
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setLayout(null);
	setSize(500, 500); // 設置大小
	setVisible(true);	 
	 D.addActionListener(this);    //D上裝動作監聽器implements宣告在MyFrame上
	 D.setSize(100, 30);
	 D.setLocation(0, 0);
	 Filename.setSize(400, 30);
	 Filename.setLocation(100, 0);
	 A.setSize(500, 460);
	 A.setLocation(0, 35);
	 A.setLineWrap(true);
	 add(D);
	 add(Filename);
	 add(A);
	 }

	 public void actionPerformed(ActionEvent e) {
	 save();
	 }

	 public void save() {
	 if ("".equals(Filename.getText().trim())) {  //如果空字串=Filename.getText().trim() 執行下列動作
	 JFrame a = new JFrame();
	 JLabel b = new JLabel("File name can't be empty!");
	 JOptionPane.showMessageDialog(a, b);  //跳出新的Frame a 裡面裝載b的訊息
	 } else {
	 JFrame a = new JFrame();
	 JLabel b = new JLabel("File " + Filename.getText() + ".txt saved!");
	 JOptionPane.showMessageDialog(a, b);
	 FileWriter F;
	 try {  //寫字到新的txt還有設定檔名
	 F = new FileWriter(Filename.getText() + ".txt", true);
	 F.write(A.getText());
	 F.close();
	 } catch (IOException E) {
	 E.printStackTrace();
	 }
	 }
	 }
	}
