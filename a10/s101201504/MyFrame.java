package ce1002.a10.s101201504;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JButton save =new JButton("Save");
	private JTextField text =new JTextField();
	private JTextArea area=new JTextArea();
	
	MyFrame()
	{
		setTitle("A10-101201504");
		setSize(935,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		area.setLineWrap(true);  
		save.setBounds(0,0,100,30);      //set save bound
		text.setBounds(100,0,935,30);     //set text bound
		area.setBounds(0,30,935,450);       //set content bound
		add(save);
		add(text);
		add(area);
		Savelistener saveListener =new Savelistener();        
		save.addActionListener(saveListener);
		
	}
	public class Savelistener implements ActionListener{
		public void actionPerformed(ActionEvent a)
		{
			JFrame window =new JFrame();
			String line =new String();
			try
			{
				if (text.getText()==null)
				{
					throw new Exception();
				}
				else 
				{
					line="File "+text.getText()+".txt saved!";
					File file =new File(text.getText());
					PrintWriter output =new PrintWriter(file);
					output.print(area.getText());
					output.close();
				}
			}
			catch (Exception e)
			{
				line="File name can't be empty!";
			}
			JOptionPane.showMessageDialog(window,line);
			
			
		}
	}
}


