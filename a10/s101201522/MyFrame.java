package ce1002.a10.s101201522;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.*;

public class MyFrame extends JFrame {
	private MyPanel panel = new MyPanel();
	
	MyFrame () {//initial
		setSize(450, 300);
		add(panel);
	}
	
	public class MyPanel extends JPanel {//panel inner class
		private JButton save = new JButton("Save");
		private JTextField name = new JTextField();
		private JTextArea content = new JTextArea();
		private JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, save, name);
		
		MyPanel () {//initial
			setLayout(null);
			save.addActionListener(new ActionListener () {//button listener

				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (name.getText().equals(""))//empty name
						JOptionPane.showMessageDialog(null, "File name can��t be empty!");
					else {
						JOptionPane.showMessageDialog(null, "File " + name.getText() + ".txt saved!");
						WriteFile();
					}
				}
				
			});
			
			/*set component position and size*/
			split.setDividerLocation(65);
			split.setBounds(5, 5, 425, 30);
			content.setBounds(5, 35, 425, 250);
			content.setOpaque(false);
			content.setLineWrap(true);
			
			
			/*add component*/
			add(split);
			add(content);
		}
		
		public void WriteFile () {//write content into file
			try {
				FileWriter out = new FileWriter(name.getText()+".txt");
				out.write(content.getText());
				out.close();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
