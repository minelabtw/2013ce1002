package ce1002.a10.s102502010;

import javax.swing.JFrame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;   
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JOptionPane;

public class MyFrame  extends JFrame implements ActionListener {
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	private JButton button = new JButton("Save");
	private JTextField textfield = new JTextField(15);
	private JTextArea textarea = new JTextArea(20,20);
	
	
	MyFrame()
	{
		setTitle("A10-102502010");
		setLayout(null);//浮動預設
		setBounds(0,0,300,300);//視窗大小
		setVisible(true);//顯示
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		paint1(p1,0,0);
		paint2(p2,70,0);
		paint3(p3,10,40);
	}
	
	public void paint1(JPanel panel, int x, int y)   //button
	{
		panel.setBounds(x, y, 70 , 40);
		panel.add(button);
		button.addActionListener((ActionListener) this);
		add(panel);
	}
	
	public void paint2(JPanel panel, int x, int y)   //textfield
	{
		panel.setBounds(x, y, 200, 40);
		panel.add(textfield);
		add(panel);
	}
	
	public void paint3(JPanel panel, int x, int y)   //textarea
	{
		panel.setBounds(x, y, 265, 200);
		textarea.setLineWrap(true);
		panel.add(textarea);
		add(panel);
	}
	
	
	 public void actionPerformed(ActionEvent a) {
		 
		 if(textfield.getText().length() == 0){  //File name can’t be empty!
		    JPanel panel = new JPanel();
		    panel.setBackground(Color.BLUE);
		    panel.setMinimumSize(new Dimension(200,200));		 
		    JFrame frame = new JFrame("訊息");
		    JOptionPane.showMessageDialog(frame, "File name can’t be empty!");
		 }
		 else {
		    try {
			   File file = new File(textfield.getText() + ".txt");  //write in .txt
			   PrintWriter output = new PrintWriter(file);
			   output.print(textarea.getText());
			   output.close();
		    } catch (FileNotFoundException e) {
			   e.printStackTrace();
		    }
		    JPanel panel = new JPanel();  //.txt saved!
		    panel.setBackground(Color.BLUE);
		    panel.setMinimumSize(new Dimension(200,200));		 
		    JFrame frame = new JFrame("訊息");
		    JOptionPane.showMessageDialog(frame, textfield.getText() + ".txt saved!");
		 }
		
	}

}
