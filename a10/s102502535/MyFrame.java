package ce1002.a10.s102502535;

//import everything we need
import java.io.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {

	// create frame, button, text box and text area
	JFrame frame = new JFrame("A10-102502535");
	JButton savebt = new JButton("Save");
	JTextField name = new JTextField(null);
	JTextArea text = new JTextArea();

	MyFrame() {
		// set frame's data
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLayout(null);
		// set others' data
		savebt.setBounds(5, 5, 70, 20);
		name.setBounds(80, 5, 295, 20);
		text.setBounds(10, 35, 365, 315);
		text.setLineWrap(true);
		// add everything into frame
		frame.add(savebt);
		frame.add(name);
		frame.add(text);
		// if the button is clicked, the following things will be done
		savebt.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (name.getText().trim().equals("")) {
			JOptionPane.showMessageDialog(null, "File name can��t be empty!",
					"�T��", JOptionPane.INFORMATION_MESSAGE);//if file's name is empty, tell the user by popping message
		} else if (e.getSource() == savebt || e.getSource() == name) {
			String FName = name.getText();
			String setFName = String.valueOf(FName);
			File saveFile = new File(setFName + ".txt"); 
			try {
				FileWriter fwriter = new FileWriter(saveFile);
				String FText = text.getText();
				String setFText = String.valueOf(FText);
				fwriter.write(setFText);
				fwriter.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			JOptionPane.showMessageDialog(null, "File " + setFName
					+ ".txt saved!", "�T��", JOptionPane.INFORMATION_MESSAGE);  //save what user wrote in a txt file
		}
	}
	
}
