package ce1002.a10.s102502543;

import javax.swing.*;
import java.awt.event.*;

public class MyFrame extends JFrame {
	JButton save = new JButton("Save");
	JTextField name = new JTextField();
	JTextArea area = new JTextArea();

	MyFrame() {
		setTitle("A10-102502543");
		setSize(470, 420);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		save.setBounds(0, 0, 70, 20);
		name.setBounds(70, 0, 385, 20);
		area.setBounds(0, 20, 455, 360);
		area.setLineWrap(true);
		add(save);
		add(name);
		add(area);
		save.addActionListener(new SaveListener());
		setVisible(true);
	}

	boolean fileOutput() {
		try {
			if (name.getText().isEmpty()) {
				new MessageBox("File name can't be empty!");
				return false;
			}
			java.io.File file = new java.io.File(name.getText() + ".txt");
			java.io.PrintWriter output = new java.io.PrintWriter(file);
			output.print(area.getText());
			output.close();
		} catch (Exception ex) {
		}
		return true;
	}

	class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent a) {
			if (fileOutput()) {
				new MessageBox("File " + name.getText() + ".txt saved!");
			}
		}
	}
}
