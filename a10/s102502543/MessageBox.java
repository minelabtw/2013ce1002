package ce1002.a10.s102502543;

import java.awt.*;
import javax.swing.*;

public class MessageBox {
	MessageBox(String message) {
		JPanel panel = new JPanel();
		panel.setMinimumSize(new Dimension(200, 200));
		JFrame frame = new JFrame("Message");
		JLabel label = new JLabel(message);
		panel.add(label);
		JOptionPane.showMessageDialog(frame, panel);
	}
}