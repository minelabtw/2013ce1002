package ce1002.a10.s102502012;

import javax.swing.JFrame;

public class A10 {

	public static void main(String[] args) {
		MyFrame window = new MyFrame();
		window.setTitle("A10-102502012");
		window.setSize(400, 300);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocationRelativeTo(null);
		window.setResizable(false);
		window.setVisible(true);
	}

}
