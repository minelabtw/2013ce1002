package ce1002.a10.s102502012;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JButton btnSave;
	private JTextField txtfTitle;
	private JTextArea txtaContent;
	
	public MyFrame(){
		super();
		
		btnSave = new JButton("Save");
		btnSave.setBounds(5, 5, 100, 30);
		
		btnSave.addActionListener(new btnSaveClick());
		
		txtfTitle = new JTextField(1);
		txtfTitle.setBounds(110, 5, 280, 30);
		
		txtaContent = new JTextArea();
		txtaContent.setBounds(5, 40, 385, 215);
		txtaContent.setLineWrap(true);
		
		JPanel container = new JPanel();
		container.setLayout(null);
		
		container.add(btnSave);
		container.add(txtfTitle);
		container.add(txtaContent);
		setContentPane(container);
	}
	
	class btnSaveClick implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e){
			// check title length is not empty
			if(txtfTitle.getText().length() > 0){
				File file = new File(txtfTitle.getText() + ".txt");
				
				try{
					PrintWriter output = new PrintWriter(file);
					output.print(txtaContent.getText());
					output.close();
				}
				catch(Exception ep){
					System.out.print(ep);
				}
				JOptionPane.showMessageDialog(new JFrame("�T��"), "File " + txtfTitle.getText() + ".txt saved!");
				
			}
			else{
				JOptionPane.showMessageDialog(new JFrame("�T��"), "File name can��t be empty!");
			}
			
		}
		
	}
	
}
