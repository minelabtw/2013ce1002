package ce1002.a10.s102502009;

import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {
	JButton Save = new JButton("Done");
	JTextField Title = new JTextField();
	JTextArea Area = new JTextArea();

	public MyFrame() { //frame
		Save.addActionListener(this);
		Save.setSize(100, 30);
		Save.setLocation(0, 0);
		Title.setSize(400, 30);
		Title.setLocation(105, 0);
		Area.setSize(500, 460);
		Area.setLocation(0, 35);
		Area.setLineWrap(true);
		add(Save);
		add(Title);
		add(Area);
		this.setTitle("A10-102502009");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null); 
		setSize(500, 500); 
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		save();
	}

	public void save() {
		if ("".equals(Title.getText().trim())) { // empty
			JFrame x = new JFrame();
			JLabel y = new JLabel("File name can't be empty!");
			JOptionPane.showMessageDialog(x, y);
		} else {
			JFrame x = new JFrame();
			JLabel y = new JLabel("File " + Title.getText() + ".txt saved!");
			JOptionPane.showMessageDialog(x, y);
			FileWriter out;
			try {
				out = new FileWriter(Title.getText() + ".txt", true);
				out.write(Area.getText());
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}