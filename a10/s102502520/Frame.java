package ce1002.a10.s102502520;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class Frame extends JFrame{
	JButton button = new JButton("Save");
	JTextField textfield = new JTextField();
	JTextArea area = new JTextArea();
	String name = null;
	String message;
	
	public Frame(){
		setLayout(null);
		setBounds(50,50,480,300);
		setTitle("A10-102502520");
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		button.setBounds(5, 0, 80, 30);
		ActionListener savefile = new savefile();
		button.addActionListener(savefile);
		textfield.setBounds(90,0,350,30);
		area.setBounds(5, 40, 450, 200);
		area.setLineWrap(true);
	
		add(button);
		add(textfield);
		add(area);
	}
	class savefile implements ActionListener{

		JFrame frame =new JFrame();
		void savefile(){
			this.frame = frame;
		}
		
		
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			name = textfield.getText();
			message = area.getText();
			if (name.isEmpty()|| name == null){
				showerror();
			}
			else {
				save();
				savesuccess();
			}
		}
		void save(){
			try {
				FileWriter	wr = new FileWriter(name + ".txt");
				wr.write(message);
				wr.flush();
			} catch (IOException e){
				e.printStackTrace();
			}
		}
		
		void showerror(){
			JOptionPane.showMessageDialog(frame, "File's name can not be empty!! ");
		}
		void savesuccess(){
			JOptionPane.showMessageDialog(frame, "File " + name + ".txt saved!");
		}
	}
	
}
