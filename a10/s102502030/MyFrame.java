package ce1002.a10.s102502030;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener {

	JTextField name = new JTextField("");
	JButton save = new JButton( "save" );
	JTextArea data = new JTextArea();
	
	public MyFrame() {
		//設定frame
		setTitle( "A10-102502030" );
		setVisible( true );
		setSize( 500, 500 );
		setLocation( 300, 300 );
		setLayout( null );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		
		//設定文字框和按鈕
		save.setBounds( 0, 0, 100, 40 );
		name.setBounds( 100, 0, 400, 40 );
		data.setBounds( 0, 40, 500, 400 );
		save.addActionListener(this);
		
		add(save);
		add(name);
		add(data);
	}
	
	public void actionPerformed( ActionEvent e ) {	
		
		//判斷檔名是否空白
		if( name.getText().isEmpty() ) {
			//產生錯誤訊息視窗
			JFrame frame = new JFrame();
			JLabel wrong = new JLabel( "File name can’t be empty!" );
			JOptionPane.showMessageDialog(frame, wrong);
		}
		else {
			//產生存檔成功視窗
			String input = name.getText() + ".txt";
			JFrame frame = new JFrame();
			JLabel success = new JLabel( "File " +  input + " saved!" );
			JOptionPane.showMessageDialog(frame, success);
			
			//開檔案
			OutputStream outputStream = null;		
			try {
			outputStream = new FileOutputStream( input );
			} catch ( Exception ec ) {
			System.out.println( ec );
			}
			
			//存進輸入資料至檔案
			PrintWriter outputStream1 = new PrintWriter(outputStream);
			outputStream1.println(data.getText());
			outputStream1.close();
			try {
			outputStream.close();
			} catch ( Exception ec ) {
			System.out.println( ec );
			}
		}
		
    }

	
}
