package ce1002.a10.s102502034;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

public class save implements ActionListener {
	private JTextField name;
	private JTextArea contents;

	save(JTextField tf,JTextArea ta ) {
		name = tf;
		 contents = ta;
		 // set name and contents to tf and ta
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try {

			String n=name.getText();
			if (n.trim().equals(""))
			{
				JOptionPane.showMessageDialog(null, "File name can’t be empty", "Info ", JOptionPane.INFORMATION_MESSAGE);
				System.exit(1);
			}
			String c=contents.getText();
			File file = new File(n+".txt");
			PrintWriter pen = new PrintWriter(file);
			pen.print(c);
			pen.close();

			JOptionPane.showMessageDialog(null, "File "+n+".txt saved", "Info ", JOptionPane.INFORMATION_MESSAGE);
			// write cotent to .txt and show the message dialog
		} catch (Exception e1) {
		//	JFrame framefail = new JFrame("File name can’t be empty");
		//	JPanel panel = new JPanel();

			JOptionPane.showMessageDialog(null, "Invalid file name", "Info ", JOptionPane.ERROR_MESSAGE);
			//error message
		}

	}
}
