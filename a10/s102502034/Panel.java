package ce1002.a10.s102502034;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Panel extends JPanel {
	Panel(){
		JTextField FileName = new JTextField(27);
		JTextArea contents = new JTextArea(18,34);
		JButton SaveBtn = new JButton("Save");
		contents.setLineWrap(true);
		
		//create TextField , TextArea , Button
		add(SaveBtn,BorderLayout.WEST);
		add(FileName,BorderLayout.CENTER);
		add(contents,BorderLayout.SOUTH);
		// set layout

		SaveBtn.addActionListener(new save(FileName,contents));    
		// set action of clicking button
		
		
		
	}

}