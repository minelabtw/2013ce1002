package ce1002.a10.s102502537;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class MyFrame extends JFrame{
	private JButton button = new JButton("Save");
	private JTextArea textA = new JTextArea();
	private JTextField textF = new JTextField();
	private JLabel lab = new JLabel();
	
	MyFrame()
	{	
		super("A10-102502537"); // name the frame
		setLayout(null); 
		setBounds(160,130,450,400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		button.setBounds(5, 5, 75, 20);
		textF.setBounds(80, 5, 325, 20);
		textA.setBounds(5, 25, 400, 350);
		textA.setLineWrap(true);
		lab.setBounds(60, 79, 225, 40);
		
		button.addActionListener(new SaveListener());
		add(button);
		add(textA);
		add(textF);
		add(lab);
	}
	
	class SaveListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			DataOutputStream outputStream;
			
			if(textF.getText().isEmpty() == true){
				JOptionPane.showMessageDialog(lab,  "File name can't be empty!", "訊息",
	                      JOptionPane.INFORMATION_MESSAGE);
			}else{
				try{
					JOptionPane.showMessageDialog(lab, ("File " + textF.getText() + ".txt saved!"), "訊息",
		                      JOptionPane.INFORMATION_MESSAGE);
					outputStream = new DataOutputStream(new FileOutputStream(textF.getText() + ".txt"));
					outputStream.writeBytes(textA.getText());
				}catch(Exception e1){
					
				}
		
			}
				
		} 
		
	}  
}
