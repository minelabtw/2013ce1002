package ce1002.a10.s102502552;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.io.FileWriter;

public class A10 {
	private JFrame frame;
	private JTextField txtYouNeedA;//檔案名
	private JTextArea txt;//內容
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A10 window = new A10();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public A10() {
		initialize();
	}

	private void initialize() {//利用windowbuilder以拖拽形式實現排版
		frame = new JFrame("A10-102502552");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton save = new JButton("Save");
		save.setBounds(0, 0, 125, 25);
		save.setFont(new Font("Bookman Old Style", Font.PLAIN, 13));
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {//按鍵觸發事件
				try{
					istitle();//判斷檔案名是否為空
					FileWriter file = new FileWriter("src/ce1002/a10/s102502552/"+txtYouNeedA.getText()+".txt",true);
					file.write(txt.getText() + "");
					file.close();//建立檔案，寫入，存檔到專案所在目錄
					SavedFrame smallframe = new SavedFrame("File " + txtYouNeedA.getText() + ".txt saved");//跳出方框提示已存檔
					smallframe.setVisible(true);
				}catch(Exception ex){
					SavedFrame smallframe = new SavedFrame(ex.getMessage());
					smallframe.setVisible(true);//檔案名為空丟出例外，跳出方框提示
				}
				
			}
		});
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(save);
		
		txtYouNeedA = new JTextField();
		txtYouNeedA.setFont(new Font("Colonna MT", Font.PLAIN, 14));
		txtYouNeedA.setHorizontalAlignment(SwingConstants.CENTER);
		txtYouNeedA.setText(null);
		txtYouNeedA.setBounds(124, 0, 310, 25);
		frame.getContentPane().add(txtYouNeedA);
		txtYouNeedA.setColumns(10);//檔案名的textfeild
		
		txt = new JTextArea();
		txt.setText("up to have your title and here to have your imformation");
		txt.setWrapStyleWord(true);
		txt.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 13));
		txt.setBounds(0, 25, 434, 236);
		txt.setLineWrap(true);
		frame.getContentPane().add(txt);//內容的textarea
		
	}
	
	public void istitle() throws Exception//判斷檔案名的函式
	{
		String title = txtYouNeedA.getText();
		
		if(title.isEmpty() == true)
		{
			throw new Exception("File name can’t be empty!");
		}//檔案名為空則丟出例外
	}
}
