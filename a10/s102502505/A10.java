package ce10002.a10.s102502505;

import java.awt.FlowLayout;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame = new JFrame();// 建立frame
		JPanel panel = new JPanel();// 建立panel

		panel.setLayout(null);
		panel.setLayout(new FlowLayout());// 面板管理員

		JButton button = new JButton("Save");// 新增button
		final JTextField textfield = new JTextField(null, 30);// 新增textfield且長度等於30
		final JTextArea textarea = new JTextArea(null, 13, 37);// 新增textarea請長寬等於13,37
		textarea.setLineWrap(true);// 超出編輯區的文字自動換行

		panel.add(button);// 加入三個物件到panel
		panel.add(textfield);
		panel.add(textarea);

		frame.pack();
		frame.setTitle("A10-102502505");
		frame.setSize(440, 290);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.add(panel);

		button.addActionListener(new ActionListener() {// 設定button的監聽使用者事件，註冊監聽器
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				String title = textfield.getText();// 拿取textfield和textarea的字
				String content = textarea.getText();

				try {// 設立例外來抓取錯誤

					if (title == null) {// 假如title為空字串，丟到例外
						throw new Exception();
					} else {// 其餘則執行下列動作
						java.io.File file = new java.io.File(title + ".txt");// 建立名為title的txt文件
						java.io.PrintWriter output = new java.io.PrintWriter(
								file);
						output.print(content);// 將內容加入文件中
						output.close();

						JDialog dialog = new JDialog(new JFrame(), "訊息");// 新增一個對話框
						dialog.setSize(150, 150);// 設大小
						JLabel savelabel = new JLabel("File " + title
								+ ".txt saved");// 建立label
						JButton savebutton = new JButton("確定");// 建立button

						dialog.setLayout(new FlowLayout());
						dialog.add(savelabel);
						dialog.add(savebutton);
						dialog.setLocationRelativeTo(null);
						dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);// 按離開時會結束對話框
						dialog.setVisible(true);

						savebutton.addActionListener(new ActionListener() {// 按"確定"按鈕時會結束程式
									public void actionPerformed(ActionEvent e) {
										System.exit(1);
									}
								});
					}
				}

				catch (Exception ex) {// 例外時，建立另一個對話框

					JDialog dialog = new JDialog(new JFrame(), "訊息");// 新增一個對話框
					dialog.setSize(150, 150);
					JLabel savelabel = new JLabel("File name can’t be empty!");// 新增label
					JButton savebutton = new JButton("確定");// 新增按鈕

					dialog.setLayout(new FlowLayout());
					dialog.add(savelabel);
					dialog.add(savebutton);
					dialog.setLocationRelativeTo(null);
					dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
					dialog.setVisible(true);

					savebutton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							System.exit(1);
						}
					});
				}
			}
		});

	}
}
