package ce1002.a10.s102502533;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.*;
public class frame extends JFrame {
	public JTextField tf = new JTextField();
	protected JButton bt = new JButton("save");
	JTextArea ta = new JTextArea();
	frame(){
		tf.setBounds(100, 0, 380, 30);
		bt.setBounds(5, 0, 80, 30);
		ta.setBounds(5, 35, 470, 222);
		ta.setLineWrap(true);
		add(bt);
		add(tf);
		add(ta);
		bt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (tf.getText().isEmpty()) // 跑出空檔訊息
					JOptionPane.showMessageDialog(null,
							"File name can’t be empty!", "訊息",
							JOptionPane.WARNING_MESSAGE); // 跳出視窗
				else
					// 跑出存檔訊息
					JOptionPane.showMessageDialog(null, "File " + tf.getText()
							+ ".txt saved!", "訊息",
							JOptionPane.INFORMATION_MESSAGE);
				PrintWriter file;
				try { // 除存檔案
					file = new PrintWriter(tf.getText() + ".txt");
					file.println(ta.getText());
					file.close();
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		});
		setLayout(null);
		setTitle("A10.102502533");
		setSize(500, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
