package ce1002.a10.s102502023;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.io.FileNotFoundException;


public class TextEditor extends JFrame {
	private JTextField jtfFileName = new JTextField(35); // initialize JTestField jtfFileName and have 35 spaces
	private JButton jbtSave = new JButton("Save"); // initialize JButton jbtSave and have String "Save"
	private JTextArea jtaContent = new JTextArea(); // initialize JTestArea jtaContent
  public TextEditor() {
	  jtaContent.setLineWrap(true);
	  jtaContent.setWrapStyleWord(true);
	  jtaContent.setEditable(true);
	  
	  jbtSave.addActionListener(new ButtonListener());
	  
	  JPanel p = new JPanel();
	  p.setLayout(new FlowLayout(FlowLayout.LEFT));
	  p.add(jbtSave);
	  p.add(jtfFileName);
	  
	  setLayout(new BorderLayout());
	  add(p, BorderLayout.NORTH);
	  add(jtaContent, BorderLayout.CENTER);
  }
  
  private class ButtonListener implements ActionListener {
	  @Override
	  public void actionPerformed(ActionEvent e) {
		
		if (jtfFileName.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "File name can��t be empty!");
			return;
		}
			
		java.io.File file = new java.io.File(jtfFileName.getText() + ".txt");
		 
		try {
		java.io.PrintWriter output = new java.io.PrintWriter(file);
		output.print(jtaContent.getText());
		output.close();
		JOptionPane.showMessageDialog(null, "File " + jtfFileName.getText() + ".txt saved!");
		}
		catch (FileNotFoundException ex) {
			System.out.println("There must be wrong");
			System.exit(1);
		}
	  }
  }
}
