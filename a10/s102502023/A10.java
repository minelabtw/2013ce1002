package ce1002.a10.s102502023;

import javax.swing.JFrame;

public class A10 {
  public static void main(String[] args) {
	  TextEditor frame = new TextEditor();
	  frame.setSize(500, 500);
	  frame.setTitle("A10-102502023");
	  frame.setLocationRelativeTo(null);
	  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  frame.setVisible(true);
  }
}
