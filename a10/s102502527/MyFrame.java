package ce1002.a10.s102502527;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame implements ActionListener{
	private  JOptionPane j=new JOptionPane();
	private FileWriter write;//先宣告下面再實作
	private JTextArea area = new JTextArea(15,28);//建立打檔名的地方
    private JTextField field = new JTextField(20);//建立打內容的地方
    private JButton button = new JButton("Save");  //建立按鈕物件
	
	public MyFrame(String title){
    	setTitle(title);
    	
    	setVisible(true);
    	
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new FlowLayout(FlowLayout.CENTER));
        
        getContentPane().add(button);  //加入按鈕
        
        getContentPane().add(field); 
        
        getContentPane().add(area);
        
        setBounds(500,200,350,350);
        
        button.addActionListener(this);  //監聽輸入
        field.addActionListener(this); //監聽輸入
        area.setLineWrap(true);  //空白後換行
    }
	
	public void actionPerformed(ActionEvent e) {
	    
		if(field.getText().equals(""))  //若輸入為方空白
			j.showConfirmDialog(this,"File name can’t be empty!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
		else{
			try {  
				write = new FileWriter(field.getText()+".txt");//將文字轉入檔案
				write.write(area.getText());
	            write.close();
	            j.showConfirmDialog(this,"File "+field.getText()+".txt saved!","訊息",JOptionPane.CLOSED_OPTION);  //跳出對話框
			} catch (IOException a) {
				
				a.printStackTrace();
			}
           
		}	
	}
}
