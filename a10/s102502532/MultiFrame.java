package ce1002.a10.s102502532;

import java.awt.event.*;
import java.io.FileNotFoundException;
import javax.swing.*;

public class MultiFrame extends JFrame {

	private JButton button = new JButton("Save"); // (字串)
	private JTextField textF = new JTextField();
	private JTextArea textA = new JTextArea();
	// private JFrame newFrame1 = new JFrame("訊息");

	public MultiFrame() {
		setLayout(null);

		textA.setLineWrap(true); // true 自動換行

		button.setBounds(5, 5, 80, 30); // Bounds 座標 + 大小
		textF.setBounds(90, 5, 260, 30);
		textA.setBounds(5, 40, 345, 200);
		add(button);
		add(textF);
		add(textA);
		/*
		 * button.setSize(20, 20); add(button , BorderLayout.WEST); add(textF ,
		 * BorderLayout.CENTER); add(textA , BorderLayout.SOUTH);
		 */
		// buttonListener listener = new buttonListener();
		button.addActionListener(new buttonListener());

	}

	private class buttonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			
			          // 直接 call內建的  JOptionPane.ShowMessageDialog() 來用
			try {
				
				if( textF.getText().isEmpty() == true ){                     // 不能用textF.getText() == null
	            	JOptionPane.showMessageDialog(null,"File name can't be empty!", "訊息", JOptionPane.INFORMATION_MESSAGE);            //建立對話視窗
	            }
				else{
				java.io.File file = new java.io.File(textF.getText() + ".txt");    // 不用ce1002.a10.s102502532/
				
				java.io.PrintWriter output = new java.io.PrintWriter(file);       // 建檔
				output.print(textA.getText());                      //字串寫入
				JOptionPane.showMessageDialog(null, "File " + textF.getText() + ".txt" + " saved!", "訊息", JOptionPane.INFORMATION_MESSAGE);
				          //null 固定
				output.close();
				}

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				// e1.printStackTrace();
				
				JOptionPane.showMessageDialog(null,"File name can't be empty!", "訊息", JOptionPane.INFORMATION_MESSAGE);
			}            // catch   ~end
			
		}
	}
} // class MultiFrame ~end

