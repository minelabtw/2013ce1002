package ce1002.a10.s102502544;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class Frame extends JFrame{
	Frame(String name){ //建構子
		setBounds(200,200,200,100); //設定邊界大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel l1 = new JLabel(name); //設定Label
		add(l1);
	}
}
