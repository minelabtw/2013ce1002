package ce1002.a10.s102502544;

import java.awt.EventQueue;
import java.util.Scanner;
import javax.print.DocFlavor.INPUT_STREAM;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

import java.awt.BorderLayout;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileWriter;

public class A10 {

	private JFrame frame;
	private JTextField field;
	private JTextArea area;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					A10 window = new A10();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public A10() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("A10-102502544");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton button = new JButton("Save");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					check();
					FileWriter file = new FileWriter("src/ce1002/a10/s102502544/"+field.getText()+".txt",true);
					file.write(area.getText()+"");
					file.close();
					Frame frame = new Frame("File "+field.getText()+".txt saved!");
					frame.setVisible(true);
					
				}
				catch(Exception e){
					Frame frame = new Frame(e.getMessage());
					frame.setVisible(true);
				}
			}
		});
		button.setBounds(0, 0, 96, 28);
		frame.getContentPane().add(button);
		
		field = new JTextField();
		field.setBounds(94, 0, 340, 28);
		frame.getContentPane().add(field);
		field.setColumns(10);
		
		area = new JTextArea();
		area.setLineWrap(true);
		area.setBounds(0, 26, 434, 235);
		frame.getContentPane().add(area);
	}
	public void check () throws Exception{
		String title = field.getText();
		
		if(title.isEmpty() == true){
			throw new Exception("File name can��t be empty!");
		}
	}
	
	
}
