package ce1002.a10.s102502514;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.*;

public class MyPanel extends JPanel implements ActionListener{
	private JButton jbt = new JButton("Save"); 
	private JTextField jtf = new JTextField();
	private JTextArea jta = new JTextArea();
	private JLabel jlb = new JLabel();
	
	MyPanel(){
		setLayout(null);
		setBounds(0, 0, 450, 400);
		
		jbt.setBounds(5, 5, 80, 30);
		
		jtf.setBounds(90, 5, 370, 30);
		jtf.setText(null);
		
		jta.setBounds(5, 40, 450, 370);
		jta.setLineWrap(true);
		
		jlb.setBounds(60, 84, 225, 40);
		
		add(jbt);
		add(jtf);
		add(jta);
		add(jlb);
		
		jbt.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		try
		{
			check(jtf.getText()); //檢查textfield是否空白,如果空白丟出例外事件
			JOptionPane.showMessageDialog(jlb, ("File " + jtf.getText() + ".txt saved!"), "訊息",
					                      JOptionPane.INFORMATION_MESSAGE);  //用label顯示存檔成功的訊息
			
			FileOutputStream FOS = new FileOutputStream(new File((jtf.getText() + ".txt")));  //輸出一個以textfield為檔名的txt檔
			
			FOS.write(jta.getText().getBytes());  //輸出textarea為txt檔的內文
			FOS.close();
		}
		catch(Exception ex) //處理例外事件
		{
			JOptionPane.showMessageDialog(jlb,  "File name can't be empty!", "訊息",
                      JOptionPane.INFORMATION_MESSAGE);  //用label顯示存檔失敗的訊息
		}
	}
	
	public boolean check(String textfieldisemptyornot) throws Exception{
		if (textfieldisemptyornot.isEmpty()){
			throw new Exception();
		}
		else{
			return true;
		}
	}
	
}
