package ce1002.a10.s102502514;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame(){
		super("A10-102502514"); //設定視窗title
		
		MyPanel panel = new MyPanel();
		
		setLayout(null); 
		setBounds(160,130,470,443);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		add(panel); //新增panel
		
		setVisible(true);
	}

}
