package ce1002.a10.s102502546;

import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener{
	private JButton bt = new JButton("Save"); 
	private JTextField tf = new JTextField();
	private JTextArea ta = new JTextArea();
	private JLabel lab = new JLabel(); 
	
	MyFrame()
	{
		// 基本設定
		setLayout(null);
		setBounds(0, 0, 450, 400);
		
		bt.setBounds(5, 0, 80, 30);
		
		tf.setBounds(90, 0, 370, 30);
		tf.setText(null);
		
		ta.setBounds(5, 35, 450, 370);
		ta.setLineWrap(true);
		
		lab.setBounds(60, 79, 225, 40);
		
		add(bt);
		add(tf);
		add(ta);
		add(lab);
		
		bt.addActionListener((ActionListener) this); //叫出event
	}

		public void actionPerformed(ActionEvent e)
		{
			try // 例外
			{
				checkEmpty(tf.getText()); // 檢查檔名
				JOptionPane.showMessageDialog(lab, ("File " + tf.getText() + ".txt saved!"), "訊息",
						                      JOptionPane.INFORMATION_MESSAGE);
				// 輸出已存檔
				FileOutputStream FOS = new FileOutputStream(new File((tf.getText() + ".txt")));

				FOS.write(ta.getText().getBytes());
				FOS.close();
			}
			catch(Exception excep) // 處理空白檔名
			{
				//輸出不能存空白檔名
				JOptionPane.showMessageDialog(lab,  "File name can't be empty!", "訊息",
	                      JOptionPane.INFORMATION_MESSAGE);
			}
		}
		
		public boolean checkEmpty(String file) throws Exception
		{
			// 檢查是否空白
			if(tf.getText().isEmpty())
			{
				throw new Exception(); 
			}
			else
			{
				return true; 
			}
		}

}
