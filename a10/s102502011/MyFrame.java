package ce1002.a10.s102502011;

import javax.swing.JFrame;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;   
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.JOptionPane;

public class MyFrame extends JFrame {

	MyFrame() {	
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new MyPanel() );
	}
	
	class MyPanel extends JPanel implements ActionListener {
		
		private JButton button = new JButton("Save") ;
		private JTextField textfield = new JTextField(5);
		private JTextArea textarea = new JTextArea(10,10);
		
		MyPanel() {
			
			setLayout(null);
			setSize(450,300) ;
			
			button.setBounds(5,5,65,30); //setButton
			button.addActionListener((ActionListener)this);
			add(button) ;
			 
			textfield.setBounds(80,5,350,30) ; //setTextfield
			add(textfield) ;

			textarea.setBounds(5,35,425,220); //setTextarea
			textarea.setLineWrap(true);
			add(textarea) ;
		}
		
		public void actionPerformed(ActionEvent a) { //save

		    try { 
			   if(textfield.getText().isEmpty())
				   JOptionPane.showMessageDialog(null, "File name can’t be empty!", "訊息", JOptionPane.INFORMATION_MESSAGE );
			   else {
				   File file = new File(textfield.getText() + ".txt");  //write in .txt
				   PrintWriter output = new PrintWriter(file);
				   output.print(textarea.getText());
				   output.close();
				   JOptionPane.showMessageDialog(null, textfield.getText() + ".txt saved!", "訊息", JOptionPane.INFORMATION_MESSAGE );
			   }
		    } catch (FileNotFoundException e) {
			   e.printStackTrace();
		    }
		
		} 
	}

}
