package ce1002.a10.s102502531;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class UseActionListener implements ActionListener{ //用來存檔的一些功能
	JTextField jtextfield;
	JTextArea jtextarea;
	JButton jbutton;
	UseActionListener(JTextField jtextfield,JTextArea jtextarea,JButton jbutton)
	{
		this.jtextfield=jtextfield;
		this.jtextarea=jtextarea;
		this.jbutton=jbutton;
		
	}
	public void actionPerformed(ActionEvent e) {
		if (jtextfield.getText().trim().length() == 0) {
			JOptionPane.showMessageDialog(null, "File name can't be empty!");
		} else {
			try {
				FileWriter filewriter = new FileWriter(jtextfield.getText() + ".txt");
				filewriter.write(jtextarea.getText());
				filewriter.flush();
			} catch (IOException a) {
				System.out.println("get a IOException a");
			}
			JOptionPane.showMessageDialog(null, "File " + jtextfield.getText()
					+ ".txt saved!");
		}
	}

}
