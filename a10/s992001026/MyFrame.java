package ce1002.a10.s992001026;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MyFrame extends JFrame {

	/**
	 * my frame of a10
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtAsd;
	private JTextArea textArea;

	/**
	 * Create the frame.
	 */
	public MyFrame(String title) {
		this.setTitle(title);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textArea = new JTextArea();
		textArea.setBounds(5, 33, 424, 219);
		textArea.setLineWrap(true);
		contentPane.add(textArea);

		JButton btnNewButton = new JButton("Save");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			}
		});
		btnNewButton.setBounds(5, 6, 69, 23);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (txtAsd.getText().equals("")) {
					JOptionPane.showMessageDialog(null,	"File name can't be empty!");
				} else {
					String fileName = txtAsd.getText() + ".txt";
					String fileConstant = textArea.getText();
					FileWriter out;
					try {
						out = new FileWriter(new File(fileName));
						out.write(fileConstant);
					    out.close();
					    JOptionPane.showMessageDialog(null,	"File " + fileName + " saved!");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null,	e.getMessage());
					}
				    
				}
			}
		});
		contentPane.add(btnNewButton);

		txtAsd = new JTextField();
		txtAsd.setBounds(77, 7, 347, 21);
		contentPane.add(txtAsd);
		txtAsd.setColumns(10);
	}
}
