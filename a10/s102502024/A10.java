package ce1002.a10.s102502024;

import java.awt.event.*;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class A10 extends JFrame  implements ActionListener{
	static JButton btn=new JButton("Save");  //設定button
	static Textarea ar=new Textarea();  //設定textarea
	static JTextfield field=new JTextfield(10);  //設定textfield
	 String name;  
     String content;  
	public static void main(String[] args) {
		A10 a=new A10();
		btn.addActionListener(a);
		a.setLayout(null);  //取消預設排版
		a.setSize(500,500);  //設定視窗大小
		a.setLocationRelativeTo(null);  //使視窗置中
		a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //設定關閉鍵
		btn.setBounds(0,0,100,25);  //設定button位置與大小
		ar.setBounds(0,25,500,475);  //設定textarea位置與大小
		ar.setLineWrap(true);  //設定自動換行
		field.setBounds(100,0,400,25);
		a.add(field);
		a.add(ar);
		a.add(btn);
		a.setVisible(true);  //使視窗可見
	}
    @Override
	public void actionPerformed(ActionEvent arg0) {
		 name=field.getText();  //存檔名
		 content=ar.getText();  //存內容
		 if(name.isEmpty()) {
             empty();
         } else {
             name += ".txt";;
             savefile();
             savesuccess();
         }
	}
    void savefile()  //使用緩衝區存檔
    {
    	try {
            FileWriter write = new FileWriter(name);
            write.write(content);
            write.flush();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    void empty() {
        JOptionPane.showMessageDialog(null, "File name can't be empty!", "A10", JOptionPane.ERROR_MESSAGE);  //彈出訊息是空白的視窗
   }
   
    void savesuccess() {
        JOptionPane.showMessageDialog(null, "File " + name + " saved!", "A10", JOptionPane.PLAIN_MESSAGE);  //彈出訊息是成功的視窗
   }
}
