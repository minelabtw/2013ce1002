package ce1002.a10.s101201506;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JButton save = new JButton("Save");
	private JTextField text = new JTextField();
	private JTextArea area = new JTextArea();

	MyFrame() { // 設定 標題 大小 .... 等
		setTitle("A10-101201506");
		setSize(1000, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		area.setLineWrap(true);
		save.setBounds(0, 0, 100, 40);
		text.setBounds(100, 0, 900, 40);
		area.setBounds(0, 40, 1000, 480);
		add(save);
		add(text);
		add(area);
		Savelistener save_listener = new Savelistener();
		save.addActionListener(save_listener);

	}

	public class Savelistener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFrame window = new JFrame("訊息");
			String line = new String();
//按下SAVE之後的小框
			try {
				if (text.getText() == null) {
					throw new Exception();
				} 
				else {
					line = "File " + text.getText() + ".txt saved!";
					File file = new File(text.getText());
					PrintWriter output = new PrintWriter(file);
					output.print(area.getText());
					output.close();
				}
			} 
			catch (Exception g) {
				line = "File name can't be empty!";
			}
			JOptionPane.showMessageDialog(window, line);

		}
	}
}
