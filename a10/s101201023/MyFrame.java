package ce1002.a10.s101201023;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame
{
	JButton button = new JButton("save");
	JTextField field = new JTextField();
	JTextArea area = new JTextArea();
	
	//set frame
	public MyFrame()
	{
		setSize(600 , 600);
		setLayout(null);
		setTitle("A10-101201023");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		setVisible(true);
		add(button);
		add(field);
		add(area);
		button.setBounds(0,0,100,50);
		field.setBounds(101,0,500,50);
		area.setBounds(0,51,600,550);
		area.setLineWrap(true);                                 //����
		button.addActionListener(new Button());
	}
	
	//judge an exception
	public class Button implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			String line = new String();
			try
			{
				if(field.getText() == null)
				{
					throw new Exception();
				}
				else
				{
					File file = new File(field.getText());
					PrintWriter edit = new PrintWriter(file);
					edit.print(area.getText());
					edit.close();
					line = "File" + field.getText() + ".txt saved!";
				}
			}
			
			catch(Exception g)
			{
				line = "File name can��t be empty!";
			}
			JOptionPane.showMessageDialog(new JFrame("�T��"), line);
		}
	}
}