package ce1002.a10.s102502515;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {//MyFrame starts
	private JButton jbtSave = new JButton("Save");//Save button
	private JTextField textfield = new JTextField();//input file name
	private JTextArea textarea = new JTextArea();//input the context
	private JLabel jlbl = new JLabel();
	
	MyFrame()
	{//constructor starts
		
		//frame settings
		setLayout(null);
		setBounds(300,300,480,300);
		setTitle("A10-102502515");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//button settings
		jbtSave.setLayout(null);
		jbtSave.setBounds(20,20,80,30);
		add(jbtSave);
		jbtSave.addActionListener(new SaveListener());
		
		//textfield settings
		textfield.setLayout(null);
		textfield.setBounds(130,20,300,30);
		textfield.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		add(textfield);
		
		//textarea settings
		textarea.setLayout(null);
		textarea.setBounds(20,60,410,150);
		textarea.setLineWrap(true);
		textarea.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		add(textarea);
		
		setVisible(true);
	}//constructor ends
	
	class SaveListener implements ActionListener {//SaveListener starts
		@Override
		public void actionPerformed(ActionEvent e){//actionPerformed starts
			java.io.File file = new java.io.File(textfield.getText()+ ".txt");//create file
			
			if (file.exists()){//如果檔案已存在，則結束程式
				System.out.println("File already exists");
				System.exit(1);
			}

				try {//first do
					check(textfield.getText());//check the file name isEmpty?
					
					//if save sucessfully pop out! for dialog.
					jlbl.setText("File " + textfield.getText()+ ".txt saved!");
					JFrame conframe = new JFrame("");
				    JOptionPane.showMessageDialog(conframe, jlbl);
				    
				    //input the context.
				    PrintWriter printwriter = new PrintWriter(file);
					printwriter.write("" + textarea.getText());
					printwriter.close();
				} catch (Exception e1) {
						//if file name is empty
						jlbl.setText("File name can't be empty!");
					    jlbl.setBackground(Color.BLUE);
					    jlbl.setMinimumSize(new Dimension(200,200));
						JFrame conframe = new JFrame("");
					    JOptionPane.showMessageDialog(conframe, jlbl);
				}
		}//actionPerformed ends	
	}//SaveListener ends

	public boolean check(String filetext) throws Exception{//check the file name isEmpty?
		if(textfield.getText().isEmpty()){
			throw new Exception();
		}
		else
			return true;
	}
}//MyFrame ends
