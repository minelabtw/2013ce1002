package ce1002.a10.s102502518;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JSplitPane splitPane;
	private JTextField fileNameArea;
	private JButton btnSave;
	private JTextArea mainTextArea;

	public MyFrame() {
		setTitle("A10-102502518");//setTitle
		setDefaultCloseOperation(3);
		setBounds(0, 0, 300, 300);//setBounds
		this.contentPane = new JPanel();
		this.contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(this.contentPane);

		this.mainTextArea = new JTextArea();//mainTextArea
		this.mainTextArea.setLineWrap(true);
		this.contentPane.add(this.mainTextArea, "Center");

		this.splitPane = new JSplitPane();//splitPane
		this.contentPane.add(this.splitPane, "North");

		this.fileNameArea = new JTextField();//fileName
		this.splitPane.setRightComponent(this.fileNameArea);
		this.fileNameArea.setColumns(10);

		this.btnSave = new JButton("Save");//Button
		this.btnSave.addActionListener(new SaveButtonActionListener(this));
		this.splitPane.setLeftComponent(this.btnSave);
	}

	class SaveButtonActionListener implements ActionListener {
		JFrame frame;

		public SaveButtonActionListener(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent e) {
			if (MyFrame.this.fileNameArea.getText().isEmpty()) {
				showEmptyFileNameMsg();
			} 
			else {
				String mainText = MyFrame.this.mainTextArea.getText();
				String fileName = MyFrame.this.fileNameArea.getText() + ".txt";//fileName
				try {
					PrintWriter out = new PrintWriter(fileName);
					out.println(mainText);
					out.close();
					showFileSavedMsg(fileName);
				} 
				catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}

		private void showEmptyFileNameMsg() {//EmptyFileNameMsg
			JOptionPane.showMessageDialog(this.frame,"File name can't be empty!");
		}

		private void showFileSavedMsg(String name) {//FileSavedMsg
			JOptionPane.showMessageDialog(this.frame, "File " + name + " saved!");
		}
	}
}
