package ce1002.a10.s102502548;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;

import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class MyPanel extends JPanel implements ActionListener {
	
	protected MyButton button=new MyButton() ;
	protected MyTextarea area=new MyTextarea() ;
	protected MyTextfield field=new MyTextfield() ;
	
	MyPanel(){
		setBounds(0, 0, 500, 300) ;
		
		setLayout(null) ;
		
		button.addActionListener(this) ;
		
		add(button) ;
		add(area) ;
		add(field) ;
	}
	
	public void actionPerformed(ActionEvent e) {
		try{
			FileWriter writer=new FileWriter(field.getText().trim()+".txt") ;//創立檔案
			
			writer.write(area.getText().trim()) ;//寫入緩衝區
			
			writer.flush() ;//把緩衝區的文字寫入檔案
			
			JOptionPane.showMessageDialog(null, field.getText().trim()+".txt has been saved") ;//跳出對話方塊
		}
		
		catch(Exception E){
			E.printStackTrace() ;
		}
	}
	
}
