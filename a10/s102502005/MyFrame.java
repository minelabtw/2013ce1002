package ce1002.a10.s102502005;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame implements ActionListener {

	JButton saveButton = new JButton("Save");//frame 上該有的原件。
	JTextField textField = new JTextField();
	JTextArea textArea = new JTextArea();

	public MyFrame() {

		saveButton.setBounds(5, 5, 70, 30);
		textField.setBounds(80, 5, 350, 30);
		textArea.setBounds(5, 40, 425, 215);

		saveButton.addActionListener(this);//讓MyFrame這個class兼任saveButton的ActionListener。

		textArea.setLineWrap(true);//讓TextArea會自動換行。

		this.setLayout(null);
		this.setSize(450, 300);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setTitle("A10-102502005");

		this.add(saveButton);
		this.add(textField);
		this.add(textArea);

	}

	@Override
	public void actionPerformed(ActionEvent e) {//當saveButton被按下時會執行什麼動作。

		String filename = textField.getText();//把檔名抓下來。
		String filecontent = textArea.getText();//把檔案內容抓下來。

		if (filename.length() == 0) {//若內容為空，TextArea和TextField都會回傳空字串，包含結尾"\0"，所以要用長度判斷而不能用 =="" 或 ==null 來判斷。
			JOptionPane.showMessageDialog(this, "File name can’t be empty!");//JOptionPane不知為何不用宣告...
		} else {
			filename += ".txt";
			try {
				java.io.File file = new java.io.File(filename);//在指定的路徑(此為相對路徑)建立檔案。
				if (file.exists()) {//若已經有同名的檔案存在的話，file.exsits會傳回true。
					file.delete();//刪除那個檔案。
					file = new java.io.File(filename);//再重做一個新的。
				}
				java.io.PrintWriter output = new java.io.PrintWriter(file);//建立一個寫檔案的class。
				output.print(filecontent);//把檔案內容寫道檔案裡。
				output.close();
				JOptionPane.showMessageDialog(this, "File " + filename
						+ " saved!");
			} catch (Exception ex) {
			}

		}
	}
}
