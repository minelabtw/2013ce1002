package ce1002.a10.s102502511;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class MyPanel extends JPanel implements ActionListener{

	private JButton button1 = new JButton("Save"); //定義按紐的輸出的字
	private JTextField textfield = new JTextField(); //呼叫打檔名的地方
	private JTextArea textarea = new JTextArea(); //呼叫打內容的地方
	private JLabel lab1 = new JLabel(); //呼叫出要另外跳出的視窗內容
	
	MyPanel(){
		setLayout(null); 
		setBounds(0,0,getWidth(),getHeight()); //定義所有東西的位置
		button1.setBounds(5,5,65,25);
		textfield.setBounds(75,5,(int)(getWidth() * 31 / 40),25);
		textfield.setText(null);
		textarea.setBounds(5,30,(int)(getWidth() * 38 / 40),(int)(getHeight() * 32 / 40));
		textarea.setLineWrap(true);
		lab1.setBounds(80,120,220,40);
		
		add(button1); //加入所有的元件
		add(textfield);
		add(textarea);
		add(lab1);
		
		button1.addActionListener(this);
	}
	
	
	public void actionPerformed(ActionEvent e) { //定義點選下去的內容
		try{
			check(textfield.getText()); //判斷要不要用例外
				JOptionPane.showMessageDialog(lab1, ("File " + textfield.getText() + ".txt saved!") , "訊息" ,
						JOptionPane.INFORMATION_MESSAGE); //點選Save後，視窗的內容，"內容"+"標題名"
				FileOutputStream FOS = new FileOutputStream(new File((textfield.getText()+".txt"))); //將檔名存儲為在textfield地方打得字
				FOS.write(textarea.getText().getBytes()); //存儲裡面的內容
				FOS.close(); //關閉fos
		}catch(Exception ex){
			JOptionPane.showMessageDialog(lab1, "File name can’t be empty!","訊息",
					JOptionPane.INFORMATION_MESSAGE); //點選Save後，視窗的內容，"內容"+"標題名"
		}
	}
	
	public boolean check (String file) throws Exception{ //篩選檔名的部分是否是空的
		if(textfield.getText().isEmpty()){ //若是空的丟出例外 ， 記的要用isEmpty的方式
			throw new Exception();
		}else{
			return true;
		}
	}
}

		


