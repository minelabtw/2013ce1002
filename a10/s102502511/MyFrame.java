package ce1002.a10.s102502511;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	MyPanel panel = new MyPanel();
	
	MyFrame(){
		super("A10-102502511");
		setLayout(null);
		setBounds(0,0,400,350);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		add(panel);
	}
}
