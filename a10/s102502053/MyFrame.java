package ce1002.a10.s102502053;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;

public class MyFrame extends JFrame  implements ActionListener {
	
	MyPanel mypanel = new MyPanel();
	
	MyFrame() //set frame size
	{
		setLayout(null); 
		setSize(500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		add(mypanel);
		mypanel.save.addActionListener(this); //button action
	}

	@Override
	public void actionPerformed(ActionEvent e) { 
		// TODO Auto-generated method stub
		
		File file = new File(mypanel.filefield.getText() + ".txt"); //output file
		java.io.PrintWriter output = null;
		
		if(mypanel.filefield.getText().equals("")) //check the file name
		{
			JOptionPane.showMessageDialog(null, "File name can't be empty!"); //windows alert
		}
		else
		{
			
			try {
				
				output = new java.io.PrintWriter(file);
				
				} catch (FileNotFoundException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
			output.print(mypanel.textfield.getText()); //print textarea into .txt
			output.close();
			JOptionPane.showMessageDialog(null, "File " + mypanel.filefield.getText() + ".txt saved!");
			
		}
	}
	
	
}
