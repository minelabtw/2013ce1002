package ce1002.a10.s102502035;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class MyFrame extends JFrame {
	JButton button = new JButton("Save");// Object
	JTextField field = new JTextField();
	JTextArea area = new JTextArea();
	OKListenerClass listener1 = new OKListenerClass();
	String filename = "";// variable
	String filetext = "";

	public MyFrame() {
		setTitle("A10-102502035");
		setSize(640, 480);
		setLayout(null);
		setproperty();
		add(button);// add
		add(field);
		add(area);
	}

	public void setproperty() {// set layout property
		button.setBounds(0, 0, 100, 30);
		field.setBounds(100, 0, 522, 30);
		area.setBounds(0, 30, 622, 410);
		button.setVisible(true);
		field.setVisible(true);
		area.setVisible(true);
		area.setLineWrap(true);
		button.addActionListener(listener1);
	}

	class OKListenerClass implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			filename = field.getText();
			filetext = area.getText();
			if (filename.length() == 0) {//no file name
				JOptionPane
						.showMessageDialog(null, "File name can’t be empty!");
			} else {
				try {
					File file = new File(filename + ".txt");
					BufferedWriter output = new BufferedWriter(new FileWriter(
							file));
					output.write(filetext);
					output.close();
					JOptionPane.showMessageDialog(null, "File " + filename
							+ ".txt saved!");
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}
}
