package ce1002.a10.s102502021;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class MyFrame extends JFrame implements ActionListener {
	JButton save = new JButton("Save");
	JTextField title = new JTextField();
	JTextArea area = new JTextArea();

	public MyFrame() {
		save.addActionListener(this);
		save.setSize(100, 30);
		save.setLocation(0, 0);
		title.setSize(400, 30);
		title.setLocation(105, 0);
		area.setSize(500, 460);
		area.setLocation(0, 35);
		area.setLineWrap(true);
		add(save);
		add(title);
		add(area);
		this.setTitle("A10-102502021");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null); // 非預設排版
		setSize(500, 500); // 設置大小
		this.setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		save();
	}

	public void save() {
		if ("".equals(title.getText().trim())) {
			JFrame b = new JFrame();
			JLabel a = new JLabel("File name can't be empty!");
			JOptionPane.showMessageDialog(b, a);
		} else {
			JFrame b = new JFrame();
			JLabel a = new JLabel("File " + title.getText() + ".txt saved!");
			JOptionPane.showMessageDialog(b, a);
			FileWriter out;
			try {
				out = new FileWriter(title.getText() + ".txt", true);
				out.write(area.getText());
				out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
