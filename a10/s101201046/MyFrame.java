package ce1002.a10.s101201046;

import java.awt.*;
import javax.swing.*;

public class  MyFrame extends JFrame {
	
	MyFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setLayout(new GridLayout(1, 1));
		this.setSize(450, 300);
		this.setTitle("A10-s101201046");

		this.add(new MainPanel());		
	}


}
