package ce1002.a10.s101201046;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;

public class MainPanel extends JPanel {
	private JSplitPane sp; //split pane
	private JTextField fn; //filname textfield
	private JButton save; //svae button
	private GridBagConstraints sp_c; //the grid of split pane 

	private JTextArea content; // the content of text file
	private GridBagConstraints content_c; //the gridof content


	private class Saving implements ActionListener { // Saving action
		private PrintWriter wf;
		
		public void actionPerformed(ActionEvent e) {
			if (fn.getText().isEmpty()) //if file name is empty
				showErrorMsgDialog("File name can't be empty!");
			else { //save file
				try {
					wf = new PrintWriter(fn.getText() + ".txt");
					wf.println(content.getText());
					wf.close();
					showSavedMsgDialog(fn.getText());
				} catch (FileNotFoundException ex) {
					ex.printStackTrace();
				}
			}
		}

		private void showSavedMsgDialog(String name) { //show success svaed message
			if (MainPanel.this.getParent() == null) 
				JOptionPane.showMessageDialog(MainPanel.this, "File " + name + " Saved!");
			else
				JOptionPane.showMessageDialog(MainPanel.this.getParent(), "File " + name + " Saved!");
		}

		private void showErrorMsgDialog(String msg) { //show error message
			if (MainPanel.this.getParent() == null) 
				JOptionPane.showMessageDialog(MainPanel.this, msg);
			else
				JOptionPane.showMessageDialog(MainPanel.this.getParent(), msg);
		}
	}

	private void sp() { //construct the infomation of split pane in main panel
		save = new JButton("Save");
		save.addActionListener(new Saving());

		fn = new JTextField();
		
		sp = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, save, fn); 

		sp_c = new GridBagConstraints();
		sp_c.weightx = 1.0;
		sp_c.weighty = 0.0;
		sp_c.anchor = GridBagConstraints.FIRST_LINE_START;
		sp_c.fill = GridBagConstraints.HORIZONTAL;	
		add(sp, sp_c);
	}

	private void content() { //construct the content in main panel
		content = new JTextArea();
		content.setLineWrap(true);
		content.setVisible(true);

		content_c = new GridBagConstraints();
		content_c.gridy = 1;
		content_c.weightx = 1.0;
		content_c.weighty = 1.0;
		content_c.anchor = GridBagConstraints.LINE_START;
		content_c.fill = GridBagConstraints.BOTH;
		
		add(content, content_c);	
	}


	MainPanel() { //initial MainPanel
		this.setLayout(new GridBagLayout());
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setVisible(true);

		sp();
		content();
	}
}
