package ce1002.a10.s102502043;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class MyPanel extends JPanel
{
	JButton button = new JButton("Save");
	JTextArea txta = new JTextArea();
	JTextField txtfd = new JTextField();
	 ActionListener save = new Save();
	MyPanel()
	{
		setLayout(null);//自行排版
		setBounds(0,0,700,520);//先設定面板的問位置與大小
		button.setBounds(0,0,100,50);//設定按鈕的大小與位置
		add(button);//加入按鈕
		button.addActionListener(save);
		txta.setBounds(0,50,685,500);//設定textarea的位置大小
		txta.setLineWrap(true);//設定textarea的自動換行
		add(txta);//加入textarea
		txtfd.setBounds(100,0,600,50);
		add(txtfd);
	}
	
}
class Save implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		
		JOptionPane.showMessageDialog(null, "File .txt saved!", "A10", JOptionPane.PLAIN_MESSAGE);
	}
	
}