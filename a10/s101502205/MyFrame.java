package ce1002.a10.s101502205;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;


public class MyFrame extends JFrame{
	private JTextField textField;
	private JTextArea textArea;	
	
	public MyFrame() {
		getContentPane().setLayout(null);
		
		// Create text Field
		textField = new JTextField();
		textField.setBounds(121, 13, 339, 22);
		getContentPane().add(textField);
		
		// Create save button
		JButton btnNewButton = new JButton("Save");
		btnNewButton.setBounds(12, 12, 97, 25);
		getContentPane().add(btnNewButton);
		// Add listener
		SaveBtnListener btnListener = new SaveBtnListener();
		btnNewButton.addActionListener(btnListener);
		
		
		// Create save button
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(12, 48, 448, 219);
		getContentPane().add(textArea);
		
		// set title and settings
		setTitle("A10-101502205");
		setSize(490, 325);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	String getFileName() throws Exception {
		String fileName = textField.getText();
		
		// if file name is empty then throw Exception
		if (fileName.length()==0){
			throw new Exception("File name can��t be empty!");
		}
		
		// Add .txt at the end of file name
		fileName += ".txt";
		
		return fileName;
	}
	
	void saveFile() {
		String fileName;
		try{
			fileName = getFileName();					// may throw Exception (file name is empty)
			File file = new File(fileName);				// may throw IOException (ex: access denied)
			PrintWriter output = new PrintWriter(file);	// may throw IOException
			output.println(textArea.getText());
			// close file
			output.close();
			// pop out message dialog
			JOptionPane.showMessageDialog(null, "File " + fileName + " saved!");
		}catch(IOException ioe) {
			// IOException handling
            JOptionPane.showMessageDialog(null, ioe.getMessage());
        }catch(Exception e) {
        	// Exception handling
        	JOptionPane.showMessageDialog(null, e.getMessage());
        }
	}
	
	// Listener for button
	class SaveBtnListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			saveFile();
		}
	}
}
