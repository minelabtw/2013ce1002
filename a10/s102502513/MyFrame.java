package ce1002.a10.s102502513;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame{
	private JButton button;//定義JBUTTON底下的參數
	private JTextArea textarea;
	private JTextField textfield;
	
	MyFrame(){//建構子
		setTitle("A10-102502513");//設定標頭
		setSize(450,300);//設定大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		
		button = new JButton("Save");//設定save的按鈕
		button.setBounds(5,5,75,30);//按鈕大小
		button.setHorizontalTextPosition(JButton.CENTER);//字體置中 
		button.addActionListener(new SaveFile());//會觸發下面的函式
		add(button);
		
		textarea = new JTextArea();
		textarea.setBounds(5,40,435,215);
		textarea.setLineWrap(true);//會換行
		add(textarea);
		
		textfield = new JTextField();
	    textfield.setBounds(85,5,355,30);
	    textfield.setColumns(10);
	    add(textfield);
		}
	
	class SaveFile implements ActionListener{
		String filename;//設定參數
		String ext = "txt";
		SaveFile(){//建構子
			
		}
		public void actionPerformed(ActionEvent event)
		{
			filename = textfield.getText();//得到檔名
			if (filename.isEmpty())//如果檔名是空的
			{
				JOptionPane.showMessageDialog(null, "File name can't be empty!", "A10", JOptionPane.ERROR_MESSAGE);
			}
			else
			{
				try
				{
					FileWriter fout = new FileWriter(filename + "." + ext);//儲存檔案
	                fout.write(textarea.getText());
	                fout.flush();
					JOptionPane.showMessageDialog(null, "File " + filename + "." + ext + " saved!", "A10", JOptionPane.PLAIN_MESSAGE);
				}
				catch(IOException ex)
				{
					ex.printStackTrace();
				}
			}
		}
		
	}
}

