package ce1002.a10.s102502048;

import java.util.Scanner;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A10 extends JFrame {

	private JButton save = new JButton("Save");
	private JTextField filename = new JTextField();
	// Create a JTextArea
	private JTextArea area = new JTextArea(12, 25);

	public A10() {
		// Panel p1 to hold labels and text fields
		JPanel p1 = new JPanel(new GridLayout(1, 2, 5, 5));
		p1.add(save);
		p1.add(filename);

		// Panel p2 to hold the button
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		area.setLineWrap(true);
		p2.add(area);

		// Add the panels to the frame
		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);

		// Register listener
		save.addActionListener(new ButtonListener());
	}

	/** Handle the Compute Payment button */
	private class ButtonListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// Get values from text fields
			String input = filename.getText();
			if(input == null)
			{
				JOptionPane.showMessageDialog(null,"Finish!","File name can��t be empty!",JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				java.io.File file = new java.io.File(input+".txt");
				if (file.exists()) {
				      System.out.println("File already exists");
				      System.exit(0);
				    }
				// Create a file
			    java.io.PrintWriter output = new java.io.PrintWriter(file);
			    
			 // Close the file
			    output.close();
			}

		}
	}

	public static void main(String[] args) {
		A10 frame = new A10();
		/* set frame's size , layout... */
		frame.pack();
		frame.setTitle("LoanCalculator");
		frame.setSize(300, 300);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
