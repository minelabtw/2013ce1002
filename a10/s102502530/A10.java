package ce1002.a10.s102502530;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class A10 {
	public static void main(String[] args) {
		final JFrame frame = new JFrame();
		final JButton button = new JButton();
		final JTextField textField = new JTextField();
		final JTextArea textArea = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(textArea);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(310, 360);
		frame.setTitle("A10-s102502530");
		frame.getContentPane().setLayout(new FlowLayout());
		button.setText("Save");
		textField.setPreferredSize(new Dimension(200, button.getPreferredSize().height));
		scrollPane.setPreferredSize(new Dimension(270, 270));
		textArea.setLineWrap(true);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(textField.getText().isEmpty())
					JOptionPane.showMessageDialog(frame, "File name can't be empty!", "Error", JOptionPane.ERROR_MESSAGE);
				else {
					try {
						FileWriter writer = new FileWriter(new File(textField.getText()));
						writer.write(textArea.getText());
						writer.close();
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				}
			}
		});
		frame.getContentPane().add(button);
		frame.getContentPane().add(textField);
		frame.getContentPane().add(scrollPane);
		frame.setVisible(true);
	}
}