package ce1002.a10.s102502545;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

class MyFrame extends JFrame {

	JButton save;
	JTextField filename;
	JTextArea text;

	public MyFrame() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		setTitle("A10");
		new_all_member();

		setSize(400, 300);
		setVisible(true);
	}

	public void new_all_member() {
		save = new JButton("Save");
		save.setBounds(5, 5, 75, 30);
		ActionListener saving = new Saving();
		save.addActionListener(saving);
		add(save);

		filename = new JTextField();
		filename.setBounds(85, 5, 355, 30);
		filename.setColumns(10);
		add(filename);

		text = new JTextArea();
		text.setBounds(5, 40, 435, 215);
		text.setLineWrap(true);
		add(text);
	}

	public class Saving implements ActionListener {// 監聽唷

		JFrame frame;
		String name;
		String content;

		Saving() {
		}

		public void actionPerformed(ActionEvent event) {// 按鈕
			name = filename.getText();
			content = text.getText();
			if (name.isEmpty()) {
				empty();
			} else {
				name += ".txt";
				;
				save();
				success();
			}
		}

		// 存檔
		public void save() {
			try {
				FileWriter fout = new FileWriter(name);
				fout.write(content);
				fout.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// 跳出empty的視窗
		public void empty() {
			JOptionPane.showMessageDialog(null, "File name can't be empty!",
					"A10", JOptionPane.ERROR_MESSAGE);
		}

		// 跳出success的視窗
		public void success() {
			JOptionPane.showMessageDialog(null, "File " + name + " saved!",
					"A10", JOptionPane.PLAIN_MESSAGE);
		}
	}
}