package ce1002.a10.s102502008;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;
public class MyFrame extends JFrame
{
		JPanel upPanel= new JPanel() ;
		JButton button= new JButton("Save") ;
		JTextField textField= new JTextField() ;
		JTextArea textArea= new JTextArea();
		ButtonListener listener= new ButtonListener();
		public MyFrame()
		{
			this.setLayout(new BorderLayout());
			this.setLocationRelativeTo(null);
			this.setTitle("A10-102502008");
			this.setSize(400,400);
			//set the frame
			upPanel.setLayout(new BorderLayout());
			button.setSize(50, 30);
			button.addActionListener(listener);
			//set button
			upPanel.add(button,BorderLayout.WEST) ;
			upPanel.add(textField,BorderLayout.CENTER) ;
			// set panel
			textArea.setLineWrap(true);
			this.add(upPanel,BorderLayout.NORTH);
			this.add(textArea,BorderLayout.CENTER) ;
			this.setVisible(true);
		}
		
		class ButtonListener implements ActionListener
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if(textField.getText().equals(""))
				{
					JPanel panel = new JPanel();  
				    panel.add(new JLabel("File name can't be empty!"));
				    // display the jpanel in a joptionpane dialog, using showMessageDialog
				    JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
				    JOptionPane.showMessageDialog(frame,panel);
				}					//輸入為空白時
				else
				{
					java.io.File file=new java.io.File(textField.getText()+".txt");
					try 
					{
						java.io.PrintWriter output= new java.io.PrintWriter(file) ;
						output.println(textArea.getText()+"\r\n") ;
						output.close();
						JPanel panel = new JPanel();  
					    panel.add(new JLabel("File "+textField.getText()+".txt saved!"));
					    // display the jpanel in a joptionpane dialog, using showMessageDialog
					    JFrame frame = new JFrame("JOptionPane showMessageDialog component example");
					    JOptionPane.showMessageDialog(frame,panel);
					} 
					catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
					
				}
			}
		}
}
