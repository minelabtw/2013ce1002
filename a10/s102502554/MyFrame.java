package ce1002.a10.s102502554;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import javax.swing.*;
import javax.swing.border.EmptyBorder;


public class MyFrame  extends JFrame
{
	private JPanel contentPane;
    private JSplitPane splitPane;
    private JTextField FileName;//the name of the text file
    private JButton SaveButton;//button of save
    private JTextArea Text;//things we input
    //name the object i will use after
	
    public class ButtonActionListener implements ActionListener{//ButtonActionListener class is begin    	
    	private void EmptyFileName(){
            JOptionPane.showMessageDialog(frame, "File name can't be empty!");
        }//print if no file name

        private void SavedFile(String name){
            JOptionPane.showMessageDialog(frame, (new StringBuilder("File ")).append(name).append(" saved!").toString());
        }//print if we save the file
    	
    	public void actionPerformed(ActionEvent e){//action begin
            if(FileName.getText().isEmpty()){
                EmptyFileName();
            }
            else{
                String mainText = Text.getText();
                String fileName = (new StringBuilder(String.valueOf(FileName.getText()))).append(".txt").toString();
                try
                {
                    PrintWriter print = new PrintWriter(fileName);
                    print.println(mainText);
                    print.close();
                    SavedFile(fileName);
                }
                catch(FileNotFoundException E)
                {
                    E.printStackTrace();
                }
            }//action end
        }//ButtonActionListener class is finish

                JFrame frame;
                MyFrame Frame;

        public ButtonActionListener(JFrame frame){
        	Frame = MyFrame.this;
            this.frame = frame;
        }
    }


    public MyFrame() {
        setTitle("A10-102502554");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(0,0,400,300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(0,0,0,0));
        contentPane.setLayout(new BorderLayout(0,0));        
        setContentPane(contentPane);       
        Text = new JTextArea();
        Text.setLineWrap(true);        
        contentPane.add(Text, "Center");        
        splitPane = new JSplitPane();
        contentPane.add(splitPane, "North");
        FileName = new JTextField();
        splitPane.setRightComponent(FileName);
        FileName.setColumns(10);
        SaveButton = new JButton("Save");
        SaveButton.addActionListener(new ButtonActionListener(this));
        splitPane.setLeftComponent(SaveButton);
        setVisible(true);
    }//set states
}

