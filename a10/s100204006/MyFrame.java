package ce1002.a10.s100204006;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MyFrame 
{
	private Panela panel1;
	
	 class Panela extends JPanel 
	 {
		 Panela() 
		 {
           // set a preferred size for the custom panel.
			 setPreferredSize(new Dimension(500,500));
		 }
		 
		 @Override 
		 public Dimension getPreferredSize() 
		 { 
			 return new Dimension(500, 500); 
		 }

		 public void paintComponent(Graphics g) 
		 {
			 super.paintComponent(g);
			 
			 JButton button = new JButton("Save");                   // 創建一個按鈕對象
			 button.setBounds(175, 135, 50, 30);                     // 設置按鈕的位置和大小
			 button.setMargin(new Insets(0, 0, 0, 0));               // 設置邊框和標籤的間距
			 button.setHorizontalTextPosition(JButton.CENTER);       // 设置文本相对于图像的显示位置
			 button.addActionListener(new ActionListener()           // 捕获按钮的ActionEvent事件
			 {         
				 public void actionPerformed(ActionEvent e) 
				 {
					 //System.out.println("ActionEvent!");
				 }
			 });
			 button.addChangeListener(new ChangeListener()           // 捕获按钮的ChangeEvent事件
			 {        
				 public void stateChanged(ChangeEvent e) 
				 {
					 //System.out.println("ChangeEvent!");
				 }
			 });
			 JTextField field = new JTextField(20);                 //新增一個文字框
			 
		 }
	 }
	 public MyFrame() 
	 {
		 this.setLayout(null);
		 newRolePos(0,0);
	 }
	 public void newRolePos(int x, int y) 
	 {
		 panel1 = new Panela();
		 panel1.setLayout(null);
		 panel1.setBounds(x, y, 500, 500);
		 		 		 
		 this.add(panel1);
		
	 } 
	 
	
}
