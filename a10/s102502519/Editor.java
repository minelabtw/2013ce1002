package ce1002.a10.s102502519;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

class Editor extends JFrame {

	JButton save;
	JTextField filename;
	JTextArea message;

	Editor() {    //邊框
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		setTitle("A10-102502519");
		new_all_member();

		setSize(450, 300);
		setVisible(true);
	}

	void new_all_member() {    //設定各種東西  邊界、建構子等
		save = new JButton("Save");
		save.setBounds(5, 5, 75, 30);
		ActionListener saving = new Saving(this);
		save.addActionListener(saving);
		add(save);

		filename = new JTextField();
		filename.setBounds(85, 5, 400, 30);
		filename.setColumns(10);
		add(filename);

		message = new JTextArea();
		message.setBounds(10, 40, 420, 210);
		message.setLineWrap(true);
		add(message);
	}

	class Saving implements ActionListener {

		JFrame frame;
		String name;
		String content;

		Saving(JFrame frame) {
			this.frame = frame;
		}

		public void actionPerformed(ActionEvent event) { // 按鈕
			name = filename.getText() + ".txt";
			content = message.getText();
			if (name.isEmpty()) {
				show_empty();
			} else {
				save_into_file();
				show_save_success();
			}
		}

		void save_into_file() { // 將字串存入檔案裡
			try {
				FileWriter f = new FileWriter(name);
				f.write(content);
				f.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		void show_empty() { // empty時輸出File name can't be empty!
			JOptionPane.showMessageDialog(frame, "File name can't be empty!");
		}

		void show_save_success() { // success時輸出成功的字串
			JOptionPane.showMessageDialog(frame, "File " + name + " saved!");
		}
	}
}
