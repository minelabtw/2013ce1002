package ce1002.a10.s102502001;

import javax.swing.JFrame;

public class Frame extends JFrame{
	
	Panel panel = new Panel();
	
	public Frame(){
		JFrame frame = new JFrame();     
		setTitle("A10-102502001");      //set frame
		setSize(450,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		add(panel);                     //add panel
	}
}
