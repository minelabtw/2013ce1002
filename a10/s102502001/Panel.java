package ce1002.a10.s102502001;

import java.awt.Dimension;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Panel extends JPanel{
	
	JButton save = new JButton("save");                  //create a new button
	JTextField field = new JTextField(30);               //create a anew textfield
	private String defaultarea;
	JTextArea area = new JTextArea(defaultarea, 11, 37); //set the size of textarea
	
	
	public Panel(){
		 area.setEditable( true );                       //set area
	     area.setAutoscrolls( true );
	     area.setLineWrap(true);
		
		add(save);                                       //add onto panel
		add(field);
		add(area);
		
		save.addActionListener(new OKListenerClass(null));//addActionListener
	}
	
	class OKListenerClass implements ActionListener{
		 JFrame frame;
		 String name;
		 String content;
		  
		 OKListenerClass(JFrame frame) {
		  this.frame = frame;
		 }
		 OKListenerClass(){
			
		}

		@Override
		public void actionPerformed(ActionEvent event) {
	    name = field.getText();
	    content = area.getText();
	    if(name.isEmpty()) {                                //check if textfield is empty
	    show_empty();
	    } 
	    else {
	    name = name + ".txt";
		save_into_file();
		show_save_success();
	    }
		}

	    void save_into_file() {
	    	try {
	    	      
	    	      //create an print writer for writing to a file
	    	      PrintWriter out = new PrintWriter(new FileWriter("output.txt"));

	    	      //close the file
	    	      out.close();
	    	   }
	    	      catch(IOException e1) {
	    	      
	    	      }
	    	   }
	    void show_empty() {
			             JOptionPane.showMessageDialog(null, "File name can't be empty!");  //show File name can't be empty!
			        }

	    void show_save_success() {
			             JOptionPane.showMessageDialog(null, "File " + name + " saved!");   //show file saved
			  }
			 
			  }
}
	


