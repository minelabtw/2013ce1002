package ce1002.a10.s102502559;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MyFrame extends JFrame{
	
	private JPanel panel;
	private JTextField _fileName;
	private JButton fileSave;
	private JTextArea fileContent;
	private JSplitPane splitPane;
	
	public MyFrame()
	{
		setTitle("A10-TA"); 
		setSize(500,500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel.setBorder(new EmptyBorder(5,5,5,5)); //top-left-bottom-right gap 
		panel.setLayout(new BorderLayout(1,1));//height-width gap
		setContentPane(panel);
		
		splitPane = new JSplitPane(); // build a split panel to contain two components
		panel.add(splitPane,"North");
		
		_fileName = new JTextField(); 
		_fileName.setColumns(10); // set width
		splitPane.setRightComponent(_fileName); // add to split panel's right
		
		fileSave = new JButton("Save");
		fileSave.addActionListener(new SaveButtonActionListener()); // activate the listener
		splitPane.setLeftComponent(fileSave);
		
		fileContent = new JTextArea();
		fileContent.setLineWrap(true); 
		panel.add(fileContent,"Center");
	}
	
	class SaveButtonActionListener implements ActionListener
	{

		String fileName;
		String ext = "txt";
		
		public SaveButtonActionListener()
		{
			
		}
		
		@Override
		public void actionPerformed(ActionEvent event) {
			
			fileName = _fileName.getText();
			if(fileName.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"File name can't be empty","A10",JOptionPane.ERROR_MESSAGE);//if name is empty,show message to warn
			}
			else
			{
				try
				{
					File file = new File(fileName+"."+ext); // Build a file
					PrintWriter output = new PrintWriter(file); // Build a file writer
					output.println(fileContent.getText()); // write something into it
					output.close();//close the writer
					JOptionPane.showMessageDialog(null, "File"+fileName+".txt saved!", "A10", JOptionPane.PLAIN_MESSAGE);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
