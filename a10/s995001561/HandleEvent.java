package ce1002.a10.s995001561;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



@SuppressWarnings("serial")
public class HandleEvent extends JFrame {
	
	// Create elements
	JButton jbtOK = new JButton("Save");
	JTextField jtf = new JTextField(21);
	JTextArea jta = new JTextArea(15,27);


	public HandleEvent() {
		
	jta.setLineWrap(true);	
	// Create a panel to hold elements
	@SuppressWarnings("unused")
	JPanel panel = new JPanel();
	setLayout(new FlowLayout());
	
	add(jbtOK);
	add(jtf);
	add(jta);
	
	OKListenerClass listener = new OKListenerClass();
	
	jbtOK.addActionListener(listener);

	
//	BufferedWriter buf = new BufferedWriter(new FileWriter(file));
//	buf.write(jta.getText());
//	buf.close();
	
	}
	
	
	class OKListenerClass implements ActionListener{
		
		JFrame frame = new JFrame();

		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			if( jtf.getText().length() == 0) JOptionPane.showMessageDialog(frame, "File name can��t be empty!");
			else JOptionPane.showMessageDialog(frame, "File txt saved!");

		}
		
		

	}
	
	
	// create file
	class WriteData{
		
		WriteData() {
		try {
			String content = jta.getText();
			File file = new File(jtf.getText()+".txt");
			// if file doesnt exists, then create it
						if (!file.exists()) {
							file.createNewFile();
						}
						
						FileWriter fw = new FileWriter(file.getAbsoluteFile());
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(content);
						bw.close();
			 
		} catch (IOException e) {
			e.printStackTrace();
			
		
		}



		}
		
	}

}	
	



