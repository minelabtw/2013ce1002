package ce1002.a10.s102502031;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyPanel extends JPanel {
	JButton button = new JButton("Save");
	JTextField textField = new JTextField(226);
	JTextArea textArea = new JTextArea();

	MyPanel(int width, int height) {
		this.setSize(width, height);
		this.setLayout(null);

		button.addActionListener(new SaveListenerClass());
		button.setLocation(5, 5);
		button.setSize(62, 26);
		textField.setLocation(72, 5);
		textField.setSize(width - 77, 26);
		textArea.setLocation(5, 36);
		textArea.setSize(width - 10, height - 41);

		this.add(button);
		this.add(textField);
		this.add(textArea);
	} // end MyPanel(width, height)

	@Override
	// a way I search in the Internet to avoid tiny error of JFrame size
	public Dimension getPreferredSize() {
		return new Dimension(this.getWidth(), this.getHeight());
	} // end override of getPreFerredSize()

	class SaveListenerClass implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent actionEvent) {
			String fileName = new String(textField.getText() + ".txt");
			switch (fileName) {
				case ".txt":
					JOptionPane.showMessageDialog(null, "File name can't be empty!", "Message", JOptionPane.ERROR_MESSAGE);
					break;
				default:
					File file = new File(fileName);
					if (!file.exists()) {
						try {
							file.createNewFile();
						} catch (IOException ioException) {
							ioException.printStackTrace();
						} // end try catch
					} // end if
					try {
						PrintWriter printWriter = new PrintWriter(fileName);
						printWriter.write(textArea.getText());
						printWriter.close();
					} catch (FileNotFoundException fileNotFoundException) {
						fileNotFoundException.printStackTrace();
					}
					JOptionPane.showMessageDialog(null, "File " + fileName + " saved!", "Message", JOptionPane.INFORMATION_MESSAGE);
					break;
			} // end switch
		} // end override of actionPerformed
	} // end class SaveListenerClass
} // end class MyPanel
