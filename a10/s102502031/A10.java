package ce1002.a10.s102502031;

public class A10 {
	public static void main(String[] args) {
		// for TA: if you consider that the frame is too small, please change here
		final int width = 400;
		final int height = 450;
		MyFrame frame = new MyFrame();
		MyPanel panel = new MyPanel(width, height); // decide the size of MyFrame by the size of MyPanel

		frame.add(panel);
		frame.pack();
	} // end main
} // end class A10