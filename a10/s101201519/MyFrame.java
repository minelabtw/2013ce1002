package ce1002.a10.s101201519;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.io.File;
import java.io.PrintWriter;

public class MyFrame extends JFrame{
	public JButton save=new JButton("Save");
	public JTextField text=new JTextField();
	public JTextArea area=new JTextArea();
	
	MyFrame()
	{
		setTitle("A-10-101201519");
		setSize(515,450);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		area.setLineWrap(true);//使區塊可以寫好幾行
		save.setBounds(0,0,100,30);
		text.setBounds(100,0,400,30);
		area.setBounds(0,30,500,380);
		
		add(save);
		add(text);
		add(area);
		
		Savelistener save_lis=new Savelistener();
		save.addActionListener(save_lis);
	}
	public class Savelistener implements ActionListener{//錯誤訊息
		public void actionPerformed(ActionEvent e)
		{
			JFrame window=new JFrame("訊息");
			String line=new String();
			try
			{
				if(text.getText()!=null)//存檔
				{
					line = "File " + text.getText() + ".txt saved!";
					File file = new File(text.getText());//open the file
					PrintWriter output = new PrintWriter(file);//use the PrintWriter class to write into the file
					output.print(area.getText());//write into the file 
					output.close();
				}
				else
				{
					throw new Exception();
				}
			}
			catch (Exception g) //無檔名時
			{
				line = "File name can't be empty!";
			}
			JOptionPane.showMessageDialog(window, line);//跳出新視窗
		}
	}
}
