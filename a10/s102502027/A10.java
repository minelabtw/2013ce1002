package ce1002.a10.s102502027;

import java.awt.event.*;
import java.io.*;
import javax.swing.*;


public class A10 extends JFrame {
	public JTextField textfield = new JTextField();
	private JButton button = new JButton("save"); 
	JPanel panel = new JPanel();
	JTextArea textarea = new JTextArea();

	A10() { //內部components

		setLayout(null);
		textfield.setBounds(100, 0, 380, 30);
		button.setBounds(5, 0, 80, 30);
		textarea.setBounds(5, 35, 470, 222);
		textarea.setLineWrap(true);
		add(button);
		add(textfield);
		add(textarea);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (textfield.getText().isEmpty()) //判斷是否有字
					JOptionPane.showMessageDialog(null,
							"File name can’t be empty!", "訊息",
							JOptionPane.INFORMATION_MESSAGE); //跳出視窗
				else
					JOptionPane.showMessageDialog(null,
							"File " + textfield.getText() + ".txt saved!",
							"訊息", JOptionPane.INFORMATION_MESSAGE);
				PrintWriter out; 
				try {        //存檔
					out = new PrintWriter(textfield.getText() + ".txt");
					out.println(textarea.getText());
					out.close();
				} catch (FileNotFoundException e1) { //例外
					e1.printStackTrace();
				}

			}
		});

	}

	public static void main(String[] args) { //大視窗
		JFrame frame = new A10();
		frame.setTitle("A10.102502027");
		frame.setSize(500, 300);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}// end
