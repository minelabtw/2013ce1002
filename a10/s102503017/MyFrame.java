package ce1002.a10.s102503017;

import javax.swing.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.*;

public class MyFrame extends JFrame {
	 
	JPanel p = new JPanel();
	JTextField input = new JTextField();
	JTextArea body = new JTextArea();
	JButton button = new JButton("Save");
	ActionListener listener = new ActionListener(){
										//declaring the event, if the title is not empty, output a txt file at local location; If not, tell the user it is empty.
										public void actionPerformed(ActionEvent event){
												String textInArea = body.getText();
												//System.out.println("111" + textInArea + "222");
												String textInField = input.getText();
												//System.out.println("111" + textInField + "222");
												File file = new File(textInField + ".txt");	
												if(textInField.equals(""))
												{
													JLabel msgerr = new JLabel("File name cannout be empty!");
													JOptionPane.showMessageDialog(null, msgerr, "�u��", JOptionPane.INFORMATION_MESSAGE);
												}
												else
												{
													try {
														BufferedWriter buf = new BufferedWriter(new FileWriter(file));
														buf.write(textInArea);
														buf.close();
														JLabel msg = new JLabel("File " + input.getText() + ".txt saved!");
														JOptionPane.showMessageDialog(null, msg, "�u��", JOptionPane.INFORMATION_MESSAGE);
													} 
													catch (IOException e) {
														e.printStackTrace();
													}
												}
										}
									};

	MyFrame()
	{
		//setting of the elements.
		setTitle("A10 - 102503017");
		getContentPane().setLayout(null);
		setVisible(true);
		setBounds(0,0,500,500);
		p.setBounds(0,0,500,500);
		p.setLayout(null);
		input.setBounds(120,10,342,20);
		button.setBounds(10,10,100,20);
		button.addActionListener(listener);
		p.add(button);
		p.add(input);		
		body.setLineWrap(true);
		body.setBounds(10,30,452,400);
		p.add(body);
		getContentPane().add(p);

		
	}
	

}
