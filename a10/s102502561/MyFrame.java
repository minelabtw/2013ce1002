package ce1002.a10.s102502561;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyFrame extends JFrame implements ActionListener {
	private JOptionPane optionpane = new JOptionPane();
	private FileWriter fileWriter;
	private JTextArea textarea = new JTextArea(15, 28);// 建立打檔名的地方
	private JTextField textfield = new JTextField(20);// 建立打內容的地方
	private JButton button = new JButton("Save"); // 建立按鈕物件

	public MyFrame() {
		setTitle("A10-102502561");
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new FlowLayout(FlowLayout.CENTER));
		getContentPane().add(button); // save button
		getContentPane().add(textfield); // main text
		getContentPane().add(textarea); // title text
		setBounds(500, 200, 400, 400); // frame size
		button.addActionListener(this); // 監聽使用者輸入事件
		textfield.addActionListener(this); // 監聽使用者輸入事件
		textarea.setLineWrap(true); // 超過空白區域就換行
	}

	public void actionPerformed(ActionEvent e) {

		if (textfield.getText().equals(""))// 輸入內容空白
			optionpane.showConfirmDialog(this, "File name can't be empty!",
					"訊息", JOptionPane.CLOSED_OPTION);

		else {
			try {
				fileWriter = new FileWriter(textfield.getText() + ".txt");
				fileWriter.write(textarea.getText());
				fileWriter.close();
				optionpane.showConfirmDialog(this,
						"File " + textfield.getText() + ".txt saved!", "訊息",
						JOptionPane.CLOSED_OPTION);
			} catch (IOException e1) {

				e1.printStackTrace();
			}

		}
	}
}
