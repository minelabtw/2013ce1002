package ce1002.a10.s984008030;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileWriter;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextArea;

public class MyFrame extends JFrame {
	
	private JTextField textField; // User types the file name
	private JTextArea textArea; // User types the content
	
	public MyFrame() {
		// Initialize the panel to place btnNewButton and textField
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		// Initialize btnNewButton and set it's click listener
		JButton btnNewButton = new JButton("Save");
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String fileName = textField.getText() + ".txt"; // Get file name
				if (fileName.equals(".txt")) {
					// fileName is empty
					JOptionPane.showMessageDialog(null,
													"File name can’t be empty!",
													"訊息",
													JOptionPane.PLAIN_MESSAGE );
				}
				else {
					// fileName is not empty, save the file
					try {
						FileWriter out = new FileWriter(fileName, true);
						out.write(textArea.getText());
						out.close();
						JOptionPane.showMessageDialog(null,
													"File " + fileName + " saved!",
													"訊息",
													JOptionPane.PLAIN_MESSAGE );
						
					} catch (Exception e2) {
						// TODO: handle exception
						JOptionPane.showMessageDialog(null,
													"File " + fileName + " can not saved!",
													"訊息",
													JOptionPane.PLAIN_MESSAGE );
					}
				}
			}
			
		});
		panel.add(btnNewButton);
		
		this.textField = new JTextField();
		panel.add(this.textField);
		this.textField.setColumns(29);
		
		this.textArea = new JTextArea();
		this.textArea.setWrapStyleWord(true);
		getContentPane().add(this.textArea, BorderLayout.CENTER);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(0, 0, 500, 500);
	}

}
