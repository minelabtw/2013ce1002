package ce1002.a10.s101201508;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.PrintWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	private JButton save = new JButton("Save");
	private JTextField text = new JTextField();
	private JTextArea area = new JTextArea();

	MyFrame() {
		//set the data
		setTitle("A10-101201508");
		setSize(1015, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		area.setLineWrap(true);
		save.setBounds(0, 0, 100, 30);
		text.setBounds(100, 0, 900, 30);
		area.setBounds(0, 30, 1000, 470);
		//add the data input the MyFrame
		add(save);
		add(text);
		add(area);
		//set the listener
		Savelistener save_listener = new Savelistener();
		//input the listener into the button
		save.addActionListener(save_listener);
	}

	public class Savelistener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFrame window = new JFrame("�T��");
			String line = new String();
			try {
				if (text.getText() == null) {
					//exception appear
					throw new Exception();
				} else {
					line = "File " + text.getText() + ".txt saved!";
					File file = new File(text.getText());//open the file
					PrintWriter output = new PrintWriter(file);//use the PrintWriter class to write into the file
					output.print(area.getText());//write into the file 
					output.close();
				}
			} catch (Exception g) {
				line = "File name can't be empty!";
			}
			//new a JFrame with a button and some string
			JOptionPane.showMessageDialog(window, line);

		}
	}
}
