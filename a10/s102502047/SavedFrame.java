package ce1002.a10.s102502047;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class SavedFrame extends JFrame{//小方框
	
	SavedFrame(String title){//跳出時顯示所給的信息
		setBounds(150, 150,200,100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		JLabel label = new JLabel(title);
		label.setBounds(20, 10, 180, 40);
		add(label);
		}

}
