package ce1002.a10.s102502050;

import java.awt.event.*;
import javax.swing.*;


public class MyFrame extends JFrame implements ActionListener 
{
	private JPanel panel = new JPanel();
	private JTextField field  = new JTextField(null);
	private JTextArea area = new JTextArea(50,50);
	private JButton button = new JButton("Save");
	
	
	public MyFrame()  throws Exception
	{
		super("A10-s102502050");
		setLayout(null);
		panel.setLayout(null);//要先setLayout，setBounds才有用
		this.setBounds(200,100,500,500);				
		
		area.setLineWrap(true);
		
		
		panel.setBounds(0,0,500,500);
		button.setBounds(0,50,100,25);
		field.setBounds(100,50,400,25);
		area.setBounds(50,75,350,400);//設定大小
		
		panel.add(button);
		panel.add(field);
		panel.add(area);
		panel.setVisible(true);
		
		this.add(panel);
		this.setVisible(true);		
		
				
		button.addActionListener(this);//button被按的listener
	}
	public void actionPerformed(ActionEvent e) 
	{
		String name,text;
		name = field.getText();
		
		if(field.getText().trim().equals(""))//textfield 為空
		{
			JFrame frame = new JFrame();
			JPanel panel2 = new JPanel();
			panel2.add(new JLabel("File name can't be empty!"));
			JOptionPane.showMessageDialog(frame, panel2);

		}
		else
		{
			text = area.getText();
			java.io.File file = new java.io.File(name + ".txt");
		
				try
				{
				java.io.PrintWriter output = new java.io.PrintWriter(file);		
				output.print(text);
				output.close();
				}
				catch (Exception e2)
				{
					
				}
				//建一個檔案丟什麼exception，這段又讓我卡30分鐘
				
				
			
				

			
			
			
			JFrame frame = new JFrame();
			JPanel panel2 = new JPanel();
			panel2.add(new JLabel("File " + name + ".txt saved!"));
			JOptionPane.showMessageDialog(frame, panel2);
				
		}
	}
}


	