package ce1002.a10.s102502508;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;

public class MyPanel extends JPanel implements ActionListener {

	JTextField txt = new JTextField(30);// 新增一個文字框
	JButton jbt = new JButton();// 新增一個按鍵
	JTextArea txtregion = new JTextArea(30, 39);// 新增一個對話框

	public MyPanel() {

		txt.selectAll();
		jbt.setText("Save");
		txtregion.setLineWrap(true);// 使對話框能自動換行
		// 將所創的案件和標籤都丟到畫板上
		add(jbt);
		add(txt);
		add(txtregion);
		jbt.addActionListener(this);// 讓使用者能在按鍵後得到下一步的操作

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

		if ("".equals(txt.getText())) { // 建造一個視窗提醒使用者文字框內不能空白
			JLabel jbl = new JLabel();
			jbl.setText("File name can’t be empty!");
			JButton jbtok = new JButton("確定");
			JPanel jpl = new JPanel();
			jpl.add(jbl);
			jpl.add(jbtok);
			final JFrame jf = new JFrame("訊息");
			jf.setSize(210, 120);
			jf.setLocationRelativeTo(null);
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jf.setVisible(true);
			jf.add(jpl);

			jbtok.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jf.dispose();// 使視窗能被關掉
				}
			});
		} else {
			try {
				// 讓使用者所輸入的檔名和文字內容加以輸出成一個文字檔
				File file = new File(txt.getText() + ".txt");
				BufferedWriter output = new BufferedWriter(new FileWriter(file));
				output.write(txtregion.getText());
				output.close();
			} catch (IOException e) {
				e.printStackTrace();// 追蹤錯誤的位子與原因
			}
			// 提醒使用者他所輸入的資料已被加以存取並存取成功
			JButton jbtyes = new JButton("確定");
			JLabel label = new JLabel();
			label.setText(txt.getText() + ".txt is saved!");
			JPanel jpl = new JPanel();
			JPanel jpl2 = new JPanel();
			jpl.add(label);
			jpl2.add(jbtyes);
			final JFrame jf = new JFrame("訊息");
			jf.setSize(210, 120);
			jf.setLocationRelativeTo(null);
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			jf.setVisible(true);
			jf.add(jpl, BorderLayout.NORTH);
			jf.add(jpl2, BorderLayout.SOUTH);

			jbtyes.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					jf.dispose();
				}
			});

		}

	}

}
