package ce1002.e8.s100203020;
import javax.swing.*;

import java.awt.*;

public class Paint extends JPanel{
	private int type;
	Paint(int type){
		this.type = type;
	};
	
	protected void paintComponent(Graphics g){
		int center=150;
		if(type==1)//sun cake
		{	
			super.paintComponent(g);
			int d=160; //d is 2*radius of circle
			g.setColor(Color.orange);
			g.fillOval(center-d/2, center-d/2, d, d);
			
			g.setColor(Color.red);
			g.drawString("太", 145, 125);
			g.drawString("陽", 145, 155);
			g.drawString("餅", 145, 185);
			g.drawRect(140, 110, 20, 90);
		}
		else if(type==2)//sun flower
		{	
			super.paintComponent(g);
			int d=120; //d is 2*radius of circle
			g.setColor(Color.yellow);

			//set polygon
			Polygon flowerPetal = new Polygon();
			int pointx[]={0,(d/8),0,-d/8};
			int pointy[]={0,(4*d/5),d,4*d/5};
			for(int i =0;i<16; i++){
				for(int j=0;j<4 ;j++){
					int x,y;
					x=(int)((pointx[j])*Math.cos(Math.PI*i/8)+(pointy[j])*Math.sin(Math.PI*i/8));
					y=(int)((pointy[j])*Math.cos(Math.PI*i/8)-(pointx[j])*Math.sin(Math.PI*i/8));
					flowerPetal.addPoint(x+center,y+center);
					
				}
			}
			
			g.fillPolygon(flowerPetal);
			g.setColor(Color.black);
			g.drawPolygon(flowerPetal);
			
			g.setColor(Color.getHSBColor(30/360f, 1.f, 0.6f));//color brown
			g.fillOval(center-d/2, center-d/2, d, d); 
			g.setColor(Color.BLACK);
			g.drawOval(center-d/2, center-d/2, d, d);

			for(int i=0;i<4 ;i++ ){
				for(int j=-1;j<2;j+=2){
					g.drawLine(center+d*i*j/8, center-(int)Math.sqrt(Math.abs(Math.pow(d/2, 2)-Math.pow(d*i/8,2))), center+d*i*j/8, center+(int)Math.sqrt(Math.abs(Math.pow(d/2, 2)-Math.pow(d*i/8,2))));
					g.drawLine(center-(int)Math.sqrt(Math.abs(Math.pow(d/2, 2)-Math.pow(d*i/8,2))), center+d*i*j/8, center+(int)Math.sqrt(Math.abs(Math.pow(d/2, 2)-Math.pow(d*i/8,2))), center+d*i*j/8);
				}
			}
			

		}
		else
		{	
			super.paintComponent(g);
			g.setColor(Color.BLACK);
			g.fillRoundRect(0,0,300,300,60,60);
			g.setColor(Color.RED);
			g.drawString("完　　　全", 60, 80);
			g.drawString("沒　　　有", 120, 160);
			g.drawString("畫　　　面", 180, 240);

		}
	}
	
}
