package ce1002.e8.s100203020;
import javax.swing.*;

public class MyFrame extends JFrame{
	protected MyPanel panel1= new MyPanel(1);
	protected MyPanel panel2= new MyPanel(2);
	protected MyPanel panel3= new MyPanel(3);
	
	MyFrame(){
		this.setLayout(null);
		//set property of my frame 
		int width =320;
		int height = 400;
		int away = 10;
		this.setBounds(300,150,width*3+away*6,20+height+4*away);
		this.setVisible(true);
		this.setTitle("E8");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		panel1.setBounds(away,away,width,height);
		panel2.setBounds(away+(width+away),away,width,height);
		panel3.setBounds(away+2*(width+away),away,width,height);
		
		
		//set Text in panels
		panel1.setProperty("Graph: Suncake","Info: Popular Taiwanese dessert.");

		panel2.setProperty("Graph: Banana","Info: This is not sunflower in taiwan.");

		panel3.setProperty("Graph: Nothing","Info: You could see nothing.");
		
		//add panels
		add(panel1);
		add(panel2);
		add(panel3);
		
		
	}
}
