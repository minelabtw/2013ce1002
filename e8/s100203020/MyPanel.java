package ce1002.e8.s100203020;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;


public class MyPanel extends JPanel{
	protected int i;//i is for Paint type
	protected Paint paint;//graphic
	protected JLabel name=new JLabel();
	protected JLabel info=new JLabel();
	
	
	MyPanel(int i){
		setLayout(null);
		setblackborder();
		this.i= i;
		paint=new Paint(i);
		this.paint.setBounds(10,10,300,300);
		this.name.setBounds(10,310,300,30);
		this.info.setBounds(10,330,300,60);

		add(paint);
		add(name);
		add(info);
		}
	
	/**setblackborder */
	private void setblackborder(){
		Border blackline;               
		blackline = BorderFactory.createLineBorder(Color.BLACK,3);
		this.setBorder(blackline);
	
	}
	/**set name and info */

	void setProperty(String names,String infos){
		this.name.setText(names);
		this.info.setText(infos);
	}
	
	
}
