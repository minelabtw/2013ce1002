package ce1002.e8.s102502548;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	MyPanel(){
		setLayout(null) ;
		
		JLabel[] label=new JLabel[6] ;
		
		setBounds(10,10,700,400) ;
		
		setBorder(new LineBorder(Color.BLACK,3)) ;
		
		for (int i=1;i<6;i=i+2){
			label[i]=new JLabel() ;
			
			label[i].setBounds(10+100*i,350,200,50) ;
			
			add(label[i]) ;
		}
		
		
		for (int i=0;i<6;i=i+2){
			label[i]=new JLabel() ;
			
			label[i].setBounds(0+150*i,250,200,50) ;
			
			add(label[i]) ;
		}
		
		label[0].setText("Graph: Rectangle") ;
		label[1].setText("Info: cute rectangle") ;
		label[2].setText("Graph: Oval") ;
		label[3].setText("Info: cute oval") ;
		label[4].setText("Graph: windmill") ;
		label[5].setText("Info: handsome windmill") ;
	}
	
	@Override
	public void paintComponent(Graphics g){
		g.setColor(Color.BLUE) ;
		
		g.fillRect(5,5,230,180) ;
		
		g.setColor(Color.green) ;
		
		g.fillOval(250, 5, 200, 180) ;
		
		g.setColor(Color.GRAY) ;
		
		for (int x=0;x<4;x++){
			g.fillArc(475, 5, 200, 200,0+x*90 ,30 ) ;
		}
	}
}
