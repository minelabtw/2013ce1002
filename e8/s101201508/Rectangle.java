package ce1002.e8.s101201508;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends Rect{
	Rectangle()
	{
		name="Rectangle";
		info="Rectangle";
		Random_color c=new Random_color();
		color=c.get_color(1);
		width=100;
		height=50;
	}
	Rectangle(boolean f, Color c, int w,int h)
	{
		name="Rectangle";
		info="Rectangle";
		fill=f;
		color=c;
		width=w;
		height=h;
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if (fill)
		{
			//g.setColor(color);
			g.fillRect(0,0,width,height);
		}
		else
		{
			g.drawRect(0,0,width,height);
		}
		repaint();
	}
}
