package ce1002.e8.s101201508;

import java.awt.Color;

import javax.swing.JPanel;

public class Graph extends JPanel{
	protected String name;
	protected String info;
	protected boolean fill=true;
	protected Color color=Color.BLACK;
	Graph()
	{
		
	}
	void set_name(String n)
	{
		name=n;
	}
	String get_name()
	{
		return name;
	}
	void set_info(String i)
	{
		info=i;
	}
	String get_info()
	{
		return info;
	}
	void set_fill(boolean f)
	{
		fill=f;
	}
	boolean get_fill()
	{
		return fill;
	}
	void set_color(Color c)
	{
		color=c;
	}
	Color get_color()
	{
		return color;
	}
}
