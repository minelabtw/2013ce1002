package ce1002.e8.s101201508;

import java.awt.Color;
import java.awt.Graphics;

public class Oval extends Rect{
	Oval()
	{
		name="Oval";
		info="Oval";
		Random_color c=new Random_color();
		color=c.get_color(1);
		width=100;
		height=50;
	}
	Oval(boolean f, Color c, int w, int h)
	{
		name="Oval";
		info="Oval";
		fill=f;
		color=c;
		width=w;
		height=h;
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if (fill)
		{
			g.setColor(color);
			g.fillOval(0,0,width,height);
		}
		else
		{
			g.drawOval(0,0,width,height);
		}
	}
}
