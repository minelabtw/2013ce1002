package ce1002.e8.s101201508;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	protected int i=0;
	protected JLabel name=new JLabel();
	protected JLabel info=new JLabel();
	protected Graph graph_panel;
	MyPanel(int i)
	{
		this.i=i;
		show_graph(i);
		setBorder(new LineBorder(Color.BLACK,2));
	}
	void show_graph(int i)
	{
		if (i==0)
		{
			graph_panel=new Rectangle();
		}
		else if (i==1)
		{
			graph_panel=new RoundRect();
		}
		else if (i==2)
		{
			graph_panel=new Oval();
		}
		else if (i>=3 && i<=7)
		{
			int x[]=new int [i];
			int y[]=new int [i];
			int r=50;
			for (int j=0;j<i;j++)
			{
				x[j]=r-r*(int)Math.cos(j*Math.PI/i);
				y[j]=r+r*(int)Math.sin(j*Math.PI/i);
			}
			Random_color c=new Random_color();
			graph_panel=new Line(true,c.get_color(1),x,y,i);
		}
		else
		{
			
		}
		setLayout(null);
		name.setText("Graph:"+graph_panel.get_name());
		info.setText("Info:"+graph_panel.get_info());
		graph_panel.setBounds(10-50*graph_panel.getWidth(),30-50*graph_panel.getHeight(),100,100);
		name.setBounds(20,140,100,20);
		info.setBounds(20,160,100,20);
		add(graph_panel);
		add(name);
		add(info);
		
	}
}
