package ce1002.e8.s101201508;

import java.awt.Color;
import java.awt.Graphics;

public class RoundRect extends Rect{
	protected int aw=10;
	protected int ah=10;
	RoundRect()
	{
		name="Rounded Rectangle";
		info="Rounded Rectangle";
		Random_color c=new Random_color();
		color=c.get_color(1);
		width=100;
		height=50;
		aw=width/2;
		ah=height/2;
	}
	RoundRect(boolean f, Color c, int w,int h, int aw, int ah)
	{
		name="Rectangle";
		info="Rounded Rectangle";
		fill=f;
		color=c;
		width=w;
		height=h;
		this.aw=aw;
		this.ah=ah;
	}
	void set_aw(int w)
	{
		aw=w;
	}
	int get_aw()
	{
		return aw;
	}
	void set_ah(int h)
	{
		ah=h;
	}
	int get_ah()
	{
		return ah;
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if (fill)
		{
			g.setColor(color);
			g.fillRoundRect(0,0,width,height,aw,ah);
		}
		else
		{
			g.drawRoundRect(0,0,width,height,aw,ah);
		}
		repaint();
	}
}
