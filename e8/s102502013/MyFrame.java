package ce1002.e8.s102502013;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
		
	MyFrame(){
		Rectangle panel1 = new Rectangle();
		Oval panel2 = new Oval();
		panel1.setBounds(5, 5, 345, 250);
		panel2.setBounds(355, 5, 345, 250);
		panel1.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		panel2.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		add(panel1);
		add(panel2);
	}
	
}
