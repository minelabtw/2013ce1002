package ce1002.e8.s102502013;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
public class Oval extends JPanel{
	Oval(){
		setLayout(null);
		
	}
	protected void paintComponent(Graphics g ){
		g.setColor(Color.red);
		g.drawOval(80, 10, 200, 100);
		g.fillOval(80, 10, 200, 100);
		g.setColor(Color.black);
		g.drawString("Graph:Oval", 80, 150);
		g.drawString("Info:An oval (from Latin ovum,...", 80, 200);
	}
}
