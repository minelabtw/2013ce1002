package ce1002.e8.s102502013;

import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		MyFrame myFrame = new MyFrame();
		myFrame.setLayout(null);
		myFrame.setTitle("102502013");
		myFrame.setSize(715,300);
		myFrame.setLocationRelativeTo(null);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrame.setVisible(true);
		myFrame.setResizable(false);
	}

}
