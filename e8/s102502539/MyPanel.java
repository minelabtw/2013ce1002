package ce1002.e8.s102502539;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel
{
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	private int mode = 0;
	
	MyPanel( int mode )
	{
		this.mode = mode;
		setSize(200,200);
		setLayout(null);
		add(label1);
		add(label2);
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		// �Ϯ� + �W�� + �T��
		if ( mode == 1)
		{
			setBounds(20,20,200,200);
			setBorder(BorderFactory.createLineBorder(Color.black, 3));
			g.setColor(Color.blue);
			g.fillRect(20, 20, 150, 80);
			label1.setBounds(20,120,220,20);
			label1.setText("Graph: Rectangle");
			label2.setBounds(20,140,220,20);
			label2.setText("Info: In Eublidean plane...");
		}
		else if ( mode == 2 )
		{
			setBounds(250,20,200,200);
			setBorder(BorderFactory.createLineBorder(Color.black, 3));
			g.setColor(Color.green);
			g.fillOval(20, 20, 160, 80);
			label1.setBounds(20,120,220,20);
			label1.setText("Graph: Oval");
			label2.setBounds(20,140,220,20);
			label2.setText("Info: An oval from latin ovum...");
		}
	}
	
}
