package ce1002.e8.s102502539;

import javax.swing.*;

public class MyFrame extends JFrame
{
	protected MyPanel panel1 = new MyPanel(1);
	protected MyPanel panel2 = new MyPanel(2);
	
	MyFrame()
	{
		setLayout(null);
		setSize(490, 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel1);
		add(panel2);
	}
}
