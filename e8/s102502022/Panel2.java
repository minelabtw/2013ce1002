package ce1002.e8.s102502022;

import java.awt.*;
import javax.swing.*;

public class Panel2 extends JPanel{
	
	Panel2(){
		
		setBounds(320,10,300,200);
		setLayout(null);
	}
	
	public void paintComponent(Graphics graph){
		
		graph.setColor(Color.YELLOW);
		graph.fillOval(25,15,250,130);
		JLabel jlabel3 = new JLabel();
		jlabel3.setBounds(15,160,220,20);
		jlabel3.setText("Graph: Oval");
		add(jlabel3);
		JLabel jlabel4 = new JLabel();
		jlabel4.setBounds(15,180,270,20);
		jlabel4.setText("Info: An oval(From Latin ovun,...");
		add(jlabel4);
	}

	

}
