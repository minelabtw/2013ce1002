package ce1002.e8.s102502022;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame(){
		
		setSize(700,300);//設定框架大小
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(null);
		construct();//呼叫下面的函示
	}
	
	public void construct(){
		
		Panel1 panel1 = new Panel1();
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));//框線
		add(panel1);//加入panel
		Panel2 panel2 = new Panel2();
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel2);
	}
	
	

	

}
