package ce1002.e8.s102502007;
import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame {
	MyFrame()
	{
		setLayout(null);
		setSize(980,300);
		setVisible(true);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		MyPanel1 MyPanel1 = new MyPanel1();
		MyPanel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(MyPanel1);
		MyPanel2 MyPanel2 = new MyPanel2();
		MyPanel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(MyPanel2);
		MyPanel3 MyPanel3 = new MyPanel3();
		MyPanel3.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(MyPanel3);
	}
}
