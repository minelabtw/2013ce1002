package ce1002.e8.s102502007;
import javax.swing.*;

import java.awt.*;
public class MyPanel3 extends JPanel {
	private static int[] x = {};
	MyPanel3()
	{
		setBounds(640,20,300,200);
		setLayout(null);
		JLabel label1 = new JLabel("Graph: Polygon");
		label1.setBounds(20,115,220,100);
		add(label1);
		JLabel label2 = new JLabel("Info: In geometry a polygon is t...");
		label2.setBounds(20,175,220,20);
		add(label2);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		Graphics2D graph = (Graphics2D)g;
		int[] x = {110,210,260,210,110,60};
		int[] y = {10,10,86,172,172,86};
		graph.fill(new Polygon(x,y,x.length));
	}

}
