package ce1002.e8.s102502541;
import javax.swing.*;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.border.LineBorder;
public class panel extends JPanel{
	JLabel name = new JLabel("Graph:3D rectangel");
	JLabel state = new JLabel("info: a raised rectangel");
	public panel()
	{
		setLayout(null);
		setBounds(5,5,150,200);
		setBorder(new LineBorder(Color.black,3));//畫邊線
		name.setBounds(5,140,150,20);
		state.setBounds(5,170,150,20);
		add(name);
		add(state);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.pink);
		g.fill3DRect(15, 20, 100, 100, true);
	}
}