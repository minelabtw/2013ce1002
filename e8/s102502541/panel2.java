package ce1002.e8.s102502541;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.border.LineBorder;
public class panel2 extends JPanel{
	JLabel name = new JLabel("Graph: bear");
	JLabel state = new JLabel("info: 6 oval");
	public panel2()
	{
		setLayout(null);
		setBounds(320,5,150,200);
		setBorder(new LineBorder(Color.black,3));//畫邊線
		name.setBounds(5,140,150,20);
		state.setBounds(5,170,150,20);
		add(name);
		add(state);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setColor(Color.ORANGE);
		g.fillOval(10, 10, 130, 130);
		g.setColor(Color.BLACK);
		g.fillOval(55, 55, 40, 40);
		g.setColor(Color.orange);
		g.fillOval(20, 5, 40, 40);
		g.fillOval(90, 5, 40, 40);
		g.setColor(Color.BLACK);
		g.fillOval(50, 30, 10, 10);
		g.fillOval(90, 30, 10, 10);
	}
}
