package ce1002.e8.s102502541;
import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.border.LineBorder;
public class panel1 extends JPanel{
	JLabel name = new JLabel("Graph: Windmill");
	JLabel state = new JLabel("info: 4 arc and 1 oval");
	public panel1()
	{
		setLayout(null);
		setBounds(160,5,150,200);
		setBorder(new LineBorder(Color.black,3));//畫邊線
		name.setBounds(5,140,150,20);
		state.setBounds(5,170,150,20);
		add(name);
		add(state);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setColor(Color.BLACK);
		g.fillArc(20,10,100,100,0,30);
		g.fillArc(20,10,100,100,90,30);
		g.fillArc(20,10,100,100,180,30);
		g.fillArc(20,10,100,100,270,30);
		g.setColor(Color.RED);
		g.fillOval(60, 50, 20, 20);
	}
}
