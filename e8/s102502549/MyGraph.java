package ce1002.e8.s102502549;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyGraph extends JPanel {

	// 三個可使用常數與一個用來存形狀的變數
	public final static int Rectangle = 1;
	public final static int Oval = 2;
	public final static int Polygon = 3;
	private int type;

	// 有形狀的建構式
	public MyGraph(int type) {
		setBorder(new LineBorder(Color.BLACK, 3));
		this.type = type;
	}

	// 覆寫paintComponent來幫我畫圖
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		switch (type) {
		// case1是Rectangle
		case 1:
			g.setColor(Color.BLUE);
			g.fillRect(20, 10, 120, 50);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Rectangle", 10, 80);
			g.drawString("Info: This is a rectangle!", 10, 100);
			break;

		// case2是Oval
		case 2:
			g.setColor(Color.yellow);
			g.fillOval(20, 10, 120, 50);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Oval", 10, 80);
			g.drawString("Info: This is an Oval!", 10, 100);
			break;

		// case3是Polygon
		case 3:
			int[] x = new int[6];
			int[] y = new int[6];

			for (int i = 0; i < 6; i++) {
				x[i] = (int) (80 + 40 * Math.cos(i * 2 * Math.PI / 6));
				y[i] = (int) (40 + 30 * Math.sin(i * 2 * Math.PI / 6));
			}

			g.setColor(Color.GREEN);
			g.fillPolygon(x, y, 6);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Polygon", 10, 80);
			g.drawString("Info: This is a Polygon!", 10, 100);
			break;
		}
	}
}
