package ce1002.e8.s102502549;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		//建立frame並將layout設為gridlayout
		JFrame frame=new JFrame();
		frame.setLayout(new GridLayout(1, 3, 10, 10));
		//建立三個圖形panel，並加入frame
		MyGraph a=new MyGraph(MyGraph.Rectangle);
		MyGraph b=new MyGraph(MyGraph.Oval);
		MyGraph c=new MyGraph(MyGraph.Polygon);
		frame.add(a);
		frame.add(b);
		frame.add(c);
		//frame的一些雜七雜八設定
		frame.setSize(500, 150);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
