package ce1002.e8.s995002046;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
@SuppressWarnings("serial")
class PolygonPanel extends JPanel{
	PolygonPanel(){
		this.setVisible(true);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));//set border
	}
	 @Override public void paintComponent(Graphics g) {//draw graphics and info
		 int[] xPoints={15*5, 25*5, 32*5, 37*5, 45*5, 27*5, 10*5};
		 int[] yPoints={15*5, 10*5, 12*5, 22*5, 25*5, 37*5, 30*5};
		 g.setColor(Color.green);
		 g.fillPolygon(xPoints, yPoints, 7);
		 g.setColor(Color.black);
		 g.drawString("Graphics: Polygon", 20, 260);
		 g.drawString("info: 456789123", 20, 280);
	}
	
}