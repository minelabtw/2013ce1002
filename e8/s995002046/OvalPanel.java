package ce1002.e8.s995002046;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
@SuppressWarnings("serial")
class OvalPanel extends JPanel{
	OvalPanel(){
		this.setVisible(true);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));//set border
	}
	 @Override public void paintComponent(Graphics g) {//draw graphics and info
		 g.fillOval(15, 15, 250, 200);
		 g.setColor(Color.black);
		 g.drawString("Graphics: Oval", 20, 260);
		 g.drawString("info: 987654321", 20, 280);
	}
	
}