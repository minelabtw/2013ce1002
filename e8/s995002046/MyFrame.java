package ce1002.e8.s995002046;
import javax.swing.JFrame;

@SuppressWarnings("serial")
class MyFrame extends JFrame{
	RectPanel rPanel;//rectangle panel
	OvalPanel oPanel;//oval panel
	PolygonPanel pPanel;//polygon panel
	MyFrame(){
		this.setLayout(null);
		init();//initialize some panel and add them in
		this.setSize(1000,400); //set size
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	private void init() {//add panel and set their bound
		// TODO Auto-generated method stub
		rPanel=new RectPanel();
		this.add(rPanel);
		rPanel.setBounds(10, 10, 300, 300);
		oPanel=new OvalPanel();
		this.add(oPanel);
		oPanel.setBounds(320, 10, 300, 300);
		pPanel=new PolygonPanel();
		this.add(pPanel);
		pPanel.setBounds(630, 10, 300, 300);
	}
}