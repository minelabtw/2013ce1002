package ce1002.e8.s102502043;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
public class Polygon extends JPanel
{
	protected JLabel label = new JLabel();
	protected JLabel labeli = new JLabel();
	Polygon()
	{
		setLayout(null);
		setBounds(500,0,500,400);
		label.setText("Graph: Rectangle");//label加文字
		labeli.setText("Info: In Euclidean plane geome...");
		label.setBounds(100,300,350,40);
		labeli.setBounds(100,350,350,40);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(label);
		add(labeli);
		
	}
	public void paintComponent(Graphics g)//話六邊形 
	{
        int x[] = {100,350,400,350,100,50};
        int y[] = {50,50,100,150,150,100};
        g.setColor(Color.YELLOW);
        g.fillPolygon(x,y,6);
    }   
 
}
