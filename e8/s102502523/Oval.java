package ce1002.e8.s102502523;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Oval extends JPanel {

	protected JLabel graph = new JLabel("Graph: Oval");
	protected JLabel info = new JLabel("Info: An oval is a closed curve in a plane which 'loosely' resembles the outline of an egg.");

	Oval() {

		/*set panel's size and layout*/
		setLayout(null);
		graph.setBounds(10,120,220,30);
		info.setBounds(10,160,240,30);
		/*add label to panel*/
		add(graph);
		add(info);

	}

	protected void paintComponent(Graphics o) {
		// draw a oval which is filled with red color
		super.paintComponent(o);
		o.setColor(Color.RED);
		o.fillOval(50,20,160,100);

	}

}
