package ce1002.e8.s102502523;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Polygon extends JPanel {

	protected JLabel graph = new JLabel("Graph: Polygon");
	protected JLabel info = new JLabel("Info: In geometry a polygon is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit.");

	Polygon() {

		/*set panel's size and layout*/
		setLayout(null);
		graph.setBounds(10,130,230,30);
		info.setBounds(10,150,230,30);
		/*add label to panel*/
		add(graph);
		add(info);

	}

	protected void paintComponent(Graphics p) {
		// draw a polygon which is filled with green color
		super.paintComponent(p);
		int x [] = {95,155,185,155,95,65};
	    int y [] = {20,20,70,120,120,70};
	    p.setColor(Color.GREEN);
		p.fillPolygon(x,y,6);
	}

}
