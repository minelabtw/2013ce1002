package ce1002.e8.s101602016;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	protected JLabel label1 = new JLabel();//標題
	protected JLabel label2 = new JLabel();//註解
	protected Rectangle rect = new Rectangle();//長方形
	protected Oval oval = new Oval();//橢圓形
	MyPanel(int a){
		if(a==1){//如果變數等於1,畫橢圓
			oval.setBounds(5,5,300,200);//設定位置,大小
			label1.setText("Graph: Oval");//標題
			label2.setText("Info: an oval");//註解
			add(oval);//加入橢圓
		}
		else{//以外的情況,畫長方形
			rect.setBounds(5,5,300,200);//設定位置,大小
			label1.setText("Graph: Rectangle");//標題
			label2.setText("Info: a rectangle");//註解
			add(rect);//加入長方形
		}
		setLayout(null);
		label1.setBounds(5,210,300,10);//標題位置,大小
		add(label1);//加入標題
		label2.setBounds(5,230,300,10);//註解位置,大小
		add(label2);//加入註解
		setBorder(new LineBorder(Color.BLACK,2));//加入邊框
	}
	
}
