package ce1002.e8.s101602016;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	protected MyPanel panel1 = new MyPanel(1);//畫橢圓所需
	protected MyPanel panel2 = new MyPanel(2);//畫長方形所需
	MyFrame(){
	setSize(700,300);//設定是眶大小
	setLocationRelativeTo(null);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setVisible(true);
	setLayout(null);
	panel1.setBounds(5,5,325,250);//第一個板子位置,大小
	panel2.setBounds(355,5,325,250);//第二個板子位置,大小
	add(panel1);//加入第一個板子
	add(panel2);//加入第二個板子
	}
}
