package ce1002.e8.s102502519;

import javax.swing.*;

import java.awt.*;


public class Frame extends JFrame{
	
	Frame(){
		setLayout(null);
		setSize(640,260);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		Panel1 panel1 = new Panel1();
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel1);
		Panel2 panel2 = new Panel2();
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel2);
	}
	
}
