package ce1002.e8.s102502519;

import javax.swing.*;

import java.awt.*;

public class Panel2 extends JPanel{
	Panel2()
	{
		setBounds(320,10,300,200);
		setLayout(null);
	}
	public void paintComponent(Graphics g)
	{   
		g.setColor(Color.YELLOW);
		g.fillOval(23,20,250,130);
		JLabel jlabel1=new JLabel();
		jlabel1.setBounds(10,140,220,50);
		jlabel1.setText("Graph: Oval");
		add(jlabel1);
		JLabel jlabel2=new JLabel();
		jlabel2.setBounds(10,170,220,20);
		jlabel2.setText("Info: An oval(From Latin ovun,...");
		add(jlabel2);
	}
}
