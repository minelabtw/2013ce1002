package ce1002.e8.s102502556;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Polygon extends JPanel {
	
	JLabel kind = new JLabel(); //儲存形狀名稱
	JLabel info = new JLabel(); //儲存形狀資訊
	int radius = 50; //多邊形半徑
	int xcenter = 100; //多邊形中心的x座標
	int ycenter = 80; //多邊形中心的y座標
	//多邊形頂點的x座標陣列
	int x[] = {xcenter + radius, (int)(xcenter + radius * Math.cos(2 * Math.PI / 6)), 
			(int)(xcenter + radius * Math.cos(2 * 2 * Math.PI / 6)),
			(int)(xcenter + radius * Math.cos(3 * 2 * Math.PI / 6)),
			(int)(xcenter + radius * Math.cos(4 * 2 * Math.PI / 6)),
			(int)(xcenter + radius * Math.cos(5 * 2 * Math.PI / 6))};
	//多邊形頂點的y座標陣列
	int y[] = {ycenter, (int)(ycenter - radius * Math.sin(2 * Math.PI / 6)), 
			(int)(ycenter - radius * Math.sin(2 * 2 * Math.PI / 6)),
			(int)(ycenter - radius * Math.sin(3 * 2 * Math.PI / 6)),
			(int)(ycenter - radius * Math.sin(4 * 2 * Math.PI / 6)),
			(int)(ycenter - radius * Math.sin(5 * 2  * Math.PI / 6))};
	
	//constructor
	Polygon () {	
		setSize(200,200); //設定大小
		setLocation(470,10); //設定位置
		setLayout(null); //設定排版方式
		setBorder(new LineBorder(Color.black, 5)); //設定邊界屬性
		kind.setText("Graph:Polygon"); //設定kind內容
		kind.setSize(100,20); //設定kind大小
		kind.setLocation(10,150); //設定kind位置
		info.setText("Info:In geometry a polygon is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit."); //設定info內容
		info.setSize(100,20); //設定info大小
		info.setLocation(10,170); //設定info位置
		add(kind);
		add(info);
	}
	
	//繪圖函數
	protected void paintComponent( Graphics g )
	{
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		g.fillPolygon(x, y, x.length);
	}

}
