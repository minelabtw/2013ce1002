package ce1002.e8.s102502556;

import javax.swing.*;

public class MyFrame extends JFrame {
	
	Rectangle rect = new Rectangle(); //宣告一個名為rect的Rectangle型態的Object
	Oval oval = new Oval(); //宣告一個名為oval的Oval型態的Object
	Polygon poly = new Polygon(); //宣告一個名為poly的Polygon型態的Object
	
	MyFrame () {
		setSize(700,300); //設定視窗大小
		setLocationRelativeTo(null); //設定視窗預設位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //設定視窗關閉
		setLayout(null); //設定排版方式
		add(rect); 
		add(oval);
		add(poly);
		setVisible(true); //使視窗可顯示在螢幕上
	}

}
