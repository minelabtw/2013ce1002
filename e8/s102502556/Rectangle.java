package ce1002.e8.s102502556;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Rectangle extends JPanel {
	
	JLabel kind = new JLabel(); //儲存形狀名稱
	JLabel info = new JLabel(); //儲存形狀資訊
	
	//constructor
	Rectangle () {
		setSize(200,200); //設定大小
		setLocation(10,10); //設定位置
		setLayout(null); //設定排版方式
		setBorder(new LineBorder(Color.black, 5)); //設定邊界屬性
		kind.setText("Graph:Rectangle"); //設定kind內容
		kind.setSize(100,20); //設定kind大小
		kind.setLocation(10,150); //設定kind位置
		info.setText("Info:In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles."); //設定info內容
		info.setSize(100,20); //設定info大小
		info.setLocation(10,170); //設定info位置
		add(kind);
		add(info);
	}
	
	//繪圖函數
	protected void paintComponent( Graphics g )
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(getWidth() / 2 - 55, getHeight() / 2 - 40, getWidth() / 2 / 2 + 60, getHeight() / 2 / 2);
		
	}

}
