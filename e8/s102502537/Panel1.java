package ce1002.e8.s102502537;

import javax.swing.*;
import java.awt.*;

public class Panel1 extends JPanel{
	
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	
	
	Panel1()
	{
		setLayout(new BorderLayout(5,10));
		setBounds(0, 0, 250 , 200);
		add(label1,BorderLayout.NORTH);
		label2.setText("Rectangle");//the state of the graphic 
		add(label2,BorderLayout.SOUTH);
	}
	
	@Override 
    public void paintComponent (Graphics g){
		
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(20, 20, 200, 150);
    }//paint the graphic

}
