package ce1002.e8.s102502005;

import javax.swing.*;

import java.awt.*;
import java.text.BreakIterator;

public class MyPanel extends JPanel {

	String[] name = new String[3];
	String[] info = new String[3];

	MyPanel(int x, int _case) {

		//引用自Wikipedia。
		name[0] = "Graph: Rectangle";
		info[0] = "Info: In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.";
		name[1] = "Graph: Oval";
		info[1] = "An oval (from Latin ovum, \"egg\") is a closed curve in a plane which \"loosely\" resembles the outline of an egg.";
		name[2] = "Graph: Polygon";
		info[2] = "In geometry a polygon /ˈpɒlɪɡɒn/ is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit. ";

		this.setLayout(null);

		this.add(new _image(_case, 15, 15));

		JLabel nameLabel = new JLabel(name[_case]);
		nameLabel.setBounds(15, 150, 280, 15);
		JLabel infoLabel = new JLabel(info[_case]);
		infoLabel.setBounds(15, 165, 280, 15);

		this.add(nameLabel);
		this.add(infoLabel);

		this.setBounds(x, 10, 300 , 200);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));
	}
}

class _image extends JPanel {//畫出圖形。

	int _case;
	int[] xPoints = new int[6];
	int[] yPoints = new int[6];

	public _image(int _case, int x, int y) {
		this._case = _case;
		this.setBounds(x, y, 270, 130);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		switch (_case) {

		case 0:
			g.setColor(Color.BLUE);
			g.fillRect(0, 0, 270, 130);
			break;

		case 1:
			g.setColor(Color.YELLOW);
			g.fillOval(0, 0, 270, 130);
			break;

		case 2:
			Polygon p = new Polygon();//建立一個Polygon，最後再傳給fillPolygon。
			p.addPoint(97, 0);
			p.addPoint(173, 0);
			p.addPoint(211, 65);
			p.addPoint(173, 130);
			p.addPoint(97, 130);
			p.addPoint(59, 65);

			g.setColor(Color.GREEN);
			g.fillPolygon(p);

		default:
			break;
		}
	}
}
