package ce1002.e8.s102502005;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame {

	public MyFrame() {

		this.setLayout(null);

		for (int i = 0; i < 3; i++) {//利用for迴圈一一加入MyPanel，順便把要畫出的形狀傳給MyPanel。
			this.add(new MyPanel(i * (300 + 10) + 10, i));
		}

		this.setSize(960, 260);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

}