package ce1002.e8.s101201005;

import java.awt.*;
import javax.swing.*;

public class Oval extends JPanel{

	protected void paintComponent(Graphics g)
	{
	    super.paintComponent(g);
	    g.setColor(Color.YELLOW);
	    g.fillOval(10, 10, 200, 100);
	    g.setColor(Color.BLACK);
	    g.drawString("Graph: Oval", 20, 145);
	    g.drawString("Info: An oval (from Latin ovem,...", 20, 160);
	}
}
