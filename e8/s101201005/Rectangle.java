package ce1002.e8.s101201005;
import java.awt.*;
import javax.swing.*;
public class Rectangle extends JPanel{

	protected void paintComponent(Graphics g)
	{
	    super.paintComponent(g);
	    g.setColor(Color.BLUE);
	    g.fillRect(10, 10, 200, 100);
	    g.setColor(Color.BLACK);
	    g.drawString("Graph: Rectangle", 20, 145);
	    g.drawString("Info: In Euclidean plane geome...", 20, 160);
	    
	}
}
