package ce1002.e8.s101201005;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	protected Rectangle rectangle = new Rectangle();
	protected Oval oval=new Oval();
	MyFrame()
	{
		setLayout(null);
		rectangle.setBounds(10, 10, 220, 200);
		rectangle.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		oval.setBounds(250, 10, 220, 200);
		oval.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		setBounds(0 , 0 , 500 , 500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(rectangle);
		add(oval);
		
	}
}
