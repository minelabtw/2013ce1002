package ce1002.e8.s102502011;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class MyFrame extends JFrame {
	
	protected Oval oval= new Oval();
	protected Rectangle rect= new Rectangle();
	protected Polygon poly = new Polygon();
	
	MyFrame() {
		setLayout(null);
		setBounds(0 , 0 , 810 , 260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		rect.setBounds(10 , 10 , 250 , 200);
		/*set panel's border*/
		rect.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*add panel to frame*/
		add(rect);
		
		/*set panel's position and size*/
		oval.setBounds(270 , 10 , 250 , 200);
		/*set panel's border*/
		oval.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*add panel to frame*/
		add(oval);
		
		poly.setBounds(530 , 10 , 250 , 200);
		/*set panel's border*/
		poly.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*add panel to frame*/
		add(poly);
	}
	

}
