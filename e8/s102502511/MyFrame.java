package ce1002.e8.s102502511;

import javax.swing.JFrame;

public class MyFrame  extends JFrame{

public	MyFrame(){
		setLayout(null);
		setSize(1375,500); //frame大小
		setVisible(true); //顯示
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Rectange rec = new Rectange(); //呼叫3個panel
		Oval ova = new Oval();
		Polygon pol = new Polygon();
		add(rec); //3個panel依序加入
		add(ova);
		add(pol);
	}
}
