package ce1002.e8.s102502511;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Polygon extends JPanel {
	
	JLabel lab1 = new JLabel("Graph: Polygon");
	JLabel lab2 = new JLabel("Info: In geometry a polygon /ˈpɒlɪɡɒn/ is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit. These segments are called its edges or sides, and the points where two edges meet are the polygon's vertices (singular: vertex) or corners. The interior of the polygon is sometimes called its body. An n-gon is a polygon with n sides. A polygon is a 2-dimensional example of the more general polytope in any number of dimensions.");
	
	Polygon(){
		setLayout(null); //重製 
		setBounds(910,10,440,440); //邊框大小位置
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3)); //邊線
		
		lab1.setBounds(15,300,300,20); //lab位置
		lab2.setBounds(15,320,300,20);
		
		add(lab1);
		add(lab2);
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		int x[] = {165,265,315,265,165,115}; //從左上角的一點順時針畫 x y對照
		int y[] = {75,75,160,245,245,160};
		g.setColor(Color.GREEN); //顯示綠色
		g.fillPolygon(x,y,6); //多邊形 抓x y座標 取六個編
	}
}
