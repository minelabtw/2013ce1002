package ce1002.e8.s102502511;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Rectange  extends JPanel{
	JLabel lab1 = new JLabel("Graph: Rectangle");
	JLabel lab2 = new JLabel("Info: In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles. It can also be defined as an equiangular quadrilateral, since equiangular means that all of its angles are equal (360°/4 = 90°). It can also be defined as a parallelogram containing a right angle. A rectangle with four sides of equal length is a square. The term oblong is occasionally used to refer to a non-square rectangle.[1][2][3] A rectangle with vertices ABCD would be denoted as Rectanglen.PNG ABCD.");
	
	
	Rectange(){
		setLayout(null);
		setBounds(10,10,440,440);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));

		lab1.setBounds(15,300,300,20);
		lab2.setBounds(15,320,300,20);
		
		add(lab1);
		add(lab2);
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLUE); //藍色
		g.fillRect(70,50,300,200); //長方形大小 和 起始位置

	}
}
