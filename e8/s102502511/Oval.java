package ce1002.e8.s102502511;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Oval  extends JPanel{
	
	JLabel lab1 = new JLabel("Graph: Oval");
	JLabel lab2 = new JLabel("Info: An oval (from Latin ovum, egg) is a closed curve in a plane which loosely resembles the outline of an egg. The term is not very specific, but in some areas (projective geometry, technical drawing, etc.) it is given a more precise definition, which may include either one or two axes of symmetry. In common English, the term is used in a broader sense: any shape which reminds one of an egg.");
	
	Oval(){
		setLayout(null);
		setBounds(460,10,440,440);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		
		lab1.setBounds(15,300,300,20);
		lab2.setBounds(15,320,300,20);
		
		add(lab1);
		add(lab2);
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.YELLOW); //黃色
		g.fillOval(70,50,300,200); //橢圓形大小 和 起始位置

	}
}