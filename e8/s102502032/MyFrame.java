package ce1002.e8.s102502032;

import java.awt.Color;
import java.util.Random;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private MyPanel				panel[]				= new MyPanel[3];
	private Random				rnd					= new Random(
															System.currentTimeMillis());
	private String				name[]				= new String[3];
	private String				info[]				= new String[3];

	// constructor
	public MyFrame(MyPanel _panel[])
	{
		this.setLayout(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.panel = _panel;
		this.name[0] = new String("Graph: Arc");
		this.name[1] = new String("Graph: map(?)");
		this.name[2] = new String("Graph: Clock");
		this.info[0] = new String("An arc...");
		this.info[1] = new String("a lot of streets...");
		this.info[2] = new String("A clock");
		this.setSize(1100, 450);
		this.setVisible(true);
		for (int i = 0; i < 3; i ++)
		{
			panel[i] = new MyPanel(i, this.getColor(), name[i], info[i]);
			panel[i].setBounds(50 + 350 * i, 50, 300, 300);
			this.add(panel[i]);
		}
	}

	// get random color
	private Color getColor()
	{
		Color color;
		color = new Color(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
		return color;
	}
}
