package ce1002.e8.s102502032;

import java.awt.*;

import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private int					type				= 0;
	private Color				color;
	private Label				nameLable			= new Label();
	private Label				infoLable			= new Label();

	// constructor
	public MyPanel(int _type, Color _color, String _name, String _info)
	{
		this.setLayout(null);
		this.type = _type;
		this.color = _color;
		this.nameLable.setText(_name);
		this.nameLable.setBounds(30, 150, 240, 60);
		this.add(nameLable);
		this.infoLable.setText(_info);
		;
		this.infoLable.setBounds(30, 210, 240, 60);
		this.add(infoLable);
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		g.setColor(this.color);
		switch (type)
		{
		// draw many fans
			case 0:
				for (int i = 0; i < 12; i ++)
				{
					g.fillArc(this.getWidth() / 4, 0, this.getWidth() / 2,
							this.getHeight() / 2, 30 * i, 15);
				}
				break;
			// drow blocks
			case 1:
				for (int i = 0; i < this.getWidth(); i += 20)
				{
					for (int j = 0; j < this.getHeight() / 2; j += 20)
					{
						g.fillRect(i, j, 10, 10);
					}
				}
				break;
			// draw a clock
			case 2:
				Point center = new Point(this.getWidth() / 2,
						this.getHeight() / 4);
				int radius = Math.min(this.getHeight(), this.getWidth()) / 5;
				g.drawOval(center.x - radius, center.y - radius, radius * 2,
						radius * 2);
				g.drawString("12", center.x - 4, center.y - radius + 14);
				g.drawString("3", center.x + radius - 8, center.y + 6);
				g.drawString("6", center.x - 2, center.y + radius - 2);
				g.drawString("9", center.x - radius + 2, center.y + 6);
				g.setColor(Color.black);
				g.drawLine(
						center.x,
						center.y,
						(int) (center.x + radius * 0.8
								* Math.cos(180 - 6 * Calendar.SECOND)),
						(int) (center.y + radius * 0.8
								* Math.sin(180 - 6 * Calendar.SECOND)));
				g.setColor(Color.green);
				g.drawLine(
						center.x,
						center.y,
						(int) (center.x + radius * 0.65
								* Math.cos(180 - 6 * Calendar.MINUTE)),
						(int) (center.y + radius * 0.65
								* Math.sin(180 - 6 * Calendar.MINUTE)));
				g.setColor(Color.red);
				g.drawLine(
						center.x,
						center.y,
						(int) (center.x + radius * 0.5
								* Math.cos(180 - 6 * Calendar.HOUR)),
						(int) (center.y + radius * 0.5
								* Math.sin(180 - 6 * Calendar.HOUR)));
				break;
			default:
				break;
		}
	}
}
