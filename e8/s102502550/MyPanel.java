package ce1002.e8.s102502550;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	private JLabel j1,j2,j3,j4 = new JLabel();          //宣告標籤
	
	MyPanel(){
		setLayout(null);
		setBounds(20 , 0 , 485 , 400);
		
		
		
			j1 = new JLabel("Graph: WindMill");         //繪製標籤
			j1.setBounds(60 ,250 ,100 ,50 );
			add(j1);
			
			j2 = new JLabel("Graph: Rectangle");
			j2.setBounds(300 ,250 ,100 ,50 );
			add(j2);
		
		
		
			j3 = new JLabel("Info: It looks just like a windmill.");
			j3.setBounds(60 ,300 ,60 ,50 );
			add(j3);
			
			j4 = new JLabel("Info: A rectangle.");
			j4.setBounds(300 ,300 ,60 ,50 );
			add(j4);
		
		
		
	}
	
	protected void paintComponent(Graphics g){                     
		super.paintComponent(g);
		
		g.setColor(new Color(0,0,255));                    //繪製標籤
		g.fillArc(20, 20, 150, 150, 0, 30);
		g.fillArc(20, 20, 150, 150, 90, 30);

		g.fillArc(20, 20, 150, 150, 180, 30);
		g.fillArc(20, 20, 150, 150, 270, 30);
		g.setColor(new Color(255,0,0));

		
		g.setColor(Color.yellow);	
		g.fillRect(300, 20, 100, 200);
			
		
		
		
	}
}
