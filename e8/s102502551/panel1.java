package ce1002.e8.s102502551;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class panel1 extends JPanel {
	panel1(){
		setPreferredSize(new Dimension(500, 500));
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.fillRect(20,20,300,200);
		g.drawString("Graph: Rectangle",20,250);
		g.drawString("Info: it's a rectangle",20,270);
	}

}
