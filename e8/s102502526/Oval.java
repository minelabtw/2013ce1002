package ce1002.e8.s102502526;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Oval extends JPanel{
	protected JLabel graph = new JLabel("Graph: Oval");
	protected JLabel info = new JLabel("Info: 紅橢圓");
	
	Oval() {

		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		add(graph);
		add(info);
	}
	
	protected void paintComponent(Graphics o) {
		super.paintComponent(o);		
		o.setColor(Color.RED);
		o.fillOval(50,20,150,100);//oval的形狀是內建的
	}
	
}
