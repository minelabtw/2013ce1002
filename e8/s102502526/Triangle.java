package ce1002.e8.s102502526;
import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Triangle extends JPanel{
	protected JLabel graph = new JLabel("Graph: Triangle");
	protected JLabel info = new JLabel("Info: 藍(三)角");
	
	Triangle() {
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		add(graph);
		add(info);
	
	}
	
	protected void paintComponent(Graphics p) {
		super.paintComponent(p);
		int x [] = {65,185,125};
	    int y [] = {20,20,120};
	    p.setColor(Color.BLUE);		

		p.fillPolygon(x,y,3); //三角形不是內建的，所以用polygon去設定它的位置然後繪出
	}
}
