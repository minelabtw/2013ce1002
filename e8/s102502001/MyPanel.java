package ce1002.e8.s102502001;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	protected JLabel label = new JLabel();
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel text = new JLabel();
	protected JLabel text2 = new JLabel();
	protected JLabel text3 = new JLabel();
	
	MyPanel(){
		setLayout(null);
		label.setBounds(5,5,170,170);
		label.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		label2.setBounds(180,5,170,170);
		label2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		label3.setBounds(355,5,170,170);
		label3.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		text.setVerticalTextPosition(label.BOTTOM);
		text.setText("rectangle");
		add(label);
		add(label2);
		add(label3);
		add(text);
}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(15,15,100,100);
		g.setColor(Color.YELLOW);
		g.fillOval(195,15,100,100);
		g.setColor(Color.BLUE);
		g.fillRect(370,15,100,100);
	}
	
}