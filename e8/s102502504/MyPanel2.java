package ce1002.e8.s102502542;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel2 extends JPanel 
{
	MyPanel2()
	{
		setSize(250,180);
		setLocation(520,0);
		//setLayout(null);
		setBorder(new LineBorder(Color.BLACK,3));
	}
	public void paintComponent(Graphics g)
	{
		int xpoints[]={50,20,50,70,90,70};
		int ypoints[]={50,30,10,10,30,50};
		int npoints=6;
		g.setColor(Color.PINK);
		g.fillPolygon(xpoints,ypoints,npoints);
		setVisible(true);
	}
}
