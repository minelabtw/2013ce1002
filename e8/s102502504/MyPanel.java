package ce1002.e8.s102502542;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel 
{
	MyPanel()
	{
		setSize(250,180);
		setLocation(0,0);
		//setLayout(null);
		setBorder(new LineBorder(Color.BLACK,3));
	}
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.RED);
		g.fillRect(30,15,180,80);
		setVisible(true);
	}
}
