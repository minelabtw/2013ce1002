package ce1002.e8.s102502560;

import java.awt.FlowLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	public MyFrame() {
		setLayout(new FlowLayout());
		add(new MyPanel(0,"Block"));
		add(new MyPanel(1,"Circle"));
		add(new MyPanel(2,"Triangle"));
		add(new MyPanel(3,"Pie"));
		setSize(600, 600);
		setVisible(true);
	}
	
}
