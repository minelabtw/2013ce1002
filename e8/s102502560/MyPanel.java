package ce1002.e8.s102502560;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	int type;
	String text;
	
	public MyPanel(int type, String text) {
		this.type=type;
		this.text=text;
		setPreferredSize(new Dimension(200, 200));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.RED);
		if(type==0)g.fill3DRect(20, 20, 100, 100, true);
		else if(type==1)g.fillOval(20, 20, 100, 100);
		else if(type==2)g.fillPolygon(new int[]{20+50,20,20+100} , new int[]{20,(int) (20+Math.sqrt(3)*50),(int) (20+Math.sqrt(3)*50)}, 3);
		else if(type==3)g.fillArc(0, 0, 200, 200, 90, -60);
		g.setFont(new Font("Arial",Font.BOLD,20));
		g.setColor(Color.BLACK);
		g.drawString(text, 100, 150);
	}
}
