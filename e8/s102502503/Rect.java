package ce1002.e8.s102502503;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Rect extends JPanel{
	public Rect() {
		setBounds(5,5,240,250);  //邊界
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));  //邊框
	}
 
	
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.BLUE);  //設定形狀顏色
		g.fillRect(10, 10, 220, 120);  //畫矩形
		g.setColor(Color.BLACK);  //設定文字顏色
		g.drawString("Graph: Rectangle.", 10, 150);  //文字
        g.drawString("Info: a rectangle...", 10, 160);
		
	}
	 
}
