package ce1002.e8.s102502503;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Oval extends JPanel{
	public Oval() {
		setBounds(250,5,240,250);  //邊界
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));  //邊框
	}
 
	
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.RED);  //設定形狀顏色
		g.fillOval(10,10,220,120);  //畫橢圓
		g.setColor(Color.BLACK);  //設定文字顏色
		g.drawString("Graph: Oval.", 10, 150);  //文字
        g.drawString("Info: an oval...", 10, 160);
		
	}
	
}