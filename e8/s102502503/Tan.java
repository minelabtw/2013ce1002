package ce1002.e8.s102502503;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Tan extends JPanel{
	public Tan() {
		setBounds(495,5,240,250);  //邊界
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));  //邊框
	}
 
	
	protected void paintComponent(Graphics g) {
		int[] x={10,120,230};  //x的座標
		int[] y={120,10,120};  //y的座標
		super.paintComponent(g);
		g.setColor(Color.GREEN);  //設定形狀顏色
		g.fillPolygon(x, y, 3);  //畫多邊形
		g.setColor(Color.BLACK);  //設定文字顏色
		g.drawString("Graph: Tangle.", 10, 150);  //文字
        g.drawString("Info: a tangle...", 10, 160);
		
	}
	 
}
