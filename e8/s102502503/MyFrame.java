package ce1002.e8.s102502503;
import java.awt.Color;
import javax.swing.*;
public class MyFrame extends JFrame{
	public MyFrame() {
		
		setLayout(null);
		setBounds(100, 100, 755, 300);  //設定視窗大小
		setVisible(true);  //顯示視窗
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		Rect rect=new Rect();  //建立矩形物件
		add(rect);
		Oval oval=new Oval();  //建立橢圓物件
		add(oval);
		Tan tan=new Tan();  //建立三角形物件
		add(tan);
	}
	

}
