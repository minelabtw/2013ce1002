package ce1002.e8.s102502035;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel2 extends JPanel {
	public MyPanel2() {
		setLayout(null);
		addLabel();
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setVisible(true);
		setSize(270, 260);
	}

	public void addLabel() {// add Label
		JLabel label1 = new JLabel("GRAPH: RECTANGLE");
		label1.setLocation(30, 120);
		label1.setSize(170, 20);
		add(label1);
		JLabel label2 = new JLabel("INFO: In Euclidean plane geome");
		label2.setLocation(30, 140);
		label2.setSize(170, 20);
		add(label2);

	}

	public void paintComponent(Graphics g) {
		g.setColor(Color.BLUE);
		g.fillOval(20, 20, 200, 100);

	}
}
