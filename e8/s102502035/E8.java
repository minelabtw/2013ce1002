package ce1002.e8.s102502035;

import java.awt.GridLayout;
import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame = new JFrame();// frame object
		MyPanel panel1 = new MyPanel();
		MyPanel2 panel2 = new MyPanel2();
		MyPanel3 panel3 = new MyPanel3();
		frame.add(panel1);
		frame.add(panel2);
		frame.add(panel3);
		frame.setSize(810, 260);
		frame.setLayout(new GridLayout(1, 3, 10, 10));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
