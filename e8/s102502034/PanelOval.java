package ce1002.e8.s102502034;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelOval extends JPanel{
	PanelOval()
	{
		setBounds(430,10,200,230);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,1));
		JLabel label1 = new JLabel("Oval");
		JLabel label2 = new JLabel("Info:A nice MAGENTA Oval");
	    setLayout(null);
		label1.setBounds(15,160,500,50);
		label2.setBounds(15,180,500,50);
		add(label1);
	    add(label2);
		setVisible(true);
		//Oval's data
	}
	
	public void paintComponent(Graphics g )
	{
		g.setColor(Color.MAGENTA);
		g.fillOval(10, 10, 180, 120);
		// drawing 
	}
	

}
