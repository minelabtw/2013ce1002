package ce1002.e8.s102502034;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class E8 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setSize(700,290);
		f.setLayout(null);
		//f.setLayout(new GridLayout(1, 3,10,10));
		PanelOval Oval = new PanelOval();
		PanelRoundRec RoundRec = new PanelRoundRec();
		PanelRec Rec = new PanelRec();

		f.add(Oval);
		f.add(RoundRec);
		f.add(Rec);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

}
