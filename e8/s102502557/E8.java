package ce1002.e8.s102502557;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class E8 extends JPanel{
	private int i;
	static JFrame frm = new JFrame("E8");
	static JPanel pac = new E8(0);//是new E8 因為這樣才會 override  paintcomponent
	static JPanel poly = new E8(1);//
	static JLabel kind = new JLabel();
	static JLabel info = new JLabel();
	static JLabel kind2 = new JLabel();
	static JLabel info2 = new JLabel();

	public E8(int i)
	{
		this.i = i;		//i傳入建構元
	}
	
		public static void main (String[] args) 
	{
		
		frm.setLayout(null);
		frm.setLocationRelativeTo(null);
		frm.setSize(700,300);
		kind.setText("PacMan");
		kind.setBounds(100, 100, 100, 100);
		pac.setLayout(null);
		pac.add(kind);//Label加進Panel裡
		info.setText("is so cute");
		info.setBounds(100,120,100,100);
		pac.add(info);
		
		kind2.setText("Polygon");
		kind2.setBounds(100, 100, 100, 100);
		poly.setLayout(null);
		poly.add(kind2);//Label加進Panel裡
		info2.setText("is so easy");
		info2.setBounds(100,120,100,100);
		poly.add(info2);
		
		LineBorder border = new LineBorder(Color.black, 5);//宣告一個lineborder類別
		pac.setBorder(border);//setborder函數 可以放顏色 寬度等引數	
		pac.setBounds(30,30,300,200);
		frm.add(pac);
		
		LineBorder border2 = new LineBorder(Color.black, 5);//宣告一個lineborder類別
		poly.setBorder(border);//setborder函數 可以放顏色 寬度等引數	
		poly.setBounds(350,30,300,200);
		frm.add(poly);
		
		frm.setVisible(true);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	 protected void paintComponent(Graphics g)	    
	 {
    	if(i==0)
    	{
		 super.paintComponent(g);
    	 g.setColor(Color.PINK);
    	 g.fillArc(40, 20, 70, 70, 30, 300);
    	 g.setColor(Color.RED);
    	 for(int x=90;x<190;x=x+20)
    	 g.fillOval(x, 50, 10, 10);
    	}	
    	if(i==1)
    	{
    		 super.paintComponent(g);
        	 int x[]={44,65,97,139,58};
        	 int y[]={34,55,40,109,127};
        	 g.setColor(Color.GREEN);
        	 g.fillPolygon(x,y,5);
    	}
	 }
}
