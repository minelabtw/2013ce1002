package ce1002.e8.s102502051;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class Rectrangle extends JPanel{
	JLabel graph=new JLabel();
	JLabel info=new JLabel();
	Rectrangle(){
		setSize(250, 280); //設定大小
		setLocation(300,10); //設定位置
		setLayout(null);
		setBorder(new LineBorder(Color.black, 3)); //設定邊界屬性
		graph.setLocation(10,200);
		graph.setSize(120,10);
		graph.setText("Graph:Rectrangle");
		add(graph);
		info.setLocation(10,220);
		info.setSize(200,10);
		info.setText("Info:Rectrangle");
		add(info);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.blue); //設定顏色
		g.fillRect(30,25,80,150); // 設定圓形位置、大小
	}

}
