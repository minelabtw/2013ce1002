package ce1002.e8.s102502009;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel2 extends JPanel {

	@Override
	protected void paintComponent(Graphics h) {
		// TODO Auto-generated method stub
		super.paintComponent(h);

		h.setColor(Color.YELLOW);
		h.fillOval(120, 50, 200, 200);
		h.setColor(Color.ORANGE);
		h.fillOval(160, 90, 120, 120);
		h.setColor(Color.GREEN);
		h.fillRect(195, 230, 50, 80);
		h.setColor(Color.GREEN);
		h.fillOval(120, 250, 70, 50);
		h.setColor(Color.GREEN);
		h.fillOval(250, 250, 70, 50);
	}

	MyPanel2() {
		// setLayout(null);
		LineBorder Border = new LineBorder(Color.BLACK, 3);
		setBorder(Border);

		add(new JLabel("Graph : Oval"));
		add(new JLabel("info : banana "));
	}

}
