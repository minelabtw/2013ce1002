package ce1002.e8.s102502530;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.image.ImageObserver;
import java.text.AttributedCharacterIterator;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class E8 {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new FlowLayout());
		frame.setSize(820, 310);
		JPanel[] panels = new JPanel[3];
		panels[0] = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				g.setColor(Color.blue);
				g.fillRect(10, 10, 240, 190);
			};
		};
		panels[1] = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				g.setColor(Color.orange);
				Polygon triangle = new Polygon();
				triangle.addPoint(10, 190);
				triangle.addPoint(240, 190);
				triangle.addPoint(125, 10);
				g.fillPolygon(triangle);
			}
		};
		panels[2] = new JPanel() {
			@Override
			public void paintComponent(Graphics g) {
				g.setColor(Color.green);
				Polygon parallelogram = new Polygon();
				parallelogram.addPoint(10, 190);
				parallelogram.addPoint(200, 190);
				parallelogram.addPoint(240, 10);
				parallelogram.addPoint(50, 10);
				g.fillPolygon(parallelogram);
			}
		};
		for(int i = 0 ; i != 3 ; i++) {
			panels[i].setLayout(null);
			panels[i].setPreferredSize(new Dimension(260, 260));
			frame.add(panels[i]);
		}
		panels[0].add(new JLabel("Graph: Rectangle"));
		panels[0].add(new JLabel("Info: It's just a rectangle."));
		panels[1].add(new JLabel("Graph: Triangle"));
		panels[1].add(new JLabel("Info: It's just a triangle."));
		panels[2].add(new JLabel("Graph: Parallelogram"));
		panels[2].add(new JLabel("Info: It's just a parallelogram"));
		for(int i = 0 ; i != 3 ; i++) {
			panels[i].getComponent(0).setBounds(10, 210, 240, 20);
			panels[i].getComponent(1).setBounds(10, 230, 240, 20);
			panels[i].setBorder(BorderFactory.createLineBorder(Color.black, 5)) ;
		}
		frame.setVisible(true);
	}
}
