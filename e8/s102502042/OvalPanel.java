package ce1002.e8.s102502042;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
public class OvalPanel extends JPanel{
	
	OvalPanel()
	{
		setLayout(null);
		JLabel label=new JLabel("Graph: Oval"),label2=new JLabel("Info: An Oval(from Latin ovum,...");
		label.setLocation(30,110);
		label2.setLocation(30,120);
		label.setSize(100,100);
		label2.setSize(180,100);
		add(label);
		add(label2);
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.fillOval(50,30,100,80);
	}
}
