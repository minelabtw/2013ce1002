package ce1002.e8.s102502042;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame{

	RectPanel p1 = new RectPanel();
	OvalPanel p2 = new OvalPanel();
	PolygonPanel p3 =new PolygonPanel();
	MyFrame()
	{
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		p1.setSize(230,250);
		p1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		p1.setLocation(30,30);
		p2.setSize(230,250);
		p2.setLocation(280,30);
		p2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		p3.setSize(230,250);
		p3.setLocation(530,30);
		p3.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(p1);
		add(p2);
		add(p3);
	}
}
