package ce1002.e8.s102502042;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
public class PolygonPanel extends JPanel{
	PolygonPanel()
	{
		setLayout(null);
		JLabel label=new JLabel("Graph: Polygon"),label2=new JLabel("Info: In geometry a polygon is t...");
		label.setLocation(30,110);
		label2.setLocation(30,120);
		label.setSize(100,100);
		label2.setSize(180,100);
		add(label);
		add(label2);
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.green);
		int x[]={50,70,110,130,110,70};
		int y[]={75,50,50,75,100,100};
		g.fillPolygon(x,y,6);
	}
}
