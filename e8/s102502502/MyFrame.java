package ce1002.e8.s102502502;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	public MyFrame()
	{
		//set frame's size , layout
		setLayout(null);
		setBounds(300 , 50 , 285 , 700);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//Create three graph panels
	    Arc panel1 = new Arc();
		//add panel1 to frame
		add(panel1);
	    Polygon panel2 = new Polygon();
		//add panel1 to frame
		add(panel2);
		Rec panel3 = new Rec();
		//add panel1 to frame
		add(panel3);
		
	}

}
