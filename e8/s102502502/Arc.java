package ce1002.e8.s102502502;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Arc extends JPanel{
       public Arc(){
   		//set its position and size
   		setBounds(10, 10, 250 , 200);
   		//set its border
   		setBorder(BorderFactory.createLineBorder(Color.BLACK,2));
       }
		public void paintComponent(Graphics g) { 
			super.paintComponent(g);
			g.setColor(Color.magenta);      //set color magenta for g 
			g.drawArc(50, 60, 90, 60, 0, 30);
			g.drawArc(50, 60, 90, 60, 90, 30);
			g.drawArc(50, 60, 90, 60, 180, 30);
			g.drawArc(50, 60, 90, 60, 270, 30);
			g.setColor(Color.magenta);      //set color magenta for g   
			g.fillArc(50, 60, 90, 60, 0, 30);
			g.fillArc(50, 60, 90, 60, 90, 30);
			g.fillArc(50, 60, 90, 60, 180, 30);
			g.fillArc(50, 60, 90, 60, 270, 30);
			g.drawString("Graph: Arc.", 10, 150);
	        g.drawString("Info: an arc...", 10, 160);
			
	}
}
