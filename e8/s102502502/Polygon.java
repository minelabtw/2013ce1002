package ce1002.e8.s102502502;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Polygon extends JPanel{
	public Polygon(){
		//set its position and size
		setBounds(10, 230, 250 , 200);
		//set its border
		setBorder(BorderFactory.createLineBorder(Color.BLACK,2));
	}

	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
		int [] x = {50, 110, 80};   
		int [] y = {50, 50, 125};
		g.setColor(Color.red);     //set color red for g 
		g.drawPolygon(x, y, 3);
		g.fillPolygon(x, y, 3);
		int [] w = {80, 50, 110};
		int [] z = {20, 95, 95};
		g.setColor(Color.red);      //set color red for g 
		g.drawPolygon(w, z, 3);
		g.fillPolygon(w, z, 3);
		g.drawString("Graph: Star.", 10, 150);
        g.drawString("Info: a star...", 10, 160);
	}
}
