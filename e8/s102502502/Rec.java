package ce1002.e8.s102502502;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Rec extends JPanel{
	public Rec() {
		setBounds(10, 440, 250 , 200);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}
 
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(10, 10, 220, 120);
		g.setColor(Color.BLACK);
		g.drawString("Graph: Rectangle.", 10, 150);
        g.drawString("Info: a rectangle...", 10, 160);
	 }
}
