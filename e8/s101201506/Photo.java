package ce1002.e8.s101201506;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Photo extends JPanel
{
	private int type;
	
	public Photo(int i) //建構式
	{
		type = i;
		setBorder(BorderFactory.createLineBorder(Color.black, 5));
	}
	

	protected void paintComponent(Graphics graphics) //畫出來
	{
		// TODO Auto-generated method stub
		super.paintComponent(graphics);
		if(type == 0)//舉行
		{
			graphics.setColor(Color.green);
			graphics.fillRect(60, 30, 150, 170);
			graphics.setColor(Color.black);
			graphics.drawString("Garph: Rectangle",40 , 200);
			graphics.drawString("Info: Just a Rectangle",40 , 220);
		}
		else if(type == 1)//橢圓
		{
			graphics.setColor(Color.pink);
			graphics.fillOval(50, 20, 200, 180);
			graphics.setColor(Color.black);
			graphics.drawString("Garph: Oval",40 , 200);
			graphics.drawString("Info: Just a OVal",40 , 220);
		}
		else//多邊形
		{
			int x[] = {100 , 140 , 180 , 140 , 100 , 60};
			int y[] = {100 , 100 , 140 , 180 , 180 , 140};
			graphics.setColor(Color.blue);
			graphics.fillPolygon(x, y, 6);
			graphics.setColor(Color.black);
			graphics.drawString("Garph: Polygon",40 , 200);
			graphics.drawString("Info: Just a 6 edge polygon",40 , 220);
			
		}
	}
}
