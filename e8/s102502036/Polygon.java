package ce1002.e8.s102502036;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
public class Polygon extends JPanel{
	protected JLabel label = new JLabel("Graphic: Polygon");
	protected JLabel label2 = new JLabel("Info:");
	public Polygon(){
		setLayout(null);
		setBounds(440,0,200,240) ;
		setBorder(new LineBorder(Color.BLACK,3)) ;

		label.setBounds(15,100,100,50);
		label2.setBounds(15,160,100,50);
		add(label) ;
		add(label2) ;
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		java.awt.Polygon poly = new java.awt.Polygon();
		
		int xCenter = getWidth() / 2-40;
		int yCenter = getHeight() / 2-50;
		int radius = (int) Math.min(getWidth(),getHeight()*0.2);
		
		poly.addPoint(xCenter +radius, yCenter);
		poly.addPoint( (int)(xCenter +radius*Math.cos(2*Math.PI/6)), (int)(yCenter-radius*Math.sin(2*Math.PI/6) ));
		poly.addPoint( (int)(xCenter +radius*Math.cos(2*2*Math.PI/6)), (int)(yCenter-radius*Math.sin(2*2*Math.PI/6) ));
		poly.addPoint( (int)(xCenter +radius*Math.cos(3*2*Math.PI/6)), (int)(yCenter-radius*Math.sin(3*2*Math.PI/6) ));
		poly.addPoint( (int)(xCenter +radius*Math.cos(4*2*Math.PI/6)), (int)(yCenter-radius*Math.sin(4*2*Math.PI/6) ));
		poly.addPoint( (int)(xCenter +radius*Math.cos(5*2*Math.PI/6)), (int)(yCenter-radius*Math.sin(5*2*Math.PI/6) ));
		
		g.setColor(Color.PINK) ;
		g.fillPolygon(poly) ;
		


	}
	

}
