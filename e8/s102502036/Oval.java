package ce1002.e8.s102502036;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.*; 
public class Oval extends JPanel{
	protected JLabel label = new JLabel("Graphic: Oval");
	protected JLabel label2 = new JLabel("Info:");
	
	public Oval(){
		setLayout(null);
		setBounds(220,0,200,240) ;
		setBorder(new LineBorder(Color.BLACK,3)) ;
		label.setBounds(15,100,100,50);
		label2.setBounds(15,160,100,50);
		add(label) ;
		add(label2) ;
		
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.PINK) ;
		g.fillOval(15,15, 100, 80) ;
		
		
		
	}

}
