package ce1002.e8.s102502529;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Panel2 extends JPanel {
	
	Panel2(){
		setLayout(null);
		setBounds(350, 20, 300, 250);
		this.setBorder(new LineBorder(Color.black, 5));
		JLabel re = new JLabel();
		re.setText("Graph: Oval");												//設定標籤內容
		re.setBounds(40, 200, 100, 20);
		JLabel info =new JLabel();
		info.setText("Info:An oval(from Latin ovum,.....");
		info.setBounds(40, 220, 200, 20);
		add(re);																//加入倒panel
		add(info);
	}
	public void paintComponent (Graphics grap){
		grap.setColor(Color.yellow);
		grap.fillOval(40, 80, 200, 100);										//畫橢圓
		
	}

}
