package ce1002.e8.s102502529;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Panel3 extends JPanel {
	
	Panel3(){
		setBounds(680, 20, 300, 250);
		this.setBorder(new LineBorder(Color.black, 5));
		
	}
	public void paintComponent (Graphics grap){
		int[] x = {30, 100, 10,  100};
        int[] y = {70,  70, 100, 10};
		grap.setColor(Color.green);
		grap.fillPolygon(x, y,x.length);
	}

}