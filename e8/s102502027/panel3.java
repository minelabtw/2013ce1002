package ce1002.e8.s102502027;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class panel3 extends JPanel {
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		int x[] = { 20, 56, 100, 150, 120, 87, 30 };
		int y[] = { 25, 40, 80, 110, 140, 90, 60 };

		g.fillPolygon(x, y, 7);
		g.setColor(Color.BLACK);
		g.drawString("Graphic :   rectanglenfo", 80, 250);
	}
	
	panel3() {
		setSize(200,50);
		LineBorder lineBorder =new LineBorder(Color.BLACK,2);
		setBorder(lineBorder);
	}
}
