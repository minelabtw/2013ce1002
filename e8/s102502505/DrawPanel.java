package ce1002.e8.s102502505;

import javax.swing.*;
import java.awt.*;

public class DrawPanel extends JPanel{
	
	protected void paintComponent(Graphics g){
		
		super.paintComponent(g);

		g.drawRect(20,20,170,180);//畫兩個邊框
		g.drawRect(220, 20, 170, 180);
		
		int x[] = {40,170,60,105,150};//畫星型
		int y[] = {85,85,160,40,160};
		g.drawPolygon(x,y,x.length);
		g.setColor(Color.BLUE);//設字的顏色
		g.drawString("Graph: Star",60,180);//輸出字串
		g.drawString("Use Pologon",60,195);
		
		g.setColor(Color.GREEN);//設定顏色
		g.fillArc(260,50,100,100,0,45);//畫弧形
		g.fillArc(260,50,100,100,90,45);
		g.fillArc(260,50,100,100,180,45);
		g.fillArc(260,50,100,100,270,45);
		g.setColor(Color.BLUE);//設定顏色
		g.drawString("Graph: Arc",275,180);
		g.drawString("Use fillArc", 275, 195);
	
	}	
}
	
