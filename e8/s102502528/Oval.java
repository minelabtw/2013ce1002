package ce1002.e8.s102502528;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

class Oval extends JPanel{

    Oval() {
        setBounds(230, 10, 210, 250);   //set location, size and border
        Border border = new LineBorder(Color.black, 5);
        setBorder(border); 
    }

    public void paintComponent(Graphics g) { //draw things that should in the panel
        g.setColor(Color.green); 
        g.fillOval(10, 10, 190, 140);
        g.setColor(Color.black);
        g.drawString("Graph: 橢圓", 30, 180);
        g.drawString("Info : 到兩個固定點的距離之和", 30, 210);
        g.drawString("         是常數的軌跡", 30, 230);
    }
}

