package ce1002.e8.s102502528;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

class Rectangle extends JPanel {

    Rectangle() {
        setBounds(10, 10, 210, 250); //set location, size and border
        Border border = new LineBorder(Color.black, 5);
        setBorder(border); 
    }

    public void paintComponent(Graphics g) { //draw things that should in the panel
        g.setColor(Color.BLUE); 
        g.fillRect(10, 10, 190, 140);
        g.setColor(Color.black);
        g.drawString("Graph: 長方形", 30, 180);
        g.drawString("Info : 為四個內角相等的四邊形", 30, 210);
    }
}
