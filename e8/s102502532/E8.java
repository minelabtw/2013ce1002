package ce1002.e8.s102502532;

import javax.swing.JFrame;

public class E8 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyFrame frame = new MyFrame(); // 建立板子
		MyPanel[] panel = new MyPanel[3]; // 建立

		frame.setSize(930, 320); // frame寫在 main 裡
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		for (int i = 0; i < 3; i++) {
			panel[i] = new MyPanel(); // 三個畫布
			panel[i].setType(i);
			frame.addpanel( panel[i], 10 + 310 * i, 20);
		}

		frame.setVisible(true); // 擺最後
	}

}
