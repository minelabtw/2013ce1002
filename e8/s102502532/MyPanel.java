package ce1002.e8.s102502532;

import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.border.LineBorder; //LineBorder

public class MyPanel extends JPanel {

	private int type = 1;

	public MyPanel() {
		setSize(260, 230); // 畫布大小
		setBorder(new LineBorder(Color.BLACK, 3)); // 畫布邊框 (用setBorder函式)
	}

	public void setType(int newtype) {
		type = newtype;
	}
	/*public int getType() {
		return type;
	}*/

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); // 改寫 畫圖形

		switch (type) {                  //自動會跑
		case 0:
			g.setColor(Color.BLUE);
			g.fillRect(20, 10, 200, 150);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Rectangle", 10, 180);
			break;
		case 1:
			g.setColor(Color.GREEN);
			g.fillOval(20, 10, 200, 150);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Ovel", 10, 180);
			break;
		case 2:
			g.setColor(Color.RED);
			g.fillRoundRect(20, 10, 200, 150, 20, 20);
			g.setColor(Color.BLACK);
			g.drawString("Graph: RoundRectangle", 10, 180);
			break;
		/*case 3:
			g.setColor(Color.YELLOW);
			g.fillRoundRect(20, 10, 200, 150, 20, 20);
			g.setColor(Color.BLACK);
			g.drawString("Graph: RoundRectangle", 10, 180);
			break;	*/	
		}
	}
}
