package ce1002.e8.s102503017;

import javax.swing.*;

import java.awt.*;

//same as Rectangle.
public class Hexagon extends JPanel 
{
	JPanel panel = new JPanel();
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	
	Hexagon()
	{
		setLayout(null);
		setBounds(0, 0, 400, 400);
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		label1.setBounds(140,300,200,25);
		label1.setText("Graph: Hexagon.");
		add(label1);
		label2.setBounds(80,325,300,25);
		label2.setText("Area: (3*(sqrt(3))*side length^2)/2");
		add(label2);
	}
	
	protected void paintComponent(Graphics g)
	{

		g.setColor(Color.RED);
		int[] x = {140,225,265,225, 140,100};
		int[] y = { 50,  50, 140,230,230,140};
		g.fillPolygon(x,y,6);
		
		
		
		
		
	}
	
	
	
}
