package ce1002.e8.s102503017;

import javax.swing.*;

import java.awt.*;

//same as Rectangle.
public class Oval extends JPanel
{
	JPanel panel = new JPanel();
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	
	Oval()
	{
		setLayout(null);
		setBounds(0, 0, 400, 400);
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
		label1.setBounds(140,300,200,25);
		label1.setText("Graph: Oval (Ellipse).");
		add(label1);
		label2.setBounds(60,325,300,25);
		label2.setText("Area: Pi*(Vertical bisector/2)*(Horizontal bisector/2)");
		add(label2);
	}
	
	protected void paintComponent(Graphics g)
	{
		g.setColor(Color.green);
		g.fillOval(50, 50, 300, 200);
		
	}
	
	
}
