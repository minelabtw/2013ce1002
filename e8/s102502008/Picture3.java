package ce1002.e8.s102502008;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.BorderFactory ;

public class Picture3 extends JPanel
{
	public Picture3()
	{
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3)) ;					//set the Borderline
		setBounds(430,10,200,200);													//set the location of the frame 
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g) ;
		g.setColor(Color.BLACK) ;													//set the color of picture is blabk 
		g.fillArc(getWidth()/2-getWidth()/4,10,getWidth()/2,getHeight()/2,0,30) ;	//draw picture
		g.fillArc(getWidth()/2-getWidth()/4,10,getWidth()/2,getHeight()/2,90,30) ;
		g.fillArc(getWidth()/2-getWidth()/4,10,getWidth()/2,getHeight()/2,180,30) ;
		g.fillArc(getWidth()/2-getWidth()/4,10,getWidth()/2,getHeight()/2,270,30) ;
		g.drawString("Graphic: Arc", 10, 120) ;										//output the info
		g.drawString("Info: the Radius is "+getWidth()/2,10,135) ;
	}
	
}
