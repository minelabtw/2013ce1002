package ce1002.e8.s102502008;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.color.*;
import javax.swing.BorderFactory ;
import javax.swing.border.*;
import javax.swing.* ;

public class Picture1 extends JPanel
{
	public Picture1()
	{
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3) );	//set the Borderline
		setBounds(220,10,200,200);									//set the location of the frame 
	}
	@Override 
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g) ;
		g.setColor(Color.BLUE) ;									//set the color of picture is blue 
		g.fillRect(getWidth()/4,10,getWidth()/2,getHeight()/2) ;	//draw picture
		g.setColor(Color.BLACK) ;									//set the color of string is black
		g.drawString("Graphic: Rectangle",10,120) ;					//output the info
		g.drawString("info: The size is 100 x 100",10 , 135) ;
	}
	
}
