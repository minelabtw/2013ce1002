package ce1002.e8.s102502544;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.border.LineBorder;

public class Rectangle extends JPanel{
	
	Rectangle(){
		setLayout(null); //設定排版
		setBounds(0,0,260,190); //設定邊界
		setBorder(new LineBorder(Color.BLACK,3)); //設定邊線
		
		JLabel l1 = new JLabel("Graph:Rectangle");
		l1.setBounds(10,130,100,15);
		
		JLabel l2 = new JLabel("Info: In Euclidean plane geome...");
		l2.setBounds(10,150,180,15);
		
		add(l1); //將此label加入此panel
		add(l2);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLUE); //設定顏色
		g.fillRect(25,10,210,100); //畫圖形
		
	}
}
