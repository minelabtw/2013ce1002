package ce1002.e8.s102502544;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	Rectangle rectangle = new Rectangle(); 
	Oval oval = new Oval();
	Polygon polygon = new Polygon();
	
	MyFrame(){
		setLayout(null); //設定排版	
		setSize(900,500); //設定大小
		
		rectangle.setLocation(10,10); //設定位置
		add(rectangle); //將此panel加入frame
		
		oval.setLocation(280,10);
		add(oval);
		
		polygon.setLocation(550,10);
		add(polygon);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
