package ce1002.e8.s102502544;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.border.LineBorder;

public class Polygon extends JPanel{
	
	Polygon(){
		setLayout(null); //設定排版
		setBounds(0,0,260,190); //設定邊界
		setBorder(new LineBorder(Color.BLACK,3)); //設定邊線
		
		JLabel l1 = new JLabel("Graph:Polygon");
		l1.setBounds(10,130,100,15);
		
		JLabel l2 = new JLabel("Info: In geometry a polygon is t...");
		l2.setBounds(10,150,190,15);
		
		add(l1); //將此label加入此panel
		add(l2);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int x[]={110,140,155,140,110,95};
		int y[]={50,50,70,90,90,70};
		g.setColor(Color.GREEN); //設定顏色
		g.fillPolygon(x,y,6); //畫圖形
	}
}
