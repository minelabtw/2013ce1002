package ce1002.e8.s102502516;

import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class MyFrame extends JPanel {
	MyFrame() {
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20)); // 設定邊框
		GridLayout gridLayout = new GridLayout(1, 4, 20, 20); // 排版
		setLayout(gridLayout);

		setVisible(true);
		MyPanel panel[] = new MyPanel[4]; // 四種圖形的panel

		for (int i = 0; i < panel.length; i++) {
			panel[i] = new MyPanel(i);
			add(panel[i]); // 分別加入
		}

	}

}
