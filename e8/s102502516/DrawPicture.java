package ce1002.e8.s102502516;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawPicture extends JPanel {

	protected int type = 0;

	public DrawPicture(int type) {
		this.type = type;
		setBounds(3, 3, 246, 356);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		switch (type) {
		// 1�¯x���B2�����B3���ΡB4�����
		case 0:
			g.setColor(Color.black);
			g.fillRect(50, 40, 130, 99);
			break;
		case 1:
			g.setColor(Color.yellow);
			g.fillOval(40, 30, 170, 130);
			break;
		case 2:
			g.setColor(Color.GREEN);
			g.fillRect(65, 30, 120, 120);
			break;
		case 3:
			g.setColor(Color.red);
			g.fillOval(60, 30, 120, 120);
			break;
		default:

			break;

		}
	}
}