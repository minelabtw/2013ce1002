package ce1002.e8.s102502516;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected int type = 0;

	MyPanel(int type) {
		GridLayout gridLayout = new GridLayout(2, 1, 20, 20);
		setLayout(gridLayout);
		setBorder(BorderFactory.createLineBorder(Color.black, 3)); // 邊框
		DrawPicture pic = new DrawPicture(type); //畫圖
		add(pic);
		Label textLabel[] = new Label[4];
		for (int i = 0; i < textLabel.length; i++) {
			textLabel[i] = new Label();
		}

		// 1黑矩型、2黃橢圓、3綠方形、4紅圓形
		textLabel[0].setText("這是黑色的矩形");
		textLabel[1].setText("這是黃色的橢圓");
		textLabel[2].setText("這是綠色的方形");
		textLabel[3].setText("這是紅色的圓形");
		add(textLabel[type]);// 加入圖形

	}

}
