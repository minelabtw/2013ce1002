package ce1002.e8.s102502545;

import javax.swing.*;

import java.awt.*;

public class Rectangle extends JPanel {
	JLabel jlb1 = new JLabel();//宣告標籤
	JLabel jlb2 = new JLabel();//宣告標籤

	public Rectangle() {

		setLayout(null);
		setBorder(BorderFactory.createLineBorder(Color.black, 4));//此Panel的邊框
		setBounds(10, 10, 380, 300);//此Panel的邊框

	}

	public void setPicture() {//做出LABEL
		jlb1.setText("Graph: Rectangle");
		jlb1.setBounds(30, 150, 100, 20);
		add(jlb1);

		jlb2.setText("info:   In Euclidean plane ....");
		jlb2.setBounds(30, 180, 100, 20);
		add(jlb2);
	}

	public void paintComponent(Graphics g) {//畫圖時間
		super.paintComponent(g);
		g.setColor(Color.blue);
		g.fillRect(30, 30, 315, 100);

	}
}
