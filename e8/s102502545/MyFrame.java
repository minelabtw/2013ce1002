package ce1002.e8.s102502545;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame {

	Rectangle Rec = new Rectangle();// new Rectangle class
	Oval Ova = new Oval();// new Oval class

	public MyFrame() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 關閉視窗後結束程式
		setLayout(null);//排版

		Rec.setPicture();//呼叫函式
		Ova.setPicture();//呼叫函式
		add(Rec);
		add(Ova);

		setSize(810, 360);// 利用frame設定外框大小
		setVisible(true);
	}

}

//這次練習真的麻煩助教們了 QAO   我一開始用了很笨的方法 haha ,  我現在有改用比較簡單的方式,感謝助教的幫忙 =)