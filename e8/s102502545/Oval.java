package ce1002.e8.s102502545;

import javax.swing.*;

import java.awt.*;

public class Oval extends JPanel {
	JLabel jlb1 = new JLabel();//宣告標籤
	JLabel jlb2 = new JLabel();//宣告標籤

	public Oval() {

		setLayout(null);
		setBorder(BorderFactory.createLineBorder(Color.black, 4));//此Panel的邊框
		setBounds(400, 10, 380, 300);//此Panel的邊框

	}

	public void setPicture() {//做出LABEL
		
		jlb1.setText("Graph: Oval");
		jlb1.setBounds( 30, 150, 200, 20);
		add(jlb1);

		jlb2.setText("info:   An oval ....");
		jlb2.setBounds( 30, 180, 200, 20);
		add(jlb2);
		
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);//畫圖時間s
		g.setColor(Color.yellow);
		g.fillOval(60, 35, 260, 100);
	}
}
