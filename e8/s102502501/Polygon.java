package ce1002.e8.s102502501;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class Polygon extends JPanel {
	
	protected JLabel graph = new JLabel("Graph:Polygon");
	protected JLabel info = new JLabel ("Info:In geometry a polygon is five sides above graph are called Polygon.");
	
	Polygon(){
		
		
		/*設置面板的大小和佈局*/
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,140,280,60);
		/*添加面板*/
		add(graph);
		add(info);
		
	}
	
	protected void paintComponent(Graphics p) {
		// 畫出一個充滿綠色的多邊形
		super.paintComponent(p);
		int x [] = {90,155,185,155,95,65};
	    int y [] = {10,20,70,120,120,70};
	    p.setColor(Color.GREEN);		
		p.fillPolygon(x,y,6);
	}

}
