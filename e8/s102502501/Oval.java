package ce1002.e8.s102502501;
import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Oval extends JPanel {
	protected JLabel graph = new JLabel("Graph: Oval");
	protected JLabel info = new JLabel("Info: The little oval office the distance between the focus and are equal to");
	
	Oval() {
		
		/*設置面板的大小和佈局*/
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		/*添加面板*/
		add(graph);
		add(info);
	
	}
	
	protected void paintComponent(Graphics o) {
		// 畫出一個充滿黃色的多邊形
		super.paintComponent(o);		
		o.setColor(Color.YELLOW);
		o.fillOval(50,20,150,100);
			
	}

}
