package ce1002.e8.s102502501;
import javax.swing.*;
import java.awt.Color;

public class MyFrame extends JFrame {
	protected Oval oval = new Oval();
	protected Polygon poly = new Polygon();
MyFrame() {
		
		setLayout(null);
		setBounds(0 , 0 , 810 , 260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		/*設置版面位置和大小*/
		oval.setBounds(10 , 10 , 250 , 200);
		/*設定版面邊框*/
		oval.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*將面板放入框架*/
		add(oval);
		
		
		poly.setBounds(530 , 10 , 250 , 200);
		/*設定版面邊框*/
		poly.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*將面板放入框架*/
		add(poly);
	}

}
