package ce1002.e8.s102502030;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Oval extends JPanel {

	protected JLabel text = new JLabel();
	
	public Oval() {
		//set panel
		setLayout( null );
		text.setBounds( 50, 120, 150, 20 );
		add( text );
	}
	
	public void paintComponent( Graphics g ) {
		//draw oval
		super.paintComponent( g );
		
		g.setColor( Color.yellow );
		g.fillOval( 10, 10, 150, 100 );
	}
	
	public void setGraph() {
		//set text
		text.setText( "Graph: Oval");
	}
}
