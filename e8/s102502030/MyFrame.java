package ce1002.e8.s102502030;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected Rect panel1 = new Rect();
	protected Oval panel2 = new Oval();
	protected Polygon panel3 = new Polygon();
	
	public MyFrame() {
		//set frame
		setLayout( null );
		setBounds( 300, 300, 700, 200 );
		setVisible( true );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		
		newRect( panel1, 10, 10 );
		newOval( panel2, 220, 10 );
		newPolygon( panel3, 430, 10 );
	}
	
	public void newRect( Rect panel, int x, int y ) {
		//create a rectangle
		panel.setBounds( x, y, 180, 150 );
		panel.setBorder( BorderFactory.createLineBorder( Color.black, 5 ) );
		panel.setGraph();
		add( panel );
	}
	
	public void newOval( Oval panel, int x, int y ) {
		//create an Oval
		panel.setBounds( x, y, 180, 150 );
		panel.setBorder( BorderFactory.createLineBorder( Color.black, 5 ) );
		panel.setGraph();
		add( panel );
	}
	
	public void newPolygon( Polygon panel, int x, int y ) {
		//create a polygon
		panel.setBounds( x, y, 180, 150 );
		panel.setBorder( BorderFactory.createLineBorder( Color.black, 5 ) );
		panel.setGraph();
		add( panel );
	}
}
