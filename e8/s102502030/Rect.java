package ce1002.e8.s102502030;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Rect extends JPanel {

	protected JLabel text = new JLabel();
	
	public Rect() {
		//set panel
		setLayout( null );
		text.setBounds( 40, 120, 150, 20 );
		add( text );
	}
	
	public void paintComponent( Graphics g ) {
		//draw rectangle
		super.paintComponent( g );
		
		g.setColor( Color.BLUE );
		g.fillRect( 15, 10, 150, 100 );
	}
	
	public void setGraph() {
		//set text
		text.setText( "Graph: Rectangle");
	}
}
