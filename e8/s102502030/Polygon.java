package ce1002.e8.s102502030;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Polygon extends JPanel {

	protected JLabel text = new JLabel();
	
	public Polygon() {
		//set panel
		setLayout( null );
		text.setBounds( 50, 120, 150, 20 );
		add( text );
	}
	
	public void paintComponent( Graphics g ) {
		//draw polygon
		super.paintComponent( g );
		
		int[] xpoint = { 60, 120, 170, 120, 60, 10 };
		int[] ypoint = { 10, 10, 60, 110, 110, 60 };
		g.setColor( Color.green );
		g.fillPolygon( xpoint, ypoint, 6 );
	}
	
	public void setGraph() {
		//set text
		text.setText( "Graph: Polygon");
	}
}
