package ce1002.e8.s101201003;

import javax.swing.JFrame;
public class E8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JFrame frame = new JFrame("E8");
		frame.setSize(650 , 300);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		MyPanel[] poly = new MyPanel[2];
		for(int i=0;i<2;i++)
		{
			poly[i] = new MyPanel(i);
			poly[i].setBounds(i*300 +5*(i+1),0,300,250);
			frame.add(poly[i]);
		}

	}

}
