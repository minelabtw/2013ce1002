package ce1002.e8.s101201003;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.BorderFactory;
import java.awt.Font;
public class MyPanel extends JPanel{
	// Define constants
	public static final int Rectangle=0;
	public static final int Oval=1;
	
	
	private int type=1;
	//Construct
	public MyPanel(){		
		
	}
	public MyPanel(int type){
		this.type=type;
		setBorder(BorderFactory.createLineBorder(Color.black, 5));
	}
	// Draw a figure on the panel
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		switch(type){
		case Rectangle:
			g.setColor(Color.BLUE);
			g.fillRect(40,20,200,100);
			g.setColor(Color.black);
			g.drawString("Garph: Rectangle",40,200);
			g.drawString("Info: Just a Rectangle",40 ,220);
			break;
		case Oval:
			g.setColor(Color.YELLOW);
			g.fillOval(50,20,200,100);
			g.setColor(Color.black);
			g.drawString("Garph: Oval",40,200);
			g.drawString("Info: Just a OVal",40,220);
			break;
		}
	}
	//Return figure type
	public int getType(){
		return type;
	}
	
	
}
