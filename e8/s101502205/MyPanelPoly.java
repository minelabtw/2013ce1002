package ce1002.e8.s101502205;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanelPoly extends JPanel{
	
	JLabel lbl1, lbl2;
	
	public MyPanelPoly(){
		setLayout(null);
		
		// Two label for text
		lbl1 = new JLabel("Graph: Polygon");
		lbl2 = new JLabel("Info: In geometry a polygon is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit.");
		// Location and size
		lbl1.setBounds(20, 200, 250, 15);
		lbl2.setBounds(20, 230, 250, 15);
		// Add to Panel
		add(lbl1);
		add(lbl2);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int xp[] = {50,150,200,150,50,0}; 	// 6 points (x-axis)
		
		// Offset
		for(int i=0; i<6 ;i++){
			xp[i] += 50;
		}
		
		int yp[] = {0,0,50,100,100,50};		// 6 points (y-axis)
		// Offset
		for(int i=0; i<6 ;i++){
			yp[i] += 45;
		}
		g.setColor(Color.green);
		g.fillPolygon(xp, yp, 6);	// Filled polygon
		g.setColor(Color.black);	// Color black
		g.drawPolygon(xp, yp, 6);	// Polygon
	}
}
