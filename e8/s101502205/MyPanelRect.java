package ce1002.e8.s101502205;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanelRect extends JPanel{
	
	JLabel lbl1, lbl2;
	
	public MyPanelRect(){
		setLayout(null);
		
		// Set two labels for text
		lbl1 = new JLabel("Graph: Rectangle");
		lbl2 = new JLabel("Info: In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.");
		lbl1.setBounds(20, 200, 250, 15);	// Location and Size
		lbl2.setBounds(20, 230, 250, 15);
		add(lbl1);	// Add to panel
		add(lbl2);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.blue);			// Color blue
		g.fillRect(30, 20, 230, 150);	// Filled rectangle
		g.setColor(Color.black);		// Color black
		g.drawRect(30, 20, 230, 150);	// Rectangle
	}
}
