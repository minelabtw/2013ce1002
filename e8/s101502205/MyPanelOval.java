package ce1002.e8.s101502205;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanelOval extends JPanel{
	
	JLabel lbl1, lbl2;
	
	public MyPanelOval(){
		setLayout(null);
		
		// Two labels for text
		lbl1 = new JLabel("Graph: Oval");
		lbl2 = new JLabel("Info: An oval (from Latin ovum, \"egg\") is a closed curve in a plane which \"loosely\" resembles the outline of an egg.");
		// Location and size
		lbl1.setBounds(20, 200, 250, 15);
		lbl2.setBounds(20, 230, 250, 15);
		// Add to panel
		add(lbl1);
		add(lbl2);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.yellow);		// Color yellow
		g.fillOval(30, 20, 230, 150);	// Filled Oval
		g.setColor(Color.black);		// Color black
		g.drawOval(30, 20, 230, 150);	// Oval
	}
}
