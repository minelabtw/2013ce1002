package ce1002.e8.s101502205;

import javax.swing.JFrame;
import javax.swing.BorderFactory;
import java.awt.Color;

public class E8 {
	
	public static void main(String[] args) {
		
		// Create a new frame
		JFrame frame = new JFrame();
		
		// Settings
		frame.setLayout(null);
		frame.setSize(960, 380);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		// First panel
		MyPanelRect p1 = new MyPanelRect();
		p1.setBounds(10, 10, 300, 300);
		p1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		frame.add(p1);		// Add to frame
		
		// Second panel
		MyPanelOval p2 = new MyPanelOval();
		p2.setBounds(320, 10, 300, 300);
		p2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		frame.add(p2);		// Add to Frame
		
		// Third panel
		MyPanelPoly p3 = new MyPanelPoly();
		p3.setBounds(630, 10, 300, 300);
		p3.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		frame.add(p3);		// Add to Frame
		
	}
 
}
