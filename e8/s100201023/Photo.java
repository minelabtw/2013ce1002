package ce1002.e8.s100201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Photo extends JPanel
{
	private int type;
	
	public Photo(int i)
	{
		type = i;
		setBorder(BorderFactory.createLineBorder(Color.black, 5));
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		// TODO Auto-generated method stub
		super.paintComponent(g);
		if(type == 0)
		{
			g.setColor(Color.red);
			g.fillRect(40, 20, 200, 100);
			g.setColor(Color.black);
			g.drawString("Garph: Rectangle",40 , 200);
			g.drawString("Info: Just a Rectangle",40 , 220);
		}
		else if(type == 1)
		{
			g.setColor(Color.yellow);
			g.fillOval(50, 20, 200, 100);
			g.setColor(Color.black);
			g.drawString("Garph: Oval",40 , 200);
			g.drawString("Info: Just a OVal",40 , 220);
		}
		else
		{
			int x[] = {40 , 80 , 100 , 80 , 40 , 20};
			int y[] = {20 , 20 , 40 , 60 , 60 , 40};
			g.setColor(Color.blue);
			g.fillPolygon(x, y, 6);
			g.setColor(Color.black);
			g.drawString("Garph: Polygon",40 , 200);
			g.drawString("Info: Just a 6 edge polygon",40 , 220);
			
		}
	}
}
