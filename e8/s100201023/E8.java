package ce1002.e8.s100201023;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class E8
{

	public static void main(String[] args)
	{
		//set frame
		JFrame frame = new JFrame("E8");
		frame.setSize(950 , 300);
		frame.setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		//set panel
		Photo[] poly = new Photo[3];
		for(int i = 0 ; i < 3 ; ++i)
		{
			poly[i] = new Photo(i);
			poly[i].setBounds(i * 300 + 5 * (i + 1), 0, 300, 250);
			frame.add(poly[i]);
		}

	}

}
