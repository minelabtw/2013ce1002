package ce1002.e8.s102502558;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Shape;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	String[] str_graph = new String[3];
	String[] str_info  = new String[3];
	int[] x = {0,0,2,2,1,1,2,2,4,4,5,5,4,4,6,6,4,4,2,2};
	int[] y = {0,2,2,3,3,5,5,4,4,5,5,3,3,2,2,0,0,2,2,0};
	int r = 10;
	MyPanel()
	{
		//set
		str_graph[0] = "Rectangle";
		str_graph[1] = "Oval";
		str_graph[2] = "Polygon";
		str_info[0] = "Just a blue rectangle";
		str_info[1] = "Just a yello oval";
		str_info[2] = "Ssssssss....";
		for (int i=0;i<x.length;i++)
		{
			x[i] = x[i] * r + 550;
			y[i] = y[i] * r + 50;
		}
	}
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		for(int i=0;i<3;i++)
		{
			g.setColor(Color.black);
			g.drawRect(10 + 250 * i, 10, 240, 180);
			g.drawString("Graph: " + str_graph[i], 20 + 250 * i, 160);
			g.drawString("Info: "  +  str_info[i], 20 + 250 * i, 175);
		}
		g.setColor(Color.blue);
		g.fillRect(20, 20, 150, 100);
		g.setColor(Color.yellow);
		g.fillOval(20 + 250, 20, 150, 100);
		g.setColor(Color.green);
		g.fillRect(20 + 500, 20, 130, 100);
		g.setColor(Color.black);
		g.fillPolygon(x, y, x.length);
	}
}
