package ce1002.e8.s102502047;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class po extends JPanel{
	private JLabel a;
	private JLabel b;
	public po() {
		
		setBounds(500,20,180,180);
		setBorder(new LineBorder(Color.BLACK,3));
		a=new JLabel("Graph: Polygon");
		b=new JLabel("Info: In geometry");
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		int x[]={50,75,100,75,50,25};
		int y[]={50,50,100,150,150,100};
		g.fillPolygon(x,y,x.length);
		//a.setBounds(25,130,100,20);
		b.setBounds(25,160,100,20);
		add(a);
		add(b);
	}
}
