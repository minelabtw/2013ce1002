package ce1002.e8.s102502047;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class ov extends JPanel{
	private JLabel a=new JLabel("Graph: Oval");
	private JLabel b=new JLabel("Info: An Oval");
	public ov() {
		setBounds(400,15,300,180);
		setBorder(new LineBorder(Color.BLACK,3));
		add(a);
		add(b);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.fillRoundRect(315,15,200,100,40,20);
		a.setBounds(325,130,100,20);
		b.setBounds(325,150,100,20);
	}

}
