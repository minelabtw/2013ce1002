package ce1002.e8.s102502047;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class re extends JPanel{
	private JLabel a=new JLabel("Graph: Rectangle");
	private JLabel b=new JLabel("Info: In Euclidean");

	public re() {
		setBounds(15,15,250,180);
		setBorder(new LineBorder(Color.BLACK,3));
		add(a);
		add(b);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(15,15,200,100);
		a.setBounds(15,130,100,20);		
		b.setBounds(15,150,100,20);	
	}

}
