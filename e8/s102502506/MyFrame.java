package ce1002.e8.s102502506;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame{
	MyFrame(){
		setSize(860,360);
		setLocationRelativeTo(null);
		setVisible(true);
		JPanel Panel = new JPanel();
		Panel.setLayout(null);  //設定Layout
		MyPanel[] P = new MyPanel[3];
		for(int x=0;x<3;x++){  //用出三個Panel
			P[x]=new MyPanel(x,20 + x * 280,20);
			Panel.add(P[x]);
		}
		add(Panel);
	}
}
