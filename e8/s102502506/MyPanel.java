package ce1002.e8.s102502506;
import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{
	private int T,x,y;
	
	MyPanel(int T,int x,int y){
		this.T=T;
		this.x=x;
		this.y=y;
		setBounds(x,y,250,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 5));  //�]�w�خ�
	}
	public void paintComponent(Graphics g){
		if(T==0){
			g.setColor(Color.GRAY);
			g.fillRect(50, 30, 150, 100);
			g.setColor(Color.BLACK);
			g.drawString("Graph:Rectangle",50,180);
			g.drawString("Info:In Euclidean plane geome...", 50, 210);
		}
		else if(T==1){
			g.setColor(Color.magenta);
			g.fillOval(50, 30, 150, 100);
			g.setColor(Color.BLACK);
			g.drawString("Graph:Oval",50,180);
			g.drawString("Info:An oval(from latin ovum...", 50, 210);
		}
		else{
			int X[]={125,50,90,160,200};
			int Y[]={30,80,145,145,80};
			g.setColor(Color.ORANGE);
			g.fillPolygon(X,Y,X.length);
			g.setColor(Color.BLACK);
			g.drawString("Graph:Polygon", 50, 180);
			g.drawString("������", 50, 210);
		}
	}
}
