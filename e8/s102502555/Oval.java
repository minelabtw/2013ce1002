package ce1002.e8.s102502555;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Oval extends JPanel{
	JLabel graph = new JLabel("Graph: Oval");
	JLabel info = new JLabel("info: An oval(from Latin ovum,...");
	
	Oval(){
		//設定panel的版面
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK, 3));
		
		//設定位置和大小
		graph.setBounds(10, 110, 200, 40);
		info.setBounds(10, 160, 200, 40);
		
		//把label放到panel
		add(graph);
		add(info);
	}
	
	//畫橢圓形
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.fillOval(10, 5, 150, 100);
	}
	
}
