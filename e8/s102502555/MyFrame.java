package ce1002.e8.s102502555;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	Rectangle r = new Rectangle();
	Oval o = new Oval();
	Polygon p = new Polygon();
	
	MyFrame(){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(800, 400);  //設Frame的大小
		setLayout(null);  //設frame的排版
		
		r.setBounds(10, 10, 250, 300);  //設長方形的位置和大小
		o.setBounds(270, 10, 250, 300);  //設橢圓形的位置和大小
		p.setBounds(530, 10, 250, 300);  //設六邊形的位置和大小
		
		add(r);  //把長方形放上去
		add(o);  //把橢圓形放上去
		add(p);  //把六邊形放上去
	}
}
