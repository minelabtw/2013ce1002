package ce1002.e8.s102502555;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;


public class Rectangle extends JPanel{
	JLabel graph = new JLabel("Graph: Rectangel");  
	JLabel info = new JLabel("info: In Euclidean plane geome...");
	
	Rectangle(){
		//設定panel的版面
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK, 3));
		
		//設定位置和大小
		graph.setBounds(10, 110, 200, 40);
		info.setBounds(10, 160, 200, 40);
		
		//把label放到panel
		add(graph);
		add(info);
	}
	
	//畫長方形
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(15, 10, 150, 80);
	}
}
