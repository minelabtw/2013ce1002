package ce1002.e8.s102502555;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.Graphics;


public class Polygon extends JPanel{
	JLabel graph = new JLabel("Graph: Polygon");
	JLabel info = new JLabel("info: In geometry a polygon is t...");
	
	Polygon(){
		//設定panel的版面
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK, 3));
		
		//設定位置和大小
		graph.setBounds(10, 110, 200, 40);
		info.setBounds(10, 160, 200, 40);
		
		//把label放到panel
		add(graph);
		add(info);
	}
	
	//畫六邊形
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int[] x = {85, 100, 140, 155, 140, 100};
		int[] y = {60, 90, 90, 60, 30, 30};
		g.setColor(Color.LIGHT_GRAY);
		g.fillPolygon(x, y, 6);
	}
	
}
