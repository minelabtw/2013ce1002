package ce1002.a8.s102502512;
import java.util.*;

public class A8{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int grade=0;
		double rec[] = new double[4];
		for(int k=0;k<4;k++)
		{
				rec[k]=0;
		}
		Scanner input = new Scanner(System.in);
		System.out.println("Input grades:");
		for(int i=0;i<8;i++)
		{
			do
			{
				grade=input.nextInt();
				if(grade<0||grade>100)
				{
					System.out.println("Out of range!");
				}
			}while(grade<0||grade>100);
			if(0<=grade&&grade<25)
			{
				rec[0]++;
			}
			else if(25<=grade&&grade<50)
			{
				rec[1]++;
			}
			else if(50<=grade&&grade<75)
			{
				rec[2]++;
			}
			else if(75<=grade&&grade<=100)
			{
				rec[3]++;
			}
		}
		for(int j=0;j<4;j++)
		{
			rec[j]=(rec[j]/8)*100;
			System.out.println("Level "+j+" is "+rec[j]+"%");
		}
		
	}

}
