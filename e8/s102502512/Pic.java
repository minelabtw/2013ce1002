package ce1002.a8.s102502512;
import javax.swing.*;

import java.awt.*;

public class Pic extends JPanel{
	private int h=0;
	Pic(int h)
	{
		this.h=h;
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int hie = geth(h);
		g.setColor(Color.gray);
		g.fillRect(0,50,75,250-h);
		g.setColor(Color.BLUE);
		g.fillRect(0, 300-h, 75, h);
	}
	public int geth(int h)
	{
		return h; 
	}
	
}
