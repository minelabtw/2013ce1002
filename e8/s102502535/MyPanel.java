package ce1002.e8.s102502535;

import java.awt.*;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected JLabel label = new JLabel();
	protected JLabel info = new JLabel();
	private int i;

	MyPanel(int i) {
		this.i = i ;
		setLayout(null);
		label.setBounds(70, 100, 200, 200);
		info.setBounds(70,130,200,200);
		add(label);
		add(info);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (i == 0) {
			g.setColor(Color.red);
			g.fillRect(45, 10, 150, 150);
			label.setText("Graph : Square");
			info.setText("It's a red square,whose side is 150.");
		} else if (i == 1) {
			g.setColor(Color.blue);
			g.fillOval(30, 20, 200, 150);
			label.setText("Graph : Oval");
			info.setText("It's a blue oval.");
		}
	}

}
