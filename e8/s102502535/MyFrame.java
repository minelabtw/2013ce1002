package ce1002.e8.s102502535;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel rect = new MyPanel(0);
	protected MyPanel oval = new MyPanel(1);
	
	MyFrame(){
		setLayout(null);
		setBounds(300 , 50 , 600 , 400);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPic(rect,10,20);	
		setPic(oval,400,20);
	}
	
	public void setPic(MyPanel panel,int x,int y){
		panel.setBounds(x, y, 350 , 300);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		setVisible(true);
		add(panel);
	}
}
