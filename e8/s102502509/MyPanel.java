package ce1002.e8.s102502509;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	JLabel lab1;
	JLabel lab2;
	
	
	MyPanel()
	{
		setBounds(60, 10, 180, 180);
		setLayout(null);
		
		lab1 = new JLabel("Graph: Rectangle");
		lab2 = new JLabel("Info: Rectangle is Rec.");
		
		lab1.setBounds(10, 160, 100, 40);// test the size and location.
		lab2.setBounds(10, 215, 100, 35);
		
		add(lab1);
		add(lab2);
		
	}
	protected void paintComponent(Graphics g) 
	{
		super.paintComponent(g);// draw the rectangle
		g.setColor(Color.red);
		g.fillRect(40, 40, 120, 60);

	}
}
