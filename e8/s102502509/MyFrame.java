package ce1002.e8.s102502509;

import java.awt.Color;
import java.awt.GridLayout;
//import javax.swing.border.Border;
import javax.swing.BorderFactory;
import javax.swing.JFrame;



public class MyFrame extends JFrame
{
	protected MyPanel p1 = new MyPanel();
	protected grafic p2 = new grafic();
	protected panel p3 = new panel();
	
	MyFrame()
	{
		setBounds(100, 100, 700, 500);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//GridLayout grid = new GridLayout(1, 3);
		//setLayout(grid);
		
		p1.setBounds(10, 10, 200, 250);// the size and location.
		p2.setBounds(220, 5, 200, 255);
		p3.setBounds(430, 5, 200, 255);
		
		p1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));// the border of panel
		p2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		p3.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		
		add(p1);// remember to add.
		add(p2);
		add(p3);
	}
}
