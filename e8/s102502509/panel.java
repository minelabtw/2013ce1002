package ce1002.e8.s102502509;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class panel extends JPanel
{
	JLabel lab1;
	JLabel lab2;
	
	panel()
	{
		setBounds(60, 10, 180, 400);
		setLayout(null);// layout failed
		
		lab1 = new JLabel("Graph: Polygon");
		lab2 = new JLabel("Info: Coordination is so hard!");
		
		lab1.setBounds(10, 160, 100, 40);
		lab2.setBounds(10, 215, 100, 35);
		
		add(lab1);
		add(lab2);
	}
	protected void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		g.setColor(Color.green);
		int x[] = {65, 125, 155, 125, 65, 35 }; // calculate the coordinate
		int y[] = {10, 10, 80, 150, 150, 80};
		g.fillPolygon(x, y, 6); // the number of the coordinate

	}
}
