package ce1002.e8.s102502053;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame {
	protected Rectangle panel1 = new Rectangle();
	protected Oval panel2 = new Oval();
	protected Hexagon panel3 = new Hexagon();

	MyFrame() 
	{ //set frame layout and size
		setLayout(null); 
		setSize(850, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		add(panel1);
		add(panel2);
		add(panel3);
	}




}
