package ce1002.e8.s102502053;

import javax.swing.*;

import java.awt.*;

public class Oval extends JPanel {

	protected JLabel label = new JLabel();
	protected JLabel label2 = new JLabel();
	
	Oval() //set oval panel
	{ 
		setBounds(260, 5, 250, 200);
		setLayout(null);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		setVisible(true);
		label.setText("Graph: Oval");
		label.setBounds(20, 100, 210, 80);
		label2.setBounds(20, 110, 210, 100);
		label2.setText("Info: An oval(from Latin ovum,...");
		setVisible(true);
		add(label);
		add(label2);
	}
	
	public void paintComponent(Graphics g) 
	{ //create oval
		g.setColor(Color.YELLOW);
		g.fillOval(25, 15, 200, 100);

	}

}
