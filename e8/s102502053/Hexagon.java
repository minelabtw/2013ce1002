package ce1002.e8.s102502053;

import javax.swing.*;

import java.awt.*;

public class Hexagon extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel label2 = new JLabel();
	
	Hexagon() //set hexagon layout
	{ 
		setBounds(515, 5, 250, 200);
		setLayout(null);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		setVisible(true);
		label.setText("Graph: Polygon");
		label.setBounds(20, 100, 210, 80);
		label2.setBounds(20, 110, 210, 100);
		label2.setText("Info: In geometry a polygon is...");
		setVisible(true);
		add(label);
		add(label2);
	}

	public void paintComponent(Graphics g) 
	{ //create hexagon 
		int x[] = {100, 139, 160, 139, 100, 80}; 
		int y[] = {45, 45, 80, 115, 115, 80};
		
		g.setColor(Color.GREEN);
		g.fillPolygon(x, y, 6);

	}


}
