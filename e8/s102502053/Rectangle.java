package ce1002.e8.s102502053;

import javax.swing.*;

import java.awt.*;

public class Rectangle extends JPanel{
	
		protected JLabel label = new JLabel();
		protected JLabel label2 = new JLabel();
		
		Rectangle() //set rectangle panel
		{
			setLayout(null);
			setBounds(5, 5, 250, 200);
			setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
			setVisible(true);
			label.setText("Graph: Rectangle");
			label.setBounds(20, 100, 210, 80);
			label2.setBounds(20, 110, 210, 100);
			label2.setText("Info: In Euclidean plane geome...");
			setVisible(true);
			add(label);
			add(label2);
		}
		
		public void paintComponent(Graphics g) //create the rectangle
		{
			g.setColor(Color.BLUE);
			g.fillRect(25, 15, 200, 100);
		}


}
