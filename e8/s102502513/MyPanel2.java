package ce1002.e8.s102502513;            //建立面板2

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel2 extends JPanel
{
	public MyPanel2()
	{
		setBounds(180,10,150,150);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setLayout(null);
		JLabel jlb =new JLabel("This is a oval.");
		jlb.setBounds(10, 120, 100, 20);
		add(jlb);	
	}
	@Override                                  //於面板中畫出橢圓形
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillOval(10,10,50,100);
	}
}
