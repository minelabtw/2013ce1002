package ce1002.e8.s102502513;       //建立面板1

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	public MyPanel()
	{
		setBounds(10,10,150,150);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setLayout(null);
		JLabel jlb =new JLabel("This is a circle.");
		jlb.setBounds(10, 120, 100, 20);
		add(jlb);	
	}
	@Override                                      //於面板中畫出圓形
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		g.fillOval(10,10,100,100);
	}
}
