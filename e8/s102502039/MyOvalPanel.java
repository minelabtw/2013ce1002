package ce1002.e8.s102502039;
import java.awt.*;

import javax.swing.JPanel;
public class MyOvalPanel extends JPanel{
public MyOvalPanel(){
		
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.fillArc(300, 20, 200, 150, 360, 360);
		g.setColor(Color.BLACK);
		g.drawString("Graph:Oval", 280, 200);
		g.drawString("Info: An oval (from Latin ovum,...)", 280, 220);
	}
}
