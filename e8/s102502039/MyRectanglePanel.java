package ce1002.e8.s102502039;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class MyRectanglePanel extends JPanel {
	public MyRectanglePanel() {

	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(15, 15, 200, 120);
		g.setColor(Color.BLACK);
		g.drawString("Graph:Rectangle", 16, 180);
		g.drawString("Info: in Education plane geome", 16, 200);

	}
}
