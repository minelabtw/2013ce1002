package ce1002.e8.s102502012;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	private MyPanel mypanel[];
	
	MyFrame(){
		JPanel tmp = new JPanel();
		tmp.setLayout(null);
		
		mypanel = new MyPanel[3];
		for(int i = 0; i < 3; i++){
			mypanel[i] = new MyPanel(i, 20 + i * 250, 20);
			
			tmp.add(mypanel[i]);
		}
		add(tmp);
	}
}
