package ce1002.e8.s102502012;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		MyFrame window = new MyFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		window.setSize(800, 300);
		window.setVisible(true);
		window.setLocationRelativeTo(null);
		window.setTitle("s102502012");
	}

}
