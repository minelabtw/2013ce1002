package ce1002.e8.s102502012;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

class MyPanel extends JPanel{
	private int type;
	
	MyPanel(int type, int x, int y){
		this.type = type;
		setBounds(x, y, 200, 200);
		setBorder(BorderFactory.createLineBorder(Color.black, 2));
	}
	
	public void paintComponent(Graphics g){
		if(type == 0){
			g.setColor(Color.red);
			g.fillRect(50, 50, 100, 100);
			g.setFont(new Font("新細明體", Font.BOLD, 14));
			g.setColor(Color.black);
			g.drawString("Graph : Rectangle", 30, 160);
			g.drawString("Info: It's a simple square", 30, 172);
		}
		else if(type == 1){
			g.setColor(Color.blue);
			g.fillOval(50, 50, 100, 75);
			g.setFont(new Font("新細明體", Font.BOLD, 14));
			g.setColor(Color.black);
			g.drawString("Graph : Oval", 30, 160);
			g.drawString("Info: It's a simple oval", 30, 172);
		}
		else if(type == 2){
			int[] xx = new int[]{80, 110, 140, 140, 80};
			int[] yy = new int[]{80, 50, 80, 110, 110};
			
			g.setColor(Color.green);
			g.fillPolygon(xx, yy, xx.length);
			g.setFont(new Font("新細明體", Font.BOLD, 14));
			g.setColor(Color.black);
			g.drawString("Graph : Polygon", 30, 160);
			g.drawString("Info: hahahaha", 30, 172);
			
		}
	}

}
