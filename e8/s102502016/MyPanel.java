package ce1002.e8.s102502016;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	public JLabel lb1;
	public JLabel lb2;
	public OvaPanel Ova = new OvaPanel();
	public RecPanel Rec = new RecPanel();

	public MyPanel() {
		this.setLayout(null); // 非預設排版
		this.setSize(300, 300); // 大小
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}

	public void setPic1() {
		Rec.setBounds(30, 30, 200, 200);
		this.add(Rec);
		lb1 = new JLabel(); // LABEL 1放文字 設置大小位置
		lb1.setText("Graph: Rectangle");
		lb1.setSize(150, 10);
		lb1.setLocation(10, 245);
		this.add(lb1); // 加到Panel�堶�
		lb2 = new JLabel(); // LABEL 2放文字 設置大小位置
		lb2.setText("Info: In Euclidean plane geome...");
		lb2.setSize(150, 10);
		lb2.setLocation(10, 255);
		this.add(lb2); // 加到Panel�堶�
	}

	public void setPic2() {
		Ova.setBounds(30, 30, 200, 200);
		this.add(Ova);
		lb1 = new JLabel(); // LABEL 1放文字 設置大小位置
		lb1.setText("Graph: Oval");
		lb1.setSize(150, 10);
		lb1.setLocation(10, 245);
		this.add(lb1); // 加到Panel�堶�
		lb2 = new JLabel(); // LABEL 2放文字 設置大小位置
		lb2.setText("Info: An oval(from Latin ovum,...");
		lb2.setSize(150, 10);
		lb2.setLocation(10, 255);
		this.add(lb2); // 加到Panel�堶�
	}
}

class RecPanel extends JPanel {
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(30, 30, 100, 200);
	}
}

class OvaPanel extends JPanel {
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillOval(30, 30, 100, 150);
	}
}