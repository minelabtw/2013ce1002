package ce1002.e8.s102502014;

import ce1002.e8.s102502014.MyPanel;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	public MyPanel panel1 = new MyPanel(); // 初始化
	public MyPanel panel2 = new MyPanel();

	public MyFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null); // 非預設排版
		this.setSize(700, 400); // 設置大小
	}

	public void setPanel(int i, MyPanel panel, int x, int y) {
		if (i == 1) {
			panel.setPic1();
		}// 設置panel
		else if (i == 2) {
			panel.setPic2();
		}// 設置panel
		panel.setLocation(x, y); // 設置PANEL位置
		this.add(panel); // 加到FRAME�堶�
		this.setVisible(true); // 可看見
	}
}