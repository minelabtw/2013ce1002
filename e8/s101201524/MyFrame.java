package ce1002.e8.s101201524;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	//create 3 graphics panels
	private MyPanel graph1 = new MyPanel();
	private MyPanel graph2 = new MyPanel();
	private MyPanel graph3 = new MyPanel();
	
	MyFrame(){
		//set status
		setLayout(null);
		setBounds(200, 50, 960, 310);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//set panels' graph
		setPanel(graph1, 10, 10, Color.RED, "Rect");
		setPanel(graph2, 320, 10, Color.BLUE, "Oval");
		setPanel(graph3, 630, 10, Color.GREEN, "Poly");
	}
	
	private void setPanel(MyPanel panel, int x, int y, Color color, String graph){
		//set graph's status
		panel = new MyPanel(color, graph);
		panel.setBounds(x, y, 300, 250);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		//add panel
		add(panel);
	}

}
