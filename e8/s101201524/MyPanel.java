package ce1002.e8.s101201524;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class MyPanel extends JPanel{
	//graph's data
	private Color color = Color.WHITE;
	private String graph = "Rect";
	private int xcenter = 150;
	private int ycenter = 75;
	private int radius = 75;
	
	MyPanel(){
	}
	
	MyPanel(Color color, String graph){
		//set color and graph
		this.color = color;
		this.graph = graph;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(color);
		//draw graph
		switch(graph){
		case "Rect" :
			g.fillRect(20, 10, 260, 150);
		case "Oval" :
			g.fillOval(20, 10, 260, 150);
		case "Poly" :
			int x[] = new int[6];
			int y[] = new int[6];
			for(int i = 0; i < 6; i++){
				x[i] = (int)(xcenter + radius * Math.cos(i * 2 * Math.PI / 6));
				y[i] = (int)(ycenter + radius * Math.sin(i * 2 * Math.PI / 6));
			}
			g.fillPolygon(x, y, 6);
		}
		//add description
		g.setColor(Color.BLACK);
		g.drawString("Graph : " + graph, 30, 200);
	}
}
