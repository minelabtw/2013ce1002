package ce1002.e8.s102502561;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{

	MyPanel(){//建構子
		
		setLayout(null) ;
		JLabel[] label=new JLabel[6] ;//註解處 LABEL
		setBounds(10,10,700,400) ;
		setBorder(new LineBorder(Color.BLACK,3)) ;//粗框

		for (int i=1;i<6;i=i+2){
			label[i]=new JLabel() ;
			label[i].setBounds(140*i-130,350,150,50) ;	
			add(label[i]) ;
		}
		
		
		for (int i=0;i<6;i=i+2){
			label[i]=new JLabel() ;
			label[i].setBounds(10+140*i,250,200,50) ;
			add(label[i]) ;
		}
		
		label[0].setText("Graph: Rectangle") ;	//每個label
		label[1].setText("Info: a red rectangle") ;
		label[2].setText("Graph: Oval") ;
		label[3].setText("Info: a blue oval") ;
		label[4].setText("Graph: 6-polygon") ;
		label[5].setText("Info: great polygon") ;
	}
	
	@Override
	public void paintComponent(Graphics g){
		g.setColor(Color.RED) ;//長方形 
		g.fillRect(10,10,200,180) ;
		
		g.setColor(Color.BLUE) ;
		g.fillOval(250, 5, 200, 180) ;//橢圓形
		
		g.setColor(Color.YELLOW) ;
		int[] xPoints = {500,550,600,650,600,550};//最左 左上 右上 又 右下 左下
		int[] yPoints = {100,50,50,100,150,150};
		g.fillPolygon(xPoints , yPoints , 6);//多邊形

	}	
}
