package ce1002.e8.s102502561;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	public MyFrame() {//建構子 視窗
		MyPanel panel = new MyPanel();
		setBounds(5, 0, 740, 460);
		//getContentPane().setBackground(Color.blue);	//可以調整Frame背景顏色
		setLayout(null);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		add(panel);
	}
}
