package ce1002.e8.s992001026;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.*;
import java.awt.*;
public class Mypanel extends JPanel  {
	
	public static final int Rectangle = 1 ;
	public static final int Oval = 2 ;
	public static final int DRectangle = 4 ;
	public static final int Triangle = 3;
	private int type = 1 ;
	private boolean filled = false ;
	
	public Mypanel(){
		setBorder(new LineBorder(Color.black,2));
	}
	
	public Mypanel(int type){
		setBorder(new LineBorder(Color.black,2));
		this.type = type ;
	}
	
	public Mypanel(int type,boolean filled ){
		setBorder(new LineBorder(Color.black,2));
		this.type = type ;
		this.filled = filled ;
	}
	
	protected void paintComponent(Graphics g ){
		super.paintComponent(g);
		
		
		int  width =  getWidth();
		int height = getHeight();
	
	
		switch (type){
		case Rectangle :
			
			if (filled){
				
				g.setColor(Color.blue);
				g.fillRect((int)(0.2*width),(int)(0.1*height), (int) (0.6*width), (int) (0.6*height)) ;
				g.drawString("Rectangle",(int) (0.4*width), (int) (0.8*height) );
			}else {
				g.setColor(Color.black);
				g.drawString("Rectangle",(int) (0.3*width), (int) (0.8*height) );
				g.setColor(Color.orange);
				g.drawString("yabi",(int) (0.4*width), (int) (0.9*height) );
				g.setColor(Color.blue);
				g.drawRect((int)(0.2*width),(int)(0.1*height), (int) (0.6*width), (int) (0.6*height)) ;
			}
		break;
		
		case Oval:
			
			if (filled){
				g.setColor(Color.black);
				g.drawString("circle",(int) (0.4*width), (int) (0.8*height) );
				g.setColor(Color.orange);
				g.drawString("you're",(int) (0.4*width), (int) (0.9*height) );
				g.setColor(Color.red);
				g.fillOval((int)(0.2*width),(int)(0.1*height), (int) (0.6*width), (int) (0.6*height)) ;
			}else{ 
				g.setColor(Color.black);
				g.drawString("circle",(int) (0.4*width), (int) (0.8*height) );
				g.setColor(Color.red);
				g.drawOval((int)(0.2*width),(int)(0.1*height), (int) (0.6*width), (int) (0.6*height)) ;
			}
		break;
		
		case Triangle :
			if (filled){
				
				int [] xpoint = { (int) (0.5*width), (int)(0.2*width),(int) (0.8*width)};
				int [] ypoint = {(int) (0.1*height),(int) (0.6*height),(int) (0.6*height)};
				g.setColor(Color.black);
				g.drawString("triangle",(int) (0.4*width), (int) (0.8*height) );
				g.setColor(Color.orange);
				g.drawString("idiot",(int) (0.4*width), (int) (0.9*height) );
				g.setColor(new Color(45, 109, 75));
				g.fillPolygon(  xpoint , ypoint , xpoint.length) ;
			}else{ 
				
		
				int [] xpoint = {(int) (0.5*width), (int)(0.2*width),(int) (0.8*width)};
				int [] ypoint = {(int) (0.1*height),(int) (0.6*height),(int) (0.6*height)};
				g.setColor(Color.black);
				g.drawString("triangle",(int) (0.4*width), (int) (0.8*height) );
				g.setColor(new Color(45, 109, 75));
				g.drawPolygon(  xpoint , ypoint , xpoint.length) ;
		
		
			}	
		}

	}
	
	
	public void setType(int type){
		this.type = type;
		repaint();
	}
	
	public int getType(){
		return type ;
	}
	
	public boolean isFilles(){
		return filled;
	}
	
	
	
}
