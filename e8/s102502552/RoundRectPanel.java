package ce1002.e8.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class RoundRectPanel extends JPanel{
	
	RoundRectPanel()//園邊矩形
	{
		setBounds(0,0,200,200);
		setBorder(new LineBorder(Color.BLACK,3));//邊框
		setLayout(null);//排版
		JLabel label = new JLabel("Graph : Round Rectangle");
		label.setBounds(30,120,200,20);
		JLabel label1 = new JLabel("a rectangle with round angles");
		label1.setBounds(15,140,200,60);//設置兩個標籤
		add(label);
		add(label1);//加入兩個標籤
		
	}
	
	public void paintComponent(Graphics G)//繪圖
	{
		super.paintComponent(G);
		G.setColor(Color.RED);//設為紅色
		G.fillRoundRect(20, 10, 160, 100, 40, 40);//畫出圓邊矩形
	}

}
