package ce1002.e8.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Polygon extends JPanel{
	
	Polygon()
	{
		setBounds(420,0,200,200);
		setBorder(new LineBorder(Color.BLACK,3));//邊框
		setLayout(null);
		JLabel label = new JLabel("Graph : Hexagon");
		label.setBounds(55,120,200,20);
		JLabel label1 = new JLabel("a polygon with six edges and six vertices");
		label1.setBounds(5,140,200,60);//設置兩個標籤
		add(label);
		add(label1);//加入兩個標籤
	}
	
	public void paintComponent(Graphics G)
	{
		super.paintComponent(G);
		
		int[]x = {70,10,70,130,190,130};
		int[]y = {10,55,100,100,55,10};//設置六邊形的六個頂點
		G.setColor(Color.BLUE);//誰為藍色
		G.fillPolygon(x, y, 6);//多變形開始繪圖
	}

}
