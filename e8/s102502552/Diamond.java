package ce1002.e8.s102502552;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Diamond extends JPanel{
	
	public Diamond()
	{
		setBounds(630,0,200,200);
		setBorder(new LineBorder(Color.BLACK,3));//邊框
		setLayout(null);
		JLabel label = new JLabel("Graph : Diamond");
		label.setBounds(50,120,200,20);
		JLabel label1 = new JLabel("a polygon with 5 edges,like a diamond");
		label1.setBounds(5,140,200,60);//設置兩個標籤
		add(label);
		add(label1);//加入兩個標籤
	}
	
	public void paintComponent(Graphics G)//繪圖
	{
		super.paintComponent(G);
		
		int[]x = {50,10,100,190,150};
		int[]y = {10,50,110,50,10};//設置鑽石的五個頂點
		G.setColor(Color.WHITE);//設為白色
		G.fillPolygon(x, y, 5);//多變性開始繪圖
	}

}
