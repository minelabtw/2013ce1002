package ce1002.e8.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Oval extends JPanel{
	
	Oval()//橢圓
	{
		setBounds(210,0,200,200);
		setBorder(new LineBorder(Color.BLACK,3));//邊框
		setLayout(null);//排版
		JLabel label = new JLabel("Graph : Oval");
		label.setBounds(65,120,200,20);
		JLabel label1 = new JLabel("a closed curve like an egg");
		label1.setBounds(25,140,200,60);//設置兩個標籤
		add(label);
		add(label1);//加入兩個標籤
	}
	
	public void paintComponent(Graphics G)
	{
		super.paintComponent(G);
		
		G.setColor(Color.GREEN);//設為綠色
		G.fillOval(50, 10, 100, 100);//畫出圓形
	}

}
