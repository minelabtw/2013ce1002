package ce1002.e8.s102502552;

import java.awt.GridLayout;
import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Graphics");
		frame.setSize(850, 240);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//建立視窗
		
		frame.setLayout(null);//四個物件一字排開
		
		RoundRectPanel panel1 = new RoundRectPanel();//園邊矩形物件
		Oval panel2 = new Oval();//橢圓物件
		Polygon panel3 = new Polygon();//六邊形物件
		Diamond panel4 = new Diamond();//鑽石物件
		frame.add(panel1);
		frame.add(panel2);
		frame.add(panel3);
		frame.add(panel4);//四個物件依序加入視窗

	}

}
