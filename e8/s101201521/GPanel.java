package ce1002.e8.s101201521;
import javax.swing.JPanel;
import javax.swing.border.*;
import java.awt.*;
public class GPanel extends JPanel{
	private int i;
	public GPanel(int i){
		this.i = i;
		setBorder(new LineBorder(Color.BLACK, 3));
		setPreferredSize(new Dimension(280,250));
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int w = getWidth();
		int h = getHeight();
		int gw = (int)(w*0.8);
		int gh = (int)(h*0.8);
		int ow = (int)(w/2.0);
		int oh = (int)(h/3.0);
		g.setFont(new Font("Dialog", Font.BOLD, 12));
		switch(i){
		case 0://honeycomb
			int x = (int)(w/2.0);
			int y = (int)(h/3.0);
			int r = (int)(h/15.0);
			//texts
			g.drawString("This is a hineycomb.", 
					ow-(int)(gw/2.0), (int)(h*3/4.0));
			g.drawString("Using hexgons to construct it.", 
					ow-(int)(gw/2.0), (int)(h*11/12.0));
			//graph
			for(int j = 0; j < 8; j++ ){
				for(int i = 0; i < 4; i++){
					int ygap = (int)(2*r * Math.cos(Math.PI/6.0));
					int xgap = (int)(r*(1+Math.sin(Math.PI/6)));
					if(j % 2 == 0)
						hexagon((int)(w/5.0)+xgap*j,(int)(h/6.0) + ygap*i, r, h, g);
					else
						hexagon((int)(w/5.0)+xgap*j,(int)(h/6.0) + ygap*i + ygap/2, r, h, g);
				}
			}
			break;
		case 1://sin(x)
			Polygon pSin = new Polygon();
			//drawing axises
			g.drawLine(ow-(int)(gw/2.0), oh,ow+(int)(gw/2.0),oh);
			g.drawLine(ow+(int)(gw/2.0), oh,ow+(int)(gw/2.0)-5,oh-5);
			g.drawLine(ow+(int)(gw/2.0), oh,ow+(int)(gw/2.0)-5,oh+5);
			g.drawLine(ow, oh+(int)(h/4.0), ow, oh-(int)(h/4.0));
			g.drawLine(ow, oh-(int)(h/4.0), ow-5, oh-(int)(h/4.0)+5);
			g.drawLine(ow, oh-(int)(h/4.0), ow+5, oh-(int)(h/4.0)+5);
			g.drawString("y", ow, (int)(h/20.0));
			g.drawString("x", (int)(w*19/20.0), oh);
			g.drawString("y = sin(x)", ow+(int)(gw/4.0), oh-(int)(h/8.0));
			//texts
			g.drawString("This is a graph of sin(x) from -2*pi to 2*pi.", 
						ow-(int)(gw/2.0), (int)(h*3/4.0));
			g.drawString("Using drawPolyline() in the class Graphics.", 
						ow-(int)(gw/2.0), (int)(h*11/12.0));
			//graph of sin(x)
			for(int i = (int)(-1*gw/3.0); i <= (int)(gw/3.0); i++){
				double xx = i*(2*Math.PI)/(gw/3.0);
				int yy = (int)(Math.sin(xx)*(h/4.0));
				pSin.addPoint(ow+i, oh-yy);
			}
			g.drawPolyline(pSin.xpoints, pSin.ypoints, pSin.npoints);
			
			break;
		}
	}
	private void hexagon(int x, int y, int r, int h, Graphics g){
		Polygon p = new Polygon();
		//adding points of hexagon
		p.addPoint(x+r, y);
		p.addPoint((int)(x+r*Math.cos(Math.PI/3)), (int)(y-r*Math.sin(Math.PI/3)));
		p.addPoint((int)(x-r*Math.cos(Math.PI/3)), (int)(y-r*Math.sin(Math.PI/3)));
		p.addPoint(x-r, y);
		p.addPoint((int)(x-r*Math.cos(Math.PI/3)), (int)(y+r*Math.sin(Math.PI/3)));
		p.addPoint((int)(x+r*Math.cos(Math.PI/3)), (int)(y+r*Math.sin(Math.PI/3)));
		//draw hexagon
		g.setColor(new Color(245,220,75));
		g.fillPolygon(p);
		g.setColor(Color.BLACK);
		g.drawPolygon(p);
	}
}
