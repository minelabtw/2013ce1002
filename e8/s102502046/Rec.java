package ce1002.e8.s102502046;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Rec extends JPanel{
	JLabel Graph = new JLabel(); //儲存文字
	JLabel info = new JLabel(); 
	
	Rec () {
		setSize(200,200); //設定大小
		setLayout(null); //設定排版方式
		setBorder(BorderFactory.createLineBorder(Color.BLACK,5));
		Graph.setText("Graph : Rectangle");
		info.setText("info : in Euclidean...");
		Graph.setSize(200,20); //設定Graph的大小
		Graph.setLocation(10,130); //設定Graph的位置
		info.setSize(200,20); //設定info的大小
		info.setLocation(10,150); //設定info的位置
		add(Graph);
		add(info);
	}
	
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		g.setColor(Color.BLUE); //更改顏色為藍色
		g.fillRect(10, 10 , 180, 80 ); 
		
	}
}
