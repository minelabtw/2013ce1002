package ce1002.e8.s102502046;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Polygon extends JPanel{
	JLabel Graph = new JLabel(); 
	JLabel info = new JLabel(); 
	
	Polygon () {
		setSize(200,200); //設定大小
		setLayout(null); //設定排版方式
		setBorder(BorderFactory.createLineBorder(Color.BLACK,5));
		Graph.setText("Graph : Polygon");
		info.setText("info : in geometry a polygon is ...");
		Graph.setSize(200,20); 
		Graph.setLocation(10,130); 
		info.setSize(200,20); 
		info.setLocation(10,150); 
		add(Graph);
		add(info);
	}
	
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		g.setColor(Color.BLUE); //更改顏色為藍色
		int x[] = {25, 65, 135, 175, 135, 65};
	    int y[] = {55, 15, 15, 55, 95, 95};     
	    g.fillPolygon(x, y, 6);
		
	}
}
