package ce1002.e8.s102502010;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;

public class MyPanel extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel info = new JLabel();
	
	MyPanel()
	{
		setLayout(null);
		setBounds(300,300,500,500);
		label.setBounds(100,170,100,20);
		info.setBounds(100,200,130,20);
		add(label);
		add(info);
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);  //drewpicture
		g.setColor(Color.green);
		g.fillRect(50,50,200,100);
		label.setText("Graph: Rectangle");
		info.setText("Info: I am Rectangle");
	}
	
}
