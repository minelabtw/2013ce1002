package ce1002.e8.s102502010;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame{
	
	MyPanel panel1 = new MyPanel();
	MyPanel2 panel2 = new MyPanel2();
	
	MyFrame()
	{
		setLayout(null);
		setBounds(300 , 300 , 610 , 300);
		setVisible(true);  //setVisible
		setDefaultCloseOperation(EXIT_ON_CLOSE);  //close
		paint(panel1,0,0);
		paint2(panel2,290,0);
	}
	
	public void paint(MyPanel panel, int x, int y)
	{
		panel.setBounds(x, y, 300 , 260);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel);  //add
	}
	public void paint2(MyPanel2 panel, int x, int y)
	{
		panel.setBounds(x, y, 300 , 260);
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(panel);  //add
	}
}
