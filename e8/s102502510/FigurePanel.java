package ce1002.e8.s102502510;

import javax.swing.*;
import java.awt.*;
public class FigurePanel extends JPanel{
	
	FigurePanel()
	{
		setLayout(null);
		JLabel j1=new JLabel("Rectangle");
		JLabel ja=new JLabel("<html>any quadrilateral with<br> four right angles</html>");
		JLabel j2=new JLabel("Oval");
		JLabel jb=new JLabel("<html>closed curve in a plane which \"loosely\"<br> resembles the outline of an egg.</html>");
		j1.setBounds(70, 100, 100, 50);
		ja.setBounds(50, 100, 900,100);//.setBounds(x,y,width,height);
		j2.setBounds(210,100, 50, 50);
		jb.setBounds(190,100,900,100);
		add(j1);
		add(ja);
		add(j2);
		add(jb);
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(50,50,100,50);
		g.setColor(Color.red);
		g.fillOval(200,50,50,50);
	}
}
