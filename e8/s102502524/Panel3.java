package ce1002.e8.s102502524;

import javax.swing.*;
import java.awt.*;

public class Panel3 extends JPanel {
	
	Panel3()
	{
		setBounds( 5 , 5 , 250 , 150);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
	}

	public void paintComponent(Graphics g) 				//ø��
	{ 
		int x[] = { 25, 225, 125};
		int y[] = { 80, 80, 10};
		
		g.setColor(Color.red);
		g.fillPolygon(x, y, 3);
		g.setColor(Color.black);
		g.drawString("Graph:Triangle", 35, 110);
		g.drawString("Info:Just a triangle.", 35, 130);
		
	}
}
