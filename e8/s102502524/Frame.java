package ce1002.e8.s102502524;

import java.awt.*;
import javax.swing.*;

public class Frame extends JFrame {

	Frame()
	{
		setVisible(true);								//框架可見
		setSize(785, 200);								//框架大小為770*190
		setDefaultCloseOperation(EXIT_ON_CLOSE);		//關閉後離開
		setLayout(null);				//將Panel排版
		
		Panel1 panel1 = new Panel1();					//把圖形放入面板裡
		panel1.setLocation(5, 5);
		add(panel1);									//加入的東西是Panel1裡全部的東西
		Panel2 panel2 = new Panel2();					//把圖形放入面板裡
		panel2.setLocation(260, 5);
		add(panel2);									//加入的東西是Panel2裡全部的東西
		Panel3 panel3 = new Panel3();					//把圖形放入面板裡
		panel3.setLocation(515, 5);
		add(panel3);									//加入的東西是Panel3裡全部的東西

	}
}
