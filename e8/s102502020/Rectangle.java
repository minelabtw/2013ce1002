package ce1002.e8.s102502020;

import javax.swing.*;
import java.awt.*;

public class Rectangle extends MyPanel{               //畫矩形
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(Color.BLUE);                       //藍色
		g.fillRect(30, 10, 190, 100);                 //填滿
		g.setColor(Color.BLACK);
		g.drawString("Graph: Rectengle", 10, 150);    //畫字串
	}
	
	

}
