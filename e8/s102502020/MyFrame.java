package ce1002.e8.s102502020;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame{
	
	protected MyPanel panel1 = new Rectangle();    //呼叫Rectangle
	protected MyPanel panel2 = new Oval();         //呼叫Oval
	
	public MyFrame(){
		setLayout(null);                           //使用預設的排版
		newRolePos(panel1, 10, 10);
		newRolePos(panel2, 270, 10);
	}
	
	public void newRolePos(MyPanel panel, int x, int y) {
		/* set panel's position and size */
		panel.setBounds(x, y, 250, 200);
		/* set panel's border */
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));   //加邊框
		/* set label which in the panel */
		add(panel);
	}

}
