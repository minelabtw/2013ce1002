package ce1002.e8.s102502018;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class Panel2 extends JPanel{

	protected JLabel label = new JLabel();
	protected JLabel label1 = new JLabel();
	public Panel2()
	{
		setLayout(null);
		setBounds(325,10,300,220);
		label.setBounds(20, 100, 100, 100);
		label.setText("Oval");
		add(label);		
		label1.setBounds(20, 120, 200, 100);
		label1.setText("Info:An Oval");
		add(label1);	
	}
	public void paintComponent(Graphics g)
	{	
		g.setColor(Color.YELLOW);
		g.fillOval(10, 20, 277, 100);	
	}
}
