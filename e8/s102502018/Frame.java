package ce1002.e8.s102502018;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
public class Frame extends JFrame{
	
	protected Panel1 panel1 = new Panel1();
	protected Panel2 panel2 = new Panel2();
	
	public Frame()
	{
		setLayout(null);
		setBounds(300, 50, 650, 280);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		add(panel1);
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		add(panel2);
	}
	
}
