package ce1002.e8.ta;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

class Polygons extends Graph{
	
	Polygons()
	{
		//Set name and info
		setGraphName("Polygon");
		setGraphInfo("In geometry a polygon is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit.");
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {	//Draw graph
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
	    Polygon polygon = new Polygon();
	    polygon.addPoint(100, 50);
	    polygon.addPoint(140, 50);
	    polygon.addPoint(160, 75);
	    polygon.addPoint(140, 100);
	    polygon.addPoint(100, 100);
	    polygon.addPoint(80, 75);
	    
	    g.drawPolygon(polygon);
	    g.setColor(Color.GREEN);
	    g.fillPolygon(polygon);

	}

}
