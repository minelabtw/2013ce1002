package ce1002.e8.ta;

import java.awt.Color;
import java.awt.Graphics;

class Rectangles extends Graph{

	Rectangles()
	{
		//Set name and info
		setGraphName("Rectangle");
		setGraphInfo("In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.");
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {	//Draw graph
		// TODO Auto-generated method stub
		super.paintComponent(g);

		g.drawRect(25, 10, 200, 100);
		g.setColor(Color.BLUE);
		g.fillRect(25, 10, 200, 100);

	}



}
