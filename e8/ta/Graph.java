package ce1002.e8.ta;

import javax.swing.JLabel;
import javax.swing.JPanel;

class Graph extends JPanel{
	private String graphName;
	private String graphInfo;

	//new JLabel
	JLabel nameLabel = new JLabel();
	JLabel infoLabel = new JLabel();
	
	Graph()
	{
		setLayout(null);
		
		//Set label
		nameLabel.setBounds(10, 120, 100, 40);
		infoLabel.setBounds(10, 140, 180, 40);
		
		//Add label
		this.add(nameLabel);
		this.add(infoLabel);
	}
	
	String getGraphName() {
		return graphName;
	}
	void setGraphName(String graphName) {
		this.graphName = graphName;
		//Set nameLabel's text
		this.nameLabel.setText("Graph: " + graphName);
	}
	String getGraphInfo() {
		return graphInfo;
	}
	void setGraphInfo(String graphInfo) {
		this.graphInfo = graphInfo;
		//Set infoLabel's text
		this.infoLabel.setText("Info: " + graphInfo);
	}
	

}
