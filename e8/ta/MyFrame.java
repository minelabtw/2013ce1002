package ce1002.e8.ta;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	Graph rectPanel = new Rectangles(); 
	Graph ovalPanel = new Ovals();
	Graph polygonPanel = new Polygons();

	MyFrame() {
		setLayout(null);
		setBounds(0, 0, 810, 260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//new Panel
		newGraphPanel(rectPanel,10,10);
		newGraphPanel(ovalPanel,270,10);
		newGraphPanel(polygonPanel,530,10);
	}
	
	void newGraphPanel(Graph panel, int x, int y) {
		/* set panel's position and size */
		panel.setBounds(x, y, 250, 200);
		/* set panel's border */
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		/* add panel to frame */
		add(panel);
	}

}
