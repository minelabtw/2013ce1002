package ce1002.e8.ta;

import java.awt.Color;
import java.awt.Graphics;

class Ovals extends Graph{
	
	Ovals()
	{
		//Set name and info
		setGraphName("Oval");
		setGraphInfo("An oval (from Latin ovum, \"egg\") is a closed curve in a plane which \"loosely\" resembles the outline of an egg.");
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {	//Draw graph
		// TODO Auto-generated method stub
		super.paintComponent(g);

		g.drawOval(25, 10, 200, 100);
		g.setColor(Color.YELLOW);
		g.fillOval(25, 10, 200, 100);

	}

}
