package ce1002.e8.s995002014;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Polygon;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	public MyFrame() {
		setLayout(new GridLayout(1,3,100,100));
		
		//加入三個JPanel
		add(new MyRec());
		add(new MyOval());
		add(new MyPoly());
		
		setSize(800, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}

//畫橢圓形
class MyOval extends JPanel {
	public MyOval(){
		setBorder(BorderFactory.createLineBorder(Color.black)); //黑色邊框
	}
	
	//畫橢圓
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.fillOval(50, 50, 100, 60);
		g.setColor(Color.black);
		g.drawOval(50, 50, 100, 60);
		g.drawString("Graph: Oval", 5, 150); //文字
		g.drawString("Info: An oval",5,165);
	}
}

class MyRec extends JPanel {
	public MyRec(){
		setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	//畫長方形
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.drawRect(50, 50, 100, 60);
		g.setColor(Color.blue);
		g.fillRect(50, 50, 100, 60); 
		g.setColor(Color.black); 
		g.drawString("Graph: Rectangle", 5, 150); //文字
		g.drawString("Info: In Euclidean plane",5,165);
	}
}

class MyPoly extends JPanel {
	public MyPoly(){
		setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	//畫多邊形
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		int xCenter=getSize().width/2;
		int yCenter=getSize().height/3;
		int radius = 50;
		
		Polygon polygon = new Polygon(); //多邊形物件
		
		//加入六個點
		polygon.addPoint(xCenter+radius, yCenter);
		polygon.addPoint((int)(xCenter+radius*Math.cos(2*Math.PI/6)), (int)(yCenter+radius*Math.sin(2*Math.PI/6)));
		polygon.addPoint((int)(xCenter+radius*Math.cos(2*2*Math.PI/6)), (int)(yCenter+radius*Math.sin(2*2*Math.PI/6)));
		polygon.addPoint((int)(xCenter+radius*Math.cos(3*2*Math.PI/6)), (int)(yCenter+radius*Math.sin(3*2*Math.PI/6)));
		polygon.addPoint((int)(xCenter+radius*Math.cos(4*2*Math.PI/6)), (int)(yCenter+radius*Math.sin(4*2*Math.PI/6)));
		polygon.addPoint((int)(xCenter+radius*Math.cos(5*2*Math.PI/6)), (int)(yCenter+radius*Math.sin(5*2*Math.PI/6)));
		
		//畫出
		g.drawPolygon(polygon);
		g.setColor(Color.green);
		g.fillPolygon(polygon);
		
		g.setColor(Color.black); 
		g.drawString("Graph: Polygon", 5, 150);
		g.drawString("Info: In geometry a polygon is",5,165);
	}
}