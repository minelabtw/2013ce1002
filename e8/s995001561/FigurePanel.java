package ce1002.e8.s995001561;

//import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class FigurePanel extends JPanel {
	
	// create varialbes
	public static final int RECTANGLE = 2;
	public static final int ROUND_RECTANGLE = 3;
	
	private int type = 1;
	private boolean filled = false;
	
	JLabel status = new JLabel();
	JLabel status2 = new JLabel();
	
	// Create function
	public FigurePanel(){
		
		
	}
	
	public FigurePanel(int type) {
		this.type = type;
	}
	
	public FigurePanel(int type, boolean filled) {
		this.type = type;
		this.filled = filled;
		this.add(status);
		this.add(status2);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));

		
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Get the appropriate size for the figure
		int width = getWidth();
		int height = getHeight();
		
		switch (type) {
		case RECTANGLE:	// Display a rectangle
			g.setColor(Color.BLUE);
			if (filled) {
				g.fillRect((int)(0.1 * width), (int)(0.1 * height), (int)(0.8 * width), (int)(0.5 * height));
				status.setText("Graphs: "+FigurePanel.gettype2());
				status.setBounds(20, 120, 400, 225);
				status2.setText("Info: "+FigurePanel.infro2());
				status2.setBounds(20, 150, 300, 225);
			}
			break;
		
		case ROUND_RECTANGLE: // Display a round-cornered rectangle
			g.setColor(Color.GREEN);
			if (filled) {
				g.fillRoundRect((int)(0.1 * width), (int)(0.1 * height), (int)(0.8 * width), (int)(0.5 * height), 20, 20);
				status.setText("Graphs: "+FigurePanel.gettype3());
				status.setBounds(20, 120, 400, 225);
				status2.setText("Info: "+FigurePanel.infro3());
				status2.setBounds(20, 150, 300, 225);
			}
			break;
		}
	}
	
	/** Set a new figure type */
	public void setType(int type) {
		this.type = type;
		repaint();
	}
	
	/** Return figure type */
	public int getType() {
		return type;
	}
	
	/** Set a new filled property */
	public void setFilled(boolean filled) {
		this.filled = filled;
        repaint();
	}
	
	/** Check if the figure is filled */
	public boolean isFilled() {
		return filled;
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(80, 80);
	}
	
	// get type and information
	public static String gettype2(){
		return "RECTANGLE";
		
	}
	
	public static String gettype3(){
		return "ROUND_RECTANGLE";
		
	}
	
	public static String infro2(){
		return "In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.";
		
	}
	
	public static String infro3(){
		return "A rounded rectangle is the shape obtained by taking the convex hull of four equal circles of radius r and placing their centers at the four corners of a rectangle with side lengths a and b. ";
		
	}
	
	
	
}
