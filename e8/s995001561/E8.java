package ce1002.e8.s995001561;

import java.awt.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class E8 extends JFrame{
	
	// setLayout
	public E8() {
		setLayout(new GridLayout(1,2,5,5));
		add(new FigurePanel(FigurePanel.RECTANGLE, true));
		add(new FigurePanel(FigurePanel.ROUND_RECTANGLE, true));
	    	
	}
	
	// set Frame
	public static void main(String[] args) {
		E8 frame = new E8();
		frame.setSize(800, 350);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
