package ce1002.e8.s102502562;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame()
	{
		setLayout(null);
		setSize(960,260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		newPaint();
	}
	public void newPaint()
	{
		Panel1 panel1=new Panel1();
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		this.add(panel1);
		Panel2 panel2=new Panel2();
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		this.add(panel2);
		Panel3 panel3=new Panel3();
		panel3.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		this.add(panel3);
	}
}
