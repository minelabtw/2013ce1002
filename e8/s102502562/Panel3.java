package ce1002.e8.s102502562;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Panel3 extends JPanel{
	Panel3()
	{
		setBounds(630,10,300,200);
		setLayout(null);
	}
	public void paintComponent(Graphics g)
	{   
		g.setColor(Color.GREEN);
		int x[]={50,80,20};
		int y[]={30,20,30};
		g.fillPolygon(x,y,3);
		JLabel jlabel1=new JLabel();
		jlabel1.setBounds(10,115,220,100);
		jlabel1.setText("Graph: Polygon");
		add(jlabel1);
		JLabel jlabel2=new JLabel();
		jlabel2.setBounds(10,170,220,20);
		jlabel2.setText("Info: In geometry a polygon is t...");
		add(jlabel2);
	}
}
