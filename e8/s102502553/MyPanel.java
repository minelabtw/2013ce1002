package ce1002.e8.s102502553;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	private  int a;
	
	MyPanel(int i)
	{
		a = i;
		setSize(200,300);
		setLayout(null);
		
		JLabel l1 = new JLabel();
		JLabel l2 = new JLabel();
		
		if(i==0)//長方形的標籤
		{
			l1 = new JLabel("Graph:Rectangle");
		    l2 = new JLabel("Info:InEuclidean plane geome...");
		}
		else if(i==1)//橢圓的標籤
		{
			 l1 = new JLabel("Graph:Oval");
			 l2 = new JLabel("Info:An oval(from Latin ovum,)...");
		}
		else//多飆行的標籤
		{
			l1 = new JLabel("Graph:polygon");
			l2 = new JLabel("Info:In geometry a polygon is t...");
		}
		
		l1.setBounds(10,170,180,10);//(10,170)為起始點，寬180，高10
		l2.setBounds(10,190,180,10);
		
		setBorder(new LineBorder(Color.BLACK,3));//邊界
		
		add(l1);
		add(l2);
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(a==0)//畫長方形
		{
			g.setColor(Color.BLUE);
			g.fillRect(10,10,180,120);
		}
		else if(a==1)//畫橢圓
		{
			g.setColor(Color.GREEN);
			g.fillOval(10,10,180,120);
		}
		else//畫多飆行
		{
			int x[] = {60,120,150,120,60,30};
			int y[] = {50,50,92,134,134,92};
			g.setColor(Color.YELLOW);
			g.fillPolygon(x,y,6);
		}
	}

}
