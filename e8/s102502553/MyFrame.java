package ce1002.e8.s102502553;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{

	MyFrame()
	{
		setSize(718,300);
		
		setLayout(new GridLayout(1,3));//建3個面板
		
		MyPanel p1 = new MyPanel(0);
		MyPanel p2 = new MyPanel(1);
		MyPanel p3 = new MyPanel(2);
		
		add(p1);
		add(p2);
		add(p3);
		
		setVisible(true);//貼上再顯示
	}
}
