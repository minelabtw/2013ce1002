package ce1002.e8.s102502520;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Oval extends JPanel{
	public Oval() {
		setBounds(250,5,240,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}
 
	
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		g.fillOval(10,10,230,150);
		g.setColor(Color.DARK_GRAY);
		g.drawString("Graph: Oval.", 10, 180);
        g.drawString("Info: an oval...", 10, 190);
		
	}
	
}