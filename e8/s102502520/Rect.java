package ce1002.e8.s102502520;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class Rect extends JPanel{
	public Rect() {
		setBounds(5,5,240,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}
 
	
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.fillRect(10, 10, 120, 100);
		g.setColor(Color.CYAN);
		g.drawString("Graph: Rectangle.", 10, 150);
        g.drawString("Info: a rectangle...", 10, 160);
		
	}
	 
}
