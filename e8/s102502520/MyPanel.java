package ce1002.e8.s102502520;
import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	
	protected JLabel label =new JLabel();
	protected JLabel label1 = new JLabel();
	
	public MyPanel(){
		setLayout(null);
		setBounds(5,0,500,100);
	}
	protected void paintComponent(Graphics g) {
		int[] x={10,120,230};
		int[] y={120,10,120};
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		g.fillPolygon(x, y, 3);
		g.setColor(Color.BLACK);
		g.drawString("Graph: Tangle.", 10, 150);
        g.drawString("Info: a tangle...", 10, 160);
		
	}


}
