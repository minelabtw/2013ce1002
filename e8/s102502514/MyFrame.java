package ce1002.e8.s102502514;

import java.awt.*;

import javax.swing.*;

public class MyFrame extends JFrame {
	
	protected Rectangle rect= new Rectangle();
	protected Oval oval= new Oval();
	protected Polygon poly = new Polygon();
	
	MyFrame() {
		setLayout(null);
		setBounds(0, 0, 810, 260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		rect.setBounds(10 , 10 , 250 , 200);
		rect.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(rect);
		
		oval.setBounds(270 , 10 , 250 , 200);
		oval.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(oval);
		
		poly.setBounds(530 , 10 , 250 , 200);
		poly.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(poly);
	}
}
