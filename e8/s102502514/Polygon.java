package ce1002.e8.s102502514;

import java.awt.*;

import javax.swing.*;

public class Polygon extends JPanel{
	protected JLabel graph = new JLabel("Graph: Polygon");
	protected JLabel info = new JLabel("Info: In geometry a polygon is traditionally a plane figure that is bounded by a finite chain of straight line segments closing in a loop to form a closed chain or circuit.");
	Polygon(){
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		add(graph);
		add(info);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int x [] = {95,155,185,155,95,65};
	    int y [] = {20,20,70,120,120,70};
	    g.setColor(Color.GREEN);		
		g.fillPolygon(x,y,6);
	}
}
