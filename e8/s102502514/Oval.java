package ce1002.e8.s102502514;

import java.awt.*;

import javax.swing.*;

public class Oval extends JPanel{
	JLabel graph = new JLabel("Graph: Oval");
	JLabel info = new JLabel("Info: In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.");
	Oval(){
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		add(graph);
		add(info);
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);		
		g.setColor(Color.RED);
		g.fillOval(50,20,150,100);
	}
}
