package ce1002.e8.s102502515;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame {

	protected MyPanel panel1 = new MyPanel(1);//宣告panel
	protected MyPanel panel2 = new MyPanel(2);
	protected MyPanel panel3 = new MyPanel(3);
	
		MyFrame(){//設定frame的大小位置、和各panel的大小位置
			setLayout(null);
			setBounds(100, 100, 1000, 400);
			setVisible(true);
			setDefaultCloseOperation(EXIT_ON_CLOSE);

			panel1.setBounds(20, 20, 300, 300);
			panel2.setBounds(340, 20, 300, 300);
			panel3.setBounds(660, 20, 300, 300);
			panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
			panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
			panel3.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
			add(panel1);
			add(panel2);
			add(panel3);

			}

	}
		
