package ce1002.e8.s102502515;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected int i ;
	JLabel jlbl1 = new JLabel("");
	JLabel jlbl2 = new JLabel("");
	
	MyPanel(int i){
		this.i = i;//i for each panel
		//setLayout(null);
		jlbl1.setLocation(40, 200);
		jlbl2.setBounds(20,360,70,70);
		add(jlbl1);
		add(jlbl2);
	}
	
	@Override
	protected void paintComponent(Graphics g){//draw pics by i
		super.paintComponent(g);
		
		if (i == 1){
			g.setColor(Color.red);
			g.fillRect(50,50,200,150);
			jlbl1.setText("Graph: Rectangle");
			jlbl2.setText("Info: ");		
		}
		else if (i == 2){
			g.setColor(Color.yellow);
			g.fillOval(50, 50, 200, 150);
			jlbl1.setText("Graph: Oval");
			jlbl2.setText("Info: ");
		}
		else{
			g.setColor(Color.CYAN);
			int x[] = {95,120,170,195,170,120};
			int y[] = {150,100,100,150,200,200};
			g.fillPolygon(x, y, 6);
			jlbl1.setText("Graph: Polygon");
			jlbl2.setText("Info: ");
		}	
	}
}
