package ce1002.e8.s101201519;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
public class MyPanel extends JPanel{
	private int i;

	public MyPanel(int i)
	{
		this.i=i;
		setBorder(new LineBorder(Color.BLACK,3));
		setPreferredSize(new Dimension(280,250));
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(i==0)
		{
			g.setColor(Color.GREEN);
			g.fillRoundRect(40, 20, 200, 150, 20, 20);
			g.setColor(Color.BLACK);
			g.drawString("Graph: RoundRectangle", 40, 200);
			g.drawString("Test 1", 40, 220);
		}
		else if(i==1)
		{
			g.setColor(Color.BLUE);
			g.fillArc(65, 20, 150, 150, 0, 60);
			g.fillArc(65, 20, 150, 150, 90, 60);
			g.fillArc(65, 20, 150, 150, 180, 60);
			g.fillArc(65, 20, 150, 150, 270, 60);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Arc", 40, 200);
			g.drawString("Test 2", 40, 220);
		}
		else
		{
			int x[]={140,35,250};
			int y[]={160,35,35};
			g.setColor(Color.RED);
			g.fillPolygon(x,y,3);
			g.setColor(Color.BLACK);
			g.drawString("Graph: Triangle", 40, 200);
			g.drawString("Test 3", 40, 220);
		}
	}

}
