package ce1002.e8.s101201519;

import javax.swing.JFrame;
import java.awt.*;
public class E8 {
	public static void main(String[] args)
	{
		JFrame frame = new JFrame("E8");
		frame.setBounds(0,0,950,300);
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		MyPanel[] image = new MyPanel[3];
		for(int i=0;i<3;i++)
		{
			image[i]= new MyPanel(i);
			frame.add(image[i]);
		}
	}
}
