package ce1002.e8.s102502041;
import javax.swing.ImageIcon;
public class Graph {
	protected String name;
	protected String description;
	protected ImageIcon image;
	public void setName(String a)
	{
		name = a;
	}
	public void setDescription(String d)
	{
		description = d;
	}
	public String getName()
	{
		return name;
	}
	public String getDescription()
	{
		return description;
	}

}
