package ce1002.e8.s102502041;
import javax.swing.*;
public class MyFrame extends JFrame{
	protected MyPanel panel1 = new MyPanel();
	MyFrame(Graph[] g)
	{
		setLayout(null);
		setVisible(true);
		setSize(555,480);
		setPanelState(20, 20, g[0], panel1);
	}
	
	public void setPanelState(int x, int y, Graph g, MyPanel panel)
	{
		panel.setGraphState(x, y, g);
		add(panel);
	}
}
