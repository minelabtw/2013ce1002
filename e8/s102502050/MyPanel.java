package ce1002.e8.s102502050;

import javax.swing.*;
import java.awt.*;
public class MyPanel extends JPanel{
	protected JLabel label1,label2,label3,label4;
	public MyPanel()
	{
		setLayout(null);
		label1 = new JLabel("Graph: Rainbow");
		label2 = new JLabel("Info: 7 arc");
		label3 = new JLabel("Graph: Star");
		label4 = new JLabel("Info: A polygon");
		
		label1.setBounds(100,300,290,20);
		label2.setBounds(100,320,290,20);
		label3.setBounds(400,300,290,20);
		label4.setBounds(400,320,290,20);
		
		add(label1);
		add(label2);
		add(label3);
		add(label4);
		setVisible(true);
	}
	public void paintComponent(Graphics g) {
		   super.paintComponent(g);

		   Color color = new Color(128,0,255);
		   Color background = new Color(238,238,238);
		   Color red2 = new Color(183,21,21);
		   g.setColor(Color.RED);
		   g.fillArc(0,0,300,300,0,180);
		   g.setColor(Color.ORANGE);
		   g.fillArc(10,10,280,280,0,180);
		   g.setColor(Color.YELLOW);
		   g.fillArc(20,20,260,260,0,180);
		   g.setColor(Color.GREEN);
		   g.fillArc(30,30,240,240,0,180);
		   g.setColor(Color.BLUE);
		   g.fillArc(40,40,220,220,0,180);
		   g.setColor(color);
		   g.fillArc(50,50,200,200,0,180);
		   g.setColor(background);
		   g.fillArc(60,60,180,180,0,180);
		   //rainbow
		   //use fillArc 7 times
		   	   
		   int []xpoint = {310,590,350,450,550};
		   int []ypoint = {120,120,300,10,300};
		   g.setColor(red2);
		   g.fillPolygon(xpoint,ypoint,5);
		   g.setColor(Color.BLACK);
		   g.drawPolygon(xpoint,ypoint,5);
		   //star
		   
		   
	} 

}
