package ce1002.e8.s100204006;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class MyFrame extends JFrame
{
	 private Panela panel1;
	 private Panelb panel2;
	 
	

	 class Panela extends JPanel 
	 {
		 Panela() 
		 {
            // set a preferred size for the custom panel.
			 setPreferredSize(new Dimension(420,420));
		 }
		 
		 @Override 
		 public Dimension getPreferredSize() 
		 { 
			 return new Dimension(400, 300); 
		 }

		 public void paintComponent(Graphics g) 
		 {
			 super.paintComponent(g);
         
             //�����
			 g.drawRect(180,170,50,50);  
			 g.setColor(Color.RED);  
			 g.fillRect(180,170,50,50);
			 
			 
			 g.drawString("Graph:Rectangle", 180, 300);
	         g.drawString("Info:An oval (Latin ovum,...", 180, 320);
			 
			 
		 }
	 }
	 
	 
	 class Panelb extends JPanel 
	 {
		 Panelb() 
		 {
            // set a preferred size for the custom panel.
			 setPreferredSize(new Dimension(420,420));
		 }
		 
		 @Override 
		 public Dimension getPreferredSize() 
		 { 
			 return new Dimension(400, 300); 
		 }

		 public void paintComponent(Graphics g) 
		 {
			 super.paintComponent(g);
         
			// ����
			 g.drawOval(180,170,50,50);  
			 g.setColor(Color.BLUE);  
			 g.fillOval(180,170,50,50);
			 
			 g.drawString("Graph:Oval", 180, 300);
	         g.drawString("Info:In Euclidean plane genome...", 180, 320);
			 
		 }
	 }
	 
	 
	 
	 public MyFrame() 
	 {
		 this.setLayout(null);
		 newRolePos(0,0);
	 }
	 public void newRolePos(int x, int y) 
	 {
		 panel1 = new Panela();
		 panel1.setLayout(null);
		 panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		 panel1.setBounds(x, y, 406, 406);
		 
		 
		 panel2 = new Panelb();
		 panel2.setLayout(null);
		 panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		 panel2.setBounds(406, y, 407, 406);
		 
		 this.add(panel1);
		 this.add(panel2);
	 } 
}
