package ce1002.e8.s102502525;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends JPanel{
	protected JLabel graph = new JLabel("Graph: Rectangle");
	protected JLabel info = new JLabel("Info: 國防布.");
	
	Rectangle() {
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		add(graph);
		add(info);
	}
	
	protected void paintComponent(Graphics r) {
		super.paintComponent(r);
		r.setColor(Color.BLACK);
		r.fillRect(50,20,150,100); //矩形是內建的
	}
	
}
