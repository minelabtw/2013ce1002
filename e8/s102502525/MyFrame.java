package ce1002.e8.s102502525;

import javax.swing.*;

import java.awt.Color;

public class MyFrame extends JFrame{
	
	protected Oval oval= new Oval();
	protected Rectangle rect= new Rectangle();
	protected Triangle tri = new Triangle();
	
	MyFrame() {
		
		setLayout(null);
		setBounds(0 , 0 , 810 , 260);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//設定panel的位置與大小
		oval.setBounds(10 , 10 , 250 , 200);
		//設定panel的邊框
		oval.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		//把panel放進frame裡
		add(oval);
		
		rect.setBounds(270 , 10 , 250 , 200);  //同oval
		rect.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(rect);
		
		tri.setBounds(530 , 10 , 250 , 200);   //同oval
		tri.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(tri);
	}
}
