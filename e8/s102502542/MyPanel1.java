package ce1002.e8.s102502542;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel1 extends JPanel 
{
	JLabel label = new JLabel("Graph: Oval");
	JLabel label1 = new JLabel("Info: An oval(from La....)");
	MyPanel1()
	{
		setSize(250,180);
		setLocation(270,10);//設定panel1座標
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK,3));
		label.setBounds(15,60,100,100);
		label1.setBounds(15,75,100,100);
		this.add(label);
		this.add(label1);
	}
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.BLUE);
		g.fillOval(50,30,150,50);
		setVisible(true);
	}
}

