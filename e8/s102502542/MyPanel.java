package ce1002.e8.s102502542;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel 
{
	JLabel label = new JLabel("Graph: Rectangle");
	JLabel label1 = new JLabel("Info: In Euclidean plane geome...");
	MyPanel()
	{
		setSize(250,180);
		setLocation(10,10);//設定panel座標
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK,3));
		label.setBounds(15,60,100,100);//設定label座標
		label1.setBounds(15,75,100,100);
		this.add(label);
		this.add(label1);
	}
	public void paintComponent(Graphics g)//繪圖函式
	{
		g.setColor(Color.RED);
		g.fillRect(30,15,180,80);
		setVisible(true);
	}
}
