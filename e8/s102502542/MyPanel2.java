package ce1002.e8.s102502542;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel2 extends JPanel 
{
	JLabel label = new JLabel("Graph: Polygon");
	JLabel label1 = new JLabel("Info: In geometry a polygon...");
	MyPanel2()
	{
		setSize(250,180);
		setLocation(530,10);//設定panel2座標
		setLayout(null);
		setBorder(new LineBorder(Color.BLACK,3));
		label.setBounds(15,60,100,100);
		label1.setBounds(15,75,100,100);
		this.add(label);
		this.add(label1);
	}
	public void paintComponent(Graphics g)
	{
		int xpoints[]={100,80,100,140,160,140};
		int ypoints[]={80,60,40,40,60,80};
		int npoints=6;
		g.setColor(Color.GREEN);
		g.fillPolygon(xpoints,ypoints,npoints);
		setVisible(true);
	}
}
