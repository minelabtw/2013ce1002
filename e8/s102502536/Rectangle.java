package ce1002.e8.s102502536;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle extends JPanel {
	
	protected JLabel graph = new JLabel("Graph: Rectangle");
	protected JLabel info = new JLabel("Info: In Euclidean plane geometry, a rectangle is any quadrilateral with four right angles.");
	
	Rectangle() {
		
		/*set panel's size and layout*/
		setLayout(null);
		graph.setBounds(10,130,220,30);
		info.setBounds(10,150,240,30);
		/*add label to panel*/
		add(graph);
		add(info);
	
	}
	
	protected void paintComponent(Graphics r) {
		// draw a rectangle which is filled with blue color
		super.paintComponent(r);
		r.setColor(Color.BLUE);
		r.fillRect(50,20,150,100);
		
		
	}

}
