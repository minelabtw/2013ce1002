package ce1002.e8.s102502028 ;
import java.awt.* ;
import javax.swing.* ;
import javax.swing.border.LineBorder;
public class E8 {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int type = 0 ;
		
        JFrame frame = new JFrame() ; //設定框架
        frame.setSize(800,350) ;
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
        frame.setLocationRelativeTo(null) ;
		frame.setLayout(null) ;
		
		RecPanel panel = new RecPanel() ; //加入長方形panel
		frame.add(panel) ;
		OvalPanel panel2 = new OvalPanel() ; //加入橢圓形panel
		frame.add(panel2) ;
	       
        frame.setVisible(true) ;
	}
	
}

class RecPanel extends JPanel{
	RecPanel() //建構子設定panel的位置 大小 及 加入元件
	{
		setLayout(null) ;
		setBounds(15,15,350,200) ;
		setBorder(new LineBorder(Color.black, 5)) ;
		JLabel label = new JLabel ("Info : The rectangle is defined as having all four interior angles 90° (right angles).") ;
		label.setBounds(15,150,300,50) ;
		add(label) ;
		JLabel label2 = new JLabel("Graph : Rectangle ") ;
		label2.setBounds(15,140,300,15) ;
		add(label2) ;
	}
	
	protected void paintComponent(Graphics g) //畫長方形
	{
		super.paintComponent(g) ;
		g.setColor(Color.BLUE) ;
		g.fillRect(50, 20,220, 100) ;
	}

}

class OvalPanel extends JPanel{
	OvalPanel() //建構子設定panel的位置 大小 及 加入元件
	{
		setLayout(null) ;
		setBounds(400,15,350,200) ;
		setBorder(new LineBorder(Color.black, 5)) ;
		JLabel label = new JLabel ("Info : A body or figure in the shape of an egg, or popularly, of an ellipse.") ;
		label.setBounds(15,150,220,50) ;	
		add(label) ;
		JLabel label2 = new JLabel("Graph : Oval ") ;
		label2.setBounds(15,140,300,15) ;
		add(label2) ;
	}
	
	protected void paintComponent(Graphics g)  //畫橢圓形
	{
		super.paintComponent(g) ;
		g.setColor(Color.ORANGE) ;
		g.fillOval(50, 20,220, 100) ;	
		g.setColor(Color.BLACK) ;
		g.drawOval(50,20, 220, 100) ;
	}
		
}
