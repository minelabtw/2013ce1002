package ce1002.a8.s101201522;

import java.awt.*;
import javax.swing.*;

public class Graph extends JPanel {
	private double[] level;//rate of each level
	
	Graph () {//initial
		level = new double[0];
	}
	
	Graph (double[] level) {//constructor with parameter
		this.level = level;
		setBounds(0,0,300,200);
	}
	
	protected void paintComponent (Graphics g) {//paint bar chart
		super.paintComponent(g);
		g.setFont(new Font("dialog", Font.BOLD, 12));
		for (int i=0;i<level.length;i++) {
			//paint bar
			g.setColor(Color.red);
			g.fill3DRect(10+50*i, 10, 10, 100, true);
			g.setColor(Color.gray);
			g.fillRect(10+50*i, 10, 9, (int)(100-100*level[i])-1);
			//write information
			g.setColor(Color.darkGray);
			g.drawString("Level "+i, 10+50*i, 125);
			g.drawString(level[i]*100+"%", 10+50*i, 145);
		}
	}
}
