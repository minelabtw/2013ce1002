package ce1002.e8.s101201046;

import javax.swing.*;
import java.lang.Math;
import java.awt.*;

public class Graph extends JPanel {
	
	private int type = 0;//graph type
	
	Graph (int type) {//constructor
		this.type = type;
		//set panel's size, position and border	
		setBounds(10+310*(type-1),10,300,200);
		this.setBorder(BorderFactory.createLineBorder(Color.black,3));
	}
	
	protected void paintComponent (Graphics g) {
		super.paintComponent(g);
		g.setFont(new Font("dialog", Font.BOLD, 12));
		if (type == 1) {//draw first graph : heart
			g.setColor(Color.red);
			g.fillArc(140, 30, 80, 50, 0, 180);
			g.fillArc(80, 30, 80, 50, 0, 180);
			g.fillArc(80, -10, 140, 130, 180, 180);
			g.setColor(Color.darkGray);
			g.setFont(new Font("dialog", Font.BOLD, 12));
			g.drawString("Graph: Heart", 10, 150);
			g.drawString("info: Use three arcs to draw a heart ...", 10, 170);
		}
		else if (type == 2) {//draw second graph : flower 
			double r = 8;
			int k = 5;
			int[] x = new int[700];
			int[] y = new int[700];
			
			for (int i = 0; i < 700; i++) {
				x[i] = (int) (r*(k+1)*Math.cos(Math.PI*2/700.0*i) - r*Math.cos((k+1)*Math.PI*2/700.0*i)) + 150;
				y[i] = (int) (r*(k+1)*Math.sin(Math.PI*2/700.0*i) - r*Math.sin((k+1)*Math.PI*2/700.0*i)) + 70;
			}
			g.setColor(Color.yellow);
			g.fillPolygon(x, y, 700);
			g.setColor(Color.orange);
			g.fillOval(130, 50, 40, 40);
			g.setColor(Color.darkGray);
			g.drawString("Graph: Flower", 10, 150);
			g.drawString("info: Use polygon with 700 points to draw an", 10, 170);
			g.drawString("Epicycloid graph ...", 37, 190);
		}
		else if (type == 3) {//draw third graph : butterfly
			double r = 8,t;
			int k = 5;
			int[] x = new int[1500];
			int[] y = new int[1500];
			
			for (int i = 0; i < 1500; i++) {
				t = 24*Math.PI/1500*i;
				x[i] = (int) (20*Math.sin(t)*(Math.exp(Math.cos(t))-2*Math.cos(4*t)+Math.pow(Math.sin(t/12),5))) + 150;
				y[i] = (int) (20*Math.cos(t)*(Math.exp(Math.cos(t))-2*Math.cos(4*t)+Math.pow(Math.sin(t/12),5))) + 70;
			}
			g.setColor(Color.green);
			g.fillOval(145, 60, 10, 50);
			g.setColor(Color.cyan);
			g.fillPolygon(x, y, 1500);
			g.setColor(Color.black);
			g.drawArc(68, 20, 80, 80, 0, 60);
			g.drawArc(152, 20, 80, 80, 120, 60);
			g.setColor(Color.darkGray);
			g.drawString("Graph: Butterfly", 10, 150);
			g.drawString("info: Use polygon with 1500 points to draw a", 10, 170);
			g.drawString("Butterfly graph ...", 37, 190);
		}
	}
}