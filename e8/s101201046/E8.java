package ce1002.e8.s101201046;

import javax.swing.*;

public class E8 {

	public static void main(String[] args) {
		//set frame's size, position, ... and add three graphs to the frame
		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setBounds(0,0,1000,300);
		frame.add(new Graph(1));
		frame.add(new Graph(2));
		frame.add(new Graph(3));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
