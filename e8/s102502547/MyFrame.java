package ce1002.e8.s102502547;

import javax.swing.*;

public class MyFrame extends JFrame {

	Rectangle a = new Rectangle();
	Circle b = new Circle();

	MyFrame() {
		setLayout(null); // 設定各物件在視窗中的布局方式
		setSize(650, 350); // 設定視窗大小
		setLocationRelativeTo(null); // 設定視窗位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 按視窗右上角的"X"會關閉程序

		add(a);
		add(b);
	}

}