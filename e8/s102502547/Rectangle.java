package ce1002.e8.s102502547;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class Rectangle extends JPanel {
	JLabel graph = new JLabel();
	JLabel info = new JLabel();

	Rectangle() {
		setSize(250, 280); // 設定大小
		setLocation(10, 10); // 設定位置
		setLayout(null);
		setBorder(new LineBorder(Color.black, 3)); // 設定邊界屬性
		graph.setLocation(10, 200);
		graph.setSize(100, 10);
		graph.setText("Graph:Rectangle");
		add(graph);
		info.setLocation(10, 220);
		info.setSize(200, 10);
		info.setText("Info:Green Rectangle");
		add(info);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.green); // 設定顏色
		g.fillRect(40, 40, 120, 120); // 設定方形位置、大小

	}

}
