package ce1002.e8.s102502547;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class Circle extends JPanel{
	JLabel graph=new JLabel();
	JLabel info=new JLabel();
	Circle(){
		setSize(250, 280); //設定大小
		setLocation(300,10); //設定位置
		setLayout(null);
		setBorder(new LineBorder(Color.black, 3)); //設定邊界屬性
		graph.setLocation(10,200);
		graph.setSize(100,10);
		graph.setText("Graph:Circle");
		add(graph);
		info.setLocation(10,220);
		info.setSize(200,10);
		info.setText("Info:Red circle");
		add(info);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.red); //設定顏色
		g.fillOval(10, 10, 180, 180); // 設定圓形位置、大小
	}

}
