package ce1002.e8.s102502019;
import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame{
	MyPanel panel1 = new MyPanel(1);
	MyPanel panel2 = new MyPanel(2);
public MyFrame()
{
	setLayout(new GridLayout(1,2,5,5));
	setBounds(300, 50, 440, 470);
	setVisible(true);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	add(panel1);
	panel1.setBounds(0,0,200,200);
	panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	add(panel2);
	panel2.setBounds(45, 0, 200, 200);
	panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
}
}
