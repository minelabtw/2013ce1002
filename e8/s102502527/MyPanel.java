package ce1002.e8.s102502527;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.Color;
import java.util.*;

public class MyPanel extends JPanel{
	    
		JPanel image;
	    JLabel name;
	    JLabel info;

	    MyPanel(JPanel image) 
	    {
	        Border border = new LineBorder(Color.black, 5);
	        setBorder(border); 
	        setLayout(null);

	        this.image = image;
	        
	        name = new JLabel();
	        name.setBounds(10, 165, 200, 20);
	        
	        info = new JLabel();
	        info.setBounds(10, 190, 200, 20);
	        
	        add(image);
	        add(name);
	        add(info);
	    }

	    static List<MyPanel> new_many() 
	    {
	        List<MyPanel> graphs = new ArrayList<MyPanel>(3);
	        MyPanel graph;
	        JPanel image;

	        image = new Rectangle();
	        graph = new MyPanel(image);
	        graph.name.setText("Graph: 正方形");
	        graph.info.setText("Info : 四邊相等且皆為直角四邊形");
	        graph.setBounds(5, 5, 220, 250);
	        graphs.add(graph);

	        image = new Oval();
	        graph = new MyPanel(image);
	        graph.name.setText("Graph: 橢圓");
	        graph.info.setText("Info : 平面上到兩個固定點的距離之和是常數的軌跡");
	        graph.setBounds(230, 5, 220, 250);
	        graphs.add(graph);

	        return graphs;
	    }
	}