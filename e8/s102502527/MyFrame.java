package ce1002.e8.s102502527;

import javax.swing.*;
import java.util.*;

class MyFrame extends JFrame{

    MyFrame() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//將叉叉加入函示跳出
        setLayout(null);//將排版的方式以座標方式顯示
		setLocationRelativeTo(null);//置中之類的排版
		setSize(500, 540);//顯示視窗大小
		
		List<MyPanel> graphs = MyPanel.new_many();

	    for(MyPanel graph : graphs) 
	    {
	    	add(graph);
	    }

        setVisible(true);//顯示視窗
    }
}