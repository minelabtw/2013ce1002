package ce1002.e8.s102502023;

import javax.swing.*;

import java.awt.*;


class MyPanel extends JPanel {
	private int x = -1; // get number to output the graphics
	//JLabel lb;
  MyPanel(int x) {
	  this.x = x;
	  //lb = new JLabel("hey");
	  //add(lb);
  }
  
  @Override
  protected void paintComponent(Graphics g) {
	  super.paintComponent(g);
	  
	  switch(x) {
	  case 0:
		  g.setColor(Color.BLUE);
		  g.fillRect(20, 20, 100, 50);
		  g.setColor(Color.BLUE);
		  g.drawString("Rectangle", 30, 100);
		  break;
	  case 1:
		  g.setColor(Color.CYAN);
		  g.fillOval(20, 20, 100, 50);
		  g.drawString("Oval", 100, 100);
		  break;
		  default:
			  g.drawString("?", 0, 0);
	  }
	  
  }
}
