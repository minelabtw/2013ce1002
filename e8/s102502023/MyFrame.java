package ce1002.e8.s102502023;

import javax.swing.*;

import java.awt.*;


class MyFrame extends JFrame {
	private MyPanel panel1 = new MyPanel(0); // give number 0
	private MyPanel panel2 = new MyPanel(1); // gibe number 1


public	MyFrame() {
	    super();
	    setLayout(null);
		Set(panel1, 20, 20);
		Set(panel2, 170, 20);

	}

void Set(MyPanel panel, int x, int y) {
	panel.setBounds(x, y, 140, 140);
	panel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	add(panel);
}  
}
