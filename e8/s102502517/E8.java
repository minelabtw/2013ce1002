package ce1002.e8.s102502517;

import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class E8 {

	public static void main(String[] args) {
		JFrame frame = new JFrame(); //產生window
		frame.setLayout(new GridLayout(1,2));
		frame.setSize(500,450);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel rectangle = new Rectangle();
		ShapePanel panel1 = new ShapePanel(rectangle,"Rectangle","In Euclidean plane geome...");
		panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		frame.add(panel1);
		
		JPanel oval = new Oval();
		ShapePanel panel2 = new ShapePanel(oval,"Triangle","An oval (from Latin ovum,...");
		panel2.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		frame.add(panel2);
	}

}
