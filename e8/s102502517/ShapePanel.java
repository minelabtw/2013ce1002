package ce1002.e8.s102502517;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ShapePanel extends JPanel{
	private JPanel panel;
	private String name;
	private String info;
	
	public ShapePanel(JPanel panel,String name,String info)
	{
		this.panel = panel;
		this.name = name;
		this.info = info;
		setLayout(new GridLayout(3,1));
		add(panel);
		add(new JLabel("Graph: " + name,JLabel.CENTER));
		add(new JLabel("Info: " + info,JLabel.CENTER));
	}
}
