package ce1002.e8.s102502554;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	
	
	MyPanel()
	{
		setLayout(new BorderLayout(5,10));
		setBounds(0, 0, 250 , 200);
		/*set panel's border*/
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		add(label1,BorderLayout.NORTH);
		label2.setText("The Graph is Rectangle. H:150 W:200");//the state of the graphic 
		add(label2,BorderLayout.SOUTH);
	}
	
	@Override 
    public void paintComponent (Graphics g){
		
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.fillRect(20, 20, 200, 150);
    }//paint the graphic

}
