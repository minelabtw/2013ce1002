package ce1002.e8.s102502521;

import javax.swing.*;
import java.awt.*;

public class Ova extends JPanel{
	public Ova() {
		setBounds(250,5,240,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}
 
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillOval(10,10,220,120);
		g.setColor(Color.BLACK);
		g.drawString("Graph: Oval.", 10, 150);
        g.drawString("Info: an oval...", 10, 160);
		
	}
}
