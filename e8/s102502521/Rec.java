package ce1002.e8.s102502521;

import java.awt.*;
import javax.swing.*;

public class Rec extends JPanel{
	public Rec() {
		setBounds(5,5,240,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
	}
 
	protected void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillRect(10, 10, 220, 120);
		g.setColor(Color.BLACK);
		g.drawString("Graph: Rectangle.", 10, 150);
        g.drawString("Info: a rectangle...", 10, 160);
		
	}
	
}
