package ce1002.e8.s102502508;
import javax.swing.BorderFactory;
import javax.swing.JPanel ;

import java.awt.Color;
import java.awt.Graphics ;

	public class Circle extends JPanel{
		
		public Circle(){
			setBounds(250, 10, 210, 210);//設定Panel的大小
			setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));//設定Panel的邊線
			
		}

		public void paintComponent(Graphics g)
	{
		g.drawOval(10, 10, 180, 180);//設定長方形的起始座標及邊長大小
		g.setColor(Color.green);//設定長方形的顏色
		g.fillOval(10, 10, 180, 180);//塗滿長方形內部
		g.setColor(Color.BLACK);//文字的顏色
		g.drawString("This is a Circle",59, 201);//輸入文字到圖形中並設定與圖形在Panel中的相對位子
	}
}
