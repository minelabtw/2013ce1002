package ce1002.e8.s102502017;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame{

	MyFrame(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(null);
		
		setPanel();
		
		this.setSize(936, 336);
		this.setVisible(true);
	}
	
	
	void setPanel(){
		
		//draw Rectangle
		MyPanel panel;
		panel = new MyPanel(0,0,0);
		panel.setState("Rectangle","http://en.wikipedia.org/wiki/Rectangle");
		add(panel);
		
		//draw Oval
		panel = new MyPanel(310,0,1);
		panel.setState("Oval","http://en.wikipedia.org/wiki/Oval");
		add(panel);
		
		//draw Circle
		panel = new MyPanel(620,0,2);
		panel.setState("Circle","http://en.wikipedia.org/wiki/Circle");
		add(panel);
		
	}
	
}
