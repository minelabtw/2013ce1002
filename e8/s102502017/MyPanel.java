package ce1002.e8.s102502017;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class MyPanel extends JPanel{
	
	JLabel name = new JLabel();
	JLabel info = new JLabel();
	
	int i;
	
	
	MyPanel(int x,int y,int i){
		//cancel default layout
		setLayout(null);	
		
		//Set Border
		setBounds(x,y,300,300);
        Border border = new LineBorder(Color.black, 5);
        setBorder(border);
        
        //determine which shape
        this.i=i;
        
        //set the location of infos
        this.name.setBounds(10, 200,300,10);
        this.add(name);
        this.info.setBounds(10,250,300,10);
        this.add(info);
	}
	
	public void setState(String name,String info){
		this.name.setText("Graph: " + name); 
		
		this.info.setText("Info: " + info);
		
	}
	
	//draw pictures
	public void paintComponent(Graphics g){
		if(i==0){
			g.setColor(Color.RED);
			g.fillRect(50, 25, 200, 100);
		}
		else if(i==1){
			g.setColor(Color.YELLOW);
			g.fillOval(50, 25, 200, 100);
		}
		else if(i==2){
			g.setColor(Color.GREEN);
			g.fillOval(75, 10, 150, 150);
		}
		
	}
	
	
	
	
}
