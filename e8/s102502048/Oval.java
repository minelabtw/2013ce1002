package ce1002.e8.s102502048;
import javax.swing.*;

import java.awt.*;

public class Oval extends JPanel
{
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillOval(20, 10, 200, 100);
		/*set label font*/
		Font font = new Font("Time New Roman", Font.BOLD, 14);
		JLabel lab = new JLabel("Oval");
		JLabel lab1 = new JLabel("Info: Oval is ...");
		lab.setFont(font);
		lab.setBounds(10,120,100,15);
		add(lab);
		lab1.setFont(font);
		lab1.setBounds(10,140,100,15);
		add(lab1);
	}
}
