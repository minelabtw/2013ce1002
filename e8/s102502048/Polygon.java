package ce1002.e8.s102502048;
import javax.swing.*;

import java.awt.*;

public class Polygon extends JPanel
{
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		int x[]={75,100,150,175,150,100};
		int y[]={60,17,17,60,103,103};
		g.fillPolygon(x,y,x.length);
		/*set label font*/
		Font font = new Font("Time New Roman", Font.BOLD, 14);
		JLabel lab = new JLabel("Polygon");
		JLabel lab1 = new JLabel("Info: Polygon is ...");
		lab.setFont(font);
		lab.setBounds(10,120,100,15);
		add(lab);
		lab1.setFont(font);
		lab1.setBounds(10,140,100,15);
		add(lab1);
	}
}
