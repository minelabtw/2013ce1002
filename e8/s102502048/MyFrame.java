package ce1002.e8.s102502048;
import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame
{
	/*Create three role panel*/
	protected Rec rect = new Rec();
	protected Oval oval = new Oval();
	protected Polygon polygon = new Polygon();
	
	MyFrame()
	{
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(0 , 0 , 810 , 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setp(rect,10,10);
		setp(oval,270,10);
		setp(polygon,530,10);
	}
	
	public void setp(JPanel panel, int x, int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 250 , 200);
		/*set panel's border*/
		panel.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		/*add panel to frame*/
		add(panel);
	}
}
