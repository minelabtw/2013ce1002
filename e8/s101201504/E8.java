package ce1002.e8.s101201504;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;

public class E8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        JFrame f = new JFrame("E8");   
		f.setSize(935, 300);
		f.setLayout(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);   
		
		//set panel
		Image[] image = new Image[3];
		for(int i = 0 ; i < 3 ; ++i)
		{
			image[i] = new Image(i);
			image[i].setBounds(i * 300 + 5 * (i + 1), 0, 300, 250);    //set size  of bound 
			f.add(image[i]);
		}
	}

}
