package ce1002.e8.s101201504;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
public class Image extends JPanel {
private int type;
	
	public Image(int i)
	{
		type = i;
		setBorder(BorderFactory.createLineBorder(Color.black, 3));
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		// TODO Auto-generated method stub
		super.paintComponent(g);
		if(type == 0)       //set rectangle
		{
			g.setColor(Color.red);     
			g.fillRect(50, 50, 200, 100);
			g.setColor(Color.black);
			g.drawString("Garph: Rectangle",10 , 200);
			g.drawString("Info:The four interior angles are equal quadrilateral",10 , 220);
		}
		else if(type == 1)     //set oval
		{
			g.setColor(Color.green);
			g.fillOval(50, 50, 200, 100);
			g.setColor(Color.black);
			g.drawString("Garph: Oval",10 , 200);
			g.drawString("Info:Oval is similar to circle",10 , 220);
		}
		else                           // set polygon
		{
			int x[] = {100, 190 , 240 , 190 , 100 , 50};
			int y[] = {50 , 50 , 100 , 150 , 150 , 100};
			g.setColor(Color.blue);
			g.fillPolygon(x, y, 6);
			g.setColor(Color.black);
			g.drawString("Garph: Polygon",10 , 200);
			g.drawString("Info:Polygon are six sides and six angles",10 , 220);
			
		}
	}

}
