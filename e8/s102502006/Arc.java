package ce1002.e8.s102502006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Arc extends JPanel{
	
public Arc() {
		JLabel graph = new JLabel("Graph: Arcs");
		JLabel info = new JLabel("Info: I'm am ....I don't know?");
		setLayout(null);
		graph.setBounds(20,130,220,30);
		info.setBounds(20, 150, 220, 30);
		add(graph);
		add(info);
	}
	
	protected void paintComponent(Graphics r) {
		super.paintComponent(r);
		r.setColor(Color.GREEN);
		for(int i=0; i<8; i++)r.fillArc(70, 20, 100, 100, i*45, 30);
	}	
}
