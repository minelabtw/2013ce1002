package ce1002.e8.s102502006;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame{
	
	protected Rectangle rec = new Rectangle(); // 長方形
	protected Oval oval = new Oval(); // 橢圓
	protected Polygon poly = new Polygon(); // 多邊形
	protected Arc arc = new Arc(); // 扇形
	
	public MyFrame(){
		setLayout(null);
		setBounds(50,50,1070,250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		rec.setBounds(10, 10, 250, 200);
		rec.setBorder(new LineBorder(Color.BLACK,3));
		add(rec);
		
		oval.setBounds(270, 10, 250, 200);
		oval.setBorder(new LineBorder(Color.BLACK,3));
		add(oval);
		
		poly.setBounds(530, 10, 250, 200);
		poly.setBorder(new LineBorder(Color.BLACK,3));
		add(poly);
		
		arc.setBounds(790, 10, 250, 200);
		arc.setBorder(new LineBorder(Color.BLACK,3));
		add(arc);
	}
}
