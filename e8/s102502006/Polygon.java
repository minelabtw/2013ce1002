package ce1002.e8.s102502006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Polygon extends JPanel{
	
	protected JLabel graph = new JLabel("Graph: Polygon");
	protected JLabel info = new JLabel("Info: I'm a yellow polygon!");
	
	public Polygon() {
		setLayout(null);
		graph.setBounds(20,130,220,30);
		info.setBounds(20,150,240,30);
		add(graph);
		add(info);
	}
	
	protected void paintComponent(Graphics p) {
		super.paintComponent(p);
		int x [] = {95,155,185,155,95,65};
	    int y [] = {20,20,70,120,120,70};
	    p.setColor(Color.YELLOW);		
		p.fillPolygon(x,y,6);
	}
}
