package ce1002.e8.s102502006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Rectangle extends JPanel{
	
	public Rectangle() {
		
		JLabel graph = new JLabel("Graph: Rectangle");
		JLabel info = new JLabel("Info: I'm a red rectangle!");
		setLayout(null);
		graph.setBounds(20,130,220,30);
		info.setBounds(20, 150, 220, 30);
		add(graph);
		add(info);
	}
	
	protected void paintComponent(Graphics r) {
		super.paintComponent(r);
		r.setColor(Color.RED);
		r.fillRect(50,20,150,100);
	}	

}
