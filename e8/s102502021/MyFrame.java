package ce1002.e8.s102502021;
import javax.swing.JFrame;

import ce1002.e8.s102502021.MyPanel;
public class MyFrame extends JFrame{
	public MyPanel panel1 = new MyPanel(); 
	public MyPanel panel2 = new MyPanel();
	public MyFrame(){
		setLayout(null);
		setBounds(0,0,800,800);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void setPanel(int i, MyPanel panel, int x, int y) {
		if (i == 1) {
			panel.setPic1();
		}
		else if (i == 2) {
			panel.setPic2();
		}
		panel.setLocation(x, y); 
		this.add(panel); 
		this.setVisible(true); 
	}
}
