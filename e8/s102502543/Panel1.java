package ce1002.e8.s102502543;

import java.awt.*;

import javax.swing.*;

public class Panel1 extends JPanel {
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();

	Panel1() {
		setBounds(10, 20, 260, 200);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setLayout(null);
		label1.setBounds(15, 140, 180, 15);
		label1.setText("Graph: Rectangle");
		label2.setBounds(15, 160, 180, 15);
		label2.setText("Info: In Euclidean plane geome...");
		add(label1);
		add(label2);

	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(25, 15, 200, 100);
	}
}
