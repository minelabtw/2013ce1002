package ce1002.e8.s102502543;

import java.awt.*;

import javax.swing.*;

public class Panel3 extends JPanel {
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();

	Panel3() {
		setBounds(550, 20, 260, 200);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		setLayout(null);
		label1.setBounds(15, 140, 180, 15);
		label1.setText("Graph: Polygon");
		label2.setBounds(15, 160, 180, 15);
		label2.setText("Info: In geometry a polygon is t...");
		add(label1);
		add(label2);

	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.GREEN);
		int xpoints[] = { 80, 150, 185, 150, 80, 45, 85 };
		int ypoints[] = { 15, 15, 65, 115, 115, 65, 15 };
		int npoints = 7;

		g.fillPolygon(xpoints, ypoints, npoints);
	}
}