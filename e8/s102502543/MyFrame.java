package ce1002.e8.s102502543;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	protected Panel1 panel1 = new Panel1();
	protected Panel2 panel2 = new Panel2();
	protected Panel3 panel3 = new Panel3();

	MyFrame() {
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(0, 0, 840, 300);
		add(panel1);
		add(panel2);
		add(panel3);
	}
}
