package ce1002.e8.s102502031;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {
	private String graphString = new String("Graph: ");
	private String infoString = new String("Info: ");
	private int width = 300;
	private int height = 250;

	public MyPanel() {
		this.setSize(width, height);
		this.setBorder(new LineBorder(Color.black, 3));
		this.setLayout(null);
	}

	public MyPanel(int panelWidth, int panelHeight) {
		this();
		width = panelWidth;
		height = panelHeight;
		this.setSize(width, height);
	}

	public void addToGraphString(String addString) {
		graphString = this.getGraphString() + addString;
	}

	public void addToInfoString(String addString) {
		infoString = this.getInfoString() + addString;
	}

	public String getGraphString() {
		return graphString;
	}

	public String getInfoString() {
		return infoString;
	}
}