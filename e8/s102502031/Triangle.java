package ce1002.e8.s102502031;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JLabel;

public class Triangle extends MyPanel {
	private int width = 300;
	private int height = 200;
	private Random random = new Random();
	private Color myColor = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));

	private JLabel graphLabel;
	private JLabel infoLabel;

	public Triangle(int panelWidth, int panelHeight) {
		width = panelWidth;
		height = panelHeight;
		this.setSize(width, height);

		super.addToGraphString("Equilateral Triangle");
		graphLabel = new JLabel(getGraphString());
		graphLabel.setLocation(10, this.getHeight() - 40);
		graphLabel.setSize(this.getWidth() - 10, 15);
		super.add(graphLabel);

		super.addToInfoString("A equilateral triangle has three equal sides and three equal angles.");
		infoLabel = new JLabel(getInfoString());
		infoLabel.setLocation(10, this.getHeight() - 20);
		infoLabel.setSize(this.getWidth() - 10, 15);
		super.add(infoLabel);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(myColor);
		int a;
		int b;
		// choose the minimum and give different value for draw a equilateral triangle
		if ((height - 40) < width * Math.sin(Math.PI / 3)) {
			a = (int) (height / Math.sin(Math.PI / 3)) * 4 / 5;
			b = height * 4 / 5;
		}
		else {
			a = width * 4 / 5;
			b = (int) (width * Math.sin(Math.PI / 3)) * 4 / 5;
		}
		int[] x = { width / 2, width / 2 - a / 2, width / 2 + a / 2 };
		int[] y = { height / 2 - 20 - b / 2, height / 2 - 20 + b / 2, height / 2 - 20 + b / 2 };
		g.fillPolygon(x, y, 3);
	}
}