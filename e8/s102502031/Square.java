package ce1002.e8.s102502031;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JLabel;

public class Square extends MyPanel {
	private int width = 300;
	private int height = 200;
	private Random random = new Random();
	private Color myColor = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));

	private JLabel graphLabel;
	private JLabel infoLabel;

	public Square(int panelWidth, int panelHeight) {
		width = panelWidth;
		height = panelHeight;
		this.setSize(width, height);
		
		super.addToGraphString("Square");
		graphLabel = new JLabel(getGraphString());
		graphLabel.setLocation(10, height - 40);
		graphLabel.setSize(width - 20, 15);
		super.add(graphLabel);

		super.addToInfoString("A square has four equal sides and four equal angles.");
		infoLabel = new JLabel(getInfoString());
		infoLabel.setLocation(10, height - 20);
		infoLabel.setSize(width - 20, 15);
		super.add(infoLabel);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(myColor);
		int a = Math.min(width, height - 40) * 4 / 5;
		// choose the minimum to draw a square but not a rectangle
		g.fillRect((width - a) / 2, (height - 40 - a) / 2, a, a);
	}
}