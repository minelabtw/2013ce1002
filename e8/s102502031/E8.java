package ce1002.e8.s102502031;

import javax.swing.JLabel;

public class E8 {
	// Figure may not be seen for its color chosen randomly! Please rerun the program.
	public static void main(String[] args) {
		final int panelWidth = 200;
		final int panelHeight = 250;
		final int row = 1;
		final int column = 3;

		MyFrame frame = new MyFrame("Show figures", row, column, panelWidth, panelHeight);
		MyPanel[][] panels = new MyPanel[row][column];

		panels[0][0] = new Triangle((frame.getWidth() - 26) / column - 10, (frame.getHeight() - 48) / row - 10);
		panels[0][1] = new Square((frame.getWidth() - 26) / column - 10, (frame.getHeight() - 48) / row - 10);
		panels[0][2] = new Circle((frame.getWidth() - 26) / column - 10, (frame.getHeight() - 48) / row - 10);
		for (int i = 0; i < column; i++) {
			for (int j = 0; j < row; j++) {
				panels[0][i].setLocation(10 + i * (panelWidth + 10), 10 + j * (panelHeight + 10));
				frame.add(panels[0][i]);
			}
		}
	}
}
