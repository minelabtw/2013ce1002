package ce1002.e8.s102502031;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JLabel;

public class Circle extends MyPanel {
	private int width = 300;
	private int height = 200;
	private Random random = new Random();
	private Color myColor = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));

	private JLabel graphLabel;
	private JLabel infoLabel;

	public Circle(int panelWidth, int panelHeight) {
		width = panelWidth;
		height = panelHeight;
		this.setSize(width, height);

		super.addToGraphString("Circle");
		graphLabel = new JLabel(getGraphString());
		graphLabel.setLocation(10, this.getHeight() - 40);
		graphLabel.setSize(this.getWidth() - 20, 15);
		super.add(graphLabel);

		super.addToInfoString("A circle is the set of all points in a plane that are at a given distance from the centre");
		infoLabel = new JLabel(getInfoString());
		infoLabel.setLocation(10, this.getHeight() - 20);
		infoLabel.setSize(this.getWidth() - 20, 15);
		super.add(infoLabel);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(myColor);
		int r = Math.min(width, height - 40) * 4 / 5;
		// choose the minimum to draw a circle but not a oval
		g.fillOval((width - r) / 2, (height - 40 - r) / 2, r, r);
	}
}