package ce1002.e8.s102502031;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame {
	private int row = 2;
	private int column = 3;
	private int panelWidth = 300;
	private int panelHeight = 250;

	public MyFrame() { // basic frame setting
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocation(100, 100);
		this.setSize(26 + (panelWidth + 10) * column, (panelHeight + 10) * row + 48);
		this.setLayout(null);
	}

	public MyFrame(String title, int row, int column, int panelWidth, int panelHeight) {
		this();
		this.setTitle(title);
		this.column = column;
		this.row = row;
		this.panelWidth = panelWidth;
		this.panelHeight = panelHeight;
		this.setSize(26 + (this.panelWidth + 10) * this.column, (this.panelHeight + 10) * this.row + 48);
	}
}
