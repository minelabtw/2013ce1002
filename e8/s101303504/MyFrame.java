package ce1002.e8.s101303504;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame{

	protected Rectangle rec = new Rectangle();
	protected Oval oval = new Oval();
	public MyFrame(){
		
		setSize(500,300);
	    setLocationRelativeTo(null); // Center the frame   
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setLayout(new FlowLayout());
	    
	    add(oval);
		add(rec);
	    setVisible(true);  
		

	}
}
