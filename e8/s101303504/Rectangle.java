package ce1002.e8.s101303504;
import java.awt.*;

import javax.swing.*;
public class Rectangle extends JPanel{

	public Rectangle(){
		
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setFont(new Font("SansSerif ", Font.BOLD, 16));
		g.setColor(Color.ORANGE);
		g.fillRect(10, 10, 150, 100);
		g.setColor(Color.BLACK);
		g.drawString("Graphics:Rectangle", 5,150 );
		g.drawRect(0, 0, 195, 195);
		g.drawString("the color is orange",5,170);
	}
	public Dimension getPreferredSize(){
		return new Dimension(200,200);
	}
}
