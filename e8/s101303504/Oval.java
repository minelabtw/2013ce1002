package ce1002.e8.s101303504;
import java.awt.*;

import javax.swing.*;
public class Oval extends JPanel{

	public Oval(){
		//setLayout(null);
		//setSize(200,150);
		//setLocation(300, 5);
		//setBounds(0, 0, 300, 300);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setFont(new Font("SansSerif ", Font.BOLD, 16));
		g.setColor(Color.GREEN);
		g.fillOval(10, 10, 150, 100);
		g.setColor(Color.BLACK);
		g.drawString("Graphics:Oval", 5,150 );
		g.drawRect(0, 0, 195, 195);
		g.drawString("the color is green",5,170);
	}
	public Dimension getPreferredSize(){
		return new Dimension(200,200);
	}
}
