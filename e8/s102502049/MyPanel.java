package ce1002.e8.s102502049;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	public static int Rect = 1;
	public static int Oval = 2;
	private String graphName;
	private String info;  
	private int mode = 1;
	
	private JLabel nameJLabel = new JLabel(); // create two label
	private JLabel infoJLabel = new JLabel();
	
	MyPanel(int mode){
		this.mode = mode;
		
		if(mode == 1){
			graphName = "Rectangle"; // set graphName and info
			info = "In Euclidean plane gecome...";
		}
		else if(mode == 2){
			graphName = "Oval";
			info = "An oval (from latin ovum,....";
		}
		
		
		setBorder(BorderFactory.createLineBorder(Color.black, 3)); // set BorderLine
		
		nameJLabel.setText("Graph: " + graphName); // set text
		infoJLabel.setText("info: " + info);
		
		add(nameJLabel); // add labels to panel
		add(infoJLabel);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		if(mode == 1){
			g.setColor(Color.blue);
			g.fillRect(30, 10, 200, 100);
			nameJLabel.setBounds(30, 125 ,180, 10); // set position
			infoJLabel.setBounds(30, 140, 180, 10);
		}
		else if(mode == 2){
			g.setColor(Color.yellow);
			g.fillOval(20, 10, 200, 100);
			nameJLabel.setBounds(20, 125 ,180, 10); // set position
			infoJLabel.setBounds(20, 140, 180, 10);
		}
		
	}
	
	
}
