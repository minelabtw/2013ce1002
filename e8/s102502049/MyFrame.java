package ce1002.e8.s102502049;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame{
	
	MyPanel panel1 = new MyPanel(MyPanel.Oval); // create panel and choose graph
	MyPanel panel2 = new MyPanel(MyPanel.Rect);
	
	MyFrame(){
		setSize(550, 260);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true); // frame setting
		
		setLayout(null); // set panel position
		panel1.setBounds(10, 10, 250, 200);
		panel2.setBounds(270, 10, 250, 200);
		add(panel1);
		add(panel2);
	}
}
