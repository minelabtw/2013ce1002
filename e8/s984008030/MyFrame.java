package ce1002.e8.s984008030;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	private MyPanel rectPanel;//欲畫出正方形之panel
	private MyPanel circlePanel;//欲畫出圓形之panel
	private MyPanel polyPanel;//欲畫出多邊形之panel
	
	public MyFrame() {
		rectPanel = new MyPanel(1, 10, 10, 250, 200);//初始化正方形panel
		circlePanel = new MyPanel(2, 270, 10, 250, 200);//初始化圓形panel
		polyPanel = new MyPanel(3, 530, 10, 250, 200);//初始化多邊形panel
		//設定MyFrame的狀態
		setLayout(null);
		setBounds(0, 0, 800, 250);//設定MyFrame位置在(0, 0) 大小為550*550
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//
		add(rectPanel);//將rectPanel加入MyFrame
		add(circlePanel);//將circlePanel加入MyFrame
		add(polyPanel);//將polyPanel加入MyFrame
	}
	
}
