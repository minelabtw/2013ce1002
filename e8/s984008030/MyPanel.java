package ce1002.e8.s984008030;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	private int type;// 類型
	private JLabel contentLabel;// 文字
	
	public MyPanel(int _type, int x, int y, int width, int height) {
		// 初始化成員變數
		this.type = _type;
		contentLabel = new JLabel();
		//設定 MyPanel 狀態
		setBounds(x, y, width, height);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		//設定 contentLabel 狀態
		contentLabel.setBounds(3, height - 43, width - 6, 40);
		switch (this.type) {
		case 1:
			contentLabel.setText("<html>Graph: Rectangle<br>Info: In Euclidean plane gnome...</html>");
			break;
		case 2:
			contentLabel.setText("<html>Graph: Oval<br>Info: An ovel (from Latin ovum, ...</html>");
			break;
		case 3:
			contentLabel.setText("<html>Graph: Polygon<br>Info: In geometry polygon is...</html>");
			break;
		default:
			contentLabel.setText("Graph: Unknown");
			break;
		}
		add(contentLabel);// 將contentLabel加入MyPanel
	}
	
	public void paintComponent(Graphics g) {
		// 根據類型決定要畫出的圖
		switch (this.type) {
		case 1:
			g.setColor(Color.BLUE);
			g.fillRect(75, 20, 100, 100);
			break;
		case 2:
			g.setColor(Color.YELLOW);
			g.fillOval(75, 20, 100, 100);
			break;
		case 3:
			g.setColor(Color.GREEN);
			int[] xPoints = new int[6];
			int[] yPoints = new int[6];
			xPoints[0] = 75;
			xPoints[1] = 100;
			xPoints[2] = 150;
			xPoints[3] = 175;
			xPoints[4] = 150;
			xPoints[5] = 100;
			yPoints[0] = 70;
			yPoints[1] = 95;
			yPoints[2] = 95;
			yPoints[3] = 70;
			yPoints[4] = 45;
			yPoints[5] = 45;
			g.fillPolygon(xPoints, yPoints, 6);
			break;
		default:
			break;
		}
	}
	
}
