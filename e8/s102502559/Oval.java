package ce1002.e8.s102502559;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;

public class Oval extends JPanel{
	Oval()
	{
		setSize(250,150);
		setLayout(null);
		LineBorder LB = new LineBorder(Color.black,3);
		setBorder(LB);
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillOval(25,30,200,150);
		
	}
	public void showinfo()
	{
		JLabel label1 = new JLabel("Graph :Oval");
		label1.setBounds(20,200, 100, 50);
		JLabel label2 = new JLabel("Info :having the general form, shape, or outline of an egg; egg-shaped.");
		label2.setBounds(20,220,200, 50);
		this.add(label1);
		this.add(label2);
	}

}
