package ce1002.e8.s102502559;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;

public class Rec extends JPanel{
	Rec()
	{
		setSize(250,150);
		setLayout(null);
		LineBorder LB = new LineBorder(Color.black,3);
		setBorder(LB);
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(20, 30, 200,150);
	}
	public void showinfo()
	{
		JLabel label1 = new JLabel("Graph :Rectangle");
		label1.setBounds(20,200,200, 50);
		JLabel label2 = new JLabel("Info :A 4-sided flat shape with straight sides where all interior angles are right angles (90�X). ");
		label2.setBounds(20,220,200, 50);
		this.add(label1);
		this.add(label2);
	}
	
	
}
