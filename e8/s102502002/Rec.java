package ce1002.e8.s102502002;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Rec extends JPanel {

	protected JLabel lab = new JLabel();
	
	Rec(){
		setBounds(0,0,250,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		lab.setText("Graph: Rectangle Info: blah");
		lab.setVerticalTextPosition(lab.BOTTOM);
		add(lab);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.fillRect( 60, 40, 130, 130);
	}

}