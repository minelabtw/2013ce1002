package ce1002.e8.s102502002;

import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected Rec rec = new Rec();
	protected Circle cir = new Circle();
	protected Oval ova = new Oval();
	
	MyPanel(){
		setBounds(0, 0, 770, 250);
		setLayout(null);
		add(rec);
		add(cir);
		add(ova);
	}
	
}
