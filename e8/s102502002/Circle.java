package ce1002.e8.s102502002;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Circle extends JPanel {

	protected JLabel lab = new JLabel();
	
	Circle(){
		setBounds(260,0,250,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		lab.setText("Graph: Circle Info: blah blah");
		lab.setVerticalTextPosition(lab.BOTTOM);
		add(lab);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.red);
		g.fillOval( 50, 30, 150, 150);
	}
}
