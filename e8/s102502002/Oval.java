package ce1002.e8.s102502002;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Oval extends JPanel {

	protected JLabel lab = new JLabel();
	
	Oval(){
		setBounds(520, 0,250,250);
		setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
		lab.setText("Graph: Oval Info: blah blah blah");
		lab.setVerticalTextPosition(lab.BOTTOM);
		add(lab);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.blue);
		g.fillOval(50, 75, 150, 90);
	}
}