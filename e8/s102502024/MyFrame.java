package ce1002.e8.s102502024;
import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	protected Rectangle rec=new Rectangle();
	protected Oval ov=new Oval();
	protected Circle cir=new Circle(); 
	public MyFrame()
	{
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		rec.setBounds(0,0,300,200);
		rec.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		add(rec);
		ov.setBounds(300,0,300,200);
		ov.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		add(ov);
		cir.setBounds(600,0,300,200);
		cir.setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		add(cir);
	}
}
