package ce1002.e8.s102502024;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
public class Circle extends JPanel{
	protected JLabel label=new JLabel();
	protected JLabel label1=new JLabel();
	Circle()
	{
		setLayout(null);
		label.setText("Graph:Circle");
		label.setBounds(10,5,100,80);
		add(label);
		label1.setText("info:It is a circle.");
		label1.setBounds(10,20,100,80);
		add(label1);
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.red);
		g.fillOval(20, 70, 30,30);
	}
}
