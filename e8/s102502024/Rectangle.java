package ce1002.e8.s102502024;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Graphics;
import java.awt.Color;
public class Rectangle extends JPanel{
	protected JLabel label=new JLabel();
	protected JLabel label1=new JLabel();
	Rectangle()
	{
		setLayout(null);
		label.setText("Graph:Rectangle");
		label.setBounds(10,5,100,80);
		add(label);
		label1.setText("info:It is a rectangle.");
		label1.setBounds(10,20,100,80);
		add(label1);
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.yellow);
		g.fillRect(20, 70, 50, 60);
	}
}
