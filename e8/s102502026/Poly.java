package ce1002.e8.s102502026;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Poly extends JPanel {
	Poly() {
		setLayout(null);
		setBounds(250, 10, 200, 200);
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
	}

	public void paint(Graphics g) {
		g.drawRect(0,0,199,199);
		int[] X = {20, 160, 40, 90, 130};
		int[] Y = { 50, 50, 120, 20, 120 };
		g.setColor(Color.black);
		g.drawPolygon(X,Y,5);
		g.setColor(Color.blue);
		g.fillPolygon(X, Y,5);
		g.setColor(Color.black);
		g.drawString("Graph: Star", 55, 150);
		
	}
}
