package ce1002.e8.s102502026;

import javax.swing.JFrame;
import javax.swing.border.LineBorder;

import java.awt.BorderLayout;
import java.awt.Color;

public class MyFrame extends JFrame {
	Oval panel1 = new Oval();
	Poly panel2 = new Poly();

	public MyFrame() {
		setLayout(null);
		add(panel1);
		add(panel2);
	}

}
