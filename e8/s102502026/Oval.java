package ce1002.e8.s102502026;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Oval extends JPanel{
public Oval(){
	setLayout(null);
	setBounds(30, 10, 200, 200);
	setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
}
	public void paint(Graphics g){
		g.drawRect(0,0,199,199);
		g.setColor(Color.black);
		g.drawOval(40, 30, 120, 90);
		g.setColor(Color.yellow);
		g.fillOval(40, 30, 120, 90);
		g.setColor(Color.black);
		g.drawString("Graph: Oval", 70, 150);
	}
}
