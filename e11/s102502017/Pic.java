package ce1002.e11.s102502017;

import javax.swing.*;
import java.awt.*;

public class Pic extends JPanel{

	public static int x,y;
	public static int type;
	public static int color;
	
	Pic(){
	}
	
	public void paint(Graphics g){
		//clear former pic
		super.paint(g);
		
		switch(color){
		case 0:
			g.setColor(Color.red);
			break;
		case 1:
			g.setColor(Color.green);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		}
		switch(type){
		case 0:
			g.fillOval(x, y, 50, 50);
			break;
		case 1:
			g.fillRect(x, y, 50, 50);
			break;
		}
		
	}
}
