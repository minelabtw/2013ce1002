package ce1002.e11.s102502017;

import java.util.*;

public class Draw extends TimerTask{

	Pic pic;
	Draw(Pic pic){
		this.pic = pic;
	}
	
	public void run(){
		Pic.x = (int)(Math.random()*500);
		Pic.y = (int)(Math.random()*500);
		Pic.type = (int)(Math.random()*2);
		Pic.color = (int)(Math.random()*3);
		//repaint
		pic.repaint();
	}
}