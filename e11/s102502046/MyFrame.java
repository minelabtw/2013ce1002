package ce1002.e11.s102502046;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyPanel panel = new MyPanel();
	MyFrame () {
		
		setSize(500, 500); //大小
		setLocationRelativeTo(null); //位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //關閉程序
		setLayout(null); //排版
		add(panel);
		setVisible(true); //顯示
		
	}

}
