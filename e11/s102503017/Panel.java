package ce1002.e11.s102503017;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Panel extends JPanel {
	

	//Initialize ActionListenr with overriding the method actionPerformed.
	ActionListener actListener = new ActionListener(){
		public void actionPerformed(ActionEvent event){
			//when the event triggered, repaint the graph.
			repaint();
		}
	};
	//Initialize timer.
	Timer timer = new Timer(1000, actListener);
	
	Panel()
	{
		//basic setting of Panel and start timer.
		setLayout(null);
		setBounds(0,0,600,600);
		timer.start();
		
	}
	
	//draw graphs.
	protected void paintComponent(Graphics g)
	{
		//clear the panel via method paintComponent(g) of JComponent.
		super.paintComponent(g) ;
		
		int x = (int) (Math.random() * 550);
		int y = (int) (Math.random() * 550);
		int shape = (int) (Math.random() * 2);
		int color = (int) (Math.random() * 4);
		int diameter = 50;
		
		switch(color){
		case 0:
			g.setColor(Color.yellow);
			break;
		case 1:
			g.setColor(Color.green);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		case 3:
			g.setColor(Color.red);
			break;
		}
		
		if(shape == 0)
		{
			g.fillRect(x, y, diameter, diameter);
		}
		else
		{
			g.fillOval(x, y, diameter, diameter);
		}
		

	}
}