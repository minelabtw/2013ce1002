package ce1002.e11.s102502504;

import javax.swing.Timer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.*;
import java.util.Random;

import javax.swing.JPanel;

public class Animation extends JPanel
{
	class TimerListioner implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) 
		{
			// TODO Auto-generated method stub
			repaint();
		}

	}
	
	private Timer timer = new Timer(500,new TimerListioner());
	private Random random = new Random();
	
	
	
	Animation()
	{
		setBounds(10,10,490,490);
		setLayout(null);	
		setVisible(true);
		timer.start();
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		int shape = random.nextInt(2);
		int color = random.nextInt(3);
		int x = random.nextInt(400);
		int y = random.nextInt(400);

	
		super.paintComponent(g);
		
		if(color==0)
		{
			g.setColor(Color.red);
		}
		else if(color==1)
		{
			g.setColor(Color.green);
		}
		else
		{
			g.setColor(Color.blue);
		}
		
		if(shape==0)
		{
			g.fillOval(x,y,50,50);
		}
		else if(shape==1)
		{
			g.fillRect(x,y,50,50);
		}
		
	}
	
}
