package ce1002.e11.s995002014;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.*;

//要實作runnable介面
public class MyFrame extends JFrame implements Runnable {
	MyRec rec = new MyRec();
	public MyFrame() {
		add(rec);
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		new Thread(this).start();
	}
	
	public void run()   {
		try{
		while(true) {
			Random rand=new Random();
			int x=rand.nextInt(4);
			rec.setpos(rand.nextInt(420),rand.nextInt(420));
			rec.setshape(rand.nextInt(2));
			if(x==0)rec.setColor(Color.red);
			if(x==1) rec.setColor(Color.green);
			if(x==2)rec.setColor(Color.blue);
			rec.repaint();			
			Thread.sleep(500); //thread停500毫秒
		}
		}
		catch (InterruptedException ex){}
	}
}

//形狀類別
class MyRec extends JPanel {
	int posx=0,posy=0;
	int shape=0;
	Color col;
	public MyRec(){
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(col); 
		if(shape==0)g.fillRect(posx, posy, 60, 60);
		else g.fillOval(posx, posy, 60, 60);
	}
	
	//設定顏色位置圖形類別
	public void setColor(Color color)
    {
      this.col = color; 
    }
	public void setpos(int x, int y) {
		posx=x;
		posy=y;
	}
	public void setshape(int x){
		shape=x;
	}
}