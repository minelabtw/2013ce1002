package ce1002.e11.s102502544;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Timer;
import javax.swing.JPanel;

public class Panel extends JPanel{
	private Timer t = new Timer(1000,new TimeListener());;
	Random x = new Random();
	Random y = new Random();
	Random z = new Random();
	
	public Panel(){
		setSize(400,400);
		t.start();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		switch(z.nextInt(2)){
		case 0:
			switch(y.nextInt(3)){
			case 0:
				g.setColor(Color.RED);
				g.fillRect(x.nextInt(460),x.nextInt(460),40,40);
				break;
			case 1:
				g.setColor(Color.GREEN);
				g.fillRect(x.nextInt(460),x.nextInt(460),40,40);
				break;
			case 2:
				g.setColor(Color.BLUE);
				g.fillRect(x.nextInt(460),x.nextInt(460),40,40);
				break;
			}
			break;
		case 1:
			switch(y.nextInt(3)){
			case 0:
				g.setColor(Color.RED);
				g.fillOval(x.nextInt(460),x.nextInt(460),40,40);
				break;
			case 1:
				g.setColor(Color.GREEN);
				g.fillOval(x.nextInt(460),x.nextInt(460),40,40);
				break;
			case 2:
				g.setColor(Color.BLUE);
				g.fillOval(x.nextInt(460),x.nextInt(460),40,40);
				break;
			}
			break;
		
		}
	}
	
	
	
	
	class TimeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			repaint();
		}
		
	}
	
}
