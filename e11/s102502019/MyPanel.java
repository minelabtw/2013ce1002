package ce1002.e11.s102502019;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Graphics;
public class MyPanel extends JPanel{
public MyPanel()
{
	setBounds(0, 0, 500,500);
	Timer timer = new Timer(100 , new TimerListener());
	timer.start();
}
protected void  paintComponent(Graphics g)
{
	super.paintComponent(g);
	int t = (int)(Math.random()*6);
	int x = (int)(Math.random()*400);
	int y = (int)(Math.random()*400);
	if(t == 0)
	{
		g.setColor(Color.red);
		g.fillRect(x, y, 20, 30);
	}
	else if(t == 1)
	{
		g.setColor(Color.blue);
		g.fillRect(x, y, 20, 30);
	}
	else if(t == 2)
	{
		g.setColor(Color.green);
		g.fillRect(x, y, 20, 30);
	}
	else if(t == 3)
	{
		g.setColor(Color.red);
		g.fillOval(x, y, 30, 30);
	}
	else if(t == 4)
	{
		g.setColor(Color.blue);
		g.fillOval(x, y, 30, 30);
	}
	   else
	{
		g.setColor(Color.green);
		g.fillOval(70, 80, 30, 30);
	}
	
}
class TimerListener implements ActionListener
{
	
	public void actionPerformed(ActionEvent e)
	{
		repaint();
	}
}
}

