package ce1002.e11.s102502025;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame extends JFrame {
	MyFrame() {
		add(new Panel());
	}

	class Panel extends JPanel {
		private Timer timer = new Timer(700, new MyFrame.TimerListener());//set time

		Panel() {
			this.timer.start();
		}

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			int graphWidth = 50;//graph size
			int graphHeight = 50;
			//random variable
			int x = (int) (Math.random() * (getWidth() - graphWidth) + 1.0D);
			int y = (int) (Math.random() * (getHeight() - graphHeight) + 1.0D);
			int color = (int) (Math.random() * 3.0D + 1.0D);
			int type = (int) (Math.random() * 2.0D + 1.0D);
			switch (color) {//choose color
			case 1:
				g.setColor(Color.RED);
				break;
			case 2:
				g.setColor(Color.YELLOW);
				break;
			case 3:
				g.setColor(Color.BLUE);
				
			}
			switch (type) {//choose graph
			case 1:
				g.fillOval(x, y, graphWidth, graphHeight);
				break;
			case 2:
				g.fillRect(x, y, graphWidth, graphHeight);
			}
		}
	}

	class TimerListener implements ActionListener {
		TimerListener() {
		}

		public void actionPerformed(ActionEvent arg0) {
			MyFrame.this.repaint();
		}
	}
}
