package ce1002.e11.s102502039;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MyPanel extends JPanel {
	private int radious = 30;//declare variables
	private int length = 50;
	private int height = 30;
	private int xCoordinate = 0;
	private int yCoordinate = 0;
	private int type;

	private Timer timer = new Timer(1000, new TimerListener());//create timer

	public MyPanel() {
		timer.start();//start timer

	}

	class TimerListener implements ActionListener {//lister class
		@Override//event handler
		public void actionPerformed(ActionEvent e) {//repaint
			repaint();

		}
	}

	@Override
	protected void paintComponent(Graphics g) {//draw icons
		super.paintComponent(g);

		type = (int) ((Math.random() * 6) + 1);
		xCoordinate = (int) ((Math.random() * 500) + 25);
		yCoordinate = (int) ((Math.random() * 400) + 25);
		switch (type) {
		case 1:
			g.setColor(Color.RED);
			g.fillRect(xCoordinate, yCoordinate, length, height);
			break;
		case 2:
			g.setColor(Color.GREEN);
			g.fillRect(xCoordinate, yCoordinate, length, height);
			break;
		case 3:
			g.setColor(Color.BLUE);
			g.fillRect(xCoordinate, yCoordinate, length, height);
			break;
		case 4:
			g.setColor(Color.RED);
			g.fillOval(xCoordinate, yCoordinate, 2 * radious, 2 * radious);
			break;
		case 5:
			g.setColor(Color.GREEN);
			g.fillOval(xCoordinate, yCoordinate, 2 * radious, 2 * radious);
			break;
		case 6:
			g.setColor(Color.BLUE);
			g.fillOval(xCoordinate, yCoordinate, 2 * radious, 2 * radious);
			break;
		}
	}

}
