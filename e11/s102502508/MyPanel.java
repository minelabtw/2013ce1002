package ce1002.e11.s102502508;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.* ;
 public class MyPanel extends JPanel{
	
	 private Timer timer=new Timer(700,new TimerListener());//設定時間
	 
	 
	 
	 
	  public MyPanel() {
			
			
			   timer.start();//啟動時間
		}
		
		
		
	      public void paintComponent(Graphics g)
			{   
	    	    super.paintComponent(g);//劃一個圖
	    	    //讓所畫的圖形起始位置不斷變動
	    	    int x = (int) (Math.random()*getWidth());
	    	    int y = (int) (Math.random()*getHeight());
	    	    //設定可以選擇圖形或顏色的隨機變數
	    	    int e=(int) (Math.random()*3+1);
	    	    int z=(int)(Math.random()*2+1) ;
	    	    switch(e)
	    	    {
	    	      //設定顏色
	    	      case 1:
	    	           g.setColor(Color.blue);
	    	           break;
	    	      case 2 :
	    	    	  g.setColor(Color.PINK);
	    	           break;
	    	      case 3:
	    	    	  g.setColor(Color.YELLOW);
	    	           break;
				
			    }
	    	    switch(z){
	    	    
	    	       case 1:
	    	           g.fillRect(x, y,49, 49);//塗滿長方形內部
	    	           break;

	    	       case 2:
	    	    	   g.fillOval(x, y, 35, 35);//塗滿圓形內部
	    	           break;

	    	    	   
	    	    }
	    	    
	    	  	
	    	    
			
				
			}
	      
	      class TimerListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				repaint();//隨著時間不斷重繪圖形
				
			}
	    	  
	      }
	      
		    

}
