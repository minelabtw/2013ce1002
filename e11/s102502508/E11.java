package ce1002.e11.s102502508;
import javax.swing.JFrame;
public class E11 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		JFrame mf= new JFrame();//建立視窗
		mf.setTitle("E11-102502508");//視窗主題
		mf.setSize(700, 500);//調整視窗大小
	    mf.setLocationRelativeTo(null) ;//讓視窗螢幕居中
		mf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;//使視窗的主程式可以被關閉
		mf.setVisible(true);  //讓使用者能看到視窗
		
		MyPanel mp=new MyPanel();//建立畫板
		mf.add(mp);//將畫板放入視窗中
	}

	

}
