package ce1002.e11.s101201508;

import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class Graph extends JPanel{
	Graph()
	{
		
	}
	public void show()
	{//redraw the graph
		repaint();
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		//set the color
		Random_color color=new Random_color();
		g.setColor(color.get_color());
		//random the location and the graph
		Random random=new Random();
		int i=random.nextInt(2);
		int x=random.nextInt(450);
		int y=random.nextInt(450);
		if (i==0)
		{
			g.fillRect(x, y, 50, 50);
		}
		else
		{
			g.fillOval(x, y, 50, 50);
		}
	}

}
