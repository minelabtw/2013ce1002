package ce1002.e11.s101201508;

import java.awt.Color;
import java.util.Random;

public class Random_color {
	Random_color()
	{
		
	}
	public Color get_color()
	{//set random color
		Random ran=new Random();
		int i=ran.nextInt(13);
		if (i==0)
			return Color.BLACK;
		else if (i==1)
			return Color.BLUE;
		else if (i==2)
			return Color.CYAN;
		else if (i==3)
			return Color.DARK_GRAY;
		else if (i==4)
			return Color.GRAY;
		else if (i==5)
			return Color.GREEN;
		else if (i==6)
			return Color.LIGHT_GRAY;
		else if (i==7)
			return Color.MAGENTA;
		else if (i==8)
			return Color.ORANGE;
		else if (i==9)
			return Color.PINK;
		else if (i==10)
			return Color.RED;
		else if (i==11)
			return Color.WHITE;
		else 
			return Color.YELLOW;
	}
}
