package ce1002.e11.s101201508;

import java.util.Random;
import java.util.Timer;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private Graph graph=new Graph();
	MyFrame()
	{
		//set data of graph
		graph.setBounds(0,0,500,500);
		//set the data of MyFrame
		super.setTitle("E11-101201508");
		super.setSize(550,550);
		super.setLayout(null);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setVisible(true);
		super.add(graph);
		//redraw the graph per second
		Timer timer=new Timer();
		timer.schedule(new DoTimer(graph), 0,1000);
		try
		{//canel the redraw when time exceed 10 second
			Thread.sleep(10000);
		}
		catch (InterruptedException e)
		{
			
		}
		timer.cancel();
	}

}
