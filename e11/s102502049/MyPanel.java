package ce1002.e11.s102502049;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private int diameter = 75;
	private int side = 75;
	
	private int xPos = 0;
	private int yPos = 0;
	
	private int mode = 0;	//	circle or rectangle
	
	private int color = 0; // color 
	
	private Timer timer = new Timer(1000, new ActionListener() {	//	random position each second
		
		@Override
		public void actionPerformed(ActionEvent e) {
		
		color = (int)(Math.random()*3);	//	0 1 2
		mode = (int)(Math.random()*2);	//	0 1
		xPos = (int)((Math.random()*6)*(getWidth()/7));
		yPos = (int)((Math.random()*6)*(getHeight()/7));
		repaint();
			
		}
	});
	
	
	MyPanel(){
		timer.start();
	}
	
	public void paintComponent(Graphics g){		//	paint graphic
		super.paintComponent(g);
		
		switch (color) {	//	color
		case 0:
			g.setColor(Color.red);
			break;
		case 1:
			g.setColor(Color.green);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		default:
			break;
		}
		
		if(mode == 0){	//	circle
			g.fillOval(xPos, yPos, diameter, diameter);
		}
		else if(mode == 1){	//	rectangle
			g.fillRect(xPos, yPos, side, side);			
		}
		
	}
}
