package ce1002.e11.s102502008;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel
{
	private int x ;
	private int y ;
	private int mode ;
	private int color ;
	private Timer timer;
	private Random random= new Random() ;
	public MyPanel()
	{
		
		Timer timer= new Timer(1000,new TimerListener()) ;
		timer.start() ;
		setBounds(0,0,800,600) ;
		x=400 ;
		y=300;
		
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g) ;	
		if(color==0)
		{
			g.setColor(Color.BLUE);
		}
		else if(color==1)
		{
			g.setColor(Color.GREEN);
		}
		else
		{
			g.setColor(Color.RED) ;
		}
		
		if(mode==0)
		{
			g.fillRect(x, y, 100, 100) ;
		}
		else
		{
			g.fillOval(x, y, 100, 100) ;
		}
	}
	class TimerListener implements ActionListener
	{
		
		public TimerListener()
		{
			
			
		}
		@Override
		public void actionPerformed(ActionEvent e)
		{
			
			mode= random.nextInt()%2;
			x=Math.abs(random.nextInt()%700) ;
			y=Math.abs(random.nextInt()%500) ;
			color=Math.abs(random.nextInt())%3 ;
			repaint();
		}
	}
	
	
}
