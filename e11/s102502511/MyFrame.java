package ce1002.e11.s102502511;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	private MyPanel myPanel = new MyPanel(); 
	
	public MyFrame() {
		setLayout(null);
		setBounds(700,300,500,500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		add(myPanel);		
	}

}
