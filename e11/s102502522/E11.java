package ce1002.e11.s102502522;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;
public class E11 extends JFrame implements ActionListener
{
static E11 frame=new E11();
static int Colorran;//顏色
static int shaperan;//形狀
static int x;//x軸值
static int y;//y軸值
static Random ran=new Random();
static javax.swing.Timer time=new javax.swing.Timer(1000,frame);//每一秒執行一次
	public static void main(String args[])
	{
		frame.setLayout(null);
		frame.setBounds(200,20,600,600);
		time.start();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	public void paint(Graphics g)
	{   g.setColor(Color.white);//先用畫白色覆蓋掉先前的圖形
		g.fillRect(0,0,600,600);
		if(Colorran==0)
		g.setColor(Color.red);
	else if(Colorran==1)
		g.setColor(Color.green);
	else
		g.setColor(Color.blue);
	if(shaperan==0)
		g.fillRect(x,y,100,100);
	else
		g.fillOval(x,y,100,100);
	}
	public void actionPerformed(ActionEvent arg0) 
	{frame.removeAll();
	   Colorran=ran.nextInt(3);//隨機顏色
	   shaperan=ran.nextInt(2);//隨機形狀
	   x=ran.nextInt(500);//隨機x軸
	   y=ran.nextInt(500);//隨機y軸
	   repaint();//重畫
	}

}

