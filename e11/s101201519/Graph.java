package ce1002.e11.s101201519;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JPanel;
public class Graph extends JPanel{
	Graph() {
		setBounds(0, 0, 500, 500); // set Panel of size
	}

	public void show() {
		repaint();
	}

	@Override
	protected void paintComponent(Graphics g) { // paintcomponent
		
		ColorChange color = new ColorChange();
		g.setColor(color.getcolor());
		Random random = new Random();
		int i = random.nextInt(2);
		int x = random.nextInt(450);
		int y = random.nextInt(450);
		if (i == 0) {
			g.fillRect(x, y, 50, 50);
		} else {
			g.fillOval(x, y, 50, 50);
		}
	}

}
