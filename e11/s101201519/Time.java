package ce1002.e11.s101201519;

import java.util.TimerTask;
import ce1002.e11.s101201519.Graph;

public class Time extends TimerTask{
	private Graph graph = new Graph();

	Time(Graph g) {
		graph = g;
	}

	@Override
	public void run() {
		graph.show();
	}

}
