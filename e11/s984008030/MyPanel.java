package ce1002.e11.s984008030;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private final Color colorArray[] = {Color.RED, Color.BLUE, Color.GREEN};
	private int width;
	private int height;
	private Timer timer;
	public MyPanel(int arg0, int arg1, int arg2,  int arg3) {
		this.width = arg2;
		this.height = arg3;
		this.timer = new Timer(1000, new TimerListener());
		this.timer.start();
		this.setBounds(arg0, arg1, arg2, arg3);
	}
	
	public void paintComponent(Graphics g) {
		Random r = new Random(); // Get random generator
		super.paintComponent(g); // Clean to empty
		g.setColor(colorArray[r.nextInt(3)]); // Set random color
		// Fill random shape
		if (r.nextInt() % 2 == 0) {
			g.fillOval(r.nextInt(this.width - 60), r.nextInt(this.height - 60), 50, 50);
		}
		else {
			g.fillRect(r.nextInt(this.width - 60), r.nextInt(this.height - 60), 50, 50);
		}
	}
	
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			repaint();
		}
		
	}
}

