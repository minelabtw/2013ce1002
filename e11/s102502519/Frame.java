package ce1002.e11.s102502519;

import java.awt.*;
import javax.swing.*;

public class Frame extends JFrame{
	protected Panel panel = new Panel();
	Frame(){
		setTitle("E11-102502519");
		setSize(600,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		
		add(panel);
	}
}
