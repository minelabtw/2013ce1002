package ce1002.e11.s102502519;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;

public class Panel extends JPanel{
	Random random = new Random();
	int type = random.nextInt(6);
	
	//基本設定
	Panel(){
		Timer time = new Timer(500, taskPerformer);
		setBounds(random.nextInt(450) + 10, random.nextInt(450) + 10, 1000, 1000);
		time.start();
	}
	
	//重劃
	ActionListener taskPerformer = new ActionListener() {
	      public void actionPerformed(ActionEvent evt) {
	    	  repaint();
	      }
	  };
	
	//畫圖
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		switch(type){
		case 0:
			g.setColor(Color.RED);
			g.fillRect(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
		case 1:
			g.setColor(Color.BLUE);
			g.fillRect(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
		case 2:
			g.setColor(Color.GREEN);
			g.fillRect(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
		case 3:
			g.setColor(Color.RED);
			g.fillOval(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
		case 4:
			g.setColor(Color.BLUE);
			g.fillOval(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
		case 5:
			g.setColor(Color.GREEN);
			g.fillOval(random.nextInt(450) + 10, random.nextInt(450) + 10,25, 25);
			type = random.nextInt(6);
			break;
			
		}
	}
}