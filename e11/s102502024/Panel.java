package ce1002.e11.s102502024;
import javax.swing.JPanel;
import javax.swing.Timer;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import java.awt.event.*;

import java.awt.*;
public class Panel extends JPanel{
	int color;
	int graph;
	int x;
	int y;
	private Timer timer=new Timer(1000,new TimerListener());
	
	
	public Panel()
	{
		
		Random r=new Random();
		color=r.nextInt(3);
		graph=r.nextInt(2);
		x=r.nextInt(450);
		y=r.nextInt(450);
		setBounds(0,0,500,500);
		timer.start();
	}
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		Random r=new Random();
		color=r.nextInt(3);
		graph=r.nextInt(2);
		x=r.nextInt(450);
		y=r.nextInt(450);
		if(color==0)
		{
			g.setColor(Color.red);
		}
		else if(color==1)
		{
			g.setColor(Color.blue);
		}
		else
		{
			g.setColor(Color.green);
		}
		if(graph==0)
		{
			g.fillRect(x, y,30,30);
		}
		else
		{
			g.fillOval(x, y, 30, 30);
		}
	}
}
