package ce1002.e11.s102502035;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;

public class MyFrame extends JFrame {
	public MyFrame() {
		setTitle("E11");
		setSize(400, 300);
		setLayout(null);
		ActionListener actionListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				repaint();
			}
		};
		final Timer timer = new Timer(1000, actionListener);
		timer.start();
	}

	public void paint(Graphics g) {
		super.paintComponents(g);
		Random rand = new Random();
		int color = rand.nextInt(3);
		int shape = rand.nextInt(2);
		switch (color) {
		case 0:
			g.setColor(Color.red);
			break;
		case 1:
			g.setColor(Color.green);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		default:
			break;
		}
		switch (shape) {
		case 0:
			g.fillRect(50 * rand.nextInt(7) + 10, 50 * rand.nextInt(5) + 32,
					50, 50);
			break;
		case 1:
			g.fillOval(50 * rand.nextInt(7) + 10, 50 * rand.nextInt(5) + 32,
					50, 50);
			break;
		default:
			break;
		}
	}
}
