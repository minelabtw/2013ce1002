package ce1002.e11.s102502043;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyFrame(){
		setSize(500,500) ;
		
		MyPanel panel=new MyPanel() ;
		
		add(panel) ;
		
		setDefaultCloseOperation(EXIT_ON_CLOSE) ;
		
		setVisible(true) ;
	}
}
