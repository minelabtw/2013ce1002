package ce1002.e11.s102502043;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	private Timer timer=new Timer(500,new TimerListener()) ;
	private Random random=new Random() ;
	private int number=0 ;
	private int x=0 ;
	private int y=0 ;
	private int kind=0 ;
	
	MyPanel(){
		setSize(500,500) ;
		
		timer.start() ;
	}
	
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			repaint() ;
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		x=random.nextInt()%50 ;
		y=random.nextInt()%50 ;
		kind=random.nextInt() ;
		number=random.nextInt() ;
		
		if (kind%2==0){
			if (number%3==0){
				g.setColor(Color.RED) ;
			}
			
			else if (number%3==1){
				g.setColor(Color.BLUE) ;
			}
			
			else {
				g.setColor(Color.GREEN) ;
			}
			
			g.fillOval(x*5,y*5 ,20 ,20 ) ;
			
			
		}
		
		else {
			if (number%3==0){
				g.setColor(Color.RED) ;
			}
			
			else if (number%3==1){
				g.setColor(Color.BLUE) ;
			}
			
			else {
				g.setColor(Color.GREEN) ;
			}
			
			g.fillRect(x*5, y*5, 20, 20) ;
		}
		
		g.dispose();
	}
}
