package ce1002.e11.s102502507;
import java.awt.Component;

import javax.swing.*;

public class MyFrame extends JFrame{

	public MyFrame(){
		setLocationRelativeTo(null) ;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		setSize(500,500) ;
		setVisible(true) ;
		MyPanel panel = new MyPanel() ;
		add(panel) ;
		
	}

}
