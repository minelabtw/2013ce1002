package se1002.e11.s1025025506;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
public class MyFrame  extends JFrame{
	  MyFrame()
	  {
	    add(new RIP());
	  }
	  
	  class TL
	    implements ActionListener
	  {
	    TL(){}
	    public void actionPerformed(ActionEvent e)
	    {
	      MyFrame.this.repaint();
	    }
	  }
	  
	  class RIP
	    extends JPanel
	  {
	    private Timer timer = new Timer(1000, new MyFrame.TL());
	    
	    RIP()
	    {
	      this.timer.start();
	    }
	    
	    protected void paintComponent(Graphics g)
	    {
	      super.paintComponent(g);
	      
	      int GW = 50;
	      int GH = 50;
	      
	      int x = (int)(Math.random() * (getWidth() - GW) + 1);
	      int y = (int)(Math.random() * (getHeight() - GH) + 1);
	      int color = (int)(Math.random() * 3 + 1);
	      int shape = (int)(Math.random() * 2 + 1);
	      switch (color)
	      {
	      case 1: 
	        g.setColor(Color.RED);
	        break;
	      case 2: 
	        g.setColor(Color.GREEN);
	        break;
	      case 3: 
	        g.setColor(Color.BLUE);
	      }
	      switch (shape)
	      {
	      case 1: 
	        g.fillOval(x, y, GW, GH);
	        break;
	      case 2: 
	        g.fillRect(x, y, GW, GH);
	      }
	    }
	  }
	  
	  
	}
	

