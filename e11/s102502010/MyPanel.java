package ce1002.e11.s102502010;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
		Random ran = new Random();
		int type = ran.nextInt(6);
		
		//基本設定
		MyPanel(){
			Timer time = new Timer(500, taskPerformer);
			setBounds(0, 0, 500, 500);
			time.start();
		}
		
		//重劃
		ActionListener taskPerformer = new ActionListener() {
		      public void actionPerformed(ActionEvent evt) {
		    	  repaint();
		      }
		  };
		
		//畫圖
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			switch(type){
			case 0:
				g.setColor(Color.RED);
				g.fillRect(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 1:
				g.setColor(Color.BLUE);
				g.fillRect(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 2:
				g.setColor(Color.GREEN);
				g.fillRect(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 3:
				g.setColor(Color.RED);
				g.fillOval(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 4:
				g.setColor(Color.BLUE);
				g.fillOval(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 5:
				g.setColor(Color.GREEN);
				g.fillOval(ran.nextInt(300) + 10, ran.nextInt(300) + 10,25, 25);
				type = ran.nextInt(6);
				break;
				
			}
		}
	}
