package ce1002.e11.s102502010;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame()
	{
		MyPanel panel = new MyPanel();
		setLayout(null);
		setTitle("E11-102502010");
		setBounds(0 , 0 , 400 , 400);
		setVisible(true);  //setVisible
		setDefaultCloseOperation(EXIT_ON_CLOSE);  //close
		add(panel);
	}
}
