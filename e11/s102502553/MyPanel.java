package ce1002.e11.s102502553;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel{
	
	private Timer timer = new Timer(1000,new TimerListener());//建立Timer，以1000毫秒為單位建立TimerListener
	Random ran = new Random();//隨機整數變數
	
	MyPanel()
	{
		setSize(600,400);
		timer.start();//start timer
	}
	
	protected void paintComponent(Graphics g)//畫圖
	{
		super.paintComponent(g);
		
		if(ran.nextInt(6) == 0)
		{
			g.setColor(Color.RED);
			g.fillOval(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		else if(ran.nextInt(6) == 1)
		{
			g.setColor(Color.GREEN);
			g.fillOval(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		else if(ran.nextInt(6) == 2)
		{
			g.setColor(Color.BLUE);
			g.fillOval(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		else if(ran.nextInt(6) == 3)
		{
			g.setColor(Color.RED);
			g.fillRect(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		else if(ran.nextInt(6) == 4)
		{
			g.setColor(Color.GREEN);
			g.fillRect(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		else
		{
			g.setColor(Color.BLUE);
			g.fillRect(ran.nextInt(300),ran.nextInt(200),100,100);
		}
		
	}
	
	class TimerListener implements ActionListener//實作TimerListener，繼承ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
		repaint();
		}
	}
	

}
