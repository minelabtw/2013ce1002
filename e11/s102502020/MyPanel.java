package ce1002.e11.s102502020;

import java.awt.*;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.*;
import java.awt.event.*;

public class MyPanel extends JPanel{
	private Timer timer = new Timer(1000, new TimerListener());
	
	protected JLabel image = new JLabel();

	public MyPanel() {
		image.setSize(50, 50);
		timer.start();                 
	}
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	@Override
	protected void paintComponent(Graphics g){     //�e��
		super.paintComponent(g);
		int color = 0;
		int graph = 0;
		Random random = new Random();
		int x = random.nextInt(420);
		int y = random.nextInt(420);
		color = random.nextInt(3);
		if(color==0){
			g.setColor(Color.RED);
		}else if(color==1){
			g.setColor(Color.GREEN);
		}else{
			g.setColor(Color.BLUE);
		}
		graph = random.nextInt(2);
		if(graph==0){
			g.fillRect(x, y, 50, 50);
		}else{
			g.fillOval(x, y, 50, 50);
		}
	}
	
}
