package ce1002.e11.s102502007;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	/**
	 * 
	 */
	private Random ran = new Random();//random obj
	private int type = ran.nextInt(6);//six different kinds of image
	
	MyPanel()
	{
		Timer time = new Timer(1000, taskPerformer);
		setBounds(0, 0, 500, 500);
		time.start();
	}
	
	ActionListener taskPerformer = new ActionListener() {
	      public void actionPerformed(ActionEvent evt) {
	    	  repaint();
	      }
	  };
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		switch(type){
		 /* 
		  *decide the location of the image randomly
		  *
		  */
		case 0:
			g.setColor(Color.RED);
			g.fillRect(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		case 1:
			g.setColor(Color.BLUE);
			g.fillRect(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		case 2:
			g.setColor(Color.GREEN);
			g.fillRect(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		case 3:
			g.setColor(Color.RED);
			g.fillOval(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		case 4:
			g.setColor(Color.BLUE);
			g.fillOval(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		case 5:
			g.setColor(Color.GREEN);
			g.fillOval(ran.nextInt(400) + 10, ran.nextInt(400) + 10, 30, 30);
			type = ran.nextInt(6);
			break;
		default:
			break;		
		}
	}
}