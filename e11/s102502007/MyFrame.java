package ce1002.e11.s102502007;
import javax.swing.*;
public class MyFrame extends JFrame {
	MyFrame()
	{
		super("E11-102502007");
		setSize(500,500);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		this.add(new MyPanel());
	}
	
}
