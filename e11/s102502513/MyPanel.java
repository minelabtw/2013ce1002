package ce1002.e11.s102502513;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.util.*;
import java.util.Random;

public class MyPanel extends JPanel {

	Timer timer = new Timer();
	Random rand = new Random();

	int x = 700;
	int y = 500;
	
	int color = rand.nextInt(3);
	int shape = rand.nextInt(2);

	public MyPanel()
	{
		setBounds(0,0,800,600);
		change();
		timer.schedule(new Task(), 0, 1000);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		if (color == 0)
			g.setColor(Color.red);
		if (color == 1)
			g.setColor(Color.blue);
		if (color == 2)
			g.setColor(Color.green);

		if (shape == 0)
			g.fillRect(rand.nextInt(x), rand.nextInt(y), 50, 50);
		if (shape == 1)
			g.fillOval(rand.nextInt(x), rand.nextInt(y), 50, 50); //隨機位置

	}
	
	private void change() 
	{
		color = rand.nextInt(3);
		shape = rand.nextInt(2); //隨機顏色、形狀
		this.repaint();
	}

	class Task extends TimerTask  //重複run 
	{
		@Override
		public void run() 
		{
			MyPanel.this.change();
		}	
	}

}