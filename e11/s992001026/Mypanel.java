package ce1002.e11.s992001026;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.event.*;
import java.awt.*; // Graphics
import java.util.Random;

public class Mypanel extends JPanel{
	
	
	private Timer timer = new Timer(500, new TimerListener() ) ;
	Random ran = new Random(); 
	
	protected void paintComponent(Graphics g){
		int type = ran.nextInt(2) ;
		int color =ran.nextInt(3) ;
		timer.start();
		super.paintComponent(g);
		
		// set location
		int x = ran.nextInt((getWidth()-40));
		int y = ran.nextInt((getHeight()-40));
		//System.out.println(getWidth());
		//System.out.println(getHeight());
		// set color
		if (color == 0) g.setColor(Color.BLUE);
		else if ( color == 1) g.setColor(Color.RED);
		else g.setColor(Color.GREEN);
		// draw
		if (type == 0 )g.fillRect( x , y , 30, 30);
		else g.fillOval(x, y, 30, 30) ;
		
	}
	
	
	

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			repaint();
		}	

	}
}
