package ce1002.e11.s102502032;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class E11
{
	public static void main(String[] args)
	{
		JFrame frame = new JFrame();
		MyPanel panel = new MyPanel();
		frame.setVisible(true);
		frame.setSize(550, 560);
		frame.setLayout(null);
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static class MyPanel extends JPanel
	{
		Timer	timer	= new Timer(1000, new timerLisenter());
		Random	rnd		= new Random();
		int		counter	= 0;

		// constructor
		public MyPanel()
		{
			setBorder(BorderFactory.createLineBorder(Color.pink));
			setSize(500, 500);
			setLocation(10, 10);
			setVisible(true);
			setLayout(null);
			timer.start();
		}

		protected class timerLisenter implements ActionListener
		{
			@Override
			public void actionPerformed(ActionEvent a)
			{
				counter ++;
				repaint();
			}
		}

		@Override
		public void paintComponent(Graphics g)
		{// set color
			super.paintComponent(g);
			switch (rnd.nextInt() % 3)
			{
				case 0:
					g.setColor(Color.red);
					break;
				case 1:
					g.setColor(Color.green);
					break;
				case 2:
					g.setColor(Color.yellow);
					break;
				default:
					g.setColor(Color.black);
					break;
			}
			// set location
			int temp = Math.abs(rnd.nextInt());
			Point pt = new Point(Math.abs(rnd.nextInt() % 450), Math.abs(rnd
					.nextInt()) % 450);
			if (temp % 2 == 0)
			{
				g.fillOval(pt.x, pt.y, 30, 30);
			}
			else
			{
				g.fillRect(pt.x, pt.y, 30, 30);
			}
		}
	}
}
