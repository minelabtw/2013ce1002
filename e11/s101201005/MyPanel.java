package ce1002.e11.s101201005;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener{

	private int x,y,side=50;
	private Color color; 
	private Timer timer = new Timer(1000, this);
	MyPanel(){
		setLayout(null);
		setBounds(0, 0, 480, 500);
		setGraphic();
		timer.start();
	}

	private void setGraphic(){
		x = (int)(Math.random()*(getWidth()-side));
		y = (int)(Math.random()*(getHeight()-side));
		switch((int)(Math.random()*3)){
			case 0 : color = Color.red;
					 break;
			case 1 : color = Color.green;
			 		 break;
			case 2 : color = Color.blue;
			 		 break;
		}
	}
	public void actionPerformed(ActionEvent e) {
		setGraphic();
		repaint();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(color);
		//paint a rectangle or oval
		if((int)(Math.random()*2) == 0)
			g.fillOval(x, y, side, side);
		else
			g.fillRect(x, y, side, side);
	}
}
