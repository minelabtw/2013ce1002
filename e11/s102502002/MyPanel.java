package ce1002.e11.s102502002;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	Random ran = new Random();
	
	class TimeListener implements ActionListener{ // action performed through time
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	Timer timer = new Timer(700, new TimeListener());
	
	MyPanel(){ // panel settings
		setBounds(0,0,500,500);
		setLayout(null);
		setBackground(Color.WHITE);
		timer.start(); // start the timer
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int px = ran.nextInt(10); // position x
		int py = ran.nextInt(10); // position y
		int color = ran.nextInt(7); // color
		int shape = ran.nextInt(3); // shape
		setColor(color,g);
		setShape(shape,px,py,g);
	}
	
	protected void setShape(int s, int x, int y, Graphics g){ // choose shape according to random numbers
		if(s==0)
			g.fillOval(50*x, 50*y, 50, 50);
		else if(s==1)
			g.fillRect(50*x, 50*y, 50, 50);
		else
			g.fillArc(50*x, 50*y, 50, 50, 86, 257);
	}
	protected void setColor(int c, Graphics g){ // choose color according to random numbers
		if (c == 1) {
			g.setColor(Color.RED);
		} else if (c == 2) {
			g.setColor(Color.ORANGE);
		} else if (c == 3) {
			g.setColor(Color.YELLOW);
		} else if (c == 4) {
			g.setColor(Color.GREEN);
		} else if (c == 5) {
			g.setColor(Color.BLUE);
		} else if (c == 6) {
			g.setColor(Color.CYAN);
		} else {
			g.setColor(Color.MAGENTA);
		}
	}
}