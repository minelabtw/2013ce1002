package ce1002.e11.s102502002;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel panel = new MyPanel();
	MyFrame(){ // set frame settings
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(200,200,515,540);
		setTitle("E11-102502002");
		setVisible(true);
		add(panel); // add panel to frame
	}
}
