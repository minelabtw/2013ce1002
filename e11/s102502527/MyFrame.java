package ce1002.e11.s102502527;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	MyFrame()
	{
		setTitle("E11-102502527");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
		this.setLocationRelativeTo(null);
		add(new MyPanel(500, 500));
	}
}