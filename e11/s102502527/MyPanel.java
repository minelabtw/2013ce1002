package ce1002.e11.s102502527;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	Timer timer = new Timer();
	Random rand = new Random();
	
	Color[] colors = {Color.RED, Color.GREEN, Color.BLUE};
	int size = 50;
	int shape;
	int color;
	int X;
	int Y;
	public MyPanel(int width, int height)
	{
		X = width - size;
		Y = height - size;
		
		timer.schedule(new MyTask(), 0, 1000);
		change();
	}
	
	private void change()
	{
		shape = rand.nextInt(2);
		color = rand.nextInt(3);
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(colors[color]);
		if (shape == 1)
		{
			g.fillRect(rand.nextInt(X), rand.nextInt(Y), size, size);
		}
		else
		{
			g.fillOval(rand.nextInt(X), rand.nextInt(Y), size, size);
		}
		g.dispose();
	}
	
	class MyTask extends TimerTask
	{
		@Override
		public void run()
		{
			MyPanel.this.change();
		}
	}
}

