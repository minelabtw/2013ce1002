package ce1002.e11.s102502518;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class MyFrame extends JFrame {
	MyFrame() {	
		setTitle("E11-102502518");
	    setSize(400, 400);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    setVisible(true);
	    add(new RandomImagePanel());
	}

	class RandomImagePanel extends JPanel {
		private Timer timer = new Timer(1000, new MyFrame.TimerListener());

		RandomImagePanel() {
			this.timer.start();
		}

		protected void paintComponent(Graphics g) {
			

			int width = 50;
			int height = 50;

			int x = (int) (Math.random() * (getWidth() - width) + 1.0D);
			int y = (int) (Math.random() * (getHeight() - height) + 1.0D);
			int color = (int) (Math.random() * 3.0D + 1.0D);
			int type = (int) (Math.random() * 2.0D + 1.0D);
			switch (color) {
			case 1:
				g.setColor(Color.BLUE);
				break;
			case 2:
				g.setColor(Color.GREEN);
				break;
			case 3:
				g.setColor(Color.RED);
				break;
			}
			switch (type) {
			case 1:
				g.fillOval(x, y, width, height);
				break;
			case 2:
				g.fillRect(x, y, width, height);
				break;
			}
		}
	}

	class TimerListener implements ActionListener {
		TimerListener() {
		}

		public void actionPerformed(ActionEvent arg0) {
			MyFrame.this.repaint();
		}
	}
}
