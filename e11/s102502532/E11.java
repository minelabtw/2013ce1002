package ce1002.e11.s102502532;
import javax.swing.JFrame;

public class E11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyFrame frame = new MyFrame();     // 建立板子
		MyPanel panel = new MyPanel();
		frame.addP(panel);
		frame.setSize(500, 500);       // frame寫在 main 裡
		frame.setTitle("E11-102502532");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
