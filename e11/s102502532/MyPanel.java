package ce1002.e11.s102502532;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyPanel extends JPanel{
	private Timer timer = new Timer(1000, new TimerListener() );
	private int n1 =0;
	private int n2 =0;
	private int n3 =0;
	
	public MyPanel() {
		
		setSize(500, 500); // 畫布大小
		timer.start();
	}
	
	private class TimerListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); // 改寫 畫圖形
		
		n1 = (int)(Math.random() *10);                        //亂數
		n2 = (int)(Math.random() *1000);
		n3 = (int)(Math.random() *1000);
		
		switch( n1 %3) {
		case 0:
			g.setColor(Color.RED);
			break;
		case 1:
			g.setColor(Color.BLUE);
			break;
		case 2:
			g.setColor(Color.GREEN);
			break;
		}
		
		switch( n1 %2) {
		case 0:
			g.fillOval(n2%500, n3%500, 50, 50);
			break;
		case 1:
			g.fillRect(n2%500, n3%500, 50, 50);
			break;
		
		}
		
	}
	
}
