package ce1002.e11.s102502561;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	Random rr = new Random();
	Timer clock = new Timer(50, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	});

	public MyPanel() {
		setBounds(0, 0, 500, 500);
		setLayout(null);
		setBackground(Color.WHITE);
		clock.start();
	}

	@Override
	protected void paintComponent(Graphics g) {// painter
		super.paintComponent(g);
		int x = rr.nextInt(10);// x
		int y = rr.nextInt(10);// y
		int c = rr.nextInt(7);// color
		int s = rr.nextInt(2);// shape
		setcolor(c, g);
		setshape(s, g, x, y);
	}

	public void setshape(int s, Graphics g, int x, int y) {// setshape
		if (s == 1) {
			g.fillOval(50 * x, 50 * y, 75, 75);
		} else {
			g.fillRect(50 * x, 50 * y, 75, 75);
		}
	}

	public void setcolor(int c, Graphics g) {// setcolor
		if (c == 1) {
			g.setColor(Color.RED);
		} else if (c == 2) {
			g.setColor(Color.ORANGE);
		} else if (c == 3) {
			g.setColor(Color.YELLOW);
		} else if (c == 4) {
			g.setColor(Color.GREEN);
		} else if (c == 5) {
			g.setColor(Color.BLUE);
		} else if (c == 6) {
			g.setColor(Color.CYAN);
		} else {
			g.setColor(Color.MAGENTA);
		}
	}
}
