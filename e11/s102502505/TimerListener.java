package ce1002.e11.s102502505;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.*;

public class TimerListener extends JPanel {

	int color;//設定color顏色
	int shape;//設定shape顏色
	int x;//位置變數
	int y;
	private Timer timer = new Timer(1000, new action());//設定timer，並將時間設為1秒

	public TimerListener() {
		// TODO Auto-generated constructor stub
		timer.start();//開始timer
	}

	@Override
	protected void paintComponent(Graphics g) {//畫圖

		super.paintComponent(g);

		if (color == 1 && shape == 1) {//判斷變數，並劃出圖形
			g.setColor(Color.RED);
			g.fillOval(x, y, 50, 50);

		} else if (color == 1 && shape == 2) {
			g.setColor(Color.RED);
			g.fillRect(x, y, 50, 50);

		} else if (color == 2 && shape == 1) {
			g.setColor(Color.GREEN);
			g.fillOval(x, y, 50, 50);

		} else if (color == 2 && shape == 2) {
			g.setColor(Color.GREEN);
			g.fillRect(x, y, 50, 50);

		} else if (color == 3 && shape == 1) {
			g.setColor(Color.BLUE);
			g.fillOval(x, y, 50, 50);

		} else {
			g.setColor(Color.BLUE);
			g.fillRect(x, y, 50, 50);

		}
	}

	class action implements ActionListener {//每隔一秒會重新執行下列動作

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Random ran = new Random();//四個變數重新設定
			color = ran.nextInt(3) + 1;
			shape = ran.nextInt(2) + 1;
			x = ran.nextInt(400);
			y = ran.nextInt(400);
			repaint();//重畫圖形
		}

	}

}
