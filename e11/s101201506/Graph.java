package ce1002.e11.s101201506;

import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;


public class Graph extends JPanel {



	Graph() {
		setBounds(0, 0, 550, 550);
	}

	public void show() {
		repaint();
	}

	// 設定圖案的隨機位子
	@Override
	protected void paintComponent(Graphics g) {
		paintComponent(g);
		RandomColor color = new RandomColor();
		g.setColor(color.getcolor());
		Random random = new Random();
		int i = random.nextInt(2);
		int x = random.nextInt(500);
		int y = random.nextInt(500);
		if (i == 0) {
			g.fillRect(x, y, 50, 50);
		} 
		else {
			g.fillOval(x, y, 50, 50);
		}

	}
}
