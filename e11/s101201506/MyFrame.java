package ce1002.e11.s101201506;

import java.util.Timer;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	private Graph graph = new Graph();

	MyFrame() {

		setTitle("E11-101201506");
		setLayout(null);
		setSize(500, 500); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		add(graph);
		Timer timer = new Timer();
		timer.schedule(new Time(graph), 0, 1000);
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {

		}
		timer.cancel();

		graph.show();
	}
}
