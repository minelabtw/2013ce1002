package ce1002.e11.s102502005;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class E11 extends JFrame {

	Timer timer;
	JPanel panel;
	
	E11() {
		setTitle("E11-102502502");
		setSize(500, 500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	public static void main(String[] args) {
		E11 frame = new E11();
		MyPanel panel = new MyPanel();//把所有動作都在建構子中完成
		frame.add(panel);
	}

}
