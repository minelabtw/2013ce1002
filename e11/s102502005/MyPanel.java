package ce1002.e11.s102502005;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener {
	Timer timer;
	int x;
	int y;
	int colorR ;
	int colorG ;
	int colorB ;
	int colorA ;
	int shape ;
	Random random;

	public MyPanel() {
		random = new Random();

		timer = new Timer(1000, this);
		timer.start();
		actionPerformed(null);//Swingtimer沒辦法在timer.start()時馬上執行ActionPerformed，
							  //一定要經過設定的延遲時間才會呼叫ActionPeerformed，這裡只好手動呼叫...

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		x = Math.abs(random.nextInt());
		x = x % (this.getWidth() - 50);

		y = Math.abs(random.nextInt());
		y = y % (this.getHeight() - 50);

		colorR = random.nextInt(256);
		colorG = random.nextInt(256);
		colorB = random.nextInt(256);
		colorA = random.nextInt(256);
		shape = random.nextInt(2);
		repaint();
	}

	public void paint(Graphics graphics) {
		super.paint(graphics);
		graphics.setColor(new Color(colorR, colorG, colorB, 255));
		switch (shape) {
		case 0:
			graphics.fillOval(x, y, 50, 50);
			break;
		case 1:
			graphics.fillRect(x, y, 50, 50);
			break;
		default:
			graphics.drawString("Default", 0, this.getHeight() / 2);
			break;
		}

	}
}
