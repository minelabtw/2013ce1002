package ce1002.e11.s102502509;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.Timer;

import javax.swing.JPanel;

public class Mypanel extends JPanel {
	Random rd = new Random();
	int number = rd.nextInt(6);// random the change number

	Mypanel() {
		Timer time = new Timer(1000, taskPerformer);
		setBounds(0, 0, 600, 600);
		time.start();

	}

	ActionListener taskPerformer = new ActionListener() // action event
	{
		public void actionPerformed(ActionEvent evt) {
			repaint();
		}
	};// repaint the article after the random

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);// switch the color and mode
		switch (number) {
		case 0:
			g.setColor(Color.RED);
			g.fillRect(rd.nextInt(500) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;// remember to break
		case 1:
			g.setColor(Color.BLUE);
			g.fillRect(rd.nextInt(500) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;
		case 2:
			g.setColor(Color.GREEN);
			g.fillRect(rd.nextInt(500) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;
		case 3:
			g.setColor(Color.RED);
			g.fillOval(rd.nextInt(500) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;
		case 4:
			g.setColor(Color.BLUE);
			g.fillOval(rd.nextInt(500) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;
		case 5:
			g.setColor(Color.GREEN);
			g.fillOval(rd.nextInt(450) + 10, rd.nextInt(450) + 10, 35, 35);
			number = rd.nextInt(6);
			break;

		}
	}
}
