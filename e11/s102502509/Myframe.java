package ce1002.e11.s102502509;

import javax.swing.JFrame;

public class Myframe extends JFrame {
	protected Mypanel p = new Mypanel();// call the mypanel

	Myframe() {
		setLayout(null);
		setBounds(50, 50, 600, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(p);// add the panel

		setVisible(true);
	}
}
