package ce1002.e11.s102502027;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;


public class E11 extends JPanel{
	Random random = new Random();
	Timer timer =new Timer(1000,new TimerListen());

	public E11(){

			setSize(600,600);
			
		}
	public void start(){
		timer.start();
	}
		
	public static void main(String[] args) {
		E11 p =new E11();
		JFrame frame =new JFrame();
		frame.setTitle("E11-102502027");
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setSize(600,600);
		frame.add(p);
		frame.setVisible(true);
		p.start();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		int a=random.nextInt(6);
		int r=random.nextInt(400);
		int b=random.nextInt(400);
		switch(a){
		case 1 :
		g.setColor(Color.green);
		g.fillRect(r,b,100,100);
		break;
		case 2 :
			g.setColor(Color.red);
			g.fillRect(r,b,100,100);
			break;
		case 3 :
			g.setColor(Color.red);
			g.fillOval(r,b,100,100);
			break;
		case 4 :
			g.setColor(Color.blue);
			g.fillRect(r,b,100,100);
			break;
		case 5 :
			g.setColor(Color.green);
			g.fillOval(r,b,100,100);
			
			break;
		case 0 :
			g.setColor(Color.blue);
			g.fillOval(r,b,100,100);

			break;
		}
		
	}
	
	class TimerListen implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
}
