package ce1002.e11.s102502514;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	int horizontal = 450;
	int vertical = 450;
	MyPanel(){
		setLayout(new FlowLayout());
		setBounds(0, 0, horizontal, vertical);
		
	}
	
	javax.swing.Timer timer = new javax.swing.Timer(1000, new TimerListener());
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		timer.start();  //開始使用Timer
		double randomnumber = Math.random();
		if(randomnumber < 0.33){
			g.setColor(Color.cyan);  //設定青綠色
		}
		else if(randomnumber < 0.66){
			g.setColor(Color.gray);  //設定灰色
		}
		else{
			g.setColor(Color.magenta);  //設定洋紅色
		}
		
		if(randomnumber < 0.5){
			g.fillRect((int) (Math.random()*(horizontal-50)), (int) (Math.random()*(vertical-50)), 50, 50);
		}
		else {
			g.fillOval((int) (Math.random()*(horizontal-50)), (int) (Math.random()*(vertical-50)), 50, 50);
		}
			
	}
	
	public class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			repaint();
		}

	}
}
