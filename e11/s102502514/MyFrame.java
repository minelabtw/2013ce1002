package ce1002.e11.s102502514;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame(){
		MyPanel panel = new MyPanel();
		setLayout(null);
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		add(panel);
		
		setVisible(true);
	}
}
