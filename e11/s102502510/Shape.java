package ce1002.e11.s102502510;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Shape extends JPanel {
	Timer t = new Timer(1000, new TimerListener());// Timer(間隔時間,執行事務)

	Shape() {
		t.start();
	}

	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		int x = (int) (Math.random() * (getWidth() - 50) + 1);
		int y = (int) (Math.random() * (getHeight() - 50) + 1);
		int type = (int) (Math.random() * 2 + 1);
		int color = (int) (Math.random() * 5 + 1);
		switch (color) {
		case 1:
			g.setColor(Color.red);
			break;
		case 2:
			g.setColor(Color.blue);
			break;
		case 3:
			g.setColor(Color.black);
			break;
		case 4:
			g.setColor(Color.green);
			break;
		case 5:
			g.setColor(Color.yellow);
			break;

		}
		switch (type) {
		case 1:
			g.fillRect(x, y, 50, 50);
			break;
		case 2:
			g.fillOval(x, y, 50, 50);
			break;
		}
	}

	class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			repaint();
		}
	}// 做protected void paintComponent
}