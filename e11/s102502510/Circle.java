package ce1002.e11.s102502510;

import java.awt.Color;
import java.awt.Graphics;

public class Circle extends Shape{
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.black);
		g.fillOval(100,100,50,50);
	}
}
