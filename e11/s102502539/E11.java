package ce1002.e11.s102502539;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class E11 extends JPanel
{
	int x_ran;
	int y_ran;
	int color_ran;
	int shape_ran;
	Random ran = new Random();
	
	public Timer timer = new Timer(1000, new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			repaint();			
		}
	});
	
	public E11()
	{
		timer.start();
		setPreferredSize(new Dimension(500,500));
	}
	
	public static void main(String[] args) 
	{
		JFrame frame = new JFrame();
		
		E11 panel=new E11();
		frame.add(panel);
		frame.setVisible(true);
		frame.pack();
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		x_ran = ran.nextInt(451);
		y_ran = ran.nextInt(451);
		
		color_ran = ran.nextInt(3);
		shape_ran = ran.nextInt(2);
		
		/* �ü��C�� */
		switch(color_ran)
		{
		case 0:
			g.setColor(Color.RED);
			break;
		case 1:
			g.setColor(Color.GREEN);
			break;
		case 2:
			g.setColor(Color.BLUE);
			break;
		}
		
		/* �üƧΪ� */
		if (shape_ran == 0)
			g.fillOval(x_ran, y_ran, 50, 50);
		else 
			g.fillRect(x_ran, y_ran, 50, 50);
	}
}


