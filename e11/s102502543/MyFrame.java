package ce1002.e11.s102502543;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyPanel panel = new MyPanel();

	MyFrame() {
		setBounds(0, 0, 600, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setLayout(null);
		add(panel);
	}
}
