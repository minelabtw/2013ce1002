package ce1002.e11.s102502543;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	Timer timer = new Timer(1000, new TimerListener());

	MyPanel() {
		setBounds(0, 0, 600, 600);
		setLayout(null);
		timer.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Random ran = new Random();
		int a = ran.nextInt(3);
		if (a == 0)
			g.setColor(Color.RED);
		if (a == 1)
			g.setColor(Color.GREEN);
		if (a == 2)
			g.setColor(Color.BLUE);
		int b = ran.nextInt(2);
		if (b == 0)
			g.fillOval(ran.nextInt(470), ran.nextInt(470), 100, 100);
		if (b == 1)
			g.fillRect(ran.nextInt(470), ran.nextInt(470), 100, 100);

	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}
