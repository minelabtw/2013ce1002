package ce1002.e11.s1025020208;
import javax.swing.* ;
import java.awt.* ;
public class E11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        JFrame frame = new JFrame() ; //設定框架
        frame.setSize(500,400) ;
        frame.setLayout(null);
        frame.setLocationRelativeTo(null) ;
        MyPanel panel = new MyPanel() ; //建立版面
        frame.add(panel) ;  //版面加到框架中
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
	}

}
