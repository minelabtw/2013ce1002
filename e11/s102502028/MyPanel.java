package ce1002.e11.s1025020208;
import javax.swing.* ;
import java.awt.* ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class MyPanel extends JPanel{
	
	int x = 0 ;  //宣告參數
	int y = 0 ;	
	int color = 0 ;
	int picture = 0 ;
	Timer timer = new Timer(1000,new TimerListener()) ; //延遲一秒
	
	MyPanel() //
	{
		timer.start() ; //timmer開始
	}
	
	void setX()  //設定圖形x的位子
	{
		x = (int)(Math.random()*1000);
		while (x > 400)
		{
			x = (int)(Math.random()*1000);
		}
	}
	
	void setY()  //設定圖形y的位子
	{
		y = (int)(Math.random()*1000);
		while (y > 300)
		{
			y = (int)(Math.random()*1000);
		}
	}
	  
	void setColor()  //設定顏色
	{
		color = (int)(Math.random()*10%3);	
	}
	 
	void setPicture()  //設定圖形
	{
		picture = (int)(Math.random()*10%2);	
	}
	
	protected void paintComponent(Graphics g)  //畫圖
	{
		super.paintComponent(g) ;
		if (color == 0 && picture == 0)
		{
			g.setColor(Color.RED) ;
			g.fillOval(0,0,50,50) ;
		}
		else if (color == 0 && picture == 1)
		{
			g.setColor(Color.RED) ;
			g.fillRect(0,0,50,50) ;
		}
		else if (color == 1 && picture == 0)
		{
			g.setColor(Color.GREEN) ;
			g.fillOval(0,0,50,50) ;
		}
		else if (color == 1 && picture == 1)
		{
			g.setColor(Color.GREEN) ;
			g.fillRect(0,0,50,50) ;
		}
		else if (color == 2 && picture == 0)
		{
			g.setColor(Color.BLUE) ;
			g.fillOval(0,0,50,50) ;
		}
		else if (color == 2 && picture == 1)
		{
			g.setColor(Color.BLUE) ;
			g.fillRect(0,0,50,50) ;
		}
	}
	
	class TimerListener implements ActionListener{  //每隔一秒隨機在視窗中產生不同顏色之圖形
		@Override
		public void actionPerformed(ActionEvent e) {
			setX() ;
			setY() ;
			setColor() ;
			setPicture() ;
			setBounds(x,y,50,50) ;
			repaint() ;
		}
		
	}
	
}
