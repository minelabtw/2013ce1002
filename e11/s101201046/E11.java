package ce1002.e11.s101201046;

import java.awt.*;
import javax.swing.*;


public class E11 extends JFrame{
	
	E11() {
		this.setVisible(true);
		this.setPreferredSize(new Dimension(500, 500)); //set the frame size
		
		this.setContentPane(new MyPanel()); //set content pane
		
		this.pack(); 
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E11();

	}

}
