package ce1002.e11.s102502524;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	Timer timer = new Timer(1000, new TimerListener()); //時間控制器
	Random rand = new Random(); //隨機
	MyPanel () {
		setSize(500, 500); //大小
		setLocation(5, 5); //位置
		timer.start(); //打開時間控制器
	}
	
	//繪圖函數，不斷的利用隨機改變畫出來的圖的位置、形狀和顏色
	protected void paintComponent ( Graphics g ) {
		super.paintComponent(g);
		if ( rand.nextInt(3) == 0 )
		{
			g.setColor(Color.RED);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
		else if ( rand.nextInt(3) == 1 )
		{
			g.setColor(Color.GREEN);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
		else 
		{
			g.setColor(Color.BLUE);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
	}
	
	//每當時間一到，則會call repaint() 函數重新繪圖
	class TimerListener implements ActionListener {
		public void actionPerformed( ActionEvent e ) {
			repaint();
		}
	}
}
