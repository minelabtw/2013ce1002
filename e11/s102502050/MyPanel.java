package ce1002.e11.s102502050;
import javax.swing.*;
import java.awt.*;
import java.util.Random;
public class MyPanel extends JPanel{
	int color=0;
	int shape=0;
	
	public MyPanel()
	{
		setLayout(null);
		setBounds(50,50,400,400);
			
		setVisible(true);
	}
	
	//隨機設定color和shape的值
	private void randcolor()
	{
		Random random = new Random();
		color = random.nextInt(3);
	}
	private void randshape()
	{
		Random random = new Random();
		shape = random.nextInt(2);
	}
	public void paintComponent(Graphics g)
	{
		randshape();
		randcolor();
		switch(color)//依據color改變顏色
		{
		case 0:g.setColor(Color.RED);
			break;
		case 1:g.setColor(Color.GREEN);
			break;
		case 2:g.setColor(Color.BLUE);
			break;
		}
		switch(shape)//依據shape改變圖形
		{
		case 0:g.fillRect(0, 0, 300, 300);
			break;
		case 1:g.fillOval(0, 0, 200, 200);
			break;
		
		}
		
		
	}
}
