package ce1002.e11.s102502050;
import java.awt.event.*;
import javax.swing.*;
public class MyFrame extends JFrame{
	MyPanel panel = new MyPanel();
	public MyFrame()
	{
		setLayout(null);
		setBounds(100,100,500,500);
		
		add(panel);
		Timer timer = new Timer(1000,new TimeListener());//每隔一秒產生事件
		timer.start();//
		setVisible(true);
	}
	class TimeListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
}
