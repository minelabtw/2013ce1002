package ce1002.e11.s102502034;

import java.util.Random;

import javax.swing.Timer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public class Panel extends JPanel {
	private Timer timer = new Timer(1000, new TimerListener());

	Panel() {
		timer.start();
		
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Random ran = new Random();
		Random rancolor = new Random();
		Random ranshape = new Random();
		if(ranshape.nextInt(2)==0)
		{
			if (rancolor.nextInt(3) == 0)
					
				g.setColor(Color.PINK);
			else if (rancolor.nextInt(3) == 1)
				g.setColor(Color.blue);
			else 
				g.setColor(Color.yellow);
			g.fillRect(ran.nextInt(240), ran.nextInt(240), 50, 50);
		}
		else 
		{
			if (rancolor.nextInt(3) == 0)
				
				g.setColor(Color.PINK);
			else if (rancolor.nextInt(3) == 1)
				g.setColor(Color.blue);
			else 
				g.setColor(Color.yellow);
			g.fillOval(ran.nextInt(240), ran.nextInt(240), 50, 50);
			
		}
	}
	
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
			
		}

	}
}
