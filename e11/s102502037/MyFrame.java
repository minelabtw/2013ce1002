package ce1002.e11.s102502037;

import java.awt.*;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
public class MyFrame extends JFrame{
	MyFrame()
	  {
	    add(new Random());
	  }
	class Random extends JPanel
  {
		 private Timer timer = new Timer(1000, new TimerListener());
		    
		    Random()
		    {
		     timer.start();
		    }
    protected void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      
      int Width = 50;
      int Height = 50;
      

      int x = (int)(Math.random() * (getWidth() - Width) + 1);
      int y = (int)(Math.random() * (getHeight() - Height) + 1);
      int color = (int)(Math.random() * 3 + 1);
      int type = (int)(Math.random() * 2 + 1);
      switch (color)
      {
      case 1: 
        g.setColor(Color.RED);
        break;
      case 2: 
        g.setColor(Color.GREEN);
        break;
      case 3: 
        g.setColor(Color.BLUE);
      }
      switch (type)
      {
      case 1: 
        g.fillOval(x, y, Width, Height);
        break;
      case 2: 
        g.fillRect(x, y, Width, Height);
      }
    }
  }
  
  class TimerListener implements ActionListener
  {
    TimerListener() {}
    
    public void actionPerformed(ActionEvent e)
    {
      MyFrame.this.repaint();
    }
  }

}
