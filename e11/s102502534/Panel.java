package ce1002.e11.s102502534;

import java.awt.*;
import java.awt.event.*;//一定要寫
import javax.swing.*;
import javax.swing.Timer;//可寫可不寫

public class Panel extends JPanel {
	
	private Timer timer = new Timer(1000, new TimerListener());

	Panel() {
		timer.start();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		double color = Math.random();
		if (color < 0.33) {
			g.setColor(Color.RED);
		} else if (color > 0.33 || Math.random() < 0.66) {
			g.setColor(Color.GREEN);
		} else {
			g.setColor(Color.BLUE);
		}
		if (Math.random() < 0.5) {
			g.fillRect((int) (Math.random() * 200),
					(int) (Math.random() * 250), 50, 50);
		} else {
			g.fillOval((int) (Math.random() * 300),
					(int) (Math.random() * 300), 50, 50);
		}
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

}
