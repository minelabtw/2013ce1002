package ce1002.e11.s102502512;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import javax.swing.*;

public class Panel extends JPanel{
	private int x = 20;	
	private int y = 20;
	private int rads = 0;
	private int radc = 0;
	private Timer timer = new Timer(1000, new TimerListener());		//make timer variable and repaint the shape

	Panel ()
	{
		setLayout(null);
		setSize(500,500);
		timer.start();				//Let the timer be inspired
	}

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Random rad = new Random();
		x = rad.nextInt(450);
		y = rad.nextInt(450);
		rads = rad.nextInt(2);
		radc = rad.nextInt(3);
		/*
		if(color==0)  
			g.setColor(Color.RED);
		else if (color==1) 
			g.setColor(Color.BLUE);
		else
			g.setColor(Color.GREEN);
		
		if (shape==0) 
			g.fillRect(x,y,50,50);
		else
			g.fillOval(x,y,50,50);
		 */
		if(rads == 0)
		{
			if(radc == 0)
			{
				g.setColor (Color.RED);
				g.fillOval(x, y, 30, 30);
			}
			if(radc == 1)
			{
				g.setColor (Color.GREEN);
				g.fillOval(x, y, 30, 30);
			}
			if(radc == 2)
			{
				g.setColor (Color.BLUE);
				g.fillOval(x, y, 30, 30);
			}
		}
		if(rads == 1)
		{
			if(radc == 0)
			{
				g.setColor (Color.RED);
				g.fillRect(x, y, 30, 30);
			}
			if(radc == 1)
			{
				g.setColor (Color.GREEN);
				g.fillRect(x, y, 30, 30);
			}
			if(radc == 2)
			{
				g.setColor (Color.BLUE);
				g.fillRect(x, y, 30, 30);
			}	
		}
		
	}

	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
			repaint();
		}
	
	}//end of timerlistener class
}
