package ce1002.e11.s102502048;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class E11 extends JFrame 
{
	public E11()
	{
		//set panel
		setLayout(null);
		MyPanel p1 = new MyPanel();
		p1.setBounds(0,0,800,600);
		add(p1);		
	}
	/* Main method */
	public static void main(String[] args) 
	{
		E11 frame = new E11();
		frame.setTitle("E11-102502048");
		frame.setLayout(null);
		frame.setBounds(0, 0, 800, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	static class MyPanel extends JPanel
	{
		//Create a timer with delay 1000 ms
		private Timer timer = new Timer(1000, new TimerListener());
		public MyPanel()
		{
			// Start timer for animation
			timer.start();			
		}
		@Override //Draw graph
		protected void paintComponent(Graphics g) 
		{	
			super.paintComponent(g);
			int color = (int)(Math.random()*3);
			if(color == 0)
				g.setColor(Color.YELLOW);
			else if(color == 1)
				g.setColor(Color.BLUE);
			else
				g.setColor(Color.RED);
			int x = (int)(Math.random()*700);
			int y = (int)(Math.random()*500);
			int type = (int)(Math.random()*2);
			if(type == 0)
				g.fillOval(x, y, 80, 80);
			else
				g.fillRect(x, y, 70, 90);
		}
		class TimerListener implements ActionListener// listener class
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				repaint();
			}
		}
	}
}
