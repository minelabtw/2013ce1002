package ce1002.e11.s102502562;

import java.awt.Color;
import java.awt.Graphics;
import java.sql.Time;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	Timer timer=new Timer();
	Random random = new Random();
	Color colors[]={Color.RED,Color.GREEN,Color.blue};//設定顏色
	int color;
	int shape;
	int x;
	int y;
	
	MyPanel()
	{
		setLayout(null);
		setBounds(0,0,530,540);
		timer.schedule(new MyTask(),0,1000);
		changeGraph();//轉變圖型和顏色
	}
	public void changeGraph()//隨機產生圖型和顏色和起始位置
	{
		color=random.nextInt(3);
		shape=random.nextInt(2);
		x=random.nextInt(450);
		y=random.nextInt(450);
		this.repaint();
	}
	public void paintComponent(Graphics g)//畫出圖型
	{
		super.paintComponent(g);
		g.setColor(colors[color]);
		if(shape>0.5)
		{
			g.fillOval(x,y,50,50);
		}
		else
		{
			g.fillRect(x,y,50,50);
		}
		g.dispose();
	}
	class MyTask extends TimerTask{
		@Override
		public void run()
		{
			MyPanel.this.changeGraph();
		}
	}
}