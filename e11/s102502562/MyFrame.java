package ce1002.e11.s102502562;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	MyFrame()
	{
		setTitle("E11-102502562");
		setSize(530,540);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(new MyPanel());
	}
}
