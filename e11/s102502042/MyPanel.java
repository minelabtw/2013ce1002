package ce1002.e11.s102502042;
import javax.swing.JPanel;
import java.awt.*;
import java.util.*;
public class MyPanel extends JPanel{
	int x;
	int y;
	int type;
	int color;
	Timer timer;
	//constructor
	MyPanel()
	{
		setLayout(null);
		setSize(500,500);
		timer = new Timer();
		Task task = new Task();
		timer.schedule(task,0,1000);
	}
	//重載畫圖方法
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Random ran = new Random();
		x = ran.nextInt(450);
		y = ran.nextInt(450);
		type = ran.nextInt()%2;
		color = ran.nextInt()%3;
		if(color==0)g.setColor(Color.red);
		else if(color==1)g.setColor(Color.green);
		else g.setColor(Color.blue);
		if(type==0)g.fillRect(x,y,30,30);
		else g.fillOval(x,y,30,30);
	}
	//timertask
	class Task extends TimerTask
	{
		public void run()
		{
			repaint();
		}
	}
}
