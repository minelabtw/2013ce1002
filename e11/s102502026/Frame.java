package ce1002.e11.s102502026;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Frame extends JFrame{
	 Frame()
	  {
	    add(new Figure()); //add panel
	  }
	}

class Figure extends JPanel{
    Timer timer = new Timer(500, new TimerListener());	//timer
    Figure()
    {
      timer.start();	//start timer
    }	
    
     protected void paintComponent(Graphics g)	//draw
    {
     super.paintComponent(g); 
      int W = 20;
	  int H = 20;
      int x = (int)(Math.random() * getWidth());
      int y = (int)(Math.random() * getHeight());
      int c = (int)(Math.random() * 3 +1);	//for 1-3
      int t = (int)(Math.random() * 2 +1);	//for 1-2
      switch (c)
      {
      case 1: 
        g.setColor(Color.BLUE);	//blue
        break;
      case 2: 
        g.setColor(Color.RED);	//red
        break;
      case 3: 
        g.setColor(Color.GREEN);	//green
      }
      switch (t)
      {
      case 1: 
        g.fillOval(x, y, W, H);	//draw figure
        break;
      case 2: 
        g.fillRect(x, y, W, H); //draw figure
      }
    }
     class TimerListener implements ActionListener
	  {
	    TimerListener() {}	//listener
	    
	    public void actionPerformed(ActionEvent arg0)
	    {
	      repaint();	//repaint
	    }
	  }
  }

