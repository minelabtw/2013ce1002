package ce1002.e11.s102502550;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;

public class E11 extends JFrame{

	Random ran = new Random();
	
	E11(){
		setSize(500, 500);                                      //初始Frame
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		setVisible(true);
		timer.start();
	}
	
	public static void main(String[] args) {
		E11 a = new E11();
		
	}
	
	Timer timer = new Timer(1000, new ActionListener() {        //建立Timer
		
		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
			
		}
	});
	
	public void paint(Graphics g){
		super.paint(g);
		
		if(ran.nextBoolean()){                                  //隨機上色
			g.setColor(new Color(ran.nextInt(255),ran.nextInt(255),ran.nextInt(255)));
			g.fillRect(ran.nextInt(450), ran.nextInt(450), 45, 45);
		}
		else{
			g.setColor(new Color(ran.nextInt(255),ran.nextInt(255),ran.nextInt(255)));
			g.fillOval(ran.nextInt(450), ran.nextInt(450), 45, 45);
		}
	}

}
