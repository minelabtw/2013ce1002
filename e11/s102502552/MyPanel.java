package ce1002.e11.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	private Timer timer = new Timer(1000,new TimerListener());//建立一個計時器，每秒一次
	private Random randcolor = new Random();//宣告設定顏色的亂數
	private Random randshape = new Random();//宣告設定形狀的亂數
	private Random randloc = new Random();//宣告設定位置的亂數
	
	public MyPanel() {
		setBounds(0,0,500,500);
		timer.start();//開始計時
	}
	
	public void paintComponent(Graphics G)
	{
		super.paintComponent(G);
		
		if(randcolor.nextInt(3) == 0)
		{
			G.setColor(Color.GREEN);
		}
		else if(randcolor.nextInt(3) == 1)
		{
			G.setColor(Color.RED);
		}
		else{
			G.setColor(Color.BLUE);
		}//設定顏色
		
		if(randshape.nextInt(2) == 0)
		{
			G.fillRect(randloc.nextInt(450),randloc.nextInt(450),40,40);
		}
		else{
			G.fillOval(randloc.nextInt(450),randloc.nextInt(450),40,40);
		}//決定位置并畫出形狀
	}
	
	class TimerListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			repaint();//每秒重畫
		}		
	}

}
