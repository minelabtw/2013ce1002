package ce1002.e11.s100203020;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class Paint extends JPanel{
	final Timer timer = new Timer(1000, new timerlistener());
	class timerlistener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	
	private int color, shape;
	Random random = new Random(); 
	
	Paint(){
		
		this.setLayout(null);

		timer.start();
		
	}
	
	protected void paintComponent(Graphics g){		
		int length=30;
		this.color = random.nextInt(3);
		this.shape = random.nextInt(2);
		super.paintComponent(g);
		
		g.setColor(selectColor(color));//set color
		
		
		//setShape
		if(shape==1)
		{	
			g.fillOval(random.nextInt(getWidth()-length), random.nextInt(getHeight()-length), length, length);
		}
		else 
		{	
			g.fillRect(random.nextInt(getWidth()-length), random.nextInt(getHeight()-length), length, length);
		}
	}
	
	
	//set color
	private Color selectColor(int nuber){
		Color c;
		switch(nuber){
			case 1: c = Color.red; 
			break;
			case 2: c = Color.green; 
			break;
			default: c = Color.blue; 
			break;
		}
				
		return c;
	};
	
	
}
