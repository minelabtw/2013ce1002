package ce1002.e11.s102502031;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	public MyFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("E11-s102502031");
		this.setVisible(true);
	} // end constructor MyFrame(maximizeedScreen)

	public void setLocationToCenter(int width, int height) {
		this.setLocationRelativeTo(null);
		int x = this.getX() - width / 2;
		int y = this.getY() - height / 2;
		this.setLocation(x, y);
	} // end setLocationToCenter(width, height)
} // end class MyFrame