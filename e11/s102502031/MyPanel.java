package ce1002.e11.s102502031;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	private int height;
	private int shape;
	private int size = 20;
	private int width;
	private Color color;
	private Random random = new Random();

	MyPanel(int width, int height) {
		this.setHeight(height);
		this.setWidth(width);

		// random shape and color for first time
		color = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
		shape = random.nextInt(2);
		size = random.nextInt(Math.min(width, height) / 3 - 20) + 20;

		Timer timer = new Timer(1000, new TimerListener());
		timer.start();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(color);

		switch (shape) {
		case 0:
			g.fillOval(random.nextInt(width - size), random.nextInt(height - size), size, size);
			break;
		case 1:
			g.fillRect(random.nextInt(width - size), random.nextInt(height - size), size, size);
			break;
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(getWidth(), getHeight());
	}

	class TimerListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			color = new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
			shape = random.nextInt(2);
			size = random.nextInt(Math.min(width, height) / 3 - 20) + 20;
			repaint();
		}
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
