package ce1002.e11.s102502031;

public class E11 {
	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
		MyPanel panel = new MyPanel(800, 600);

		frame.add(panel);
		frame.setLocationToCenter(panel.getWidth(), panel.getHeight());
		frame.pack();
	}
}