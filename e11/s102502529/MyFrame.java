package ce1002.e11.s102502529;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class MyFrame extends JFrame{
	public MyFrame()
	{
		setTitle("E11-102502529");// set frame's title
		setSize(300, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MovingPanel panel = new MovingPanel();
		panel.setLayout(new BorderLayout(3, 3));
		panel.setBorder(new EmptyBorder(3, 3, 3, 3));
		LineBorder lineBorder = new LineBorder(Color.black, 1);
		panel.setBorder(lineBorder);
		setContentPane(panel);
	}




	public class MovingPanel extends JPanel{
		private Timer timer = new Timer(200, new TimerListener());
		private int xLocation;
		private int yLocation;
		private int color;
		private int shape;
		private Random ran;

		public MovingPanel()
		{
			timer.start();
			ran = new Random();
		}

		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			xLocation = ran.nextInt(250)+1;
			yLocation = ran.nextInt(250)+1;
			color = ran.nextInt(3)+1;
			shape = ran.nextInt(2)+1;
			if(shape == 1)
			{
				if(color == 1)
				{
					g.setColor(Color.RED);
					g.fillRect(xLocation, yLocation, 40,40);
				}
				else if(color == 2)
				{
					g.setColor(Color.GREEN);
					g.fillRect(xLocation, yLocation, 40,40);
				}
				else
				{
					g.setColor(Color.BLUE);
					g.fillRect(xLocation, yLocation, 40,40);
				}
			}
			if(shape == 2)
			{
				if(color == 1)
				{
					g.setColor(Color.RED);
					g.fillOval(xLocation, yLocation, 40, 40);
				}
				else if(color == 2)
				{
					g.setColor(Color.GREEN);
					g.fillOval(xLocation, yLocation, 40, 40);
				}
				else
				{
					g.setColor(Color.BLUE);
					g.fillOval(xLocation, yLocation, 40, 40);
				}
			}
		}
	}


	public class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			repaint();
		}

	}
}
