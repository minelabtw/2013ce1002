package ce1002.e11.s102502540;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.*;

public class MyFrame extends JFrame {
	MyFrame() { //設定視窗格式大小
		setSize(500, 500);
		setTitle("E11-102502540");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(null);
		MyPanel p = new MyPanel();
		add(p);
		setVisible(true);
	}
}
