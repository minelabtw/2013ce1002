package ce1002.e11.s102502540;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	MyPanel() {
		setSize(480, 380);
		setLocation(5, 5);
		setLayout(null);
		setVisible(true);
		timer.start();
	}

	Timer timer = new Timer(1000, new timerlistener());

	class timerlistener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Random ran = new Random();
		if (ran.nextInt(2) == 1) {
			Random ran1 = new Random();
			if (ran1.nextInt(3) == 0) { //設計要隨機產生之圖形及顏色
				g.setColor(Color.RED);
				g.fillRect(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			} else if (ran1.nextInt(3) == 1) {
				g.setColor(Color.BLUE);
				g.fillRect(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			} else {
				g.setColor(Color.GREEN);
				g.fillRect(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			}
		} else {
			Random ran1 = new Random();
			if (ran1.nextInt(3) == 0) {
				g.setColor(Color.RED);
				g.fillOval(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			} else if (ran1.nextInt(3) == 1) {
				g.setColor(Color.BLUE);
				g.fillOval(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			} else {
				g.setColor(Color.GREEN);
				g.fillOval(ran1.nextInt(320), ran1.nextInt(250), 60, 60);
			}
		}
	}

}
