package ce1002.e11.s102502011;

import java.awt.* ;
import java.awt.event.*;

import javax.swing.* ;

import java.util.Random;

public class MyPanel extends JPanel {
	private Timer timer = new Timer(1000 , new TimerListener() ) ; //set timer
	
	MyPanel() {
		setLayout(null);
		setSize(500,500) ;
		timer.start(); //let it go
	}
	

	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Random random = new Random() ;
		int color , shape , x = 0 , y = 0 ;
		
		color = random.nextInt(3) ;
		shape = random.nextInt(2) ;
		x = random.nextInt(440) ;
		y = random.nextInt(440) ;
		
		if(color==0)  
			g.setColor(Color.RED); //random color
		else if (color==1) 
			g.setColor(Color.BLUE);
		else
			g.setColor(Color.GREEN);
		
		if (shape==0) 
			g.fillRect(x,y,50,50); //random shape 
		else
			g.fillOval(x,y,50,50);
			
	}
	
	class TimerListener implements ActionListener { //using timer

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			repaint() ;
		}
		
		
	}
}


