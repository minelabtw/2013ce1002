package ce1002.e11.ta;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame extends JFrame{
	MyFrame(){
		//New and add panel
		add(new RandomGraphPanel());
	}
	class RandomGraphPanel extends JPanel{
		//New Timer 
		private Timer timer = new Timer(1000, new TimerListener());
		RandomGraphPanel(){
			//Start timer
			timer.start();
		}
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			//Set graph's width and height
			int graphWidth=50;
			int graphHeight=50;

			//Random coordinate,color and graph's type
			int x = (int) (Math.random()*(this.getWidth()-graphWidth) + 1 );
			int y = (int) (Math.random()*(this.getHeight()- graphHeight) + 1 );
			int color = (int) (Math.random()*3+1);	//Red,Green,Blue
			int type = (int) (Math.random()*2+1);	//Oval,Rectangle

			//Set color
			switch(color){
				case 1:
					g.setColor(Color.RED);
					break;
				case 2:
					g.setColor(Color.GREEN);
					break;
				case 3:
					g.setColor(Color.BLUE);
			}
			
			//Set type
			switch(type){
				case 1:
					g.fillOval(x, y, graphWidth, graphHeight);
					break;
				case 2:
					g.fillRect(x, y, graphWidth, graphHeight);
					break;
			}
		}
	}
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			repaint();
		}
		
	}

}
