package ce1002.e11.s102502538;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JLabel {
	private int color;
	private int shape;
  public MyPanel() {
	  
  }
  
  @Override
  protected void paintComponent(Graphics g) {
	  super.paintComponents(g);
	  
	  color = (int)(Math.random() * 3 + 1);
	  shape = (int)(Math.random() * 2 + 1);
	  int x = (int)((Math.random() * getWidth()));
	  int y = (int)((Math.random() * getHeight()));
	  
	  if (color == 1 && shape == 1) {
		  g.setColor(Color.RED);
		  g.fillOval(x, y, 50, 50);
	  }
	  else if (color == 1 && shape == 2) {
		  g.setColor(Color.RED);
		  g.fillRect(x, y, 50, 50);
	  }
	  else if (color == 2 && shape == 1) {
		  g.setColor(Color.GREEN);
		  g.fillOval(x, y, 50, 50);
	  }
	  else if (color == 2 && shape == 2) {
		  g.setColor(Color.GREEN);
		  g.fillRect(x, y, 50, 50);
	  }
	  else if (color == 3 && shape == 1) {
		  g.setColor(Color.BLUE);
		  g.fillOval(x, y, 50, 50);
	  }
	  else if (color == 3 && shape == 2) {
		  g.setColor(Color.BLUE);
		  g.fillRect(x, y, 50, 50);
	  }
	  else {
		  System.out.println("There must be wrong");
		  System.exit(1);
	  }
  }
}