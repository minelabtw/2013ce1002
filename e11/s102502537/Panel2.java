package ce1002.e8.s102502537;

import javax.swing.*;

import java.awt.*;


public class Panel2 extends JPanel{
	
	protected JLabel label1 = new JLabel();
	protected JLabel label2 = new JLabel();
	
	
	Panel2()
	{
		setLayout(new BorderLayout(5,10));
		setBounds(260, 0, 250 , 200);
		add(label1,BorderLayout.NORTH);
		label2.setText("Oval");//the state of the graphic
		add(label2,BorderLayout.SOUTH);
	}
	
	@Override 
    public void paintComponent (Graphics g){
		
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.fillOval(20, 20, 200, 150);
    }//paint the graphics


}
