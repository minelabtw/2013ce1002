package ce1002.e8.s102502537;

import java.awt.*;

import javax.swing.*;


public class MyFrame extends JFrame{
	
	protected Panel panel1 = new Panel();
	protected Panel2 panel2 = new Panel2();//2 panels for 2 graphics
	
	
	MyFrame()
	{
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(0 , 0 , 550 , 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel1);
		add(panel2);//add two panels
		setVisible(true);
	}
	

}
