package ce1002.e11.s102502526;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class MyFrame extends JFrame{
	Random random = new Random();
	private int xCoordinate;
	private int yCoordinate;
	private int color;
	private int photo;
	private Timer timer = new Timer(1000, new TimerListener());

	public MyFrame() {
		timer.start();
	}

	public static void main(String[] args) {

		MyFrame Panel = new MyFrame();
		JFrame frame = new JFrame();
		
		frame.add(Panel);
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

	public Dimension getPreferreSize() {
		return new Dimension(500, 500);
	}



	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}

	}
}
