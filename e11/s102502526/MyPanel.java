package ce1002.e11.s102502526;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private int xCoordinate;
	private int yCoordinate;
	private int color;
	private int photo;
	Random random = new Random();
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		xCoordinate = random.nextInt(400);
		yCoordinate = random.nextInt(400);
		color = random.nextInt(3);
		photo = random.nextInt(2);
		
		switch (color) {
		case 0:
			g.setColor(Color.red);
			break;
		case 1:
			g.setColor(Color.blue);
			break;
		case 2:
			g.setColor(Color.green);
			break;

		}

		
		switch (photo) {
		case 0:
			g.fillOval(xCoordinate, yCoordinate, 50, 50);
			break;
		case 1:
			g.fillRect(xCoordinate, yCoordinate, 50,50);
			break;
		}
		
		
	}
}
