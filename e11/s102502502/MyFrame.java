package ce1002.e11.s102502502;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame  extends JFrame{
	private static final long serialVersionUID = 1L;

MyFrame(){
    setTitle("E11-102502502");
    setSize(500, 500);
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setVisible(true);
    RandomPanel p = new RandomPanel();
	add(p);
  }
  
  class RandomPanel  extends JPanel  {
	private static final long serialVersionUID = 1L;
	private Timer timer = new Timer(1000, new MyFrame.TimerListener(MyFrame.this));
    
    RandomPanel() {
      this.timer.start();
    }
    
    protected void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      
      int w = 30;              // the width of the graph
      int h = 30;             // the height of the graph
      
      int x = (int)(Math.random() * (getWidth() - w));
      int y = (int)(Math.random() * (getHeight() - h));
      int color = (int)(Math.random() * 5+1);
      int type = (int)(Math.random() * 3+1);
     switch (color)
      {
      case 1: 
        g.setColor(Color.CYAN);
        break;
      case 2: 
			Color c = new Color(100, 50, 100);                  //set color c for g 
			g.setColor(c);  
        break;
      case 3: 
			Color r = new Color(255, 51, 204);                  //set color r for g 
			g.setColor(r);   
			break;
      case 4: 
			Color o = new Color(200, 100, 250);
			g.setColor(o);                     				   //set color o for g   
        break;
      case 5: 
    	  g.setColor(Color.RED);     					       //set color red for g
      }
      switch (type)
      {
      case 1: 
			g.fillArc(x, y, w, h, 0, 30);
			g.fillArc(x, y, w, h, 90, 30);
			g.fillArc(x, y, w, h, 180, 30);
			g.fillArc(x, y, w, h, 270, 30);
        break;
      case 2: 
        g.fillRect(x, y, w, h);
        break;
      case 3:
  		int [] a = {x, x+w/2, x+w};  
  		int [] b = {y, y+h, y};
  		g.drawPolygon(a, b, 3);
  		g.fillPolygon(a, b, 3);
  		int [] c = {x, x+w/2, x+w};  
  		int [] d = {y+h/2, y-h/2, y+h/2};
  		g.drawPolygon(c, d, 3);
  		g.fillPolygon(c, d, 3);
      }
    }
  }
  
  class TimerListener  implements ActionListener{         // create TimerListener to listen the motion and change of graph with time 
    TimerListener(MyFrame f) {                            // set the constructor
    }
    public void actionPerformed(ActionEvent arg0)         // define the action of TimerListener
    {                                                     // call the function repaint to redraw the graph
      MyFrame.this.repaint();
    }
  }
}
