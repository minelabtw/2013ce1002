package ce1002.e11.s101201522;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import java.util.ArrayList;
import java.lang.Math;

public class MyPanel extends JPanel implements ActionListener {
	private ArrayList<Polygon> polys; //the set of graphs
	
	MyPanel() {
		this.setVisible(true);
		
		polys = new ArrayList<Polygon>();
		
		
		// create a polygon 6 sides
		Polygon tmp = new Polygon();
		
		for (int i = 0; i < 6; i++)
			tmp.addPoint((int)(50 * Math.cos(Math.PI*2 / 6.0 * i)), (int) (50*Math.sin(Math.PI *2 / 6.0 *i)));
		
		polys.add(tmp);
		
		
		//create a phase
		tmp = new Polygon();
		
		for (int i = 0;i < 1000; i++) {
			double angle = (Math.PI * 2.0 /1000.0) * i;
			int x = (int) (20 * Math.cos(angle) + 50 * Math.cos(2.0/3.0 *angle));
			int y = (int) (20 *Math.sin(angle) - 50 * Math.sin(2.0/3.0*angle));
			
			tmp.addPoint(x, y);
		}
		
		polys.add(tmp);
		
		//create a Hypocycloid with 7 cups
		tmp = new Polygon();
		
		for (int i = 0; i < 1000; i++) {
			double angle = (Math.PI * 2.0 /1000.0) * i;
			int x = (int) (5 * 6 * Math.cos(angle) + 5 * Math.cos(6 * angle));
			int y = (int) (5 * 6 * Math.sin(angle) - 5 * Math.sin(6 * angle));
			
			tmp.addPoint(x, y);
		}
		
		polys.add(tmp);
		
		
		
		Timer t = new Timer(1000, this); //a timer to repeat repaint this panel between 1 second
		
		t.start();
	}
	
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (!polys.isEmpty()) {
			int pick = (int) ((polys.size()) * Math.random()); //choose polygon
			int xb = (int)polys.get(pick).getBounds().getWidth(); //get the width of this polygon
			int yb = (int)polys.get(pick).getBounds().getHeight();//get the Height of this polygon
			
			int x = (int) ((this.getWidth() - xb) * Math.random() + xb/2); //randomly choose the x position
			int y = (int) ((this.getHeight() - yb) * Math.random() + yb/2); //randomly choose the y position
			
			int c = (int)(Double.doubleToRawLongBits(Math.random()) & 0xffffffL); //randomly choose the color
			
			g.setColor(new Color(c));
			g.translate(x, y);// set the new coordinate
			g.fillPolygon(polys.get(pick)); //fill the polygon
			
			g.translate(-x, -y); //restore the coordinate
			
		}
		
	} 
	
	public void actionPerformed(ActionEvent e) {
		repaint(); //repaint action
	}
}
