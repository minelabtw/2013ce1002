package ce1002.e11.s102502051;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class E11 extends JFrame implements ActionListener
{
Container c;
//設定UI元件
java.util.Random rg;
int x1,y1,x2,y2,mode,ms;//mt時間,ms分數
double mt;
javax.swing.Timer t;
public E11() 
{
super("E11-102502051 "); 
//初始化共用變數
mode=0;
rg=new Random();
c=getContentPane();//取得ContentPane
//設定版面設定
c.setLayout(new FlowLayout());
//初始化UI元件
t=new javax.swing.Timer(500,this);
setSize(1024,800);//設定size，顯示出去
setVisible(true);
t.start();
}
public void paint(Graphics g) 
{
super.paint(g);//畫出元件
x1= rg.nextInt(600)+1;x2=rg.nextInt(600)+1;
y1=rg.nextInt(700)+1;y2=rg.nextInt(700)+1;
if(mode==1)

g.setColor(Color.RED);
g.fillOval(x1,y1,50,50);
g.setColor(Color.GREEN);
if(mode==2)
g.setColor(Color.BLUE);
g.fillRect(x2,y2,50,50);
g.setColor(Color.GREEN);
}

//UI元件事件處理類別寫在這裡
public void actionPerformed(ActionEvent e)
{
 if (e.getSource()==t)
{
mt=mt-1;
mode=rg.nextInt(2)+1;

repaint();
}
}



public static void main(String args[]) //程式起點
{
E11 app=new E11(); 
app.addWindowListener(new WindowAdapter(){ 
public void windowClosing(WindowEvent e)
{
System.exit(0);
}
}); //處理視窗關閉要求
}
}