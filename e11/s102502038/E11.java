package ce1002.e11.s102502038;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E11 {
	JFrame frame = new JFrame();
	DrawPanel panel = new DrawPanel();
	public void go(){
		frame.getContentPane().add(panel);//init
		frame.setVisible(true);
		frame.setTitle("E11 - s102502038");
		frame.setSize(60,100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		repaintLoop();
	}
	static public void main(String[] agrs){
		E11 gui = new E11();
		gui.go();
	}
	public void repaintLoop(){
		Timer time = new Timer();
		time.schedule(new RepaintTask(panel),new Date(), 500);
	}
}
class DrawPanel extends JPanel{
	Date date = new Date();
    @Override
    public void paintComponent(Graphics g) {//random draw
        super.paintComponent(g);
        Color[] colorArr = {Color.RED,Color.BLUE,Color.GREEN};
        g.setColor(colorArr[(int) Math.floor(Math.random()*3)]);
        if(Math.random() > 0.5){
        	g.fillOval(10, 10, 40, 40);
        }else{
        	g.fillRect(10, 10, 40, 40);
        }
    }
}
class RepaintTask extends TimerTask{//timer task
	DrawPanel panel;
	RepaintTask(DrawPanel panel){
		this.panel = panel;
	}
	@Override
    public void run() {
//		System.out.println("repaint");
		panel.repaint();
	}
}