package ce1002.e11.s102502533;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class E11 extends JFrame {
	public E11() {
		add(new moving());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		E11 f = new E11();
		f.setTitle("E11-102502533");
		f.setLocation(300, 300);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500,500 );
		f.setVisible(true);
	}

	static class moving extends JPanel {
		private int a;
		private int b ;
		private int x ;
		private int y ;
		private Timer timer = new Timer(1000,new TimerListener());
		
		moving() {
			timer.start();
		}
		class TimerListener implements ActionListener{
			 @Override
			 public void actionPerformed (ActionEvent e){
				 
					 repaint();
				 
				 
			 }
			
		}
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			a = (int) (Math.random() * 100 % 2);
			b = (int) (Math.random() * 100 % 3);
			x = (int) (Math.random() * 100 % 3);
			y = (int) (Math.random() * 100 % 3);
			if (a == 0) {
				if (b == 0) {
					g.setColor(Color.BLUE);
					g.fillRect(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
				if (b == 1) {
					g.setColor(Color.gray);
					g.fillRect(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
				if (b == 2) {
					g.setColor(Color.pink);
					g.fillRect(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
			}
			if (a == 1) {
				if (b == 0) {
					g.setColor(Color.BLUE);
					g.fillOval(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
				if (b == 1) {
					g.setColor(Color.gray);
					g.fillOval(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
				if (b == 2) {
					g.setColor(Color.pink);
					g.fillOval(50 + 100 * x, 200 + 100 * y, 50, 50);
				}
			}
		}
		 
	}
}