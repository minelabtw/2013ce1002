package ce1002.e11.s102502503;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Random;

public class E11 extends JFrame implements ActionListener
{
	int color=-1;//顏色
	int shape=-1;//形狀
	int x=-200;//x軸值
	int y=-200;//y軸值
	Random ran=new Random();
	
	public static void main(String args[])
	{
		E11 frame=new E11();
		frame.setLayout(null);
		frame.setBounds(200,20,350,350);
		javax.swing.Timer time=new javax.swing.Timer(1000,frame);//每一秒執行一次
		time.start();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);	
	}
	public void paint(Graphics g)
	{  
		g.setColor(Color.white);//先用畫白色覆蓋掉先前的圖形
		g.fillRect(0,0,600,600);
		
		if(color==0)  //設定顏色
		g.setColor(Color.red);
		else if(color==1)
		g.setColor(Color.green);
		else
		g.setColor(Color.blue);
		
		if(color==0)  //設定形狀
		g.fillRect(x,y,50,50);
		else
		g.fillOval(x,y,50,50);
	}
	public void actionPerformed(ActionEvent arg0) 
	{
	   color=ran.nextInt(3);//隨機顏色
	   shape=ran.nextInt(2);//隨機形狀
	   x=ran.nextInt(250)+50;//隨機x軸
	   y=ran.nextInt(250)+50;//隨機y軸
	   repaint();//重畫
	}

}

