package ce1002.e11.s102502030;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Graph extends JPanel {

	public Graph() {
		//set panel
		setSize( 500, 500 );
		setLocation( 0, 0 );
		//set timer
		Timer timer = new Timer( 1000, new TimerListener() );
		timer.start();
	}
	
	public void paintComponent( Graphics g ) {
		super.paintComponent( g );
		
		int color = (int)(Math.random()*3);
		int shape = (int)(Math.random()*2);
		int x = (int)(Math.random()*450);
		int y = (int)(Math.random()*450);
		
		//randomly draw graph
		switch( color ) {
		case 0:
			g.setColor( Color.green );
			break;
		case 1:
			g.setColor( Color.red );
			break;
		case 2:
			g.setColor( Color.blue );
			break;
		case 3:
			g.setColor( Color.green );
			break;
		default :
			break;
		}
		switch( shape ) {
		case 0:
			g.fillRect( x, y, 50, 50 );
			break;
		case 1:
			g.fillOval( x, y, 50, 50 );
			break;
		case 2:
			g.fillRect( x, y, 50, 50 );
			break;
		default :
			break;
		}
	}
	
	private class TimerListener implements ActionListener {
		@Override
		public void actionPerformed( ActionEvent e ) {
			repaint();
		}
	}
}
