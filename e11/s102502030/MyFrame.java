package ce1002.e11.s102502030;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

public class MyFrame extends JFrame {

	protected Graph graph = new Graph();
	
	public MyFrame() {
		//set frame
		setSize( 500, 500);
		setLocation( 500, 250 );
		setVisible( true );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		
		add( graph );
	}
	
}
