package ce1002.e11.s102502501;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	Random rr = new Random();
	Timer clock = new Timer(1000,new ActionListener (){ //�X��
		
		@Override
		public void actionPerformed(ActionEvent a){
			repaint();
			
		}
	});
	
	public MyPanel(){
		setBounds(0,0,500,500);
		setLayout(null);
		setBackground(Color.WHITE); //�I���C��
		clock.start();
	}
	@Override
	protected void paintComponent(Graphics b){
		super.paintComponent(b);
		int w = rr.nextInt(10);
		int x = rr.nextInt(10);
		int y = rr.nextInt(7);
		int z = rr.nextInt(2);
		setcolor(y,b);
		setshape(y,b,w,x);
	}
	public void setshape(int i ,Graphics b, int w, int x){
		if(i == 1 ){
			b.fillOval(50 * w , 50 * x, 75, 75);
		}
		else{
			b.fillRect(50 * w , 50 * x, 75, 75);
		}
	}
	public void setcolor(int j, Graphics b){
		if(j ==1){
			b.setColor(Color.BLACK);
		}
		else if (j == 2){
			b.setColor(Color.GREEN);
		}
		else if(j== 3){
			b.setColor(Color.RED);
		}
	}
}
