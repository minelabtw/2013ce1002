package ce1002.e11.s102502517;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame extends JFrame {

	public MyFrame() {
		add(new MyPanel());
	}

	public class MyPanel extends JPanel {
		private Timer timer = new Timer(1000, new MyFrame.TimerListener()); //新增ActionListener

		public MyPanel() {
			this.timer.start();
		}

		public void paint(Graphics g) {
			Random random = new Random(); //隨機設定
			int x = random.nextInt(getWidth()-50);
			int y = random.nextInt(getHeight()-50);
			int color = random.nextInt(3);
			int shape = random.nextInt(2);
			
			switch (color) {
			case 0:
				g.setColor(Color.RED);
				break;
			case 1:
				g.setColor(Color.GREEN);
				break;
			case 2:
				g.setColor(Color.BLUE);
				break;
			default:
				break;
			}

			switch (shape) {
			case 0:
				g.fillRect(x, y, 50, 50);
				break;
			case 1:
				g.fillOval(x, y, 50, 50);
				break;
			default:
				break;
			}
		}
	}

	public class TimerListener implements ActionListener {
		TimerListener() {
		}

		public void actionPerformed(ActionEvent arg0) {
			MyFrame.this.repaint();
		}
	}
}
