package ce1002.e11.s101502205;

import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyPanel extends JPanel{
	private int _x;		// x coordinate
	private int _y;		// y coordinate
	private Color[] colors = new Color[3];	// three colors
	private Random rand = new Random();		// random obj
	private Timer timer = new Timer(1000, new TimerListener());		// timer listner
	public MyPanel() {
		// set all colors
		colors[0] = Color.red;
		colors[1] = Color.green;
		colors[2] = Color.blue;
		// start timer
		timer.start();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// random x, y coordinate and color
		_x = rand.nextInt(getWidth()-50);
		_y = rand.nextInt(getHeight()-50);
		g.setColor(colors[rand.nextInt(3)]);
		// draw rectangle or oval
		if (rand.nextBoolean()){
			g.fillRect(_x, _y, 50, 50);
		}else{
			g.fillOval(_x, _y, 50, 50);
		}
	}

	class TimerListener implements ActionListener {
		@Override 
		public void actionPerformed(ActionEvent e) {
			// repaint
			repaint();
		}
	}
}
