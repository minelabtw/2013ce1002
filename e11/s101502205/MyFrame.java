package ce1002.e11.s101502205;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	public MyFrame(){
		// settings
		setTitle("E11-101502205");
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		// Add a new panel to the frame
		add(new MyPanel());
	}
}
