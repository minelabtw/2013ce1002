package ce1002.e11.s100201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Photo extends JPanel
{
	private int graphtype;
	private Color colortype;

	@Override
	public void paint(Graphics g)
	{
		// TODO Auto-generated method stub
		super.paint(g);
		g.setColor(colortype);
		if(graphtype == 0)

			g.fillRect(0, 0, 50, 50);
		else
			g.fillOval(0, 0, 50, 50);
	}
	
	//setting method
	void setgraphtype(int g)
	{
		graphtype = g;
	}
	
	void setcolortype(Color c)
	{
		colortype = c;
	}
}
