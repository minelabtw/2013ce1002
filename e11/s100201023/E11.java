package ce1002.e11.s100201023;

import java.util.Timer;

import javax.swing.JFrame;

public class E11
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Timer time = new Timer();
		
		//setting frame
		JFrame myframe = new JFrame("E11-100201023");
		myframe.setSize(300, 300);
		myframe.setLayout(null);
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setVisible(true);
		
		time.schedule(new Play(myframe), 0, 1000);
	}

}
