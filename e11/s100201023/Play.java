package ce1002.e11.s100201023;

import java.awt.Color;
import java.util.Random;
import java.util.TimerTask;

import javax.swing.JFrame;

public class Play extends TimerTask
{
	private JFrame frame;
	private Photo p;
	
	public Play(JFrame myframe)
	{
		// TODO Auto-generated constructor stub
		frame = myframe;
		p = new Photo();
	}
	
	@Override
	public void run()
	{	
		// TODO Auto-generated method stub
		Random rand = new Random();
		
		//decide type of color
		int colortype = rand.nextInt(3);
		Color color;
		if(colortype == 0)
			color = new Color(255, 0, 0);
		else if(colortype == 1)
			color = new Color(0, 255, 0);
		else 
			color = new Color(0, 0, 255);
		p.setcolortype(color);
		
		//decide type of graph
		int graphtype = rand.nextInt(2);
		p.setgraphtype(graphtype);
		
		//draw graph
		p.setBounds(rand.nextInt(200), rand.nextInt(200), 100, 100);
		frame.add(p);
	}
}
