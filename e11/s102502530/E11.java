package ce1002.e11.s102502530;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E11 extends JPanel {
	private static int x = 0;
	private static int y = 0;
	private static int width = 800;
	private static int height = 800;
	private static int shape = 0;
	private static Color color;
	public static void main(String[] args) {
		//set frame
		final JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		frame.setTitle("E11-102502530");
		
		//set panel
		final JPanel panel = new E11();
		frame.getContentPane().add(panel);
		frame.setVisible(true);
		
		//set thread with timer
		Thread thread = new Thread(new Runnable() {
			public void run() {
				//set timer
				Timer timer = new Timer();
				final Random random = new Random();
				timer.schedule(new TimerTask() {
					@Override
					public void run() {
						E11.width = frame.getContentPane().getWidth();
						E11.height = frame.getContentPane().getHeight();
						E11.x = random.nextInt(E11.width - 100);
						E11.y = random.nextInt(E11.height - 100);
						E11.shape = random.nextInt(7);
						E11.color = new Color(random.nextFloat(), random.nextFloat(), random.nextFloat());
						
						//repaint
						panel.repaint();
					}
				}, 0, 1000);
			}
		});
		
		//start thread with timer
		thread.start();
	}
	
	@Override
	protected void paintComponent(Graphics graphics) {
		//clear
		super.paintComponent(graphics);
		Polygon polygon;
		graphics.setColor(color);
		switch (shape) {
		case 0://Circle
			graphics.fillOval(x, y, 100, 100);
			break;
		case 1://triangle
			polygon = new Polygon();
			polygon.addPoint(x + 50, y);
			polygon.addPoint(x, y + 87);
			polygon.addPoint(x + 100, y + 87);
			graphics.fillPolygon(polygon);
			break;
		case 2://Rectangle
			graphics.fillRect(x, y, 100, 100);
			break;
		case 3://Pentagon
			polygon = new Polygon();
			polygon.addPoint(x + 50, y);
			polygon.addPoint(x, y + 36);
			polygon.addPoint(x + 19, y + 95);
			polygon.addPoint(x + 81, y + 95);
			polygon.addPoint(x + 100, y + 36);
			graphics.fillPolygon(polygon);
			break;
		case 4://Hexagonal
			polygon = new Polygon();
			polygon.addPoint(x + 25, y);
			polygon.addPoint(x, y + 43);
			polygon.addPoint(x + 25, y + 87);
			polygon.addPoint(x + 75, y + 87);
			polygon.addPoint(x + 100, y + 43);
			polygon.addPoint(x + 75, y);
			graphics.fillPolygon(polygon);
			break;
		case 5://Heptagon
			polygon = new Polygon();
			polygon.addPoint(x + 50, y);
			polygon.addPoint(x + 10, y + 19);
			polygon.addPoint(x, y + 63);
			polygon.addPoint(x + 28, y + 97);
			polygon.addPoint(x + 72, y + 97);
			polygon.addPoint(x + 100, y + 63);
			polygon.addPoint(x + 90, y + 19);
			graphics.fillPolygon(polygon);
			break;
		case 6://Octagonal
			polygon = new Polygon();
			polygon.addPoint(x + 29, y);
			polygon.addPoint(x, y + 29);
			polygon.addPoint(x, y + 71);
			polygon.addPoint(x + 29, y + 100);
			polygon.addPoint(x + 71, y + 100);
			polygon.addPoint(x + 100, y + 71);
			polygon.addPoint(x + 100, y + 29);
			polygon.addPoint(x + 71, y);
			graphics.fillPolygon(polygon);
			break;
		}
	};
}