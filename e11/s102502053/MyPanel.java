package ce1002.e11.s102502053;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Random;


public class MyPanel extends JPanel{
	
	int x;
	int y;
	Random random = new Random();
	Timer timer = new Timer(1000, new TimerListener());
	int shape;
	int colour;
	
	MyPanel()
	{
		
		setBounds(0, 0, 480, 380);
		setLayout(null);
		setVisible(true);
		timer.start();
	}
	
	class TimerListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
	
	public void paintComponent(Graphics g) 
	{ //create oval
		super.paintComponent(g);
		shape = random.nextInt(3);
		x = random.nextInt(410);
		y = random.nextInt(300);
		if(shape == 0)
		{
			colour = random.nextInt(3);
			switch (colour)
			{
			case 0: 
			{
				g.setColor(Color.RED);
				g.fillOval(x, y, 50, 50);break;
			}
			
			
			case 1:
			{
				g.setColor(Color.GREEN);
				g.fillOval(x, y, 50, 50);break;
			}
			
			
			case 2:
			{
				g.setColor(Color.BLUE);
				g.fillOval(x, y, 50, 50);break;
			}
				
			}
		}else
		{
			colour = random.nextInt(3);
			switch (colour)
			{
			case 0: 
			{
				g.setColor(Color.RED);
				g.fillRect(x, y, 50, 50);
				break;
			}
			
			
			case 1:
			{
				g.setColor(Color.GREEN);
				g.fillRect(x, y, 50, 50);
				break;
			}
			
			
			case 2:
			{
				g.setColor(Color.BLUE);
				g.fillRect(x, y, 50, 50);
				break;	
			}
			
			}
		}
	}
}
