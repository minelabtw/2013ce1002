package ce1002.e11.s102502053;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyFrame extends JFrame{
	
	MyFrame() //set frame size
	{
		MyPanel panel = new MyPanel();
		setLayout(null); 
		setSize(500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		add(panel);
	}

	
	
}
