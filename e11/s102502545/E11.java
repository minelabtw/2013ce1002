package ce1002.e11.s102502545;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class E11 extends JPanel {
	Random random = new Random();
	private int xCoordinate;
	private int yCoordinate;
	private int color;
	private int photo;
	private Timer timer = new Timer(1000, new TimerListener());

	public E11() {
		timer.start();
	}

	public static void main(String[] args) {

		E11 Panel = new E11();
		JFrame frame = new JFrame();
		
		frame.add(Panel);
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

	public Dimension getPreferreSize() {
		return new Dimension(500, 500);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		// ���ü�

		xCoordinate = random.nextInt(400);
		yCoordinate = random.nextInt(400);
		color = random.nextInt(3);
		photo = random.nextInt(2);
		
		switch (color) {
		case 0:
			g.setColor(Color.red);
			break;
		case 1:
			g.setColor(Color.blue);
			break;
		case 2:
			g.setColor(Color.green);
			break;

		}

		
		switch (photo) {
		case 0:
			g.fillOval(xCoordinate, yCoordinate, 50, 50);
			break;
		case 1:
			g.fillRect(xCoordinate, yCoordinate, 50,50);
			break;
		}
		
		
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}

	}

}
