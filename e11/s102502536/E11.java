package ce1002.e11.s102502536;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.Math;

public class E11 extends JFrame{

	public E11() {
	    // create new MovingGraphicPanel and set the size, location, layout of panel then add panel into the frame 
		MovingGraphicPanel panel = new MovingGraphicPanel();
		panel.setBounds(50 , 50 , 620 , 620);
		setLayout(null);
		add(panel);
	}
	
	public static void main(String[] args) {
		// create new frame and set the size,... of the frame
		E11 frame = new E11();
		frame.setTitle("E11");
		frame.setBounds(50 , 50 , 630, 630);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static class MovingGraphicPanel extends JPanel {
		// create timer
		private Timer timer = new Timer(1000, new TimerListner());
		
		public MovingGraphicPanel() {
			timer.start();  // start timer
		}
		
		protected void paintComponent(Graphics g) {  // paint the graphics
			super.paintComponent(g);
			int x = (int)(Math.random()*500) + 5;
			int y = (int)(Math.random()*500) + 5;
			int graph = (int)(Math.random()*2);
			int color = (int)(Math.random()*3);
			if(graph == 0 && color == 0)
			{
				g.setColor(Color.RED);
				g.fillRect(x , y , 35 , 35);	
			}
			if(graph == 0 && color == 1)
			{
				g.setColor(Color.BLUE);
				g.fillRect(x , y , 35 , 35);
			}
			if(graph == 0 && color == 2)
			{
				g.setColor(Color.GREEN);
				g.fillRect(x , y , 35 , 35);
			}
			if(graph == 1 && color == 0)
			{
				g.setColor(Color.RED);
				g.fillOval(x , y , 40 , 40);
			}
			if(graph == 1 && color == 1)
			{
				g.setColor(Color.BLUE);
				g.fillOval(x , y , 40 , 40);
			}
			if(graph == 1 && color == 2)
			{
				g.setColor(Color.GREEN);
				g.fillOval(x , y , 40 , 40);
			}
			
		}
		
		class TimerListner implements ActionListener {  // listener class
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		}
		
	}

}
