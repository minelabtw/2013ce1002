package ce1002.e11.s102502558;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	MyFrame()
	{
		// basic setup
		setTitle("E11-102502558");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
		this.setLocationRelativeTo(null);
		add(new MyPanel(500, 500));
	}
}
