package ce1002.e11.s102502558;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	// use a timer to invoke change function
	Timer timer = new Timer();
	Random rand = new Random();
	// color
	Color[] colors = {Color.RED, Color.GREEN, Color.BLUE};
	int SIZE = 50; // shape size
	int shape;
	int color;
	// make the shape contained in the panel
	int maxX;
	int maxY;
	public MyPanel(int width, int height)
	{
		// set max width height
		maxX = width - SIZE;
		maxY = height - SIZE;
		// add a task
		timer.schedule(new MyTask(), 0, 1000);
		change();
	}
	
	// change shape color and repaint
	private void change()
	{
		shape = rand.nextInt(2);
		color = rand.nextInt(3);
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		// draw color shape
		super.paintComponent(g);
		g.setColor(colors[color]);
		if (shape == 1)
		{
			g.fillRect(rand.nextInt(maxX), rand.nextInt(maxY), SIZE, SIZE);
		}
		else
		{
			g.fillOval(rand.nextInt(maxX), rand.nextInt(maxY), SIZE, SIZE);
		}
		g.dispose();
	}
	
	class MyTask extends TimerTask
	{
		@Override
		public void run()
		{
			// call outerclass change function
			MyPanel.this.change();
		}
	}
}
