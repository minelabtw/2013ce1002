package ce1002.e11.s102502012;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	private MyPanel content;
	
	public MyFrame(int w, int h){
		setSize(w, h);
		
		content = new MyPanel(w, h);
		
		add(content);
	}
	
	public void draw(){
		content.repaint();
	}
}
