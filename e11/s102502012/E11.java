package ce1002.e11.s102502012;

import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

public class E11 extends TimerTask{

	private static MyFrame window;
	
	public static void main(String[] args) {
		window = new MyFrame(400, 300);
		window.setTitle("s102502012");
		window.setResizable(false);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		window.setLayout(null);
		
		Timer timer = new Timer();
		timer.schedule(new E11(), 1000, 100);
	}

	@Override
	public void run() {
		window.repaint();
	}

}
