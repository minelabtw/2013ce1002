package ce1002.e11.s102502012;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class MyPanel extends JPanel {

	private int radius = 50;
	private int edge = 50;
	private int width;
	private int height;
	
	public MyPanel(int w, int h){
		width = w;
		height = h;
		setBounds(0, 0, width, height);
	}
	
	public void paintComponent(Graphics g){
		Random rand = new Random();
		int color = rand.nextInt(3);
		int type = rand.nextInt(2);
		
		int x = rand.nextInt(width);
		int y = rand.nextInt(height);
		
		
		if(color == 0)
			g.setColor(Color.red);
		else if(color == 1)
			g.setColor(Color.green);
		else if(color == 2)
			g.setColor(Color.blue);
		
		if(type == 0){ // circle
			while(x + 2 * radius > width) // do not exceed the border
				x = rand.nextInt(width);
			while(y + 2 * radius > height) // do not exceed the border
				y = rand.nextInt(height);
			g.fillOval(x, y, radius, radius);
		}
		else if(type == 1){
			while(x + edge > width) // do not exceed the border
				x = rand.nextInt(width);
			while(y + edge > height) // do not exceed the border
				y = rand.nextInt(height);
			g.fillRect(x, y, edge, edge);
		}
	}
}
