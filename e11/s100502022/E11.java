package ce1002.e11.s100502022;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class E11 extends JFrame{
	public imagePanel ip = new imagePanel();
	public int image, color, x,y;
	public E11(){
		setTitle("E11-100502022");
		setBounds(400,100,500,500);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Timer timer = new Timer();
	    timer.schedule(new AppearImage(), 0, 1000);
	    add(ip);
	}
	public static void main(String[] args){
		new E11();
	}
	class AppearImage extends TimerTask{
		public void run(){
			//postion color image
			Random ran = new Random();
			image = ran.nextInt(2);
			color = ran.nextInt(3);
			x = ran.nextInt(400);
			y = ran.nextInt(400);
			//reset
			ip.setValue(x, y, color, image);
			//repaint
			repaint();
		}
	}
}

class imagePanel extends JPanel{
	private Color[] colorset = {Color.green, Color.blue, Color.red};
	int image,color,x,y;
	public imagePanel(){
		setBounds(250,250, 55, 55);
	}
	public void setValue(int x, int y,int color, int image){
		this.image = image;
		this.color = color;
		this.x = x;
		this.y = y;
		setBounds(x, y, 55, 55);
	}
	public void paintComponent(Graphics g){
		g.setColor(colorset[color]);
		if(image==0){
			g.fillRect(0, 0, 50, 50);
		}
		else{
			g.fillOval(0, 0, 50, 50);
		}
		
	}
}