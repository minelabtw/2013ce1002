package ce1002.e11.s102502535;

import java.awt.event.*;
import java.awt.*;
import java.util.Random;
import javax.swing.*;

public class MyFrame extends JFrame {

	JFrame frame = new JFrame("E11-102502535");
	Timer timer = new Timer(1000, new TimerListener());
	Random random = new Random();

	int x = random.nextInt(350);
	int y = random.nextInt(350);

	MyFrame() {
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setLayout(null);
		timer.start();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponents(g);
		g.fillOval(x, y, 50, 50);
	}

	private class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}

	}

}
