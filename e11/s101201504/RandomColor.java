package ce1002.e11.s101201504;

import java.awt.Color;
import java.util.Random;

public class RandomColor {
	RandomColor() {

	}

	public Color getcolor() { // select Color
		Random ran = new Random();
		int i = ran.nextInt(9);
		if (i == 1)
			return Color.RED;
		if (i == 2)
			return Color.ORANGE;
		if (i == 3)
			return Color.YELLOW;
		if (i == 4)
			return Color.GREEN;
		if (i == 5)
			return Color.BLUE;
		if (i == 6)
			return Color.MAGENTA;
		if (i == 7)
			return Color.BLACK;
		if (i == 8)
			return Color.white;
		else
			return Color.gray;

	}
}
