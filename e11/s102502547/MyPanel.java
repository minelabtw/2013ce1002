package ce1002.e11.s102502547;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import java.util.*;
import java.util.Random;

public class MyPanel extends JPanel {

	Timer timer = new Timer();
	Random ran = new Random();
	int i = 1;
	int x = 700;
	int y = 500;
	int color = ran.nextInt(3);
	int shape = ran.nextInt(2);

	public MyPanel() {
		setBounds(0, 0, 800, 600);
		change();
		timer.schedule(new Task(), 0, 1000); //timer
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (color == 0)
			g.setColor(Color.red);
		if (color == 1)
			g.setColor(Color.blue);
		if (color == 2)
			g.setColor(Color.green);

		if (shape == 0)
			g.fillRect(ran.nextInt(x), ran.nextInt(y), 100, 100);
		if (shape == 1)
			g.fillOval(ran.nextInt(x), ran.nextInt(y), 100, 100); //隨機位置

		g.dispose();
		}

	private void change() {
		color = ran.nextInt(3);
		shape = ran.nextInt(2); //隨機顏色、形狀
		this.repaint();
	}

	class Task extends TimerTask {
		
		@Override
		public void run() {
			MyPanel.this.change();
		}
		
	}
	
}
