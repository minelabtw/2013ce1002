package ce1002.e11.s102502547;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	MyFrame() {
		setLayout(null);
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(new MyPanel());
		setVisible(true);

	};

}
