package ce1002.e11.s102502551;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyPanel panel = new MyPanel();
	MyFrame () {
		setSize(500, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		add(panel);
		setVisible(true);
	}
}
