package ce1002.e11.s102502520;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class Panel extends JPanel{
	
	protected int x,y;
	protected int z,w;
	public Panel()
	{
		setBounds(0,0,600,600);	
		setVisible(true);
		
	}
	public void paintComponent(Graphics g)
	{
		Random r = new Random();
		x = r.nextInt(500);
		y = r.nextInt(500);
		z = r.nextInt(3)+1;
		w = r.nextInt(2)+1;
		
		switch(z)
		{
		case 1:
			g.setColor(Color.blue);
			break;
		case 2:
			g.setColor(Color.GREEN);
			break;
		case 3:
			g.setColor(Color.RED);
			break;
		
		}
		switch(w)
		{
		case 1:
			g.fillRect(x, y, 50, 50);
			break;
		case 2:
			g.fillOval(x, y, 50, 50);
			break;		
		}
		
	}
}
