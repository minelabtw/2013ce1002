package ce1002.e11.s101303504;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;

import java.util.*;

public class MyShape extends JPanel{

	private Random r1 = new Random();
	private Random r2 = new Random();
	private Random r3 =new Random();
	private Random r4 =new Random();
	private Timer timer = new Timer(1000, new TimerListener());
	private int i=1,j=1,x=0,y=0;
	MyShape()
	{
		timer.start();
		this.setSize(450, 450);
	}
	 class TimerListener implements ActionListener {
	      @Override 
	      public void actionPerformed(ActionEvent e) {
	    	i = r1.nextInt(2)+1;
	  		j = r2.nextInt(3)+1;
	  		x = r3.nextInt(500);
	  		y = r4.nextInt(500);
	        repaint();
	      }
	    }
	 
	 protected void paintComponent(Graphics g){
		 super.paintComponent(g);
		 switch(j){
		 case 1:
			 g.setColor(Color.blue);
			 break;
		 case 2:
			 g.setColor(Color.green);
			 break;
		 case 3:
			 g.setColor(Color.red);
			 break;
		 }
		 switch(i){
		 case 1:
			 g.fillOval(x, y,50, 50);
			 break;
		 case 2:
			 g.fillRect(x, y, 50, 70);
			 break;
		 }
	 }
	
}
	
