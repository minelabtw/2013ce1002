package ce1002.e11.s102502515;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel {
	Random ran = new Random();//Random
	int type = ran.nextInt(6);//random 0-5 for type and color

	MyPanel(){//panel settings
		Timer timer = new Timer(1000,task);//int for delay and task for actionListener
		setBounds(0,0,480,460);
		timer.start();//Starts the Timer, causing it to start sending action events to its listeners.
	}
	
	//ActionListener for timer to use and repeat
	ActionListener task = new ActionListener(){
		public void actionPerformed(ActionEvent AE){
		repaint();
		}
	};
		//draw picture
		@Override
		protected void paintComponent (Graphics g){
			super.paintComponent(g);
			switch(type){//use switch to select which one do it draw.
			case 0:
				g.setColor(Color.RED);
				g.fillOval(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);//the location is random.
				type = ran.nextInt(6);//new a random
				break;//jump out the switch
			case 1:
				g.setColor(Color.GREEN);
				g.fillOval(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 2:
				g.setColor(Color.BLUE);
				g.fillOval(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 3:
				g.setColor(Color.RED);
				g.fillRect(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 4:
				g.setColor(Color.GREEN);
				g.fillRect(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			case 5:
				g.setColor(Color.BLUE);
				g.fillRect(ran.nextInt(450) + 10, ran.nextInt(420) + 10,25, 25);
				type = ran.nextInt(6);
				break;
			}
		}
}
