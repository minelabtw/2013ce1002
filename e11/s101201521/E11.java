package ce1002.e11.s101201521;
import javax.swing.*;

public class E11 extends JFrame{
	private MyPanel myPanel = new MyPanel();
	public E11(String title){
		super(title);
		//add panel
		add(myPanel);
		//setting
		setSize(500,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		E11 frame = new E11("E11-101201521");
	}

}
