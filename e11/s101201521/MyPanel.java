package ce1002.e11.s101201521;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;
public class MyPanel extends JPanel{
	//the color to paint
	private int color;
	//the shape to paint
	private int shape;
	//the x location to paint
	private int x;
	//the t location to paint
	private int y;
	private Random random = new Random();
	//time to get random color, shape, and location
	private Timer go = new Timer(1000, new TListener());
	public MyPanel(){
		color = random.nextInt(3);
		shape = random.nextInt(2);
		x = random.nextInt(434);
		y = random.nextInt(413);
		go.start();
	}
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//choose color
		switch(color){
		case 0:
			g.setColor(Color.RED);
			break;
		case 1:
			g.setColor(Color.GREEN);
			break;
		case 2:
			g.setColor(Color.BLUE);
			break;
		}
		//choose shape
		switch(shape){
		case 0:
			g.fillOval(x, y, 50, 50);
			break;
		case 1:
			g.fillRect(x, y, 50, 50);
			break;
		}
	}
	class TListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			color = random.nextInt(3);
			shape = random.nextInt(2);
			x = random.nextInt(434);
			y = random.nextInt(413);
			repaint();
		}
		
	}
}
