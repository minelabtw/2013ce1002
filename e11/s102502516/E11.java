package ce1002.e11.s102502516;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class E11 extends JFrame {
	int x, y, width, height;

	public static void main(String[] args) {
		E11 e11 = new E11();
	}

	public E11() {
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(1100, 300, 700, 700);
		setVisible(true);
		setTitle("E11-102502516"); // 視窗設定
		add(new DrawShape()); // 畫圖
	}

	public class DrawShape extends JPanel {
		private Timer timers = new Timer(1000, new TimerListener());

		DrawShape() {
			timers.start();
			setBounds(0, 0, 700, 700);
		}

		public void RandomNewShape() {
			width = (int) (Math.random() * 80 + 120);
			height = (int) (Math.random() * 80 + 120);
			x = (int) (Math.random() * (700 - 30 - width)) + 10;
			y = (int) (Math.random() * (700 - 80 - height)) + 10; // 隨機位置與大小
			repaint();

		}

		// @Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(new Color((int) (Math.random() * 256), (int) (Math
					.random() * 256), (int) (Math.random() * 256), (int) (Math
					.random() * 56 + 200))); // 隨機顏色

			switch ((int) (Math.random() * 6)) { // 隨機形狀

			case 0: // 矩型
				g.fillRect(x, y, width, height);
				break;

			case 1: // 橢圓
				g.fillOval(x, y, width, height);
				break;

			case 2: // 三角
				Polygon triangle = new Polygon();
				triangle.addPoint(x + width / 2, y);
				triangle.addPoint(x, y + height);
				triangle.addPoint(x + width, y + height);
				g.fillPolygon(triangle);
				break;

			case 3: // 十字
				if (width == Math.max(width, height)) {
					width = width + height;
					height = width - height;
					width = width - height;
				} // 使width < height
				width *= 0.5;
				g.setColor(new Color((int) (Math.random() * 256), (int) (Math
						.random() * 256), (int) (Math.random() * 256)));
				g.fillRect(x + (height - width) / 2, y, width, height);
				g.fillRect(x, y + (height - width) / 2, height, width);
				break;

			case 4: // n邊形
				x += width / 2;
				y += height / 2;
				Polygon ploygon = new Polygon();
				int n = (int) (Math.random() * 3 + 5);
				for (int j = 0; j < n; j++) {
					ploygon.addPoint((int) (x + Math.cos(Math.PI * 2 / n * j)
							* width / 2),
							(int) (y + Math.sin(Math.PI * 2 / n * j) * height
									/ 2));
				}
				g.fillPolygon(ploygon);
				break;

			case 5: // 奇怪的東西
				Polygon strange = new Polygon();
				int strangeN = (int) (Math.random() * 50) + 3;
				for (int j = 0; j < strangeN; j++) {
					strange.addPoint((int) (Math.random() * width) + x,
							(int) (Math.random() * height) + y);
				}
				g.fillPolygon(strange);
				break;

			default:
				break;
			}
		}

		class TimerListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				RandomNewShape();
			}
		}
	}
}
