package ce1002.e11.s102502023;


import java.util.*;

import javax.swing.JFrame;

public class E11 {
  public static void main(String[] args) {
	 Timer timer = new Timer(); // initialize Timer timer
	 //timer.start();
	 MyFrame frame = new MyFrame();
	 frame.setSize(500, 500);
	 frame.setTitle(null);
	 frame.setLocationRelativeTo(null);
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	 frame.setVisible(true);
	 timer.schedule(new Task(frame),500, 1000);
  }
  
}
