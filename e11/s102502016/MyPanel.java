package ce1002.e11.s102502016;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener {
	private Timer timer = new Timer(1000, this);
	private ImageIcon a = new ImageIcon("Knight.jpg");

	MyPanel() {
		this.setSize(1280, 720);
		this.timer.start();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int graphWidth = 200;
		int graphHeight = 200;
		int x = (int) (Math.random() * (1280 - graphWidth) + 1);// 隨機座標
		int y = (int) (Math.random() * (720 - graphHeight) + 1);// 隨機座標
		int color = (int) (Math.random() * 3);// 隨機顏色種類
		int type = (int) (Math.random() * 2);
		switch (color) {
		case 0:
			g.setColor(Color.RED);
			break;
		case 1:
			g.setColor(Color.GREEN);
			break;
		case 2:
			g.setColor(Color.BLUE);
		}
		switch (type) {
		case 0:
			g.fillOval(x, y, graphWidth, graphHeight);
			break;
		case 1:
			g.fillRect(x, y, graphWidth, graphHeight);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}
}