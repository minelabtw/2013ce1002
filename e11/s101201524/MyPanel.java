package ce1002.e11.s101201524;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener{
	//graphic's data
	private int x,y,side;
	private Color color;
	//repaint timer
	private Timer timer = new Timer(1000, this);
	MyPanel(){
		//set panel's status
		setLayout(null);
		setBounds(0, 0, 480, 500);
		//set the side and start paint
		side = 50;
		setGraphic();
		timer.start();
	}
	//create a random location and color
	private void setGraphic(){
		x = (int)(Math.random()*(getWidth()-side));
		y = (int)(Math.random()*(getHeight()-side));
		switch((int)(Math.random()*3)){
			case 0 : color = Color.red;
					 break;
			case 1 : color = Color.green;
			 		 break;
			case 2 : color = Color.blue;
			 		 break;
		}
	}
	public void actionPerformed(ActionEvent e) {
		setGraphic();
		repaint();
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(color);
		//paint a rectangle or oval
		if((int)(Math.random()*2) == 0)
			g.fillOval(x, y, side, side);
		else
			g.fillRect(x, y, side, side);
	}
}
