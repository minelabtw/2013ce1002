package ce1002.e11.s102502022;

import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyPanel mypanel = new MyPanel();
	MyFrame () {
		setSize(500, 500); //大小
		setLocationRelativeTo(null); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setLayout(null); //排版
		add(mypanel);
		setVisible(true); //顯示
	}
}