package ce1002.e11.s102502022;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel{
	
	Timer timer = new Timer(1000, new TimerListener());
	Random rand = new Random();
	MyPanel () {
		setSize(500, 500); //大小
		setLocation(5, 5); //位置
		timer.start(); 
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		if ( rand.nextInt(3) == 0 )
		{
			g.setColor(Color.RED);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
		else if ( rand.nextInt(3) == 1 )
		{
			g.setColor(Color.BLUE);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
		else if ( rand.nextInt(3) == 2)
		{
			g.setColor(Color.GREEN);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}	
			else
			{
				g.fillOval(rand.nextInt(430), rand.nextInt(430), 30, 30);
			}
		}
	}
	
	class TimerListener implements ActionListener{
		public void actionPerformed( ActionEvent e ) {
			repaint();//重新更新畫面
		}
	}
}
