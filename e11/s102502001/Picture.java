package ce1002.e11.s102502001;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;


class Picture extends JPanel{
	private Timer timer = new Timer(1000, new TimerListener());       //set timer 
	private Random rand = new Random();                               //set a random number
	
	Picture(){
		timer.start();
	}
protected void paintComponent(Graphics g){                            //draw picture 
	super.paintComponent(g);
	if(rand.nextInt(4)==0){
		g.setColor(Color.RED);
		g.fillRect(5, 5, 40, 40);
	}
	else if(rand.nextInt(4)==1){
		g.setColor(Color.GREEN);
		g.fillRect(160, 160, 40, 40);
	}
	else if(rand.nextInt(4)==2){
		g.setColor(Color.GREEN);
		g.fillOval(80,80,40,40);
	}
	else {
		g.setColor(Color.BLUE);
		g.fillRect(240, 240, 40, 40);
	}
}

private class TimerListener implements ActionListener{             //actionlistener
	public void actionPerformed(ActionEvent e){
		repaint();                                                 //repaint
	}
}
}
