package ce1002.e11.s102502009;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyFrame extends JFrame {
	MyFrame() {
		add(new RandomImagePanel());
	}

	class RandomImagePanel extends JPanel {
		private Timer timer = new Timer(1000, new MyFrame.TimerListener()); //timer(run per 1/1000 second, run() method)
		

		RandomImagePanel() {
			this.timer.start(); //reset the timer 
		}

		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			int x = (int) (Math.random() * ((getWidth()-50) + 1.0D)); //random(1~Width-50)
			int y = (int) (Math.random() * ((getHeight()-50) + 1.0D));
			int color = (int) (Math.random() * 3.0D + 1.0D);
			int type = (int) (Math.random() * 2.0D + 1.0D);
			switch (color) {
			case 1:
				g.setColor(Color.RED);
				break;
			case 2:
				g.setColor(Color.GREEN);
				break;
			case 3:
				g.setColor(Color.BLUE);
			}
			switch (type) {
			case 1:
				g.fillOval(x, y, 50, 50);
				break;
			case 2:
				g.fillRect(x, y, 50, 50);
			}
		}
	}

	class TimerListener implements ActionListener {
		TimerListener() {
		}

		public void actionPerformed(ActionEvent arg0) {
			MyFrame.this.repaint(); //repaint
		}
	}
}