package ce1002.e11.s101201023;

import java.util.Timer;

import javax.swing.JFrame;

public class MyFrame extends JFrame
{
	private MyGraph graph = new MyGraph();
	
	public MyFrame()
	{
		setSize(600 , 600);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		setVisible(true);
		
		add(graph);
		
		//以一秒為週期隨機製造圖案
		Timer timer = new Timer();
		timer.schedule(new GraphTask(graph), 1000 , 1000);
		
	}
}
