package ce1002.e11.s101201023;

import java.util.TimerTask;

public class GraphTask extends TimerTask
{
	private MyGraph graph = new MyGraph();
	
	public GraphTask(MyGraph g)
	{
		graph = g;
	}
	
	@Override
	public void run()
	{
		graph.repaint();              //anew painting graph
	}
}
