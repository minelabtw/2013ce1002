package ce1002.e11.s101201023;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.JPanel;

public class MyGraph extends JPanel
{
	public MyGraph()
	{
		setBounds(0,0,500,500);
	}
	
	//produce a graph in random
	@Override
	protected void paintComponent(Graphics g)
	{
		Random random = new Random();
		int i = random.nextInt(2);
		
		super.paintComponent(g);
		g.setColor(getColor());
		if (i == 0)
			g.fillRect(random.nextInt(450),random.nextInt(450),50,50);
		else
			g.fillOval(random.nextInt(450),random.nextInt(450),50,50);
	}
	
	//color in random
	public Color getColor()
	{
		Random random = new Random();
		int i = random.nextInt(3);
		
		if (i == 0)
			return Color.RED;
		else if(i == 1)
			return Color.GREEN;
		else
			return Color.BLUE;
	}
}