package ce1002.e11.s995002046;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class Panel extends JPanel{
	int color=0;
	int shape=0;
	Panel(){
	}
	void paintcolorfulshape(int color,int shape){//set color and shape and repaint
		this.color=color;
		this.shape=shape;
		repaint();
	}
	@Override public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//decide what to draw,color and shape
		if(color==1){
			g.setColor(Color.red);
		}
		else if(color==2){
			g.setColor(Color.green);
		}
		else if(color==3){
			g.setColor(Color.blue);
		}		
		if(shape==1){
			g.fillRect(0, 0, 200, 200);
		}
		else if(shape==2){
			g.fillOval(0, 0, 200, 200);
		}
	}
	
}