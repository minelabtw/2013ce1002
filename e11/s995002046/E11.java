package ce1002.e11.s995002046;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Timer;
import javax.swing.JFrame;

class E11{
	Timer timer;
	Panel p=new Panel();
	private static final int TIMER_DELAY = 1000;
	E11(){
		//create frame and add panel
		JFrame myf =new JFrame();
		myf.setLayout(null);
		myf.setSize(500, 500);
		myf.setVisible(true);
		myf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myf.add(p);
		p.setBounds(50, 50, 300, 300);
		//new timer and set what it needs to do
		new javax.swing.Timer(TIMER_DELAY, new ActionListener() {
		     public void actionPerformed(ActionEvent e) {
		    	 //random two numbers and call panel to paint it
		    	Random r1 = new Random();  
		        Random r2 = new Random();  
		        int i1 = r1.nextInt(3)+1;
		        int i2 = r2.nextInt(2)+1;
		        p.paintcolorfulshape(i1, i2);
		        System.out.print(""+i1+i2);
		     }
		  }).start();
	}


	public static void main(String[] args){
		new E11();
	}
}