package ce1002.e11.s102502542;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.*;

public class MyFrame extends JFrame
{
	MyFrame()
	{
		setSize(500,400);
		setTitle("E11-102502542");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);//將frame視窗固定在center
		setLayout(null);//設定排版
		MyPanel p = new MyPanel();
		add(p);
		setVisible(true);
	}
}
