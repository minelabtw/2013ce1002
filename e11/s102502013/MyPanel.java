package ce1002.e11.s102502013;

import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class MyPanel extends JPanel{
	private Timer timer = new Timer(1000, new TimerListener());//use Timer
	private Random rand = new Random();//use Random
	MyPanel(){
		setBounds(5, 5, 495, 495);
		timer.start();//start Timer
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//create a random integer value for switch color
		int color = rand.nextInt(3);
		if(color == 0){
			//create a random integer value for switch Rectangle or Circle
			int type = rand.nextInt(2);
			g.setColor(Color.red);
			if(type == 0){
				g.fillRect(rand.nextInt(300), rand.nextInt(300), 40, 40);
			}
			else if (type == 1)
				g.fillOval(rand.nextInt(420), rand.nextInt(420), 40, 40);
		}
		
		else if(color == 1){
			//create a random integer value for switch Rectangle or Circle
			int type = rand.nextInt(2);
			g.setColor(Color.green);
			if(type == 0){
				g.fillRect(rand.nextInt(420), rand.nextInt(420), 40, 40);
			}
			else if (type == 1)
				g.fillOval(rand.nextInt(420), rand.nextInt(420), 40, 40);
		}
		
		else if(color == 2){
			//create a random integer value for switch Rectangle or Circle
			int type = rand.nextInt(2);
			g.setColor(Color.blue);
			if(type == 0){
				g.fillRect(rand.nextInt(420), rand.nextInt(420), 40, 40);
			}
			else if (type == 1)
				g.fillOval(rand.nextInt(420), rand.nextInt(420), 40, 40);
		}
	}
	class TimerListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
}

