package ce1002.e11.s102502013;
import javax.swing.JFrame;
public class MyFrame extends JFrame{
	MyFrame(){
		setLayout(null);
		setSize(500, 500);
		setTitle("102502013");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(true);
		setLocationRelativeTo(null);
		setVisible(true);
		MyPanel panel = new MyPanel();
		add(panel);
	}
}
