package ce1002.e11.s102502554;

import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyPanel Panel = new MyPanel(500,500);//set a new panel and bounds
	
	MyFrame(){
		setTitle("E11-102502554");//GUI title
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setBounds(0,0,550,550);
	    add(Panel);//add panel
	    setVisible(true);
	}

}
