package ce1002.e11.s102502554;

import java.awt.*;

import javax.swing.*;

import java.util.*;
import java.util.Timer;//import to use timer

public class MyPanel extends JPanel{
	
	Random rand = new Random();//set a random
	Timer timer = new Timer ();//set a timer
	
	int shape;
	int color;
	int size = 50;//the size of graph
	int maxX;
	int maxY;
	Color [] colors = {Color.red,Color.green,Color.blue};//use array to set the colors
	
	public MyPanel (int width , int height){
		
		maxX = width - size;
		maxY = height - size;//location
		timer.schedule(new task(), 0, 1000);//task , delay , period
		change();//change graph and color + repaint
	}
	
	public void change(){
		
		shape = rand.nextInt(2);//random shape
		color = rand.nextInt(3);//random color
		this.repaint();//repaint
	}
	
	@Override
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		g.setColor(colors[color]);//set color
		
		if(shape == 1){
			g.fillRect(rand.nextInt(maxX), rand.nextInt(maxY), size, size);
		}else {
			g.fillOval(rand.nextInt(maxX), rand.nextInt(maxY), size, size);
		}//Draw graph
		
		g.dispose();
	    
	}
	
	public class task extends TimerTask{
		
		public void run(){
			
			change();
			
		}
		
	}
}
