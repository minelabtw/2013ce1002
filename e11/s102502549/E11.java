package ce1002.e11.s102502549;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class E11 extends JPanel {

	private Random random = new Random();//神奇的random
	private int xRandom;//亂數x座標
	private int yRandom;//亂數y座標
	private int colorRandom;//亂數顏色
	private int shapeRandom;//亂數形狀
	
	//這是timer
	private Timer timer = new Timer(1000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	});

	//建構式
	public E11() {
		setPreferredSize(new Dimension(500, 500));
		timer.start();
	}

	public static void main(String[] args) {

		//建立panel然後加入frame
		E11 panel = new E11();
		JFrame frame = new JFrame();

		frame.add(panel);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		//各種取亂數
		xRandom=random.nextInt(451);
		yRandom=random.nextInt(451);
		colorRandom=random.nextInt(3);
		shapeRandom=random.nextInt(2);
		
		//設定顏色
		switch (colorRandom) {
		case 0:
			g.setColor(Color.RED);
			break;
		case 1:
			g.setColor(Color.GREEN);
			break;
		case 2:
			g.setColor(Color.BLUE);
			break;
		}
		
		//畫圖
		switch (shapeRandom) {
		case 0:
			g.fillRect(xRandom, yRandom, 50, 50);
			break;
		case 1:
			g.fillOval(xRandom, yRandom, 50, 50);
			break;
		}
	}
}
