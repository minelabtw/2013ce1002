package ce1002.e11.s102502546;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;
public class MyFrame extends JFrame {
	Timer time = new Timer(1000,new timerlistener());
	Random rand = new Random();
	MyPanel panel = new MyPanel();
	
	MyFrame(){
		setSize(600,600); //設定JFrame的視窗大小。
		setLocationRelativeTo(null); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //設定JFrame按下叉叉關閉鈕可以關閉程式。
		setVisible(true); //顯示JFrame。
		time.start();
		add(panel);
	}
	
	
	class timerlistener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
}
