package ce1002.e11.s102502546;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	Random rand = new Random();
	
	MyPanel(){
		setSize(500,500);
	}
	
	protected void paintComponent(Graphics g)
	{
		
		if(rand.nextInt(3) == 0)
		{
			g.setColor(Color.RED);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(550),rand.nextInt(550),50,50);
			}
			else
			{
				g.fillOval(rand.nextInt(550),rand.nextInt(550),50,50);
			}
		}
		else if(rand.nextInt(3) == 1)
		{
			g.setColor(Color.GREEN);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(550),rand.nextInt(550),50,50);
			}
			else
			{
				g.fillOval(rand.nextInt(550),rand.nextInt(550),50,50);
			}
		}
		else
		{
			g.setColor(Color.BLUE);
			if ( rand.nextInt(2) == 0 )
			{
				g.fillRect(rand.nextInt(550),rand.nextInt(550),50,50);
			}
			else
			{
				g.fillOval(rand.nextInt(550),rand.nextInt(550),50,50);
			}
		}
	}
	
}
