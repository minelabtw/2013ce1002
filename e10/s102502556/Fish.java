package ce1002.e10.s102502556;

class Fish extends Entity {
	//constructor，並設定name的值
	Fish (String name) {
		super(name);
	}
	//Override swim函式
	public void swim () {
		System.out.println("Fish: " + name + " is swimming!");
	}
}
