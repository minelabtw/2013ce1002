package ce1002.e10.s102502556;

class Shark extends Fish {
	//constructor，並設定name的值
	Shark (String name) {
		super(name);
	}
	//Override swim函式
	public void swim () {
		System.out.println("Shark: " + name + " is swimming!");
	}
}
