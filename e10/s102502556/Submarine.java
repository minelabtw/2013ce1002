package ce1002.e10.s102502556;

class Submarine extends Entity {
	//constructor，並設定name的值
	Submarine (String name) {
		super(name);
	}
	//Override swim函式
	public void swim () {
		System.out.println("Submarine " + name + " is swimming!");
	}
}	
