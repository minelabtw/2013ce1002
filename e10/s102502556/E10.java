package ce1002.e10.s102502556;

import java.util.ArrayList;
import java.util.List;
 
public class E10 {
 
	public static void main(String[] args) {
		//儲存各種Object的ArrayList
		List<ISwimable> swimObjs = new ArrayList<ISwimable>();
		//把物件新增進去ArrayList裡面
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));
		//call每個Object裡的swim函式
		for (ISwimable iSwimable : swimObjs) {
			iSwimable.swim();
		}
	}
 
}


