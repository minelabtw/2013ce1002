package ce1002.e10.s102502515;
public class Shark extends Entity implements ISwimable {
	public Shark(String name) 
		{
			super(name);//catch
		}
	    
	    @Override
	    public void swim() {
	        System.out.println("Shark: " + name + " is swimming!");
	    }
}
