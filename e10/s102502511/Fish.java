package ce1002.e10.s102502511;

public class Fish extends Entity implements ISwimable{ //繼承entity 運用iswimable
	Fish(String name){
		super(name); //定義名字
	}
	
	@Override
	public void swim(){ //重新定義swim的內容
		System.out.println("Fish: " + name + " is swimming!");		
	}
}

