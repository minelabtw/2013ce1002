package ce1002.e10.s102502511;

public class Submarine extends Entity implements ISwimable{ //繼承entity 運用虛擬的iswimable

	Submarine(String name){
		super(name); //定義名稱
	}
	
	@Override
	
	public void swim(){ //重新定義swim
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
