package ce1002.e10.s102502050;

public class Submarine extends Entity implements ISwimable{
	public Submarine(String name)
	{
		this.name=name;
	}
	public void swim()
	{
		System.out.println("Submarine: " + name + " is swimming!");
	}
	

}
