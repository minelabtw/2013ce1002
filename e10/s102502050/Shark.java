package ce1002.e10.s102502050;

public class Shark  extends Fish implements ISwimable{
	
	public Shark(String name)
	{
		this.name=name;
	}
	
	public void swim()
	{
		System.out.println("Shark: " + name + " is swimming!");
	}
}
