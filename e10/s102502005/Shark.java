package ce1002.e10.s102502005;

public class Shark extends Entity implements ISwimable {

	public Shark(String name) {//繼承自Entity的建構子。
		super(name);
	}

	@Override
	public void swim() {//覆寫自ISwimable的函式。
		System.out.println("Shark: " + name + " is swimming!");
	}

}
