package ce1002.e10.s102502005;

public class Submarine extends Entity implements ISwimable {

	public Submarine(String name) {//繼承自Entity的建構子。
		super(name);
	}

	@Override
	public void swim() {//覆寫自ISwimable的函式。
		System.out.println("Submarine: " + name + " is swimming!");

	}

}
