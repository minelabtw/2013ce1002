package ce1002.e10.s102502027;


public class Shark extends Fish{

	Shark(String name) {
		super(name);
		
	}
	
	@Override
	public void swim() {
		System.out.println(name+" is swimming!");
		
	}
	
}
