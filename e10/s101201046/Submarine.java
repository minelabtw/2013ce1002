package ce1002.e10.s101201046;

public class Submarine extends Entity implements ISwimable{

	public Submarine(String name) {//construct submarine
		super(name);
	}

	@Override
	public void swim() {//way of submarine swimming
		System.out.println("Submarine: " + name + " is swimming!");
	}
	
}
