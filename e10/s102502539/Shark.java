package ce1002.e10.s102502539;

public class Shark extends Fish
{
	Shark(String s)
	{                                  
		super(s);
	}
	public void swim()
	{								
		System.out.println("Shark: " + this.name + " is swimming!");
	}
}
