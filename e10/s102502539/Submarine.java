package ce1002.e10.s102502539;

public class Submarine extends Entity implements ISwimable
{
	Submarine(String s)
	{								
		super(s);
	}
	public void swim()
	{								
		System.out.println("Submarine: " + this.name + " is swimming!");
	}
}
