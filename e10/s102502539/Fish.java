package ce1002.e10.s102502539;

public class Fish extends Entity 
{
	Fish(String s)
	{					
		super(s);
	}
	
	public void swim()
	{							
		System.out.println("Fish: " + this.name + " is swimming!");
	}
}