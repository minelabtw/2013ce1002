package ce1002.e10.s102502032;

public class Submarine extends Entity implements ISwimable
{
	public Submarine()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public Submarine(String name)
	{
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim()
	{
		System.out.println("Submarine: " + super.name + " is swimming!");
	}
}
