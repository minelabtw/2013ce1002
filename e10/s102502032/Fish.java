package ce1002.e10.s102502032;

public class Fish extends Entity implements ISwimable
{
	public Fish()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public Fish(String name)
	{
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void swim()
	{
		System.out.println("Fish: " + super.name + " is swimming!");
	}
}
