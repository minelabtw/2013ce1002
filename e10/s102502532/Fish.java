package ce1002.e10.s102502532;

public class Fish extends Entity implements ISwimable{

	public Fish(String name){
		super.name = name;
	}
	
	@Override
	public void swim(){                                  //public
		System.out.println("Fish: " + super.name + " is swimming!");
	}
	
}
