package ce1002.e10.s102502532;

public class Submarine extends Entity implements ISwimable {

	public Submarine(String name) {
		super.name = name;
	}

	@Override
	public void swim() {
		System.out.println("Submarine: " + super.name + " is swimming!");
	}
}
