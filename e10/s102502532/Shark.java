package ce1002.e10.s102502532;

public class Shark extends Entity implements ISwimable {

	public Shark(String name){
		super.name = name;
	}
	
	@Override
	public void swim() {
		System.out.println("Shark: " + super.name + " is swimming!");
	}
}
