package ce1002.e10.s102502506;

public class Shark extends Fish implements ISwimable{
	public Shark(String name){
		super(name);
	}

	@Override
	public void swim() {
		System.out.println("Shark: "+name+" is swimming");
	}
	
}
