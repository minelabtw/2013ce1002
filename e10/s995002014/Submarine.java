package ce1002.e10.s995002014;

//潛水艇，基本上code跟魚差不多
public class Submarine extends Entity implements ISwimable {

	public Submarine(String name) {
		super(name);
	}
	
	public void swim() {
		System.out.println("Submarine: "+name+" is swimming!");
	}
}
