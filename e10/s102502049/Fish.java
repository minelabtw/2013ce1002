package ce1002.e10.s102502049;

public class Fish extends Entity implements ISwimable{
	
	Fish(String name){	// constructor
		super(name);
	}
	
	public void swim(){	// show action
		System.out.println("Fish: "+ name +" is swimming!");
	}
	
}
