package ce1002.e10.s102502049;

public abstract class Entity implements ISwimable{ // abstract class
	protected String name; // name uesd by sub-class
	
	Entity(String name){	// parameter constructor, polymorphism
		this.name = name;
	}
	
	public abstract void swim();	// abstract class
}
