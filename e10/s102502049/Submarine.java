package ce1002.e10.s102502049;

public class Submarine extends Entity implements ISwimable{
	
	Submarine(String name){	// constructor
		super(name);
	}
	
	public void swim(){	// show action
		System.out.println("Submarine: "+ name +" is swimming!");
	}

}
