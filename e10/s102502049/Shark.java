package ce1002.e10.s102502049;

public class Shark extends Fish implements ISwimable{
	
	Shark(String name){	// constructor
		super(name);
	}
	
	public void swim(){	// show action
		System.out.println("Shark: "+ name +" is swimming!");
	}

}
