package ce1002.e10.s102502562;

public class Submarine extends Entity implements ISwimable{
	Submarine(String name)//建構子初始化名子
	{
		super(name);
	}
	//Override
	public void swim()
	{
		System.out.println("Submarine: " + this.name + " is swimming!");
	}
}
