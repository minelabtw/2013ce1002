package ce1002.e10.s102502512;

public class Submarine extends Entity implements ISwimable {
	public Submarine(String name) {
		super(name);								//Override the name
	}
	public void swim(){
		System.out.println("Submarine: " + name + " is swimming!");
	};
}