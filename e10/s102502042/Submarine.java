package ce1002.e10.s102502042;

public class Submarine extends Entity{

	String type; 
	
	//constructor
	public Submarine(String name) {
		super(name);
		type = "Submarine";
	}

	//override entity's swim
	public void swim()
	{
		System.out.print(type + ": ");
		super.swim();
	}
}
