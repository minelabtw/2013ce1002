package ce1002.e10.s102502042;

public class Fish extends Entity{

	String type;
	public Fish(String name) {
		super(name);
		type = "Fish";
	}
	
	//override Entity's swim
	public void swim()
	{
		System.out.print(type + ": ");
		super.swim();
	}
}
