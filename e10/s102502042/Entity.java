package ce1002.e10.s102502042;

public abstract class Entity implements ISwimable{
	public String name;
	 
	public Entity(String name) {
		this.name = name;
	}
	
	//implement swim
	public void swim()
	{
		System.out.println(this.name+" is swimming!");
	}
}
