package ce1002.e10.s101502205;

public class Shark extends Entity implements ISwimable{
	public Shark(String name) {
		super(name);		
	}
	public void swim() {
		// swimming!
		System.out.println("Shark: " + name + " is swimming!");
	}
}
