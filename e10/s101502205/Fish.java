package ce1002.e10.s101502205;

public class Fish extends Entity implements ISwimable{
	public Fish(String name) {
		super(name);		
	}
	public void swim() {
		// swimming!
		System.out.println("Fish: " + name + " is swimming!");
	}
}
