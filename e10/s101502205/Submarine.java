package ce1002.e10.s101502205;

public class Submarine extends Entity implements ISwimable{
	public Submarine(String name) {
		super(name);		
	}
	public void swim() {
		// swimming!
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
