package ce1002.e10.s102502022;

public class Submarine extends Entity implements ISwimable{

	public Submarine(String name,String type){
		super(name,type);
		type = "Submarine";
	}
	
	public void swim(){
		super.swim();
	}

}
