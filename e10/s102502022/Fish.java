package ce1002.e10.s102502022;

public class Fish extends Entity implements ISwimable{

	public Fish(String name,String type){
		super(name,type);
		type = "Fish";
	}
	
	public void swim(){
		super.swim();
	}

}
