package ce1002.e10.s102502022;

import java.util.ArrayList;
import java.util.List;

public class E10 {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<ISwimable> swimObjs = new ArrayList<ISwimable>();
		swimObjs.add(new Fish("f1","Fish"));
		swimObjs.add(new Fish("f2","Fish"));
		swimObjs.add(new Shark("s1","Shark"));
		swimObjs.add(new Submarine("sub1","Submarine"));
		swimObjs.add(new Shark("s2","Shark"));
 
		for (ISwimable iSwimable : swimObjs) {
			// everybody swim
			iSwimable.swim();
		}
	}

}
