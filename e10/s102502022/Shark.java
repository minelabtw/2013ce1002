package ce1002.e10.s102502022;

public class Shark extends Entity implements ISwimable{

	public Shark(String name,String type){
		super(name,type);
		type = "Shark";
	}
	
	public void swim(){
		super.swim();
	}

}
