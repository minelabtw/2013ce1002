package ce1002.e10.s102502022;

public abstract class Entity{
	
	public String name;
	public String type;
	
	public Entity(String name,String type) {
		this.name = name;
		this.type = type;
	}
	
	public void swim(){
		System.out.print(type+": ");
		System.out.println(name + " is swimming!");
	}

}
