package ce1002.e10.s102502047;

public class Fish extends Entity implements ISwimable{
	private String n;
	public Fish(String n) {
		super(n);
	}
	public void swim()
	{
		System.out.println("Fish: "+super.name+" is swimming!");
	}
}
