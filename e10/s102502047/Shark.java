package ce1002.e10.s102502047;

public class Shark extends Entity implements ISwimable{
	private String n;
	public Shark(String n) {
		super(n);
	}
	public void swim()
	{
		System.out.println("Shark: "+super.name+" is swimming!");
	}
}
