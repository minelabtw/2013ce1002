package ce1002.e10.s102502547;

public class Submarine extends Entity implements ISwimable{ //多重繼承
	
	Submarine(String name)
	{
		super(name); //呼叫父類別的建構式
	}
	
	@Override
	public void swim(){
		System.out.println("Submarine: " + name + " is swimming!");
	}

}
