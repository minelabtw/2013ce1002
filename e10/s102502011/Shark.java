package ce1002.e10.s102502011;

public class Shark extends Entity implements ISwimable {
	
	public Shark(String name) {
		super(name);						//Override the name
	}
	public void swim(){
		System.out.println("Shark: " + name + " is swimming!");
	};

}
