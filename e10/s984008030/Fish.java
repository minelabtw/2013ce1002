package ce1002.e10.s984008030;

public class Fish extends Entity implements ISwimable {
	
	public Fish(String name) {
		super(name);
	}
	
	public void swim(){
		System.out.println("Fish: " + this.name + " is swimming!");
	}
	
}
