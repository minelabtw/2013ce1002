package ce1002.e10.s984008030;

public class Submarine extends Entity implements ISwimable {
	
	public Submarine(String name) {
		super(name);
	}
	
	public void swim(){
		System.out.println("Submarine: " + this.name + " is swimming!");
	}
	
}