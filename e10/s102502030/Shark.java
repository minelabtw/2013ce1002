package ce1002.e10.s102502030;

public class Shark extends Fish {

	public Shark(String string) {
		name = string;
	}

	public void swim() {
		System.out.println( "Shark " + name + " is swimming!" );
	}
}
