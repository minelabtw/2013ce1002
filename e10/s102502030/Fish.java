package ce1002.e10.s102502030;

public class Fish extends Entity{

	public Fish() {
		
	}
	
	public Fish(String string) {
		name = string;
	}

	public void swim() {
		System.out.println( "Fish " + name + " is swimming!" );
	}
}
