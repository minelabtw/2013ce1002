package ce1002.e10.s102502553;

class Shark extends Fish {

	public Shark(String name) {
		super(name);	
	}
	
	public void swim()//override swim
	{
		System.out.println("Shark: " + name + " is swimming!");
	}

}
