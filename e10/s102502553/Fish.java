package ce1002.e10.s102502553;

class Fish extends Entity {

	public Fish(String name) {
		super(name);
	}

	public void swim()//override swim()
	{
		System.out.println("Fish: " + name + " is swimming!");
	}

}
