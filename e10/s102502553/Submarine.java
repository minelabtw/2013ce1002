package ce1002.e10.s102502553;

class Submarine extends Entity {
	public Submarine(String name) {
		super(name);
	}
	
	public void swim()//override swim()
	{
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
