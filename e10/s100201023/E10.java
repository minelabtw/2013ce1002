package ce1002.e10.s100201023;

import java.util.ArrayList;
import java.util.List;

public class E10
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		// list contains swimmers
		List<ISwimable> swimObjs = new ArrayList<ISwimable>();
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));
		 
		for (ISwimable iSwimable : swimObjs) 
		{
			// everybody swim
			iSwimable.swim();
		}
	}

}
