package ce1002.e10.s100201023;

public class Submarine extends Entity implements ISwimable
{
	public Submarine(String name)
	{
		super(name);
	}
	
	public void swim()
	{
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
