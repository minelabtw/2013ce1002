package ce1002.e10.s100201023;

public class Shark extends Fish implements ISwimable
{
	public Shark(String name)
	{
		// TODO Auto-generated constructor stub
		super(name);
	}
	
	public void swim()
	{
		System.out.println("Shark: " + name + " is swimming!");
	}
}
