package ce1002.e10.s102502023;

import java.util.ArrayList;
import java.util.List;

public class E10 {
  public static void main(String[] args) {
	  List<ISwimable> swimObjs = new ArrayList<ISwimable>(); //initialize List<ISwimable> swimObjs
	  swimObjs.add(new Fish("f1")); // initialize Fish to f1
	  swimObjs.add(new Fish("f2")); // initialize Fish to f2
	  swimObjs.add(new Shark("s1")); // initialize Shark to s1
	  swimObjs.add(new Submarine("sub1")); // initialize Submarine sub1
	  swimObjs.add(new Shark("s2")); // initialize Shark s2
	  
	  for (ISwimable iSwimable : swimObjs) { // output the objects
		  iSwimable.swim();
	  }
  }
}
