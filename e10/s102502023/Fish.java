package ce1002.e10.s102502023;

class Fish extends Entity {
	public Fish(String name) { // Constructor
		super(name);
	}
  @Override
  public void swim() {
	  System.out.println("Fish: " + name + " is swimming!");
  }
}
