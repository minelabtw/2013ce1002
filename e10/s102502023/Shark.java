package ce1002.e10.s102502023;

public class Shark extends Fish {
  public Shark(String name) { // Constructor
	  super(name);
  }
  
  @Override
  public void swim() {
	  System.out.println("Shark: " + name + " is swimming!");
  }
}
