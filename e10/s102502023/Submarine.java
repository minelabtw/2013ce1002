package ce1002.e10.s102502023;

class Submarine extends Entity {
  public Submarine(String name) { // Constructor
	  super(name);
  }
  
  @Override
  public void swim() {
	  System.out.println("Submarine: " + name + " is swimming!");
  }
}
