package ce1002.e10.s101303504;

public class Fish extends Entity implements ISwimable{

	public Fish(String name){
		super(name);
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Fish : " + this.name + " is swimming !");
	}
	
}
