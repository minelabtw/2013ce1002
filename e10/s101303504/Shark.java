package ce1002.e10.s101303504;

public class Shark extends Entity implements ISwimable{

	public Shark(String name){
		super(name);
	}

	@Override
	public void swim() {
		// TODO Auto-generated method stub
		System.out.println("Shark : " + this.name + " is swimming !");
	}
	
}