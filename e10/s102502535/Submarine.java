package ce1022.e10.s102502535;

public class Submarine extends Entity implements ISwimable {

	public Submarine(String name) {
		super(name);
	}

	public void swim() {
		System.out.println("Submarine: " + name + " is swimming!");
	} // override swim method

}
