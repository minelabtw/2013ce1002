package ce1022.e10.s102502535;

public class Fish extends Entity implements ISwimable {

	public Fish(String name) {
		super(name);
	}

	public void swim() {
		System.out.println("Fish: " + name + " is swimming!");
	} // override swim method

}
