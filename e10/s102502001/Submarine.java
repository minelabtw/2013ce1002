package ce1002.e10.s102502001;

public class Submarine extends Entity implements ISwimable{

	public Submarine(String name) {
		super(name);
	}

	

	@Override
	public void swim() {
		System.out.println("Submarine: "+ name +" is swimming!");
		
	}
}
