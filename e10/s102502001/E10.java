package ce1002.e10.s102502001;

import java.util.List;
import java.util.ArrayList;

public class E10 {

	public static void main(String[] args) {
		// list contains swimmers
				List<ISwimable> swimObjs = new ArrayList<ISwimable>();    // use arraylist to store each data
				swimObjs.add(new Fish("f1"));
				swimObjs.add(new Fish("f2"));
				swimObjs.add(new Shark("s1"));
				swimObjs.add(new Submarine("sub1"));
				swimObjs.add(new Shark("s2"));
		 
				for (ISwimable iSwimable : swimObjs) {                    // call ISwimable iSwimable and implement every array in it
					// everybody swim
					iSwimable.swim();
				}
			}

	}

