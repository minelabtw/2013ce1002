package ce1002.e10.s102502001;

public class Fish extends Entity implements ISwimable{    //the thing after implement must be abstract

	public Fish(String name) {                            //set name
		super(name);	
	}

	@Override
	public void swim() {
		System.out.println("Fish: "+ name +" is swimming!"); //print out the string
		
	}

	
	
}
