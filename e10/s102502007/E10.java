package ce1002.e10.s102502007;

import java.util.ArrayList;
import java.util.List;

public class E10 {
	E10()
	{
		List<ISwimable> swimObjs = new ArrayList<ISwimable>();
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));
		
		for (ISwimable iSwimable : swimObjs) {
			iSwimable.swim();
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new E10();
	}
}