package ce1002.e10.s102502513;

public abstract class Entity 
{
	protected String name;
	
	public Entity(String name)
	{
		this.name = name; // abstracted class for no "mew"
	}
}
