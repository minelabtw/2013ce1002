package ce1002.e10.s102502513;

import java.util.ArrayList;
import java.util.List;

public class E10 
	{

		public static void main(String[] args) 
		{
			List<ISwimable> swimObjs = new ArrayList<ISwimable>(); // get some array to add
			swimObjs.add(new Fish("f1"));
			swimObjs.add(new Fish("f2"));
			swimObjs.add(new Shark("s1"));
			swimObjs.add(new Submarine("sub1"));
			swimObjs.add(new Shark("s2"));
	 
			for (ISwimable iSwimable : swimObjs)  // like the for loop to enter the object
			{
				// everybody swim
				iSwimable.swim();
			}

		}
	}


