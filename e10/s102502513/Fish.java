package ce1002.e10.s102502513;

public class Fish extends Entity implements ISwimable 
{
	public Fish(String name)
		{
			super(name); // catch this class name
		}
	
	@Override
	public void swim() // override the function in the interface
		{
			System.out.println( "Fish: " + name + " is swimming!");
		}
}

