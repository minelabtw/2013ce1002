package ce1002.e10.s102502505;

import java.util.ArrayList;
import java.util.List;

public class E10 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// list contains swimmers
				List<ISwimable> swimObjs = new ArrayList<ISwimable>();
				swimObjs.add(new Fish("f1"));
				swimObjs.add(new Fish("f2"));
				swimObjs.add(new Shark("s1"));
				swimObjs.add(new Submarine("sub1"));
				swimObjs.add(new Shark("s2"));
		 
				for (ISwimable iSwimable : swimObjs) {//利用迴圈跑一遍所創建的物件
					// everybody swim
					iSwimable.swim();//呼叫各個override掉的swim method
				}
			}
	}


