package ce1002.e10.s102502537;

public class Shark extends Fish{

	public Shark(String name) {
		super(name);
		type = "Shark";
	}

	//override Fish's swim
	public void swim()
	{
		super.swim();
	}
}
