package ce1002.e10.s101201519;

public class Submarine extends Entity implements ISwimable{
	Submarine(String name){
		super(name);
	}
	public void swim(){
		System.out.println("Submarine: "+super.name+" is swimming!");
	}

}
