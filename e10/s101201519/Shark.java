package ce1002.e10.s101201519;

public class Shark extends Entity implements ISwimable{
	Shark(String name){
		super(name);
	}
	public void swim(){
		System.out.println("Shark: "+super.name+" is swimming!");
	}

}
