package ce1002.e10.s101201519;

public class Fish extends Entity implements ISwimable{
	Fish(String name){
		super(name);
	}
	public void swim(){
		System.out.println("Fish: "+super.name+" is swimming");
	}

}
