package ce1002.e10.s101201521;

public class Submarine extends Entity{

	public Submarine(String name) {
		super(name);
	}
	//print Swimming action
	public void swim(){
		System.out.println("Submarine: " + this.name + " is swimming!");
	}
}
