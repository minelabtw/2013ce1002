package ce1002.e10.s101201521;

public class Fish extends Entity{
	public Fish(String name){
		super(name);
	}
	//print Swimming action
	public void swim(){
		System.out.println("Fish: " + this.name + " is swimming!");
	}
}
