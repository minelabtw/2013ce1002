package ce1002.e10.s101201521;

public class Shark extends Fish{
	public Shark(String name) {
		super(name);
	}
	//print Swimming action
	public void swim(){
		System.out.println("Shark: " + this.name + " is swimming!");
	}
}
