package ce1002.e10.s102502527;
import java.util.ArrayList;
import java.util.List;

public class E10 {
	public static void main(String[] args) {
		
		List<ISwimable> water = new ArrayList<ISwimable>();
		water.add(new Fish("f1"));
		water.add(new Fish("f2"));
		water.add(new Shark("s1"));
		water.add(new Submarine("sub1"));
		water.add(new Shark("s2"));

		for (ISwimable iSwimable : water) 
		{
			iSwimable.swim();
		}
	}
}

