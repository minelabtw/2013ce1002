package ce1002.e10.s102502555;

public class Submarine extends Entity implements ISwimable{

	public Submarine(String name) {
		super(name);
	}

	public void swim(){
		System.out.println("Submarine: " + name + " is swimming!");  //how Submarine swim
	}
	
}  //end Submarine
