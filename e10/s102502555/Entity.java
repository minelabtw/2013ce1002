package ce1002.e10.s102502555;

public abstract class Entity implements ISwimable {
	public String name;
	
	public Entity(String name){
		this.name = name;
	}
	
	public abstract void swim();
	
}  //end Entity
