package ce1002.e10.s102502555;

public class Fish extends Entity implements ISwimable{
	
	public Fish(String name) {
		super(name);
	}

	public void swim(){
		System.out.println("Fish: " + name + " is swimming!");  //how fish swim
	}
	
}  //end Fish
