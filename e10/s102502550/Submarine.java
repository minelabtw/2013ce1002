package ce1002.a10.s102502550;

public class Submarine extends Entity implements ISwimable{
	Submarine(String s){									//建構子
		super(s);
	}
	public void swim(){										//實作swim方法
		System.out.println(this.getClass().getSimpleName() + ": " + this.name + " is swimming!");
	}
}
