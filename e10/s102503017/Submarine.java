package ce1002.e10.s102503017;

public class Submarine extends Entity implements ISwimable {

	protected String name;
	
	Submarine()
	{
		this.name = null;
	}
	
	Submarine(String name)
	{
		this.name = name;
	}
	
	public void swim()
	{
		System.out.println("Submarine: " + this.name + " is swimming!");
	}
}
