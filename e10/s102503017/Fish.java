package ce1002.e10.s102503017;

public class Fish extends Entity implements ISwimable{
	
	protected String name;
	
	Fish()
	{
		this.name = null;
	}
	
	Fish(String name)
	{
		this.name = name;
	}
	
	public void swim()
	{
		System.out.println("Fish: " + this.name + " is swimming!");
	}
	
}
