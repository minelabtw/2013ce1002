package ce1002.e10.s102503017;

public class Shark extends Fish implements ISwimable{
	
	protected String name;
	
	Shark()
	{
		this.name = null;
	}
	
	Shark(String name)
	{
		this.name = name;
	}
	
	public void swim()
	{
		System.out.println("Shark: " + this.name + " is swimming!");
	}
	
	
	
	
}
