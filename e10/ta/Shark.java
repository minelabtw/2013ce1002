package ce1002.e10.ta;

public class Shark extends Fish {

	public Shark(String name) {
		super(name);// from Fish(String name)
	}

	@Override
	public void swim() {
		System.out.println("Shark: " + name + " is swimming!");
	}
}
