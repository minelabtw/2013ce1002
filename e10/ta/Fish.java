package ce1002.e10.ta;

public class Fish extends Entity implements ISwimable {

	public Fish(String name) {
		super(name);// from Entity(String name)
	}

	@Override
	public void swim() {
		System.out.println("Fish: " + name + " is swimming!");
	}

}
