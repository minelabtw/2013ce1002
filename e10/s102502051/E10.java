package ce1002.e10.s102502051;
import java.util.ArrayList;
import java.util.List;
public class E10 {
	public static void main(String[] args) {
		// list contains swimmers
		List<ISwimable> swimObjs = new ArrayList<ISwimable>(); //宣告陣列來放三種文件
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));
 
		for (ISwimable iSwimable : swimObjs) {
			// everybody swim
			iSwimable.swim();   //定義了游泳
		}
	}
 
}
