package ce1002.e10.s102502031;

public class Submarine extends Entity implements ISwimable {
	public Submarine(String name) {
		super(name);
	} // Submarine(name)

	@Override
	public void swim() {
		System.out.println("Submarine: " + name + " is swimming!"); // submarine can swim
	} // end override of swim()
} // end class Submarine