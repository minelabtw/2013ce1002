package ce1002.e10.s102502031;

public class Shark extends Entity implements ISwimable {
	public Shark(String name) {
		super(name);
	} // end Shark(name)

	@Override
	public void swim() {
		System.out.println("Shark: " + name + " is swimming!"); // shark can swim
	} // end override of swim()
} // end class Shark