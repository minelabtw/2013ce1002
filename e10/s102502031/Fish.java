package ce1002.e10.s102502031;

public class Fish extends Entity implements ISwimable{
	public Fish(String name) {
		super(name);
	} // end Fish(name)

	@Override
	public void swim() {
		System.out.println("Fish: " + name + " is swimming!"); // fish can swim
	} // end override of swim()
} // end class Fish