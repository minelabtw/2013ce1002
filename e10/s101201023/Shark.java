package ce1002.e10.s101201023;

public class Shark extends Entity implements ISwimable {
	Shark(String n) {
		super(n);
	}

	public void swim() {
		//print
		System.out.println("Shark: " + super.name + " is swimming!");
	}
}
