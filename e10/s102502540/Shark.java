package ce1002.e10.s102502540;

public class Shark extends Entity implements ISwimable

{
	Shark(String a) {
		super(a);
	}

	public void swim() {
		System.out.println("Shark: " + name + " is swimming!");
	}

}