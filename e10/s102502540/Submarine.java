package ce1002.e10.s102502540;

public class Submarine extends Entity implements ISwimable

{
	Submarine(String a) {
		super(a);
	}

	public void swim() {
		System.out.println("Submarine: " + name + " is swimming!");
	}

}