package ce1002.e10.s101201524;

public class Fish extends Entity{
	//set fish's name
	Fish(String name){
		super(name);
	}
	//print the fish is swimming
	public void swim(){
		System.out.println("Fish: " + name + " is swimming!");
	}
}
