package ce1002.e10.s101201524;

public class Shark extends Fish{
	//set shark's name
	Shark(String name){
		super(name);
	}
	// print the shark is swimming
	public void swim(){
		System.out.println("Shark: " + name + " is swimming!");
	}
}
