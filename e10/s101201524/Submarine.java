package ce1002.e10.s101201524;

public class Submarine extends Entity{
	//set submarine's name
	Submarine(String name){
		super(name);
	}
	//print the submarine is swimming
	public void swim() {
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
