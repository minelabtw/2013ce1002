package ce1002.e10.s101201508;

public class Fish extends Entity implements ISwimable {
	Fish(String n) {
		super(n);
	}

	public void swim() {
		//print 
		System.out.println("Fish: " + super.name + " is swimming!");
	}
}
