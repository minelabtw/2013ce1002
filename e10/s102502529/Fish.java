package ce1002.e10.s102502529;

public  class Fish extends Entity implements ISwimable{

	public Fish(String string) {
		super(string);
		
	}

	public void swim() {
			System.out.println("Fish: " + name + " is swimming!");
	}
}
