package ce1002.e10.s102502529;

public class Shark extends Fish implements ISwimable {

	public Shark(String string) {
		super(string);
	}

	public void swim() {
		System.out.println("Shark: " + name + " is swimming!");
	}
}