package ce1002.e10.s102502514;

public abstract class Entity {  //抽象類別，給子類別繼承用
	public String name;
 
	public Entity(String name) {
		this.name = name;
	}
}
