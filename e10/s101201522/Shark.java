package ce1002.e10.s101201522;

public class Shark extends Entity implements ISwimable {

	public Shark(String name) {//construct shark
		super(name);
	}

	@Override
	public void swim() {//way of shark swimming
		System.out.println("Shark: " + name + " is swimming!");
	}

}
