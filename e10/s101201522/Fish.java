package ce1002.e10.s101201522;

public class Fish extends Entity implements ISwimable {

	public Fish(String name) {//construct fish
		super(name);
	}

	@Override
	public void swim() {//way of fish swimming
		System.out.println("Fish: " + name + " is swimming!");
	}
	
}
