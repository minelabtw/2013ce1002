package ce1002.e10.s102502557;

public abstract class Entity   //抽象類別不能被new 只能被繼承
{
	public String name;
	public Entity(String name)
	{
		this.name = name;
	}
}
