package ce1002.e10.s102502034;

import java.awt.List;
import java.util.ArrayList;

public class E10 {

	public static void main(String[] args) {
		// list contains swimmers
		ArrayList<ISwimable> swimObjs = new ArrayList<ISwimable>();
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));

		for (ISwimable iSwimable : swimObjs) {
			// everybody swim
			iSwimable.swim();
		}
		

	}
}
