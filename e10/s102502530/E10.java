package ce1002.e10.s102502530 ;

public class E10
{
   public static void main(String[] args)
   {
      (new Fish("f1")).swim() ;
      (new Fish("f2")).swim() ;
      (new Shark("s1")).swim() ;
      (new Submarine("sub1")).swim() ;
      (new Shark("s2")).swim() ;
   }
}
