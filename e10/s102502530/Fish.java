package ce1002.e10.s102502530 ;

public class Fish extends Entity implements ISwimable
{

   public Fish(String name)
   {
      super(name) ;
   }

   @Override
   public void swim()
   {
      System.out.println("Fish: " + name + " is swimming!") ;
   }
}

