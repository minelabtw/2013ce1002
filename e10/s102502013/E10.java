package ce1002.e10.s102502013;
import java.util.ArrayList;
import java.util.List;
public class E10 {

	public static void main(String[] args) {
		List<ISwimable> swimObjs = new ArrayList<ISwimable>();//create a list
		swimObjs.add(new Fish("f1"));
		swimObjs.add(new Fish("f2"));
		swimObjs.add(new Shark("s1"));
		swimObjs.add(new Submarine("sub1"));
		swimObjs.add(new Shark("s2"));
 
		for (ISwimable iSwimable : swimObjs) {//對列表中的每一個swimObjs執行swim
			// everybody swim
			iSwimable.swim();
		}
	}

}
