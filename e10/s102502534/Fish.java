package ce1002.e10.s102502534;

public class Fish extends Entity{

	public Fish(String name) {
		super(name);
	}

	@Override
	public void swim() {
		System.out.println("Fish: "+name+" is swimming!");
	}

}
