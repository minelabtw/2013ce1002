package ce1002.e10.s102502534;

public class Submarine extends Entity{

	public Submarine(String name) {
		super(name);
	}

	@Override
	public void swim() {

		System.out.println("Submarine: "+name+" is swimming!");
	}

}
