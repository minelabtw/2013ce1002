package ce1002.e10.s102502016;

public class Fish extends Entity implements ISwimable{
	public Fish(String name) {
		super(name);//進行父類別的建構式(存入name)
	}

	public void swim() {
		System.out.println("Fish: " + name + " is swimming!");
		
	}
}
