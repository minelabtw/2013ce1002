package ce1002.e10.s102502544;

public class Submarine extends Entity{
	Submarine(String name){
		super(name);
	}
	public void swim(){
		System.out.println("Submarine: "+name+" is swimming!");
	}
}
