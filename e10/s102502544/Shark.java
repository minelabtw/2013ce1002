package ce1002.e10.s102502544;

public class Shark extends Entity{
	Shark(String name){
		super(name);
	}
	public void swim(){
		System.out.println("Shark: "+name+" is swimming!");
	}
}
