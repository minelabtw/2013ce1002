package ce1002.e10.s102502006;

public class Submarine extends Entity implements ISwimable {

	public Submarine(String string) {
		super(string);
	}

	public void swim() {
		System.out.println("Submarine: " + name + " is swimming!");
	}
}
