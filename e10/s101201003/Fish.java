package ce1002.e10.s101201003;

public class Fish extends Entity implements ISwimable{
	public Fish(String n){
		super(n);
	}

	public void swim(){		
		System.out.println("Fish: "+super.name+" is swimming!");
	}

}
