package ce1002.e10.s101201003;

public class Submarine extends Entity implements ISwimable{
	public Submarine(String n){
		super(n);
	}

	public void swim(){	
		System.out.println("Submarine: "+super.name+" is swimming!");
	}
}
