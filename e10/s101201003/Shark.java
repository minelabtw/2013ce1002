package ce1002.e10.s101201003;

public class Shark extends Entity implements ISwimable{
	public Shark(String n){
		super(n);
	}

	public void swim(){		
		System.out.println("Shark: "+super.name+" is swimming!");
	}

}
