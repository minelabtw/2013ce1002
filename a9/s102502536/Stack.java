package ce1002.a9.s102502536;

import java.util.ArrayList;

public class Stack {
	
	private int size;  // declare variable
	private String name;
	private int counter = 0;
	
	private ArrayList<Object> list = new ArrayList<Object>();  // declare arraylist
	
	Stack(int size , String name) {  // constructor
		this.size = size;
		this.name = name;
	}
	
	public int getSize() {  // return stack size
		return list.size();
	}
	
	public Object peek() {  // return the top object of the stack
		return list.get(getSize()-1);
	}
	
	public void push(Object o) throws Exception {  // push the object
		if (counter == size)
		    throw new Exception(name + " is full\nAn Exception has been caught");  // throw exception while the stack is full
		list.add(o);
		counter++;
		System.out.println(name + " push " + o);
	}
	
	public void pop() throws Exception {  // pop the object
		if (counter == 0)
			throw new Exception(name + " is empty\nAn Exception has been caught");  // throw exception while the stack is empty
		Object o = list.get(getSize()-1);
		System.out.println(name + " pop " + peek());
		list.remove(o);
		counter--;    
	}

}

class Node  // not used = =
{
    Object data;
    Node next;
    public Node(Object data)
    {
    	this.data = data;
    	this.next = null;
    }
}
