package ce1002.a9.s102502017;

public class Stack {
	
	String name;
	int max_size;
	int size;
	Node head;
	
	Stack(int size , String name){
		this.name = name;
		this.max_size = size;
		this.size = 0;
	}

	void push(Object data) throws Exception{
		try{
			if(!isFull()){
				Node node = new Node();
                node.data = data;
                node.next = head;
                head = node;
                System.out.println(name + " push " + data);
                size++;
				}
			else throw new Exception(name + " is full");
			}
			catch(Exception e){
				throw e;
			}
		}
	// 1111 is integer
	void push(int dataInt) throws Exception{
		try{
			if(!isFull()){
				Object data = String.valueOf(dataInt);
				Node node = new Node();
                node.data = data;
                node.next = head;
                head = node;
                System.out.println(name + " push " + data);
                size++;
				}
			else throw new Exception(name + " is full");
			}
			catch(Exception e){
				throw e;
			}
		}
	void pop() throws Exception{
		try{
			if(!isEmpty()){
				size--;
				Node node = head;
				head = node.next;
				System.out.println(name + " pop " + node.data);
			}
			else throw new Exception(name + "is empty");
		} catch (Exception e){
			throw e;
		}
	}
	
	boolean isFull() throws Exception{
		if(size == max_size)return true;
		else return false;
	}
	
	boolean isEmpty(){
		if(size==0)return true;
		else return false;
	}
}

class Node{
	
	Object data;
	Node next;
	
}
