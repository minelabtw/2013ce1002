package ce1002.a9.s995002014;

public class Stack {
	private Node[] mystack;
	private String name;
	private int pointer=0;
	int size;
	
	//建構子，用一個迴圈初始化Node陣列(如果不用就會錯)
	Stack(int size , String name) {
		this.mystack=new Node[size];
		this.size=size;
		this.name=name;
		for(int i=0;i<size;i++){
			mystack[i] = new Node();
		}
	}
	
	void push(Object obj) throws Exception {
		if(pointer>=size)throw new Exception(name+" is full \nAn Exception has been caught"); //設定例外訊息
		System.out.println(name+" push "+obj);
		mystack[pointer].setData(obj);
		pointer++;
	}
	Object pop() throws Exception {
		if(pointer==0)throw new Exception(name+" is Empty \nAn Exception has been caught"); //設定例外訊息
		pointer--;
		Node temp=mystack[pointer];
		System.out.println(name+" pop "+temp.getData());
		return temp;
	}
	
}

//Node class, use setter and getter
class Node {
    Object data;
    Node next;
    
    void setData(Object obj) {
    	data=obj;
    }
    Object getData() {
    	return data;
    }
}