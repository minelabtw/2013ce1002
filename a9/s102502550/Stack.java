package ce1002.a9.s102502550;

public class Stack {                                 //堆疊class
	private String name;
	private int size;
	private int count;								 //計算是否超過size
	private Node top;
	
	
	
	Stack(int size , String name){
		this.name = name;
		this.size = size;
		top = null;
		count = 0;
		
	}
	public void push(Object obj)throws Exception{                 //push方法
		
		if(count < size){
			Node newNode = new Node(); 
	        newNode.data = obj; 
	        newNode.next = top; 
	        top = newNode;
	        System.out.println(name +" push "+ obj);
	        count++;
		}
		else{
			System.out.println(name +" is full");
			throw new Exception("An Exception has been caught"); 
		}
	}
	public Object pop()throws Exception{						   //pop方法
		
	   Node tmpNode; 
	   tmpNode = top; 
	   
       if(count == 0) { 
    	   System.out.println(name +" is empty");
    	   throw new Exception("An Exception has been caught");  
       } 

       top = top.next; 
       count--; 
       System.out.println(name +" pop "+ tmpNode.data);
       return tmpNode.data;
		
	}
}
