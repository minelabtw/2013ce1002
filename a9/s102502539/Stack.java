package ce1002.a9.s102502539;

public class Stack 
{
	int i = 0;
	int size = 0;
	String name;
	Node node = new Node();

	Stack(int size, String name) 
	{
		this.size = size;
		this.name = name;

	}

	public void push(Object obj) 
	{
		try 
		{
			//先判斷，再執行
			if (i == size) 
			{
				throw new Exception(" is full");
			}
			System.out.println(name + " push " + obj);
			Node nextnode = new Node();
			nextnode.next = node;
			node = nextnode;
			node.data = obj;
		} 
		catch (Exception ex) 
		{
			System.out.println(name + ex.getMessage());
		}
		i++;

	}

	public void pop() 
	{
		try 
		{
			//先判斷，再執行
			if (i == 0) 
			{
				throw new Exception("An Exception has been caught");
			}
			System.out.println(name + " pop " + node.data);
			Object obj = node.data;
			node = node.next;
		} 
		catch (Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		i--;
	}

	public String getName() 
	{
		return name;
	}

}

class Node 
{
	Object data;	// 內容
	Node next;	//串列
}
