package ce1002.a9.s102502028;

public class Stack {
	
	int size = 0 ; //目前stack的大小
	int maxSize ;
	String name ;
	Node head ; //頭
	
	Stack(int size , String name)
	{
		this.name = name ;
		this.maxSize = size ;
	}
	
	public void push(Object obj) throws Exception  //塞入物件到stack裡面
	{		
		size++ ;
		if (size > maxSize)
		{
			System.out.println(name+" is full") ;
			throw new Exception("An Exception has been caught") ;
		}		
		Node node = new Node() ;
		node.data = obj ;
		node.next = head ;
		head = node ;
		System.out.println(name+" push "+obj) ;
	}
	
	public void pop() throws Exception  //丟出一個物件到外面
	{	
		if (size < 1)		
			throw new Exception("An Exception has been caught") ;
		size-- ;
		Node node = head;
		head = node.next;
		System.out.println(name + " pop " + node.data);
	}	
	
}

class Node //節點包含data和next
{
    	Object data;
    	Node next;	
}
	
