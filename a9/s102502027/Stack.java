package ce1002.a9.s102502027;
public class Stack extends Node{
	public int top=0;
	public int size;
	public String name;
	Node n; //創新節點
	Stack(int size , String name) {
		this.size=size;
		this.name=name;
	}
	public void pop() throws Exception{ //執行和判斷POP是否有例外
		if(top == 0){
			throw new Exception("An Exception has been caught");
		}
		System.out.println(name+" pop "+n.data);
		n = n.next;
		top -= 1;
	}
	public void push(Object data)throws Exception{ //執行和判斷PUSH是否有例外 
		if(top == size){
			System.out.println(name+" is full");
			throw new Exception("An Exception has been caught");
		}
		else{
			Node in = new Node();
			in.data = data;
			in.next = n;
			n = in;
			System.out.println(name+" push "+data);
			top += 1;
		}
		
	}
}