package ce1002.a9.s102502542;

public class Stack 
{
	public String stack;
	public int size=0;
	public int top;
	Node pointer;
	Stack(int size , String name)
	{
		stack=name;
		top=size;
	}
	public void push(Object obj) throws Exception
	{
		if(top==size)//當push新的物件進Stack時超過Stack的size時丟出例外
		{
			throw new Exception(stack +" is full");
		}
		size++;
		Node a = new Node(obj);//創建暫存node
		a.next = pointer;
		pointer = a;
		System.out.println(stack + " push " + obj);
	}
	public void pop() throws Exception
	{
		if ( size == 0 ) //當Stack在pop時沒有任何資料在Stack中時丟出例外
		{
			throw new Exception(stack+" is empty");
		}
		size--;
		Node a = pointer; 
		pointer = pointer.next; 
		System.out.println(stack+" pop "+a.data);
		a = null;
	}
	class Node //利用Node的資料結構做為Stack存放物件的單位
	{
	    Object data;
	    Node next;
	    Node (Object obj)
	    {
	    	data = obj;
	    }
	}
}
