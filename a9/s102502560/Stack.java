package ce1002.a9.s102502560;

public class Stack {
	
	int size;
	String name;
	
	Node headNode=null;
	int length=0;
	
	public Stack(int size, String name) {
		this.size=size;
		this.name=name;
	}
	
	public void push(Object obj) throws Exception {
		if(length>=size)throw new Exception(name+" is full");
		headNode=new Node(obj,headNode);
		System.out.println(name+" push "+obj);
		length++;
	}
	
	public Object pop() throws Exception {
		if(length<=0)throw new Exception(name+" is empty");
		Object obj=headNode.data;
		headNode=headNode.next;
		System.out.println(name+" pop "+obj);
		length--;
		return obj;
	}
}
