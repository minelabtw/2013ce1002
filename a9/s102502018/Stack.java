package ce1002.a9.s102502018;

public class Stack {

	private String name;
	protected Object num[];		//物件陣列
	protected int a=0;
	public Stack(int size , String name)
	{
		this.name = name;
		this.num = new Object [size];
	}
	public void push(Object obj)
	{	
		try
		{			
			if(a<num.length)
			{
				num[a] = obj;		//將物件存入陣列
				a++;
				System.out.println(name+" push "+obj);
				
			}
			else
			{
				throw new Exception("An Exception has been caught");
			}
			
		}
		catch(Exception e)
		{
			System.out.println(name+" is full");
			System.out.println(e.getMessage());
		}
		
		
	}
	public void pop()
	{
		try
		{
			a--;
			if(a>=0)
			{							//a-1後位置會是目前陣列頂端之物件		
				System.out.println(name+" pop "+num[a]);
				num[a]=null;	//取出後將該位置資料清空
			}
			//else
			//	throw new Exception("An Exception has been caught");
		}
		catch(Exception e)
		{
			
			//System.out.println(name+" is empty");
			//System.out.println(e.getMessage());
		}
	}
}
