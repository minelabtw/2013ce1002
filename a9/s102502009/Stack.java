package ce1002.a9.s102502009;

public class Stack {
	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	private int size;
	private int now = 0;// Stack目前數量
	private String name;
	public Node top; // 最頂端

	public void push(Object obj) {
		try {
			if (now != size) {// 沒超過
				Node node = new Node();
				node.data = obj;// 存資料
				add(node);
				System.out.println(name + " push " + obj);
			} else {
				System.out.println(name + " is full");
				throw new Exception("An Exception has been caught");// 丟出例外
			}
		} catch (Exception ex) {// 捕捉例外
			System.out.println(ex.getMessage());
			System.exit(0);
		}
	}

	public void add(Node node) {
		now = now + 1;
		node.next = top;
		top = node;
	}

	public void pop() {
		try {// 不等於0就提出
			if (now != 0) {
				now = now - 1;
				Node a = top;
				top = top.next;
				System.out.println(name + " pop " + a.data);
			} else {
				System.out.println(name + " is emtpy");
				throw new Exception("An Exception has been caught");// 丟出例外
			}
		} catch (Exception ex) {// 捕捉例外
			System.out.println(ex.getMessage());
			System.exit(0);// 終止程式
		}
	}

}