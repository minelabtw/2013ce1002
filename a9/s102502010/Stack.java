package ce1002.a9.s102502010;

public class Stack {
	
	class Node 
	{
	    Object data;
	    Node next;
	    Node(Object data){  //linked
	    	this.data=data;
	    	this.next=null;
	    }
	}
	
	private Node head;  //node
	private int size;
	private String name;
	
	Stack(int size,String name)
	{
		this.size=size;
		this.name=name;
		head=new Node(null);
	}
	
	public Object pop() throws Exception  //pop
	{
		int count = 0;
		Node cur = head;
		Node pre = null;
		while(cur.next!=null)
		{
			pre=cur;
			cur=cur.next;
			count++;
		}
		if(count==0)
		{
			throw new Exception("An Exception has been caught");
		}
		else
		{
			System.out.println(name + " pop " + cur.data);
			pre.next = null;
			return null;
		}
		
	}
	
	public void push(Object thing) throws Exception  //push
	{
		int count = 0;
		Node cur=head;
		while(cur.next!=null)
		{
			cur=cur.next;
			count++;
		}
		if(count==size)
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		else
		{
			System.out.println(name + " push " + thing);
			cur.next=new Node(thing);
		}
	}
}
