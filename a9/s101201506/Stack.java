package ce1002.a9.s101201506;

public class Stack {
	private Node top;
	private String name;
	private int size;
	private int count = 0;

	Stack() {
		top = null;
	}

	// 建構式
	Stack(int _size, String _name) {
		top = null;
		size = _size;
		name = _name;
	}

	public void push(Object in) {
		if (count == size) {
			System.out.println(name + " is full");
		} else {
			Node node = new Node();
			node.setData(in);
			node.setNext(top);
			top = node;
			count++;
			System.out.println(name + " push " + top.getData());
		}
	}

	public void pop() {
		// temp node
		Node temp_node = top;

		// 當出現 Exception 出現時輸出"An Exception has been caught"且程式就結束不再執行後面的步驟。
		if (count == 0) {
			System.out.println("An Exception has been caught");
		} else {
			top = temp_node.getNext();
			count--;
			System.out.println(name + " pop " + temp_node.getData());
		}
	}

}
