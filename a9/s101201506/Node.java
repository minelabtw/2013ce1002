package ce1002.a9.s101201506;

public class Node {
	// 利用Node的資料結構做為Stack存放物件的單位。

	private Object data;
	private Node next;

	public void setData(Object d) {
		data = d;
	}

	public Object getData() {
		return data;
	}

	public void setNext(Node n) {
		next = n;
	}

	public Node getNext() {
		return next;
	}
}
