package ce1002.a9.s101201508;

public class Stack {
	private int size;
	private String name;
	private Node top;
	private int count=0;
	Stack()
	{
		top=null;
	}
	Stack(int s,String n)
	{//input the size and name
		top=null;
		size=s;
		name=n;
	}
	public void push(Object in)
	{
		if (count<size)
		{//the general situation
			Node new_node=new Node();
			new_node.set_data(in);
			new_node.set_next(top);
			top=new_node;
			System.out.println(name+" push "+top.get_data());
			count++;
		}
		else
		{//this is full
			System.out.println(name+" is full");
		}
	}
	public void pop()throws Exception
	{
		if (count>0)
		{//the general situation
			Node temp_node=top;
			top=temp_node.get_next();
			count--;
			System.out.println(name+" pop "+temp_node.get_data());
		}
		else
		{//exception
			throw new Exception("An Exception has been caught");
		}
	}
}
