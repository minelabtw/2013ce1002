package ce1002.a9.s101201508;

public class A9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//use the Stack to store
		Stack s1=new Stack(5,"Stack1");
		Stack s2=new Stack(2,"Stack2");
		//input the data
		try
		{
			s1.push("ABC");
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		}
		catch (Exception e)
		{//do this when there is an Exception
			System.out.println(e.getMessage());
		}
		
	}

}
