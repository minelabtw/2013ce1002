package ce1002.a9.s101201508;

public class Node {
	private Object data;
	private Node next;
	Node()
	{
		
	}
	public void set_data(Object d)//set the data
	{
		data=d;
	}
	public Object get_data()//get the data
	{
		return data;
	}
	public void set_next(Node n)//set the last node
	{
		next=n;
	}
	public Node get_next()//get the last node
	{
		return next;
	}

}
