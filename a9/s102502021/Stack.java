package ce1002.a9.s102502021;

import ce1002.a9.s102502021.Node;

public class Stack {
	Stack(int size, String name) {
		_size = size;
		_name = name;
	}

	private int _size;
	private int now = 0;
	private String _name;
	public Node top;

	public void push(Object obj) {
		try {
			if (now != _size) {
				Node node = new Node();
				node.data = obj;// 存資料
				add(node);
				System.out.println(_name + " push " + obj);
			} else {
				System.out.println(_name + " is full");
				throw new Exception();
			}
		} catch (Exception ex) {
			System.out.println("An Exception has been caught");
			System.exit(0);
		}
	}

	public void add(Node node) {
		now = now + 1;
		node.next = top;// 新的TOP.next變成舊的TOP
		top = node;// node變新的TOP
	}

	public void pop() {
		try {
			Node a = top;// 暫存node
			top = top.next;// TOP往前移
			System.out.println(_name + " pop " + a.data);
		} catch (Exception ex) {// 捕捉例外
			System.out.println(_name + " is emtpy");
			System.out.println("An Exception has been caught");
			System.exit(0);// 終止程式
		}
	}
}
