package ce1002.a9.s102502024;
class Node{
	Object data;
    Node next;
}
public class Stack {
	
     int counter;
	 int siz;
	 String nam;
	 Node[] arr;  //宣告node陣列
	
	public Stack(int size,String name) 
	{
		counter=0;  //計算stack堆到哪
		nam=name;
		siz=size;
	    arr=new Node[size];
		for(int i=0;i<size;i++)
		{
			arr[i]=new Node();
		}
	}
	
	public void push(Object obj) throws Exception
	{
		if(counter!=siz)
		{   
			System.out.println(nam+" push "+obj);
			arr[counter].data=obj;  //存入資料
			counter++;
		}
		else
		{
			throw new Exception(nam+" is full");  //如果有例外
		}
	}	
	public void pop() throws Exception
	{
		if(counter!=0)
		{
			counter--;
			System.out.println(nam+" pop "+arr[counter].data);  //讀出資料
			arr[counter].data=null;  //丟掉資料
		}
		else
		{
			throw new Exception(nam+" is empty");
		}
	}
}