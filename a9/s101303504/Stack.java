package ce1002.a9.s101303504;
public class Stack {

	private int size;
	private String name;
	private Node top;
	int i=0;
	Stack(int size , String name){
		this.size = size;
		this.name = name;
		top = null;
	}
	
	// 插入資料至頂端 
    public void push(Object data) 
    { 
    	i = i+1;
        if(i> this.size)
        {
        	System.out.print(this.name +" is empty.");
        	System.out.println("An Exception has been caught");
        }
        else
        {
        	Node newNode = new Node();
            newNode.setData(data); 
            newNode.setNext(top); 
            top = newNode;
            System.out.println(this.name + " push " + top.getData());
        }
    } 
 

    // 刪除頂端資料 
    public void pop() { 
    	Node tmpNode;
        tmpNode = top; 
        if(tmpNode == null)
        {
        	System.out.print("An Exception has been caught");
        	return;
        }
        System.out.println(this.name + " pop " + top.getData());
        top = top.getNext();
        tmpNode = null;
    } 
 
}

class Node {
	
	public Object data;
	public Node next;

    public void setData(Object data) {  // 節點資料 
        this.data = data; 
    } 

    public void setNext(Node next) {  // 下一個節點位置 
        this.next = next; 
    } 

    public Object getData() {  // 傳回節點資料 
        return data; 
    } 

    public Node getNext() {  // 傳回下一個節點位置 
        return next; 
    } 
	
}
