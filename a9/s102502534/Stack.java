package ce1002.a9.s102502534;

public class Stack {

	class Node {
		Object data;
		Node next;

		// Node��constructor
		Node(Object data) {
			// set Node
			this.data = data;
			this.next = null;
		}
	}

	private Node head = null;
	private int size = 0;
	private String name;

	// Stack��constructor
	Stack(int size, String name) {
		this.size = size;
		this.name = name;
		head = new Node(null);
	}

	// push function
	public void push(Object thing) throws Exception {

		int s = 0;
		Node cur = head;
		while (cur.next != null) { // set cur
			cur = cur.next;
			s++;
		}

		if (s == size) {
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");

		} else {
			System.out.println(this.name + " push " + thing);
			Node newnode = new Node(thing); // use newnode let cur.next letter it
			cur.next = newnode;
		}
	}

	// pop function
	public Object pop() throws Exception {
		int s = 0;
		Node cur = head;
		Node pre = null;

		while (cur.next != null) { // turn a round
			pre = cur;
			cur = cur.next;
			s++;
		}

		if (s == 0)
			throw new Exception("An Exception has been caught");
		else {
			System.out.println(name + " pop " + cur.data);
			pre.next = null;
			return null;
		}
	}

}
