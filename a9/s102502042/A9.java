package ce1002.a9.s102502042;

public class A9 {

	public static void main(String[] args) {
		//create two stacks
		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");
		//try push and pop
		try{
			s1.push("ABC");
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		}
		catch(Exception ex)	//Exception condition
		{
			System.out.println(ex.getMessage());
			System.out.println("An Exception has been caught");
		}
	}

}
