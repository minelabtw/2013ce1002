package ce1002.a9.s102502042;

public class Stack {
	public int size;
	public int msize;	//max size
	public String name;
	public Node Top;	//top element
	
	//constructor
	Stack(int msize,String name)
	{
		this.msize = msize;
		this.name = name;
	}
	
	//push function
	void push(Object data) throws Exception
	{
		try{
			if(size+1<=msize)
			{
				Node node = new Node();
				node.data = data;
				node.next = Top;
				Top = node;
				size++;
				System.out.println(name + " push " + node.data);
			}
			else
			{
				throw new Exception(name + " is full");
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
	
	//pop functiion
	void pop() throws Exception
	{
		try{
			if(size!=0)
			{
				size--;
				System.out.println(name + " pop " +Top.data);
				Top = Top.next; 
			}
			else
			{
				throw new Exception(name + " is empty");
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
}
