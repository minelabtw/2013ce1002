package ce1002.a9.s102502521;

public class A9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//兩個Stack
		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");
		
		//Try & Catch
		try{		
			s1.push("ABC");
			System.out.println("Stack1 push ABC");
			
			s1.push("DEF");
			System.out.println("Stack1 push DEF");
			
			s1.push(1111);
			System.out.println("Stack1 push 1111");
			
			s2.push("123");
			System.out.println("Stack2 push 123");
			
			s2.push("456");
			System.out.println("Stack2 push 456");
			
			s1.pop();
			s1.pop();
			s1.pop();
				
			s2.push("789");
			System.out.println("Stack2 push 789");
			
			s1.pop();
		}catch(Exception e){
			System.out.println("An Exception has been caught");
		}
	}

}
