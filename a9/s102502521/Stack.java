package ce1002.a9.s102502521;

public class Stack {
	private int size;
	private int count=0;
	private String name;
	protected Node top=null;

	//Stack的屬性資料
	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	//Push
	public void push(Object obj) throws Exception{
		if(count<size){
			Node node = new Node();
			node.data = obj;
			node.next = top;
			top = node;
			count++;
		}
		else{
			System.out.println(name+" is full");
			throw new Exception();
		}
	}

	//Pop
	public void pop() throws Exception{
		Node tmpNode; 
		tmpNode = top;
		top = top.next;	 
		
		if(count>=0){	       		       
			System.out.println(name+ " pop "+tmpNode.data);
			count--;
		}
		
		tmpNode = null;
	}
}
