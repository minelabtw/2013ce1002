package ce1002.a9.s102502030;

public class Stack {

	protected Object top;
	protected Object[] data;
	protected int counter;
	protected int size;
	protected String name;
	
	public Stack( int size , String name ) {
		this.size = size;
		this.name = name;
		data = new Object[size];
		counter = 0;
	}
	
	public void push( Object obj )  throws Exception {
		//陣列已滿時傳出exception
		if( counter==size ) {
			throw new Exception( name + " is full" );
		}
		//正常時存入資料 計數器後移
		else {
			data[counter]=obj;
			counter++;
		}
	}
	public void pop() throws Exception {
		//陣列空時傳出exception
		if( data==null ){
			throw new Exception( name + " is null" );
		}
		//正常時清除資料 計數器前移
		else {
			data[counter]=null;
			counter--;
		}
	}
}