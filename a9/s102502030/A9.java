package ce1002.a9.s102502030;

public class A9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//開兩個推疊
		Stack s1 = new Stack( 5 , "Stack1" );
		Stack s2 = new Stack( 2 , "Stack2" );
		
		//丟入測試資料
		try {
			s1.push("ABC");
			System.out.println( "Stack1 push ABC" );			
			s1.push("DEF");
			System.out.println( "Stack1 push DEF" );			
			s1.push(1111);
			System.out.println( "Stack1 push 1111" );
			s2.push("123");
			System.out.println( "Stack2 push 123" );
			s2.push("456");
			System.out.println( "Stack2 push 456" );
			s1.pop();
			System.out.println( "Stack1 pop 1111" );
			s1.pop();
			System.out.println( "Stack1 pop DEF" );
			s1.pop();
			System.out.println( "Stack1 pop ABC" );
			s2.push("789");
			System.out.println( "Stack2 push 789" );
			s1.pop();
			System.out.println( "Stack1 pop" );	
		} catch( Exception e ) {
			//取得錯誤訊息並結束程式
			System.out.println( e.getMessage() );
			System.out.println( "An Exception has been caught" );
			System.exit(0);
		}
	}

}
