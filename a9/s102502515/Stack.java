package ce1002.a9.s102502515;

public class Stack {

	private int size;
	private String name;
	private Node head; // give a top name
	private int number = 0; // for switch the size number
	
	class Node 
	{
		Object data; // any object in code called 'data'
		Node next; // record the reference of the next node
	}
	
	Stack(int size, String name)
	{
		this.size = size;
		this.name = name;
	}
	
	public void push(Object obj) // push the object into the stack
	{
		Node node = new Node(); // new a node
		if (number == size) // identify the size
		{
			System.out.println(name + " is full");
		}
		else
		{
			node.next = head; // the next point to the top (in)
			node.data = obj; // bring the data into node
			number++; // next node
			head = node; // node for top 
			System.out.println(name + " push " + node.data);
		}
	}
	
	public void pop() 
	{
		
		if(head == null)
		{
			System.out.println("An Exception has been caught");
		}
		else 
		{		
			Object node = head.data;
			System.out.println(name + " pop " + node);
			head = head.next; // take the top out of the stack
			number--; // the lower one

		}
	}
	
}
