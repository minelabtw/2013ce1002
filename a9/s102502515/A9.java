package ce1002.a9.s102502515;

public class A9 {
	public static void main(String[] args) {

			Stack s1 = new Stack(5, "Stack1");
			Stack s2 = new Stack(2, "Stack2"); // create new Stacks
		
				s1.push("ABC");//push the object to the stack
				s1.push("DEF");
				s1.push(1111);
				s2.push("123");
				s2.push("456");
				s1.pop(); //pop the object out 
				s1.pop();
				s1.pop();
				s2.push("789");
				s1.pop();//test the full situation
		}
}
