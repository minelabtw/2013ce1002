package ce1002.a9.s102502557;

public class Stack 
{
	private int nowsize;
	private int Max_size;
	private String sname;
	private Node[] node;
	int counter;
	
	Stack(int size , String name)
	{
		Max_size = size;
		nowsize=0;//目前size預設0
		node = new Node[Max_size];
		sname= name;//把資料寸進Stack
		for(int i=0; i<Max_size ;i++)
		{
			node[i] = new Node();//設定n個 Node類別的陣列 //////bgggggggu
		}
	}
	
	void push(Object obj) throws Exception
	{
		try
		{
			if( counter != Max_size )
			{
				node[counter].data = obj;//把資料先存進data
				System.out.println( sname + " push " + node[counter].data );
				counter++;
			}
			else 
				throw new Exception(sname + " is full");
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	void pop()throws Exception 
	{
		try
		{
			if(counter >=0)
			{
				counter--;  //因為最後在push會加一 會是空的
				System.out.println( sname + " pop  " + node[counter].data );
			}
			else
				throw new Exception(sname + " is empty ");
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	class Node 
	{
	    Object data;
	    Node next;
	}
	
}
