package ce1002.a9.s992001026;

public class Stack {

	public int size;
	public int i = -1;
	public String name;
	public Node nodes[];

	// set stack 
	public Stack(int n, String str) {
		this.size = n;
		this.name = str;

		this.nodes = new Node[n];
		
		for (int i = 0; i < n; ++i) {
			nodes[i] = new Node();
		}

	}
	// set push
	public void push(Object obj) throws Exception {
		if (this.i == size - 1)
			throw new Exception( name + " is full" );
		else {
			this.i = i + 1;
			nodes[i].data = obj;

			if (i != size - 1) {
				nodes[i].next = nodes[i + 1];
			} else {
				nodes[i].next = null;
			}

			System.out.println(name + " push " + obj);
		}

	}
	// set pop
	public void pop() throws Exception {
		if (i == -1)
			throw new Exception( name + " is empty" );
		else {

			System.out.println(name + " pop " + nodes[i].data);
			nodes[i].data = null;
			i = i - 1;

		}

	}

}
