package ce1002.a9.s102502512;

public class Stack{											//Line 3-7: create several variable
	private Node top; 
	private String lab;
	private int conta;
	private int contr;
class Node													//Line 8-12: create a node class with data object and node itself
{
	 Object data;
	 Node next;
}

	
	Stack(int size , String name)							//Line 15-19: get the stack's size and name
	{
		conta=size;
		name=lab;
	}
	 public void push(Object data) throws Exception			//Line 20-42: create a method to push data to the stack
	 {
		Node nod = new Node(); 								//Line 22: to new a nod object
		try													//Line 23 and 38: use the exception structure to avoid pushing to much data to the stack
		{
			if(contr == conta)
			{
				throw new Exception(lab+" is full\nAn Exception has been caught");
			}
			else
			{
				nod.next = top;
				nod.data = data;
				contr++;
				top = nod;
				System.out.println(lab+" push " +nod.data);
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	 } 
	 public void pop() throws Exception
	 {
		 try
		 {
		 if(top == null)
			{
				throw new Exception(" An Exception has been caught");
			}
			else
			{
				Object nod = top.data;
				System.out.println(lab + " pop " + nod);
				top = top.next; 
				contr--;
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			System.exit(1);
		}
	 }

}
