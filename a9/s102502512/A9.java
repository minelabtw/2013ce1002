package ce1002.a9.s102502512;

public class A9 {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		Stack s1 = new Stack(5 , "Stack1");					//Line 7-8: create two stack s1 and s2
		Stack s2 = new Stack(2 , "Stack2");
		
		s1.push("ABC");										//Line 10-19: pushing or popping with s1 and s2
		s1.push("DEF");
		s1.push(1111);
		s2.push("123");
		s2.push("456");
		s1.pop();
		s1.pop();
		s1.pop();
		s2.push("789");
		s1.pop();
		
	}

}
