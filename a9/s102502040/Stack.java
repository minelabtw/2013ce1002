package ce1002.a9.s102502040;
class Node {
    Object data;
    Node next;
}
public class Stack {
    int size = 0;
    int Max_size;
    String name;
    Node head;
    Stack(int size, String name) {
        this.Max_size = size;
        this.name = name;
    }
    int size() {
        return size;//取Stack最大值
    }
    String name() {
        return name;//取Stack名稱
    }

    //設定放進去的函式,與例外條件
    void push(Object data) throws Exception{
        try {
            if(!full()) {
                size++;
                Node node = new Node();
                node.data = data;
                node.next = head;
                head = node;
                System.out.println(name + " push " + data);
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    //拿出來的函式與例外
    void pop() throws Exception{
        try {
            if(!empty()) {
                size--;
                Node node = head;
                head = node.next;
                System.out.println(name + " pop " + node.data);
            }
        } catch (Exception e) {
            throw e;
        }
    }
    
    //判斷pop與push是否例外
    boolean full() throws Exception{
        if(size == Max_size) {
            throw new Exception(name + " is full");
        }
        return false;
    }
    boolean empty() throws Exception{
        if(size == 0) {
            throw new Exception(name + " is empty");
        } 
        return false;
    }
}
