package ce1002.a9.s102502038;

public class A9 {
	private static Stack s1 = new Stack(5 , "Stack1"); //init
	private static Stack s2 = new Stack(2 , "Stack2");
	public static void main(String[] args){
		try {
			s1.push("ABC");
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		} catch (Exception e) { //catch exception for once
			System.out.println(e.getMessage());
			System.out.println("An Exception has been caught");
		}
	}
}
class Stack{//inner class Stack
	private int size;
	private int layer;
	private String name;
	private Node gate;
	Stack(int size,String name){
		this.size = size;
		this.name = name;
		gate = null;
		layer = 0;
	}
	public void push(Object data) throws Exception{
		if(layer >= size)
			throw new Exception(name + " is full");
		gate = new Node(data,gate);
		layer++;
		System.out.println(name + " push " + data.toString());
	}
	public void pop() throws Exception{
		if(layer <= 0)
			throw new Exception( name + "is empty");
		System.out.println(name + " pop " + gate.data.toString());
		gate = gate.next;
	}
}
class Node{//inner class Node
	public Object data;
	public Node next;
	Node(Object data,Node next){
		this.data = data;
		this.next = next;
	}
}