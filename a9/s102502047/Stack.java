package ce1002.a9.s102502047;

public class Stack {
	private int s;
	private String n;
	private Object o[]=new Object[10];
	private int c=0;
	public Stack(int s,String n) {
		this.s=s;
		this.n=n;
	}

	void push(Object obj)throws Exception//宣告例外
	{
		if(c>s-1)
			throw new Exception("Stack "+n+" is full");//丟出例外條件
		else
		{
			o[c]=obj;
			c++;
			System.out.println(n+" push "+obj);
		}
	}
	void pop()throws Exception//宣告例外
	{
		if(o[0]==null)
			throw new Exception("Stack "+n+" is empty");//丟出例外條件
		else
		{
			System.out.println(n+" pop "+o[c-1]);
			o[c-1]=null;
			c--;
		}
	}
}