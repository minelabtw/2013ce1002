package ce1002.a9.ta;

public class Stack 
{
	private Node head;
	private Node secondnode;
    private int size;
    private int count;
    private String name = "Unknown";
    
    Stack(int s , String n)
    {
    	name = n;
    	size=s;
    	count=0;
    	head =null;
    }
    class Node 
    {
        Object data;
        Node next;
    }
    public void push(Object s)throws Exception//塞物件到 Stack 最上面且呼叫此函式時可能會產生Exception
    {
    	if (count == size) //若  Stack 滿時又再塞物件進去時產生Exception
    	{
        	System.out.println(name + " is full.");
            throw new Exception();
        }
        Node tmp = new Node();
        tmp.next = head;
        tmp.data = s;
        count++;
        head = tmp;
        System.out.println(name + " push " + s);
    }
    public void pop() throws Exception //拿出 Stack 最上面的東西
    {
        if (head == null) //若 Stack 沒東西又要拿物件出來時產生Exception
        {
        	System.out.println(name + " is empty." );
            throw new Exception();
        }
        Object tmp = head.data;
        secondnode = head.next;
        head = null;//清掉最上面的Node
        head = secondnode;
        count--;
        System.out.println(name + " pop " + tmp);
    }
}
