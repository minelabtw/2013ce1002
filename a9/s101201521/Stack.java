package ce1002.a9.s101201521;

public class Stack {
	//size of the stack
	private int size;
	//current size of the stack
	private int curSize = 0;
	//name of the stack
	private String name;
	//top of the stack
	private Node top;
	//constructor with stack size and name
	public Stack(int size, String name){
		this.size = size;
		this.name = name;
	}
	//push ob into the stack
	public void push(Object ob) throws Exception{
		//check whether the stack is full or not
		if(curSize == size){
			System.out.println(getName() + " is full");
			throw new Exception("An Exception has been caught");
		}
		top = new Node(ob, top);
		curSize++;
		//print action
		System.out.println(getName() + " push " + ob);
	}
	//pop ob out of the stack
	public void pop() throws Exception{
		//check whether the stack is empty or not
		if(curSize == size){
			System.out.println(getName() + " is empty");
			throw new Exception("An Exception has been caught");
		}
		//print action
		System.out.println(getName() + " pop " + top.data);
		
		top = top.next;
		curSize--;
	}
	//return stack name
	public String getName(){
		return this.name;
	}
	
	//the class of the structure of the stack's data
	class Node {
		//data of the node
		Object data;
		//point to the next node
		Node next;
		//constructor with no parameter
		public Node(){};
		//constructor with data
		public Node(Object data, Node next){
			this.data = data;
			this.next = next;
		}
	}
}
