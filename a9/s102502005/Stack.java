package ce1002.a9.s102502005;

public class Stack {

	private int size;
	private int currentSize = 0;
	private String name;
	private Node top;

	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	public void push(Object obj) throws Exception{//throws代表這個函式可能會丟Exception。
		if (this.currentSize == this.size)
			throw new Exception(this.name + " is full");
		Node node = new Node();
		node.data = obj;
		node.next = this.top;
		this.top = node;
		this.currentSize += 1;
		System.out.println(this.name + " push " + obj);
	}

	public Object pop() throws Exception {
		if (this.currentSize == 0)
			throw new Exception(this.name + " is empty");
		this.currentSize -= 1;
		Object temp = this.top.data;
		this.top = this.top.next;
		System.out.println(this.name + " pop " + temp);
		return temp;
	}
}

class Node {
	Object data;
	Node next;
}