package ce1002.a9.s102502005;

public class A9 {

	public static void main(String[] args) {
		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");
		
		try { //需要先告訴程式try裡面的程式碼可能會丟出exception。
		s1.push("ABC");
		s1.push("DEF");
		s1.push(1111);
		s2.push("123");
		s2.push("456");
		s1.pop();
		s1.pop();
		s1.pop();
		s2.push("789");
		s1.pop();
			
		} catch (Exception e) {//丟出Exception後程式直接跳到這裡開始執行。
			System.out.println(e.getMessage());
			System.out.println("An Exception has been caught");
		}

	}

}
