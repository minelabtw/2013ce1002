package ce1002.a9.s102502016;

public class Stack {
	private int size, nowsize = 0;//stack的大小,目前大小
	private String name;
	private Node top;//一個Node型態的top(內沒有任何東西)

	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	public void push(Object obj) {
		if (nowsize != size) {//沒滿
			Node node = new Node();
			node.data = obj;//存資料
			add(node);
			System.out.println(name + " push " + obj);
		} else {
			System.out.println(name + " is full");
			System.out.println("An Exception has been caugth");
			System.exit(0);//結束程式
		}

	}

	public void pop() {
		if (nowsize != 0) {
			Node new1 = top;//最上層的存入new1
			top = top.next;//top換成舊的(top.next)
			System.out.println(name + " pop " + new1.data);
		} else {
			System.out.println(name + " is empty");
			System.out.println("An Exception has been caugth");
			System.exit(0);
		}
	}

	public void add(Node node) {
		nowsize = nowsize + 1;//算幾個資料
		node.next = top;//舊的node放進node.next
		top = node;//放入top
	}
}

class Node {
	Object data;
	Node next;//做連結
}