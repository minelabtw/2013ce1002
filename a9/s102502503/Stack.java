package ce1002.a9.s102502503;

public class Stack {
	
	private int stacksize ;
	private String stackname ;
	private Node pre;
	
	Stack(int size , String name) {  //stack建構子設定大小和名字
		stacksize = size ;
		stackname = name ;
		pre=new Node(null);
	}
	
	public void push(Object obj) throws Exception { //push函式
		
		int i = 0 ;
		Node cur=pre;
		
		while(cur.next!=null) { 
			cur=cur.next;
			i=i+1;
		}
			
		if(i<stacksize) { //如果stack未滿
			System.out.println(stackname + " push " + obj ) ;
			Node newnode = new Node(obj) ;
			cur.next = newnode ;	
		}
		
		else {  //如果stack滿了
			System.out.println(stackname +" is full") ;
			throw new Exception("An Exception has been caught");
		}
	}
	
	public Object pop() throws Exception  { //pop函式
		
		int i=0 ;
		Node cur = pre ;
		Node pre = null;
		
		while(cur.next!=null) {  //反向 
			pre=cur;
			cur=cur.next;
			i=i+1;
		}
		
		if (i!=0){  //如果stack不是空的
			System.out.println(stackname + " pop " + cur.data);
			pre.next = null;
			return null;
		} 	
		
		else { //如果stack是空的
			throw new Exception("An Exception has been caught");
		}
	}
}

class Node {
    Object data;
    Node next;
    public Node(Object data) { //Node建構子
    	this.data = data ;
    	this.next = null ;
    }
}
