package ce1002.a9.s102502503;
import ce1002.a9.s102502503.Stack;

public class A9 {

	public static void main(String[] args) {
		Stack s1 = new Stack(5 , "Stack1");  //建立stack
		Stack s2 = new Stack(2 , "Stack2");
		
		try {
			s1.push("ABC"); //實作stack
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
			}
			catch(Exception e) { //exception
				System.out.println(e.getMessage() );
			};
	}

}