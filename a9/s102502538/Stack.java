package ce1002.a9.s102502538;

public class Stack {
	private int size ;
	private String name;
	private String[] obj = new String [100];
	private int i = 2;
	
	
	public Stack(){
	
	}
	public Stack(int s, String n){
		size = s;
        name = n;
	}

	public void push(String x) {
		
		obj[i++] = x;

	}
	
	public String pop() {
		
		return obj[--i];	
	}
	
	public boolean empty(){
		return i == 0;
	}
	
	public boolean full(){
		return i == size;
	}
	public String getname(){
		return name;
	}
}
