package ce1002.a9.s100204006;


public class MyStack 
{

	    private int size; 
	    private String name;      // 只是個名稱 
	    private Node top;         //堆疊頂端
	    private int count = 0;
	   
	    //利用建構子建立堆疊
	    public MyStack(String name,int size) 
		{
	    	this.name = name;
	    	top = null;
	    	this.size = size;
	    	
		}
	    public MyStack()
	    {
	    	this("list",3);
	    }
	    
	    
	    // 插入資料至頂端 
	    public void push(Object data)throws Exception 
	    { 
	    	if( count >= size)
	    	{
	    		throw new Exception("An Exception has been caught");
	    	}
	    	
	        Node newNode = new Node(); 
	        newNode.setData(data); 
	        newNode.setNext(top);  
	        top = newNode; 
	        System.out.println(this.name+" push "+ top.data);
	    	count++;
	        
	    } 

	    // 傳回頂端資料 
	    public void pop() throws Exception
	    { 
	       if( count == 0)
	    	{
	    		throw new Exception("An Exception has been caught");
	    	}
	       
	       	System.out.println(this.name+" pop "+top.data);
	       	top = top.getNext();
	    
	    } 
	    
}

