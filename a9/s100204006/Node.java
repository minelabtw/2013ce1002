package ce1002.a9.s100204006;

public class Node {

	
    	public Object data;        // 節點資料
    	public Node next;          // 下一個節點位置
    	
	
    	public void setData(Object data)     // 節點資料 
    	{  
    		this.data = data; 
    	} 

    	public void setNext(Node next)       // 下一個節點位置 
    	{  
    		this.next = next; 
    	} 
    	
    	public Object getData()         // 傳回節點資料 
    	{  
    		return data; 
    	} 

    	public Node getNext()     // 傳回下一個節點位置
    	{   
    		return next; 
    	}
   
    
}
