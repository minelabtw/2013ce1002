package ce1002.a9.s100204006;


public class A9 
{
	    public static void main (String arg[]) 
	    {
	    	MyStack s1 = new MyStack("Stack1",5);
	    	MyStack s2 = new MyStack("Stack2",2);
	    	try
	    	{	
	    	s1.push("ABC");
	    	s1.push("DEF");
	    	s1.push(1111);
	    	s2.push("123");
	    	s2.push("456");
	    	s1.pop();
	    	s1.pop();
	    	s1.pop();
	    	s2.push("789");
	    	s1.pop();
	    	}
	    	catch(Exception e)
	    	{
	    		System.out.println(e.getMessage());
	    	}
	    	
	    	
	    }
	    
	}

