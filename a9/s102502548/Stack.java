package ce1002.a9.s102502548;

import java.awt.Cursor;

public class Stack {//宣告
	protected String name;
	protected int size;
	protected int count=0;
	private Node headnode = new Node();

	Stack(int size, String name) {//建構式
		this.size = size;
		this.name = name;
	}

	public void push(Object obg) throws Exception {//放置物件
		if (count >= size) {
			System.out.println(name+" is full") ;
			
			throw new Exception(" is full") ;//丟出例外
		}

		else {
			Node node = new Node();
			node.data = obg;
			
			Node curNode=new Node();

			curNode=headnode ;

			for (int x=0;x<count;x++){
				curNode=curNode.next ;
			}
			
			curNode.next=node ;
			
			count++ ;
		}
	}

	public Object pop() throws Exception {//取出物件
		if (count==0){
			throw new Exception(name+" is empty") ;
		}
		
		Node currentNode=new Node() ;
		
		currentNode=headnode ;
		
		for (int x=0;x<count-1;x++){
			currentNode=currentNode.next ;
		}
		
		Object o=currentNode.next.data ;
		
		currentNode.next=null ;
		
		count-- ;
		
		System.out.println(name+" pop "+o) ;
		
		return o ;
	}

	public class Node {//儲存空間
		Object data;
		Node next;
	}
	
	public String getName(){
		return name ;
	}
}
