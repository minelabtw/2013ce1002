package ce1002.a9.s102503017;

public class Stack {
	
	protected int size;
	protected String name;
	Object[] object;	
	//Default constructor.	
	Stack()
	{
		size = 0;
		name = null;		
	}
	//Parameter constructor.
	Stack(int size, String name)
	{
		this.size = size;
		this.name = name;
		setObjectArray(size);
	}
	//setter of the size of the Object array.
	void setObjectArray(int size)
	{
		object = new Object[size];
	}
	//getter of the name of the Stack object.
	String getName()
	{
		return name;
	}
	//push method of the Stack.
	void push(Object object) throws Exception
	{
		for(int i = 0; i <= size ; i++)
		{
			//When the Stack object array is full, throw an Exception.
			if(i == size)
			{
				System.out.println(name + " is full.");
				throw new Exception("An exception has been caught.");
			}
			//push the object into the Stack object array from lower to upper and i cannot equal to the size, since the reference number of the elements is started from 0.
			if(this.object[i] == null && i != size)
			{
				System.out.println(name + " push " + object);
				this.object[i] = object;
				break;
			}

		}
	}
	//pop method of the Stack.
	void pop() throws Exception
	{
		for( int i = size; i >= 0 ; i--)
		{
			//when the Stack object array is empty, throw an Exception.
			if(i == 0)
			{
				System.out.println(name + " is empty.");
				throw new Exception("An exception has been caught.");
			}
			//pop the object out from the Stack object array from upper to lower and i cannot equal to 0,since the reference number of the elements cannot equal to -1. 
			if(this.object[i-1] != null && i != 0)
			{
				System.out.println(name + " pop " + object[i-1]);
				this.object[i-1] = null;
				break;
			}

		}
	}
	
	
	
}
