package ce1002.a9.s102502033;

public class Stack
{
	private Object objarr[];//物件陣列
	private String name;//儲存名子
	private int i = 0;//疊中第幾個

	public Stack(int size, String name)
	{
		objarr = new Object[size];
		this.name = name;
	}

	public void push(Object obj) throws Exception
	{
		if (i >= objarr.length)//每放一個進來就要+1 然後爆掉要EXCEPTION
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		System.out.println(name + " push " + obj);
		objarr[i] = obj;
		i++;
	}

	public void pop() throws Exception
	{
		if (i < 0)//每放一個進來就要-1 然後沒了要EXCEPTION
		{
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		}
		System.out.println(name + " pop " + objarr[i-1]);
		i--;

	}

}
