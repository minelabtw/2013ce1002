package ce1002.a9.s102502023;

public class Stack {
    private Node head; // initialize Node head
    private int size; // initialize integer size
    private int count; // initialize integer count
    private String name = "?"; // initialize String name to ?
    
    public Stack(int size, String name) {
    	this.size = size;
    	this.name = name;
    }
    class Node {
    	Object data;
    	Node next;
    }
    public void push(Object obj) throws Exception {
    	if (count == size) {
    		throw new Exception(name + " is full");
    	}
    	System.out.println(name + " push " + obj);
    	Node tmp = new Node();
    	tmp.next = head;
    	tmp.data = obj;
    	count++;
    	head = tmp;
    }
    public Object pop() throws Exception {
    	if (head == null) {
    		throw new Exception(name + " is empty");
    	}
    	System.out.println(name + " pop " + head.data);
    	Object tmp = head.data;
    	head = head.next;
    	count--;
    	return tmp;
    }
}
