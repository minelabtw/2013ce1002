package ce1002.a9.s102502517;

public class Stack {
	private int size; // 大小
	private int counter; // 計數器
	private String name; // 名稱
	private Node top; // top node

	public Stack(int size, String name) // 建構式，計數器歸零
	{
		this.size = size;
		this.name = name;
		counter = 0;
	}

	public void push(Object obj) // push
	{
		Node newnode = new Node(); // 宣告新的node
		newnode.data = obj; // 將push進來的object給newnode.data
		newnode.next = top; // top變為newnode.next
		top = newnode; //newnode變為新的top

		if (counter == size) // 若計數器等於大小
		{
			System.out.println(name + " is full");
			System.out.println("An Exception has been caught");
			System.exit(0); // 強制中斷
		} else
			System.out.println(name + " push " + top.data);

		counter++; // 計數器+1
	}

	public void pop() // pop
	{
		if (counter == 0) // 若計數器為0
		{
			System.out.println(name + " is empty");
			System.out.println("An Exception has been caught");
			System.exit(0); // 強制中斷
		} else
			System.out.println(name + " pop " + top.data);

		top = top.next; // top.next變為新的top
		counter--; // 計數器-1
	}
}

class Node {
	public Object data;
	public Node next;
}
