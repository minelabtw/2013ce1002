package ce1002.a9.s102502036;

public class Stack {
	int i = 0;
	int size = 0;
	String name;
	Node node = new Node();

	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	public void push(Object obj) {
		try {
			if (i == size) {
				throw new Exception(name + " is full");// exception
			}
			System.out.println(name + " push " + obj);
			Node t = new Node();
			t.next = node;
			node = t;
			node.data = obj;
		} catch (Exception ex) {// handle
			System.out.println(ex.getMessage());
		}
		i++;
	}

	public void pop() {
		try {
			if (i == 0) {
				throw new Exception("An Exception has been caught");// exception
			}
			System.out.println(name + " pop " + node.data);

			node = node.next;
		} catch (Exception ex) {// handle
			System.out.println(ex.getMessage());
		}
		i--;
	}

	public String getName() {
		return name;
	}

}

class Node {
	Object data;
	Node next;
}
