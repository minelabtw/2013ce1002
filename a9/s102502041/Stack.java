package ce1002.a9.s102502041;

public class Stack {
	int size;
	Node top;
	String name;
	Stack(int s, String n)
	{
		name = n;
		size = s;
	}
	public void push(Object data)
	{
		System.out.println(name+" push "+data);
		Node newNode = new Node();
		newNode.setData(data);
		newNode.setNext(top);
		top = newNode;
		
	}
	
	public void pop()
	{
		Node tmpNode;
		tmpNode = top;
		System.out.println(name+" pop "+top);
		
		if(tmpNode == null)
		{
			System.out.println(name+" is empty");
			
		}
	}
}
class myException extends Exception
{
	public myException()
	{
		super();
	}
	public myException(String message)
	{
		super(message);
	}
}
