package ce1002.a9.s102502540;

public class Stack {
	String stackname;
	int stacksize;
	int n = 0;
	private Node top;

	class Node 
	{
		Object data;
		Node next;
	}

	public Stack(int size, String name) 
	{
		stackname = name;
		stacksize = size;
	}

	public void push(Object obj) throws Exception //push�禡
	{
		n++;
		if (n > stacksize) {
			throw new Exception(stackname
					+ " is full \nAn Exception has been caught");
		} else {
			Node node = new Node();
			node.next = top;
			node.data = obj;
			top = node;
			System.out.println(stackname + " push " + obj);
		}
		
	}

	public void pop() throws Exception //pop�禡
	{
		n--;
		if (top == null) {
			throw new Exception(stackname + "An Exception has been caught");
		} else {
			Object data = top.data;
			top = top.next;
			System.out.println(stackname + " pop " + data);
		}
		
	}

}
