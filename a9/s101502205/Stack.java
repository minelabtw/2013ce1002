package ce1002.a9.s101502205;

public class Stack {

	private String name;
	private int size, count;
	private Node list, top;

	public Stack(int size, String name) {
		
		// Set size and name
		this.size = size;
		this.name = name;
		
		// Create a node with no data at head
		list = new Node();
		top = list;
		count = 0;
	}

	public void push(Object obj) {
		try {
			if(count==size){
				System.out.println(name + " is full");					// stack is full
				throw new Exception("An Exception has been caught");	// throw exception
			}else{
				count++;
			}
		}catch (Exception e) {
			// exception handling
			System.out.println(e.getMessage());
			System.exit(0);
			
		}
		
		// create new node
		top.next = new Node();
		// set new node as top
		top = top.next;
		top.data = obj;
		// output
		System.out.println(name + " push " + top.data);
	}

	public void pop() {
		
		try {
			if(count==0){
				System.out.println(name + " is empty");					// stack is empty
				throw new Exception("An Exception has been caught");	// throw exception
			}else{
				count--;
			}
		}catch (Exception e) {
			// exception handling
			System.out.println(e.getMessage());
			System.exit(0);
		}
		
		// output
		System.out.println(name + " pop " + top.data);
		
		// set top as previous node
		top = list;
		for(int i=0; i<count; i++){
			top = top.next;
		}
		// delete object
		top.next = null;
	}
}

// linked list data structure
class Node {
	Object data;
	Node next;
}