package ce1002.a9.s102502545;

public class Stack {
	private int size ;//宣告大小變數
	private int count = 0;//宣告丟幾次	
	private String name;//宣告名稱
	private Node node[];//宣告陣列

	Stack(int size, String name) {
		node = new Node[size];//宣告陣列來儲存
		this.size = size;
		this.name = name;
		for(int i = 0 ; i < size ; i++){
			node[i] = new Node();//new出node的空間
		}
		
	}

	public void push(Object A) throws Exception {
		if (count>=size) 
		{
			System.out.println(name+"is full");
			throw new Exception("An Exception has been caught");
		} 
		else 
		{
			node[count].data = A;
			System.out.println(name+"push"+node[count].data);
			count++;
		}
	}

	void pop() throws Exception {

		count--;//先減
		if (node[count].data == null) 
		{
		    System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		} 
		else 
		{
			System.out.println(name + " pop " + node[count].data);
			node[count].data = null;
		}
	}

	class Node {
		Object data = new Object();
		Node next;
	}
}