package ce1002.a9.s101201003;

public class Stack {
	private Node top;
	private String name;
	private int size;
	private int count=0;
	//construct
	Stack(){
		top=null;
	}
	
	Stack(int size, String name){
		top=null;
		this.size=size;
		this.name=name;
	}
	//function
	public void push(Object in){
		if (count==size){
			System.out.println(name+" is full");
		}
		else{
			Node node=new Node();
			node.setdata(in);
			node.setnext(top);
			top=node;
			count++;
			System.out.println(name+" push "+top.getdata());
		}
	}
	public void pop(){
		Node tempnode =top;
		if (count==0){
			System.out.println("An Exception has been caught");
		}
		else{
			top=tempnode.getnext();
			count--;
			System.out.println(name+" pop "+tempnode.getdata());
		}
	}

}
