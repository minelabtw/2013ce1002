package ce1002.a9.s101201003;

public class Node {
	private Object data;
	private Node next;
	public void setdata(Object d){
		data=d;
	}
	public Object getdata(){
		return data;
	}
	public void setnext(Node next){
		this.next=next;
	}
	public Node getnext(){
		return next;
	}

}
