package ce1002.a9.s102502543;

public class Stack {
	class Node {
		Object data;
		Node next;

		public Node(Object data, Node next) {
			this.data = data;
			this.next = next;
		}
	}

	int Size;
	int n = 0;
	String Name;
	private Node top = null;

	Stack(int size, String name) {
		Size = size;
		Name = name;
	}

	public void push(Object obj) throws Exception {
		n++;
		if (n > Size) {
			System.out.println(Name + " is full");
			throw new Exception("An Exception has been caught");
		} else {
			top = new Node(obj, top);
			System.out.println(Name + " push " + obj);
		}
	}

	public void pop() throws Exception {
		n--;
		if (top == null) {
			System.out.println(Name + " is empty");
			throw new Exception("An Exception has been caught");
		} else {
			Object data = top.data;
			top = top.next;
			System.out.println(Name + " pop " + data);
		}
	}
}
