package ce1002.a9.s102502561;

public class Stack {

	int maxsize;// 這class有的變數
	int size;
	String name;
	Node head;

	class Node {

		Object data;
		Node next;
	}

	Stack(int i, String string) {// 建構子
		this.maxsize = i;
		this.name = string;
		this.size = 0;// stack初始值
	}

	void push(Object Data) throws Exception {
		try {
			if (size != maxsize) {// 當stack還沒滿

				Node node = new Node();// 每多個位置 就多擺置一個node
				node.data = Data;
				node.next = head; // 將head丟給下個位址
				head = node; // 將這個node丟給head
				System.out.println(name + " push " + Data);
				size++;
			} else {
				throw new Exception(name + " is full");
			}
		} catch (Exception e) {// 若滿了會丟出Exception
			throw e;
		}
	}

	void pop() throws Exception {
		try {
			if (size != 0) {// 還沒滿的話

				Node node = head;
				head = node.next;
				System.out.println(name + " pop " + node.data);
				size--;
			} else {
				throw new Exception(name + " is empty");
			}
		} catch (Exception e) { // Exception：如果Stack裡面是空的  , 這catch會抓上面的的throw 然後傳給下面的throw 
			throw e;//這個throw會丟給外面的catch
		}
	}
}
