package ce1002.a9.s102502546;

public class Stack {
	private int size ;
	private int c = 0;
	private String name;
	private Node node[];

	Stack(int size, String name) {//建構子
		this.size = size;
		node = new Node[size];
		this.name = name;
		for(int i = 0 ; i < size ; i++){//讓每個都有記憶體
			node[i] = new Node();
		}
	}

	public void push(Object obj) throws Exception { //push
		if (c >= size) {
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		} else {
			node[c].data = obj;
			System.out.println(name + " push " + node[c].data);
			c++;
		}
	}

	void pop() throws Exception { //pop

		c--;
		if (node[c].data == null) {
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		} else {
			System.out.println(name + " pop " + node[c].data);
			node[c].data = null;
		}
	}

	class Node {
		Object data = new Object();
		Node next;
	}
}
