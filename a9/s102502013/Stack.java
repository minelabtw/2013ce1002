package ce1002.a9.s102502013;

public class Stack {
	class Node 
	{
		Object data;
		Node next;
		Node(Object obj){
			this.data = obj;
			this.next = null;
			
		}
	}
	private Node head;//declare a node variable
	private String Name;//declare a string variable
	private int Size=0;//declare a integer variable
	
	
	Stack(int size , String name)
	{
		Name = name;
		Size = size;
		head = new Node(null);//create a null head
	}
	//push function
	public void push(Object obj)throws Exception
	{
		int counter = 0;
		Node cur = head;
		while(cur.next!=null){
			cur = cur.next;
			counter++;
		}
		if(counter == Size)
		{
			System.out.println(Name + " is full");
			throw new Exception ("An Exception has been caught");
		}
		else
		{
			System.out.println(Name + " push " + obj);
			cur.next= new Node(obj);
		}
	}
	//pop function
	public Object pop()throws Exception
	{
		int counter = 0;
		Node pre = null;
		Node cur = head;
		while(pre.next!=null){
			pre = cur;
			cur = cur.next;
			counter++;
		}
		if(counter<0){
			System.out.println(Name + " is empty");
			throw new Exception ("An Exception has been caught");
		}
		else
		{
			System.out.println(Name + " pop " + pre.next);
			pre.next = null;
			return null;
		}
	}
}

