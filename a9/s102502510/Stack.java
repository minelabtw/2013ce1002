package ce1002.a9.s102502510;

public class Stack {
	Stack(int s,String n)
	{
		size=s;
		name=n;
	}
	private Node head;
    private int size;
    private String name;
    class Node {
        Object data;
        Node next;
    }
    public void push(Object s) {
        Node tmp = new Node();
        tmp.next = head;
        tmp.data = s;
        size++;
        head = tmp;
    }
    public Object pop() throws Exception {
        if (head == null) {
            throw new Exception();
        }
        Object tmp = head.data;
        head = head.next;
        size--;
        return tmp;
    }
}
