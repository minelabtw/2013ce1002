package ce1002.a9.s102502510;

public class A9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack s1 = new Stack(5, "Stack1");
		Stack s2 = new Stack(2, "Stack2");
		s1.push("ABC");
		System.out.println("Stack1 push ABC");
		s1.push("DEF");
		System.out.println("Stack1 push DEF");
		s1.push(1111);
		System.out.println("Stack1 push 1111");
		s2.push("123");
		System.out.println("Stack2 push 123");
		s2.push("456");
		System.out.println("Stack2 push 456");
		
		try {
			s1.pop();
			System.out.println("Stack1 pop 1111");
		} catch (Exception e) {System.out.println("An Exception has been caught");
		}
		try {
			s1.pop();

			System.out.println("Stack1 pop DEF");
		} catch (Exception e) {System.out.println("An Exception has been caught");
		}
		s2.push("789");
		System.out.println("Stack2 is full");
		try {
			s1.pop();
			System.out.println("Stack1 pop ABC");
		} catch (Exception e) {System.out.println("An Exception has been caught");
		}
		try {
			s1.pop();
			System.out.println();
		} catch (Exception e) {
			System.out.println("An Exception has been caught");
		}
	}

}
