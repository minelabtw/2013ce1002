package ce1002.a9.s102502556;

public class A9 {

	public static void main(String[] args) throws Exception {
		Stack s1 = new Stack(5, "Stack1"); //建立二個新的堆疊(Stack)
		Stack s2 = new Stack(2, "Stack2");
		try //試著執行以下增加或刪除的指令
		{
			s1.push("ABC");
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		}
		catch (Exception e) //例外回報
		{
			System.out.println(e.getMessage());
			System.out.println("An Exception has been caught");
		}
	}

}
