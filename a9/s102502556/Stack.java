package ce1002.a9.s102502556;

public class Stack {
	
	String name; //堆疊名稱
	int size = 0; //目前大小
	int maxSize; //最大大小
	Node head; //最上方的節點
	
	Stack (int size, String name) { //設定名子和最大大小
		this.name = name;
		this.maxSize = size;	
	}
	
	void push (Object obj) throws Exception { //新增Object到堆疊最上方
		if ( size == maxSize ) //如果已經是最大的大小，則回傳錯誤訊息
		{
			throw new Exception(name+" is full");
		}
		size++; 
		Node temp = new Node(obj); //新增一個暫時節點
		//讓temp的下一個指向原本的head，再讓head變成現在的temp(最上方的Node)
		temp.next = head;
		head = temp;
		System.out.println(name+" push "+obj);
	}
	
	void pop () throws Exception { //從堆疊最上方刪除Object
		if ( size == 0 ) //如果是空堆疊，則回傳錯誤訊息
		{
			throw new Exception(name+" is empty");
		}
		size--;
		Node temp = head; //先讓一個暫時節點temp指向head(最上方的Node)
		head = head.next; //再讓head變成它的下一個Node
		System.out.println(name+" pop "+temp.data);
		temp = null; //清掉temp內的暫存內容
	}

}
