package ce1002.a9.s102502048;

public class Node 
{
	private Object data;   // 節點資料 
    private Node next;  // 下一個節點位置 

    public void setData(Object data) {  // 節點資料 
        this.data = data; 
    } 

    public void setNext(Node next) {  // 下一個節點位置 
        this.next = next; 
    } 

    public Object getData() {  // 傳回節點資料 
        return data; 
    } 

    public Node getNext() {  // 傳回下一個節點位置 
        return next; 
    } 
}
