package ce1002.a9.s102502048;

public class Stack 
{
	private Node top; 
    private String name;
    private int size=0;
    private int checksize=0;
    
    public static void check(int s,int ss) throws Exception//check method
	{
    	if(s<0||s>ss)//throw exception}
    		throw new Exception("1.push新的物件進Stack時超過Stack 的size。" +
    							"2.Stack在pop時沒有任何資料在Stack中。" );
	}
    // 利用建構子建立堆疊 
    public Stack(int size , String name) 
    { 
        this.size = size;
    	this.name = name; 
        top = null; 
    } 
    // 插入資料至頂端 
    public boolean push(Object data) 
    { 
    	checksize++;
        Node newNode = new Node(); 
        try//try block invoke method
		{
        	check(checksize,size);
			newNode.setData(data); 
			newNode.setNext(top); 
			top = newNode; 
			System.out.println(name+" push "+data);
			return true;
		}
		catch (Exception ex)//catch block
		{
			System.out.println(name+" is full"+"\nAn Exception has been caugh");
			return false;
		}        
    } 
    // 刪除頂端資料 
    public boolean pop() 
    { 
    	checksize--;
        Node tmpNode; 
        try//try block invoke method
		{
        	System.out.println(name+" pop "+top.getData());
        	check(checksize,size);
			tmpNode = top; 
			top = top.getNext(); 
			tmpNode = null;
			return true;
		}
		catch (Exception ex)//catch block
		{
			System.out.println(name+" is empty"+"\nAn Exception has been caugh");
			return false;
		}        
    }
}
