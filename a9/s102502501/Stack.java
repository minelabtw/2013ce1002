package ce1002.a9.s102502501;

public class Stack {

	class Node 
	{
	    Object data;
	    Node next;
	    Node(Object data) { //設置節點
	    	this.data = data ;
	    	this.next = null ;
	    }
	}
	
	private Node head ; //私人用
	private int a ;
	private String name ;
	
	Stack(int a , String name) {
		this.a = a ;
		this.name = name ;
		head=new Node(null);
	}
	
	public void push(Object thing) throws Exception
		{ //推的功能,宣告函式
		
		int b = 0 ;
		Node now=head;
		while(now.next!=null) //設定now
		{ 
			now=now.next;
			b++;
		}
			
		if(b==a)
		{
			System.out.println(name +" is full") ;
			throw new Exception("An Exception has been caught");
			
		}
		else {
			System.out.println(this.name + " push " + thing ) ;
			Node newnode = new Node(thing) ; //使用newnode讓 now.next
			now.next = newnode ;
		}
	}
	
	public Object pop() throws Exception  
	{ 
		int a=0 ;
		Node now = head ;
		Node pre = null;
		
		while(now.next!=null) //宣告迴圈 設定now 
		{ 
			pre=now;
			now=now.next;
			a++;
		}
		
		if (a==0) 
			throw new Exception("An Exception has been caught");
		else
		{
			System.out.println(name + " pop " + now.data);
			pre.next = null;
			return null;
		}
	}
}