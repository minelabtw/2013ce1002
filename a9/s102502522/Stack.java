package ce1002.a9.s102502522;
public class Stack {
	public int num;//容量大小
	public String name;//名字
	public int account=0;//計數器
	public Node top;
Stack(int num,String name)//將傳入的名字與容量大小存檔
{
	this.num=num;
	this.name=name;
	
}	
public void push(Object o)throws Exception//有例外
{
	if(account>=num)//測試容量是否還夠
		throw new Exception("Stack2 is full\nAn Exception has been caught");
	else
	{
		Node node=new Node();
	    node.data=o;
	    add(node);
	    System.out.println(name+" push "+o);
	}
	}
public void pop()throws Exception//有例外
{
	if(account<=0)//測試Stack裡是否還有資料
		throw new Exception("An Exception has been caught");
	else
		System.out.println(name+" pop "+top.data);
	sub(top);	
}
public void add(Node node)
{
	account++;//每加入一筆資料,容量就少1
	node.next=top;
	top=node;
}
public void sub(Node node)
{
	account--;//每讀出一筆資料,資料就少一
	top=node.next;
	
}
}
class Node
{
	Object data;
	Node next;
}
