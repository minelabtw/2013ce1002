package ce1002.a9.s102502516;

public class Stack {
	Node top; // 頂端物件
	int size, currentNumber; // currentNumber表目前堆疊中物件數量
	String name; // 堆疊名稱

	public Stack(int size, String name) {
		this.size = size;
		this.name = name;
		top = null;
		currentNumber = 0;
	}

	public void push(Object obj) throws Exception {
		if (++currentNumber > size) {
			System.out.println(name + " is full");
			throw new Exception();
		} // 堆疊滿
		Node newNode = new Node(); // 創立暫時物件
		newNode.setData(obj); // 新資料填入新物件
		newNode.setNext(top); // 新資料的下一筆是原先的頂端物件
		top = newNode; // 原先的頂端物件拉回頂端
		System.out.println(name + " push " + obj.toString());
	}

	public void pop() throws Exception {
		if (--currentNumber < 0) {
			throw new Exception();
		} // 已無物件
		System.out.println(name + " pop " + top.getData().toString());
		top = top.getNext(); // 將第二筆物件指定給頂端
	}

	class Node {
		Object data; // 物件的資料
		Node next; // 下一個物件

		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}
	}
}
