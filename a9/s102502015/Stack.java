package ce1002.a9.s102502015;

import ce1002.a9.s102502015.Node;

public class Stack {
	Stack(int size, String name) {
		_size = size;
		_name = name;
	}

	private int _size;// STACK大小
	private int now = 0;// STACK目前數量
	private String _name;// STACK名稱
	public Node top;// 最頂端

	public void push(Object obj){
		try {
			if (now != _size) {// 沒超過就PUSH
				Node node = new Node();// 新的TOP
				node.data = obj;// 存資料
				add(node);// 把node變top top變top.next
				System.out.println(_name + " push " + obj);
			} else {
				System.out.println(_name + " is full");
				throw new Exception("An Exception has been caught");// 丟出例外
			}
		} catch (Exception ex) {// 捕捉例外
			System.out.println(ex.getMessage());
			System.exit(0);// 終止程式
		}
	}

	public void add(Node node) {
		now = now + 1;
		node.next = top;// 新的TOP.next變成舊的TOP
		top = node;// node變新的TOP
	}

	public void pop(){
		try {// 不等於0就提出
			if (now != 0) {
				now = now - 1;
				Node a = top;// 暫存node
				top = top.next;// TOP往前移
				System.out.println(_name + " pop " + a.data);
			} else {
				System.out.println(_name + " is emtpy");
				throw new Exception("An Exception has been caught");// 丟出例外
			}
		} catch (Exception ex) {// 捕捉例外
			System.out.println(ex.getMessage());
			System.exit(0);// 終止程式
		}
	}

}
