package ce1002.a9.s102302053;

public class Stack {
	private String name;
	private int size;
	private int counter = 0;
	private Node cur;
	Stack(int size , String name){//建構stack
		this.size = size;
		this.name = name;	
		cur = null;
	}
	
	public void push(Object data){//將資料存入最上層
		if (counter == size){//如果超過size則輸出提示並跳出程式
			System.out.println(name +" is full\nAn Exception has been caught");
			System.exit(0);
		}
		else{//將資料存入
			System.out.println(name + " push " + data);
			Node newnode = new Node(data,cur);
			cur = newnode;
			counter ++;
		}
	}

	public void pop(){//將最上層資料提出
		if(counter==0){//如果沒有資料則輸出提示並跳出程式
			System.out.println(name + " is empty\nAn Exception has been caught");
			System.exit(0);
		}
		else{//將資料提出
			System.out.println(name +" pop " + cur.getdata());
			 Node tmp; 
		     tmp = cur;
		     cur = cur.getNext(); 
		     tmp = null; 
			counter--;
		}
	}
}
class Node
{
	Object data;
	Node next;
	Node(Object data, Node nextNode){//建構節點
	    this.data = data;
	    next = nextNode;
	  }
	public Object getdata(){//回傳資料
		return data;
	}
	public Node getNext() {  //傳回下一個節點位置 
	    return next; 
	} 
	
}