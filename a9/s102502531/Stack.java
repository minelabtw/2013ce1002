package ce1002.a9.s102502531;

class Stack { // stack

	int size = 0;
	int maxsize;
	String stackname;
	Node head;

	Stack(int maxsize, String stackname) {
		setmaxsize(maxsize);
		setstackname(stackname);
	}

	void setmaxsize(int maxsize) {
		this.maxsize = maxsize;
	}

	void setstackname(String stackname) {
		this.stackname = stackname;
	}

	void push(Object data) throws Exception {
		try {
			if (!(boolean) (size == maxsize)) {
				size++;
				Node node = new Node();
				node.data = data;
				node.next = head;
				head = node;
				System.out.println(stackname + " push " + data);
			} else if (size == maxsize) {
				throw new Exception(stackname + " is full");
			}
		} catch (Exception e) {
			throw e;
		}
	}

	void pop() throws Exception {
		try {
			if (!(boolean) (size == 0)) {
				size--;
				Node node = head;
				head = node.next;
				System.out.println(stackname + " pop " + node.data);
			} else if (size == 0) {
				throw new Exception(stackname + " is empty");
			}
		} catch (Exception e) {
			throw e;
		}
	}
}
class Node {
	Object data;
	Node next;
}