package ce1002.a9.s102502504;

import java.util.ArrayList;

public class Stack 
{
	private ArrayList<Object> list = new ArrayList<>(); //儲存元素的串列
	private static int size = 0; //stack的大小
	private String name; //stack的名字
	
	Stack(int size, String name) 
	{
		this.size = size;
		this.name = name;
	}
	
	public static boolean check1(int num) throws Exception  //檢查push新的物件進Stack時是否超過Stack的size
	{
		if(num > size) 
		{
			throw new Exception("An Exception has been caught");
		}
		return false;
	}
	
	public static boolean check2(int num) throws Exception //檢查Stack在pop時有沒有任何資料在Stack中
	{
		if(num < 0) 
		{
			throw new Exception("An Exception has been caught");
		}
		return false;
	}
	
	public int getSize() //串列大小
	{
		return list.size();
	}
	
	public void push(Object obj) //新增元素到堆疊中
	{
		try 
		{
			list.add(obj); //加進去
			check1(getSize()); //檢查push新的物件進Stack時是否超過Stack的size(有進入if才會執行例外)
			System.out.println(this.name + " push " + obj);
		} 
		catch (Exception ex) 
		{
			System.out.println(this.name + " is full");
		}
	}
	
	public void pop() //移除第一個元素
	{
		try 
		{
			check2(getSize() - 1); //檢查Stack在pop時有沒有任何資料在Stack中(有進入if才會執行例外)
			Object o = list.get(getSize() - 1);
			System.out.println(this.name + " pop " + o);
			list.remove(getSize() - 1);
		} 
		catch (Exception ex) 
		{
			System.out.println(ex.getMessage()); 
		}
	}
	
}
