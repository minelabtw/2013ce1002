package ce1002.a9.s102502006;

public class Stack {
	int size; // 大小
	int Max_size; // 最大
	String name; // 名子
	Node top; // 最上層
	
	public Stack(int size,String name){
		this.Max_size = size;
		this.name = name;
		this.size = 0;
	}
	
	public int get_size(){
		return size;
	}
	
	public String get_name(){
		return name;
	}
	
	public void push(Object data) throws Exception{ // 放入
		
		if(size<Max_size){
			size++;
			Node node = new Node();
			node.data = data;
			node.next = top;
			top = node;
			System.out.println(name + " push " + data);
		}
		else throw new Exception(name + " is full");	
	}
	
	public void pop() throws Exception{ // 取出
		
		if(size!=0){
			size--;
			Node node = top;
			top = top.next;
			System.out.println(name + " pop " + node.data);
		}
		else throw new Exception(name + " is empty");
	}
}
