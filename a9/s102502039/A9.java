package ce1002.a9.s102502039;

public class A9 {

	public static void main(String[] args) throws Exception {

		Stack s1 = new Stack(5, "Stack1");//set two stacks function
		Stack s2 = new Stack(2, "Stack2");
		try {//output sequentially
			s1.push("ABC");
			System.out.println("Stack1 push ABC");
		} catch (Exception ex) {
			System.out.println("Stack1 is full");
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s1.push("DEF");
			System.out.println("Stack1 push DEF");
		} catch (Exception ex) {
			System.out.println("Stack1 is full");
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s1.push(1111);
			System.out.println("Stack1 push 1111");
		} catch (Exception ex) {
			System.out.println("Stack1 is full");
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s2.push("123");
			System.out.println("Stack2 push 123");
		} catch (Exception ex) {
			System.out.println("Stack2 is full");
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s2.push("456");
			System.out.println("Stack2 push 456");
		} catch (Exception ex) {
			System.out.println("Stack2 is full");

			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s1.pop();
			System.out.println("Stack1 pop 1111");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s1.pop();
			System.out.println("Stack1 pop DEF");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s1.pop();
			System.out.println("Stack1 pop ABC");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		try {
			s2.push("789");
			System.out.println("Stack2 push 789");
		} catch (Exception ex) {
			System.out.println("Stack2 is full");
			System.out.println(ex.getMessage());
			System.exit(1);
		}

	}

}
