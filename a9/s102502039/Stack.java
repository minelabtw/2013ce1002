package ce1002.a9.s102502039;

import java.util.jar.Attributes.Name;

public class Stack {
	private Node head;//declare variables
	private int size = 0;
	private int maxsize;
	private String name;

	class Node {
		Object data;
		Node next;
	}

	public Stack(int i, String string) {//constructor of stack
		name = string;
		maxsize = i;
	}

	public void push(Object s) throws Exception {//push function
		if (size == maxsize) {
			throw new Exception("An Exception has been caught");
		}
		Node tmp = new Node();
		tmp.next = head;
		tmp.data = s;
		size++;
		head = tmp;

	}

	public Object pop() throws Exception {//pop function
		if (head == null) {
			System.out.println("An Exception has been caught");
			throw new Exception();
		}
		Object tmp = head.data;
		head = head.next;
		size--;
		return tmp;
	}
}
