package ce1002.a9.s102502003;

public class Stack {
	
	private int size;
	private String name;
	private Node top;
	private int now = 0;
	
	class Node 
	{
	    Object data;
	    Node next;
	    
	}
	
    public Stack(int size , String name){
    	this.size=size;
    	this.name=name;
	}
	
	public void push(Object obj) throws Exception{
		if(now >= size){
			throw new Exception("An Exception has been caught");
		}
		Node n = new Node();
		n.next=top;
		n.data=obj;
		now++;
	}
	
	public Object pop()	throws Exception{
		if(top==null){
			throw new Exception("An Exception has been caught");
		}
		
		Object obj = top.data;
		top=top.next;
		now--;
		
		return obj;
	}

	
}

