package ce1002.a9.s102502529;

public class Stack {
	int size;
    int Max_size;
    String name;
    Node head;
    Stack(int size, String name){							//setter
    	this.Max_size = size;
        this.name = name;
        this.size = 0;
    }
    public void pop() throws Exception {
    	if(!Empty()){										//不是空的就輸出
    		size--;
            Node node = head;
            head = node.next;
            System.out.println(name + " pop " + node.data);
    	}
    	else{
    		throw new Exception(name + " is empty");		//代表空的處理例外
    	}
    }
    public void push(Object data)throws Exception{
    	if(!Full()){										//不是滿的就push
    		size++;
            Node node = new Node();
            node.data = data;
            node.next = head;
            head = node;
            System.out.println(name + " push " + data);			
    	}
    	else{
    		throw new Exception(name + " is full");			//滿的舊曆處理例外
    	}
    }
    boolean Empty(){										//判斷是不是空
    	 if(size == 0) {
          return true;
    	 }
         return false;
    }
    boolean Full(){											//判斷是不是滿
    	 if(Max_size== size) {
             return true;
         } 
         return false;
    }
}
class Node {

    Object data;
    Node next;
}

