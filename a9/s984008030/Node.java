package ce1002.a9.s984008030;

public class Node {
	Object data;
    Node next;
    public Node() {
    	
    }
    
    public Node(Object data, Node next) {
    	this.data = data;
    	this.next = next;
    }
    
}
