package ce1002.a9.s984008030;

public class Stack {
	
	private int size; // Stack size
	private String name; // Stack name
	private int counter; // counter number of element
	private Node top; // Top of stack
	
	public Stack(int size , String name) {
		this.size = size;
		this.name = name;
		this.counter = 0;
		this.top = null;
	}
	
	public void push(Object obj) throws Exception {
		if (this.counter < this.size) {
			//Stack still has space to store element
			System.out.println(this.name + " push " + obj);
			Node newElement = new Node(obj, this.top); // Add top chain to newElement next pointer
			this.top = newElement;
			this.counter++;
		}
		else {
			//Stack has no space
			System.out.println(this.name + " is full");
			throw new Exception("An Exception has been caught");
		}
	}
	
	public Node pop() throws Exception {
		if (this.counter > 0) {
			Node returnElement = top;
			top = returnElement.next; // Assign remain chain to top
			System.out.println(this.name + " pop " + returnElement.data);
			return returnElement;
		}
		else {
			//Stack is empty
			System.out.println(this.name + " is empty");
			throw new Exception("An Exception has been caught");
		}
	}
	
}
