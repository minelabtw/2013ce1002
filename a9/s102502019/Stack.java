package ce1002.a9.s102502019;

public class Stack {
	Stack(int size, String name) {
		stacksize = size;
		stackname = name;
		}
		private int stacksize;// STACK大小
		private int nownumber = 0;// STACK目前數量
		private String stackname;// STACK名稱
		public Node top;// 最頂端
		public void push(Object obj) {
		try {
		if (nownumber != stacksize) {// 沒超過就PUSH
		Node node = new Node();// 新的TOP
		node.data = obj;// 存資料
		add(node);// 把node變top top變top.next
		System.out.println(stackname + " push " + obj);
		} else {
		System.out.println(stackname + " is full");
		throw new Exception();//丟出例外
		}
		} catch (Exception ex) {//捕捉例外
		System.out.println("An Exception has been caught");
		System.exit(0);//終止程式
		}
		}
		public void add(Node node) {
		nownumber = nownumber + 1;
		node.next = top;// 新的TOP.next變成舊的TOP
		top = node;// node變新的TOP
		}
		public void pop() {
		try {// 不等於0就提出
		Node a = top;// 暫存node
		top = top.next;// TOP往前移
		System.out.println(stackname + " pop " + a.data);
		} catch (Exception ex) {//捕捉例外
		System.out.println(stackname + " is emtpy");
		System.out.println("An Exception has been caught");
		System.exit(0);//終止程式
		}
		}
}
class Node 
{
    Object data;
    Node next;
}
