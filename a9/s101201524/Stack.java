package ce1002.a9.s101201524;

public class Stack {
	//create stack's data
	private String name;
	private Node top;
	private int size;
	
	Stack(int size, String name){
		//set stack's data
		this.name = name;
		this.size = size;
		top = null;
	}
	
	public void push(Object obj) throws Exception{
		try{
			//check the rest space of stack 
			if(size == 0)
				throw new Exception("An Exception has been caught");
			//add Node into stack
			Node newitem = new Node();
			newitem.data = obj;
			newitem.next = top;
			//set the latest item at the top
			top = newitem;
			System.out.println(name + " push " + obj);
			size--;
		}
		catch(Exception e){
			//show that the exception happens
			System.out.println(name + " is full");
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
	public void pop(){
		try{
			//check the rest item in the stack
			if(top == null)
				throw new Exception("An Exception has been caught");
			//take the top item and set the second one as the top
			System.out.println(name + " pop " + top.data);
			top.data = null;
			top = top.next;
			size++;
		}
		catch(Exception e){
			//show that the exception happens
			System.out.println(name + " is empty");
			System.out.println(e.getMessage());
			System.exit(1);
		}
	}
}
//create a node class
class Node{
	Object data;
	Node next;
}