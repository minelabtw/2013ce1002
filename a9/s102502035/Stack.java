package ce1002.a9.s102502035;

import java.util.LinkedList;

public class Stack {
	int size;
	int counter = 0;
	String name;
	Node node;

	public Stack(int size, String name) {// constructor
		this.size = size;
		this.name = name;
	}

	public void push(Object o) {// push Object
		Node newNode = null;
		if (counter < size) {
			newNode = new Node();
		}
		try {
			newNode.next = node;
			node = newNode;
			newNode.data = o;
			counter++;
			System.out.println(name + " push " + o);
		} catch (Exception ex) {
			System.out.println(name + " is full");
		}
	}

	public void pop() {
		try {
			Object string = node.data;// 另存
			node = node.next;
			System.out.println(name + " pop " + string);
			counter--;
		} catch (java.lang.NullPointerException ex) {
			System.out.println("An Exception has been caught");
		}
	}

	class Node {
		Object data;
		Node next;
	}
}
