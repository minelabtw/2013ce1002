package ce1002.a9.s102502518;

public class Stack {
	Node top; 
	int size, currentNumber; // currentNumber
	String name; // stack name

	public Stack(int size, String name) {
		this.size = size;
		this.name = name;
		top = null;
		currentNumber = 0;
	}

	public void push(Object obj) throws Exception {
		if (++currentNumber > size) {
			System.out.println(name + " is full");
			throw new Exception();
		} // stack full
		Node newNode = new Node(); // create object
		newNode.setData(obj); // new data filled with new object
		newNode.setNext(top); // next new data is top object
		top = newNode; // original object push back to top
		System.out.println(name + " push " + obj.toString());
	}

	public void pop() throws Exception {
		if (--currentNumber < 0) {
			throw new Exception();
		} 
		System.out.println(name + " pop " + top.getData().toString());
		top = top.getNext(); 
	}

	class Node {
		Object data; // object data
		Node next; // next object

		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}
	}
}
