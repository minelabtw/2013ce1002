package ce1002.a9.s101201504;
class Node { 
    private Object data;   // link data
    private Node next;  // next link address

    public void setData(Object data) {  // link data
        this.data = data; 
    } 

    public void setNext(Node next) {  // next link data 
        this.next = next; 
    } 

    public Object getData() {  //  return link data
        return data; 
    } 

    public Node getNext() {  // return  next link address
        return next; 
    } 
} 
class Stack {
	private Node top;
	private String name;
	private int size;
	private int count=0;
	Stack()
	{
		top=null;
	}
	Stack(int s, String n)
	{
		top=null;
		size=s;
		name=n;
	}
	public void push(Object in)     //push data
	{
		if (count>=size)
		{
			System.out.println(name+" is full");
		}
		else
		{
			Node newNode=new Node();
			newNode.setData(in);
			newNode.setNext(top);
			top=newNode;
			count++;
			System.out.println(name+" push "+top.getData());
		}
		}
	public void pop()      //pop data
	{
		Node tempNode =top;
		if (count==0)
		{
			System.out.println("An Exception has been caught");
		}
		else
		{
			top=tempNode.getNext();
			count--;
			System.out.println(name+" pop "+tempNode.getData());
		}
	}
}

public class A9 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");
		s1.push("ABC");
		s1.push("DEF");
		s1.push(1111);
		s2.push("123");
		s2.push("456");
		s1.pop();
		s1.pop();
		s1.pop();
		s2.push("789");
		s1.pop();

	}

}
