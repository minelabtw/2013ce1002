package ce1002.a9.s101201046;

public class Stack {
	private Node top; //top node
	private int max_size; //max size
	private int size; //now size
	private String name; //the name of stack
	
	class Node { //data node
		Object data;
		Node next;
	}
	
	Stack(int size, String name) { //initialization
		top = null;
		this.max_size = size;
		this.size = 0;
		this.name = name;
	}
	
	public void push(Object data) throws Exception { // push data
		Node d = new Node();
		
		if (size == max_size) 
			throw new Exception(name + " is full");
		
		size ++;
		System.out.println(name + " push " + data);
		d.data = data;
		d.next = top;
		
		top = d;
	}
	
	public Object pop() throws Exception { // pop data
		Node d = top;
		
		if (top == null)
			throw new Exception(name + " is empty");
		
		System.out.println(name + " pop " + d.data);
		top = top.next;
		
		return d.data;
	}

}
