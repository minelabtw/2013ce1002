package ce1002.a9.s102502514;

public class Stack {
	
	private String name;
	private int size = 0;
	private int maxsize;
	private Node head;
	
	Stack(int size , String name){
		this.maxsize = size;
		this.name = name;
	}
	
	public void push(Object obj) throws Exception{  //實作push
		if (size == maxsize){
			throw new Exception(name + " is full");  //如果超過上限大小則丟出例外資訊
		}
		else{
			size++;
			Node newnode = new Node();
			newnode.data = obj;  //將obj資料給新節點的data
			newnode.next = head;  //新節點的next指標指向堆疊頂端
			head = newnode;  //將新節點的data與link一起放至堆疊頂端
			System.out.println(name + " push " + obj);
		}
	}
	
	public void pop() throws Exception{
		if (size == 0){
			throw new Exception(name + " is empty");  //如果本身已是空堆疊則丟出例外資訊
		}
		else{
			size--;
			Node tempnode = new Node();
			tempnode = head;  //將頂端資料暫存於新節點
			head = head.next;  //將頂端節點指向下一個節點,意即將頂端節點pop
			System.out.println(name + " pop " + tempnode.data);
			tempnode = null;  //將用來暫存資料的新節點清空,以利下一次的pop
		}
	}
}

class Node 
{
    Object data;
    Node next;
}
