package ce1002.a9.s102502544;


public class Stack {
	int size; //宣告大小
	String name; //宣告名字
	int count=0; 
	Node[] node;
	
	Stack(int size , String name){ //建構子
		this.size = size;
		this.name = name;
		node = new Node[size];
		for(int i = 0 ; i < size ; i++){
			node[i] = new Node();
		}
	}
	public void push(Object obj) throws Exception{
		if(count >= size){
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}else {
			node[count].data = obj;
			System.out.println(name + " push " + node[count].data);
		}
		count++;
	}
	public void pop() throws Exception{
		count--;
		if(count < 0){
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		}else {
			System.out.println(name + " pop " + node[count].data);
			node[count].data = null;
		}
	}
	
	public class Node{
		Object data;
	    Node next;
	}
}
