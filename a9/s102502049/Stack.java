package ce1002.a9.s102502049;

public class Stack {
	class Node{
		 Object data; 
		 Node next;	// next node address
	}
	
	private Node toppt;  // top node pointer
	private int nowsize=0;
	private int maxsize;
	private String name;
	
	public Stack(int size, String name) { // constructor
		maxsize = size;
		this.name = name;
	}
	
	public void push(Object data) throws Exception{
		if(nowsize==maxsize){
			System.out.println(name +  " is full");
			throw new Exception();
		}
		Node newNode = new Node();
		newNode.next = toppt; // create link
		newNode.data = data; // set data
		System.out.println( name + " push " + data);
		toppt = newNode; // set new node address to top pointer
		nowsize++; // increase size
	}
	
	public Object pop() throws Exception{
		if(nowsize==0){
			System.out.println(name + " is empty");
			throw new Exception();
		}
		Object holdData = toppt.data;
		toppt = toppt.next; // set the lower node to the top node
		System.out.println( name + " pop " + holdData);
		nowsize--;
		return holdData;
	}
	
	
}
