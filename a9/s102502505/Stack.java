package ce1002.a9.s102502505;

public class Stack {
	private Node top; 
    private String name;
    private int size;
    private int nowsize=0;

    public Stack(int size,String name) {
        this.name=name;
        this.size=size;
        top=null;//設定top初始
    } 

    public void push(String data) { //將data推入stack中
    	
        Node newNode = new Node(); //建立新節點
        newNode.setData(data); //將data傳入新節點中的data
        newNode.setNextNode(top);//設定下一個節點位置
        top = newNode;
        nowsize++;//計算data數量
        
        try{//找出例外
        	if(nowsize>size)//如果data數量大於size，丟到例外
        	{
        		throw new Exception();
        	}
        	else//其餘則執行下列動作
        	{
        		System.out.println(name+" push "+data);
        	}
        }
        
        catch(Exception ex){//例外時執行下列動作
        	
        	System.out.println(name+" is full");//data數量超出stack大小
        }
        
    } 

    public void pop() {//將top的data從stack中捨去
    	
    	try{//找出例外
         	if(top!=null)//假如stack不是空的
         	{
         		 System.out.println(name+" pop "+getTop());//先輸出下列
         	}
         	else//假如stack已經空了，丟到例外
         	{
         		throw new Exception();
         	}
        }
         
        catch(Exception ex){//例外時輸出下列
         	System.out.println("An Exception has been caught");
        }
    	
    	Node tmpNode;//呼叫上一個node
        tmpNode = top;//將上一個node，設為top
        top = top.getNextNode(); 
      
    } 
    
    public String getTop() { //找出top的data
        
    	return top.getNodeData();
    } 
} 

