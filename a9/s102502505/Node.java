package ce1002.a9.s102502505;

public class Node {
	
	 	String data;   // 建立節點資料 
	    Node nextnode;  // 建立下一個節點的位置 

	    public void setData(String data) {  // 設立data
	        this.data = data; 
	    } 

	    public void setNextNode(Node next) {  // 設立下一個node位置
	        this.nextnode = next;
	    } 

	    public String getNodeData() {  // 回傳node的data
	        return data;
	    } 

	    public Node getNextNode() {  // 傳回下一個節點位置 
	        return nextnode; 
	    } 
}
