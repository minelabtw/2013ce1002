package ce1002.a9.s102502002;

public class Stack {
	Object[] el; // declare a object array
	String name; // string to save names of the stacks
	int size = 0; // declare an integer size to save size of the array
	int i = 0; // counter

	Stack(int size, String name) {
		el = new Object[size]; // create a new object array
		this.name = name; // save the name and size
		this.size = size;
	}

	public void push(Object str) {
		try { // save each input to el array
			el[i] = str;
			System.out.println(name + " push " + el[i]);
			if (i > size) // throw new exception if the stack is full
				throw new Exception();
			i++;
		} catch (Exception ex) {
			System.out.println(name + " is full");
			System.out.println("An Exception has been caught");
			System.exit(0); // end program
		}
	}

	public void pop() {
		try { // pop out elements from the last
			if (i < 0)
				throw new Exception(); // if the stack is empty throw an exception
			System.out.println(name + " pop " + el[--i]);
		} catch (Exception e) {
			System.out.println(name + " is empty");
			System.out.println("An Exception has been caught");
			System.exit(0);
		}
	}

}
