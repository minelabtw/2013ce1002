package ce1002.a9.s102502559;

public class Stack {
	
	private String name;//stack名子
	private int maxSize;//總長度
	private int size;//當前長度
	private Node head;//指向
	
	
	public Stack(int length , String name)
	{
		this.maxSize = length;
		this.name = name;
		this.size = 0;
	}
	
	public int getSize()
	{
		return this.size;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void push(Object data) throws Exception//將資料加入
	{
		try
		{
			if(!fulling()) {
                size++;
                Node node = new Node();//新建一個node
                node.data = data;//設定data值
                node.next = head;//將此節點只向下一個
                head = node;//接著讓此新節點成為第一個節點head
                System.out.println(name + " push " + data);
            }
		}
		catch(Exception e)
		{
			 throw e;
		}
	}
	
	public void pop () throws Exception
	{
		try
		{
			if(!empty()) {
                size--;//stack長度減少1
                Node node = head;//新增一個節點與head相同（（取代原先head
                head = node.next;//指向下一個節點
                System.out.println(name + " pop " + node.data);
			}
		}
		catch(Exception e)
		{
			throw e;
		}
	}
	
	public boolean empty() throws Exception
	{
		if(size == 0)
		{
			throw new Exception(this.name + " is Empty ");
		}
		return false;
	}
	
	public boolean fulling() throws Exception
	{
		if(size == maxSize)
		{
			throw new Exception(this.name + " is full ");
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
