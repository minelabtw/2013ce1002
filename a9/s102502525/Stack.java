package ce1002.a9.s102502525;

public class Stack
{
	int maxSize;
	int size;
	String name;//變數
	
	Node head;
	// Stack 建構子
	Stack(int maxSize, String name)
	{
		this.maxSize = maxSize;
		this.size = 0;
		this.name = name;
	}
	void push(Object obj) throws Exception
	{
		// 檢查
		if (size == maxSize)
			throw new Exception(this.name + " is full");
		
		// 增加node
		size++;
		Node tmp = new Node(obj);
		tmp.next = head;
		head = tmp;
		
		// 印出來
		System.out.println(this.name + " push " + obj);
	}
	
	void pop() throws Exception
	{
		// 檢查是否是空的
		if (size == 0)
			throw new Exception(this.name + " is empty");
		
		// 移除node
		size--;
		Node tmp = head;
		head = head.next;
		
		// 印出來
		System.out.println(this.name + " pop " + tmp.data);
		tmp = null; // clear the tmp
	}
}
