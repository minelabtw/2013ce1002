package ce1002.a9.s102502053;

public class Stack {
	
	//call variables
	String name;
	int size;
	int n = 1;
	private Node top;
	
	class Node //node class
	{
		Object data;
		Node next;
	}
	
	
	
	public Stack(int size , String name) //constructor
	{
		this.name = name;
		this.size = size;
	}
	
	public void push(Object obj) throws Exception //push
	{
		if(n > size)
		{
			throw new Exception(name + " is full \nAn Exception has been caught");
		}else
		{
			Node node = new Node();
			node.next = top;
			node.data = obj;
			top = node;
			System.out.println(name + " push " + obj);
		}
		n++;
	}
	
	public void pop() throws Exception //pop
	{
		if (top == null) 
		{ 
			throw new Exception(name + "An Exception has been caught");
		}else
		{
			Object data = top.data;
			top = top.next;
			System.out.println(name + " pop " + data);
		}
		n--;
	}
	
}
	