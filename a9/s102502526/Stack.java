package ce1002.a9.s102502526;

public class Stack {
	int maxSize;
	int size;
	String name;
	
	Node head;
	Stack(int maxSize, String name)
	{
		this.maxSize = maxSize;
		this.size = 0;
		this.name = name;
	}	// constructor
	
	
	void push(Object obj) throws Exception  //可丟可不丟exception，若有exception則回A9 class去執行
	{
		if (size == maxSize)
			throw new Exception(this.name + " is full");

		size++;
		Node tmp = new Node(obj);
		tmp.next = head;
		head = tmp;
		
		System.out.println(this.name + " push " + obj);
	}
	
	void pop() throws Exception
	{
		if (size == 0)
			throw new Exception(this.name + " is empty");
		
		size--;
		Node tmp = head;
		head = head.next;
		
		System.out.println(this.name + " pop " + tmp.data);
		tmp = null; // 清除tmp
	}
}
