package ce1002.a9.s100502022;

public class Stack {
	//use node to store data 
	class Node {
        Object data;
        Node next;
        Node(Object data, Node next) {
            this.data = data;
            this.next = next;
        }
    }
	
	private Node head = new Node(null, null);
	private int size;
	private String name;
	private int i=0;
	//Constructor
	public Stack(int size, String name){
		this.size=size;
		this.name=name;
	}
	//push
	public void push(Object obj) throws Exception{
		if(i<size){
			System.out.println(name+" push "+obj.toString());
			head = new Node(obj, head);
			i++;
		}
		//if stack is full
		else{
			System.out.println(name+" is full");
			throw new Exception("An Exception has been caught");
		}
	}
	public Object pop() throws Exception{
		//if stack is empty
		if(head.data==null){
			System.out.println(name+" is empty");
			throw new Exception("An Exception has been caught");
		}
		else{
			Object item = head.data;
			System.out.println(name+" pop "+item.toString());
			head=head.next;
			i--;
			return item;
		}
	}
}

