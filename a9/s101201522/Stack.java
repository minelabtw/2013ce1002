package ce1002.a9.s101201522;

public class Stack {
	private Node top; //top node
	private int max_size; //max size
	private int size; //now size
	private String name; //the name of stack
	
	class Node { //data node
		Object data;
		Node next;
	}
	
	Stack (int size, String name) { //initial
		top = null;
		this.max_size = size;
		this.size = 0;
		this.name = name;
	}
	
	public void push (Object data) throws Exception { //push data
		Node t = new Node();
		
		if (size == max_size) 
			throw new Exception(name + " is full"); //exception message
		size++;
		System.out.println(name + " push " + data);
		t.data = data;
		t.next = top;
		top = t;
	}
	
	public Object pop() throws Exception { //pop data
		Node t = top;
		
		if (top == null)
			throw new Exception(name + " is empty"); //exception message
		
		System.out.println(name + " pop " + t.data);
		top = top.next;
		return t.data;
	}

}
