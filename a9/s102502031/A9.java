package ce1002.a9.s102502031;

public class A9 {
	public static void main(String[] args) {
		Stack stack1 = new Stack(5, "Stack1");
		Stack stack2 = new Stack(2, "Stack2");

		stack1.push("ABC");
		stack1.push("DEF");
		stack1.push(1111);
		stack2.push("123");
		stack2.push("456");
		stack1.pop();
		stack1.pop();
		stack1.pop();
		stack2.push("789");
		stack1.pop();
	}
}
