package ce1002.a9.s102502031;

public class Stack {
	private int size;
	private String name;
	private Object[] objects;
	private int count = 0;

	public Stack(int size, String name) {
		this.size = size;
		this.name = name;
		objects = new Object[size];
	}

	public void push(Object object) {
		try {
			count++;
			if (count > size)
				throw new Exception(name + " is full.");
			// push statement
			for (int i = count - 1; i > 0; i--)
				objects[i] = objects[i - 1];
			objects[0] = object;
			System.out.println(name + " push " + objects[0]);
		} catch (Exception full) {
			System.out.println(name + " is full.");
			System.out.println("An Exception has been caught.");
			System.exit(0);
		}
	}

	public void pop() {
		try {
			count--;
			if (count < 0)
				throw new Exception(name + " is empty.");
			// pop statement
			System.out.println(name + " pop " + objects[0]);
			for (int i = 0; i < count; i++)
				objects[i] = objects[i + 1];
		} catch (Exception empty) {
			System.out.println(name + " is empty.");
			System.out.println("An Exception has been caught.");
			System.exit(0);
		}
	}
}
