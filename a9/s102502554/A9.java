package ce1002.a9.s102502554; 

import java.lang.*;

public class A9 {
	
	public static void main (String [] args) throws Exception {
		
		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");//crate 2 stack for different name and size
		
		try {
			s1.push("ABC");
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		}//push and pop the data
		catch(Exception e) {//catch the Exception and print it			
			System.out.println(e.getMessage());
		}
		
	}
}
