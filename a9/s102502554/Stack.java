package ce1002.a9.s102502554;

public class Stack {
	
	    private Node top = new Node();//node point to point the data 
	    private String name;  //stack's name
	    private int maxsize;//stack's maxsize
	    int size = 0;

	    public Stack(int size, String name) { 
	        this.maxsize = size;
	        this.name = name;
	    }  
	    
	    public void pop() throws Exception {
	    	
	    	if( size == 0 )
	    	{
	    		throw new Exception(name+" is empty");
	    	}//if empty , it will have an Exception
	    	size--;//if pop something , the size will -1
	    	Object r = top.getData();//save the top data	    	
	    	Node tmpNode = new Node(); 
	        tmpNode = top; 
	        top = top.getNext(); //send the top data to the next node
	        tmpNode = null; //delete the top data
	        System.out.println(name+" pop "+r);//print the state of execution
	    	
	    }
	    
	    public void push(Object obj) throws Exception {
	    	if( size == maxsize )
	    	{
	    		throw new Exception(name+" is full"+"\nAn Exception has been caught");
	    	}//if empty , it will have an Exception
	    	size++;//if push something , size will +1
	    	Node newNode = new Node(); 
	        newNode.setData(obj); 
	        newNode.setNext(top); 
	        top = newNode; 
	        System.out.println(name+" push "+obj);
	    }
	} 
	
	
class Node { 
    private Object data;   // 節點資料 
    private Node next;  // 下一個節點位置 

    public void setData(Object data) {  // 節點資料 
        this.data = data; 
    } 

    public void setNext(Node next) {  // 下一個節點位置 
        this.next = next; 
    } 

    public Object getData() {  // 傳回節點資料 
        return data; 
    } 

    public Node getNext() {  // 傳回下一個節點位置 
        return next; 
    } 
} 
	


