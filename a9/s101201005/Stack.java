package ce1002.a9.s101201005;

public class Stack {
	private Node top;
	private String name;
	private int size;
	private int count=0;
	Stack()
	{
		top=null;
	}
	Stack(int s, String n)
	{
		top=null;
		size=s;
		name=n;
	}
	public void push(Object in)
	{
		if (count==size)
		{
			System.out.println(name+" is full");
		}
		else
		{
			Node new_node=new Node();
			new_node.set_data(in);
			new_node.set_next(top);
			top=new_node;
			count++;
			System.out.println(name+" push "+top.get_data());
		}
		}
	public void pop()
	{
		Node temp_node =top;
		if (count==0)
		{
			System.out.println("An Exception has been caught");
		}
		else
		{
			top=temp_node.get_next();
			count--;
			System.out.println(name+" pop "+temp_node.get_data());
		}
	}
}
