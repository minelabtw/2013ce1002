package ce1002.a9.s101201005;


public class Node {
	private Object data;
	private Node next;
	public void set_data(Object d)
	{
		data=d;
	}
	public Object get_data()
	{
		return data;
	}
	public void set_next(Node n)
	{
		next=n;
	}
	public Node get_next()
	{
		return next;
	}
}
