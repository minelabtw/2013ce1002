package ce1002.a9.s102502001;

import java.util.Arrays;

public class Stack {
	public int size;               //declare size,name
	public String name;
	public Object [] arr;          //declare an object array
	int count = 0;                 //declare count = 0

	Stack(int size, String name) { //get the size and name
		this.size = size;
		this.name = name;
		arr= new Object [size];    //set the size of array 
	}

	void push(Object obj) throws Exception {
		if(count==size){          //if count is equal to the size,it is full 
			System.out.println( name + " is full");
			throw new Exception();//throw new Exception
		}
		System.out.println( name + " push " + obj );
		arr[count]= obj ;
		count++;
	}

	void pop() throws Exception{
		if(count<=0){           //if count is 0,it is empty
			System.out.println( name + " is empty");
			throw new Exception();
		}
		System.out.println( name + " pop " +  arr[count-1]);
		count--;
	}

	public class Node {
		Object data;
		Node next;
	}
}
