package ce1002.a9.s102502555;

public class Stack {
	private int size;  //stack大小
	private String name;//stack名子
	private Node[] node;//裝資料的盒子
	private int counter = 0;//紀錄stack有幾層
	
	Stack(int size, String name){
		this.size = size;  //設定stack大小
		this.name = name;  //設定stack名子
		node = new Node[size];  //設定有幾個盒子
		
		//初始化盒子
		for(int i = 0 ; i < size ; i++){
			node[i] = new Node();
		}
	}
	
	public void push(Object obj){
		if(counter >= size){  //告知使用者stack滿出來
			System.out.println(name + " is full");
			throw new ArrayIndexOutOfBoundsException("Stack is full");
		} else {  //沒滿出來就放東西
			System.out.println(name + " push " +obj);
			node[counter].data = obj;
			counter++;
		}
	}
	
	public void pop(){ 
		counter--;
		if(counter < 0){  //告知使用者stack空了
			System.out.println(name + " is empty");
			throw new ArrayIndexOutOfBoundsException("Stack is empty");
		} else {  //還有東西就把最上層的東西拿出來
			System.out.println(name + " pop " + node[counter].data);
			node[counter] = null;
		}
	}
}

//裝東西的盒子
class Node { 
	Object data = new Object();
	Node next;
}
