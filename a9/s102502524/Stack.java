package ce1002.a9.s102502524;

public class Stack {

	int size;
	int Maxsize;
	String name;
	Node head;

	// some setting
	Stack(int size, String name) {
		this.Maxsize = size;
		this.name = name;
		this.size = 0;
	}

	int size() {
		return size;
	}

	String name() {
		return name;
	}

	// void push
	void push(Object data) throws Exception {
		try {
			if (full() == false) {
				size++;
				Node node = new Node();
				node.data = data;
				node.next = head;
				head = node;
				System.out.println(name + " push " + data);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	// void pop
	void pop() throws Exception {
		try {
			if (empty() == false) {
				size--;
				Node node = head;
				head = node.next;
				System.out.println(name + " pop " + node.data);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	// if stack is full, throw an exception
	boolean full() throws Exception {
		if (size == Maxsize) {
			throw new Exception(name + " is full");
		}
		return false;
	}

	// if stack is empty, throw an exception
	boolean empty() throws Exception {
		if (size == 0) {
			throw new Exception(name + " is empty");
		}
		return false;
	}
}

class Node {
	Object data;
	Node next;
}
