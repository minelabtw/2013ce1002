package ce1002.a9.s102502553;

public class Stack {

    private String stname;
    private int sizes;
    private int counter;
    private Node[] arr;//宣告Node型態的陣列
	
	public Stack(int size , String name)
	{
		sizes = size;
		stname = name;
		counter = 0;
		arr = new Node[size];
		for(int i = 0;i < size;i++)
			arr[i] = new Node();//把每一項都設為Node型態
	}
	
	public void push(Object obj) throws Exception//加入東西
	{
		if(counter != sizes)//沒有滿出來
		{
			System.out.println(stname + " push " + obj);
			arr[counter].data = obj;//把東西存進陣列的資料，如果沒加data只會顯示陣列的address
			counter++;
		}
		else//滿出來了
		{
			throw new Exception(stname + " is full");
		}
	}

	public void pop() throws Exception//排出東西
	{
		if(counter != 0)//還沒空
		{
			counter--;
			System.out.println(stname + " pop " + arr[counter].data);
			arr[counter].data = null;//把資料清空
			
		}
		else//空了
		{
			throw new Exception("An Exception has been caught");
		}	
	}
}

class Node 
{
    Object data;//Object可以放很多型態的資料
    Node next;
}
