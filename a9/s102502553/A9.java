package ce1002.a9.s102502553;

public class A9 {

	public static void main(String[] args) {
		
		Stack s1 = new Stack(5 , "Stack1");//宣告Stack的Object
		Stack s2 = new Stack(2 , "Stack2");
		
		try
		{
			s1.push("ABC");//放入
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();//拿出
			s1.pop();
			s1.pop();
			s2.push("789");//列外的情況會結束try
		}
		catch(Exception ex)//例外的情況
		{
			System.out.println(ex.getMessage());//印出在例外裡的字串
		}
		
		try
		{
			s1.pop();
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
}
