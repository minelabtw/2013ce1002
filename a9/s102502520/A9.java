package ce1002.a9.s102502520;
public class A9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Stack s1 = new Stack(5 , "Stack1"); //create two Stack
		Stack s2 = new Stack(2 , "Stack2");
		try{
			s1.push("ABC"); //use push
			s1.push("DEF");
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop(); //use pop
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();
		}
		catch (Exception ex)//output Exception
		{
			System.out.print(ex.getMessage());
		}
	}

}