package ce1002.a9.s102502520;
public class Stack {
	private Node top = null; //define many things
	private Node next =null;
	private int a= 0;
	private int size =0;
	private String name;
	private Object data;
	Stack(int size , String name){ //set size and name 
		this.size = size;
		this.name=name;
	}
	class Node //Node
	{
	    Object data;
	    Node next;
	    public void setData(Object data) { //set data
	        this.data = data; 
	    } 
	    public void setNext(Node next) { //set 節點
	        this.next = next; 
	    }
	    public Object getData() { //get data
	        return data; 
	    } 

	    public Node getNext() { // get 節點
	        return next; 
	    } 
	}
	public void push(Object data)throws Exception{ //push 
		if(a>size-1){
			System.out.println(name+" is full"); // output 
			throw new Exception("An Exception has been caught"); //Exception
		}
		Node n = new Node(); 
		n.setData(data); //set data
		n.setNext(top); //傳至頂端
		//top = n; 
		a++;
		System.out.println(name+ " push "+top.getData());//output
	}
	public void pop()throws Exception{ 
		Node tmpNode; 
        tmpNode = top; 
		if(tmpNode == null){ //if Stack is empty then output	
			System.out.println(name+ " is empty");
			throw new Exception("An Exception has been caught");
		}
		System.out.println(name+ " pop "+top.getData()); //output
		top = top.getNext(); // set top
	    tmpNode = null; // clear top
	}
}