package ce1002.a9.s102502527;

public class Stack
{
	int max;
	int number;
	String thing;
	
	Node head;//建立建構子
	Stack(int maxSize, String name)
	{
		this.max = maxSize;
		this.number = 0;
		this.thing = thing;
	}
	
	void push(Object obj) throws Exception//push的函示
	{
		if (number == max)//檢查函式是否滿
			throw new Exception(this.thing + " is full");
		number++;//增加Node
		Node node = new Node(obj);
		node.next = head;
		head = node;
		
		System.out.println(this.thing + " push " + obj);//顯示字彙
	}
	
	void pop() throws Exception//pop的函示
	{
		if (number == 0)//檢查函示是否空
			throw new Exception(this.thing + " is empty");
		number--;
		Node node = head;
		head = head.next;
		
		System.out.println(this.thing + " pop " + node.data);//顯示字彙
		node = null; //清除
	}
}
