package ce1002.a9.s102502532;

public class A9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Stack s1 = new Stack(5 , "Stack1");
		Stack s2 = new Stack(2 , "Stack2");
		
		try {
			s1.push("ABC");
			s1.push("DEF");             //可寫一起
			s1.push(1111);
			s2.push("123");
			s2.push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.push("789");
			s1.pop();               //不執行
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			System.out.println(e.getMessage());     //利用 getMessage() 印出Exception的訊息
			System.out.println("An Exception has been caught");
			//e.printStackTrace();
		}
	}

}
