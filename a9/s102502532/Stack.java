package ce1002.a9.s102502532;

public class Stack {
	
	private int Size =0;
	private String Stackname;
	private Node [] arr;            //Node
	private int n;

	public Stack(int size , String name){
		Size = size;
		Stackname = name;
		arr = new Node[Size];
		n =0;
	}
	
	public void push(Object obj) throws Exception{
		
		Node a = new Node();
		a.data = obj;
		
		if(n == Size){
			//System.out.println(Stackname + " is full");
			throw new Exception(Stackname + " is full");		//�ҥ~	
		}
		arr[n] = a;
		System.out.println(Stackname + " push " + a.data);
		n++;
	}
	
	public void pop() throws Exception{

		if( arr[n-1].data == null ){
			throw new Exception(Stackname + " is empty");       //�ҥ~
		}
		System.out.println(Stackname + " pop " + arr[n-1].data);
		
		arr[n-1].data = null;
		n--;
	}
}

class Node 
{
    Object data;
    Node next;
}
