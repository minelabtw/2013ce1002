package ce1002.a9.s102502541;
public class Stack {
	private Node temp;
	private int max;
	private String name;
	private int count = 0;
	public Stack(int size , String name)
	{
		this.name = name;
		this.max = size;
	}
	public void push(Object obj) throws Exception 
	{	
		Node n = new Node();
		if(count<max)
		{
			n.data = obj;//obj丟給data儲存
			n.next  = temp;// temp的位置給n的下一個位置
			temp = n;//把n目前的位置給temp
			System.out.println(name+" push "+ obj);
			count++;//紀錄占一格
		}	
		else
		{
			System.out.println(name+" is full");
			throw new Exception("An Exception has been caught");
		}
			
		
	}
	public void pop() throws Exception
	{
		Node npop = new Node();
		npop = temp;//把temp的位置給npop
		if(npop == null)
		{
			System.out.println(name+" is empty"); 
			throw new Exception("An Exception has been caught");//丟出exception
		}
		else
		{
			System.out.println(name+" pop "+npop.data);
		}	
			temp = npop.next;//把npop的下一個位置給temp
			count--;//紀錄少一格
	}
}
class Node 
{
    Object data;
    Node next;
}