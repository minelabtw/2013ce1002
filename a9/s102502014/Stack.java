package ce1002.a9.s102502014;

public class Stack {
	private int size1;// STACK大小
	private String name1;// STACK名稱
	private int count = 0;// STACK目前數量
	public Node top;// 最頂端

	public Stack(int size, String name) {
		size1 = size;
		name1 = name;
	}
	public void push(Object obj) {
		try {
		if (count != size1) {// 沒超過就PUSH
		Node node = new Node();// 新的TOP
		node.data = obj;// 存資料
		add(node);// 把node變top top變top.next
		System.out.println(name1 + " push " + obj);
		} else {
		System.out.println(name1 + " is full");
		throw new Exception();//丟出例外
		}
		} catch (Exception ex) {//捕捉例外
		System.out.println("An Exception has been caught");
		System.exit(0);//終止程式
		}
		}
	public void add(Node node) {
		count = count + 1;    //數量多一
		node.next = top;// top移到下面
		top = node;// 這次的node變新的TOP
	}
	public void pop() {
		try {// 不等於0就提出
		Node a = top;// 暫存node
		top = top.next;// TOP往前移
		System.out.println(name1 + " pop " + a.data);
		} catch (Exception ex) {//捕捉例外
		System.out.println(name1 + " is emtpy");
		System.out.println("An Exception has been caught");
		System.exit(0);//終止程式
		}
	}
}
class Node {
	Object data;// 存資料
	Node next;// 下一個NODE
}