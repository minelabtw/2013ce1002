package ce1002.a9.s102502022;

public class Stack {

	
	int size;
	String name;
	int fullsize;
	Node head;
	
	Stack(int size,String name){
		this.size = 0;//傳入大小初始化值為0
		this.name = name;
		this.fullsize = size;
	}
	
	public void push(Object data)throws Exception{
		try{
			if(!matchfull()){
				size++;//大小+1
				Node node = new Node();//先建一個node
				node.data = data;//node的資料
			    node.next = head;//node指向頭
			    head = node;
				System.out.println(name + " push " + node.data);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	public void pop()throws Exception{
		try{
			if(!matchempty()){
				size--;//大小-1
				Node node = head;
				head = node.next;
				System.out.println(name + " pop " + node.data);
			}
		}catch(Exception e){
			throw e;
		}
	}
	
	public boolean matchfull()throws Exception{
		if(size == fullsize){
			throw new Exception(name + " is full");
		}
		return false;
	}
	
	public boolean matchempty()throws Exception{
		if(size == 0){
			throw new Exception(name + " is empty");
		}
		return false;
	}
	
	class Node 
	{
	    Object data;
	    Node next;
	}

}
