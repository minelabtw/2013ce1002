package ce1002.a9.s102502558;

public class Stack
{
	// variables
	int maxSize;
	int size;
	String name;
	
	Node head;
	// constructor
	Stack(int maxSize, String name)
	{
		this.maxSize = maxSize;
		this.size = 0;
		this.name = name;
	}
	
	// a function called push... I think it push something
	void push(Object obj) throws Exception
	{
		// check full
		if (size == maxSize)
			throw new Exception(this.name + " is full");
		
		// add node
		size++;
		Node tmp = new Node(obj);
		tmp.next = head;
		head = tmp;
		
		// print debug message
		System.out.println(this.name + " push " + obj);
	}
	
	// a function called pop... I think it pop something
	void pop() throws Exception
	{
		// check empty
		if (size == 0)
			throw new Exception(this.name + " is empty");
		
		// remove node
		size--;
		Node tmp = head;
		head = head.next;
		
		// print debug message
		System.out.println(this.name + " pop " + tmp.data);
		tmp = null; // clear the tmp
	}
}
