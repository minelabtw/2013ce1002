package ce1002.a9.s102502011;

public class Stack {
	
	class Node 
	{
	    Object data;
	    Node next;
	    Node(Object data) { //set Node
	    	this.data = data ;
	    	this.next = null ;
	    }
	}
	
	private Node head ; //private
	private int x ;
	private String name ;
	
	Stack(int x , String name) {
		this.x = x ;
		this.name = name ;
		head=new Node(null);
	}
	
	public void push(Object thing) throws Exception { //push function
		
		int y = 0 ;
		Node cur=head;
		while(cur.next!=null) { //set cur
			cur=cur.next;
			y++;
		}
			
		if(y==x) {
			System.out.println(name +" is full") ;
			throw new Exception("An Exception has been caught");
			
		}
		else {
			System.out.println(this.name + " push " + thing ) ;
			Node newnode = new Node(thing) ; //use newnode let cur.next letter it
			cur.next = newnode ;
		}
	}
	
	public Object pop() throws Exception  { //pop function
		int y=0 ;
		Node cur = head ;
		Node pre = null;
		
		while(cur.next!=null) { //turn a round 
			pre=cur;
			cur=cur.next;
			y++;
		}
		
		if (y==0) 
			throw new Exception("An Exception has been caught");
		else {
			System.out.println(name + " pop " + cur.data);
			pre.next = null;
			return null;
		}
	}

}
