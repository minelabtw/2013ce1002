package ce1002.a9.s102502547;

public class Stack {

	int size; //max size
	int h = 0; //現在大小
	String name;
	Node top = null;

	Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	public void push(Object data) throws Exception {
		if (h == size) {
			throw new Exception(name + " is full"); //例外
		}
		
		h++;
		Node newNode = new Node(data);
		newNode.next = top;
		top = newNode;
		System.out.println(name + " push " + data);
	}

	void pop() throws Exception {
		if (h == 0) {
			throw new Exception(name + " is empty"); //例外
		}
		
		h--;
		Node newNode = top;
		top = top.next;
		System.out.println(name + " pop " + newNode.data);
		newNode = null;
	}

}
