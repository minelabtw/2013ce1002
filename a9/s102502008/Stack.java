package ce1002.a9.s102502008;

public class Stack 
{
	 	private Node head;
	    private int size;
	    private int index ;
	    private String name;
	    public Stack(int size , String name)
	    {
	    	this.size= size ;
	    	this.name= name ;
	    }
	    class Node 
	    {
	        Object data;
	        Node next;
	    }
	    public void push(Object s) throws Exception{
	        if(index==size)
	        {
	        	System.out.println(name+" is full") ;
	        	throw new Exception("An Exception has been caught") ;
	        }
	    	Node tmp = new Node();
	        tmp.next = head;
	        tmp.data = s;
	        index++;
	        head = tmp;
	        System.out.println(name+" push "+s) ;
	    }
	    public void pop() throws Exception {
	        if (head == null) {
	            throw new Exception("An Exception has been caught");
	        }
	        Object tmp = head.data;
	        head = head.next;
	        index--;
	        System.out.println(name+" pop " +tmp);
	    }
}
	
	

