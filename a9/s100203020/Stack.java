package ce1002.a9.s100203020;

public class Stack {
	int size;
	int top=-1;
	String name;
	Node[] node;
	
	//constructor
	Stack(int size , String name){
		this.name = name;
		this.size = size;
		buildList();
	}
	
	//build list for stack using
	
	private void buildList(){
		node = new Node[size];
		for(int i=0; i < size; i++){
			node[i]= new Node();
		}
		for(int i=0; i < size-1; i++){
			node[i].next = node[i+1];
		}
		node[size-1].next = null;
	}
	
	//push
	void push(Object obj) throws Exception{
		if(top == size-1)
			throw new Exception(this.name+" is full\n" + "An Exception has been caught");
		top++;
		node[top].data = obj;
		System.out.println(this.name + " push " + obj);
	}
	//pop
	void pop()throws Exception{
		if(top==-1)
			throw new Exception("An Exception has been caught");
		System.out.println(this.name + " pop " + node[top].data);
		node[top].data = null;
		top--;
	}
	
	
}
// node
class Node{
	Object data;
	Node next;
}
