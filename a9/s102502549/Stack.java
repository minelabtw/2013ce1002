package ce1002.a9.s102502549;

//LinkedList的節點
class Node {
	Object data;
	Node next;
}

//此stack的方法根據計概課本實作，所以沒有計實課本中的peek方法，絕對不是我懶的做
public class Stack {

	// 堆疊的名字，大小，當前大小，以及LinkedList的頭指標(Java沒指標好不方便，用了個空data的node代替)
	private String name;
	private int size;
	private int current_size = 0;
	private Node headnode = new Node();

	// 在建構式中設定好屬性
	public Stack(int size, String name) {
		this.size = size;
		this.name = name;
	}

	// 這裡也用了個cur當指標記錄最後一個node
	public void Push(Object o) throws Exception {

		if (current_size >= size) {
			System.out.println(name + " is full");
			throw new Exception();
		} else {

			//建立新節點
			Node newNode = new Node();
			newNode.data = o;

			Node cur = headnode;

			// 將cur移到最後一個node
			for (int i = 0; i < current_size; i++) {
				cur = cur.next;
			}

			cur.next = newNode;
			current_size++;

			System.out.println(name + " push " + o);
		}
	}

	// 跟push差不多，只是cur改成移到倒數第二個node
	public Object pop() throws Exception {
		if (current_size == 0) {
			System.out.println(getname() + " is empty");
			throw new Exception();
		} else {
			Node cur = headnode;

			for (int i = 0; i < current_size - 1; i++) {
				cur = cur.next;
			}

			Object o = cur.next.data;
			cur.next = null;//斷開魂結，斷開一切的牽連
			current_size--;

			System.out.println(name + " pop " + o);

			return o;
		}
	}
	
	//stack的三大方法之一
	public boolean empty()
	{
		if(current_size==0)
			return true;
		else 
			return false;
	}

	// 一些getter
	public String getname() {
		return name;
	}

	public int getsize() {
		return size;
	}

	public int getcurrent_size() {
		return current_size;
	}
}
