package ce1002.a9.s102502549;

public class A9 {

	public static void main(String[] args) {

		Stack s1 = new Stack(5, "Stack1");
		Stack s2 = new Stack(2, "Stack2");

		// 讓我們踹踹看，還測試了empty方法
		try {
			System.out.println(s1.getname()+" is empty? "+s1.empty());
			s1.Push("ABC");
			System.out.println(s1.getname()+" is empty? "+s1.empty());
			s1.Push("DEF");
			s1.Push(1111);
			System.out.println(s2.getname()+" is empty? "+s2.empty());
			s2.Push("123");
			System.out.println(s2.getname()+" is empty? "+s2.empty());
			s2.Push("456");
			s1.pop();
			s1.pop();
			s1.pop();
			s2.Push("789");
			s1.pop();
		} catch (Exception e) {
			// 為了方便追蹤，把例外路徑印出來
			System.out.println("An Exception has been caught");
			e.printStackTrace();
		}
	}
}
