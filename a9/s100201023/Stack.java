package ce1002.a9.s100201023;

public class Stack
{
	//properties
	String name;
	Object value[];
	int nowsize , maxsize;
	
	//constructure
	public Stack(int size , String n)
	{
		name = n;
		value = new Object[size];
		maxsize = size;
		nowsize = 0;
	}
	
	//method
	public String getname()
	{
		return name;
	}
	
	public void push(Object in) throws Exception
	{
		//check if stack is full
		if(nowsize == maxsize)
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		
		//push stack
		value[nowsize] = in;
		System.out.println(name + " push " + in);
		++nowsize;
	}
	
	public void pop() throws Exception
	{
		//check if stack is empty
		if(isempty())
		{
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		}
		
		//pop stack
		--nowsize;
		System.out.println(name + " pop " + value[nowsize]);
	}
	
	public boolean isempty()
	{
		if(nowsize == 0)
			return true;
		else
			return false;
	}
}
