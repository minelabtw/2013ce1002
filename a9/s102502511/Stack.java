package ce1002.a9.s102502511;

public class Stack {
	
	private int size;
	private Node head;
	private String name;
	private int number = 0;
	
	class Node {
        Object data; //每個內容都稱為data
        Node next; //連結的指向叫next
	}
	
	Stack(int size , String name){
		this.size = size;
		this.name = name;
	}
	
	public void push(Object obj){
		Node nob = new Node();
		if(number == size){
			System.out.println(name + " is full");
		}else{
			nob.next = head; //head為nob的指向
			nob.data = obj; //將內容傳至data中
			number++; //將記錄往下一個
			head = nob; //將nob設為頂部
			System.out.println(name + " push " + head.data);
		}
	}
	
	public void pop(){
		if (head == null) {
            System.out.println("An Exception has been caught");
        }else{
        	Object nob = head.data; //將頂部data傳給nob
        	System.out.println(name + " pop " + nob);  
        	head = head.next; //指向給後一個數
        	size--;
        }
	}
}
