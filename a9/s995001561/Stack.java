package ce1002.a9.s995001561;

public class Stack {
	
	// create variables
	private String name;
	private int space;
	private Object[] stacks;
	private int counter = 0;
	
	// stack constructor
	Stack(int a, String b){
		
	this.setName(b);
	this.setSpace(a);
	
	stacks = new Object[space];
		
	}
	
	// pop function
	void pop() throws Exception{
		
		if(counter!=0)
		{
		System.out.println(name + " pop " + stacks[counter-1]);
		stacks[counter-1] = null;
		counter--;
		
		}
		else {
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		}
		
	}
	
	// push function
	void push(Object a) throws Exception{
		
		if(counter<space)
		{
		stacks[counter] = a;
		counter++;
		System.out.println(name + " push " + a);
		}
		else {
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
	}
	
	
	// no use
	class Node 
	{
	    Object data;
	    
	}

	
	// getter and setter
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpace() {
		return space;
	}

	public void setSpace(int space) {
		this.space = space;
	};

}
