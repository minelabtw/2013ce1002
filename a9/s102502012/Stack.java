package ce1002.a9.s102502012;

public class Stack {
	class Node{
		private Object data;
		private Node next;
		
		Node(Object data){	this.data = data;	next = null;	}
	}
	
	private Node head;
	private String name;
	private int size;
	
	public Stack(int size, String name){
		this.size = size;
		this.name = name;
		head = new Node(null);
	}
	
	public void push(Object obj) throws Exception{
		int count = 0;
		Node ptr = head;
		Node newNode = new Node(obj);
		while(ptr.next != null){	ptr = ptr.next;	count++;	}
		if(count == size){
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		else{
			System.out.println(name + " push " + obj);
			ptr.next = newNode; // set last node's next as newNode
		}
		
	}
	
	public Object pop() throws Exception{
		Node prev = null;
		Node ptr = head;
		while(ptr.next != null){	prev = ptr;	ptr = ptr.next;	}	
		
		if(ptr == head){
			System.out.println(name + " is empty");
			throw new Exception("An Exception has been caught");
		}
		else{
			System.out.println(name + " pop " + ptr.data);
			prev.next = null; // set previous one's next as null
			return ptr.data;
		}
		
	}
}
