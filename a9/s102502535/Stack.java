package ce1002.a9.s102502535;

public class Stack {

	Object[] data;
	int top;
	int max;
	String name;

	// set stack's data
	Stack(int size, String name) {
		data = new Object[size];
		top = -1;
		max = size;
		this.name = name;
	}

	// push objects into data array and check if the stack is full
	void push(Object obj) {
		try {
			data[++top] = obj;
			System.out.println(name + " push " + data[top]);

			if (top >= max) {
				throw new Exception();
			}
		} catch (Exception e) {
			System.out.println(name + " is full");
			System.out.println("An Exception has been caught");
			System.exit(0);
		}
	}

	// pop objects out of data array and check if the stack is empty
	void pop() {
		try {
			System.out.println(name + " pop " + data[top]);
			top = top - 1;

			if (top < -1) {
				throw new Exception();
			}
		} catch (Exception e) {
			System.out.println(name + " is empty");
			System.out.println("An Exception has been caught");
			System.exit(0);
		}
	}

}
