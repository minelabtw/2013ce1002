package ce1002.a9.s102502025;

public class Stack {
	private int Number = 0;// the sequence
	private String StackName;// output stackname
	private int MaxSize;
	private Node top;

	public Stack(int i, String string) {
		MaxSize = i;
		StackName = string;
	}

	public void push(Object o) throws Exception {
		if (Number == MaxSize) { // if number == maxsize
			System.out.println(StackName + " is full");
			throw new Exception("An Exception has been caught");
		}
		Node node = new Node();
		node.next = top; // 位置
		node.data = o; // 拿一個值然後輸出去node
		Number++;
		top = node;
		System.out.println(StackName + " push " + o);
	}

	public void pop() throws Exception {
		if (top == null) { // if no data
			throw new Exception("An Exception has been caught");
		}
		Object o = top.data; // the data of the save it space data
		top = top.next; // return the previous space;
		Number--;
		System.out.println(StackName + " pop " + o);
	}

	class Node {
		Object data;
		Node next;
	}

}
