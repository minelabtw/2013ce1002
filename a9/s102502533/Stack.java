package ce1002.a9.s102502533;
import java.util.ArrayList;
public class Stack {
	private ArrayList<Object> list = new ArrayList<>();//建一個物件陣列
	private static int size = 0;
	private String name;

	public static boolean check(int sizes) throws Exception {//push的exception
		if (sizes > size  ) {
			throw new Exception("more than size");
		}
		return false;
	}
	public static boolean checks(int sizes) throws Exception {//pop's exception
		if (sizes < 0) {
			throw new Exception("less than size");
		}
		return false;
	}
	Stack(int size, String name) {//Stacks's constructor to set name and size
		this.size = size;
		getname(name);
	}

	String getname(String names) {//get name
		this.name = names;
		return this.name;
	}

	public int getSize() {//使用內建函式計算陣列大小
		return list.size();
	}

	public void push(Object obj) {//把傳進來的直放進陣列
		try {
			list.add(obj);
			check(getSize());
			System.out.println(this.name + " Push " + obj);
		} catch (Exception ex) {
			System.out.println(this.name + " is full");
		}
	}

	public void pop() {//將值傳出去並把它移除
		try {
			checks(getSize() - 1);
			Object o = list.get(getSize() - 1);
			System.out.println(this.name + " Pop " + o);
			list.remove(getSize() - 1);
		} catch (Exception ex) {
			System.out.println("An Exception has been caught");
		}
	}
}
