package ce1002.a9.s995002046;

import java.util.ArrayList;
import java.util.List;

class Stack{
	List<Node>nodes=new ArrayList<Node>();
	String name;
	Stack(int size, String name){//set node and name
		for(int i=0; i<=size-1 ; i++){
			nodes.add(new Node());
		}
		this.name=name;
	}
	void pop() throws Exception{//pop stack if it's empty throw an exception
		for(int i=nodes.size()-1;i>=0;i--){
			if(nodes.get(i).getData()!=null){
				System.out.println(name+" pop "+nodes.get(i).getData().toString());
				nodes.get(i).removeData();
				break;
			}
			if(i==0 && nodes.get(0).getData()==null){
				System.out.println(name+" is empty");
				throw new Exception("An Exception has been caught");
			}				
		}
	}
	void push(Object data) throws Exception{//push stack if it's full throw an exception
		for(Node n:nodes){
			if(n.getData()==null){
				n.setData(data);
				System.out.println(name+" push "+data.toString());
				break;							
			}
			if(n.equals(nodes.get(nodes.size()-1)) && n.getData()!=null){
				System.out.println(name+" is full");
				throw new Exception("An Exception has been caught"); 
			}
		}
	}
	class Node 
	{//store data 
	    Object data;
	    Node next;
	    void setData(Object data){//set data
	    	this.data=data;
	    }
	    Object getData(){//get data
	    	return data;
	    }
	    void removeData(){//remove data
	    	data=null;
	    }
	}
}