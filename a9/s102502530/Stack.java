package ce1002.a9.s102502530 ;

public class Stack
{
   private int size ;
   private String name ;
   private int num ;
   private Node top = null ;

   public Stack(int size, String name)
   {
      this.size = size ;
      this.name = name ;
      this.num = 0 ;
      this.top = null ;
   }

   public void push(Object obj) throws Exception
   {
      if(num == size)
         throw new Exception(name + " is full") ;
      System.out.println(name + " push " + obj) ;
      Node temp = new Node() ;
      temp.data = obj ;
      temp.next = top ;
      top = temp ;
      num++ ;
   }

   public void pop() throws Exception
   {
      if(num == 0)
         throw new Exception(name + " is empty") ;
      System.out.println(name + " pop " + top.data) ;
      top = top.next ;
      num-- ;
   }

}

class Node
{
   Object data ;
   Node next ;
}
