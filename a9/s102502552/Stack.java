package ce1002.a9.s102502552;
public class Stack {
	private String name;
	private int size;
	private int counter;
	private Node[] stack;//名字，大小，計數器，以及堆疊本體陣列
	
	public Stack(int size,String name)
	{
		this.size = size;
		this.name = name;
		counter = 0;
		stack = new Node[this.size];
		
		for(int i = 0;i < size;i++)
		{
			stack[i] = new Node();
		}
	}//建構式直接設定名字，大小，建立堆疊物件，以及歸零計數器
	
	public void push(Object obj) throws Exception
	{                                                                                                                
		if(counter != size)
		{
			System.out.println(name + " push " + obj);
			stack[counter].data = obj;
			counter++;
		}//當堆疊未滿，加入信息
		else
		{
			throw new Exception(name + " is full");//堆疊已滿，拋出例外
		}		
	}
	
	public void pop() throws Exception
	{
		if(counter != 0)
		{
			counter--;
			System.out.println(name + " pop " + stack[counter].data);
			stack[counter].data = null;
		}//堆疊不為空，刪去頂層信息
		else
			throw new Exception(name + " is empty");//底層為空，拋出例外
	}
}
class Node{
	Object data;
	Node next;
}
