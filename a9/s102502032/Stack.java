package ce1002.a9.s102502032;

public class Stack
{
	protected int		size;
	protected String	name;
	protected Node		node[];
	private int			ptr	= 0;

	Stack(int size, String name)
	{
		this.size = size;
		this.name = name;
		this.node = new Node[size];
		for (int i = 0; i < size; i ++)
		{
			this.node[i] = new Node();
		}
	}

	public void push(Object obj)
	{
		if (ptr < size && ptr >= 0)
		{
			System.out.println(this.name + " push " + obj.toString());
			node[ptr].data = obj;
			ptr ++;
		}
		else
		{
			System.out.println("Stack2 is full");
			System.out.println("An Exception has been caught");
			System.exit(-1);
		}
	}

	public Object pop()
	{
		if (ptr <= size && ptr > 0)
		{
			ptr --;
			System.out.println(this.name + " pop " + node[ptr].data.toString());
			return node[ptr + 1].data;
		}
		else
		{
			System.out.println("Stack x is empty");
			System.out.println("An Exception has been caught");
			System.exit(-1);
		}
		return 0;
	}
}
