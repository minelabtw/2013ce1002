package ce1002.a9.s102502020;

public class Stack {
	private int i = 0;
	private int size = 0;
	private String name = "";
	private Node n = null;
	
	public Stack(int size , String name){                               //堆疊的大小與名稱
		this.size = size;
		this.name = name;
	}
	class Node                                                          //節點
	{
	    Object data;
	    Node next;
	}
	public void push(Object obj) throws Exception{                      //把資料存在堆疊理
		if(i>=size){
			throw new Exception(name + " is full");                     //堆疊以經存滿
		}
		else{
			Node newnode = new Node();
			newnode.data = obj;
			newnode.next = n;
			n = newnode;
			i++;
			System.out.println(name + ".push " + newnode.data);
		}
	}
	public void pop() throws Exception{                                 //輸出堆疊最上層的資料
		if(i<=0){
			throw new Exception("An Exception has been caught");        //堆疊以空
		}
		else{
			Node topnode = new Node();
			topnode = n;
			System.out.println(name + ".pop " + topnode.data);
			n = n.next;
			i--;
		}
		
	}

	
	

}

