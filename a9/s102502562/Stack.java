package ce1002.a9.s102502562;


public class Stack {
	int size;
	int maxsize;
	String name;
	
	Node head;
	
	Stack(int maxsize,String name)
	{
		this.maxsize=maxsize;
		this.size=0;
		this.name=name;
	}
	void push(Object data)throws Exception//push的function
	{
		if (size==maxsize)//檢查輸入是否超過最大值
			throw new Exception(this.name + " is full");
		size++;//新增node
		Node node=new Node(data);
		node.next=head;
		head=node;
		System.out.println(this.name + " push " + data);
	}
	void pop()throws Exception//pop的function
	{
		if (size==0)//檢查size是否已經到最下面
			throw new Exception(this.name + " is empty");
		size--;//刪除node
		Node node=head;
		head=node.next;
		System.out.println(this.name + " pop " + node.data);
		node=null;//清除值
	}
	class Node//Node的class
	{
	    Object data;
	    Node next;
	    Node(Object data)
		{
			this.data=data;
		}
	}
}
