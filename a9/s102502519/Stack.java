package ce1002.a9.s102502519;

public class Stack {
	class Node 
	{
	    Object data;
	    Node next;
	    Node(Object data) {     //設定Node
	    	this.data = data ;
	    	this.next = null ;
	    }
	}
	
	private Node head ; 
	private int x ;
	private String name ;
	
	Stack(int x , String name) {
		this.x = x ;
		this.name = name ;
		head=new Node(null);
	}
	
	public void push(Object thing) throws Exception {     //將物件丟入或是找不到輸出例外
		
		int y = 0 ;
		Node cur=head;
		while(cur.next!=null) { 
			cur=cur.next;
			y++;
		}
			
		if(y==x) {
			System.out.println(name +" is full") ;
			throw new Exception("An Exception has been caught");
			
		}
		else {
			System.out.println(this.name + " push " + thing ) ;
			Node newnode = new Node(thing) ;
			cur.next = newnode ;
		}
	}
	
	public Object pop() throws Exception  {     //將最上的輸出或是找不到輸出例外
		int y=0 ;
		Node cur = head ;
		Node pre = null;
		
		while(cur.next!=null) {  
			pre=cur;
			cur=cur.next;
			y++;
		}
		
		if (y==0) 
			throw new Exception("An Exception has been caught");
		else {
			System.out.println(name + " pop " + cur.data);
			pre.next = null;
			return null;
		}
	}

}
