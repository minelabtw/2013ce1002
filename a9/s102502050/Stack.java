package ce1002.a9.s102502050;

public class Stack {
	private String name;
	private Node start;//node的起點，不存資料
	private Node currentnode;//堆疊的頂端
	private int maximum;//堆疊最大值
	private int now;//堆疊中node的數量
	public Stack(int number,String stackname)
	{
		start = new Node();
		currentnode =start;
		name=stackname;
		maximum=number;
		now=0;
	}
	
	//push字串
	public void push(String s) throws Exception
	{
		if(now==maximum)//堆疊已經滿了
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		else
		{
			//建立下一個node
			currentnode.Setnext(new Node());
			currentnode = currentnode.Getnextnode();
			currentnode.Setdata(s);
			
			System.out.println(name + " push " +currentnode.Getstringdata());
			now++;
		}
	}
	
	//push 整數
	public void push(int number) throws Exception
	{
		if(now==maximum)
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		else
		{
			currentnode.Setnext(new Node());
			currentnode = currentnode.Getnextnode();
			currentnode.Setdata(number);
			
			System.out.println(name + " push " +currentnode.Getnumberdata() );
			now++;
		}
	}
	
	//從堆疊pop
	public void pop() throws Exception
	{
		if(now==0)//堆疊為空
		{
			System.out.println(name + " is full");
			throw new Exception("An Exception has been caught");
		}
		else
		{
			if(currentnode.Getstringdata()== null)//pop整數
				System.out.println(name + " pop " +currentnode.Getnumberdata() );
			else							     //pop字串		
				System.out.println(name + " pop " +currentnode.Getstringdata());
			now--;
			
			
			currentnode = start;
			for(int i=0;i<now;i++)//重新設定currentnode
			{
				currentnode = currentnode.Getnextnode();
			}
			currentnode.Setnull();	
		}
	}

}
