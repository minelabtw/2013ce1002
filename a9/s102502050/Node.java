package ce1002.a9.s102502050;

public class Node {
	private int numberdata;
	private String stringdata;
	private Node nextnode;
	
	public Node()
	{
		stringdata=null;
		nextnode=null;
	}
	
	//set
	public void Setdata(int number)
	{
		numberdata = number;
	}
	public void Setdata(String s)
	{
		stringdata =s;
	}
	public void Setnext(Node next)
	{
		nextnode=next;
	}
	public void Setnull()
	{
		nextnode=null;
	}
	
	//get
	public int Getnumberdata()
	{
		return numberdata;
	}
	public String Getstringdata()
	{
		return stringdata;
	}
	public Node Getnextnode()
	{
		return nextnode;
	}
	
	
}
