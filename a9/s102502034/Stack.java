package ce1002.a9.s102502034;

public class Stack {
	String stackName;
	int stackSize;
	int itemCount;
	Node sp;
	//dim variables

	Stack(int size, String name) {
		stackName = name;
		stackSize = size;
		itemCount = 0;
		sp = null;
	}
	//set name and size

	void push(Object o) {
		Node t=(itemCount>=stackSize)? null:new Node();

		try {
			t.data = o;
			t.next = sp;
			sp = t;
			itemCount++;
			System.out.println(stackName + " push " + o);
			// push an item into stack
		} catch (Exception e) {
			System.out.println(stackName + " is full");
			System.out.println( "An Exception has been caught");
			System.exit(1);
		}
		// exception handling

	}

	void pop() {

		try {
			Object o = sp.data;
			System.out.println(stackName + " pop " + o);
			sp = sp.next;
			itemCount--;
			// pop an item out of stack
		} catch (Exception e) {
			System.out.println(stackName + " is empty");
			System.out.println( "An Exception has been caught");
			System.exit(1);
		}
		//exception handling . when there's no item in the stack , can't do the push
	}

	class Node {
		Object data;
		Node next;
	}
	// set node

}
