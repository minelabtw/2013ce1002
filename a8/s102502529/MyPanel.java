package ce1002.a8.s102502529;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;
import java.util.Set;

public class MyPanel extends JPanel{
	private float p=0;
	public MyPanel(float f,int level) {							//拿來做資料的存取與排版
		// TODO Auto-generated constructor stub
		setBounds(20, 20, 100, 300);
		this.setBorder(new LineBorder(Color.black, 5));
		setLayout(null);
		JLabel levelinfo=new JLabel("Level "+level);
		levelinfo.setBounds(30, 220, 200, 20);
		JLabel info=new JLabel(""+f*100+"%");
		info.setBounds(40, 250, 200, 20);
		add(levelinfo);
		add(info);
		p=f;													//建構子傳來的資料丟入區域變數
	}
	public void paintComponent(Graphics grap){
			grap.setColor(Color.blue);
			grap.fillRect(40, 20, 20, 200);		
			grap.setColor(Color.red);	
			grap.fillRect(40, 220-(int)(200*p), 20,(int)(200*p));	//畫圖
	}
	
}
