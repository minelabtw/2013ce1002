package ce1002.a8.s102502529;

import javax.swing.*;
public class MyFrame extends JFrame{
	public MyFrame(float p[]) {
		// TODO Auto-generated constructor stub
		setSize(600,400);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		MyPanel  panel[] = new MyPanel[4];								//宣告物件陣列
		for(int i=0;i<4;i++){
		panel[i]=new MyPanel(p[i],i);									//實體化
		panel[i].setBounds(20+150*i, 20, 100, 300);						//設定座標
		add(panel[i]);
		}
		setVisible(true);
		
	}
}
