package ce1002.a8.s102502529;
import java.util.Scanner;
import java.util.logging.Level;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s =new Scanner(System.in);
		int [] grade= new int [8];
		int []level={0,0,0,0};
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){											//判斷測資
			grade[i]=s.nextInt();
			while(grade[i]<0||grade[i]>100){
				System.out.println("Out of range!");
				grade[i]=s.nextInt();
			}
			if(0<=grade[i]&&grade[i]<=25){								//計算所屬哪個level
				level[0]++;}
			if(26<=grade[i]&&grade[i]<=50){
				level[1]++;}
			if(51<=grade[i]&&grade[i]<=75){
				level[2]++;}
			if(76<=grade[i]&&grade[i]<=100){
				level[3]++;}
		}
		for(int i =0 ; i<4;i++){
			System.out.println("Level "+i+" is "+((float)level[i]/8)*100+"%");
		}
		s.close();
		float p[]={(float)level[0]/8,(float)level[1]/8,(float)level[2]/8,(float)level[3]/8};
		new MyFrame(p);													//將資料的陣列傳入建構子
		

	}

}
