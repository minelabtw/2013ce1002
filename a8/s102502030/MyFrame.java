package ce1002.a8.s102502030;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel panel;
	
	public MyFrame( double[] x ) {
		//設定Frame資料
		setLayout( null );
		setBounds( 0, 0, 600, 600 );
		setVisible( true );
		setDefaultCloseOperation( EXIT_ON_CLOSE );
		panel = new MyPanel( x );
		panel.setText();
		add( panel );
	}
}
