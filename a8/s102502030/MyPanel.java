package ce1002.a8.s102502030;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	
	protected JLabel data1 = new JLabel();
	protected JLabel data2 = new JLabel();
	protected JLabel data3 = new JLabel();
	protected JLabel data4 = new JLabel();
	protected int[] intLevel = new int[4];
	protected double[] level = new double[4];
	
	public MyPanel( double[] x ) {
		//設定Panel資料
		setLayout( null );
		setBounds( 10, 10, 500, 500 );
		level = x;
		for( int i=0; i<4; i++ ) {
			intLevel[i] = (int)x[i];
		}
		data1.setBounds( 0, 40, 200, 30 );
		data2.setBounds( 0, 160, 200, 30 );
		data3.setBounds( 0, 280, 200, 30 );
		data4.setBounds( 0, 400, 200, 30 );
		add( data1 );
		add( data2 );
		add( data3 );
		add( data4 );
	}
	
	public void paintComponent( Graphics g ) {
		super.paintComponents( g );
		
		//塗黑長條
		g.setColor( Color.black );
		g.fillRect( 0, 0, 400, 20);
		g.fillRect( 0, 120, 400, 20);
		g.fillRect( 0, 240, 400, 20);
		g.fillRect( 0, 360, 400, 20);
		//塗紅長條
		g.setColor( Color.red );
		g.fillRect( 0, 0, intLevel[0]*50, 20);
		g.fillRect( 0, 120, intLevel[1]*50, 20);
		g.fillRect( 0, 240, intLevel[2]*50, 20);
		g.fillRect( 0, 360, intLevel[3]*50, 20);
	}

	public void setText() {
		//設定文字顯示
		data1.setText( "Level 0    " + level[0]*100/8 + "%" );
		data2.setText( "Level 1    " + level[1]*100/8 + "%" );
		data3.setText( "Level 2    " + level[2]*100/8 + "%" );
		data4.setText( "Level 3    " + level[3]*100/8 + "%" );
	}
}
