package ce1002.a8.s102502030;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] grade = new int[8];
		double[] level = { 0, 0, 0, 0 };
		
		Scanner input = new Scanner( System.in );
		//輸入成績並檢查錯誤
		System.out.println( "Input grades: " );
		for( int i=0; i<8; i++ ) {
			do {
				grade[ i ] = input.nextInt();
				if( grade[ i ]<0 || grade[ i ]>100 ) {
					System.out.println( "Out of range!" );
				}
			} while( grade[ i ]<0 || grade[ i ]>100 );
		}
		//分級
		for( int i=0; i<8; i++ ) {
			if( grade[ i ]>=0 && grade[ i ]<=25 )
				level[0]++;
			else if( grade[ i ]>25 && grade[ i ]<=50 )
				level[1]++;
			else if( grade[ i ]>50 && grade[ i ]<=75 )
				level[2]++;
			else if( grade[ i ]>75 && grade[ i ]<=100 )
				level[3]++;
		}
		
		//計算比率
		for( int i=0; i<4; i++)  {
			System.out.println( "Level " + i + " is " + level[i]*100/8 + "%" );
		}
		MyFrame frame = new MyFrame( level );
	}

}
