package ce1002.a8.s102502026;

import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class A8 {
	private static Scanner input;
	// make level[] be global for MyPanel Use
	public static double level[] = new double[4];

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		input = new Scanner(System.in);
		int grade = 0; // grade
		int c = 0; // counter

		System.out.println("Input grades:");
		do {
			grade = input.nextInt(); // ask grade
			if (grade < 0 || grade > 100) {
				System.out.println("Out of range!"); // error for <0 >100
			}
			if (grade > -1 && grade < 101) { // for counter to 8
				c++;
			}
			if (grade < 26 && grade > -1) {
				level[0]++; // level 0
			} else if (grade > 25 && grade < 51) {
				level[1]++; // level 1
			} else if (grade > 50 && grade < 76) {
				level[2]++; // level 2
			} else if (grade > 75 && grade < 101) {
				level[3]++; // level 3
			}
		} while (c < 8); // do it 8 times

		// output answer
		for (int i = 0; i < 4; i++) {
			level[i] = (level[i] / 8) * 100;
			System.out.println("Level " + i + " is " + level[i] + "%");
		}

		// for the frame
		MyFrame frame = new MyFrame();
		frame.setSize(300, 200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		input.close(); // close input
	}
}
