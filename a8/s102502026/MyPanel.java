package ce1002.a8.s102502026;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	public MyPanel() {
		setLayout(null); // set layout of the panel
		setBounds(10, 10, 200, 200);	//Bounds of panel
	}

	public void paint(Graphics g) {
		g.setFont(new Font("default", Font.BOLD, 12)); // for font of strings
		int x = 0; // for x axis
		// for graph
		for (int i = 0; i < 4; i++) {
			g.setColor(Color.red);
			g.drawRect(x, 0, 10, 100);
			g.setColor(Color.gray);
			g.fillRect(x, 0, 10, 100);
			g.setColor(Color.red);
			g.fillRect(x, 100 - (int) A8.level[i], 10, (int) A8.level[i]);
			g.setColor(Color.DARK_GRAY);
			g.drawString("Level " + i, x, 115);
			g.drawString(String.valueOf(A8.level[i]) + "%", x, 135);
			x = x + 50;
		}
	}
}