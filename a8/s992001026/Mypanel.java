package ce1002.a8.s992001026;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.*;
import java.awt.*;

import java.awt.*;
public class Mypanel  extends JPanel {
	
	private float percent ;
	private int gradelevel ;
	
	// set the level and percent
	public Mypanel(int gradelevel ,float percent){
		
		this.gradelevel = gradelevel ;
		this.percent = percent/100 ;
	}	
	
	
	// paint 
	protected void paintComponent(Graphics g ){
		super.paintComponent(g);
		
		int  width =  getWidth();
		int height = getHeight();
	
		//paint 
		g.setColor(Color.gray);
		g.fillRect((int)(0.3*width),(int)(0.05*height), (int) (0.3*width), (int) (0.75*height));
		
		g.setColor(Color.red);
		g.fillRect((int)(0.58*width),(int)(0.05*height), (int) (0.02*width), (int) (0.75*height));
		g.fillRect((int)(0.3*width),(int)((0.05+0.75*(1-percent))*height), (int) (0.3*width), (int) ((0.75*percent)*height));
		
		g.setColor(Color.black);
		g.fillRect((int)(0.58*width),(int)((0.05+0.75*(1-percent))*height), (int) (0.02*width), (int) ((0.75*percent)*height));
		
		if (percent == 0.){
			g.setColor(Color.red);
			g.fillRect((int)(0.3*width),(int)(0.79*height), (int) (0.3*width), (int) ((0.01)*height));
		}else {
			g.setColor(Color.black);
			g.fillRect((int)(0.3*width),(int)(0.79*height), (int) (0.3*width), (int) ((0.01)*height));
		}
		
		g.setColor(Color.black);
		
		
		// normal string
		//g.drawString(" Level "+gradelevel,(int) (0.3*width), (int) (0.9*height) );
		//g.drawString(percent+"%",(int) (0.4*width), (int) (0.98*height) );
		
		
		//set the front and size of string
		Font font1 = new Font(" Level "+gradelevel, Font.BOLD, 13);  
		g.setFont(font1); 
        g.drawString(font1.getName() ,(int) (0.3*width), (int) (0.9*height));
        
        Font font2 = new Font(percent+"%", Font.BOLD, 13);  
		g.setFont(font2); 
        g.drawString(font2.getName() ,(int) (0.35*width), (int) (0.98*height));
    
        
	}

	
}
