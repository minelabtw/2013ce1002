package ce1002.a8.s992001026;
import java.util.*;

public class A8 {

public static void main(String[] args) {
	
		Scanner scn = new Scanner(System.in);
		int []grade =  new  int[8];
		int n ; 
		
		
		System.out.println("Input grades:");
		
		grade[7] = 101;
		int i = 0 ;
		
		//judge the grade is ok or not ok and put it in arr 
		while (grade[7]== 101){
			n = scn.nextInt();
			if (n<0 || n>100) {
				//test
				//System.out.println("haha");
				System.out.println("Out of range!");
			}else {
				grade[i]= n ;
				i++;
			}	
		}
		
		
		// compute the percent
		int a=0 , b=0 , c= 0, d= 0 ;
		for ( int j = 0 ; j < 8 ; j ++){
			if ( grade[j] >= 0 && grade[j]<=25) a++ ;
			else if ( grade[j] >= 26 && grade[j]<= 50) b++ ;
			else if ( grade[j] >= 51 && grade[j]<= 75) c++ ;
			else if ( grade[j] >= 76 && grade[j]<=100) d++ ;
			
		}
		
		
		float [] percent = new float [4];
		
		percent [0] =  (float) (a *100/8.) ;
		percent [1] =  (float) (b *100/8.) ;
		percent [2] =  (float) (c *100/8.) ;
		percent [3] =  (float) (d *100/8.) ;
		
		
		for (int k = 0 ; k < 4 ; k++)
		System.out.println("Level "+ k +" is "+percent[k]+"%");
		
	
		Myframe f = new Myframe(percent) ;
		
		
	}

	
}
