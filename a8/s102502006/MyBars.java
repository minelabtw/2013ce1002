package ce1002.a8.s102502006;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyBars extends JPanel{
	private JLabel label; // Level
	private JLabel per; // percent
	private int i; // bar 
	private int x; // x �y��
	private float num; // percent number
	private int height; // first rectangle's height
	
	
	public MyBars(int i, int bar, int x){
		this.i = i;
		this.x = x;
		num = bar*12.5f;
		label = new JLabel("Level " + i);
		per = new JLabel(num + "%");
		label.setBounds(x, 110, 50, 20);
		per.setBounds(x,130, 50, 20);
		add(label);
		add(per);
	}

	public void paintComponent(Graphics g){
		height = (int)(100-num);
		g.setColor(Color.GRAY);
		g.fillRect(x, 5, 10, height); // draw the upper rectangle
		g.setColor(Color.RED);
		g.fillRect(x, 5+height, 10, 100-height); // draw the down rectangle
	}	
}
