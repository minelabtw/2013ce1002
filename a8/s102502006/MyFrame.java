package ce1002.a8.s102502006;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	protected MyBars barpic; // 圖片
	protected int x; // x 座標
	public MyFrame(){
		setLayout(null);
		setBounds(300, 10, 400, 200); // 視窗大小
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 關閉板子
	}
	public void newBar(int i, int bar){
		x = 5+50*i; // x 座標
		barpic = new MyBars(i,bar,x);
		barpic.setLayout(null);
		barpic.setBounds(x, 5, 200, 200);
		add(barpic);
	}
}
