package ce1002.a8.s102502006;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int time =0; // input times
		int[] bars = new int[4]; // bars information
		float grade =-1; // input
		Scanner scanner = new Scanner(System.in);
		MyFrame frame = new MyFrame();
		
		System.out.println("Input grades:");
		while(time<8 || grade<0 || grade>100){
			grade = scanner.nextFloat();
			if(grade<0 || grade>100){
				System.out.println("Out of range!");
				time--;
			}
			else {
				if(grade<=25){
					bars[0]++;
				}
				else if(grade<=50){
					bars[1]++;
				}
				else if(grade<=75){
					bars[2]++;
				}
				else bars[3]++;
			}
		time++;	
		}
		for(int i=0;i<4;i++)frame.newBar(i, bars[i]); // �ǤJ��T
		frame.setVisible(true); // show out
	}
}
