package ce1002.a8.s101201046;

import java.awt.*;
import javax.swing.*;

public class Graph extends JPanel {
	Graph() {
		this.setLayout(null); //set layoout
		this.setSize(520, 300); //set panel size
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setFont(new Font("Dialog", Font.BOLD, 16)); //set font

		for (int i = 0; i < 4; i++) {

			g.setColor(Color.red); //set bar
			g.fillRect(20 + 70*i, 30, 13, 100);

			g.setColor(Color.gray);//set cut down
			g.fillRect(20 + 70*i, 30, 13, (int)(100.0 - 12.5 * A8.stat[i]));

			g.setColor(Color.black);//information
			g.drawString("Level " + i, 20 + 70*i, 150);
			g.drawString((double)A8.stat[i]*12.5 + "%", 20 + 70*i, 170);
		}
	}
}
