package ce1002.a8.s101201046;

import java.util.Scanner;
import javax.swing.*;

public class A8 extends JFrame{
	static int[] stat = {0, 0, 0, 0};

	A8() {
		Scanner in = new Scanner(System.in); //set scanner
		int grade;

		System.out.println("Input grades:");

		for (int i = 0; i < 8; i++) { //read the grade and statistic
			do {
				grade = in.nextInt();

				if (grade < 0 || grade > 100)
					System.out.println("Out of range!");
			} while(grade < 0 || grade > 100);

			if (grade > 75)
				stat[3]++;
			else if (grade > 50)
				stat[2]++;
			else if (grade > 25)
				stat[1]++;
			else
				stat[0]++;
		}

		in.close(); //close scanner
		
		for (int i = 0; i < 4; i++) //output statistic
			System.out.println("Level " + i + " is " + (double)stat[i]*12.5 + "%");
		
		this.setLayout(null);
		this.setSize(520, 300);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(new Graph()); //show in GUI
	}

	public static void main(String arg0[]) {
		new	A8();
		System.gc();	
	}
}
