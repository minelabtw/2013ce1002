package ce1002.a8.s102502053;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		//call variables
		float marks; 
		float level0 = 0;
		float level1 = 0;
		float level2 = 0;
		float level3 = 0;
		float p0;
		float p1;
		float p2;
		float p3;
		
		//input marks
		System.out.println("Input grades:"); 
		for (int a = 0; a < 8; a++) {
			do
			{
				marks = input.nextFloat();
				if (marks < 0 || marks > 100) 
				{
					System.out.println("Out of range!");
				}
				
			} while (marks < 0 || marks > 100);

			if (marks >= 0 && marks <= 25) 
			{
				level0++;
			} else if (marks >= 26 && marks <= 50) 
			{
				level1++;
			}else if (marks >= 51 && marks <= 75)
			{
				level2++;
			}else if (marks >=76 && marks <= 100)
			{
				level3++;
			}

		}
		
		//calculation for percentage
		p0 = (level0/8) * 100;
		p1 = (level1/8) * 100;
		p2 = (level2/8) * 100;
		p3 = (level3/8) * 100;
		
		//display calculations
		System.out.println("Level 0 is " + p0 + "%");
		System.out.println("Level 1 is " + p1 + "%");
		System.out.println("Level 2 is " + p2 + "%");
		System.out.println("Level 3 is " + p3 + "%");
		
		//output the frame
		MyFrame myframe = new MyFrame(p0, p1, p2, p3);

	}

}
