package ce1002.a8.s102502053;

import java.awt.*;

import javax.swing.*;

public class Level2 extends JPanel{
	
	protected JLabel label = new JLabel();
	protected JLabel label2 = new JLabel();
	float per;
	int rate;
	
	//constructor
	Level2(float l2)
	{
		per = l2;
		rate = (int)(l2);//change type
		
		//layout of the panel
		setLayout(null);
		setBounds(210, 10, 100, 200);
		//display of the words as label
		label.setText("Level 2");
		label.setBounds(10, 140, 100, 20);
		label2.setText(per + "%");
		label2.setBounds(10, 160, 100, 20);
		setVisible(true);
		add(label);
		add(label2);
		
	}
	
	public void paintComponent(Graphics g)
	{
		//draw the bar chart
		g.setColor(Color.GRAY);
		g.fillRect(20, 30, 8, 100);
		g.setColor(Color.RED);
		g.fillRect(20, (30+(100-rate)), 8, rate);
	}
	
}
