package ce1002.a8.s102502053;

import javax.swing.*;


import java.awt.*;

public class MyFrame extends JFrame{
	
	//call variables
	float level0;
	float level1;
	float level2;
	float level3;
	
	//constructor
	MyFrame(float l0, float l1, float l2, float l3)
	{
		
		level0 = l0;
		level1 = l1;
		level2 = l2;
		level3 = l3;
		
		Level0 panel0 = new Level0(level0);
		Level1 panel1 = new Level1(level1);
		Level2 panel2 = new Level2(level2);
		Level3 panel3 = new Level3(level3);
		
		//set layout of the frame
		setLayout(null); 
		setSize(520, 260);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		
		//add panel 
		add(panel0); 
		add(panel1);
		add(panel2);
		add(panel3);
		
	}

	

}
