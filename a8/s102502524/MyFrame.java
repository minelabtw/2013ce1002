package ce1002.a8.s102502524;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame(float pass[])
	{
		setVisible(true);								//框架可見
		setSize(500, 200);								//框架大小為500*200
		setDefaultCloseOperation(EXIT_ON_CLOSE);		//關閉後離開
		setLayout(null);								//將Panel排版關閉
		
		Panel0 panel0 = new Panel0(pass[0]);			//設定Level0的Panel
		panel0.setLocation(20, 10);
		add(panel0);
		Panel1 panel1 = new Panel1(pass[1]);			//設定Level1的Panel
		panel1.setLocation(140, 10);
		add(panel1);
		Panel2 panel2 = new Panel2(pass[2]);			//設定Level2的Panel
		panel2.setLocation(260, 10);
		add(panel2);
		Panel3 panel3 = new Panel3(pass[3]);			//設定Level3的Panel
		panel3.setLocation(380, 10);
		add(panel3);
	}

}
