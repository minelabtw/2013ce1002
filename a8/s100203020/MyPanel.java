package ce1002.a8.s100203020;
import javax.swing.*;
public class MyPanel extends JPanel{
	protected Paints graphics;
	protected JLabel levels= new JLabel();
	protected JLabel persents = new JLabel();

	
	MyPanel(int i,double persent){
		setLayout(null);

		this.graphics = new Paints(persent);
		
		this.levels.setText("Level "+i);
		this.persents.setText(persent+"%");
		
		this.graphics.setBounds(10,0,140,180);
		this.levels.setBounds(10,200, 140, 20);
		this.persents.setBounds(10,230, 140, 30);
		add(graphics);
		add(levels);
		add(persents);
		
	}
}
