package ce1002.a8.s100203020;

import java.util.*;
public class A8 {
	
	public static Scanner scan ;
	public static void main(String[] args) {
		int[] student=new int[8];

		System.out.println("Input grades: ");
		scan=new Scanner(System.in);
		for(int i=0;i<8;){
			student[i]=scan.nextInt();
			if(student[i]<0 || student[i]>100)
				System.out.println("Out of range!");
			else
				i++;
		}
		allocate(student);

	}
	/** allocate the grades to four levels */
	public static void allocate(int[] grade) {
		int[] level={0, 0, 0, 0};
		for(int i=0;i<8;i++){
			if(grade[i]<=25)
				level[0]++;
			else if(grade[i]<=50)
				level[1]++;
			else if(grade[i]<=75)
				level[2]++;
			else 
				level[3]++;
		}
		percent(level);//allocate the levels to percents
	}
	
	/** print percents of four levels and call MyFrame*/
	public static void percent(int[] level) {
		double[] Percentage={0, 0, 0, 0};

		for(int i=0;i<4;i++){
			Percentage[i]=(double)level[i]/(double)(level[0]+level[1]+level[2]+level[3])*100;
			System.out.println("Level "+i+" is "+Percentage[i]+"%");

		}
		
		MyFrame frame=new MyFrame(Percentage);//call myframe

	}
}

