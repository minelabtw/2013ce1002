package ce1002.a8.s100203020;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	protected MyPanel[] panel=new MyPanel[4];

	MyFrame(double[] Percentage){
		this.setLayout(null);
		this.setBounds(30,30,520, 330);
		this.setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//set panel's bounds,and call construct func.
		for(int i=0;i<4;i++){
			panel[i]=new MyPanel(i,Percentage[i]);
			panel[i].setBounds(130*i,0,130,330);
			add(panel[i]);
			}

		}
		
	}
	


