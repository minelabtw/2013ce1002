package ce1002.a8.s102502013;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
public class MyPanel extends JPanel{
	private double [] ratioOfLevel;
	MyPanel(double []ratio){
		ratioOfLevel = ratio;
	}
	//draw chart about grade
	protected void paintComponent(Graphics g ){
		for(int i=0;i<4;i++){
			g.setColor(Color.gray);
			g.drawRect((65 + i*80), 45, 20, 100);
			g.fillRect((65 + i*80), 45, 20, 100);
			g.setColor(Color.red);
			g.fillRect((65 + i*80), (int) (45 + (100 - ratioOfLevel[i])), 20, (int)(ratioOfLevel[i]));
			g.setColor(Color.black);
			g.drawString("Level" + i, (65 + i*80), 200);
			g.drawString(ratioOfLevel[i] + "%", (65 + i*80), 225);
		}
		
	}
}
