package ce1002.a8.s102502013;
import javax.swing.JFrame;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		final int number = 8;//declare a variable for inputdata's number
		int score = 0;
		double []level = new double[4];
		double []ratioOfLevel = new double[4];
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
			score = input.nextInt();
			while(score<0||score>100){
				System.out.println("Out of range!");
				System.out.println("Input grades: ");
				score = input.nextInt();
			}
			//change score to grade
			if(score>=0&&score<25){
				level[0]++;
			}
			else if(score>=25&&score<50){
				level[1]++;
			}
			else if(score>=50&&score<75){
				level[2]++;
			}
			else{
				level[3]++;
			}
		}
		//show the percentage of grade
		for (int i=0;i<4;i++){
			ratioOfLevel[i] = (level[i] / 8) * 100;
		}
		for (int i=0;i<4;i++){
			System.out.println("Level " + i + " is " + ratioOfLevel[i] + "%");
		}
		MyFrame myframe = new MyFrame(ratioOfLevel);
		myframe.setTitle("102502013");
		myframe.setSize(575, 300);
		myframe.setLocationRelativeTo(null);
		myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myframe.setVisible(true);
		myframe.setResizable(false);
	}

}
