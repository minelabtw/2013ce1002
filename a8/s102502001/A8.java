package ce1002.a8.s102502001;
import java.awt.Graphics;
import java.util.Scanner;
public class A8 {
	public static double lv0 = 0;                    //declare variable
	public static double lv1 = 0;
	public static double lv2=0;
	public static double lv3=0;
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double []arr = new double[8];               //declare array
		
		System.out.println("Input grades: ");
			for(int i=0;i<8;i++){                   //determine the range
				do{
					arr[i] = scanner.nextDouble();
					if(arr[i]>=0 && arr[i]<=25)	
						lv0++;
					if(arr[i]>=26 && arr[i]<=50)	
						lv1++;
					if(arr[i]>=51 && arr[i]<=75)	
						lv2++;
					if(arr[i]>=76 && arr[i]<=100)	
						lv3++;
					if(arr[i]>100 || arr[i]<0)
							System.out.println("Out of range!");
				}while(arr[i]>100 || arr[i]<0);			
			}			
			scanner.close();
			System.out.println("Level 0 is "+(lv0/8)*100+"%");  //print the message
			System.out.println("Level 1 is "+(lv1/8)*100+"%");
			System.out.println("Level 2 is "+(lv2/8)*100+"%");
			System.out.println("Level 3 is "+(lv3/8)*100+"%");
						
			MyFrame frame = new MyFrame();                      //declare frame
	}
}
