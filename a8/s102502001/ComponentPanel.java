package ce1002.a8.s102502001;
import java.awt.*;
import javax.swing.*;

import javax.swing.JPanel;
public class ComponentPanel extends JPanel{ 	
	    A8 a8 = new A8();                              //declare a8
		protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.GRAY);                        //draw gray rectangle
		g.fillRect(15,15,15,200);
		g.setColor(Color.GRAY);
		g.fillRect(95,15,15,200);
		g.setColor(Color.GRAY);
		g.fillRect(180,15,15,200);
		g.setColor(Color.GRAY);
		g.fillRect(265,15,15,200);
		g.setColor(Color.RED);                         //draw red rectangle
		g.fillRect(15,(int)(215-(a8.lv0*25)),15,(int)(a8.lv0*25));
		g.setColor(Color.RED);
		g.fillRect(95,(int)(215-(a8.lv1*25)),15,(int)(a8.lv1*25));
		g.setColor(Color.RED);
		g.fillRect(180,(int)(215-(a8.lv2*25)),15,(int)(a8.lv2*25));
		g.setColor(Color.RED);
		g.fillRect(265,(int)(215-(a8.lv3*25)),15,(int)(a8.lv3*25));
		g.setColor(Color.BLACK);
		g.drawString("Level 0",10,240);                //set string
		g.drawString((a8.lv0*12.5)+"%",10,260);
		g.drawString("Level 1",95,240);
		g.drawString((a8.lv1*12.5)+"%",95,260);
		g.drawString("Level 2",180,240);
		g.drawString((a8.lv2*12.5)+"%",180,260);
		g.drawString("Level 3",265,240);
		g.drawString((a8.lv3*12.5)+"%",265,260);
}
}

