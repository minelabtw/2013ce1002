package ce1002.a8.s102502001;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
public class MyPanel extends JPanel{

	protected JLabel label1 = new JLabel();         //declare labels
	protected JLabel label2 = new JLabel();
	protected JLabel label3 = new JLabel();
	protected JLabel label4 = new JLabel();
	
	
	ComponentPanel data = new ComponentPanel();
	
	public MyPanel(){                               //set panel
		setLayout(null);
		setVisible(true);
		label1.setBounds(5,5,80,280);
		label2.setBounds(90,5,80,280);
		label3.setBounds(175,5,80,280);
		label4.setBounds(260,5,80,280);
		add(label1);                               //add panel
		add(label2);
		add(label3);
		add(label4);	
		
	}
	
}
