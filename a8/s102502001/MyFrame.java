package ce1002.a8.s102502001;
import java.awt.Graphics;

import javax.swing.*;

public class MyFrame extends JFrame {
	public MyFrame(){
		ComponentPanel comp = new ComponentPanel();          //declare comp		
		MyPanel panel= new MyPanel();	                     //declare panel
		setSize(500,350);                                    //set frame
		setLocationRelativeTo(null);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setVisible(true);
		add(panel);                                          //add panel and comp on frame
		add(comp);
	}
}
