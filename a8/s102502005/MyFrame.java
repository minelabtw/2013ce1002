package ce1002.a8.s102502005;
import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame{

	MyFrame(double[] levelpercent){
		
		this.setLayout(null);
		
		for(int i=0;i<4;i++){//利用for迴圈依序添加元件。
			MyPanel panel = new MyPanel(levelpercent[i]) ;//再把各個level的percent傳給各個panel。
			panel.setBounds(10+i*50,10,50,190);
			this.add(panel);
	
			JLabel label1 = new JLabel ("Level " + i); 
			label1.setBounds(10+i*50,200,50,15);
			this.add(label1);
	
			JLabel label2 = new JLabel (levelpercent[i] + "%"); 
			label2.setBounds(10+i*50,215,50,15);
			this.add(label2);
		}
		
		this.setSize(230, 270);//frame的size要從最外框起算(包含最小化、最大化、叉叉)。
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
