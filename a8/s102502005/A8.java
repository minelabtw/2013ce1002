package ce1002.a8.s102502005;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int temp;
		double[] levelpercent = new double[4];

		for (int i = 0; i < 4; i++) { // 初始化。
			levelpercent[i] = 0;
		}

		System.out.println("Input grades: ");// 吃資料。
		for (int i = 0; i < 8; i++) {
			temp = input.nextInt();
			while (temp < 0 || temp > 100) {
				System.out.println("Out of range!");
				temp = input.nextInt();
			}
			if (temp >= 0 && temp <= 25) {levelpercent[0]++;} 
			else if (temp >= 26 && temp <= 50) {levelpercent[1]++;} 
			else if (temp >= 51 && temp <= 75) {levelpercent[2]++;} 
			else if (temp >= 76 && temp <= 100) {levelpercent[3]++;}
		}
		input.close();

		for (int i = 0; i < 4; i++) { // 算百分比。
			levelpercent[i] = levelpercent[i] / 8 * 100; // 整數除整數會回傳整數，造成double吃不到資料。
			System.out.println("Level " + i + " is " + levelpercent[i] + "%");
		}

		MyFrame frame = new MyFrame(levelpercent);// 把百分比傳進去之後印出視窗。

	}

}
