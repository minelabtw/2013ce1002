package ce1002.a8.s102502005;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;

import javax.swing.JPanel;

public class MyPanel extends JPanel {
	MyPanel(double percent) {
		this.setLayout(null);

		Block block = new Block(percent);//把percent傳給要畫出正方形的panel。
		this.add(block);
	}
}

class Block extends JPanel {
	int percent;
	
	public Block(double percent) {
		this.setBounds(0, 0, 20, 190);
		this.percent = (int) (Math.ceil(percent*190/100));
	}

	protected void paintComponent(Graphics g) { //複寫paintComponent來畫出長條圖。
		super.paintComponent(g); 
		g.setColor(Color.GRAY); 
		g.fillRect(0, 0, 50, 190);
		g.setColor(Color.RED);
		g.fillRect(2, 190-this.percent, 50, this.percent);
	}
}

