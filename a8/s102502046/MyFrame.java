package ce1002.a8.s102502046;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame ( double[] level ) {
		MyPanel[] panel = new MyPanel[4];
		setSize(400,200); //視窗大小
		setTitle("LOOK") ;
		setLocationRelativeTo(null); //視窗預設位置 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //關閉視窗
		setLayout(null); //內部元素的排版方式
		//設定panel的屬性值
		for ( int i = 0 ; i < 4 ; i++ ) {
			panel[i] = new MyPanel();
			panel[i].setLocation(10 + 100 * i, 10); //設定位置
			panel[i].setState(level[i], i); 
			add(panel[i]);
		}
		setVisible(true); //使視窗顯示在螢幕上
	}
	
}
