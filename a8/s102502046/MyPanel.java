package ce1002.a8.s102502046;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
public class MyPanel extends JPanel{
	JLabel state = new JLabel(); //儲存等級名稱
	JLabel num = new JLabel(); //儲存百分比
	int level = 0; //儲存等級編號

	MyPanel () {
		setSize(100,200); //設定大小
		setLayout(null); //設定排版方式
	}
	//設定2個Label的屬性值
	public void setState ( double level ,int id ) {
		state.setText("Level " + id);
		num.setText(level+"%");
		this.level = (int)level;
		state.setSize(50,20); //設定state的大小
		state.setLocation(10,105); //設定state的位置
		num.setSize(50,20); //設定num的大小
		num.setLocation(10,125); //設定num的位置
		add(state);
		add(num);
	}
	//繪圖函數
	protected void paintComponent( Graphics g ) {
		super.paintComponent(g);
		int width = 20; //儲存長條圖寬度
		int height = 100; //儲存長條圖高度
		g.setColor(Color.RED); //更改顏色為紅色
		g.fillRect(10, 110 - height * level / 100, width, height * level / 100); 
		g.setColor(Color.BLUE);
		g.fillRect(10, 10 , width, 100 - height * level / 100 ); 
		
	}
}