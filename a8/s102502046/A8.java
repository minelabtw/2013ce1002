package ce1002.a8.s102502046;
import java.util.Scanner;
import javax.swing.JFrame;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int grade ;
		double[] level = new double[4] ;
		System.out.println("Input grades:");
		Scanner in = new Scanner(System.in);
		for(int i=0 ; i<8 ; i++) //輸入成績並分段
		{
			grade = in.nextInt();
			while(grade<0||grade>100)
			{
				System.out.println("Out of range!");
				grade = in.nextInt();
			}
			if(grade<25)
				level[0]++;
			else if(grade<50)
				level[1]++;
			else if(grade<75)
				level[2]++;
			else if(grade<100)
				level[3]++;
		}
		level[0] = level[0] / 8 * 100; //將人數更改成百分比
		level[1] = level[1] / 8 * 100;
		level[2] = level[2] / 8 * 100;
		level[3] = level[3] / 8 * 100;
		for ( int i = 0 ; i < 4 ; i++ ) //輸出結果
		{
			System.out.println("Level " + i + " is " + level[i] + "%");
		}
		MyFrame frame = new MyFrame(level);
		frame.setVisible(true);
	}

}
