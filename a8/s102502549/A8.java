package ce1002.a8.s102502549;

import java.util.Scanner;

import javax.swing.JFrame;

public class A8 {

	public static void main(String[] args) {

		// 宣告學生人數，各level人數陣列，各level比例陣列
		int num_of_student = 8;
		int[] num_of_level = new int[4];
		double[] proportion=new double[4];

		// 建立輸入用物件
		Scanner input = new Scanner(System.in);

		System.out.println("Input grades: ");
		
		//輸入成績並統計人數，包含檢查
		for (int i = 0; i < num_of_student; i++) {
			double scoretemp;
			
			do {
				scoretemp = input.nextDouble();

				if (0 <= scoretemp && scoretemp <= 25)
					num_of_level[0]++;
				else if (26 <= scoretemp && scoretemp <= 50)
					num_of_level[1]++;
				else if (51 <= scoretemp && scoretemp <= 75)
					num_of_level[2]++;
				else if (76 <= scoretemp && scoretemp <= 100)
					num_of_level[3]++;
				else
					System.out.println("Out of range!");

			} while (scoretemp < 0 || scoretemp > 100);
		}
		
		//關掉Scanner
		input.close();
		
		//印出結果
		for(int i=0;i<4;i++)
		{
			proportion[i]=(double)num_of_level[i]/(double)num_of_student;
			System.out.println("Level "+i+" is "+proportion[i]*100+"%");
		}
		
		//顯示視窗
		JFrame frame=new JFrame();
		RectangleGraphics p=new RectangleGraphics(proportion);
		frame.add(p);
		frame.setLayout(null);
		frame.setSize(250, 300);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
