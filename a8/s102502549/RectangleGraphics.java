package ce1002.a8.s102502549;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class RectangleGraphics extends JPanel {

	// 用來存level%數
	private double[] proportion = new double[4];

	// 長條圖的長寬
	private final int recwidth = 20;
	private final int recheight = 200;

	// 有比例陣列的建構式
	public RectangleGraphics(double[] proportion) {
		setBounds(0, 0, 300, 300);
		this.proportion = proportion;
	}

	@Override
	// 覆寫paintComponent
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		for (int i = 0; i < 4; i++) {
			// 畫長條圖
			g.setColor(Color.GRAY);
			g.fill3DRect(10 + i * 50, 10, recwidth, recheight, true);

			//實測後發現y值即使為0仍然會強調突起效果，所以y=0時乾脆不畫
			if (proportion[i] != 0) {
				g.setColor(Color.RED);
				g.fill3DRect(10 + i * 50, 10 + recheight - ((int) (recheight * proportion[i])), recwidth, (int) (recheight * proportion[i]), true);
			}

			// 畫字串
			g.setColor(Color.BLACK);
			g.drawString("Level " + i, 10 + i * 50, 10 + recheight + 15);
			g.drawString(proportion[i] * 100 + "%", 10 + i * 50, 10 + recheight + 30);
		}
	}
}
