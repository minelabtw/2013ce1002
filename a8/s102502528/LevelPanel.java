package ce1002.a8.s102502528;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class LevelPanel extends JPanel {
	JPanel rec_gray;
	JPanel rec_red;
	JLabel text;
	JLabel persentLabel;
	String name;

	LevelPanel() {
		Student s = new Student(); // let user input grade of students
		setLayout(null);
		setBounds(0, 0, 250, 240);

		for (int j = 0; j < 4; j++) { // make out table and its info
			new_texts(j, s.level[j]);
			new_rec(j, s.level[j]);
			add(text);
			add(persentLabel);
			add(rec_gray);
			add(rec_red);
		}
	}

	void new_rec(int level, int persent) {
		rec_gray = new JPanel();
		rec_gray.setBackground(Color.gray);
		rec_gray.setBounds(level * 55 + 10, 10, 30,     //draw it
				80 - (int) (0.8 * 12.5 * persent));

		rec_red = new JPanel();
		rec_red.setBackground(Color.red);
		rec_red.setBounds(level * 55 + 10, 10, 30, 80);
	}

	void new_texts(int level, int persent) {
		text = new JLabel();                 //set texts
		text.setText("level " + level);
		text.setBounds(level * 55 + 5, 75, 50, 80);
		persentLabel = new JLabel();
		persentLabel.setText(persent * 12.5 + "%");
		persentLabel.setBounds(level * 55 + 5, 95, 50, 80);
		System.out.println("level " + level + " is " + persent * 12.5 + "%");
	}

}
