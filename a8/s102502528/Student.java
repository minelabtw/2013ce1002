package ce1002.a8.s102502528;

import java.util.*;

public class Student {
	int grade[] = new int[8];
	int[] level = new int[4];

	Student() {
		set_grade();
	}

	public void set_grade() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input grades(0~100): ");

		for (int i = 0; i < 8; i++) {  //8 students
			do {                       //judge the input
				grade[i] = scanner.nextInt();
				if (grade[i] < 0 || grade[i] > 100) {
					System.out.println("Out of range!");
				}
			} while (grade[i] < 0 || grade[i] > 100);

		}
		for (int i = 0; i < 4; i++) {
			level[i] = 0;
		}
		for (int i = 0; i < 8; i++) {   //put them into level
			if (grade[i] > 75) {
				level[3] += 1;
			} else if (grade[i] > 50) {
				level[2] += 1;
			} else if (grade[i] > 25) {
				level[1] += 1;
			} else {
				level[0] += 1;
			}
		}
	}

}
