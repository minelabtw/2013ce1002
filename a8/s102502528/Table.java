package ce1002.a8.s102502528;

import javax.swing.JFrame;

public class Table extends JFrame {
	Table() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //make a frame for rectangle table
		setLayout(null);
		setSize(250, 240);
		setLocationRelativeTo(null);
		
		LevelPanel levelPanel = new  LevelPanel();
		add(levelPanel);
		
		setVisible(true);
	}
}
