package ce1002.a8.s102502027;

import java.awt.*;
import java.util.Scanner;
import javax.swing.*;

public class A8 extends JPanel {

	public static float Level[] = new float[4];

	A8(){
		setSize(200,250);
	}
	public void paintComponent(Graphics g) { // 長條圖
		super.paintComponent(g);
		for(int i=0;i<=3;i++){
		g.setColor(Color.GRAY); //顏色設定
		g.fillRect(16*(6*(i+1)-3), 5, 15,100 ); //有顏色的填滿及位置
		g.setColor(Color.RED);
		g.fillRect(16*(6*(i+1)-3), 105-(int)Level[i] * 100 / 8 , 15,(int)Level[i] * 100 / 8 );
		g.setColor(Color.BLACK);
		g.setFont(new Font("Level "+i, Font.BOLD, 15));
		g.setFont(new Font(Level[i] * 100 / 8 + "%", Font.BOLD, 15)); //調整字體大小
		g.drawString("Level  "+i, 10*(10*i+3), 120); //調整字體位置
		g.drawString(Level[i] * 100 / 8 + "%", 10*(10*i+3), 140);
		}
	}

	public static void main(String[] args) { 
		Scanner input = new Scanner(System.in);
		int a = 1;
		float grade;
		System.out.println("Input grades:");
		while (a <= 8) {   //執行判斷
			grade = input.nextInt();
			if (grade > 100 || grade < 0)
				System.out.println("Out of range!");
			else if (0 <= grade && grade < 25) {
				Level[0]++;
				a++;
			} else if (26 <= grade && grade < 50) {
				Level[1]++;
				a++;
			} else if (51 <= grade && grade < 75) {
				Level[2]++;
				a++;
			} else if (76 <= grade && grade <= 100) {
				Level[3]++;
				a++;
			}

		}
		for (int i = 0; i <= 3; i++) { //輸出
			System.out
					.println("Level " + i + " is " + Level[i] * 100 / 8 + "%");
		}
		fram frame = new fram();
	}
}
