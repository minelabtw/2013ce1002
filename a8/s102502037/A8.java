package ce1002.a8.s102502037;
import java.awt.*;
import java.util.Scanner;

import javax.swing.*;


public class A8 {
	public static void main(String[] args) {
		System.out.println("Input grades:");//輸入成績
		int inf[]=new int[8];
		int i,a=0;
		int level[][]=new int [4][8];
		double	ca=0,cb=0,cc=0,cd=0;
		for(i=0 ; i < 8 ; i++)
		{
		Scanner input = new Scanner (System.in);
		int put = input.nextInt();
		if(put<0 || put >100)
		{
			a=1;
		}
		while(a==1)
		{
			a=0;
			System.out.println("Out of Range!");
			put = input.nextInt();
			if(put<0 || put >100)
			{
				a=1;
			}
		}
		inf[i]=put;
		put=0;
		level[0][i]=-1;
		level[1][i]=-1;
		level[2][i]=-1;
		level[3][i]=-1;
		}
		for(i=0 ; i<8 ; i++)//做判斷並且統計
		{
			if(0<=inf[i] && inf[i]<=25)
			{
				level[0][i]=inf[i];
				ca=ca+1;
			}
			else if(26<=inf[i] && inf[i]<=50)
			{
				level[1][i]=inf[i];
				cb=cb+1;
			}
			else if(51<=inf[i] && inf[i]<=75)
			{
				level[2][i]=inf[i];
				cc=cc+1;
			}
			else if(76<=inf[i] && inf[i]<=100)
			{
				level[3][i]=inf[i];
				cd=cd+1;
			}
		}		
		System.out.println("Level 0 is "+ (ca /8)*100+"%");//輸出結果並傳到FRAME裡面
		System.out.println("Level 1 is "+ (cb /8)*100+"%");
		System.out.println("Level 2 is "+ (cc /8)*100+"%");
		System.out.println("Level 3 is "+ (cd /8)*100+"%");
		MyFrame frame=new MyFrame(ca ,cb ,cc ,cd);
		frame.add(new MyPanel(ca ,cb ,cc ,cd));
		frame.setVisible(true);
}	
}
