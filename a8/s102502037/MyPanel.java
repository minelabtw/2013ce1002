package ce1002.a8.s102502037;
import java.awt.*;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;
public class MyPanel extends JPanel{
	protected JLabel Label1 = new JLabel();//建立LABEL
	protected JLabel Label2 = new JLabel();
	protected JLabel Label3 = new JLabel();
	protected JLabel Label4 = new JLabel();
	protected JLabel Label5 = new JLabel();
	protected JLabel Label6 = new JLabel();
	protected JLabel Label7 = new JLabel();
	protected JLabel Label8 = new JLabel();
	protected JLabel Label9 = new JLabel();
	private int i,j,k,l;
	protected void paintComponent(Graphics g) 
	{

		super.paintComponents(g);//畫圖
		Color color = g.getColor();
		g.setColor(Color.gray);
		g.fillRect(40, 50, 20, 80);
		g.setColor(Color.red);
		g.fillRect(40, 130-i*80/8, 20, i*80/8);
		g.setColor(Color.gray);
		g.fillRect(110, 50, 20,80);
		g.setColor(Color.red);
		g.fillRect(110, 130-j*80/8, 20, j*80/8);
		g.setColor(Color.gray);
		g.fillRect(180, 50, 20, 80);
		g.setColor(Color.red);
		g.fillRect(180, 130-k*80/8, 20, k*80/8);
		g.setColor(Color.gray);
		g.fillRect(250, 50, 20, 80);
		g.setColor(Color.red);
		g.fillRect(250, 130-l*80/8, 20, l*80/8);

		
	}
	MyPanel(double a,double b,double c,double d)//接收統計資料
	{
		this.i = (int) a;
		this.j = (int) b;
		this.k = (int) c;
		this.l = (int) d;
		setLayout(null);
		add(Label1);	//新增LABEL
		add(Label2);	
		add(Label3);	
		add(Label4);
		add(Label5);
		add(Label6);	
		add(Label7);	
		add(Label8);
		add(Label9);
		Label1.setText("結果是");//輸出結果文字
		Label1.setBounds(15,10,210,15);
		Label2.setText("Level0");
		Label2.setBounds(30,150,50,10);
		Label3.setText("Level1");
		Label3.setBounds(100,150,50,10);
		Label4.setText("Level2");
		Label4.setBounds(170,150,50,10);
		Label5.setText("Level3");
		Label5.setBounds(240,150,50,10);
		Label6.setText((a/8)*100+"%");
		Label6.setBounds(30,170,50,10);
		Label7.setText((b/8)*100+"%");
		Label7.setBounds(100,170,50,10);
		Label8.setText((c/8)*100+"%");
		Label8.setBounds(170,170,50,10);
		Label9.setText((d/8)*100+"%");
		Label9.setBounds(240,170,50,10);
		
	}
}
