package ce1002.a8.s102502552;

import java.awt.GridLayout;
import java.util.Scanner;
import javax.swing.JFrame;

public class A8 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int[] scores = new int[8];//八個分數
		double[] level = new double[4];//四種人數
	    double[] percents = new double[4];//四種百分比
	    
	    System.out.print("Input Grades:");
		
	    for(int i = 0;i < scores.length;i++)
	    {
	    	int score = 0;
		   	do{
		   		score = sc.nextInt();
		   		if(score < 0 || score > 100)
		   		{
	    			System.out.println("Out of Range!");
	    		}
	    		else break;
		   	}while(score < 0 || score > 100);
		
		   	scores[i] = score;//輸入分數，檢查，無誤后記錄
			
		   	if(scores[i] >= 0 && scores[i] <= 24)
	    	{
	    		level[0]++;
		   	}
		   	else if(scores[i] >= 25 && scores[i] <= 50)
		   	{
		   		level[1]++;
	    	}
		   	else if(scores[i] >= 51 && scores[i] <= 75)
		   	{
		   		level[2]++;
		   	}
		   	else
	    	{
	    		level[3]++;		    	
	    	}//根據分數算出四種人數
	    }
	    
	    sc.close();
		
	    for(int i = 0;i < 4;i++)
		   {
		   	percents[i] = level[i] / 8 * 100;
	    	System.out.println("Level " + i + " is " + percents[i] + "%");
		   }//算出四種百分比并顯示
	    
	    JFrame myframe = new JFrame();
	    myframe.setSize(400,350);
	    myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    myframe.setVisible(true);//建立窗口
	    myframe.setLayout(new GridLayout(1,4));//四個圖形一字排開
	    
	    MyPanel panel[] = new MyPanel[4];
	    
	    for(int i = 0;i < 4;i++)
	    {
	    	panel[i] = new MyPanel(i,percents[i]);//把參數傳入各面板
	    	myframe.add(panel[i]);//將面板加入窗口
	    }
	}
}