package ce1002.a8.s102502552;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private double percent;
	
	MyPanel(int i,double percents)
	{
		setSize(100,300);
		setpercents(percents);//設置面板預期中參數
		setLayout(null);//排版
		ImagePanel imagepanel = new ImagePanel(percents);//圖形面板
		JLabel label1 = new JLabel("Level" + i);
		label1.setBounds(40,200,100, 50);//等級標籤
		JLabel label2 = new JLabel("" + percent + "%");
		label2.setBounds(40,250,100,50);//百分比標籤
		add(imagepanel);
		add(label1);
		add(label2);//依序放入三個物件
	}
	
	public void setpercents(double percents)
	{
		percent = percents;
	}//導入參數
}
