package ce1002.a8.s102502552;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class ImagePanel extends JPanel{
	private double percent;
	
	ImagePanel(double percents)
	{
		setBounds(0,0,100, 200);
		setpercents(percents);
	}//圖形面板各參數
	
	public void paintComponent(Graphics G)//繪圖
	{
		super.paintComponents(G);
		G.setColor(Color.GRAY);
		G.fillRect(40, 10, 20, 180);//大長條
		G.setColor(Color.RED);
		G.fillRect(40, 10 + (int)((100 - percent) * 1.8), 20, (int)(percent * 1.8));//百分比的小長條
	}
	
	public void setpercents(double percents)
	{
		percent = percents;
	}//導入參數
}
