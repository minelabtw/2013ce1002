package ce1002.a8.s101201508;

import java.awt.FlowLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel panel[]=new MyPanel[4];
	protected float percentage[]=new float [4];
	MyFrame(float p[])
	{//input the data
		percentage=p;
		for (int i=0;i<4;i++)
		{
			panel[i]=new MyPanel(i,p[i]);
			panel[i].setBounds(20+120*i,0,120,220);
		}
		setLayout(null);
		setSize(550,280);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		//input the data into the panel
		for (int i=0;i<4;i++)
		{
			add(panel[i]);
		}
	}
}
