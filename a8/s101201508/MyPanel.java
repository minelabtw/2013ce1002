package ce1002.a8.s101201508;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	protected int level=0;
	protected float percentage=0;
	protected graphPanel gPanel=new graphPanel();
	protected JLabel lLabel=new JLabel();
	protected JLabel pLabel =new JLabel();
	MyPanel(int l,float p)
	{//input the data
		level=l;
		percentage=p;
		setLayout(null);
		gPanel.setBounds(40,20,40,100);
		gPanel.setPercentage(percentage);
		lLabel.setBounds(30,140,60,40);
		pLabel.setBounds(30,180,60,40);
		lLabel.setText("Level "+level);
		pLabel.setText(percentage+"%");
		add(gPanel);
		add(lLabel);
		add(pLabel);
	}
}