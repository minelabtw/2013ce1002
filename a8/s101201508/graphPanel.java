package ce1002.a8.s101201508;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class graphPanel extends JPanel{
	protected float percentage=0;
	graphPanel()
	{
		percentage=0;
	}
	graphPanel(float p)
	{
		percentage=p;
	}
	void setPercentage(float p)//set percentage
	{
		percentage=p;
	}
	float getPercentage()//get percentage
	{
		return percentage;
	}
	@Override
	protected void paintComponent(Graphics g) //use the graphics to print the graph
	{//show the graph
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 20, 100-(int)percentage);
		g.setColor(Color.RED);
		g.fillRect(0, 100-(int)percentage, 20, (int)percentage);
	}
}
