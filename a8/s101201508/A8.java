package ce1002.a8.s101201508;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//user input the data
		Scanner sc=new Scanner(System.in);
		int grades[]=new int [8];
		System.out.println("Input grades: ");
		for (int i=0;i<8;i++) 
		{
			grades[i]=sc.nextInt();
			while (grades[i]<0 || grades[i]>100)
			{
				System.out.println("Out of range!");
				grades[i]=sc.nextInt();
			}
		}
		//get the percentage
		float level[]=new float [4];
		for (int i=0;i<8;i++)
		{
			if (grades[i]<25)
				level[0]++;
			else if (grades[i]<50)
				level[1]++;
			else if (grades[i]<75)
				level[2]++;
			else
				level[3]++;
		}
		//show the percentage 
		for (int i=0;i<4;i++)
		{
			
			level[i]=level[i]*25/2;
			System.out.println("Level "+i+" is "+level[i]+"%");
		}
		//use the MyFrame to show the graph
		MyFrame F=new MyFrame(level);
	}
	

}
