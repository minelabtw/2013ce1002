package ce1002.a8.s102502536;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		float L0 = 0; // declare variables
		float L1 = 0;
		float L2 = 0;
		float L3 = 0;
		int grade = 0;
		int [] grades = new int [8];  // declare a array to store the grades
		
		System.out.println("Input grades:");
		
		for (int i = 0; i < 8; i++)  // input the grades and store in to grades
		{
			do 
			{
				grade = input.nextInt();
				
				if (grade < 0 || grade > 100)
					System.out.println("Out of range!");
				
			} while (grade < 0 || grade > 100);
			
			grades[i] = grade;
		}
		
		for (int i = 0; i < 8; i++)  // judge amount of each level
		{
			if (grades[i] <= 25 && grades[i] >= 0)
				L0++;
			if (grades[i] <= 50 && grades[i] >= 26)
				L1++;
			if (grades[i] <= 75 && grades[i] >= 51)
				L2++;
			if (grades[i] <= 100 && grades[i] >= 76)
				L3++;
		}
		
		float [] s = new float [4]; // count the percents and store in to the float array
		
	    s[0] = L0 * 100/8;
		s[1] = L1 * 100/8;
		s[2] = L2 * 100/8;
		s[3] = L3 * 100/8;
		
		System.out.println("Level 0 is " + s[0] + "%\n" + "Level 1 is " + s[1] + "%\n" + "Level 2 is " + s[2] + "%\n" + "Level 3 is " + s[3] + "%\n");

		MyFrame frame = new MyFrame(s);
		
		input.close();
	}

}
