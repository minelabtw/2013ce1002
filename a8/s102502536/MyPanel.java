package ce1002.a8.s102502536;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel {
	
	protected JLabel level = new JLabel();
    protected JLabel percent = new JLabel();
    protected int p;
    
	MyPanel() {
		/* set panel's size and layout */
		setLayout(null);
		level.setBounds(35, 160, 70, 20);
		percent.setBounds(35, 180, 70, 20);
		/* add label to panel */
		add(level);
        add(percent);

	}
	
	public void setS(float s) {  // set the length of the second rectangle 
		
		 p = 100 - (int)s;
	}
	 
	public void setLabelText(float s , int n) {  // set label text

	    level.setText("Level " + n);
		percent.setText(s + "%");
	    
	}
	
	protected void paintComponent(Graphics g) {  // draw the histogram
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(35, 50, 30, 100);
		g.setColor(Color.GRAY);
		g.fillRect(35, 50, 30, p);
				
	}
}
