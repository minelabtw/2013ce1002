package ce1002.a8.s102502536;

import javax.swing.*;

public class MyFrame extends JFrame {
	/* Create four panels */
	protected MyPanel panel0 = new MyPanel();
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	protected float [] s;
	
	MyFrame(float[] s) {
		this.s = s;
		/* set frame's size , layout... */
		setLayout(null);
		setBounds(0, 0, 460, 340);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// set histogram position and label text ...
		setHistogram(s[0], 0, panel0, 20, 20);
		setHistogram(s[1], 1, panel1, 120, 20);
		setHistogram(s[2], 2, panel2, 220, 20);
		setHistogram(s[3], 3, panel3, 320, 20);
		
	}
	
	public void setHistogram(float s, int n, MyPanel panel, int x, int y) {
		/* set panel's position and size */
		panel.setBounds(x, y, 100, 300);
		/* set label which in the panel */
		panel.setLabelText(s, n);
		/* add panel to frame */
		panel.setS(s);
		add(panel);
	}

}
