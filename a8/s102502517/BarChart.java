package ce1002.a8.s102502517;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class BarChart extends JPanel{
	private int level;
	
	public BarChart(int level)
	{
		this.level = level;
	}
	
	public void paint(Graphics g) //印出長條圖
	{
		g.setColor(Color.BLACK);
		g.fillRect(0,0,20,96);
		g.setColor(Color.RED);
		g.fillRect(0,96-12*level,20,12*level);
	}
}
