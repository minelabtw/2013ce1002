package ce1002.a8.s102502517;

import java.awt.GridLayout;
import java.util.Scanner;
import javax.swing.JFrame;

public class A8 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int [] level = new int[4]; //以陣列存取Level次數
		int score = 0;
		
		System.out.println("Input grades: ");
		for(int i=0;i<=7;i++)
		{
			do
			{
				score = scanner.nextInt();
				if(score<0 || score>100)
					System.out.println("Out of range!");
			}
			while(score<0 || score>100);
			
			if(score>=0 && score<=25)
				level[0]++;
			else if(score>=26 && score<=50)
				level[1]++;
			else if(score>=51 && score<=75)
				level[2]++;
			else if(score>=76 && score<=100)
				level[3]++;
		}
		
		for(int i=0;i<=3;i++)
			System.out.println("Level " + i + " is " + level[i]*12.5 + "%");
		
		JFrame frame = new JFrame(); //宣告JFrame
		frame.setLayout(new GridLayout(1,4)); //使用GridLayout
		frame.setSize(1000,600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		for(int i=0;i<=3;i++) //加入Panel
			frame.add(new MyPanel(level[i],i));
	}

}
