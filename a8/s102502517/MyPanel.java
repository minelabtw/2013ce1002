package ce1002.a8.s102502517;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	public MyPanel(int level,int i)
	{
		setLayout(new GridLayout(3,1)); //使用GridLayout
		add(new BarChart(level));
		add(new JLabel("Level"+i));
		add(new JLabel(level*12.5+" %"));
	}
}
