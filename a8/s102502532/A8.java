package ce1002.a8.s102502532;

import java.util.Scanner;
import javax.swing.JFrame;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[] grades = new int[8];                       //陣列
		System.out.println("Input grades:");
		int[] levels = new int[4];

		for (int i = 0; i < 8; i++) {
			
			grades[i] = input.nextInt();
			while (grades[i] < 0 || grades[i] >100) {
				System.out.println("Out of range!");
				grades[i] = input.nextInt();
			}
			if(grades[i] >=0 && grades[i] <=25)
				levels[0]++;                                 //儲存人數
			else if(grades[i] >=26 && grades[i] <=50)
				levels[1]++;
			else if(grades[i] >=51 && grades[i] <=75)
				levels[2]++;
			else 
				levels[3]++;			
		}
		double[] presentage = new double[4];
		
		MyFrame frame = new MyFrame();
		MyPanel [] panels = new MyPanel[4];          
		
		frame.setSize(380, 230);          //尺寸
		
		for(int i = 0; i < 4; i++){
			
			presentage[i] = (double)levels[i] /8 *100;
			System.out.println("Level " + i + " is " + presentage[i] + "%");
			panels[i] = new MyPanel();
			frame.addpanel(panels[i], 0+ 90*i, 0);
			panels[i].setheight(presentage[i]);        //set 傳入值
			panels[i].setnum(i);
		}
		
		input.close();     //用不到       關掉
		
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}
}
