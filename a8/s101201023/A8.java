package ce1002.a8.s101201023;

import java.awt.Graphics;
import java.util.Scanner;

import ce1002.a8.s101201023.MyFrame;

public class A8 
{
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		double grade[]=new double [8];
		double L[]=new double [4];
		double percent[]=new double [4];

		Scanner input = new Scanner(System.in);
		
		//input
		System.out.println("Input grades: ");
		for(int i=0 ; i < 8 ; i++)
		{
			grade[i] = input.nextInt();
			while(grade[i] < 0 || grade[i] > 100)
			{
				System.out.println("Out of range!");
				grade[i] = input.nextInt();
			}
			
			if(grade[i] >= 0 && grade[i] <= 25)
				L[0]++;
			else if(grade[i] >= 26 && grade[i] <= 50)
			    L[1]++;
			else if(grade[i] >= 51 && grade[i] <= 75)
				L[2]++;
			else
				L[3]++;
		}
		
		//compute percentage
		for(int i=0 ; i < 4 ; i++)
		{
			percent[i] = (L[i]/(L[0]+L[1]+L[2]+L[3]))*100;
			System.out.println("Level " + i + " is " + percent[i] + "%");
		}
		
		//output
        MyFrame frame = new MyFrame();
		
		for(int i=0 ; i < 4 ; i++)
		{
			frame.graphshow(percent);
		}
	}
}
