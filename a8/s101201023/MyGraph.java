package ce1002.a8.s101201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyGraph extends JPanel
{
	private double height;
	
	//set MyGraph
	public MyGraph(double h)
	{
		height = h;
		
		setBounds(40,20,40,100);
	}
	
	//paint rectangent
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(0, 100-(int)height, 25, 50);
	}
}
