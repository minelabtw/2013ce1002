package ce1002.a8.s101201023;

import java.awt.Graphics;

import javax.swing.JFrame;

import ce1002.a8.s101201023.MyPanel;

public class MyFrame extends JFrame
{
	private MyPanel panel[] = new MyPanel[4];
	
	//set frame
	public MyFrame()   
	{
		setSize(600 , 600);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		setVisible(true);
	}
		
	//add MyPanel to frame
	public void graphshow(double h[])
	{
		for (int i=0;i<4;i++)
		{
			panel[i]=new MyPanel(h[i],i);
			panel[i].setBounds(20+100*i,0,120,220);
		}
		
		for (int i=0;i<4;i++)
		{
			add(panel[i]);
		}
	}
}
