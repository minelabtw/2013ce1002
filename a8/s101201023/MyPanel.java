package ce1002.a8.s101201023;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	private double height;
	private int level;
	protected JLabel LVLabel=new JLabel();
	protected JLabel PCLabel=new JLabel();
	
	//set panel(Label and graph)
	public MyPanel(double h , int l)
	{	
		height = h;
		level = l;
		
		LVLabel.setBounds(30,120,60,40);
		LVLabel.setText("Level " + level);
		
		PCLabel.setBounds(30,140,60,40);
		PCLabel.setText(height + "%");
		
		add(new MyGraph(height));
		add(LVLabel);
		add(PCLabel);
	}
}
