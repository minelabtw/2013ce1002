package ce1002.a8.s102502539;

import javax.swing.*;

public class MyFrame extends JFrame
{
	
	public MyFrame(int level0, int level1, int level2, int level3 )
	{

		setLayout(null);
		setSize(640, 360);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// 給 X 座標並畫出圖表
		newLevel( 80, 0, level0 );
		newLevel( 230, 1, level1 );
		newLevel( 380, 2, level2 );
		newLevel( 530, 3, level3 );
		setVisible(true);
		
	}
	
	public void newLevel( int x, int level, int number )
	{
		MyPanel panel = new MyPanel(x, level, number);
		panel.setLabel(level, number);
		this.add(panel);
	}
}
