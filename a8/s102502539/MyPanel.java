package ce1002.a8.s102502539;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel
{
	private int x;
	private int level;
	private int number;
	public JLabel lvLabel = new JLabel();
	public JLabel rateLabel = new JLabel();
	
	public MyPanel ( int x , int level , int number )
	{
		this.x = x;
		this.level = level;
		this.number = number;
		
		setLayout(null);
		setBounds( x - 80 , 0 , 150 , 300);
		lvLabel.setBounds( 70 , 230 , 150 , 20 );
		rateLabel.setBounds( 70 , 250 , 150 , 20 );
		add(lvLabel);
		add(rateLabel);
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		// ������
		g.setColor(Color.red);
		g.fillRect( 70, 20 + (8-number) * 25, 15, number * 25 );
		g.setColor(Color.black);
		g.drawRect( 70, 20, 15, 200);
	}
	
	public void setLabel(int level, int number)
	{
		lvLabel.setText("Level " + level);
		rateLabel.setText( (double)number * 12.5 + "%" );
	}

}
