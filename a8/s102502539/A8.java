package ce1002.a8.s102502539;

import java.util.Scanner;

public class A8 
{
	public static void main(String[] args) 
	{
		int grade;
		int level[] = new int [4];
		Scanner input = new Scanner(System.in);
	
		System.out.println("Input grades:");
	
		//輸入
		for ( int i = 0 ; i < 8 ; i++ )
		{
			do
			{
				grade = input.nextInt();
				if ( grade < 0 || grade > 100 )
					System.out.println("Out of range!");			
			} while ( grade < 0 || grade > 100 );
			
			if ( grade < 26 )
				level[0]++;
			else if ( grade < 51 )
				level[1]++;
			else if ( grade < 76 )
				level[2]++;
			else
				level[3]++;
		}
		
		//輸出
		for ( int i = 0 ; i < 4 ; i++ )
		{
			System.out.println("Level " + i + " is " + (double)level[i] * 12.5 + "%" );
		}
		
		//建立圖表
		MyFrame frame = new MyFrame(level[0], level[1], level[2], level[3]);
	}
}
