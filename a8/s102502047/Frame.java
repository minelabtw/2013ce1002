package ce1002.a8.s102502047;

import javax.swing.JFrame;

public class Frame extends JFrame{
	private double p;

	public Frame(double a,double b,double c,double d) {
		setLayout(null);
		setBounds(10,10,600,600);//設定框架
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		P1 p1 =new P1( a, b, c, d);//長條圖
		P2 p2 =new P2( a, b, c, d);//數據
		add(p1);
		add(p2);
		
	}
	
}
