package ce1002.a8.s102502047;

import java.awt.GridLayout;
import java.awt.Label;

import javax.swing.JPanel;

public class P2 extends JPanel{

	public P2(double a,double b,double c,double d) {
		setBounds(50,350,500,100);
		setLayout(new GridLayout(2,4,25,25));
		for(int i=0;i<4;i++)
			add(new Label("Level "+i));
		add(new Label(a*100+"%"));
		add(new Label(b*100+"%"));
		add(new Label(c*100+"%"));
		add(new Label(d*100+"%"));
	}

}
