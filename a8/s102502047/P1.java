package ce1002.a8.s102502047;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class P1 extends JPanel{
	private double a,b,c,d;
	public P1(double a,double b,double c,double d) {
		setLayout(null);
		setBounds(30,30,500,300);	
		this.a=a;
		this.b=b;
		this.c=c;
		this.d=d;
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(10,10,30,250);
		g.setColor(Color.RED);
		g.fillRect(10,10+(int)(250*(1-a)),30,(int)(250*a));
		g.setColor(Color.GRAY);
		g.fillRect(135,10,30,250);
		g.setColor(Color.RED);
		g.fillRect(135,10+(int)(250*(1-b)),30,(int)(250*b));
		g.setColor(Color.GRAY);
		g.fillRect(285,10,30,250);
		g.setColor(Color.RED);
		g.fillRect(285,10+(int)(250*(1-c)),30,(int)(250*c));
		g.setColor(Color.GRAY);
		g.fillRect(430,10,30,250);
		g.setColor(Color.RED);
		g.fillRect(430,10+(int)(250*(1-d)),30,(int)(250*d));
		
	}

}
