package ce1002.a8.ta;

import javax.swing.JFrame;


class MyFrame extends JFrame{
	protected MyPanel panel1 = new MyPanel();	
	protected MyPanel panel2 = new MyPanel();	
	protected MyPanel panel3 = new MyPanel();	
	protected MyPanel panel4 = new MyPanel();
	
	protected float[] percentageOfLevel;
	
	MyFrame(float[] percentageOfLevel) {
		this.percentageOfLevel = percentageOfLevel;
		
		//Set frame's size , layout...
		setLayout(null);
		setBounds(0, 0, 300, 200);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
				
		//Set level's position and percentage
		newLevelPos(percentageOfLevel[0],"0",panel1,10, 10);	//level 0
		newLevelPos(percentageOfLevel[1],"1",panel2,60, 10);	//level 1
		newLevelPos(percentageOfLevel[2],"2",panel3,110,10);	//level 2
		newLevelPos(percentageOfLevel[3],"3",panel4,160,10);	//level 3
	}
	public void newLevelPos(float percentage,String level,MyPanel panel , int x , int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 40, 150);
		
		/*set level which in the panel*/
		panel.setLevel(level);
		
		/*set percentage which in the panel*/
		panel.setPercentage(percentage);
		
		/*add panel to frame*/
		add(panel);
	}

}
