package ce1002.a8.ta;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int numberOfStudent=8;
		int numberOfLevel=4;
		int[] grade = new int[numberOfStudent];
		float[] percentageOfLevel = new float[numberOfLevel];

		Scanner input = new Scanner(System.in);

		//Input grade
		System.out.println("Input grades: ");
		for (int i=0 ; i < numberOfStudent ; i++)
		{
			do{
				grade[i]=input.nextInt();
				if(grade[i]<0 || grade[i]>100)
					System.out.println("Out of range!");
			}while(grade[i]<0 || grade[i]>100);
			
		}
		input.close();
		
		//Calculate percentage 	
		GradeSystem gs = new GradeSystem(numberOfStudent,numberOfLevel);
		percentageOfLevel = gs.calculatePercentage(grade);
		
		//Output each level
		for (int i=0; i < numberOfLevel ; i++){
			System.out.println("Level " + i + " is " + percentageOfLevel[i] + "%");
		}
		
		//Create a new frame
		MyFrame frame = new MyFrame(percentageOfLevel);
	}

}
