package ce1002.a8.ta;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private JLabel levelLabel = new JLabel("undefine");// level
	private JLabel percentageLabel = new JLabel("undefine");// percentage
	private float percentage;

	MyPanel()
	{		
		setLayout(null);
		
		//set label
		levelLabel.setBounds(0, 100, 100, 20);
		percentageLabel.setBounds(0, 120, 100, 20);
		
		//add label
		this.add(levelLabel);
		this.add(percentageLabel);

	}
	void setLevel(String level)
	{
		this.levelLabel.setText("Level " + level);
	}
	void setPercentage(float percentage)
	{
		this.percentage = percentage;
		this.percentageLabel.setText(percentage + "%");
	}

	@Override
	protected void paintComponent(Graphics g) {	//Draw graph
		// TODO Auto-generated method stub
		super.paintComponent(g);
		

		g.drawRect(0, 0, 10, 100);
		g.setColor(Color.red);
		g.fillRect(0, 0, 10, 100);

		//Use gray to cover red		
		g.drawRect(0, 0, 10,(int) (100-percentage));
		g.setColor(Color.gray);
		g.fillRect(0, 0, 10, (int) (100-percentage));
	    
	}

}

