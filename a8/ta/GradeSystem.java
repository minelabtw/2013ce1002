package ce1002.a8.ta;

class GradeSystem {
	private int numberOfStudent;
	private int numberOfLevel;
	private int grade[];
	private float countLevel[];
	private float percentageOfLevel[];
	
	GradeSystem(int numberOfStudent,int numberOfLevel) {
		this.numberOfStudent = numberOfStudent;
		this.numberOfLevel = numberOfLevel;
		countLevel = new float[numberOfLevel];
		percentageOfLevel = new float[numberOfLevel];
	}
	
	//Count level & Calculate percentage
	float[] calculatePercentage(int grade[]){
		this.grade = grade;
		
		//Count the number of students in each level
		for(int i = 0 ; i < numberOfStudent ; i++ )
		{
			if (grade[i] <= 25)
				countLevel[0]++;
			else if (grade[i] <= 50)
				countLevel[1]++;
			else if (grade[i] <= 75)
				countLevel[2]++;
			else
				countLevel[3]++;
		}
		
		//Calculate the percentage in each level
		for(int j = 0 ; j < numberOfLevel ; j++ )
		{
			percentageOfLevel[j] = 100*countLevel[j]/numberOfStudent;
		}

		return percentageOfLevel;
		
	}

}
