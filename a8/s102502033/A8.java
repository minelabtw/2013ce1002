package ce1002.a8.s102502033;

import java.util.Scanner;
import javax.swing.JFrame;

public class A8
{

	public static void main(String[] args)
	{
		int[] level = new int[4];//用陣列來存成績分布的值
		float grade;

		Scanner input = new Scanner(System.in);

		System.out.println("Input grades:");
		for (int i = 1; i <= 8; i++)
		{
			grade = input.nextInt();
			while (grade > 100 || grade < 0)
			{
				System.out.println("Out of range!");
				grade = input.nextInt();
			}
			if (grade >= 0 && grade <= 25)//作八次分類
			{
				level[0]++;
			} else if (grade >= 26 && grade <= 50)
			{
				level[1]++;
			} else if (grade >= 51 && grade <= 75)
			{
				level[2]++;
			} else
			{
				level[3]++;
			}
		}

		System.out.println("Level 0 is " + level[0] / 8.0 * 100 + "%");
		System.out.println("Level 1 is " + level[1] / 8.0 * 100 + "%");
		System.out.println("Level 2 is " + level[2] / 8.0 * 100 + "%");
		System.out.println("Level 3 is " + level[3] / 8.0 * 100 + "%");

		MyFrame frame = new MyFrame(level[0] / 8.0, level[1] / 8.0, level[2] / 8.0, level[3] / 8.0);//把值給FRAME
		frame.setSize(500, 500);//frame的基礎設定
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

}
