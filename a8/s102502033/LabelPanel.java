package ce1002.a8.s102502033;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LabelPanel extends JPanel
{
	public LabelPanel(double a, double b, double c, double d)
	{
		setLocation(5, 360);
		setSize(450, 40);
		setLayout(new GridLayout(2,4,5,1));//這樣就不用SET囉
		add(new JLabel("Level 0"));
		add(new JLabel("Level 1"));
		add(new JLabel("Level 2"));
		add(new JLabel("Level 3"));
		add(new JLabel(a*100+"%"));
		add(new JLabel(b*100+"%"));
		add(new JLabel(c*100+"%"));
		add(new JLabel(d*100+"%"));
		
	}

}
