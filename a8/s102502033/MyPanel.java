package ce1002.a8.s102502033;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	private double a;
	private double b;
	private double c;
	private double d;

	public MyPanel(double a, double b, double c, double d)
	{
		setLayout(null);
		setSize(600, 350);
		this.a = 1 - a;
		this.b = 1 - b;
		this.c = 1 - c;
		this.d = 1 - d;
	}

	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.pink);//畫底色
		g.fillRect(5, 5, 50, 350);
		g.fillRect(120, 5, 50, 350);
		g.fillRect(235, 5, 50, 350);
		g.fillRect(340, 5, 50, 350);
		g.setColor(Color.gray);//再畫黑色所以要1-趴值
		g.fillRect(5, 5, 50, (int) (350 * a));
		g.fillRect(120, 5, 50, (int) (350 * b));
		g.fillRect(235, 5, 50, (int) (350 * c));
		g.fillRect(340, 5, 50, (int) (350 * d));

	}
}
