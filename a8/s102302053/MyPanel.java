package ce1002.a8.s102302053;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	JLabel figure = new JLabel();
	JLabel title = new JLabel();
	JLabel value = new JLabel();
	int counter = 0;
	MyPanel(){//建構panel
		setLayout(null);
		title.setBounds(10,150,80,30);
		add(title);
		value.setBounds(10,185,80,30);
		add(value);
		add(figure);
	}


	public void setLabel(String level, double percent){//設定文字
		title.setText(level);
		title.setVerticalTextPosition(JLabel.BOTTOM);
		value.setText(percent + "%");
	}
	@Override
	public void paintComponent(Graphics g){//畫圖
		super.paintComponent(g);
		figure.setBounds(10,120,20,100);
		g.setColor(Color.red);
		g.drawRect(18, 50, 10, 100);
		g.fillRect(18, 50, 10, 100);
		g.setColor(Color.gray);
		double value;
		value = getValue();
		g.drawRect(18, 50, 10, (100 -(int)value));
		g.fillRect(18, 50, 10, (100 -(int)value));	
		
	}
	public double getValue(){//傳回要求的資料
		double tmp = 0;
		if(counter == 0){
			tmp = DataProccessing.level0;
		}
		if(counter == 1){
			tmp = DataProccessing.level1;
		}
		if(counter == 2){
			tmp = DataProccessing.level2;
		}
		if(counter == 3){
			tmp = DataProccessing.level3;
		}
		counter++;
		return tmp;
		
		
	}
	

}
