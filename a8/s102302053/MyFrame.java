package ce1002.a8.s102302053;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyPanel p0 = new MyPanel();
	MyPanel p1 = new MyPanel();
	MyPanel p2 = new MyPanel();
	MyPanel p3 = new MyPanel();
	MyFrame(){//設定整體框架並加入元素
		JFrame myFrame = new JFrame();
		
		panelSet(p0,"Level 0", DataProccessing.level0,10,10);
		myFrame.add(p0);
		panelSet(p1,"Level 1", DataProccessing.level1,30,10);
		myFrame.add(p1);
		panelSet(p2,"Level 2", DataProccessing.level2,50,10);
		myFrame.add(p2);
		panelSet(p3,"Level 3", DataProccessing.level3,70,10);
		myFrame.add(p3);
		setLayout(null);
		myFrame.setSize(600, 300);
		myFrame.setLayout(new GridLayout(1, 4, 10, 0 ));
		myFrame.setVisible(true);
		myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	public void panelSet(MyPanel panel, String level, double percent,int x,int y){//設置panel
		panel.setLabel(level,percent);
		
		panel.setBounds(x,y,30,200);
		
		
		
	}
	

}
