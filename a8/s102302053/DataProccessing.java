package ce1002.a8.s102302053;

import java.util.Scanner;

public class DataProccessing {
	static double level0 = 0;
	static double level1 = 0;
	static double level2 = 0;
	static double level3 = 0;
	
	
	public void gradeSetterAndTextDisplay(){//要求使育者輸入符合條件的資料並且輸出結果
		int counter = 0;
		int tmp;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input grades: ");
		do{
			tmp = scanner.nextInt();
			if (tmp <0 || tmp > 100){
				System.out.println("Out of range!");
			}
			else if (tmp>= 0 && tmp <=25){
				level0++;
				counter++;
			}
			else if (tmp>= 26 && tmp <=50){
				level1++;
				counter++;
			}
			else if (tmp>= 51 && tmp <=75){
				level2++;
				counter++;
			}
			else if (tmp>= 76 && tmp <=100){
				level3++;
				counter++;
			}
		}while(counter<8);
		
		level0 = (level0/8)*100;
		level1 = (level1/8)*100;
		level2 = (level2/8)*100;
		level3 = (level3/8)*100;
		
		System.out.println("Level 0 is " + level0 + "%" + "\nLevel 1 is " + level1 + "%" + "\nLevel 2 is " + level2 + "%" + "\nLevel 3 is " + level3 + "%" );
		
	}
	

}
