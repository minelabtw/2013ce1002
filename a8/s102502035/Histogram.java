package ce1002.a8.s102502035;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Histogram extends JPanel {
	int[] Level = new int[8];

	public Histogram(int[] level) {
		setSize(400, 250);
		setLayout(new BorderLayout());
		setVisible(true);
	}

	public void setDetail(int[] level) {// set bar detail
		this.Level = level;
	}

	public void paint(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(80, 10, 30, 200);
		g.fillRect(160, 10, 30, 200);
		g.fillRect(240, 10, 30, 200);
		g.fillRect(320, 10, 30, 200);
		g.setColor(Color.RED);
		g.fillRect(80, 10 + ((8 - Level[0]) * 25), 30, Level[0] * 25);
		g.fillRect(160, 10 + ((8 - Level[1]) * 25), 30, Level[1] * 25);
		g.fillRect(240, 10 + ((8 - Level[2]) * 25), 30, Level[2] * 25);
		g.fillRect(320, 10 + ((8 - Level[3]) * 25), 30, Level[3] * 25);
	}
}
