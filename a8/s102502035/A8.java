package ce1002.a8.s102502035;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[] grade = new int[8];
		int[] Level = new int[4];
		System.out.println("Input grades: ");
		for (int i = 0; i < 8; i++) {
			grade[i] = input.nextInt();
			while (grade[i] > 100 || grade[i] < 0) {
				System.out.println("Out of range!");
				grade[i] = input.nextInt();
			}
			if (grade[i] >= 0 && grade[i] < 25 && grade[i] / 25 == 0)
				Level[0]++;
			else if (grade[i] % 25 >= 0 && grade[i] % 25 < 25
					&& grade[i] / 25 == 1)
				Level[1]++;
			else if (grade[i] % 25 >= 0 && grade[i] % 25 < 25
					&& grade[i] / 25 == 2)
				Level[2]++;
			else if ((grade[i] % 25 >= 0 && grade[i] % 25 < 25 && grade[i] / 25 == 3)
					|| grade[i] == 100)
				Level[3]++;
		}

		System.out.println("Level 0 is " + (Level[0] * 12.5) + "%");
		System.out.println("Level 1 is " + (Level[1] * 12.5) + "%");
		System.out.println("Level 2 is " + (Level[2] * 12.5) + "%");
		System.out.println("Level 3 is " + (Level[3] * 12.5) + "%");
		JFrame frame = new JFrame();
		frame.setSize(400, 300);
		frame.setLayout(new BorderLayout(5, 10));
		Histogram panel = new Histogram(Level);
		panel.setDetail(Level);
		frame.add(panel, BorderLayout.CENTER);
		JPanel panel2 = new JPanel();
		{// set label detail
			panel2.setLayout(new GridLayout(2, 5, 10, 10));
			panel2.add(new JLabel());
			panel2.add(new JLabel("Level 0"));
			panel2.add(new JLabel("Level 1"));
			panel2.add(new JLabel("Level 2"));
			panel2.add(new JLabel("Level 3"));
			panel2.add(new JLabel());
			panel2.add(new JLabel(Level[0] * 12.5 + "%"));
			panel2.add(new JLabel(Level[1] * 12.5 + "%"));
			panel2.add(new JLabel(Level[2] * 12.5 + "%"));
			panel2.add(new JLabel(Level[3] * 12.5 + "%"));
		}
		frame.add(panel2, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}
}
