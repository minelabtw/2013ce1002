package ce1002.a8.s102502020;

import javax.swing.*;

import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel{
	
	private String level;
	private int percent;
	
	JLabel Level = new JLabel();
	JLabel Percent = new JLabel();

	public MyPanel() {
		// TODO Auto-generated constructor stub
		setLayout(null);
		Level.setBounds(10, 120,40,30);
		Percent.setBounds(10, 160 ,30,30);
		this.add(Level);
		this.add(Percent);
	}
	@Override                                              //�e�Ϫ�
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(10,10,10,100);
		g.setColor(Color.RED);
		g.drawRect(10, 110-percent, 10, percent);
		g.setColor(Color.RED);
	}
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
		this.Level.setText("Level " + level);
	}
	public int getPercent() {
		return percent;
	}
	public void setPercent(int percent) {
		this.percent = percent;
		this.Percent.setText(percent + "%");
	}
	
	
	

}
