package ce1002.a8.s102502020;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] grade = new int[8];               //宣告陣列
		double[] number = new double[4];
		double[] Level = new double[4];
		System.out.println("Input grades: ");
		for(int i = 0; i < 8; i++){             //輸入8個成績
			do{
				Scanner input = new Scanner(System.in);
				grade[i] = input.nextInt();
				if(grade[i]<0 || grade[i]>100){
					System.out.println("Out of range!");
				}
			}while(grade[i]<0 || grade[i]>100);
			if(grade[i]<=25){
				number[0]++;
			}
			else if(grade[i]<=50){
				number[1]++;
			}
			else if(grade[i]<=75){
				number[2]++;
			}
			else{
				number[3]++;
			}
		}
		
		for(int i = 0; i < 4; i++){
			Level[i] = (number[i]/(number[0] + number[1] + number[2] + number[3]))*100;
			System.out.println("Level "+ i + " is " + Level[i] + "%");                    //書出4個區間的百分比
			
		}
		MyFrame frame = new MyFrame();

	}

}
