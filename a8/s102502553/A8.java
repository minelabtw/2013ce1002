package ce1002.a8.s102502553;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		int arr[] = new int[8];//8個數字
		double level[] = new double[4];//4個等級
		int grade;
		
		for(int i = 0;i < 8;i++)//輸入成績
		{
			do
			{
				System.out.println("Input grades:");
				grade = input.nextInt();
				if(grade > 100 || grade < 0)
					System.out.println("Out of range!");
			}
			while(grade > 100 || grade < 0);
			
			if(grade <= 25)
				level[0]++;
			else if(grade <= 50)
				level[1]++;
			else if(grade <= 75)
				level[2]++;
			else
				level[3]++;
		}
		
		for(int i = 0;i < 4;i++)//輸出
		{
			System.out.println("Level " + i +" is " + level[i]/8*100 + "%");
		}
		
		MyFrame frame = new MyFrame(level);
	}
}
