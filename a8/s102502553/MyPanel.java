package ce1002.a8.s102502553;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	public static final LayoutManager RECTANGLE = null;
	double arr;//存level的次數
	
	MyPanel(double level[] , int i)
	{
		setSize(100 , 600);
		setLocation(i * 95 , 5);
		setLayout(null);
		arr = level[i];
		
		JLabel l1 = new JLabel("Level " + i);
		JLabel l2 = new JLabel("" + level[i] / 8 * 100 + "%");//建立標籤，並存入圖片
		 
		l1.setSize(100 , 50);//設長寬
	    l1.setLocation(10 , 390);//設初始位置
		
		l2.setSize(100 , 50);//設長寬
		l2.setLocation(10 , 410);//設初始位置
		 
		add(l1);//貼上標籤
		add(l2);
	}

	protected void paintComponent(Graphics g)//畫圖
	{
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(10 , 10 , 30 , 300);
		g.setColor(Color.RED);
		g.fillRect(10 , (int)(10 + 300 * (1 - (arr / 8))) , 30 , (int)(arr  /8 * 300));
	}
	
}
