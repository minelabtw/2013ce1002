package ce1002.a8.s102502554;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	
	protected JLabel sl0 = new JLabel();
	protected JLabel sl1 = new JLabel();
	protected JLabel sl2 = new JLabel();
	protected JLabel sl3 = new JLabel();
	protected JLabel title = new JLabel();//5 panels to represent the states
	
	public double level[] = new double [4];
	
	
	MyPanel (){
		setBounds(0, 0, 300, 300);//set the bounds of the panel
		setLayout(null);
		title.setText("Level 0      Level 1          Level 2          Level 3");
		title.setBounds(5,105,250,20);
		sl0.setBounds(5,135,50,20);
		sl1.setBounds(75,135,50,20);
		sl2.setBounds(145,135,50,20);
		sl3.setBounds(215,135,50,20);//set the site of each title
	}
	
	     public void paintComponent (Graphics g){
			
			super.paintComponent(g);
			g.setColor(Color.gray);
			
			g.fillRect(5,5,10,100);
			g.fillRect(75,5,10,100);
			g.fillRect(145,5,10,100);
			g.fillRect(215,5,10,100);
			
			g.setColor(Color.red);
			g.fillRect(5,105-(int)level[0],10,(int)level[0]);
			g.fillRect(75,105-(int)level[1],10,(int)level[1]);
			g.fillRect(145,105-(int)level[2],10,(int)level[2]);
			g.fillRect(215,105-(int)level[3],10,(int)level[3]);
				
			
			};//draw rectangles for each level
			
}
