package ce1002.a8.s102502554;

import java.util.Scanner;
import javax.swing.*;
import java.awt.*;

public class A8 {
	public static void main (String [] args) {
		int [] grade ;
		grade = new int [8];
		Scanner input = new Scanner(System.in);//creat a scanner object
		int number = 0;
		System.out.println("Input grades: ");
		for (int i = 0 ; i < 8 ; i ++){
			number = input.nextInt();
			while ( number < 0 || number > 100){
				System.out.println("Out of range! ");
				number = input.nextInt();
			}
			grade [i] = number;
		}//enter the grades
		
		double l0 = 0;
		double l1 = 0;
		double l2 = 0;
		double l3 = 0;
		
		for (int i = 0 ; i < 8 ; i ++){
			if ( grade [i] >= 0 && grade [i] <=25  )
				l0++;
			else if ( grade [i] >= 26 && grade [i] <=50 )
				l1++;
			else if ( grade [i] >= 51 && grade [i] <=75 )
				l2++;
			else if ( grade [i] >= 76 && grade [i] <=100 )
				l3++;
		}//define the percentage of each level
		
		System.out.println("Level 0 is "+(l0/8)*100+"%");
		System.out.println("Level 1 is "+(l1/8)*100+"%");
		System.out.println("Level 2 is "+(l2/8)*100+"%");
		System.out.println("Level 3 is "+(l3/8)*100+"%");//print the percentage
		
		MyPanel P = new MyPanel();
		P.level[0]= (l0/8)*100;
		P.level[1]= (l1/8)*100;
		P.level[2]= (l2/8)*100;
		P.level[3]= (l3/8)*100;
		P.sl0.setText(P.level[0]+"%");
		P.sl1.setText(P.level[1]+"%");
		P.sl2.setText(P.level[2]+"%");
		P.sl3.setText(P.level[3]+"%");
		P.add(P.title);
		P.add(P.sl0);
		P.add(P.sl1);
		P.add(P.sl2);
		P.add(P.sl3);//send the value into MyPanel
		
		MyFrame frame = new MyFrame();
		frame.add(P);//plus the Panel in the frame
		
	}

}
