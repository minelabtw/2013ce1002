package ce1002.a8.s102502050;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int []number ={0,0,0,0};
		
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++)
		{
			//輸入8個數並檢查
			int input;
			do
			{
				
				input = scanner.nextInt();
				if(input>100 || input<0)
					System.out.println("Out of range!");
			}
			while(input>100 || input<0);
			if(input!= 100)
				number[input/25]++;
			else
				number[3]++;
				
		}
		
		for(int i=0;i<4;i++)
			System.out.println("level " + i + " is " +(double)12.5*number[i] +" %");
		MyFrame frame = new MyFrame(number);
	}

}
