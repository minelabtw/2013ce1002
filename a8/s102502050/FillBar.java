package ce1002.a8.s102502050;

import java.awt.*;

import javax.swing.*;
public class FillBar extends JPanel{
	private int length;
	private double sum;
	protected JLabel level,rate;
	public FillBar(int length, int number)
	{
		setLayout(null);
		this.length=length;
		sum = (double)12.5*length;
		//計算百分比
		
		level = new JLabel("Level" + number);
		rate = new JLabel(sum+"%");
		
		level.setBounds(20,90,80,20);
		rate.setBounds(20,110,80,20);
		add(level);
		add(rate);
		setVisible(true);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(20, 0, 20, 80);
		//產生空白的長條
		
		g.setColor(Color.RED);
		g.fillRect(20, 80- length*10, 20, length*10);
		//依比例填滿長條
		
	}
}
