package ce1002.a8.s102502050;

import javax.swing.*;
public class MyFrame extends JFrame{
	protected FillBar []fillbar = new FillBar[4];
	public MyFrame(int [] number)
	{
		setLayout(null);
		setBounds(100,100,800,450);

		for(int i=0;i<4;i++)
		{
			fillbar[i] = new FillBar(number[i],i);
			fillbar[i].setBounds(100+i*150,50,100,400);
			add(fillbar[i]);
		}
		
		setVisible(true);
	}

}
