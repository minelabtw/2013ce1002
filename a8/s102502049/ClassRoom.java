package ce1002.a8.s102502049;


public class ClassRoom {
	protected float[] grades = new float[8];
	private Student[] students = new Student [8];
	private float[] levels = new float[4];
	private float[] percent = new float[4];
	
	ClassRoom () {
		
	}
	
	ClassRoom(float[] grades){
		for(int i=0; i<8; i++){ // construct students
			students[i] = new Student();
			students[i].setGrade(grades[i]); // set student grade
			students[i].setLevel();
			selectLevel(students[i].getLevel()); // selectLevel
		}
		
		for(int i=0; i<4; i++){
			percent[i] = Math.round((levels[i]/8)*10000)/100; // set percent
		}
	}
	
	public void selectLevel(int level){ // add levels
		switch(level){
		case 0:
			levels[0]++;
			break;
		case 1:
			levels[1]++;
			break;
		case 2:
			levels[2]++;
			break;
		case 3:
			levels[3]++;
			break;
		}
	}
	
	public float[] getLevels(){
		return levels;
	}
	
	public Student[] getStudents(){
		return students;
	}
	
	public float[] getPercents(){ // percents array getter
		return percent;
	}
}
