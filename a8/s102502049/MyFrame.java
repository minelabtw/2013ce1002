package ce1002.a8.s102502049;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	private MyPanel panel0;
	private MyPanel panel1;
	private MyPanel panel2;
	private MyPanel panel3; // four panel
	
	protected ClassRoom myclass;
	
	MyFrame(ClassRoom myclass){
		this.myclass = myclass;
		panel0 = new MyPanel(myclass.getPercents()[0],0);
		panel1 = new MyPanel(myclass.getPercents()[1],1);
		panel2 = new MyPanel(myclass.getPercents()[2],2);
		panel3 = new MyPanel(myclass.getPercents()[3],3); // set percent to panel
		
		setLayout(null);
		panel0.setBounds(10, 10, 100, 185);
		panel1.setBounds(120, 10, 100, 185);
		panel2.setBounds(230, 10, 100, 185);
		panel3.setBounds(340, 10, 100, 185); // set panel position
		add(panel0);
		add(panel1);
		add(panel2);
		add(panel3); // add panels
	}
	
	
}
