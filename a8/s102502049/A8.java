package ce1002.a8.s102502049;

import java.util.Scanner;

import javax.swing.JFrame;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		float[] grades = new float[8];
		
		for(int i=0; i<8; i++){ //set 8 students grade
			grades[i] = input.nextFloat();
			if(grades[i]<0 || grades[i]>100 ){
				System.out.println("Out of range!");
				i--;
			}
		}
		
		ClassRoom myclass = new ClassRoom(grades); // set grade in students
		for(int i=0; i<4; i++){
			System.out.println("Level " + i + " is " + myclass.getPercents()[i] + "%"); // print percent
		}
		
		
		MyFrame frame = new MyFrame(myclass);
		frame.setSize(470, 245);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
