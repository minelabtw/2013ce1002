package ce1002.a8.s102502049;

import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	private float posHeight = 125; // chart's length
	private float percent = 0;
	private int index; // level i
	
	JLabel level = new JLabel();
	JLabel percentJLabel = new JLabel();
	
	MyPanel(float percent, int index) {
		this.percent = percent;
		this.index = index; // save to set labelText
		//setBorder(BorderFactory.createLineBorder(Color.black,2)); // create the borderline
		
		level.setText("Level " + index); // set labelText
		percentJLabel.setText(""+ percent + "%");
		
		add(level); // add two label
		add(percentJLabel);
	}
	
	protected void paintComponent(Graphics g) { // paint chart and labels
		super.paintComponent(g);
		
		g.setColor(Color.gray);
		g.fillRect(15, 10, 10, (int)(posHeight*(1-percent/100)));
		level.setBounds(15, 145, 180, 10);
		
		g.setColor(Color.red);
		g.fillRect(15, 10+(int)(posHeight*(1-percent/100)), 10, (int)(posHeight*(percent/100)));
		percentJLabel.setBounds(15, 165, 180, 10);
	}
}
