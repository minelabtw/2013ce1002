package ce1002.a8.s102502049;

public class Student extends ClassRoom {
	private float grade = 0;
	private int level = 0;

	Student() {
		
	}

	public void setLevel() { // classify level
		if (0 <= grade && grade <= 25) // level setter
			level = 0;
		else if (26 <= grade && grade <= 50)
			level = 1;
		else if (51 <= grade && grade <= 75)
			level = 2;
		else if (76 <= grade && grade <= 100)
			level = 3;
	}
	
	public int getLevel() { // level getter
		return level;
	}
	
	public void setGrade(float grade){ // grade setter
		this.grade = grade;
	}

	public float getGrade() { // grade getter
		return grade;
	}

}
