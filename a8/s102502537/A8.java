package ce1002.a8.s102502537;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		double[] level = new double[4];
		int grade;
		Scanner input = new Scanner(System.in);
		
		//存數字
		System.out.println("Input grades:");
		for(int i = 0 ; i < 8 ; i++){	
			do{
				grade = input.nextInt();
				if(grade < 0 || grade > 100){
					System.out.println("Out of range!");
				}
			}while(grade < 0 || grade > 100);
			
			if(grade >= 0 && grade <= 25){
				level[0]++;
			}else if(grade >= 26 && grade <= 50){
				level[1]++;
			}else if(grade >= 51 && grade <= 75){
				level[2]++;
			}else if(grade >= 76 && grade <= 100){
				level[3]++;
			}
		}
		
		input.close();
		
		//輸出趴數
		for(int i = 0 ; i < 4 ; i++){
			System.out.println("Level " + i + " is " + (level[i] / 8) * 100 + "%");
		}
		
		//印出表格
		MyFrame myframe = new MyFrame(level);
		
	}

}
