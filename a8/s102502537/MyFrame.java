package ce1002.a8.s102502537;

import javax.swing.JFrame;
import java.awt.GridLayout;

public class MyFrame extends JFrame{	
	
	MyFrame(double[] grade){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true); 
		setSize(500 , 300);
		setLayout(null);
		
		//設定panel
		MyPanel p0 = new MyPanel(0 , (grade[0] / 8) * 100);
		MyPanel p1 = new MyPanel(1 , (grade[1] / 8) * 100);
		MyPanel p2 = new MyPanel(2 , (grade[2] / 8) * 100);
		MyPanel p3 = new MyPanel(3 , (grade[3] / 8) * 100);
		
		p0.setBounds(0, 10, 50, 300);
		p1.setBounds(60, 10, 50, 300);
		p2.setBounds(120, 10, 50, 300);
		p3.setBounds(180, 10, 50, 300);
		
		//把panel加上去
		add(p0);
		add(p1);
		add(p2);
		add(p3);
	}
	
}

