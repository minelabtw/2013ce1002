package ce1002.a8.s102502537;

import javax.swing.*;

import java.awt.*;

public class MyPanel extends JPanel{
	JLabel level;
	JLabel lp;
	double percent;
	
	MyPanel(int level , double percent){
		setLayout(null);
		
		this.percent = percent;
		
		//設label
		this.level = new JLabel("Level " + level);
		this.level.setBounds(10 , 110 , 40 , 20);
		
		lp = new JLabel(percent + "%");
		lp.setBounds(10 , 130 , 40 , 20);
		
		add(this.level);
		add(lp);
	}
	
	protected void paintComponent(Graphics g){
		g.setColor(Color.GRAY);
		g.fillRect(10, 0, 30, 100);
		g.setColor(Color.RED);
		g.fillRect(10, 100 - (int)(percent), 30, (int)(percent));
	}
	
}
