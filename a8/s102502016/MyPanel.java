package ce1002.a8.s102502016;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	MyPanel() {
		setLayout(null);//非預設
		setSize(100, 600);//一個panel的size
	}

	public void setRectangle(double y, int l) {//panel中的function
		JLabel name = new JLabel();
		name.setSize(100, 20);
		name.setLocation(0, 170);
		name.setText("Level " + l);
		add(name);//家到panel上

		JLabel gray = new JLabel();
		gray.setSize(20, (int) (100 - y));
		gray.setBackground(Color.GRAY);
		gray.setLocation(0, 50);
		add(gray);
		gray.setOpaque(true);//不透明

		JLabel red = new JLabel();
		red.setSize(20, (int) y);
		red.setBackground(Color.RED);
		red.setLocation(0, (int) (150 - y));//轉int
		add(red);
		red.setOpaque(true);

		JLabel name1 = new JLabel();
		name1.setSize(100, 20);
		name1.setLocation(0, 190);
		name1.setText(y + "%");
		add(name1);

	}
}