package ce1002.a8.s102502016;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	public MyPanel mp1, mp2, mp3, mp4;//4個panel(leve0~3)

	MyFrame() {
		setLayout(null);
		setSize(600, 600);//整個frame(框架)的大小
		setDefaultCloseOperation(EXIT_ON_CLOSE);//可關閉
	}

	public void getRectangle(int x, double y, int l, MyPanel mp) {//frame中的function
		mp = new MyPanel();
		mp.setRectangle(y, l);
		mp.setLocation(x, 190);
		add(mp);//加到frame
		setVisible(true);//可以跑出來
	}
}
