package ce1002.a8.s102502516;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected JLabel labelT = new JLabel();
	protected JLabel labelPersent = new JLabel();
	protected double persent[];
	protected int i;

	MyPanel(double persent[], int i) {
		setLayout(null);
		this.persent = persent;
		this.i = i;
		int x, y, w, h = 0;
		x = 20;
		y = (int) (20 + 160 * (1 - (persent[i] / 100)));
		w = 60;
		h = 180 - y; // 設定長條圖大小
		MyHistogram histogram = new MyHistogram(persent[i], i, 1);
		histogram.setBounds(x, 20, w, y - 20);
		add(histogram); // 先鋪上長條圖底色

		histogram = new MyHistogram(persent[i], i, 0);
		histogram.setBounds(x, y, w, h);
		add(histogram); // 再放入長條圖

		labelT.setBounds(x + 10, 200, 80, 15);
		labelPersent.setBounds(x + 13, 225, 80, 10);
		add(labelT);
		add(labelPersent); //放入文字

	}

	public void setLabelText() {
		labelT.setText("Level " + i);
		labelPersent.setText(persent[i] + "%"); // 設定文字
	}
}
