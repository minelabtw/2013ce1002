package ce1002.a8.s102502516;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyHistogram extends JPanel {
	protected double persent;
	protected int i, j = 0;

	MyHistogram(double persent, int i, int j) {
		this.persent = persent;
		this.i = i;
		this.j = j;
	}

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponents(g);
		if (j == 1)
			g.setColor(Color.lightGray); //底圖灰色
		else
			g.setColor(Color.red); // 長條圖紅色
		g.fillRect(0, 0, 1000, 1000); 
	}
}
