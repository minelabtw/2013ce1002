package ce1002.a8.s102502516;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel[] levelPanels = new MyPanel[4];

	MyFrame(double[] persent) {
		setLayout(null);
		setBounds(800, 600, 400 + 20, 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE); // 視窗相關設定

		for (int i = 0; i < persent.length; i++) {
			setPanels(persent, i); // 分別設定四個分類
		}
	}

	public void setPanels(double[] persent, int i) {
		levelPanels[i] = new MyPanel(persent, i); // 實體化
		levelPanels[i].setBounds(100 * i, 0, 100, 300); // 邊界
		levelPanels[i].setLabelText(); // 設定文字
		add(levelPanels[i]);
	}
}
