package ce1002.a8.s102502516;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int grades[] = new int[8];
		int level[] = new int[4];
		double persent[] = new double[4];
		int i = 0; // 計數器

		System.out.println("Input grades: ");
		do {
			grades[i] = scanner.nextInt();
			if (grades[i] < 0 || grades[i] > 100)
				System.out.println("Out of range!");
			else
				i++;
		} while (i < 8);
		scanner.close(); // 取得八個成績

		for (int j = 0; j < grades.length; j++) {
			if (grades[j] >= 76)
				level[3]++;
			else if (grades[j] >= 51)
				level[2]++;
			else if (grades[j] >= 26)
				level[1]++;
			else
				level[0]++;
		} // 分類成績的對應級別

		for (int j = 0; j < level.length; j++) {
			persent[j] = level[j] / (double) grades.length * 100;
			System.out.println("Level " + j + " is " + persent[j] + "%");
		} // 輸出

		MyFrame frame = new MyFrame(persent);

	}
}
