package ce1002.a8.s102502533;

import javax.swing.*;
import java.awt.*;

public class Mypanel extends JPanel {
	protected double[] level = new double[4];

	Mypanel(int[] level) {
		for (int k = 0; k < 4; k++) {//把int 改成double
			this.level[k] = (double) level[k] / 8 * 100;
		}
	}

	protected void paintComponent(Graphics g) {//畫矩形
		super.paintComponent(g);
		for (int i = 0; i < 4; i++) {
			g.setColor(Color.white);
			g.fillRect(70 + 50 * i, 50, 30, (200 - (2 * (int) this.level[i])));
			g.setColor(Color.red);
			g.fillRect(70 + 50 * i, 50 + (200 - 2 * (int) this.level[i]), 30,
					(2 * (int) this.level[i]));
		}
		for(int k=0;k<4;k++){//印知料
			JLabel jl = new JLabel("<html>LEVEL " + k + "<br> " + this.level[k]+ "%<html>");
			jl.setBounds(70+50*k, 250, 100, 50);
			add(jl);
		}
	}

}
