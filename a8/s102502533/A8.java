package ce1002.a8.s102502533;
import java.util.Scanner;
public class A8 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int grade;
		int [] grades = new int [8];
		int [] level = new int [4];
		
		System.out.println("Input grades: ");
		for(int i =0;i < 8;i++){//輸入成績
			do{
			grade = input.nextInt();
			if(grade < 0 || grade > 100)
				System.out.println("Out of range!");
			}while(grade < 0 || grade >100);
			grades[i] = grade;
		}
		input.close();//關掉input
		for(int k = 0;k < 8;k++){//計算各區間的數目
			if(grades[k]<=25 && grades[k]>=0)
				level[0]++;
			if(grades[k]<=50 && grades[k]>=25)
				level[1]++;
			if(grades[k]<=75 && grades[k]>=50)
				level[2]++;
			if(grades[k]<=100 && grades[k]>=75)
				level[3]++;
		}
		Myframe mf = new Myframe(level);//計算各區間的%數
		System.out.println("Level 0 is "+(double)level[0]/8*100+"%");
		System.out.println("Level 1 is "+(double)level[1]/8*100+"%");
		System.out.println("Level 2 is "+(double)level[2]/8*100+"%");
		System.out.println("Level 3 is "+(double)level[3]/8*100+"%");
	}


}
