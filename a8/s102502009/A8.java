package ce1002.a8.s102502009;

import java.util.Scanner;
import ce1002.a8.s102502009.MyFrame;

public class A8 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int[] grade = new int[8];
		float LEVEL1 = 0;
		float LEVEL2 = 0;
		float LEVEL3 = 0;
		float LEVEL4 = 0;
		System.out.println("Input grades: ");
		for (int i = 0; i < 8; i++) {
			do {
				grade[i] = scanner.nextInt();
				if (grade[i] > 100 || grade[i] < 0)
					System.out.println("Out of range!");
			} while (grade[i] > 100 || grade[i] < 0);
			if (grade[i] >= 0 && grade[i] <= 24) { // 分LEVEL
				LEVEL1 = LEVEL1 + 1;
			} else if (grade[i] >= 25 && grade[i] <= 50) {
				LEVEL2 = LEVEL2 + 1;
			} else if (grade[i] >= 51 && grade[i] <= 75) {
				LEVEL3 = LEVEL3 + 1;
			} else {
				LEVEL4 = LEVEL4 + 1;
			}
		}
		System.out.println("Level 0 is " + (LEVEL1 / 8) * 100 + "%");
		System.out.println("Level 1 is " + (LEVEL2 / 8) * 100 + "%");
		System.out.println("Level 2 is " + (LEVEL3 / 8) * 100 + "%");
		System.out.println("Level 3 is " + (LEVEL4 / 8) * 100 + "%");
		MyFrame mainframe = new MyFrame();
		mainframe.setPanel(0, LEVEL1 / 8 * 100, mainframe.panel1, 0, 0); // 設置GUI
		mainframe.setPanel(1, LEVEL2 / 8 * 100, mainframe.panel2, 60, 0);
		mainframe.setPanel(2, LEVEL3 / 8 * 100, mainframe.panel3, 120, 0);
		mainframe.setPanel(3, LEVEL4 / 8 * 100, mainframe.panel4, 180, 0);
		scanner.close();
	}
}
