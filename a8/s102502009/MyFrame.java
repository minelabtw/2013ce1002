package ce1002.a8.s102502009;

import ce1002.a8.s102502009.MyPanel;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {
	public MyPanel panel1 = new MyPanel();
	public MyPanel panel2 = new MyPanel();
	public MyPanel panel3 = new MyPanel();
	public MyPanel panel4 = new MyPanel();

	public MyFrame() {
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setSize(300, 300); // 大小
	}

	public void setPanel(int a, float b, MyPanel panel, int x, int y) {
		panel.setBar(a, b); // 傳入 %.level
		panel.setLocation(x, y);
		this.add(panel); // 加到FRAME裏面
		this.setVisible(true);
	}
}