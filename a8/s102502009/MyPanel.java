package ce1002.a8.s102502009;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel {
	public JLabel label1;
	public JLabel label2;
	public JLabel label3;
	public JLabel label4;

	public MyPanel() {
		this.setLayout(null);
		this.setSize(60, 150); // 大小
	}

	public void setBar(int a, float b) {
		label1 = new JLabel(); // NEW
		label1.setBackground(Color.RED);
		label1.setSize(10, (int) (110 - b));
		label1.setLocation(10, 10);
		label1.setOpaque(true);// 不透明
		this.add(label1); // 加到Panel裏面
		label2 = new JLabel();
		label2.setBackground(Color.BLUE);
		label2.setSize(10, (int) b);
		label2.setLocation(10, (int) (120 - b));
		label2.setOpaque(true);
		this.add(label2);
		label3 = new JLabel();
		label3.setText("Level " + a);
		label3.setSize(50, 10);
		label3.setLocation(10, 125);
		this.add(label3);
		label4 = new JLabel();
		label4.setText(b + "%");
		label4.setSize(50, 10);
		label4.setLocation(10, 135);
		this.add(label4);
	}
}