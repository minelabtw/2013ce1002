package ce1002.a8.s102502041;
import java.awt.Color;

import javax.swing.*;
public class MyPanel extends JPanel {
	protected JLabel label = new JLabel();
	protected JLabel labelchart1 = new JLabel();
	protected JLabel labelchart2 = new JLabel();
	MyPanel()
	{
		setLayout(null);
		label.setBounds(0, 0, 100, 20);
	}
	public void setLevelState(int x, int y, int i,float per)
	{
		setBounds(x,y,500,20);
		label.setText("Level "+i+" "+per+"%");
		add(label);
	}
	public void setChartState(int x, int y,float a)
	{
		labelchart1.setBounds(x, y, 350, 20);
		labelchart2.setBounds(x, y, (int)(a*350), 20);
		labelchart1.setOpaque(true);
		labelchart2.setOpaque(true);
		labelchart1.setBackground(Color.white);
		labelchart2.setBackground(Color.red);

		add(labelchart2);
		add(labelchart1);
	}

}
