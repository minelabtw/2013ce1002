package ce1002.a8.s102502041;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		// Input grades
		System.out.println("Input grades:");
		float[] grades = new float[8];
		for(int i=0; i<8; i++)
		{
			float grade;
			do{
				grade = input.nextFloat();
				if(grade<0||grade>100) System.out.println("Out of range!");
			}while(grade<0||grade>100);
			grades[i]=grade;
		}
		// Classifying
		float count0=0,count1=0,count2=0,count3=0;
		for(int i=0; i<8; i++)
		{
			if(grades[i]>=0&&grades[i]<=25)count0+=1;
			else if(grades[i]>=26&&grades[i]<=50)count1+=1;
			else if(grades[i]>=51&&grades[i]<=75)count2+=1;
			else count3+=1;
		}
		// Print result
		System.out.println("Level 0 is "+count0*100/8+"%");
		System.out.println("Level 1 is "+count1*100/8+"%");
		System.out.println("Level 2 is "+count2*100/8+"%");
		System.out.println("Level 3 is "+count3*100/8+"%");
		MyPanel panel0 = new MyPanel();
		MyPanel panel1 = new MyPanel();
		MyPanel panel2 = new MyPanel();
		MyPanel panel3 = new MyPanel();
		panel0.setLevelState(10, 20, 0, count0*100/8);
		panel0.setChartState(100, 0, count0/8);
		panel1.setLevelState(10, 60, 1, count1*100/8);
		panel1.setChartState(100, 0, count1/8);
		panel2.setLevelState(10, 100, 2, count2*100/8);
		panel2.setChartState(100, 0, count2/8);
		panel3.setLevelState(10, 140, 3, count3*100/8);
		panel3.setChartState(100, 0, count3/8);
		MyFrame frame = new MyFrame();
		frame.add(panel0);
		frame.add(panel1);
		frame.add(panel2);
		frame.add(panel3);
	}

}
