package ce1002.e8.s102502546;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	Rectangle rec = new Rectangle();
	Oval ova = new Oval();

	MyFrame() {
		setLayout(null);
		setSize(550, 300);
		setVisible(true);//可見
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		rec.setBounds(20, 20, 200, 150);
		ova.setBounds(250, 20, 200, 150);
		add(rec);//加入rec
		add(ova);//加入ova
	}

}
