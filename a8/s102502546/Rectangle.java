package ce1002.e8.s102502546;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class Rectangle extends JPanel {
	Rectangle() {
		setSize(250, 100);
		setLayout(null);
		a.setBounds(15, 90, 200, 10);
		b.setBounds(15, 100, 200, 10);
		add(a);
		add(b);
		setBorder(new LineBorder(Color.BLACK, 3));
	}

	JLabel a = new JLabel("Graph:Rectangle");
	JLabel b = new JLabel("Info:In Euclidean plane geome...");

	protected void paintComponent(Graphics g) {
		g.setColor(Color.GREEN);
		g.fillRect(55, 10, 100, 50);

	}
}
