package ce1002.a8.s102502039;

import java.awt.*;

import javax.swing.JPanel;

public class Level0Panel0 extends JPanel {
	public double r1;

	public Level0Panel0(double a) {// constructor
		this.r1 = a;
	}

	public void setheight(double a) {
		this.r1 = a;
		repaint();
	}

	public double getheight() {
		return r1;
	}

	@Override
	protected void paintComponent(Graphics g) {// paint icon
		super.paintComponent(g);
		int height0 = (int) (getheight());
		g.setColor(Color.GRAY);
		g.fillRect(30, 10, 10, (100 - height0));
		g.setColor(Color.RED);
		g.fillRect(30, (10 + (100 - height0)), 10, height0);
		g.setColor(Color.GRAY);
		g.setFont(new Font("Calibri", Font.BOLD, 14));
		g.drawString("Level0", 25, 120);
		g.drawString("" + getheight() + "%", 25, 140);
	}
}
