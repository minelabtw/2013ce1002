package ce1002.a8.s102502039;

import javax.swing.JPanel;

import java.awt.*;

public class Level1Panel1 extends JPanel {
	public double r2;

	public Level1Panel1(double b) {// constructor
		this.r2 = b;
	}

	public void setheight(double b) {
		this.r2 = b;
		repaint();
	}

	public double getheight() {
		return r2;
	}

	@Override
	protected void paintComponent(Graphics g) {// paint icon
		super.paintComponent(g);
		int height1 = (int) (getheight());
		g.setColor(Color.GRAY);
		g.fillRect(45, 10, 10, (100 - height1));
		g.setColor(Color.RED);
		g.fillRect(45, (10 + (100 - height1)), 10, height1);
		g.setColor(Color.GRAY);
		g.setFont(new Font("Calibri", Font.BOLD, 14));
		g.drawString("Level1", 40, 120);
		g.drawString("" + getheight() + "%", 40, 140);

	}

}
