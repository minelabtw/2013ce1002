package ce1002.a8.s102502039;

import javax.swing.JPanel;

import java.awt.*;

public class Level2Panel2 extends JPanel {
	public double r3;

	public Level2Panel2(double c) {// constructor
		this.r3 = c;
	}

	public void setheight(double c) {
		this.r3 = c;
		repaint();
	}

	public double getheight() {
		return r3;
	}

	@Override
	protected void paintComponent(Graphics g) {// paint icon
		super.paintComponent(g);
		int height2 = (int) (getheight());
		g.setColor(Color.GRAY);
		g.fillRect(100, 10, 10, (100 - height2));
		g.setColor(Color.RED);
		g.fillRect(100, (10 + (100 - height2)), 10, height2);
		g.setColor(Color.GRAY);
		g.setFont(new Font("Calibri", Font.BOLD, 14));
		g.drawString("Level2", 95, 120);
		g.drawString("" + getheight() + "%", 95, 140);

	}

}
