package ce1002.a8.s102502039;

import javax.swing.JPanel;

import java.awt.*;

public class Level3Panel3 extends JPanel {
	public double r3;

	public Level3Panel3(double d) {// constructor
		this.r3 = d;
	}

	public void setheight(double d) {
		this.r3 = d;
	}

	public double getheight() {
		return r3;
	}

	@Override
	protected void paintComponent(Graphics g) {// paint icon
		super.paintComponent(g);
		int height2 = (int) (getheight());
		g.setColor(Color.GRAY);
		g.fillRect(150, 10, 10, (100 - height2));
		g.setColor(Color.RED);
		g.fillRect(150, (10 + (100 - height2)), 10, height2);
		g.setColor(Color.GRAY);
		g.setFont(new Font("Calibri", Font.BOLD, 14));
		g.drawString("Level3", 145, 120);
		g.drawString("" + getheight() + "%", 145, 140);

	}

}
