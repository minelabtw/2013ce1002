package ce1002.a8.s102502039;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	public MyFrame(double a, double b, double c, double d) {// set frame
		setSize(1200, 250);
		setLayout(new GridLayout(1, 4, 10, 160));
		Level0Panel0 p0 = new Level0Panel0(a);
		Level1Panel1 p1 = new Level1Panel1(b);
		Level2Panel2 p2 = new Level2Panel2(c);
		Level3Panel3 p3 = new Level3Panel3(d);

		add(p0);

		add(p1);
		add(p2);
		add(p3);
		p0.setSize(30, 150);
		p1.setSize(30, 150);
		p2.setSize(30, 150);
		p3.setSize(30, 150);
		getContentPane().repaint();

		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

}
