package ce1002.a8.s102502039;

import java.util.Scanner;
import java.util.logging.Level;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] gradearray = new int[8];// set array

		System.out.println("Input grades: ");// input 8 grades
		for (int i = 0; i < 8; i++) {
			gradearray[i] = input.nextInt();
			while (gradearray[i] < 0 || gradearray[i] > 100) {
				System.out.println("Out of range!");
				gradearray[i] = input.nextInt();
			}
		}
		double level0 = 0;// set 4 variables
		double level1 = 0;
		double level2 = 0;
		double level3 = 0;
		for (int j = 0; j < 8; j++) {// calculate numbers of 4 levels

			if (gradearray[j] >= 0 && gradearray[j] <= 25) {
				level0++;
			} else if (gradearray[j] >= 26 && gradearray[j] <= 50) {
				level1++;
			} else if (gradearray[j] >= 51 && gradearray[j] <= 75) {
				level2++;
			} else
				level3++;

		}

		double percent0 = (level0 / 8) * 100;// set 4 variables
		double percent1 = (level1 / 8) * 100;
		double percent2 = (level2 / 8) * 100;
		double percent3 = (level3 / 8) * 100;
		System.out.println("Level 0 is " + percent0 + "%");// output
		System.out.println("Level 1 is " + percent1 + "%");
		System.out.println("Level 2 is " + percent2 + "%");
		System.out.println("Level 3 is " + percent3 + "%");

		MyFrame frame = new MyFrame(percent0, percent1, percent2, percent3);
		frame.repaint();

	}

}
