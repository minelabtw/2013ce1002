package ce1002.a8.s101502205;

import java.util.Scanner;
import javax.swing.JFrame;

public class A8 {
	
	private static Scanner input;
	
	public static void main(String[] args) {
		// Create a scanner
		input = new Scanner(System.in);
		
		double[] levelPercentage = new double[4];
		int tmp, n=8;	// n: number of score
		
		// Inputs
		System.out.println("Input grades: ");
		for(int i=0; i<n; i++){
			tmp = input.nextInt();
			while(tmp<0 || tmp>100){
				// Error
				System.out.println("Out of range!");
				// Inputs again
				tmp = input.nextInt();
			}
			
			// Counting
			tmp = (tmp-1)/25;
			levelPercentage[tmp]++;
		}
		for(int i=0; i<4; i++){
			// Evaluate the percentage
			levelPercentage[i] *= 100.0/n;
			// Outputs
			System.out.println("Level " + i + " is " + levelPercentage[i] + "%");
		}	
		
		// Create a frame
		JFrame frame = new JFrame();
		// Settings
		frame.setLayout(null);
		frame.setSize(350,350);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		// Create panels
		MyPanel[] panels = new MyPanel[4];
		for(int i=0; i<4; i++){
			panels[i] = new MyPanel(i, levelPercentage[i]);
			// Set panels location and size
			panels[i].setBounds(10+80*i, 10, 70, 300);
			// Add to frame
			frame.add(panels[i]);
		}
	}
}