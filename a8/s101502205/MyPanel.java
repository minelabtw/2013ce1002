package ce1002.a8.s101502205;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Graphics;
import java.awt.Color;

public class MyPanel extends JPanel{
	
	private int level;
	private double percentage;
	private JLabel text1,text2;
	
	public MyPanel(int level, double percentage){
		// Set layout
		setLayout(null);
		
		// get level and percentage
		this.level = level;
		this.percentage = percentage;
		
		// Create label and set text
		text1 = new JLabel("Level " + level);
		text2 = new JLabel(percentage + "%");
		
		// Set location and size of labels
		text1.setBounds(15, 233, 50, 15);
		text2.setBounds(15, 255, 50, 15);
		
		// Add labels to panel
		add(text1);
		add(text2);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// Property of rectangles
		int x=20, y=20;		// Offset
		int width=20, height=200, lineWidth=2;		// size of background rectangles
		int blockHeight = (int)(height*percentage/100);		// size of color block
		
		// Draw background
		// 	Dark red shadow
		g.setColor(Color.getHSBColor(90f, 1.0f, 0.5f));
		g.fillRect(0+lineWidth+x, 0+lineWidth+y, width, height);
		// Gray background
		g.setColor(Color.gray);
		g.fillRect(0+x, 0+y, width, height);
		
		// If percentage != 0
		if(blockHeight != 0){
			// A dark red rectangle for lines
			g.setColor(Color.getHSBColor(90f, 1.0f, 0.75f));
			g.fillRect(0+x, height-blockHeight+y, width, blockHeight);
			
			// Red rectangle is  a bit smaller than the dark red rectangle
			g.setColor(Color.red);
			g.fillRect(0+lineWidth+x, height-blockHeight+lineWidth+y, width-2*lineWidth, blockHeight-2*lineWidth);
		}
	}
}
