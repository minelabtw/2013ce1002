package ce1002.a8.s101201519;
import java.awt.*;
import javax.swing.*;

public class drawPanel extends JPanel {
	protected float percent=0;
	drawPanel()
	{
		percent=0;
	}
	drawPanel(float p)
	{
		percent=p;
	}
	void setPercent(float p)
	{
		percent=p;
	}
	float getPercent()
	{
		return percent;
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 20, 100-(int)percent);
		g.setColor(Color.RED);
		g.fillRect(0, 100-(int)percent, 20, (int)percent);
	}
}
