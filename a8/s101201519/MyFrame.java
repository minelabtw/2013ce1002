package ce1002.a8.s101201519;
import java.awt.*;
import javax.swing.*;
public class MyFrame extends JFrame{
	private MyPanel[] panel=new MyPanel[4];
	protected float[] percent=new float [4];
	MyFrame(float percent[])
	{
		this.percent=percent;
		for(int i=0;i<percent.length;i++)
		{
			panel[i]=new MyPanel(i,percent[i]*25/2);
			panel[i].setBounds(20+150*i,0,120,220);
		}
		setLayout(null);
		setSize(550,280);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		for(int i=0;i<percent.length;i++)
		{
			add(panel[i]);
		}
	}	
}
