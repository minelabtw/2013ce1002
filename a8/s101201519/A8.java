package ce1002.a8.s101201519;
import java.util.Scanner;
public class A8 {
	
	public static void main(String[]args){
		Scanner input = new Scanner(System.in);
		int[] grades = new int [8];
		float[] level = new float[4];
		System.out.println("Input grades: ");
		for(int i=0;i<grades.length;i++)
		{
			do
			{
				grades[i]=input.nextInt();
				if(grades[i]<0 || grades[i]>100)
				{
					System.out.println("Out of range!");
				}
				
			}while(grades[i]<0 || grades[i]>100);
		}
		
		for(int i=0;i<grades.length;i++)
		{
			if(grades[i]>=0 && grades[i]<=25)
				level[0]++;
			else if(grades[i]>=26 && grades[i]<=50)
				level[1]++;
			else if(grades[i]>=51 && grades[i]<=75)
				level[2]++;
			else 
				level[3]++;
		}
		
		for(int i=0;i<level.length;i++)
		{
			System.out.println("Level "+i+" is "+level[i]*25/2+"%");
		}
		MyFrame f=new MyFrame(level);
	}
}
