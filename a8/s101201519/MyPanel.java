package ce1002.a8.s101201519;
import javax.swing.*;
public class MyPanel extends JPanel{
	protected int level=0;
	protected float percent=0;
	protected JLabel levelLabel = new JLabel();
	protected JLabel percentLabel = new JLabel();
	protected drawPanel dPanel = new drawPanel();
	
	MyPanel(int level,float percent)
	{
		this.level = level;
		this.percent = percent;
		setLayout(null);
		dPanel.setBounds(40,20,40,100);
		dPanel.setPercent(percent);
		levelLabel.setBounds(30,140,60,40);
		percentLabel.setBounds(30,180,60,40);
		levelLabel.setText("Level "+level);
		percentLabel.setText(percent+"%");
		add(dPanel);
		add(levelLabel);
		add(percentLabel);
	}
}
