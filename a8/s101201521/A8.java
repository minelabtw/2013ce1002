package ce1002.a8.s101201521;
import java.awt.GridLayout;
import java.util.Scanner;
import javax.swing.JFrame;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int[] scores = new int[8];
		int[] levels = new int[4];
		//prompt for 8 grades 
		//and classify the grades into 4 parts 0~25, 26~50, 51~75, 76~100 
		System.out.println("Input grades: ");
		for(int i = 0; i < scores.length; i++){
			do{
				scores[i] = input.nextInt();
				if(scores[i] < 0 || scores[i] > 100)
					System.out.println("Out of range!");
			}while(scores[i] < 0 || scores[i] > 100);
			if(scores[i] <= 25)
				levels[0]++;
			else if(scores[i] <= 50)
				levels[1]++;
			else if(scores[i] <= 75)
				levels[2]++;
			else
				levels[3]++;
		}
		input.close();
		//draw a bar chart of the grades
		JFrame frame = new JFrame();
		MyPanel[] p = new MyPanel[4];
		for(int i = 0; i < levels.length; i++){
			System.out.println("Level " + i + " is " + 
						((double)levels[i])/8.0 * 100 + "%");
			p[i] = new MyPanel(i, ((double)levels[i])/8.0 * 100);
			frame.add(p[i]);
		}
		frame.setLayout(new GridLayout(1,4,0,0));
		frame.setSize(300,300);
		frame.setLocation(0,0);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
