package ce1002.a8.s101201521;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Color;

import javax.swing.JPanel;
public class MyPanel extends JPanel{
	private int i;
	private double percent;
	public MyPanel(int i, double percent){
		this.i = i;
		this.percent = percent;
		setBackground(Color.RED);
	}
	protected void paintComponent(Graphics g){
		g.setFont(new Font("Dialog", Font.BOLD, 12));
		g.drawString("Level " + i, 5, 225);
		g.drawString(percent + "%", 5, 250);
		//background bar chart
		g.setColor(Color.GRAY);
		g.fillRect(5, 5, 20, 200);
		//foreground bar chart
		g.setColor(Color.RED);
		int redlength = (int)(2 * percent);
		g.fill3DRect(5, 5 + 200 - redlength, 20, redlength, true);
	}
}
