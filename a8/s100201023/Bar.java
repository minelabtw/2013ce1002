package ce1002.a8.s100201023;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Bar extends JPanel
{
	//properties
	private int type;
	private double level;
	
	//constructure
	public Bar(int i , double j)
	{
		type = i;
		level = j;
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		//pain bar
		g.setColor(Color.red);
		g.fillRect(0, 0, 10, 100);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 10, 100 - (int)level);
		
		//write information
		g.drawString("Level " + type, 0, 120);
		g.drawString(level + "%", 0, 140);
	}
}
