package ce1002.a8.s100201023;

public class A8
{

	public static void main(String[] args)
	{
		//static score
		Statics student = new Statics();
		
		student.inputdata();
		student.computdata();
		student.outputdata();
		
		//make gui
		GUI gui = new GUI();
		gui.paintbar(student.getlevel());
	}

}
