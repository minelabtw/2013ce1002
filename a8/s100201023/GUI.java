package ce1002.a8.s100201023;

import javax.swing.JFrame;

public class GUI extends JFrame
{
	//properties
	private Bar[] barchart;
	
	//constructure
	public GUI()
	{
		barchart = new Bar[4];
		setSize(500, 200);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	//method
	public void paintbar(double[] level)
	{
		for(int i = 0 ; i < 4 ; ++i)
		{
			barchart[i] = new Bar(i , level[i]);
			barchart[i].setBounds(i * 50 + 10 * (i + 1), 10, 50, 200);
			add(barchart[i]);
		}
	}
}
