package ce1002.a8.s100201023;

import java.util.Scanner;

public class Statics
{
	//properties
	private int[] score;
	private double[] level;
	
	//constructure
	public Statics()
	{
		score = new int[8];
		level = new double[4];
		for(int i = 0 ; i < 4 ; ++i)
			level[i] = 0;
	}
	
	//method
	public void inputdata()
	{
		Scanner input = new Scanner(System.in);
		int count = 0;
		
		//input data
		System.out.println("Input grades: ");
		while(true)
		{
			score[count] = input.nextInt();
			
			if(score[count] >= 0 && score[count] <= 100)
				++count;
			else
				System.out.println("Out of range!");
			
			if(count == 8)
				break;
		}
	}
	
	public void computdata()
	{
		//seperate level
		for(int i = 0 ; i < 8 ; ++i)
		{
			if(score[i] >=0 && score[i] <= 25)
				++level[0];
			else if(score[i] >=26 && score[i] <= 50)
				++level[1];
			else if(score[i] >=51 && score[i] <= 75)
				++level[2];
			else
				++level[3];
		}
		
		//compute level
		for(int i = 0 ; i < 4 ; ++i)
			level[i] = level[i] / 8 * 100;
	}
	
	public void outputdata()
	{
		for(int i = 0 ; i < 4 ; ++i)
			System.out.println("Level " + i + " is " + level[i]+ "%");
	}
	
	public double[] getlevel()
	{
		return level;
	}
}
