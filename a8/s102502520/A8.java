package ce1002.a8.s102502520;
import java.util.*;
import java.awt.*;

public class A8 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		int grades;
		float per = 0;
		Rect[] rect=new Rect[4];
		int[] grade=new int [8];
		int[] level=new int [4];
		float[] percent=new float[4];
		System.out.println("Input grades: ");
		for(int i=0 ; i<4 ;i++){
			rect[i]=new Rect(i);
		}
		for(int x=0;x<8;x++){
			grades = cin.nextInt();
			while(grades<0||grades>100){
				System.out.println("Out of range! ");
				grades = cin.nextInt();
			}
			grade[x]=grades;
		}
		cin.close();
		for(int a=0;a<4;a++){
			level[a]=0;
			percent[a]=0;
		}
		for(int i=0; i<8;i++){
			if (grade[i]>=0&&grade[i]<25){
				level[0]++;
			}
			if (grade[i]>=25&&grade[i]<50){
				level[1]++;
			}
			if (grade[i]>=50&&grade[i]<75){
				level[2]++;
			}
			if (grade[i]>=75&&grade[i]<=100){
				level[3]++;
			}
		}	
		for (int y=0;y<4;y++){
			percent[y]=(float)level[y]/8*100;
			per=percent[y];
			rect[y].setper(per);
		}
		//在這下面可以直接到Rect(int a)裡面弄好4個PNAEL --->移駕到Rect.java
		MyFrame frame =new MyFrame(rect);   //走吧，MyFrame.java
		for(int j =0 ;j<4;j++){
			System.out.println("Level "+j+" is "+percent[j]+"%");
		}
	}

}