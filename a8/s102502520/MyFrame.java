package ce1002.a8.s102502520;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame{

	protected Rect[] rect;

	MyFrame(Rect[] rect){
		this.rect=rect;
		
		setLayout(null);
		setBounds(5,10,900,500);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		for(int x=0;x<4;x++){
			add(rect[x]);
		}
	}

}