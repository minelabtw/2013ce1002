package ce1002.a8.s102502520;

import java.awt.*;
import javax.swing.*;

public class Rect extends JPanel{
	protected int a;
	protected float per=0;
	protected Rect[] rect;
	
	public Rect(int a){
		this.a=a;
		setBounds(50+100*a,15,100,400);	
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(10, 15, 20, 200);
		g.setColor(Color.GRAY);
		g.fillRect(10, 15, 20, (int) (200-per*2));
		g.setColor(Color.BLUE);
		g.drawString("level"+a, 10, 230);
		g.drawString(per+" %", 10,240 );
	}
	public void setper(float per){
		this.per=per;
	}
}