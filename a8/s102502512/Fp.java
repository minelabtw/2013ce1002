package ce1002.a8.s102502512;
import javax.swing.*;

import java.awt.*;


public class Fp extends JPanel{						//Line 7-20: create a panel output the statics
	private double h=0;
	private JLabel lb1 = new JLabel();
	private JLabel lb2 = new JLabel();
	Fp(double h, int i,int x,double c)
	{
		seth(h);
		geth(h);
		setLayout(null);
		setBounds(x,10,100,400);
		setlb(c,i);
		add(lb1);
		add(lb2);
	}
	public void setlb(double c, int i)				//Line 21-27: create a function to set labels
	{
		lb1.setText("Level "+(i-1));
		lb2.setText(c+"%");
		lb1.setBounds(20 , 350, 120, 20);
		lb2.setBounds(20 , 370, 120, 20);
	}
	@Override										//Line 28-45: draw pictures according to static form user's input
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		double hie = geth(h);
		g.setColor(Color.gray);
		g.fillRect(10,50,75,250-(int)hie);
		g.setColor(Color.RED);
		g.fillRect(10, 300-(int)hie, 75, (int)hie);
	}
	public void seth(double h)
	{
		this.h=h;
	}
	public double geth(double h)
	{
		return h; 
	}

}
