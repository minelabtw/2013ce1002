package ce1002.a8.s102502512;
import java.util.*;

import javax.swing.*;

import java.awt.*;

public class A8 extends JFrame{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A8 trry = new A8();
		int grade=0;										//Line 13-52: let users input the grades and record it
		double rec[] = new double[4];
		for(int k=0;k<4;k++)
		{
				rec[k]=0;
		}
		Scanner input = new Scanner(System.in);
		System.out.println("Input grades:");
		for(int i=0;i<8;i++)
		{
			do
			{
				grade=input.nextInt();
				if(grade<0||grade>100)
				{
					System.out.println("Out of range!");
				}
			}while(grade<0||grade>100);
			if(0<=grade&&grade<25)
			{
				rec[0]++;
			}
			else if(25<=grade&&grade<50)
			{
				rec[1]++;
			}
			else if(50<=grade&&grade<75)
			{
				rec[2]++;
			}
			else if(75<=grade&&grade<=100)
			{
				rec[3]++;
			}
		}
		for(int j=0;j<4;j++)
		{
			rec[j]=(rec[j]/8)*100;
			System.out.println("Level "+(j)+" is "+rec[j]+"%");
		}
		trry.setLayout(null);										//Line 53-62: create a frame with the statics in it
		trry.setBounds(300 , 50 , 700 , 450);
		trry.setVisible(true);
		trry.setDefaultCloseOperation(EXIT_ON_CLOSE);
		trry.setgraph(rec[0],1,10);
		trry.setgraph(rec[1],2,200);
		trry.setgraph(rec[2],3,390);
		trry.setgraph(rec[3],4,580);
		
	}
	public void setgraph(double rec,int i,int x)					//Line 63-68: make a function to pass the data to panel
	{
		double h=0;
		h=rec*2.5;
		Fp gg = new Fp(h,i,x,rec);
		add(gg);
	}

}
