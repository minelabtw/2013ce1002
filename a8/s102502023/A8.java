package ce1002.a8.s102502023;

import java.util.*;
import java.text.*;


public class A8{

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
    int grade = -1; // initialize grade to -1
    int[] grades = new int[8]; // initialize array grades
    int[] level = new int[4]; // initialize array level
    System.out.println("Input grades:");
    for (int i = 0; i < grades.length; i++) {
    	do {
    	grade = input.nextInt();
    	if (grade < 0 || grade > 100)
    		System.out.println("Out of range!");
    }
    while(grade < 0 || grade > 100);
    	
    	grades[i] = grade;
    	
    	if (0 <= grades[i] && grades[i] <= 25)
    		level[0]++;
    	else if(26 <= grades[i] && grades[i] <= 50)
    		level[1]++;
    	else if(51 <= grades[i] && grades[i] <= 75)
    		level[2]++;
    	else if(76 <= grades[i] && grades[i] <= 100)
    		level[3]++;
    	else 
    		System.exit(1);
    
}
    
    for (int i = 0; i < level.length; i++) {
    	Format fm = new DecimalFormat("#0.0%"); // output in percent form
    	System.out.println("Level " + i + " is " + fm.format((double)level[i] / 8));
    }
    input.close();
    MyFrame frame = new MyFrame(level);// invoke MyFrame
}
}
