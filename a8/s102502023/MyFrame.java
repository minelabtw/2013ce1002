package ce1002.a8.s102502023;

import javax.swing.*;
import java.awt.*;
public class MyFrame extends JFrame {
	Histogram h = new Histogram();
  MyFrame(int[] count) {
	  h.showHistogram(count);
	  add(h);
	  setTitle(null);
	  setVisible(true);
	  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  setLocationRelativeTo(null); // center the frame
	  pack();
  }
}
