package ce1002.a8.s102502023;

import javax.swing.*;

import java.awt.*;
import java.text.*;

public class Histogram extends JPanel {
	private int[] count; // initialize count as array
	Format fm = new DecimalFormat("#0.0%"); // output in percent form
	public void showHistogram(int[] count) {
		this.count = count;
		repaint(); // repaint the panel
	}
    @Override
    protected void paintComponent(Graphics g) {
    	if (count == null) return;
    	
    	super.paintComponent(g);
    	
    	int width = getWidth();
    	int height = getHeight();
    	int interval = (width - 40) / count.length;
    	int indevidualWidth = (int)((width - 40) / 24 * 0.60);
    	
    	int maxCount = count.length;
    	
    	int x = 30;
    	
    	g.drawLine(10, height - 45, width - 10, height -45);
    	for (int i = 0; i < count.length; i++) {
    		int barHeight = (int)(((double)count[i] / (double)maxCount) * (height - 55));
    		
    		g.setColor(Color.RED);
    		g.fillRect(x, height - 45 - barHeight, indevidualWidth, barHeight);
    		
    		g.setColor(Color.BLACK);
    		g.drawString("Level " + i , x, height - 30);
    		g.drawString(fm.format((double)count[i] / 8), x, height - 15);
    				
    		x += interval;
    	} 
    	
    }
    
    @Override
    public Dimension getPreferredSize() {
    	return new Dimension(300, 300);
    }
}
