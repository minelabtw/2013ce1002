package ce1002.a8.s102502503;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
public class MyPanel extends JPanel{
	float[] level=new float [4];
	
	public MyPanel(){  //設定邊界
		setBounds(0,0,480,480);
	}
	public void setlevel(float[] level){  
		this.level=level;
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(Color.GRAY);  //畫出level 0 比例的長條圖
		g.fillRect(50, 50, 10, (int)(100-level[0]));
		g.setColor(Color.RED);
		g.fillRect(50, 50+(int)(100-level[0]), 10, (int)level[0]);
		
		g.setColor(Color.BLACK);
		g.drawString("Level 0", 50, 160);  //文字
		g.drawString(level[0]+"%", 50, 170);
		
		g.setColor(Color.GRAY);  //畫出level 1 比例的長條圖
		g.fillRect(120, 50, 10, (int)(100-level[1]));
		g.setColor(Color.RED);
		g.fillRect(120, 50+(int)(100-level[1]), 10, (int)level[1]);
		
		g.setColor(Color.BLACK);
		g.drawString("Level 1", 120, 160);  //文字
		g.drawString(level[1]+"%", 120, 170);
		
		g.setColor(Color.GRAY);  ////畫出level 2 比例的長條圖
		g.fillRect(190, 50, 10, (int)(100-level[2]));
		g.setColor(Color.RED);
		g.fillRect(190, 50+(int)(100-level[2]), 10, (int)level[2]);
		
		g.setColor(Color.BLACK);
		g.drawString("Level 2", 190, 160);  //文字
		g.drawString(level[2]+"%", 190, 170);
		
		g.setColor(Color.GRAY);  //畫出level 0 比例的長條圖
		g.fillRect(260, 50, 10, (int)(100-level[3]));
		g.setColor(Color.RED);
		g.fillRect(260, 50+(int)(100-level[3]), 10, (int)level[3]);
		
		g.setColor(Color.BLACK); 
		g.drawString("Level 3", 260, 160);  //文字
		g.drawString(level[3]+"%", 260, 170);
		
	}
}
