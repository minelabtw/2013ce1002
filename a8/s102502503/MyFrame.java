package ce1002.a8.s102502503;
import javax.swing.*;
public class MyFrame extends JFrame{
	
	public MyFrame(float[] level){
		setLayout(null);
		setBounds(200,200,350,260);  //設定邊界
		setVisible(true);  //顯示視窗
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		MyPanel mypanel=new MyPanel();  //建立MyPanel物件
		mypanel.setlevel(level);  //將等級的陣列傳入MyPanel
		add(mypanel);
	}
}
