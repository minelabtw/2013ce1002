package ce1002.a8.s102502503;
import java.util.Scanner;
public class A8 {
	private static Scanner scanner;
	public static void main(String[] args) {
		scanner=new Scanner(System.in);
		int[] grade=new int[8];
		float[] level=new float[4];
		int level0=0;
		int level1=0;
		int level2=0;
		int level3=0;
		
		for(int i=0; i<8; i++){  //輸入八個成績
			do{  //檢查成績範圍
				System.out.println("Input grades: ");
				grade[i]=scanner.nextInt();
				if(grade[i]>100 || grade[i]<0)
					System.out.println("Out of range!");
			}while(grade[i]>100 || grade[i]<0);
			
			if(0<=grade[i] && grade[i]<=25)  //將成績分等
				level0++;
			else if(26<=grade[i] && grade[i]<=50)
				level1++;
			else if(51<=grade[i] && grade[i]<=75)
				level2++;
			else if(76<=grade[i] && grade[i]<=100)
				level3++;
		}
		scanner.close();
		
		level[0]=(float)level0/8*100;  //將各等級佔的百分比存入陣列
		level[1]=(float)level1/8*100;
		level[2]=(float)level2/8*100;
		level[3]=(float)level3/8*100;
		
		for(int i=0; i<4; i++){  //輸出比例
			System.out.println("Level "+i+" is "+level[i]+"%");
		}
	
		MyFrame myframe=new MyFrame(level);  //建立MyFrame物件並把等級的陣列傳入
	}

}
