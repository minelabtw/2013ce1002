package ce1002.a8.s102502012;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class GradePanel extends JPanel{
	private int[] grade;
	private int[] level;
	
	GradePanel(int[] grade){
		this.grade = grade;
		setBounds(0, 0, 500, 300);
		count();
	}
	
	private void count(){
		level = new int[4];
		for(int i = 0; i < level.length; i++)
			level[i] = 0;
		
		for(int i = 0; i < grade.length; i++){
			if(grade[i] <= 25)	level[0]++;
			else if(grade[i] <= 50)	level[1]++;
			else if(grade[i] <= 75)	level[2]++;
			else 					level[3]++;
		}
		
		// output status
		for(int i = 0; i < level.length; i++){
			System.out.println("Level " + i + " is " + ((double)level[i] / grade.length * 100) + "%");
		}
	}
	
	public void paintComponent(Graphics g){
		for(int i = 0; i < level.length; i++){
			int height = (int)(100 * (double)level[i] / grade.length);
			g.setColor(Color.gray);
			g.fillRect(10 + i * 50, 10, 20, 100);
			g.setColor(Color.red);
			g.fillRect(10 + i * 50, 10 + 100 - height, 20, height);
			g.drawString("level" + i, 10 + i * 50, 120);
			g.drawString(((double)level[i] / grade.length * 100) + "%", 10 + i * 50, 140);
		}
	}
}
