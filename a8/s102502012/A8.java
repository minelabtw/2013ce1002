package ce1002.a8.s102502012;

import java.awt.Dimension;
import java.util.Scanner;

import javax.swing.JFrame;

public class A8 {

	private static boolean outOfRange(){
		System.out.println("Out of range!");
		return true;
	}
	
	public static void main(String[] args) {
		int[] grade = new int[8];
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input grades:");
		for(int i = 0; i < grade.length; i++){
			do{
				grade[i] = input.nextInt();
			}while((grade[i] > 100 || grade[i] < 0) && outOfRange());
		}
		input.close();
		
		GradeFrame window = new GradeFrame(grade);
		window.setTitle("s102502012");
		window.setSize(250, 250);
		window.setVisible(true);		
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}
