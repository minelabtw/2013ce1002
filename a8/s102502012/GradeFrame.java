package ce1002.a8.s102502012;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ce1002.a7.s102502012.MyPanel;

public class GradeFrame extends JFrame{
	GradePanel gradepanel;
	GradeFrame(int[] grade){
		gradepanel = new GradePanel(grade);
		add(gradepanel);
	}
}
