package ce1002.a8.s102502040;


import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel{
	JLabel jlabel1=new JLabel();
	JLabel jlabel2=new JLabel();
	int a;
	double percent;
	MyPanel(double percent,int a) {
		setLayout(null);
		this.percent=percent;
		this.a=a;
	}
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.gray);//灰色剩於
		g.fillRect(20,20,30,(int)(200-((2*this.percent))));
		g.setColor(Color.red);//紅色佔據
		g.fillRect(20,(int)(200-((2*this.percent))+20),30,(int)(2*(this.percent)));
		jlabel1.setText("Level  " + a);//等級與百分比
		jlabel2.setText(this.percent + " %");
		jlabel1.setBounds(20,220,50,50);
		jlabel2.setBounds(20,250,50,50);
		this.add(jlabel1);
		this.add(jlabel2);
	}

}

