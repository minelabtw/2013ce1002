package ce1002.a8.s102502040;

import javax.swing.*;

public class MyFrame extends JFrame{
	MyFrame(double[]percent) {
		setLayout(null);
		setSize(400,390);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		newHistogram(percent);
	}
	public void newHistogram(double[] percent)
	{
		for(int i=0;i<4;i++)//設定出4個panel做長條圖,等級,百分比
		{
			MyPanel panel = new MyPanel(percent[i],i);
			panel.setBounds(i*100,35,360,360);
			this.add(panel);
		}
	}

}