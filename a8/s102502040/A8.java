package ce1002.a8.s102502040;

import java.util.*;
import ce1002.a8.s102502040.MyFrame;

public class A8 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number;
		int[] grade;
		double[] percent;
		grade = new int [4];//有四個長條圖(分數)
		percent = new double [4];
		System.out.println("Input grades:");
		for(int i=1;i<=8;i++)//輸入8個數字,有限定範圍,再分等級
		{
			do
			{
				number=input.nextInt();
				if(number<0 || number>100)
				{
					System.out.println("Out of range!");
				}
				if(number<=25 && number>=0)
				{
					grade[0]++;
				}
				else if(number<=50 && number>=26)
				{
					grade[1]++;
				}
				else if(number<=75 && number>=51)
				{
					grade[2]++;
				}
				else if(number<=100 && number>=76)
				{
					grade[3]++;
				}
			}while( number < 0 || number > 100 );
		}
		for(int a=0;a<4;a++)//計算個別百分比,然後輸出
		{
			percent[a]=(double)grade[a]/(double)8*(double)100;
			System.out.println("Level " + a + " is " + percent[a] + "%");
		}
		MyFrame frame = new MyFrame(percent);
	}
}
