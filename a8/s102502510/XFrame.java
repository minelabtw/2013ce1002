package ce1002.a8.s102502510;

import javax.swing.*;

public class XFrame extends JFrame{
	double u[]=new double[4];
	XFrame(int l1,int l2,int l3,int l4,double x0,double x1,double x2,double x3)
	{
		XPanel xp1=new XPanel(l1,100);
		XPanel xp2=new XPanel(l2,200);
		XPanel xp3=new XPanel(l3,300);
		XPanel xp4=new XPanel(l4,400);
		JLabel j1=new JLabel("Level 0");
		JLabel j2=new JLabel("Level 1");
		JLabel j3=new JLabel("Level 2");
		JLabel j4=new JLabel("Level 3");
		JLabel k0=new JLabel(x0+"%");
		JLabel k1=new JLabel(x1+"%");
		JLabel k2=new JLabel(x2+"%");
		JLabel k3=new JLabel(x3+"%");
		j1.setBounds(35,300,100,50);
		j2.setBounds(135,300,100,50);
		j3.setBounds(235,300,100,50);
		j4.setBounds(335,300,100,50);
		k0.setBounds(50,340,100,50);
		k1.setBounds(150,340,100,50);
		k2.setBounds(250,340,100,50);
		k3.setBounds(350,340,100,50);
		xp1.setBounds(50, 0, 100, 300);
		xp2.setBounds(150,0, 100, 300);
		xp3.setBounds(250,0, 100, 300);
		xp4.setBounds(350,0, 100, 300);
		add(xp1);
		add(xp2);
		add(xp3);
		add(xp4);
		add(j1);
		add(j2);
		add(j3);
		add(j4);
		add(k0);
		add(k1);
		add(k2);
		add(k3);
		setSize(1600,600);//setsize
		setLayout(null);
		setVisible(true);
	}
}
