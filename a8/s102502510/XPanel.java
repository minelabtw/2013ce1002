package ce1002.a8.s102502510;
import javax.swing.*;

import java.awt.*;
public class XPanel extends JPanel{
	protected int p;
	protected int xco;
	XPanel(int y,int x)
	{
		p=y;
		xco=x;
	}
	//draw charts
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(0,50,4,200-p);
		g.setColor(Color.RED);
		g.fillRect(0,200-p+50, 4, p);
	}
}