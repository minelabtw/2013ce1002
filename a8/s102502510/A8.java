package ce1002.a8.s102502510;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double score[]=new double[8];
		int n=8;
		Scanner input=new Scanner(System.in);
		System.out.println("Input grades: ");
		while(n>0)
		{
			double num;
			do
			{
				num=input.nextDouble();
				if(!(num>=0&&num<=100))
					System.out.print("Out of range!");
			}while(!(num>=0&&num<=100));
			score[8-n]=num;
			n--;
		}
		int level[]=new int [4];
		for(int i=0;i<8;++i)
		{
			if(score[i]<=25)
				++level[0];
			else if(score[i]<=50)
				++level[1];
			else if(score[i]<=75)
				++level[2];
			else 
				++level[3];
		}
		double u[]=new double [4];
		for(int i=0;i<4;++i)
		{
			level[i]=level[i]*200/8;
			u[i]=((int)level[i])/2;
		}
		//create a Frame
		XFrame x=new XFrame(level[0],level[1],level[2],level[3],u[0],u[1],u[2],u[3]);
	}

}
