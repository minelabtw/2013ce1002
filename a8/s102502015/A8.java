package ce1002.a8.s102502015;

import java.util.Scanner;
import ce1002.a8.s102502015.MyFrame;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int[] grade = new int[8]; // ������
		float a = 0; // ��LEVEL
		float b = 0;
		float c = 0;
		float d = 0;
		System.out.println("Input grades: ");
		for (int i = 0; i < 8; i++) {
			do {
				grade[i] = scanner.nextInt();
				if (grade[i] > 100 || grade[i] < 0)
					System.out.println("Out of range!");
			} while (grade[i] > 100 || grade[i] < 0);
			if (grade[i] >= 0 && grade[i] <= 25) { // ��LEVEL
				a = a + 1;
			} else if (grade[i] >= 26 && grade[i] <= 50) {
				b = b + 1;
			} else if (grade[i] >= 51 && grade[i] <= 75) {
				c = c + 1;
			} else {
				d = d + 1;
			}
		}
		System.out.println("Level 0 is " + (a / 8) * 100 + "%");
		System.out.println("Level 1 is " + (b / 8) * 100 + "%");
		System.out.println("Level 2 is " + (c / 8) * 100 + "%");
		System.out.println("Level 3 is " + (d / 8) * 100 + "%");

		MyFrame mainframe = new MyFrame();
		mainframe.setPanel(0, a / 8 * 100, mainframe.panel1, 0, 0); // �ݒuGUI
		mainframe.setPanel(1, b / 8 * 100, mainframe.panel2, 60, 0);
		mainframe.setPanel(2, c / 8 * 100, mainframe.panel3, 120, 0);
		mainframe.setPanel(3, d / 8 * 100, mainframe.panel4, 180, 0);
		scanner.close();
	}
}
