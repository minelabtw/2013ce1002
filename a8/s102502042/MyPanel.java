package ce1002.a8.s102502042;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;
public class MyPanel extends JPanel{
	private float per1, per2 ,per3, per4;
	JLabel label1,label2,label3,label4;
	MyPanel()
	{
		setLayout(null);
	}
	
	public void setPer(float per1, float per2, float per3, float per4)
	{
		this.per1 = per1;
		this.per2 = per2;
		this.per3 = per3;
		this.per4 = per4;
		
		//set label text
		label1 = new JLabel("<html>Level 0<br>"+per1*100+"%</html>");
		label2 = new JLabel("<html>Level 1<br>"+per2*100+"%</html>");
		label3 = new JLabel("<html>Level 2<br>"+per3*100+"%</html>");
		label4 = new JLabel("<html>Level 3<br>"+per4*100+"%</html>");
		
		//set size
		label1.setSize(70,70);
		label2.setSize(70,70);
		label3.setSize(70,70);
		label4.setSize(70,70);
		
		//set location
		label1.setLocation(10,200);
		label2.setLocation(90,200);
		label3.setLocation(170,200);
		label4.setLocation(250,200);
		
		add(label1);
		add(label2);
		add(label3);
		add(label4);
	}
	
	//graph 
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(10,10,10,200);
		g.setColor(Color.red);
		g.fillRect(10,210-(int)(per1*200),10,(int)(per1*200));
		
		g.setColor(Color.gray);
		g.fillRect(90,10,10,200);
		g.setColor(Color.red);
		g.fillRect(90,210-(int)(per2*200),10,(int)(per2*200));
		
		g.setColor(Color.gray);
		g.fillRect(170,10,10,200);
		g.setColor(Color.red);
		g.fillRect(170,210-(int)(per3*200),10,(int)(per3*200));
		
		g.setColor(Color.gray);
		g.fillRect(250,10,10,200);
		g.setColor(Color.red);
		g.fillRect(250,210-(int)(per4*200),10,(int)(per4*200));
	}
}
