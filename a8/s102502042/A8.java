package ce1002.a8.s102502042;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input grades:");
		//the percentages
		float[] per = new float[4];
		//temp grade
		int grade;
		for(int i=0;i<8;++i){
			grade = input.nextInt();
			while(grade<0 || grade>100)
			{
				System.out.println("Out of range!");
				grade = input.nextInt();
			}
			if(grade>=0&&grade<=25)per[0]++;
			else if(grade>=26&&grade<=50)per[1]++;
			else if(grade>=51&&grade<=75)per[2]++;
			else per[3]++;
		}
		per[0]/=8;
		per[1]/=8;
		per[2]/=8;
		per[3]/=8;
		
		//set GUI
		MyFrame frame = new MyFrame();
		frame.setPanel(per[0], per[1], per[2], per[3]);
		frame.setSize(400,300);
		frame.setVisible(true);
	}

}
