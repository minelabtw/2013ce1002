package ce1002.a8.s102502530; 	//package name

import java.awt.Color; 	//import
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class A8 extends JPanel {
	public static final Color[] colors = {Color.red, Color.yellow, Color.green, Color.blue}; 	//colors of the four bar chart
	public static double[] ratio = new double[4]; 	//ratios of the four levels
	public int num; 	//which level
	
	public static void main(String[] args) {
		for(int i = 0 ; i != 4 ; i++) 	//initialize
			ratio[i] = 0;
		
		int grade; 	//declare
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Input grades: "); 	//ipnut
		for(int i = 0 ; i != 8 ; i++) {
			grade = scanner.nextInt();
			if(grade < 0 || grade > 100) {
				System.out.println("Out of range!");
				i--;
			}
			else
				ratio[(grade - 1) / 25]++;
		}
		
		for (int i = 0; i != 4 ; i++) { 	//caculate the ratio and output
			ratio[i] = ratio[i] * 100 / 8;
			System.out.printf("Level " + i + " is %.1f%%\n", ratio[i]);
		}
		
		JFrame frame = new JFrame(); 	//set frame
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(240, 510);
		
		for (int i = 0; i != 4 ; i++) { 	//set panel
			A8 panel = new A8(i);
			panel.setPreferredSize(new Dimension(50, 500));
			panel.setLayout(null);
			panel.add(new JLabel("Level " + i));
			panel.add(new JLabel(String.format("%.1f%%", ratio[i])));
			panel.getComponent(0).setBounds(5, 420, 45, 20);
			panel.getComponent(1).setBounds(5, 440, 45, 20);
			frame.add(panel);
		}
		
		frame.setVisible(true); 	//open the frame
	}
	@Override
	protected void paintComponent(Graphics g) { 	//paint the bar chart
		g.setColor(Color.gray);
		g.fillRect(10, (int) (10 + 4 * (100 - ratio[num])), 20, (int) (4 * ratio[num]));
		g.setColor(colors[num]);
		g.fillRect(10, 10, 20, (int) (400 - 4 * ratio[num]));
		g.fillRect(10, (int) (10 + 4 * (100 - ratio[num])), 19, (int) (4 * ratio[num]) - 1);
		g.setColor(Color.gray);
		g.fillRect(10, 10, 19, (int) (399 - 4 * ratio[num]));
	}
	public A8(int num) { 	//set num
		this.num = num;
	}
}