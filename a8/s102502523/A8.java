package ce1002.a8.s102502523;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		int grade = 0;
		float c[] = {0,0,0,0};
		float print[] = {0,0,0,0};

		System.out.println("Input grades: ");
		for( int a=0;a<8;a++ )												//
		{
			grade = input.nextInt();
			while( grade<0 || grade>100 )
			{
				System.out.println("Out of range!");
				grade = input.nextInt();
			}
			if( grade<=25 )
				c[0]++;
			else if( grade>25 && grade <=50 )
				c[1]++;
			else if( grade>50 && grade <=75 )
				c[2]++;
			else
				c[3]++;
		}
		input.close();

		for( int b=0;b<4;b++)												//
		{
			float percent = c[b]/8*100;
			System.out.println("Level " + b + " is " + percent + "%");
			print[b] = percent;
		}
		new MyFrame(print);													//
	}

}
