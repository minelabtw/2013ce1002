package ce1002.a8.s102502523;

import java.awt.*;
import javax.swing.*;


public class MyFrame extends JFrame{

	MyFrame(float pass[])
	{
		setVisible(true);								//setVisible
		setSize(500, 200);								//SIZE500*200
		setDefaultCloseOperation(EXIT_ON_CLOSE);		//EXIT_ON_CLOSE
		setLayout(null);								 

		Panel0 panel0 = new Panel0(pass[0]);			 
		panel0.setLocation(20, 10);
		add(panel0);
		Panel1 panel1 = new Panel1(pass[1]);			 
		panel1.setLocation(140, 10);
		add(panel1);
		Panel2 panel2 = new Panel2(pass[2]);			 
		panel2.setLocation(260, 10);
		add(panel2);
		Panel3 panel3 = new Panel3(pass[3]);			 
		panel3.setLocation(380, 10);
		add(panel3);
	}

}
