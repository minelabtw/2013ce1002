package ce1002.a8.s102502538;

public class Grade {
       public int grade;
       public double percent1=0;
       public double percent2=0;
       public double percent3=0;
       public double percent4=0;
       
       
       public Grade(){
    	   
       }
       
       public void setGrade(int n){
    	 this.grade = n ;
       }
       
       public void setPercent(int grade){
    	   if(grade>=0 && grade<=25){
    		   percent1 = percent1+12.5;
    	   } 
    	   else if(grade>=26 && grade<=50){
    		   percent2 =percent2 +12.5;
    	   }
    	   else if(grade>=51 && grade<=75){
    		   percent3 =percent3 +12.5;
    	   }
    	   else {
    		   percent4 =percent4 +12.5;
    	   }   
       }
       public double getPercent1(){
    		   return percent1;
       }
       public double getPercent2(){
		   return percent2;
   }
       public double getPercent3(){
		   return percent3;
   }
       public double getPercent4(){
		   return percent4;
   }
       
	
}
