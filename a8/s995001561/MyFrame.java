package ce1002.a8.s995001561;

import java.awt.GridLayout;
import java.util.Scanner;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MyFrame extends JFrame{
		
	MyFrame(){
		
		// create variables
		int[] grade = new int[8];
		double[] Level = new double[4];
		int n;
		System.out.println( "Input grades: " );
		Scanner input = new Scanner(System.in);	
		
		// set values
		for(int i =0; i<8; i++) {
		
		n = input.nextInt();
		
		while(n<0 || n>100) {
		System.out.println( "Out of range!" );
		n = input.nextInt();
		}
		
		grade[i] = n;
		if(grade[i]>=0&&grade[i]<=25) Level[0] = Level[0]+1;
		else if(grade[i]>=26&&grade[i]<=50) Level[1] = Level[1]+1;
		else if(grade[i]>=51&&grade[i]<=75) Level[2] = Level[2]+1;
		else if(grade[i]>=76&&grade[i]<=100) Level[3] = Level[3]+1;
	    }
		
		
		System.out.println("Level 0 is " + Level[0]*100/8 + "%");
		System.out.println("Level 1 is " + Level[1]*100/8 + "%");
		System.out.println("Level 2 is " + Level[2]*100/8 + "%");
		System.out.println("Level 3 is " + Level[3]*100/8 + "%");
	
	  
	input.close();
		
	// create rectangles
	setLayout(new GridLayout(1,2,5,5));
	add(new Rectangles(Level[0], 0));
	add(new Rectangles(Level[1], 1));
	add(new Rectangles(Level[2], 2));
	add(new Rectangles(Level[3], 3));
	

	}
	
}

