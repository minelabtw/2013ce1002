package ce1002.a8.s102502504;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame
{
	public MyFrame(double[] level) //level陣列從a8被放到myframe
	{
		MyPanel panel0 = new MyPanel(0 , level[0]); //陣列level再從myframe丟給mypanel用
		MyPanel panel1 = new MyPanel(1 , level[1]);
		MyPanel panel2 = new MyPanel(2 , level[2]);
		MyPanel panel3 = new MyPanel(3 , level[3]);
		
		setBounds(0,0,425,300); //設定frame起始位置和大小
		setLayout(null); 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null); //置中
		setVisible(true); //設為可見 
		
		panelx(panel0 , 30 , 10);
		panelx(panel1 , 120 ,10);
		panelx(panel2 , 210 ,10);
		panelx(panel3 , 300 ,10);
	}
	
	public void panelx(MyPanel p , int x , int y) //用來設定panel的大小跟起始位置等等(myframe跟mypanel沒有繼承關係 所以先新增mypanel物件，再來這邊設定)
	{
		p.setBounds(x,y,80,230); 
		add(p); 
	}

}
