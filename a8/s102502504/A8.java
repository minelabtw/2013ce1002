package ce1002.a8.s102502504;

import java.util.Scanner;

public class A8 
{
	public static void main(String[] args) 
	{
		int grade;
		int people0=0,people1=0,people2=0,people3=0;
		double[] level = new double[4]; //用陣列存各個level的人數有多少個
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input grades: ");
		for(int i=0; i<=7; i++) //判斷輸入的數有無超出範圍
		{
			do
			{
				grade = input.nextInt(); //把輸入的數丟給grade
				if(grade<0 || grade>100) //超出範圍顯示out of range
				{
					System.out.println("Out of range!");
				}
			} while(grade<0 || grade>100);
			if(grade>=0 && grade<=25) //如果介於0~25
			{
				people0++; //people從0開始加上去
				level[0] = people0;
			}
			if(grade>=26 && grade<=50) //如果介於26~50
			{
				people1++; //people從0開始加上去
				level[1] = people1;
			}
			if(grade>=51 && grade<=75) //如果介於51~75
			{
				people2++; //people從0開始加上去
				level[2] = people2;
			}
			if(grade>=76 && grade<=100) //如果介於76~100
			{
				people3++; //people從0開始加上去
				level[3] = people3;
			}
		}
		
		System.out.println("Level 0 is " + level[0]/8 * 100 + "%");
		System.out.println("Level 1 is " + level[1]/8 * 100 + "%");
		System.out.println("Level 2 is " + level[2]/8 * 100 + "%");
		System.out.println("Level 3 is " + level[3]/8 * 100 + "%");
		
		input.close();
		
		MyFrame f = new MyFrame(level); //建立myframe物件(並且把level陣列傳入myframe才可使用到a8內的數值)

	}
	
}
