package ce1002.a8.s102502504;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel 
{
	protected int k;
	protected double level;

	public MyPanel(int i,double level) //這個建構子裡面的東西都是下面要用到的
	{
		k=i;
		this.level=level; 
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if(k==0) //panel0時
		{
			g.setColor(Color.gray);
			g.fillRect(30,20,20,130);
			
			g.setColor(Color.red);
			g.fillRect(30,(int)(130-(130*(level/8)-20)),20,(int)(130*level/8));
			
			g.setColor(Color.black);
			g.drawString("Level 0",20,170);
			g.drawString(level/8 * 100 +"%",20,200);
			
		}
		if(k==1) //panel1時
		{
			g.setColor(Color.gray);
			g.fillRect(30,20,20,130);
			
			g.setColor(Color.red);
			g.fillRect(30,(int)(130-(130*level/8-20)),20,(int)(130*level/8));
			
			g.setColor(Color.black);
			g.drawString("Level 1",20,170);
			g.drawString(level/8 * 100 +"%",20,200);
		}
		if(k==2) //panel2時
		{
			g.setColor(Color.gray);
			g.fillRect(30,20,20,130);
			
			g.setColor(Color.red);
			g.fillRect(30,(int)(130-(130*level/8-20)),20,(int)(130*level/8));
			
			g.setColor(Color.black);
			g.drawString("Level 2",20,170);
			g.drawString(level/8 * 100 +"%",20,200);
		}
		if(k==3) //panel3時
		{
			g.setColor(Color.gray);
			g.fillRect(30,20,20,130);
			
			g.setColor(Color.red);
			g.fillRect(30,(int)(130-(130*level/8-20)),20,(int)(130*level/8));
			
			g.setColor(Color.black);
			g.drawString("Level 3",20,170);
			g.drawString(level/8 * 100 +"%",20,200);
		}
	}
}

