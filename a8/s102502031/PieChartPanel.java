package ce1002.a8.s102502031;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PieChartPanel extends JPanel {
	// declaration of variables
	private int[] levels;
	private int studentsNumber;
	private int panelWidth;
	private int panelHeight;

	// declaration of objects
	JLabel[][] labels;

	public PieChartPanel(int levels[], int panelWidth, int panelHeight) {
		// give value to variables
		this.levels = levels;
		int studentsNumber = 0;
		for (int i = 0; i < levels.length; i++) {
			studentsNumber = studentsNumber + levels[i];
		}
		setStudentsNumber(studentsNumber);
		setPanelWidth(panelWidth);
		setPanelHeight(panelHeight);

		// basic panel setting
		this.setSize(getPanelWidth(), getPanelHeight());
		this.setLocation(10, 20);
		this.setLayout(null);

		// label setting and adding
		labels = new JLabel[levels.length][2];
		for (int i = 0; i < levels.length; i++) {
			labels[i][0] = new JLabel("Level " + i);
			labels[i][0].setLocation(10 + (getPanelWidth() - 50) / 4 * i + 10 * i, getPanelHeight() - 40);
			labels[i][0].setSize((getPanelWidth() - 30) / 3, 15);
			this.add(labels[i][0]);
			labels[i][1] = new JLabel((double) levels[i] / (double) studentsNumber * 100 + "%");
			labels[i][1].setLocation(10 + (getPanelWidth() - 50) / 4 * i + 10 * i, getPanelHeight() - 20);
			labels[i][1].setSize((getPanelWidth() - 30) * 2 / 3, 15);
			this.add(labels[i][1]);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int[] percentage = new int[levels.length];
		for (int i = 0; i < levels.length; i++) {
			percentage[i] = (int) ((double) levels[i] / (double) getStudentsNumber() * 360);
		}

		int colorValue = (int) (255.0 / (double) percentage.length);
		int startAngle = 0;
		for (int i = 0; i < percentage.length; i++) {
			g.setColor(new Color(colorValue * i, colorValue * i, colorValue * i));
			g.fillArc(10, 10, getPanelWidth() - 10, getPanelHeight() - 60, startAngle, percentage[i]);
			startAngle = startAngle + percentage[i];
		}
	}

	// getters
	public int getStudentsNumber() {
		return studentsNumber;
	}

	public int getPanelWidth() {
		return panelWidth;
	}

	public int getPanelHeight() {
		return panelHeight;
	}

	// setters
	public void setStudentsNumber(int studentsNumber) {
		this.studentsNumber = studentsNumber;
	}

	public void setPanelWidth(int panelWidth) {
		this.panelWidth = panelWidth;
	}

	public void setPanelHeight(int panelHeight) {
		this.panelHeight = panelHeight;
	}
}