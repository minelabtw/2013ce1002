package ce1002.a8.s102502031;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class MyFrame extends JFrame {
	// declaration of variables
	private int studentsNumber;
	private int levelsNumber;
	private int barChartPanelWidth = 70;
	private int panelHeight = 250;
	private final int frameXError = 11;
	private final int frameYError = 36;

	// constructor
	public MyFrame(int[] levels) {
		// give value of variables
		int studentsNumber = 0;
		for (int i = 0; i < levels.length; i++) {
			studentsNumber = studentsNumber + levels[i];
		}
		this.setStudentsNumber(studentsNumber);
		this.setLevelsNumber(levels.length);

		// basic frame setting
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize((getPanelWidth() + 10) * getLevelsNumber() + 310 + frameXError, getPanelHeight() + 50 + frameYError);
		this.setLayout(null);

		// bar chart panels creation, setting and adding
		JPanel barChartBaseJPanel = new JPanel();
		barChartBaseJPanel.setSize((getPanelWidth() + 10) * getLevelsNumber() + 10, getPanelHeight() + 30);
		barChartBaseJPanel.setLocation(10, 10);
		barChartBaseJPanel.setBorder(new TitledBorder("Bar Chart"));
		barChartBaseJPanel.setLayout(null);
		BarChartPanel[] barPanels = new BarChartPanel[getLevelsNumber()];
		for (int i = 0; i < getLevelsNumber(); i++) {
			barPanels[i] = new BarChartPanel(i, levels[i], getStudentsNumber(), getPanelWidth(), getPanelHeight());
			barChartBaseJPanel.add(barPanels[i]);
		}
		this.add(barChartBaseJPanel);

		// pie chart panels creation, setting and adding
		JPanel pieChartBaseJPanel = new JPanel();
		pieChartBaseJPanel.setSize(270, getPanelHeight() + 30);
		pieChartBaseJPanel.setLocation(27 + (getPanelWidth() + 10) * getLevelsNumber(), 10);
		pieChartBaseJPanel.setBorder(new TitledBorder("Pie Chart"));
		pieChartBaseJPanel.setLayout(null);
		PieChartPanel piePanel;
		piePanel = new PieChartPanel(levels, 250, getPanelHeight());
		pieChartBaseJPanel.add(piePanel);
		this.add(pieChartBaseJPanel);
	}

	// getters
	public int getStudentsNumber() {
		return studentsNumber;
	}

	public int getLevelsNumber() {
		return levelsNumber;
	}

	public int getPanelWidth() {
		return barChartPanelWidth;
	}

	public int getPanelHeight() {
		return panelHeight;
	}

	// setters
	public void setStudentsNumber(int studentsNumber) {
		this.studentsNumber = studentsNumber;
	}

	public void setLevelsNumber(int levelsNumber) {
		this.levelsNumber = levelsNumber;
	}
}
