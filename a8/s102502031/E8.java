package ce1002.a8.s102502031;

import java.util.Scanner;

public class E8 {
	public static void main(String[] args) {
		// declaration of variables
		final int studentsNumber = 8;
		final int levelsNumber = 4;
		double[] studentsScore = new double[studentsNumber];
		int[] levels = new int[levelsNumber];

		// declaration of objects
		Scanner input = new Scanner(System.in);
		MyFrame frame;

		// data processing
		System.out.println("Input grades: ");
		for (int i = 0; i < studentsNumber; i++) {
			studentsScore[i] = cheakInRange(input);
			levels[classification(studentsScore[i], levelsNumber)]++;
		}

		// new frame
		frame = new MyFrame(levels);
		frame.setLocation(30, 30);
	}

	// the method used to check the input is in range[0, 100] and return the legal input value
	public static double cheakInRange(Scanner input) {
		double a;
		do {
			a = input.nextDouble();
			if (a < 0 || a > 100) {
				System.out.println("Out of range!");
			}
		} while (a < 0 || a > 100);
		return a;
	}

	// the method used to count the numbers of the data of every levels
	public static int classification(double studentsScore, int levelsNumber) {
		int returnValue = -1;
		for (int i = 0; i < levelsNumber; i++) {
			if (studentsScore <= (i + 1) * 100 / levelsNumber) {
				returnValue = i;
				i = levelsNumber;
			}
		}
		return returnValue;
	}
}