package ce1002.a8.s102502031;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class BarChartPanel extends JPanel {
	// declaration of variables
	private int levelValue;
	private double percentage;
	private int panelWidth;
	private int panelHeight;

	// declaration of objects
	String[] strings = new String[2];
	JLabel[] labels = new JLabel[2];

	public BarChartPanel(int ordinal, int levelValue, int studentsNumber, int panelWidth, int panelHeight) {
		// give value to variables
		setLevelValue(levelValue);
		setPercentage((double) getLevelValue() / (double) studentsNumber);
		setPanelWidth(panelWidth);
		setPanelHeight(panelHeight);

		// basic panel setting
		this.setSize(getPanelWidth(), getPanelHeight());
		this.setLocation(ordinal * (getPanelWidth() + 10) + 10, 20);
		this.setLayout(null);

		// label setting and adding
		strings[0] = "Level " + ordinal;
		strings[1] = getPercentage() * 100 + "%";
		for (int i = 0; i < labels.length; i++) {
			labels[i] = new JLabel(strings[i]);
			labels[i].setLocation(10, getPanelHeight() - 40 + i * 20);
			labels[i].setSize(getPanelWidth() - 20, 15);
			this.add(labels[i]);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int figureHeight = (int) ((getPanelHeight() - 60) * getPercentage());
		g.setColor(new Color(210, 215, 255));
		g.fillRect(getPanelWidth() / 2 - 5, 10, 10, getPanelHeight() - 60);

		g.setColor(Color.blue);
		g.fillRect(getPanelWidth() / 2 - 5, getPanelHeight() - 50 - figureHeight, 10, figureHeight);
	}

	// getters
	public int getLevelValue() {
		return levelValue;
	}

	public double getPercentage() {
		return percentage;
	}

	public int getPanelWidth() {
		return panelWidth;
	}

	public int getPanelHeight() {
		return panelHeight;
	}

	// setters
	public void setLevelValue(int levelValue) {
		this.levelValue = levelValue;
	}

	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

	public void setPanelWidth(int panelWidth) {
		this.panelWidth = panelWidth;
	}

	public void setPanelHeight(int panelHeight) {
		this.panelHeight = panelHeight;
	}
}
