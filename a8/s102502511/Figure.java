package ce1002.a8.s102502511;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Figure extends JPanel {
	
	double percent; //定一個變數percent
	
	Figure(double percent) //建構值中帶著傳入的參數
	{
		setLayout(null);
	    this.percent = percent; //傳入的參數等於這個類別中的percent
		
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.setColor(Color.GRAY); //長條圖顏色為灰色
		g.fillRect(0, 0, 10, 100); //從0,0畫起 大小為10*100
		
		g.setColor(Color.RED); //記錄的長條圖為紅色
		g.fillRect(0, (int)(100 - percent), 10, (int)percent); //從0,100-%數開始往下畫
	}
}
