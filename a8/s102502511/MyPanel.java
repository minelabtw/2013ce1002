package ce1002.a8.s102502511;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	MyPanel(int level , double percent){ //建構值中帶入參數
		setLayout(null);
		
		Figure fig = new Figure(percent); //呼叫figure 並且傳回%數
		JLabel lab1 = new JLabel("Level " + level); //呼叫出lab 並且在其中設定裡面的內容
		JLabel lab2 = new JLabel(percent + "%");
		
		fig.setBounds(0,0,10,100); //定figure的大小和起始點
		lab1.setBounds(0, 105, 50, 10);//定lab的大小和起始點
		lab2.setBounds(0, 125, 50, 10);
		
		add(fig); //加入figure
		add(lab1); //加入lab
		add(lab2);
	}
}
