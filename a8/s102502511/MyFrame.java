package ce1002.a8.s102502511;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame(double [] rate){
		setBounds(300,200,500, 500); //設定起始點和大小
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		MyPanel panell = new MyPanel(0, rate[0]); //叫出panel 並且傳入level的數字 和 %數
		MyPanel panel2 = new MyPanel(1, rate[1]);
		MyPanel panel3 = new MyPanel(2, rate[2]);
		MyPanel panel4 = new MyPanel(3, rate[3]);
		
		panell.setBounds(20, 10, 60, 200); //規定panel的起始點 和 大小
		panel2.setBounds(80, 10, 60, 200);
		panel3.setBounds(150, 10, 60, 200);
		panel4.setBounds(210, 10, 60, 200);
		add(panell); //加入panel
		add(panel2);
		add(panel3);
		add(panel4);
	}
}
