package ce1002.a8.s102502511;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {	
		
		int [] Grade = new int [8]; //記錄成績的陣列
		double [] frequency = new double [4]; //記錄次數的陣列
		double [] percent = new double [4]; //記綠%數的陣列
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input grades: ");
		for(int i = 0 ; i < 8 ; i++){ //存取成績 並且判斷是否符合規則
			do{
				Grade[i] = input.nextInt();
				if(Grade[i] < 0 || Grade[i] > 100){
					System.out.println("Out of range!");
				}
			}while(Grade[i] < 0 || Grade[i] > 100);
		}
		
		for(int i = 0 ; i < 8 ; i++){ //分類別
			if(Grade[i] >= 0 && Grade[i] <= 24){
				frequency[0]++;
			}
			if(Grade[i] >= 25 && Grade[i] <= 50){
				frequency[1]++;
			}
			if(Grade[i] >= 51 && Grade[i] <= 74){
				frequency[2]++;
			}
			if(Grade[i] >= 75 && Grade[i] <= 100){
				frequency[3]++;
			}
		}
		
		for(int i = 0 ; i < 4 ; i++){ //記錄%數
			percent[i] = frequency[i] / 8 * 100;
		}
		
		for(int i = 0 ; i < 4 ; i++){
			System.out.println("Level " + i + " is " + percent[i]+ "%");
		}
		
		MyFrame myframe = new MyFrame(percent); //呼叫MyFrame
		
		input.close();
	}

}
