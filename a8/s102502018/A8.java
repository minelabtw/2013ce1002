package ce1002.a8.s102502018;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		int grade=0;
		double level[] = new double [4];
		double l0=0,l1=0,l2=0,l3=0;		
		System.out.println("Input grades: ");		
		for(int a=0;a<8;a++)
		{
			grade = scan.nextInt();
			while(grade<0||grade>100)
			{
				System.out.println("Out of range!");
				grade = scan.nextInt();
			}
			if(grade>=0&&grade<=25)			//各範圍出現的次數
			{
				l0++;
			}
			else if(grade>25&&grade<=50)
			{
				l1++;
			}
			else if(grade>50&&grade<=75)
			{
				l2++;
			}
			else if(grade>75&&grade<=100)
			{
				l3++;
			}
		}		
		level[0]=l0/8*100;					//每個等級百分比值
		level[1]=l1/8*100;
		level[2]=l2/8*100;
		level[3]=l3/8*100;
		System.out.println("level 0 is "+level[0]+"%");
		System.out.println("level 1 is "+level[1]+"%");
		System.out.println("level 2 is "+level[2]+"%");
		System.out.println("level 3 is "+level[3]+"%");
		scan.close();
		Frame frame = new Frame(level[0],level[1],level[2],level[3]);
		
	}

}
