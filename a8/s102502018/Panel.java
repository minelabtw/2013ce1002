package ce1002.a8.s102502018;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class Panel extends JPanel{
	
	protected int p;
	protected JLabel label = new JLabel();
	protected JLabel label1 = new JLabel();
	public Panel(int x,int y,double a,String t)
	{
		setLayout(null);
		setBounds(x,y,600,300);
		p = (int) (a*2);
		label.setBounds(10, 200, 50, 50);
		label.setText(t);
		label1.setBounds(10, 220, 50, 50);
		label1.setText(a+"%");
		add(label);
		add(label1);
	}
	public void paintComponent(Graphics g)	  	//長條圖
	{
		g.setColor(Color.GRAY);
		g.fillRect(10, 10, 30, 200);
		g.setColor(Color.BLUE);
		g.fillRect(10, 210-p, 30, p);			//畫出所佔比例
	}
	
	
	
}
