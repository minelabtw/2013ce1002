package ce1002.a8.s102502018;
import java.awt.LayoutManager;

import javax.swing.JFrame;
public class Frame extends JFrame{	
	protected Panel panel;
	protected Panel panel1;
	protected Panel panel2;
	protected Panel panel3;
	public Frame(double a,double b,double c,double d)
	{
		panel = new Panel(10,10,a,"Level0");           //設定panel位置及代入比例和等級
		panel1 = new Panel(110,10,b,"Level1");
		panel2 = new Panel(210,10,c,"Level2");
		panel3 = new Panel(310,10,d,"Level3");
		setLayout(null);
		setBounds(200,300,390,320);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(panel);
		add(panel1);
		add(panel2);
		add(panel3);
		
	}

}
