package ce1002.a8.s102502561;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Panel;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	
	
	MyPanel() {				//建構子

		this.setBackground(Color.BLUE);
		setBounds(10,10,700,400) ;
		setBorder(new LineBorder(Color.BLACK,3)) ;//粗框

		JLabel[] labellv = new JLabel[4] ;//註解處 LABEL
		JLabel[] labelper = new JLabel[4];
		
		Gradeasker gradeasker = new Gradeasker();
		for (int i=0;i<4;i++){
			labellv[i]=new JLabel() ;
			labelper[i]=new JLabel() ;
			labellv[i].setBounds(100*i+10,650,150,50) ;	
			labelper[i].setBounds(100*i+10,750,150,50) ;
				add(labellv[i]) ;			
				add(labelper[i]) ;
			}

		labellv[0].setText("lv0") ;	//每個label
		labellv[1].setText("lv1") ;
		labellv[2].setText("lv2") ;
		labellv[3].setText("lv3") ;
		labelper[0].setText(gradeasker.getlvpercent(0) + "%") ;	//每個label
		labelper[2].setText(gradeasker.getlvpercent(2) + "%") ;
		labelper[3].setText(gradeasker.getlvpercent(3) + "%") ;
		
		
	}
	
	public void paintComponent(Graphics gp)
	{
		Gradeasker g = new Gradeasker();
		for(int i =0;i<4;i++)
		{
		gp.setColor(Color.RED) ;//長方形 
		gp.fillRect(10+i*200,10,30,200) ;
		gp.setColor(Color.GRAY);
		int percent = (int) (g.getlvpercent(i)*200);//取得percent來繪製%of gray area
		gp.fillRect(10+i*200, 10, 30, 200*percent);
		
		}
	}
}

