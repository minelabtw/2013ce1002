package ce1002.a8.s102502561;

import java.util.Scanner;

public class Gradeasker {

	Scanner input = new Scanner(System.in);
	
	int n=0,w=0,x=0,y=0,z=0;
	double[] lv = new double[4];
	double[] lb = new double[4];
	int arr[] = new int[8];
	int grade[][] = new int[4][8];	//四個等級,八個學生去分

	public void getgrade() {
		System.out.println("Input grades: ");

		for (int i = 0; i < 8; i++) {
			n = input.nextInt(); // 讀入一成績
			while (n < 0 || n > 100) // 當超出範圍
			{
				System.out.println("Out of range!");
				n = input.nextInt(); // 矯正範圍
			}
			arr[i] = n; // 數字存到這陣列
		}
	}
	
	public void setgrade(){
		
		for(int i=0;i<8;i++){	//八位學生分級方法w,x,y,z初始值=0
			if(arr[i]>=0&&arr[i]<=25)
			{
				grade[0][w] = arr[i];		//grade[0][w]代表最低等級 w範圍從0~8，此級每多位學生w+1
				w++;
			}
			else if(arr[i]>=26&&arr[i]<=50)
			{
				grade[1][x] = arr[i];
				x++;
			}
			else if(arr[i]>=51&&arr[i]<=75)
			{
				grade[2][y] = arr[i];
				y++;
			}
			else if(arr[i]>=75&&arr[i]<=100)
			{
				grade[3][z] = arr[i];
				z++;
			}
		}
		
	}
	
	public void getgradepercent(){
		
		lv[0] = w/8.0*100;			//int/int一定會得到int 所以用8.0(double)去做除法得double數
		lv[1] = x/8.0*100;
		lv[2] = y/8.0*100;
		lv[3] = z/8.0*100;	
		this.lb[0] = lv[0];
		this.lb[1] = lv[1];
		this.lb[2] = lv[2];
		this.lb[3] = lv[3];
	
		System.out.println("Level 0 is "+lv[0]+"%");
		System.out.println("Level 1 is "+lv[1]+"%");
		System.out.println("Level 2 is "+lv[2]+"%");
		System.out.println("Level 3 is "+lv[3]+"%");
		
	}
	public int getlv(int i){
		return i;
	}
	public double getlvpercent(int i){
		double percent = lb[i];
		return percent;
	}

}
