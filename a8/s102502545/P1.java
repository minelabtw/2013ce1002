package ce1002.a8.s102502545;

import javax.swing.*;

import java.awt.*;

public class P1 extends JPanel {
	JLabel jlb1 = new JLabel();// 宣告標籤
	JLabel jlb2 = new JLabel();// 宣告標籤
	float grade = 0;
	int rate=0;
	
	

	public P1(float grade) {
		
		this.grade = grade;
		this.rate = (int) (85-(85* grade/100));
		
		setLayout(null);
		setBounds(0, 0, 50, 200);// 此Panel的邊框

		jlb1.setText("Level 0");
		jlb1.setBounds(6, 105, 50, 20);
		add(jlb1);

		jlb2.setText("" + grade + "%");
		jlb2.setBounds(6, 125, 50, 20);
		add(jlb2);
	}
	
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.red);//長條圖
		g.fillRect(10, 10, 10, 85);
		g.setColor(Color.gray);
		g.fillRect(10, 10, 10,rate);
		
	}
}
