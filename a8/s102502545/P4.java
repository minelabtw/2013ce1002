package ce1002.a8.s102502545;

import javax.swing.*;

import java.awt.*;

public class P4 extends JPanel {
	JLabel jlb1 = new JLabel();// 宣告標籤
	JLabel jlb2 = new JLabel();// 宣告標籤
	float grade = 0;
	int rate = 0;

	public P4(float grade) {
		
		this.grade = grade;
		this.rate = (int) (85-(85* grade/100));
		
		setLayout(null);
		setBounds(150, 0, 50, 200);// 此Panel的邊框

		jlb1.setText("Level 3");
		jlb1.setBounds(6, 105, 50, 20);// 標籤製作
		add(jlb1);

		jlb2.setText("" + grade + "%");
		jlb2.setBounds(6, 125, 50, 20);
		add(jlb2);
	}

	

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.red);
		g.fillRect(10, 10, 10, 85);// 長條圖製作
		g.setColor(Color.gray);
		g.fillRect(10, 10, 10, rate);
	}
}
