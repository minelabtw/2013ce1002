package ce1002.a8.s102502545;

import javax.swing.*;

import java.awt.*;

public class MyFrame extends JFrame {

	float grade1 = 50;
	float grade2 = 50;
	float grade3 = 50;
	float grade4 = 50;

	

	public MyFrame(float grade1, float grade2, float grade3, float grade4) {
		
		this.grade1 = grade1;
		this.grade2 = grade2;// 傳值
		this.grade3 = grade3;
		this.grade4 = grade4;
		
		P1 p1 = new P1(grade1);
		P2 p2 = new P2(grade2);
		P3 p3 = new P3(grade3);
		P4 p4 = new P4(grade4);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 關閉視窗後結束程式
		setLayout(null);		

		add(p1);// 把PANEL丟進去
		add(p2);
		add(p3);
		add(p4);

		setSize(300, 200);// 利用frame設定外框大小
		setVisible(true);

	}

	
}
