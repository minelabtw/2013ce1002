package ce1002.a8.s102502043;
import java.awt.*;
import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.JLabel;
public class MyPanel extends JPanel
{
	protected JLabel label = new JLabel();
	protected int Level;
	protected int x,y;
	MyPanel()
	{
		setSize(400,500);//設定panel大小
		setLayout(null);//自行排版
		add(label);//將label文字傳入
	}
	@Override public void paintComponent(Graphics g)
	{
		g.drawRect(90*getx()+20,20,50,400);//將長條圖先畫出
		g.setColor(Color.BLACK);//將趴數得地方上色
		g.fillRect(90*getx()+20,420-gety()*50,50,gety()*50);//將趴數得範圍畫出
	}
	public void setWord(int num,double per)
	{
		label.setBounds(90*num+20,430,90,50);//設定文字位置
		label.setText("Level"+num+" "+per+"%");//設定文字內容
	}
	public void setx(int x)//利用setter getter取得參數並使用
	{
		this.x=x;
	}
	public int getx()
	{
		return this.x;
	}
	public void sety(int y)
	{
		this.y=y;
	}
	public int gety()
	{
		return this.y;
	}
	
	
}
