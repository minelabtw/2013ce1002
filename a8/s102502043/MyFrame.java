package ce1002.a8.s102502043;
import javax.swing.JFrame;
public class MyFrame extends JFrame
{
	protected MyPanel panel0 = new MyPanel();
	protected MyPanel panel1 = new MyPanel();
	protected MyPanel panel2 = new MyPanel();
	protected MyPanel panel3 = new MyPanel();
	MyFrame(double per0,double per1,double per2,double per3,int l0,int l1,int l2,int l3)
	{
		setLayout(null);//框架自行排版
		setBounds(0,0,420,500);//框架設定大小
		panel0.setWord(0,per0);//將word餐數傳入
		panel0.setx(0);
		panel0.sety(l0);
		panel1.setWord(1,per1);
		panel1.setx(1);
		panel1.sety(l1);
		panel2.setWord(2,per2);
		panel2.setx(2);
		panel2.sety(l2);
		panel3.setWord(3,per3);
		panel3.setx(3);
		panel3.sety(l3);
		add(panel0);//加入面板
		add(panel1);
		add(panel2);
		add(panel3);
	}
}
