package ce1002.a8.s102502043;
import java.util.Scanner;
public class A8 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int grade[]=new int[8];
		int l1=0;
		int l2=0;
		int l3=0;
		int l4=0;
		double level[]=new double[4];
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++)//利用迴圈輸入八個數字
		{
			grade[i] = input.nextInt();
			while(grade[i]<0||grade[i]>100)//限定輸入廢完
			{
				System.out.println("Out of range!");
				grade[i] = input.nextInt();
			}
		}
		for(int j=0;j<8;j++)//將八個輸入的數字分區
		{
			if(grade[j]>=0&&grade[j]<=25)
			{
				l1++;
			}
			else if(grade[j]>=26&&grade[j]<=50)
			{
				l2++;
			}
			else if(grade[j]>=51&&grade[j]<=75)
			{
				l3++;
			}
			else if(grade[j]>=76&&grade[j]<=100)
			{
				l4++;
			}
		}

		level[0]=l1;
		level[1]=l2;
		level[2]=l3;
		level[3]=l4;
		for(int t=0;t<4;t++)
		{
			System.out.println("Level "+t+" is "+(level[t]*100/8)+"%");
		}
		MyFrame frame = new MyFrame(level[0]*12.5,level[1]*12.5,level[2]*12.5,level[3]*12.5,l1,l2,l3,l4);//將拍數傳入和圖形上的趴數傳入
		frame.setVisible(true);
	}

}
