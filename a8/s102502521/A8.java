package ce1002.a8.s102502521;

import java.util.*;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		Level level[] = new Level[4];

		// 宣告變數
		int grade = 0;
		int[] sscore = new int[8];
		float[] count = new float[4];

		System.out.println("Input grades: ");

		// 初始化陣列
		for (int i = 0; i < 4; i++) {
			level[i] = new Level(i);
		}

		// 輸入成績
		for (int i = 0; i < 8; i++) {
			do {
				grade = scanner.nextInt();
				if (grade < 0 || grade > 100) {
					System.out.println("Out of range!");
				}
			} while (grade < 0 || grade > 100);
			sscore[i] = grade;
		}

		// 要記得關掉喔~~
		scanner.close();

		// 分類分類
		for (int i = 0; i < 8; i++) {
			if (sscore[i] >= 0 && sscore[i] <= 25) {
				count[0]++;
			} else if (sscore[i] >= 26 && sscore[i] <= 50) {
				count[1]++;
			} else if (sscore[i] >= 51 && sscore[i] <= 75) {
				count[2]++;
			} else {
				count[3]++;
			}
		}

		// 呼叫Level setter
		for (int i = 0; i < 4; i++) {
			count[i] = count[i] * 100 / 8;
			level[i].setLevel(count[i]);
			level[i].setCount(i);
			System.out.println("Level " + i + " is " + count[i] + "%");
		}

		// 呼叫視窗出來囉
		MyFrame myFrame = new MyFrame(level);
	}
}
