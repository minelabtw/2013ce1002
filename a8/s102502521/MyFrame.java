package ce1002.a8.s102502521;

import javax.swing.*;

public class MyFrame extends JFrame {
	protected Level[] level;

	MyFrame(Level[] level) {
		this.level = level;
		setLayout(null);
		setBounds(100, 100, 525, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		for (int i = 0; i < 4; i++) {
			add(level[i]);
		}
	}
}
