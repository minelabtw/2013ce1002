package ce1002.a8.s102502521;

import javax.swing.*;
import java.awt.*;

public class Level extends JPanel {
	private float level;
	private int count;

	Level(int i) {
		setLayout(null);
		setBounds(125 * i, 10, 100, 400);
	}

	//畫畫囉
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(50, 30, 20, 201);
		g.setColor(Color.GRAY);
		g.fillRect(50, 30, 19, 200);
		if(level!=0){
			g.setColor(Color.BLACK);
			g.fillRect(50, 30 + (200 - (int) (level * 2)), 20, (int) (level * 2)+1);
		}
		g.setColor(Color.RED);
		g.fillRect(50, 30 + (200 - (int) (level * 2)), 19, (int) (level * 2));
		g.setColor(Color.BLACK);
		g.drawString("Level "+count, 50, 250);
		g.drawString(level+"%", 50, 275);
	}

	//變數的setter 
	public void setLevel(float level) {
		this.level = level;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
