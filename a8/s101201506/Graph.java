package ce1002.a8.s101201506;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Graph extends JPanel{
	protected float percentage=0;
	
	public Graph()
	{
		percentage=0;
	}
	public Graph(float p)
	{
		percentage=p;
	}
	public void setPercentage(float p)
	{
		percentage=p;
	}
	public float getPercentage()
	{
		return percentage;
	}
	
	protected void paintComponent(Graphics g) //�e��
	{
		super.paintComponent(g);
		g.setColor(Color.pink);
		g.fillRect(0, 0, 40, 100-(int)percentage);
		g.setColor(Color.black);
		g.fillRect(0, 100-(int)percentage, 40, (int)percentage);
	}

}
	
