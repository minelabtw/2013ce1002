package ce1002.a8.s101201506;

import java.util.Scanner;

public class A8 {
	private static Scanner scanner;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		scanner = new Scanner(System.in);
		int grades[] = new int[8];
		float level[] = new float[4];

		System.out.println("Input grades: "); 

		for (int i = 0; i < 8; i++) { //總共輸入8次成績
			do {
				grades[i] = scanner.nextInt();
				if (grades[i] < 0 || grades[i] > 100)
					System.out.println("Out of range!");
			} while (grades[i] < 0 || grades[i] > 100);
		}

		for (int j = 0; j < 8; j++) { //分類統計
			if (grades[j] <= 25 && 0 <= grades[j])
				level[0]++;
			if (grades[j] <= 50 && 26 <= grades[j])
				level[1]++;
			if (grades[j] <= 75 && 51 <= grades[j])
				level[2]++;
			if (grades[j] <= 100 && 76 <= grades[j])
				level[3]++;
		}

		for (int k = 0; k < 4; k++) { // 輸出

			level[k] = level[k] * 100 / 8;
			System.out.println("Level " + k + " is " + level[k] + "%");
		}
		MyFrame F = new MyFrame(level);
	}

}
