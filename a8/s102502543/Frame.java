package ce1002.a8.s102502543;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {
	protected Panel panel = new Panel();

	Frame(double n1, double n2, double n3, double n4) {
		panel.setNumber(n1, n2, n3, n4);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 300);
		add(panel);
	}
}
