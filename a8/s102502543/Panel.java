package ce1002.a8.s102502543;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
	JLabel label1a = new JLabel();
	JLabel label1b = new JLabel();
	JLabel label2a = new JLabel();
	JLabel label2b = new JLabel();
	JLabel label3a = new JLabel();
	JLabel label3b = new JLabel();
	JLabel label4a = new JLabel();
	JLabel label4b = new JLabel();
	double n1;
	double n2;
	double n3;
	double n4;

	Panel() {
		setBounds(0, 0, 600, 300);
		setLayout(null);
		label1a.setBounds(20, 190, 100, 10);
		label1b.setBounds(20, 210, 100, 10);
		label2a.setBounds(170, 190, 100, 10);
		label2b.setBounds(170, 210, 100, 10);
		label3a.setBounds(320, 190, 100, 10);
		label3b.setBounds(320, 210, 100, 10);
		label4a.setBounds(470, 190, 100, 10);
		label4b.setBounds(470, 210, 100, 10);
		add(label1a);
		add(label1b);
		add(label2a);
		add(label2b);
		add(label3a);
		add(label3b);
		add(label4a);
		add(label4b);
	}

	void setNumber(double a, double b, double c, double d) {
		n1 = a;
		n2 = b;
		n3 = c;
		n4 = d;
		label1a.setText("Level 0");
		label1b.setText("" + a * 100 + "%");
		label2a.setText("Level 1");
		label2b.setText("" + b * 100 + "%");
		label3a.setText("Level 2");
		label3b.setText("" + c * 100 + "%");
		label4a.setText("Level 3");
		label4b.setText("" + d * 100 + "%");
	}

	double getNumber1() {
		return n1;
	}

	double getNumber2() {
		return n2;
	}

	double getNumber3() {
		return n3;
	}

	double getNumber4() {
		return n4;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(20, 20, 20, 160);
		g.fillRect(170, 20, 20, 160);
		g.fillRect(320, 20, 20, 160);
		g.fillRect(470, 20, 20, 160);
		g.setColor(Color.RED);
		g.fillRect(20, (int) (20 + 160 * (1 - getNumber1())), 20,
				(int) (160 * getNumber1()));
		g.fillRect(170, (int) (20 + 160 * (1 - getNumber2())), 20,
				(int) (160 * getNumber2()));
		g.fillRect(320, (int) (20 + 160 * (1 - getNumber3())), 20,
				(int) (160 * getNumber3()));
		g.fillRect(470, (int) (20 + 160 * (1 - getNumber4())), 20,
				(int) (160 * getNumber4()));
	}
}
