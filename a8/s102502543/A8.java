package ce1002.a8.s102502543;

import java.util.Scanner;

public class A8 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int grades[];
		grades = new int[8];
		double levels[];
		levels = new double[4];
		System.out.println("Input grades: ");
		for (int a = 0; a < 8; a++) {
			do {
				grades[a] = input.nextInt();
				if (grades[a] < 0 || grades[a] > 100)
					System.out.println("Out of range!");
			} while (grades[a] < 0 || grades[a] > 100);
			if (grades[a] >= 0 && grades[a] <= 25)
				levels[0]++;
			if (grades[a] >= 26 && grades[a] <= 50)
				levels[1]++;
			if (grades[a] >= 51 && grades[a] <= 75)
				levels[2]++;
			if (grades[a] >= 76 && grades[a] <= 100)
				levels[3]++;
		}
		for (int a = 0; a < 4; a++)
			System.out.println("Level " + a + " is " + (levels[a] * 100 / 8)
					+ "%");
		Frame frame = new Frame(levels[0] / 8, levels[1] / 8, levels[2] / 8,
				levels[3] / 8);
	}
}
