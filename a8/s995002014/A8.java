package ce1002.a8.s995002014;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int[] grades = new int[8]; //分數陣列
		int i;
		System.out.print("Input grades: \n");
		
		//使用者輸入分數
		for(i=0;i<8;i++){
			do 
			{
				grades[i] = scanner.nextInt();
				if (grades[i]<0||grades[i]>100)
					System.out.print("Out of range! \n");
			} while (grades[i]<0||grades[i]>100);
		}
		scanner.close();
		float level0=0,level1=0,level2=0,level3=0;
		
		//計算各level中的人數
		for(i=0;i<8;i++){
			if(grades[i]<=25)level0++;
			else if(grades[i]<=50&&grades[i]>25)level1++;
			else if(grades[i]<=75&&grades[i]>50)level2++;
			else if (grades[i]<=100&&grades[i]>75)level3++;
		}
		
		//換成百分比
		level0/=8;level1/=8;level2/=8;level3/=8;
		
		//印出
		System.out.println("Level 0 is "+level0*100+"%");
		System.out.println("Level 0 is "+level1*100+"%");
		System.out.println("Level 0 is "+level2*100+"%");
		System.out.println("Level 0 is "+level3*100+"%");
		
		//for test
		/*float level0=30,level1=70,level2=60,level3=50;
		MyFrame frame = new MyFrame(level0,level1,level2,level3);*/
		
		MyFrame frame = new MyFrame(level0*100,level1*100,level2*100,level3*100);
		frame.setTitle("A8");

	}

}
