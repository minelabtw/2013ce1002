package ce1002.a8.s995002014;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Polygon;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyFrame extends JFrame {
	public MyFrame(float x,float y,float z,float w) {
		setLayout(new GridLayout(1,3,100,100));
		add(new MyRec(x,y,z,w));
		setSize(500, 275);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}


class MyRec extends JPanel {
	int l1,l2,l3,l4;
	float x,y,z,w;
	
	//建構子傳入四個float
	public MyRec(float x,float y,float z,float w){
		setBorder(BorderFactory.createLineBorder(Color.black,3));
		l1=(int)x;
		l2=(int)y;
		l3=(int)z;
		l4=(int)w;
		this.x=x;this.y=y;this.z=z;this.w=w;
	}
	public MyRec(){
		setBorder(BorderFactory.createLineBorder(Color.black,3));
	}
	
	//畫長條圖
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		//底色
		g.setColor(Color.gray); 
		g.fillRect(50, 50, 15, 100);
		g.setColor(Color.red);
		//百分比色條
		g.fillRect(50, 150-l1, 15,l1); 
		g.setColor(Color.black);
		//文字
		g.drawString("Level0", 50, 160); 
		g.drawString(Float.toString(x)+"%",50,175);
		
		g.setColor(Color.gray);
		g.fillRect(125, 50, 15, 100);
		g.setColor(Color.red);
		g.fillRect(125, 150-l2, 15,l2);
		g.setColor(Color.black);
		g.drawString("Level1", 125, 160); //文字
		g.drawString(Float.toString(y)+"%",125,175);
		
		g.setColor(Color.gray);
		g.fillRect(200, 50, 15, 100);
		g.setColor(Color.red);
		g.fillRect(200, 150-l3, 15,l3);
		g.setColor(Color.black);
		g.drawString("Level2", 200, 160); //文字
		g.drawString(Float.toString(z)+"%",200,175);
		
		g.setColor(Color.gray);
		g.fillRect(275, 50, 15, 100);
		g.setColor(Color.red);
		g.fillRect(275, 150-l4, 15,l4);
		g.setColor(Color.black);
		g.drawString("Level3", 275, 160); //文字
		g.drawString(Float.toString(w)+"%",275,175);
	}
}

