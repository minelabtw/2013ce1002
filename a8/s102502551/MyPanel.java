package ce1002.a8.s102502551;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private double percent;
	
	MyPanel(double percent, int x){
		setLayout(null);
		
		JLabel[] label=new JLabel[2];
		
		label[0]=new JLabel();
		label[1]=new JLabel();
		
		for (int y=0;y<2;y++){
			label[y].setBounds(0,200+20*y,130,30);											//設定位置和大小
		}
		
		setBounds(5+80*x, 10, 50, 300);
		
		this.percent=percent;
		
		if (x==0){
			label[0].setText("Level 0");
		}
		
		if (x==1){
			label[0].setText("Level 1");
		}
		
		if (x==2){
			label[0].setText("Level 2");
		}
		
		if (x==3){
			label[0].setText("Level 3");
		}
		
		label[1].setText(this.percent+"%");
		
		for (int y=0;y<2;y++){
			add(label[y]);
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
		
		g.setColor(Color.RED);														//畫紅色的矩形
		g.fillRect(0, 5, 10, 200);
		g.setColor(Color.GRAY);														//用灰色蓋掉紅色的
		g.fillRect(0, 5, 10, (int)(200*(1-percent/100)));
	}
}