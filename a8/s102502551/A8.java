package ce1002.a8.s102502551;

import java.util.Scanner; ;

public class A8 {

	public static void main(String[] args) {
		Level[] level=new Level[4];
		int[] grade=new int[8];												//存數字
		int gra=0;
		double[] percent=new double[4];
		Scanner scanner=new Scanner(System.in);
		
		System.out.println("Input grades:");
		
		level[0]=new Level(0);
		level[1]=new Level(1);
		level[2]=new Level(2);
		level[3]=new Level(3);
		
		for (int x=0;x<8;x++){												//判斷是否在範圍內
			gra=scanner.nextInt();
				
			if (gra<0 || gra>100){
				System.out.println("Out of range!");
				
				x=x-1;
			}
			else{
				grade[x]=gra;
			}
		}
		
		for (int x=0;x<8;x++){												//歸類
			if (grade[x]>50){
				if (grade[x]>75){
					level[3].counter(); 
				}
				else {
					level[2].counter();
				}
			}
			else {
				if (grade[x]<25){
					level[0].counter();
				}
				else {
					level[1].counter();
				}
			}
		}
		
		for (int x=0;x<4;x++){
			percent[x]=level[x].getcounter()/8.0*100;
		}
		
		for (int x=0;x<4;x++){
			System.out.println(level[x].getname()+" is "+percent[x]+"%");
		}
		
		MyFrame frame=new MyFrame(percent);
	}

}
