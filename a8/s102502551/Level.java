package ce1002.a8.s102502551;

public class Level {
	private int counter=0 ;
	private String x ;
	private String name ;
	
	Level(int number){
		setname("Level "+number) ;
	}
	
	public void counter(){
		counter++ ;
	}
	
	public int getcounter(){
		return counter ;
	}
	
	public void setname(String x){
		name=x ;
	}
	
	public String getname(){
		return name ;
	}

}
