package ce1002.a8.s102502551;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
	MyFrame(double[] percent){
		MyPanel panel0=new MyPanel(percent[0], 0);
		MyPanel panel1=new MyPanel(percent[1], 1);
		MyPanel panel2=new MyPanel(percent[2], 2);
		MyPanel panel3=new MyPanel(percent[3], 3);		
		
		setLayout(null);
		
		add(panel0);
		add(panel1);
		add(panel2);
		add(panel3);
		
		setBounds(10, 30, 400, 300);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setVisible(true);
	}
}