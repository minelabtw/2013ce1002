package ce1002.a8.s102502003;
import javax.swing.*;

public class Graph extends JPanel {
	private float percent;
	private String level;
	
	JLabel gradelevel = new JLabel();
	JLabel percentOflevel = new JLabel();
	
	Graph()
	{
        setLayout(null);
		
		//Set label
		gradelevel.setBounds(10, 120, 100, 40);
		percentOflevel.setBounds(10, 140, 180, 40);
		
		//Add label
		this.add(gradelevel);
		this.add(percentOflevel);
	}

	public float getPercent() {
		return percent;
	}

	public void setPercent(float percent) {
		this.percent = percent;
		//Set percentOflevel's text
		this.percentOflevel.setText(percent+"%");
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
		//Set gradelevel's text
		this.gradelevel.setText(level);
	}
	
	
	
	

}
