package ce1002.a8.s102502003;
import java.util.Scanner;
import javax.swing.*;


public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		int counter=0;
		float grade=0;
		float[] grades = new float[8];
		int numberOflevel0 = 0;
		int numberOflevel1 = 0;
		int numberOflevel2 = 0;
		int numberOflevel3 = 0;
		float level0 = 0;
		float level1 = 0;
		float level2 = 0;
		float level3 = 0;
		
		
		System.out.println("Input grades: ");
		do
		{
			do
			{
				grade=scanner.nextFloat();				
				if(grade<0||grade>100)
					System.out.println("Out of range!");			
			}while(grade<0||grade>100);
			grades[counter]=grade;
			counter++;
		}while(counter<8);
		
		scanner.close();
		
		for(counter=0; counter<8; counter++)
		{
			if(grades[counter]>=0 && grades[counter]<=25)
				numberOflevel0++;
			else if(grades[counter]>=26 && grades[counter]<=50)
				numberOflevel1++;
			else if(grades[counter]>=51 && grades[counter]<=75)
				numberOflevel2++;
			else
				numberOflevel3++;			
		}
		

		level0 = (numberOflevel0 / 8) * 100;
		level1 = (numberOflevel1 / 8) * 100;
		level2 = (numberOflevel2 / 8) * 100;
		level3 = (numberOflevel3 / 8) * 100;

		
		System.out.println("Level 0 is "+level0+"%");
		System.out.println("Level 1 is "+level1+"%");
		System.out.println("Level 2 is "+level2+"%");
		System.out.println("Level 3 is "+level3+"%");
		
		Graph rect = new DrawRect();
		rect.setPercent(level0);
		rect.setLevel("Level0");
		MyFrame frame = new MyFrame();

	}


}
