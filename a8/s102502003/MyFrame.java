package ce1002.a8.s102502003;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame {

	Graph panel0 = new DrawRect();
	Graph panel1 = new DrawRect();
	Graph panel2 = new DrawRect();
	Graph panel3 = new DrawRect();
	
	MyFrame() {
		setLayout(null);
		setBounds(0, 0, 400, 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//new Panel
		DrawGraph(panel0,10,10);
		DrawGraph(panel1,100,10);
		DrawGraph(panel2,200,10);
		DrawGraph(panel3,300,10);
		
	}
	
	void DrawGraph(Graph panel, int x, int y)
	{
		panel.setBounds(x, y, 20, 100);
		add(panel);
	}


}
