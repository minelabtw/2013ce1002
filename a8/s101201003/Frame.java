package ce1002.a8.s101201003;
import javax.swing.JFrame;
public class Frame extends JFrame{
	
	private Panel[] bar;
	public Frame(){
		bar=new Panel[4];
		setSize(500, 200);
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}//constructure
	
	public void paintbar(double[] level){
		for(int i=0;i<4;i++){
			bar[i] = new Panel(i,level[i]);
			bar[i].setBounds(i*50+8*(i+1),10,50,200);
			add(bar[i]);
		}
	}//method

}
