package ce1002.a8.s101201003;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class Panel extends JPanel{
	
	private int type;
	private double level;
	
	public Panel(int i , double j){
		type=i;
		level=j;
	}//constructure
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);		
		g.setColor(Color.blue);
		g.fillRect(0,0,10,100);
		g.setColor(Color.gray);
		g.fillRect(0,0,10,100-(int)level);
		g.drawString("Level "+type,0,120);
		g.drawString(level+ "%",0,140);
	}//paintbar
	
	
	

}
