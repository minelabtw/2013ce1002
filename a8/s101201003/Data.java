package ce1002.a8.s101201003;
import java.util.Scanner;
public class Data {
	private int[] score;
	private double[] level;
	
	public Data(){
		score=new int[8];
		level=new double[4];
		for(int i=0;i<4;i++){
			level[i]=0;	
		}
	}//constructure

	public void input(){
		Scanner inputscore=new Scanner(System.in);
		int times=0;
		System.out.println("Input grades: ");
		while(true)
		{
			score[times]=inputscore.nextInt();
			if(score[times]>=0&&score[times]<=100)
				times++;
			else
				System.out.println("Out of range!");
			if(times==8)
				break;
		}		
	}//method

	public void statics(){
		for(int i=0 ;i<8;i++)
		{
			if(score[i]>=0&&score[i]<=25)
				level[0]++;
			else if(score[i]>=26&&score[i]<=50)
				level[1]++;
			else if(score[i]>=51&&score[i]<=75)
				level[2]++;
			else
				level[3]++;
		}
		for(int i=0;i<4;i++){
			level[i]=(level[i]/8)*100;
		}		
	}//static
	public void output()
	{
		for(int i=0;i<4;i++){
			System.out.println("Level "+i+" is "+level[i]+ "%");
		}
	}
	public double[] getlevel()
	{
		return level;
	}
	
	
	
	
}
