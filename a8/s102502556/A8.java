package ce1002.a8.s102502556;

import java.util.Scanner;

public class A8 {
	
	public static void main (String args[]) {
		System.out.println("Input grades: ");
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		double[] level = new double[4]; //儲存每個等級的人數百分比
		for ( int i = 0 ; i < 8 ; i++ ) 
		{
			int temp = input.nextInt(); //暫存分數
			while ( temp < 0 || temp > 100 ) //判斷輸入是否合乎標準
			{
				System.out.println("Out of range!");
				temp = input.nextInt();
			}
			//計算每個等級的人數
			if ( temp <= 25 ) {
				level[0] += 1 ; 
			}
			else if ( temp >= 26 && temp <= 50 )	{
				level[1] += 1;
			}
			else if ( temp >= 51 && temp <= 75 )	{
				level[2] += 1;
			}
			else {
				level[3] += 1;
			}
		}
		input.close(); //關閉Scanner
		level[0] = level[0] / 8 * 100; //將人數更改成百分比
		level[1] = level[1] / 8 * 100;
		level[2] = level[2] / 8 * 100;
		level[3] = level[3] / 8 * 100;
		for ( int i = 0 ; i < 4 ; i++ ) //輸出結果
		{
			System.out.println("Level " + i + " is " + level[i] + "%");
		}
		MyFrame frame = new MyFrame(level); //新增視窗
	}
}
