package ce1002.a8.s102502556;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyPanel[] panel = new MyPanel[4]; //4個分數等級分別使用1個panel

	MyFrame ( double[] level ) {
		setSize(400,200); //視窗大小
		setLocationRelativeTo(null); //視窗預設位置 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //關閉視窗
		setLayout(null); //內部元素的排版方式
		//設定panel的屬性值
		for ( int i = 0 ; i < 4 ; i++ ) {
			panel[i] = new MyPanel();
			setPanel(panel[i], level[i], 10 + 100 * i, 10, i);
		}
		setVisible(true); //使視窗顯示在螢幕上
	}
	public void setPanel ( MyPanel panel, double level, int x, int y, int id ) {
		panel.setLocation(x, y); //設定位置
		panel.setState(level, id); 
		add(panel);
	}
}
