package ce1002.a8.s102502560;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class A8 {
	public static void main(String[] args) {
		Scanner STDIN=new Scanner(System.in);
		int[] scores=new int[8];
		int[] ranges=new int[4];
		int datas=0;
		int n;
		System.out.println("Input grades: ");
		while(datas<scores.length){
			n=STDIN.nextInt();
			if(n<0||n>100){System.out.println("Out of range!");continue;}
			scores[datas++]=n;
		}
		STDIN.close();
		
		for(int score: scores){
			if(score<=25)ranges[0]++;
			else if (score<=50)ranges[1]++;
			else if (score<=75)ranges[2]++;
			else ranges[3]++;
		}
		
		int max=max(ranges);
		
		double[] percents=new double[]{
				(double)ranges[0]/scores.length,
				(double)ranges[1]/scores.length,
				(double)ranges[2]/scores.length,
				(double)ranges[3]/scores.length
		};
		
		for(int i=0;i<percents.length;i++){
			System.out.println("Level "+i+" is "+percents[i]*100+"%");
		}
		new MyFrame(percents);
	}
	
	public static int max(int[] arr) {
		int max=arr[0];
		for(int i: arr){
			if(i>max)max=i;
		}
		return max;
	}
}
