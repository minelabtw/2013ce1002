package ce1002.a8.s102502560;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel{
	
	double[] p;
	public MyPanel(double[] p) {
		this.p=p;
		setPreferredSize(new Dimension(300,250));
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		g.setColor(Color.RED);
		for(int i=0; i<p.length; i++){
			g.fillRect(20+50*i, 10+0, 10, 100);
		}
		g.setColor(Color.DARK_GRAY);
		for(int i=0; i<p.length; i++){
			g.fillRect(20+50*i, 10+0, 10, (int)((1-p[i])*100));
		}
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.BOLD, 12));
		for(int i=0; i<p.length; i++){
			g.drawString("Level "+i, 20+50*i, 130);
			g.drawString(p[i]*100+"%", 20+50*i, 150);
		}
	}
}
