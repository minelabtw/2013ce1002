package ce1002.a8.s102502560;

import java.awt.FlowLayout;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	public MyFrame(double[] p) {
		setSize(300,250);
		setLayout(new FlowLayout());
		add(new MyPanel(p));
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
}
