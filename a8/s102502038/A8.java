package ce1002.a8.s102502038;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class A8 {
	private static int[] grades = new int[8];
	private static double[] pers = new double[5];
	public static void main(String[] args){
		A8 gui = new A8();
		int num;
		String inputln;
		for(int i = 0;i<5;i++){
			pers[i] = (double)0;
		}
		// input section
		System.out.println("Input grades:");
		for(int i = 0;i<8;i++){
			while(true){
				try{
					BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
					inputln = buff.readLine();
					num = Integer.parseInt(inputln);
				}catch(IOException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}catch(java.lang.NumberFormatException error){
					System.out.println("Input Section Error: " + error);
					continue;
				}
				if(num<=0){
					System.out.println("Out of range!");
					continue;
				}
				break;
			}
			grades[i] = num;
			if(num!=0){
				pers[(int) Math.floor((num - 1)/25)] += 12.5;
			}else{
				pers[0] += 12.5;
			}
		}
		//end of input section.
		gui.go();
	}
	public void go(){
		JFrame frame = new JFrame();
		frame.setTitle("Midi Mixer");
		frame.setVisible(true);
		frame.setSize(300,200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout());
		frame.getContentPane().add(new Chart("Level 0",pers[0]));
		frame.getContentPane().add(new Chart("Level 1",pers[1]));
		frame.getContentPane().add(new Chart("Level 2",pers[2]));
		frame.getContentPane().add(new Chart("Level 3",pers[3]));
		frame.getContentPane().add(new Chart("Level 4",pers[4]));//add panel
		
	}
	class Chart extends JPanel{//inner class
		private Bar bar;
		Chart(String name,double percent){
			bar = new Bar(percent);
			setSize(25,150);
			setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
			add(bar);
			add(new JLabel(name));
			add(new JLabel(String.valueOf(percent) + "%"));
		}
	}
	class Bar extends JPanel{
		private int barLength;
		private char[] name;
		Bar(double percent){
			setSize(15,120);
			barLength = (int) ((percent/100)*this.getHeight());
		}
		@Override public void paintComponent(Graphics g) {//graph
	    	super.paintComponent(g);
	    	g.setColor(Color.RED);
	    	g.fillRect(0,120 - barLength,15,barLength);
	    	g.setColor(Color.GRAY);
	    	g.fillRect(0,0,15,120 - barLength);
		}
	}
}
