package ce1002.a8.s101201504;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	protected int level=0;
	protected float percentage=0;
	protected Graph graph=new Graph();
	protected JLabel lLabel=new JLabel();
	protected JLabel pLabel =new JLabel();
	MyPanel(int l,float p)
	{
		level=l;
		percentage=p;
		setLayout(null);
		graph.setBounds(40,20,40,100);        //set bound
		graph.setPercentage(percentage);
		lLabel.setBounds(30,140,60,40);
		pLabel.setBounds(30,180,60,40);
		lLabel.setText("Level "+level);
		pLabel.setText(percentage+"%");
		add(graph);
		add(lLabel);
		add(pLabel);
	}

}
