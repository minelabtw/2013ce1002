package ce1002.a8.s101201504;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Graph extends JPanel{
	protected float percentage=0;
	Graph()
	{
		percentage=0;
	}
	Graph(float p)
	{
		percentage=p;
	}
	void setPercentage(float p)
	{
		percentage=p;
	}
	float getPercentage()
	{
		return percentage;
	}
	@Override
	protected void paintComponent(Graphics g) //use the graphics to print the graph
	{
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 20, 100-(int)percentage);
		g.setColor(Color.RED);
		g.fillRect(0, 100-(int)percentage, 20, (int)percentage);
	}

}
