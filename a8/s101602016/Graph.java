package ce1002.a8.s101602016;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Graph extends JPanel{
	protected double percent=0;
	void setPercent(double p){
		percent=p;//獲得百分比
	}
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.gray);//將長條圖上方畫灰色
		g.fillRect(0, 0, 20, 100-(int)percent);//依照百分比把長條圖上方畫完
		g.setColor(Color.RED);//將長條圖下方畫紅色
		g.fillRect(0, 100-(int)percent, 20, (int)percent);//依照百分比把長條圖下方畫完
	}
}
