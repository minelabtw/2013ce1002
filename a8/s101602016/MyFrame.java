package ce1002.a8.s101602016;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	protected MyPanel panel[]=new MyPanel[4];//因為有四個等級，所以需要四個板子
	MyFrame(double[] p){
		for (int i=0;i<4;i++){
			panel[i]=new MyPanel(i,p[i]);//將數值傳入MyPanel
			panel[i].setBounds(5+50*i,0,50,200);//設定每個等級的板子大小
		}
		setLayout(null);
		setSize(225,200);//設定視窗大小
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		for (int j=0;j<4;j++)
			add(panel[j]);//將四個板子加入
	}
}
