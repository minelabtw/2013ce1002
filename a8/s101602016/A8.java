package ce1002.a8.s101602016;

import java.util.Scanner;

public class A8 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double grades=0;//給使用者輸入成績
		double level[]=new double[4];//存放個等級成績數量的陣列
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input grades: ");//請使用者輸入成績
		for(int i=0;i<8;i++){
			do{//輸入成績,如超出範圍則重新輸入
				grades=scanner.nextDouble();
				if(grades<0||100<grades)
					System.out.println("Out of range!");
			}while(grades<0||100<grades);
			//判斷該成績在哪個等級範圍內
			if(0<=grades&&grades<=25)
				level[0]++;
			else if(26<=grades&&grades<=50)
				level[1]++;
			else if(51<=grades&&grades<=75)
				level[2]++;
			else if(76<=grades&&grades<=100)
				level[3]++;
		}
		for(int j=0;j<4;j++){
			level[j]=level[j]*12.5;//將等級換算成百分比
			System.out.println("Level "+j+" is "+level[j]+"%");//書出資料
		}
		MyFrame frame=new MyFrame(level);//畫長條圖
	}
}
