package ce1002.a8.s101602016;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	Graph graphPanel = new Graph();//長條圖
	JLabel levelLabel = new JLabel();//等級
	JLabel percentLabel = new JLabel();//百分比
	MyPanel(int level,double percent){
		setLayout(null);
		graphPanel.setBounds(5,5,20,100);//設定長條圖的大小,範圍
		graphPanel.setPercent(percent);//將百分比傳入Graph裡運算
		levelLabel.setBounds(5,115,50,20);//等級的大小,範圍
		percentLabel.setBounds(5,135,50,20);//百分比的大小,範圍
		levelLabel.setText("Level "+level);//貼上等級
		percentLabel.setText(percent+"%");//貼上百分比
		add(graphPanel);//加入長條圖
		add(levelLabel);//加入等級
		add(percentLabel);//加入百分比
	}
}
