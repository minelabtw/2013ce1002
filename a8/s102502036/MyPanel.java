package ce1002.a8.s102502036;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{  
	protected float c ;
	protected float d;
	protected float e ;
	protected float f ;     
  
     
	public MyPanel(float a[]){
		c=a[0];
		d=a[1];
		e=a[2];
		f=a[3];
		JLabel label = new JLabel("Level 0");//製作可放文字的六個label
	    JLabel label2 = new JLabel(""+(c*100/8)+"%");
	    JLabel label3 = new JLabel("Level 1");
	     JLabel label4 = new JLabel(""+d*100/8+"%");
	    JLabel label5 = new JLabel("Level 2");
	    JLabel label6 = new JLabel(""+e*100/8+"%");
	  JLabel label7 = new JLabel("Level 3");
	     JLabel label8= new JLabel(""+f*100/8+"%");
		setLayout(null);
		setBounds(0,0,260,350) ;
		label.setBounds(15,180,50,30);
		label2.setBounds(15,220,50,30);
		add(label) ;
		add(label2) ;
		label3.setBounds(70,180,50,30);
		label4.setBounds(70,220,50,30);
		add(label3) ;
		add(label4) ;
		label5.setBounds(130,180,50,30);
		label6.setBounds(130,220,50,30);
		add(label5) ;
		add(label6) ;
		label7.setBounds(190,180,50,30);
		label8.setBounds(190,220,50,30);
		add(label7) ;
		add(label8) ;
		
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawRect(15, 15, 20, 150);//畫六個矩形
		g.drawRect(70, 15, 20, 150);
		g.drawRect(130, 15, 20, 150);
        g.drawRect(190, 15, 20, 150);
		g.setColor(Color.BLACK) ;
		g.fillRect(15,(int)(150-(150*c/8)+16), 20,(int)(150*c/8)) ;		//並將顏色塗滿		
		g.fillRect(70,(int)(150-(150*d/8)+16), 20,(int)(150*d/8)) ;		
		g.fillRect(130,(int)(150-(150*e/8)+16), 20,(int)(150*e/8)) ;		
		g.fillRect(190,(int)(150-(150*f/8)+16), 20,(int)(150*f/8)) ;
	}

}
