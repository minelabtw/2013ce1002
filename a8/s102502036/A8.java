package ce1002.a8.s102502036;
import java.util.Scanner ;


public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in) ;
		
		float [] score = new float [8] ;
		float [] level = new float [4] ;
		float grade=0 ;
		System.out.print("Input grades:") ;
		for(int i=0 ;i<8 ;i++){
			do{	
						
			grade=input.nextInt();
			if(0>grade||grade>100){
				System.out.println("Out of range!") ;
			}
			
			}while(0>grade||grade>100) ;
			score[i] = grade ; 
			if(score[i]>=0&&score[i]<=25){
				level[0]++ ;
			}
			if(score[i]>=26&&score[i]<=50){
				level[1]++ ;
			}
			if(score[i]>=51&&score[i]<=75){
				level[2]++ ;
			}
			if(score[i]>=76&&score[i]<=100){
				level[3]++ ;
			}
			
		}
		MyFrame f = new MyFrame(level) ;//製作frame object並把陣列傳進去
		for(int i=0;i<4;i++){
			System.out.println("Level "+i+" is "+level[i]*100/8+"%") ;
		}
		

	}

}
