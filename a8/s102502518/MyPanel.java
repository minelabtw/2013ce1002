package ce1002.a8.s102502518;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
    
	public int count[] = new int[4];
	//constructor
	public MyPanel(int[] count)
	{
		//create 8 label to display level and percentage
		JLabel[] label = new JLabel[8];		
		setLayout(null);
		setLayout(new GridLayout(2,4,-80,-380));
		
		//add "Level" label
		for(int i=0;i<4;i++)
		{
			label[i] = new JLabel("Level " + i);
			add(label[i]);
		}
		
		//add "percentage" label
		for(int i=4;i<8;i++)
		{
			label[i] = new JLabel((float)count[i-4]/0.08 + "%");
			add(label[i]);
		}
		
		for(int i=0;i<4;i++)
		{
			this.count[i]=count[i];
		}
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fill3DRect(10, 20, 30, 160, true);
		g.fill3DRect(110, 20, 30, 160, true);
		g.fill3DRect(210, 20, 30, 160, true);
		g.fill3DRect(310, 20, 30, 160, true);
		//paint red on all rectangle region
		g.setColor(Color.GRAY);
		g.fill3DRect(10, 20, 30, 160-160*count[0]/8, true);
		g.fill3DRect(110, 20, 30, 160-160*count[1]/8, true);
		g.fill3DRect(210, 20, 30, 160-160*count[2]/8, true);
		g.fill3DRect(310, 20, 30, 160-160*count[3]/8, true);
		//paint gray partly on rectangle
	}
}