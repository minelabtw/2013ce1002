package ce1002.a8.s102502518;

import java.util.Scanner;

public class A8 {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int num;
		int [] g = new int[8];
		
		System.out.println("Input grades:");
		for (int i = 0; i < 8; i++) {
			do {
				num = input.nextInt();
				if (num < 0 || num > 100)
					System.out.println("Out of range!");
			} while (num < 0 || num > 100);
			
            g[i]=num;
		}
		input.close();
		int count[] = new int[4];
		for (int i = 0; i < 8; i++) {
	        if (g[i]>=0 && g[i]<=25)
	        	count[0]++;
	        if (g[i]>=26 && g[i]<=50)
	        	count[1]++;
	        if (g[i]>=51 && g[i]<=75)
	        	count[2]++;
	        if (g[i]>=76 && g[i]<=100)
	        	count[3]++;
        }
		System.out.println("Level 0 is "+ (float)count[0]/8*100 + "%");
		System.out.println("Level 1 is "+ (float)count[1]/8*100 + "%");
		System.out.println("Level 2 is "+ (float)count[2]/8*100 + "%");
		System.out.println("Level 3 is "+ (float)count[3]/8*100 + "%");
		MyFrame frame = new MyFrame(count);
	}
}
