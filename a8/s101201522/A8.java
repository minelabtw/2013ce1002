package ce1002.a8.s101201522;

import java.util.Scanner;
import javax.swing.*;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int grade;//student's grade
		double[] level = {0,0,0,0};
		System.out.println("Input grades: ");
		for (int i=0;i<8;i++) {//input grades and statistics 
			do {
				grade = input.nextInt();
				if (grade>=0 && grade<=25)
					level[0]++;
				else if (grade>=26 && grade<=50)
					level[1]++;
				else if (grade>=51 && grade<=75)
					level[2]++;
				else if (grade>=76 && grade<=100)
					level[3]++;
				else
					System.out.println("Out of range!");
			} while (grade<0 || grade>100);
		}
		for (int i=0;i<4;i++) {//output result
			level[i] /= 8;
			System.out.println("Level "+i+" is "+level[i]*100+"%");
		}
		input.close();
		
		//set frame's size, position, ... and add bar chart to frame
		JFrame frame = new JFrame();
		frame.setLayout(null);
		frame.setBounds(0,0,300,200);
		frame.add(new Graph(level));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
