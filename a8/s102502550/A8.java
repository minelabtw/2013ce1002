package ce1002.a8.s102502550;

import java.util.Scanner;

import javax.swing.JFrame;

public class A8 {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int[] a = new int[8];                       //輸入到的陣列
		double[] b = new double[4]; 				//轉換後的%數
		
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
				do {
					
		
					a[i] = in.nextInt();
		
					if (a[i] < 0 || a[i] > 100)
						System.out.println("Out of range!");
		
				} while (a[i] < 0 || a[i] > 100);
		}
		
		for(int i=0;i<8;i++){
			if(a[i]>=0 && a[i]<=25)
				b[0]++;
			else if(a[i]>=26 && a[i]<=50)
				b[1]++;
			else if(a[i]>=51 && a[i]<=75)
				b[2]++;
			else
				b[3]++;
		}
		
		for(int i=0;i<4;i++){
			b[i] = b[i]/8;
		}
		MyFrame f = new MyFrame(b);
		
		
	
		
	}

}
