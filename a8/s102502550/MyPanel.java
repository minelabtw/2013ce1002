package ce1002.a8.s102502550;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	private double[] a = new double[4];
	private JLabel[] j = new JLabel[8]; 
	
	MyPanel(double[] a){
		setLayout(null);
		setBounds(20 , 0 , 485 , 400);
		for(int i=0;i<a.length;i++)	
			this.a[i] = a[i];
		
		for(int i=0;i<4;i++){
			j[i] = new JLabel("Level "+i);
			j[i].setBounds(i*100 ,250 ,60 ,50 );
			add(j[i]);
		}
		
		for(int i=0;i<4;i++){
			j[i] = new JLabel(a[i]*100 +" %");
			j[i].setBounds(i*100 ,300 ,60 ,50 );
			add(j[i]);
		}
		
		
	}
	
	protected void paintComponent(Graphics g){                     
		super.paintComponent(g);
		
		g.setColor(new Color(255,0,0));

		for(int i=0;i<4;i++){                      //線畫紅長條
			
			g.fillRect(i*100, 20, 30, 200);
			
		}
		
		g.setColor(Color.gray);
		
		for(int i=0;i<4;i++){					   //再化灰色調
			g.fillRect(i*100, 20, 30, (int)(200*(1-a[i])));
		}
		
		
	}
}
