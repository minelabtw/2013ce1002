package ce1002.a8.s101303504;
import java.awt.*;

import javax.swing.*;
public class Grade extends JPanel{

	private double graPer;
	
	public Grade(){
		
	}
	
	public void setGP(double per){
		this.graPer = per;
	}
	
	public double getGP(){
		return graPer;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawString(getGP()+ "%", 10, 20);
	}
	
	public Dimension getPerferredSize(){
		return new Dimension(50,300);
	}
}
