package ce1002.a8.s101303504;
import java.awt.*;
import javax.swing.*;
public class MyPanel extends JPanel{

	public MyPanel(){
		
	}
	/* draw the form */
	private int y;
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(Color.RED);
		g.fillRect(10, 5, 20, 80);
		g.setColor(Color.BLACK);
		g.clearRect(10, 5, 20, 80-10*getY());
		g.drawRect(10, 5, 20, 80);
		
		
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public int getY(){
		return y;
	}
	
	public Dimension getPerferredSize(){
		return new Dimension(90,300);
	}
}
