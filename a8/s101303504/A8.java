package ce1002.a8.s101303504;
import java.util.Scanner;
import java.awt.*;

import javax.swing.*;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int []stu;
		int grade;
		int num0=0;
		int num1=0;
		int num2=0;
		int num3=0;
		stu = new int[8]; /* set a array to store grade */
		
		for(int i=0;i<8;i++){
			do{
			System.out.print("Input "+ (i+1) + " grade:");
			grade = input.nextInt();
			if(grade<0 || grade>100)
				System.out.println("Out of range!");
			}while(grade<0 || grade>100);
			stu[i] = grade;
	}
		
		for(int i=0;i<stu.length;i++){
			/* give the level */
			if(stu[i]<=25)
			{
			num0 += 1;
			}
			else if(stu[i]>25 && stu[i]<=50)
			{
				num1 += 1;
			}
			else if(stu[i]>50 && stu[i]<=75)
			{
				num2 += 1;
			}
			else if(stu[i]>75)
			{
				num3 += 1;
			}
			
		}
		/* print out the % */
		System.out.println("Level 0 is "+ (12.5*num0)+ "%");
		System.out.println("Level 1 is "+ (12.5*num1)+ "%");
		System.out.println("Level 2 is "+ (12.5*num2)+ "%");
		System.out.println("Level 3 is "+ (12.5*num3)+ "%");
		
		
		JFrame frame = new JFrame();
		/* give value for each panel */
		 MyPanel p0 = new MyPanel();
		 MyPanel p1 = new MyPanel();
		 MyPanel p2 = new MyPanel();
		 MyPanel p3 = new MyPanel();
		 level l0 = new level("0");
		 level l1 = new level("1");
		 level l2 = new level("2");
		 level l3 = new level("3");
		 /* set the form's red parts */
		    p0.setY(num0);
			p1.setY(num1);
			p2.setY(num2);
			p3.setY(num3);

		frame.setLayout(new GridLayout(3,4,1,1));/* array */
		/* all panel add in frame */
		frame.add(p0);
		frame.add(p1);
		frame.add(p2);
		frame.add(p3);
		
		frame.add(l0);
		frame.add(l1);
		frame.add(l2);
		frame.add(l3);
		
    
		Grade g0 = new Grade();
		Grade g1 = new Grade();
		Grade g2 = new Grade();
		Grade g3 = new Grade();
		g0.setGP(num0*12.5);
		g1.setGP(num1*12.5);
		g2.setGP(num2*12.5);
		g3.setGP(num3*12.5);
		
		frame.add(g0);
		frame.add(g1);
		frame.add(g2);
		frame.add(g3);
		// show the frame
		
		
		
		frame.setSize(250,350);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
		frame.setVisible(true);/* display */
}
		
}
