package ce1002.a8.s102502506;

import java.awt.*;
import javax.swing.*;
import java.text.NumberFormat;

public class levelPanel extends JPanel{
	public static int x=0;  //用來當計數器
	private double data;
	private JLabel t1 = new JLabel();
	public levelPanel(double D){  //輸出
		NumberFormat nf = NumberFormat.getPercentInstance();
	    nf.setMaximumFractionDigits( 2 );
		this.data = D;
		setBounds(0,0,100,300);
		t1.setText("<html>Level "+x+"<br>"+nf.format(D)+"</html>");
		x++;
		add(t1);
	}
	public void paintComponent(Graphics g){  //劃出長條圖
		g.setColor(Color.gray);
		g.fillRect(30, 50, 20, 200);
		g.setColor(Color.red);
		g.fillRect(30, 250-(int)(data*200), 20, (int)(data*200));
	}
}
