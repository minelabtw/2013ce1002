package ce1002.a8.s102502506;

import java.text.NumberFormat;
import java.util.Scanner;

public class A8 {
	public static void main(String[] args){
		Scanner P = new Scanner(System.in);
		NumberFormat NF = NumberFormat.getPercentInstance();
		NF.setMaximumFractionDigits(2);  //設定小數位數
		
		int[] L = new int[4];
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
			int G;
			do{
				G = P.nextInt();
				if(G>=0&&G<=100){
					if(G<25&&G>=0){
						L[0]++;
					}
					else if(G<50&&G>=25){
						L[1]++;
					}
					else if(G<75&&G>=50){
						L[2]++;
					}
					else if(G<=100&&G>=75){
						L[3]++;
					}
				}
				else{
					System.out.println("Out of range!");
				}
			}while(G<0||G>100);
		}
		double[] Ld=new double[4];
		for(int i=0;i<4;i++){  //輸出
			Ld[i]=(double)L[i]/8;
			System.out.println("Level "+i+" is " + NF.format((double)L[i]/8));
		}
		MyFrame Frame = new MyFrame(Ld);
	}
}
