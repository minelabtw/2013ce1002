package ce1002.a8.s102502527;

import java.util.Scanner;
import javax.swing.JFrame;

public class A8 extends JFrame
{
	MyPanel panel = new MyPanel();//叫出mypanel的class
	
	A8()
	{
		setSize(300, 200);//設定視窗大小
		setLocationRelativeTo(null);//置中or置左or置右
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//將離開的程式設入右上叉叉
		setContentPane(panel);
	}
	
	public void addGrade(int name, float number)
	{
		panel.addGrade(name, number);//叫出mypanel內的函示
	}
	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		A8 x = new A8();//設定此函式
		
		float[] level = new float[4];
		int grade = 0;
		
		System.out.println("Input grades: ");
		
		for (int i = 0 ; i < 8 ; i++)//將分數依序存進陣列
		{
			grade = input.nextInt();//當分數在範圍外時重新輸入
			while (grade < 0 || grade > 100)
			{
				System.out.println("Out of range!");
				grade = input.nextInt();
			}
			
			level[ grade / 25 + ( grade % 25 == 0 && grade > 0 ? -1 : 0 ) ]++;
		}
		
		for (int i = 0 ; i < 4 ; i++)//依序叫出直條圖
		{
			System.out.println("Level " + i + " is " + level[i] / 8 * 100 + "%");
			x.addGrade(i, level[i] / 8);
		}
		
		input.close();//終止輸入
		x.setVisible(true);//顯示視窗
	}

}

