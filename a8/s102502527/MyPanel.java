package ce1002.a8.s102502527;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	private float[] level = new float[4];
	
	MyPanel(){}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
        for(int i=0;i<4;i++)
        {
        	g.setColor(Color.black);//設定上半部的顏色
        	g.drawString("Level " + i, 10 + 50 * i, 120);//顯示地一行字彙
        	g.drawString(level[i] * 100 + "%", 10 + 50 * i, 135);//顯示第二行字彙
        	g.fillRect(10 + 50 * i, 10, 20, 100);//設定塗滿黑色的區塊
        	g.setColor(Color.red);//顯示下半部的顏色
        	g.fillRect(10 + 50 * i, 10 + (int)(100 * (1 - level[i])), 20, (int)(100 * level[i]));//設定塗滿紅色的色塊
        }
    }
	
	public void addGrade(int name, float number)
	{
		level[name] = number;
	}
}
