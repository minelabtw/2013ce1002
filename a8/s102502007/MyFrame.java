package ce1002.a8.s102502007;
import javax.swing.*;
public class MyFrame extends JFrame {
	//create a frame and set its properties
	public MyFrame(int[] count)
	{
		//transmit the parameter from MyFrame to MyPanel
		add(new MyPanel(count));
		setSize(500,500);
		setVisible(true);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("102502007");
		
	}

}
