package ce1002.a8.s102502007;
import java.util.Scanner;
public class A8 {

	Scanner input = new Scanner(System.in);
	/** create two arrays to store score and 
	 *  the number of the people in that range respectively
	 * */
	private static int score[] = new int[8];
	private static int count[] = new int[4];
	public static int i=0;
	/**constructor */
	public A8() {
	{
		//loop till the correct number is input
		System.out.println("Input grades: ");
		while(i<8)
		{
			score[i] = input.nextInt();
			if(score[i]<0 || score[i]>100)
				System.out.println("Out of range!");
			else
				i++;
		}
		//count the number of the people in that range
		for(i=0;i<8;i++)
		{
			if(score[i]>=0 && score[i]<=25)
				count[0]++;
			else if(score[i]>25 && score[i]<=50)
				count[1]++;
			else if(score[i]>50 && score[i]<=75)
				count[2]++;
			else
				count[3]++;
		}
		for(i=0;i<4;i++)
		{
			System.out.println("Level " + i + " is " + (float)count[i]/0.08 + "%");
		}
		//transmis the parameter from this class to MyFrame
		MyFrame frame = new MyFrame(count);
	}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new A8();

	}

}
