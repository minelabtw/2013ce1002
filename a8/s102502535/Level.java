package ce1002.a8.s102502535;

public class Level {

	private double l0;
	private double l1;
	private double l2;
	private double l3;

	// getter and setter of each level's percentage
	public void setLv0(double l0) {
		this.l0 = l0;
	}

	public void setLv1(double l1) {
		this.l1 = l1;
	}

	public void setLv2(double l2) {
		this.l2 = l2;
	}

	public void setLv3(double l3) {
		this.l3 = l3;
	}

	public double getLv0() {
		return this.l0;
	}

	public double getLv1() {
		return this.l1;
	}

	public double getLv2() {
		return this.l2;
	}

	public double getLv3() {
		return this.l3;
	}

}
