package ce1002.a8.s102502535;

import javax.swing.JFrame;

public class MyFrame extends JFrame {

	protected MyPanel level0 = new MyPanel(0);
	protected MyPanel level1 = new MyPanel(1);
	protected MyPanel level2 = new MyPanel(2);
	protected MyPanel level3 = new MyPanel(3);

	MyFrame() {
		setLayout(null);
		setBounds(300, 50, 450, 250);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPanel(level0, 10, 20);
		setPanel(level1, 100, 20);
		setPanel(level2, 200, 20);
		setPanel(level3, 300, 20);
	} // set frame's data

	public void setPanel(MyPanel panel, int x, int y) {
		panel.setBounds(x, y, 80, 200);
		setVisible(true);
		add(panel);
	} // set each panel's data and add it into panel

}
