package ce1002.a8.s102502535;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		Level lv = new Level();

		int[] grades = new int[8]; // declare an array

		System.out.println("Input grades: ");

		for (int i = 0; i < 8; i++) {
			grades[i] = input.nextInt();
			while (grades[i] < 0 || 100 < grades[i]) {
				System.out.println("Out of range!");
				grades[i] = input.nextInt();
			}
		} // input grades and save them into array

		input.close();

		double l0 = 0;
		double l1 = 0;
		double l2 = 0;
		double l3 = 0;

		for (int j = 0; j < 8; j++) {
			if (-1 < grades[j] && grades[j] < 26) {
				l0 = l0 + 1;
			} else if (25 < grades[j] && grades[j] < 51) {
				l1 = l1 + 1;
			} else if (50 < grades[j] && grades[j] < 76) {
				l2 = l2 + 1;
			} else if (75 < grades[j] && grades[j] < 101) {
				l3 = l3 + 1;
			}
		}

		double p1 = (l0 / 8) * 100;
		double p2 = (l1 / 8) * 100;
		double p3 = (l2 / 8) * 100;
		double p4 = (l3 / 8) * 100;

		lv.setLv0(p1);
		lv.setLv1(p2);
		lv.setLv2(p3);
		lv.setLv3(p4);

		System.out.println("Level 0 is " + p1 + "%");
		System.out.println("Level 1 is " + p2 + "%");
		System.out.println("Level 2 is " + p3 + "%");
		System.out.println("Level 3 is " + p4 + "%");

		MyFrame frame = new MyFrame();
	}
}
