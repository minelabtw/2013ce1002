package ce1002.a8.s102502535;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected JLabel level = new JLabel();
	protected JLabel percent = new JLabel();

	Level lv = new Level();

	private int i;
	private double l0;
	private double l1;
	private double l2;
	private double l3;

	MyPanel(int i) {
		this.i = i;
		setLayout(null);
		level.setBounds(20, 50, 200, 200);
		percent.setBounds(20, 70, 200, 200);
		add(level);
		add(percent);
	} // set panel's each data

	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (i == 0) {
			l0 = lv.getLv0();
			g.setColor(Color.red);
			g.fillRect(35, 20, 10, 100);
			g.setColor(Color.gray);
			g.fillRect(35, 20, 10, 100 - (int) l0);
			level.setText("Level 0");
			percent.setText(l0 + "%");
		} // set panel lv0 and output the graph
		if (i == 1) {
			l1 = lv.getLv1();
			g.setColor(Color.red);
			g.fillRect(35, 20, 10, 100);
			g.setColor(Color.gray);
			g.fillRect(35, 20, 10, 100 - (int) l1);
			level.setText("Level 1");
			percent.setText(l1 + "%");
		} // set panel lv1 and output the graph
		if (i == 2) {
			l2 = lv.getLv2();
			g.setColor(Color.red);
			g.fillRect(35, 20, 10, 100);
			g.setColor(Color.gray);
			g.fillRect(35, 20, 10, 100 - (int) l2);
			level.setText("Level 2");
			percent.setText(l2 + "%");
		} // set panel lv2 and output the graph
		if (i == 3) {
			l3 = lv.getLv3();
			g.setColor(Color.red);
			g.fillRect(35, 20, 10, 100);
			g.setColor(Color.gray);
			g.fillRect(35, 20, 10, 100 - (int) l3);
			level.setText("Level 3");
			percent.setText(l3 + "%");
		} // set panel lv3 and output the graph

	}

}
