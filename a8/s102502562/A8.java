package ce1002.a8.s102502562;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number;
		int[] level;
		double[] percent;
		level=new int [4];
		percent=new double [4];
		System.out.println("Input grades:");
		for(int i=1;i<=8;i++)//詢問8個數字,並判斷是否在範圍裡面,然後根據輸入的數字所在範圍計數
		{
			do
			{
				number=input.nextInt();
				if(number<0 || number>100)
				{
					System.out.println("Out of range!");
				}
				if(number<=25 && number>=0)
				{
					level[0]++;
				}
				else if(number<=50 && number>=26)
				{
					level[1]++;
				}
				else if(number<=75 && number>=51)
				{
					level[2]++;
				}
				else if(number<=100 && number>=76)
				{
					level[3]++;
				}
			}while(number<0 || number>100);
		}
		for(int i=0;i<4;i++)//計算所佔總數的百分比,並輸出
		{
			percent[i]=(double)level[i]/(double)8*(double)100;
			System.out.println("Level " + i + " is " + percent[i] + "%");
		}
		MyFrame frame = new MyFrame(percent);
	}
}
