package ce1002.a8.s102502562;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	JLabel jLabel1=new JLabel();
	JLabel jLabel2=new JLabel();
	int i;
	double percent;
	MyPanel(double percent,int i) {
		setLayout(null);
		this.percent=percent;
		this.i=i;
	}
	public void paintComponent(Graphics g)
	{
		g.setColor(Color.GRAY);//畫上面灰色部分
		g.fillRect(20,20,30,(int)(200-((2*this.percent))));
		g.setColor(Color.RED);//畫下面紅色部分
		g.fillRect(20,(int)(200-((2*this.percent))+20),30,(int)(2*(this.percent)));
		jLabel1.setText("Level " + i);//顯示等級和百分比
		jLabel2.setText(this.percent + "%");
		jLabel1.setBounds(20,220,50,50);
		jLabel2.setBounds(20,240,50,50);
		this.add(jLabel1);
		this.add(jLabel2);
	}

}
