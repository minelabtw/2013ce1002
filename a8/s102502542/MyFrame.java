package ce1002.a8.s102502542;

import javax.swing.*;

public class MyFrame extends JFrame
{
	
	MyFrame(double a[])
	{
		MyPanel panel = new MyPanel(a);//建構子將陣列傳入
		setSize(300,220);//設定frame大小
		//setLocation(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);//設定排版方向
		this.add(panel);
		setVisible(true);
	}

}
