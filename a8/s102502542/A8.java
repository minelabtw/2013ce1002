package ce1002.a8.s102502542;

import java.util.Scanner;

public class A8 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		double arr[]=new double[4];//陣列儲存分級次數
	    System.out.println("Input grades: ");
		for (int i = 0; i < 8; i++) 
		{
			int a = input.nextInt();
			while (a < 0 || a > 100) 
			{
				System.out.println("Out of range!");
				a = input.nextInt();
			}
			if(a>=0&&a<=25)
			{
			    arr[0]++;
			}
			else if(a>25&&a<=50)
			{
				arr[1]++;
			}
			else if(a>50&&a<=75)
			{
				arr[2]++;
			}
			else if(a>75&&a<=100)
			{
				arr[3]++;
			}
		}
		System.out.println("Level 0 is "+ arr[0]*100/8 + "%");
		System.out.println("Level 1 is "+ arr[1]*100/8 + "%");
		System.out.println("Level 2 is "+ arr[2]*100/8 + "%");
		System.out.println("Level 3 is "+ arr[3]*100/8 + "%");
		MyFrame frame = new MyFrame(arr);//建構子將陣列傳入
	}

}
