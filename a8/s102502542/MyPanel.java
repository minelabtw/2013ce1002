package ce1002.a8.s102502542;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.*;

public class MyPanel extends JPanel
{
	double L0;
	double L1;
	double L2;
	double L3;
	MyPanel(double a[])
	{
		 L0=a[0];
		 L1=a[1];
		 L2=a[2];
		 L3=a[3];
		JLabel label = new JLabel("Level 0");
		JLabel label1 = new JLabel("Level 1");
		JLabel label2 = new JLabel("Level 2");
		JLabel label3 = new JLabel("Level 3");
		JLabel label4 = new JLabel(""+L0*100/8+"%");
		JLabel label5 = new JLabel(""+L1*100/8+"%");
		JLabel label6 = new JLabel(""+L2*100/8+"%");
		JLabel label7 = new JLabel(""+L3*100/8+"%");
		setSize(250,150);//設定panel大小
		setLocation(10,10);//設定panel座標
		setLayout(null);
		label.setBounds(2,100,50,50);
		label1.setBounds(62,100,50,50);
		label2.setBounds(122,100,50,50);
		label3.setBounds(182,100,50,50);
		label4.setBounds(2,120,50,50);
		label5.setBounds(62,120,50,50);
		label6.setBounds(122,120,50,50);
		label7.setBounds(182,120,50,50);
		this.add(label);
		this.add(label1);
		this.add(label2);
		this.add(label3);
		this.add(label4);
		this.add(label5);
		this.add(label6);
		this.add(label7);
	}
	public void paintComponent(Graphics g)//繪圖函式
	{
		g.setColor(Color.RED);
		g.fillRect(2,5,12,105);
		g.fillRect(62,5,12,105);
		g.fillRect(122,5,12,105);
		g.fillRect(182,5,12,105);
		g.setColor(Color.GRAY);
		g.fillRect(2,5,12,(int)(105*(1-L0/8)));//將double轉成int
		g.fillRect(62,5,12,(int)(105*(1-L1/8)));
		g.fillRect(122,5,12,(int)(105*(1-L2/8)));
		g.fillRect(182,5,12,(int)(105*(1-L3/8)));
		setVisible(true);
	}

}
