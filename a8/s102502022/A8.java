package ce1002.a8.s102502022;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		double a=0;//宣告可以計算%數的變數
		double b=0;
		double c=0;
		double d=0;
		
		System.out.println("Input grades: ");
		for(int t=0;t<8;t++){//迴圈
			int grade = s.nextInt();//輸入
			while(grade<0 || grade>100){
				System.out.println("Out of Range!");
				System.out.println("Input grades: ");
				grade = s.nextInt();
			}
			if(grade>=0 && grade<=25)
				a++;
			else if(grade>25 && grade<=50)
				b++;
			else if(grade>50 && grade<=75)
				c++;
			else if(grade>75 && grade<=100)
				d++;
		}
		 
		double level0 = (a/8)*100;//算出百分比
		double level1 = (b/8)*100;
		double level2 = (c/8)*100;
		double level3 = (d/8)*100;
		
		System.out.println("Level 0 is "+level0+"%");
		System.out.println("Level 1 is "+level1+"%");
		System.out.println("Level 2 is "+level2+"%");
		System.out.println("Level 3 is "+level3+"%");
		
		MyFrame myframe = new MyFrame(level0 , level1 , level2 , level3);//呼叫myframe
	    
	}
	
	

}
