package ce1002.a8.s102502022;

import java.awt.*;

import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame(double level0,double level1, double level2, double level3){
		setSize(600,500);//建立框架大小
		setLocationRelativeTo(null);
		setLayout(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	    setVisible(true);//看得到!!
		construct(level0 , level1 , level2 , level3);//呼叫函式
	}
	
	public void construct(double level0,double level1, double level2, double level3){
		Panel1 panel1 = new Panel1(level0);//加入面板
		add(panel1);
		Panel2 panel2 = new Panel2(level1);
		add(panel2);
		Panel3 panel3 = new Panel3(level2);
		add(panel3);
		Panel4 panel4 = new Panel4(level3);
		add(panel4);
	}

	

}
