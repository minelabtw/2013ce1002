package ce1002.a8.s102502022;

import java.awt.*;

import javax.swing.*;

public class Panel3 extends JPanel{
	double temp=0;
	Panel3(double level2){
		temp= level2;
		setBounds(210,10,90,380);
		setLayout(null);
	}
	
	public void paintComponent(Graphics graph){
		graph.setColor(Color.GRAY);
		graph.fillRect(0, 0, 30, 340);
		graph.setColor(Color.RED);
		graph.fillRect(0, (int)(340-340*(temp/100)), 30, (int)(340*(temp/100)));
		JLabel jlabel5 = new JLabel();
		jlabel5.setBounds(0, 340, 90, 20);
		jlabel5.setText("Level 2");
		add(jlabel5);
		JLabel jlabel6 = new JLabel();
		jlabel6.setBounds(0, 360, 90, 20);
		jlabel6.setText(temp+"%");
		add(jlabel6);
	}


	

}
