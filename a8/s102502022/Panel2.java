package ce1002.a8.s102502022;

import java.awt.*;

import javax.swing.*;

public class Panel2 extends JPanel{
	double temp=0;//宣告暫時變數
	Panel2(double level1){
		temp= level1;//傳入level1給暫時變數
		setBounds(110,10,90,380);//建立大小
		setLayout(null);
	}
	
	public void paintComponent(Graphics graph){
		graph.setColor(Color.GRAY);//塗上顏色灰
		graph.fillRect(0, 0, 30, 340);//塗滿框框
		graph.setColor(Color.RED);
		graph.fillRect(0, (int)(340-340*(temp/100)), 30, (int)(340*(temp/100)));
		JLabel jlabel3 = new JLabel();
		jlabel3.setBounds(0, 340, 90, 20);
		jlabel3.setText("Level 1");//寫上文字
		add(jlabel3);//加入jlabel
		JLabel jlabel4 = new JLabel();
		jlabel4.setBounds(0, 360, 90, 20);
		jlabel4.setText(temp+"%");
		add(jlabel4);
	}

	

}
