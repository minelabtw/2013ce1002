package ce1002.a8.s102502022;

import java.awt.*;

import javax.swing.*;

public class Panel4 extends JPanel{
	double temp=0;
	Panel4(double level3){
		temp = level3;
		setBounds(310,10,90,380);
		setLayout(null);
	}
	
	public void paintComponent(Graphics graph){
		graph.setColor(Color.GRAY);
		graph.fillRect(0, 0, 30, 340);
		graph.setColor(Color.RED);
		graph.fillRect(0, (int)(340-340*(temp/100)), 30, (int)(340*(temp/100)));
		JLabel jlabel7 = new JLabel();
		jlabel7.setBounds(0, 340, 90, 20);
		jlabel7.setText("Level 3");
		add(jlabel7);
		JLabel jlabel8 = new JLabel();
		jlabel8.setBounds(0, 360, 90, 20);
		jlabel8.setText(temp+"%");
		add(jlabel8);
	}


	

}
