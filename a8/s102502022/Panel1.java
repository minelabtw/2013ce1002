package ce1002.a8.s102502022;

import java.awt.*;

import javax.swing.*;

public class Panel1 extends JPanel{
	double temp=0;//宣告暫時變數
	Panel1(double level0){
		temp= level0;//傳入L0的值
		setBounds(10,10,90,380);//建立大小
		setLayout(null);
	}
	
	public void paintComponent(Graphics graph){
		graph.setColor(Color.GRAY);//塗上顏色
		graph.fillRect(0, 0, 30, 340);//建立大小
		graph.setColor(Color.RED);
		graph.fillRect(0, (int)(340-340*(temp/100)), 30, (int)(340*(temp/100)));
		JLabel jlabel1 = new JLabel();
		jlabel1.setBounds(0, 340, 90, 20);//建立大小
		jlabel1.setText("Level 0");//寫上文字
		add(jlabel1);//加入面板
		JLabel jlabel2 = new JLabel();
		jlabel2.setBounds(0, 360, 90, 20);
		jlabel2.setText(temp+"%");
		add(jlabel2);
	}

}
