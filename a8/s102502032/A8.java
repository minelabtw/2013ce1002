package ce1002.a8.s102502032;

import java.util.Scanner;

public class A8
{
	public static void main(String[] args)
	{
		int grade[] = new int[8];
		double levelPercent[] = new double[4];
		Scanner jin = new Scanner(System.in);
		System.out.println("Input grades: ");
		for (int i = 0; i < grade.length; i ++)
		{
			grade[i] = getSize(0, 100, jin);
		}
		levelPercent = addUp(grade);
		MyFrame frame = new MyFrame(levelPercent);
		printResult(levelPercent);
		jin.close();
	}

	// print in console
	public static void printResult(double percent[])
	{
		for (int i = 0; i < percent.length; i ++)
		{
			System.out.println("Level " + i + " is " + percent[i] + "%");
		}
	}

	// add up each level (in perceent)
	public static double[] addUp(int grade[])
	{
		double level[] = new double[4];
		for (int i = 0; i < grade.length; i ++)
		{
			if (grade[i] < 24)
			{
				level[0] += 12.5;
			}
			else if (grade[i] >= 24 && grade[i] < 50)
			{
				level[1] += 12.5;
			}
			else if (grade[i] >= 50 && grade[i] < 75)
			{
				level[2] += 12.5;
			}
			else
			{
				level[3] += 12.5;
			}
		}
		return level;
	}

	// get size;
	public static int getSize(int min, int Max, Scanner jin)
	{
		if (min > Max)
		{
			System.out.print("getSize: size error (min > Max)");
			return -1;
		}
		int size = 0;
		do
		{
			size = jin.nextInt();
			if (size < min || size > Max)
				System.out.println("Out of range!");
		}
		while (size < min || size > Max);
		return size;
	}
}
