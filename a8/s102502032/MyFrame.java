package ce1002.a8.s102502032;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private GraphLabel			graphLabel[]		= new GraphLabel[4];
	private JLabel				levelLabel[]		= new JLabel[4];
	private JLabel				percentlLabel[]		= new JLabel[4];

	// constructor
	public MyFrame(double percent[])
	{
		this.setLayout(new GridLayout(3, 4));
		for (int i = 0; i < 4; i ++)
		{
			graphLabel[i] = new GraphLabel(percent[i]);
			this.add(graphLabel[i]);
		}
		for (int i = 0; i < 4; i ++)
		{
			levelLabel[i] = new JLabel();
			this.levelLabel[i].setText("Level " + i);
			this.add(levelLabel[i]);
		}
		for (int i = 0; i < 4; i ++)
		{
			percentlLabel[i] = new JLabel();
			this.percentlLabel[i].setText(percent[i] + "%");
			this.add(percentlLabel[i]);
		}
		this.setSize(220, 250);
		this.setVisible(true);
	}
	// setter
	// getter
	// methods
}
