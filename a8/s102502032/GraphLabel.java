package ce1002.a8.s102502032;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;

public class GraphLabel extends JLabel
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private double				percent				= 0;

	public GraphLabel(double number)
	{
		this.percent = number;
	}

	// setter
	// getter
	// methods
	// graph the form
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.black);
		g.fillRect((int) (30 / 2 - 30 * 0.1), (int) (50 / 2 - 50 * 0.1),
				(int) (30 * 0.2), (int) (50 * 0.8 * (100 - percent) / 100));
		g.setColor(Color.red);
		g.fillRect((int) (30 / 2 - 30 * 0.1), (int) (50 / 2 - 50 * 0.1)
				+ (int) (50 * 0.8 * (100 - percent) / 100), (int) (30 * 0.2),
				(int) (50 * 0.8 * percent / 100));
		this.repaint();
	}
}
