package ce1002.a8.s102502002;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class MyPanel extends JPanel {

	protected A8 a = new A8();	
	
	MyPanel(){
		//set bounds and visibility of the panel
		setBounds(0,0,450,300);
		setVisible(true); 
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.RED); //draw four red rectangles in the bottom
		g.fillRect( 100, 60, 15, 200);
		g.fillRect( 200, 60, 15, 200);
		g.fillRect( 300, 60, 15, 200);
		g.fillRect( 400, 60, 15, 200);

		g.setColor(Color.lightGray); //cover the four rectangles with gray rectangles of specific length 
		g.fillRect( 100, 60, 15, (int)(200*(1-a.a)));
		g.fillRect( 200, 60, 15, (int)(200*(1-a.b)));
		g.fillRect( 300, 60, 15, (int)(200*(1-a.c)));
		g.fillRect( 400, 60, 15, (int)(200*(1-a.d)));
		
		g.setColor(Color.black); //print the results of each level
		g.drawString("Level 0",  90, 280);
		g.drawString("Level 1", 190, 280);
		g.drawString("Level 2", 290, 280);
		g.drawString("Level 3", 390, 280);
		
		g.drawString(""+a.a*100+"%",  90, 300);
		g.drawString(""+a.b*100+"%", 190, 300);
		g.drawString(""+a.c*100+"%", 290, 300);
		g.drawString(""+a.d*100+"%", 390, 300);
	}
}
