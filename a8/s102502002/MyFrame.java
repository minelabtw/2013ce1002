package ce1002.a8.s102502002;
import javax.swing.JFrame;

public class MyFrame extends JFrame {
	
	protected MyPanel panel = new MyPanel(); //create a panel object
	MyFrame(){
		//set bounds, layout and visibility of the frame
		setBounds(300,300,600,400);
		setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(panel); //add panel to frame
	}
}
