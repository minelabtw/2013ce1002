package ce1002.a8.s102502002;
import java.util.Scanner;

public class A8 {

	public static float a=0;
	public static float b=0;
	public static float c=0;
	public static float d=0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		A8 a = new A8();
		Scanner input = new Scanner(System.in);
		int [] gd = new int[8];
		float le0=0;
		float le1=0;
		float le2=0;
		float le3=0;
		
		//determine if input is out of range, and save it into an array
		for(int i=0;i<8;i++){
			System.out.print("Input grades: ");
			gd[i]=input.nextInt();
			while(gd[i]<0||gd[i]>100){
				System.out.println("Out of range!");
				System.out.print("Input grades: ");
				gd[i]=input.nextInt();
			}
		}
		input.close(); //close scanner
		for(int i=0;i<8;i++){ //count the amount of each level
			if(gd[i]<26)
				le0++;
			else if(gd[i]>=26&&gd[i]<51)
				le1++;
			else if(gd[i]>=51&&gd[i]<76)
				le2++;
			else
				le3++;
		}
		//save to public static variable
		a.a=le0/8;
		a.b=le1/8;
		a.c=le2/8;
		a.d=le3/8;
		//output result
		System.out.println("Level 0 is "+le0*100/8+"%");
		System.out.println("Level 1 is "+le1*100/8+"%");
		System.out.println("Level 2 is "+le2*100/8+"%");
		System.out.println("Level 3 is "+le3*100/8+"%");
		MyFrame frame = new MyFrame(); //create frame
	}

}
