package ce1002.a8.s102502505;

import java.util.Scanner;
import javax.swing.*;

public class A8 {

	private static Scanner input;

	public static void main(String[] args) {
		input = new Scanner(System.in);
		int[] gradelist = new int[8];//設定成績矩陣
		int[] levellist = new int[4];//設定等級矩陣
		int currentgrade;//設定輸入值
		int level0=0;//設定等級人數
		int level1=0;
		int level2=0;
		int level3=0;
		
		System.out.println("Input grades: ");//輸出字串
		for(int i=0;i<8;i++)//存入成績陣列中
		{
			currentgrade=input.nextInt();
			while(currentgrade<0||currentgrade>100)//設定範圍
			{
				System.out.println("Out of range!");
				currentgrade=input.nextInt();
			}
			gradelist[i]=currentgrade;//存入陣列
		}
		
		for (int j=0;j<8;j++)//計算每等級的人數
		{
			if(gradelist[j]<=25)
			{
				level0++;
			}	
			else if(gradelist[j]<=50)
			{
				level1++;
			}	
			else if(gradelist[j]<=75)
			{
				level2++;
			}	
			else
			{
				level3++;
			}	
		}
		System.out.println("Level 0 is "+level0*12.5+"%");//輸出結果
		System.out.println("Level 1 is "+level1*12.5+"%");
		System.out.println("Level 2 is "+level2*12.5+"%");
		System.out.println("Level 3 is "+level3*12.5+"%");
		
		levellist[0]=level0;//存人數至陣列中
		levellist[1]=level1;
		levellist[2]=level2;
		levellist[3]=level3;
	
		MainFrame frame = new MainFrame(levellist);//新增mainframe物件
		
		frame.setSize(400,250);//設定frame
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		

	}

}
