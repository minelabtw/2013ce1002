package ce1002.a8.s102502505;

import javax.swing.*;

import java.awt.*;

public class MainPanel extends JPanel{
	
	int l0,l1,l2,l3;
	
	public MainPanel(int[] levellist)//將傳入的陣列值存入l0,l1,l2,l3
	{
		l0=levellist[0];
		l1=levellist[1];
		l2=levellist[2];
		l3=levellist[3];
	}
	
	public void paintComponent(Graphics g)//繪圖
	{
		super.paintComponent(g);
		
		g.setColor(Color.BLACK);//先畫背景黑色
		g.fillRect(25, 25, 25, 80);
		g.fillRect(125, 25, 25, 80);
		g.fillRect(225, 25, 25, 80);
		g.fillRect(325, 25, 25, 80);
		
		g.setColor(Color.RED);//再畫長條圖
		g.fillRect(25, 105-l0*10, 25, l0*10);
		g.fillRect(125, 105-l1*10, 25, l1*10);
		g.fillRect(225, 105-l2*10, 25, l2*10);
		g.fillRect(325, 105-l3*10, 25, l3*10);
		
		g.setColor(Color.GRAY);//輸出結果字串
		g.drawString("Level0",25,130);
		g.drawString(""+(l0*12.5), 25, 142);
		g.drawString("Level1",125,130);
		g.drawString(""+(l1*12.5), 125, 142);
		g.drawString("Level2",225,130);
		g.drawString(""+(l2*12.5), 225, 142);
		g.drawString("Level3",325,130);
		g.drawString(""+(l3*12.5), 325, 142);
		
	}
	
	
}
