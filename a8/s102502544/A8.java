package ce1002.a8.s102502544;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		int num=0;  //宣告變數
		int grade;
		float l0=0,l1=0,l2=0,l3=0; //宣告浮點數
		
		System.out.println("Input grades: "); //輸出
		do{
			grade=input.nextInt(); //輸入
			if(grade<0 || grade>100){ //設定不符合範圍
				System.out.println("Out of range!");
			}
			else{ //記錄等級
				num++;
				if(grade>=0 && grade<=25){
					l0++;
				}
				if(grade>=26 && grade<=50){
					l1++;
				}
				if(grade>=51 && grade<=75){
					l2++;
				}
				if(grade>=76 && grade<=100){
					l3++;
				}
			}
		}while((grade<0 || grade>100) || num<8);
		
		System.out.println("Level 0 is "+(l0/8)*100+"%");
		System.out.println("Level 1 is "+(l1/8)*100+"%");
		System.out.println("Level 2 is "+(l2/8)*100+"%");
		System.out.println("Level 3 is "+(l3/8)*100+"%");
		MyFrame frame=new MyFrame(l0,l1,l2,l3); 
		
	}
}
