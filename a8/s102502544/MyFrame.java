package ce1002.a8.s102502544;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame(float l0 , float l1 , float l2 , float l3){
		int arr[]={0,1,2,3}; //設定陣列
		MyPanel p1=new MyPanel(l0,arr[0]);
		MyPanel p2=new MyPanel(l1,arr[1]);
		MyPanel p3=new MyPanel(l2,arr[2]);
		MyPanel p4=new MyPanel(l3,arr[3]);
		
		setLayout(null); //設定排版
		setBounds(0,0,500,500); //設定邊界
		
		p1.setBounds(0,0,70,160); //設定每個panel邊界
		p2.setBounds(70,0,70,160);
		p3.setBounds(140,0,70,160);
		p4.setBounds(210,0,70,160);
		
		add(p1); //加入panel到frame
		add(p2);
		add(p3);
		add(p4);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
}
