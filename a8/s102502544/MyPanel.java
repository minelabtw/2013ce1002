package ce1002.a8.s102502544;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	protected float i; //宣告浮點數
	MyPanel(float l,int a){
		setLayout(null); //設定排版
		i=l;
		
		JLabel l1=new JLabel("Level "+a);
		l1.setBounds(10,105,50,30);
		JLabel l2=new JLabel((l/8)*100+"%");
		l2.setBounds(10,125,50,30);
		add(l1); //加入label到panel
		add(l2);
		
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.GRAY); //上底色
		g.fillRect(10,10,10,100); //畫圖形
		
		g.setColor(Color.RED); //覆蓋上色
		g.fillRect(10,(int)(110-(i/8)*100),10,(int)((i/8)*100)); //畫圖形
		
	}
}
