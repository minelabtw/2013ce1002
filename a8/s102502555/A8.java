package ce1002.a8.s102502555;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		double[] gradeLevel = new double[4];
		int grade;
		Scanner input = new Scanner(System.in);
		MyFrame myframe;
		
		//輸入數字
		System.out.println("Input grades:");
		for(int i = 0 ; i < 8 ; i++){	
			do{
				grade = input.nextInt();
				if(grade < 0 || grade > 100){
					System.out.println("Out of range!");
				}
			}while(grade < 0 || grade > 100);
			
			if(grade >= 0 && grade <= 25){
				gradeLevel[0]++;
			}else if(grade >= 26 && grade <= 50){
				gradeLevel[1]++;
			}else if(grade >= 51 && grade <= 75){
				gradeLevel[2]++;
			}else if(grade >= 76 && grade <= 100){
				gradeLevel[3]++;
			}
		}
		
		//印出數字
		for(int i = 0 ; i < 4 ; i++){
			System.out.println("Level " + i + " is " + (gradeLevel[i] / 8) * 100 + "%");
		}
		
		//印出表格
		myframe = new MyFrame(gradeLevel);
		
	}

}
