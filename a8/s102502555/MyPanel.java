package ce1002.a8.s102502555;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	JLabel level;
	JLabel lPercent;
	FigurePanel fp;
	
	MyPanel(int level , double percent){
		setSize(50 , 300);
		setLayout(null);
		
		//設定長條圖
		fp = new FigurePanel();
		fp.setPercent(percent);
		fp.setBounds(10, 0, 15, 100);
		
		//設定底下的字
		this.level = new JLabel("Level " + level);
		this.level.setBounds(10 , 110 , 40 , 20);
		
		lPercent = new JLabel(percent + "%");
		lPercent.setBounds(10 , 130 , 40 , 20);
		
		add(fp);
		add(this.level);
		add(lPercent);
	}
	
}
