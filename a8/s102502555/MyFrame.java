package ce1002.a8.s102502555;

import javax.swing.JFrame;
import java.awt.GridLayout;

public class MyFrame extends JFrame{	
	MyPanel p1;
	MyPanel p2;
	MyPanel p3;
	MyPanel p4;
	
	MyFrame(double[] grade){
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true); 
		setSize(500 , 300);
		setLayout(new GridLayout(1 , 4));
		
		p1 = new MyPanel(0 , (grade[0] / 8) * 100);
		p2 = new MyPanel(1 , (grade[1] / 8) * 100);
		p3 = new MyPanel(2 , (grade[2] / 8) * 100);
		p4 = new MyPanel(3 , (grade[3] / 8) * 100);
		
		//把panel加上去
		add(p1);
		add(p2);
		add(p3);
		add(p4);
	}
	
}
