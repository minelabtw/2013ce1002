package ce1002.a8.s102502555;

import javax.swing.*;

import java.awt.*;

public class FigurePanel extends JPanel{
	double percent;
	
	FigurePanel(){
		setLayout(null);
	}
	
	public void setPercent(double percent){
		this.percent = percent;
	}
	
	//劃出長條圖
	protected void paintComponent(Graphics g){
		g.setColor(Color.GRAY);
		g.fillRect(0, 0, 10, 100);
		g.setColor(Color.RED);
		g.fillRect(0, 100 - (int)(percent), 10, (int)(percent));
	}
}
