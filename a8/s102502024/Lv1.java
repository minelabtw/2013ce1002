package ce1002.a8.s102502024;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Graphics;
public class Lv1 extends JPanel{
	protected int i=0;
	protected JLabel label=new JLabel("Level 1");
	protected JLabel label1=new JLabel();
	Lv1(int s)
	{
		setLayout(null);  //不用預設排版
		i=s;  //存次數的值
		label.setBounds(50, 320, 120, 30);  //設定label大小與位置
		label1.setText(i*12.5+"%");
		label1.setBounds(50, 350, 100, 30);
		add(label);  //將label加到panel上
		add(label1);
	}
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.gray);  //畫長條圖
		g.fillRect(50,50 , 40, 100);
		g.setColor(Color.red);
		g.fillRect(50,150-i*10 , 40, i*10);
	}
}
