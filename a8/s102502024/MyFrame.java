package ce1002.a8.s102502024;
import javax.swing.JFrame;
import javax.swing.JLabel;
public class MyFrame extends JFrame{
	MyFrame(int[] s)
	{
		setSize(500,500);  //設定視窗大小與位置
		setLocationRelativeTo(null);  //使視窗置中
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //設定關閉鍵
		setLayout(null);  //不用預設排版
		Lv0 lv0=new Lv0(s[0]);  //產生panel
		Lv1 lv1=new Lv1(s[1]);
		Lv2 lv2=new Lv2(s[2]);
		Lv3 lv3=new Lv3(s[3]);
		lv0.setBounds(0,0,120,500);  //設定panel大小與位置
		lv1.setBounds(120,0,120,500);
		lv2.setBounds(240,0,120,500);
		lv3.setBounds(360,0,120,500);
		add(lv0);  //將panel加到視窗上
		add(lv1);
		add(lv2);
		add(lv3);
	}
}
