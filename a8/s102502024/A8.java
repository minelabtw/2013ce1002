package ce1002.a8.s102502024;
import java.util.Scanner;
public class A8 {
	public static void main(String[] args) {
		int grade[]=new int[8];  //成績陣列
		int s[]=new int[4];  //次數陣列
		System.out.println("Input grades:");
		Scanner scn=new Scanner(System.in);
		for(int i=0;i<8;i++)
		{
			int num=scn.nextInt();
			while(num>100 || num<0)  //判斷是否符合條件
			{
				System.out.println("Out of range!");
				num=scn.nextInt();
			}
			grade[i]=num;  //存入陣列
			if(num<=25 && num>=0)
			{
				s[0]++;
			}
			if(num<=50 && num>=26)
			{
				s[1]++;
			}
			if(num<=75 && num>=51)
			{
				s[2]++;
			}
			if(num<=100 && num>=76)
			{
				s[3]++;
			}
		}
		System.out.println("Level 0 is "+s[0]*12.5+"%");
		System.out.println("Level 1 is "+s[1]*12.5+"%");
		System.out.println("Level 2 is "+s[2]*12.5+"%");
		System.out.println("Level 3 is "+s[3]*12.5+"%");
		MyFrame frm=new MyFrame(s);  //產生視窗
		frm.setVisible(true);
	}
}
