package ce1002.a8.s102502531;

import java.util.Scanner;
import javax.swing.*;

public class A8 {

	public static void main(String[] args) { //這�堿O輸入學生的分數
		int[] studentsgrads = new int[8];
		double level_0 = 0, level_1 = 0, level_2 = 0, level_3 = 0;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Input grades: ");
		for (int i = 0; i < 8; i++) {
			do {
				studentsgrads[i] = scanner.nextInt();
				if (studentsgrads[i] < 0 || studentsgrads[i] > 100) {
					System.out.println("Out of range!");
				}
			} while (0 > studentsgrads[i] || studentsgrads[i] > 100);
			if (0 <= studentsgrads[i] && studentsgrads[i] <= 25) {
				level_0 = level_0 + 12.5;
			}
			if (26 <= studentsgrads[i] && studentsgrads[i] <= 50) {
				level_1 = level_1 + 12.5;
			}
			if (51 <= studentsgrads[i] && studentsgrads[i] <= 75) {
				level_2 = level_2 + 12.5;
			}
			if (76 <= studentsgrads[i] && studentsgrads[i] <= 100) {
				level_3 = level_3 + 12.5;
			}
		}
		System.out.println("Level 0 is "+level_0+"%"); //輸出學生的分數
		System.out.println("Level 1 is "+level_1+"%");
		System.out.println("Level 2 is "+level_2+"%");
		System.out.println("Level 3 is "+level_3+"%");
		Myframe myframe = new Myframe(level_0,level_1,level_2,level_3);
		
	}
	
}
