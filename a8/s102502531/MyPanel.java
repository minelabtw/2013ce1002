package ce1002.a8.s102502531;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
public class MyPanel extends JPanel{
	double level_0,level_1,level_2,level_3;	
	public MyPanel(double level_0,double level_1,double level_2,double level_3) {
		// TODO Auto-generated constructor stub
		setBounds(0, 0, 250, 150);
		this.level_0=level_0;
		this.level_1=level_1;
		this.level_2=level_2;
		this.level_3=level_3;
	}
	protected void paintComponent(Graphics g) {  //�e��
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(5, 10, 10,(int) (100-level_0));
		g.setColor(Color.black);
		g.drawString("Level 0",5,130);
		g.drawString(""+level_0+"%",5,145);
		g.setColor(Color.RED);
		g.fillRect(5, 10+(int) (100-level_0), 10,(int) (level_0));
		g.setColor(Color.GRAY);
		g.fillRect(55, 10, 10,(int) (100-level_1));
		g.setColor(Color.black);
		g.drawString("Level 1",55,130);
		g.drawString(""+level_1+"%",55,145);
		g.setColor(Color.RED);
		g.fillRect(55, 10+(int) (100-level_1), 10,(int) (level_1));
		g.setColor(Color.GRAY);
		g.fillRect(105, 10, 10,(int) (100-level_2));
		g.setColor(Color.black);
		g.drawString("Level 2",105,130);
		g.drawString(""+level_2+"%",105,145);
		g.setColor(Color.RED);
		g.fillRect(105, 10+(int) (100-level_2), 10,(int) (level_2));
		g.setColor(Color.GRAY);
		g.fillRect(155, 10, 10,(int) (100-level_3));
		g.setColor(Color.black);
		g.drawString("Level 3",155,130);
		g.drawString(""+level_3+"%",155,145);
		g.setColor(Color.RED);
		g.fillRect(155, 10+(int) (100-level_3), 10,(int) (level_3));
		}

}
