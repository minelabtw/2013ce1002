package ce1002.a8.s102502017;

import javax.swing.*;
import java.awt.*;
import java.util.Scanner;

public class MyFrame extends JFrame{
	
	double[] level = new double[4];

	MyFrame(){
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setLayout(null);
		
		docalc();
		setPanel();
		setDraw();
		
		
		
		this.setSize(416,236);
		setVisible(true);
	}
	
	
	//do the calc.s
	void docalc(){
		Scanner input = new Scanner(System.in);
		double[] score = new double[8];
		
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
			while(true){
				double temp;
				temp = input.nextDouble();
				if(temp >=0 && temp<=100){
					score[i] = temp;
					if(temp <25)level[0]++;
					else if(temp<50)level[1]++;
					else if(temp<75)level[2]++;
					else level[3]++;
					break;
				}
				else System.out.println("Out of range!");
			}
		}
		
		for(int i=0;i<4;i++){
			System.out.println("Level " + i + " is " + level[i]/8*100 + "%");
		}
		
	}
	
	//addPicture
	void setDraw(){
		
		JPanel panel;
		panel  = new Draw(0,0,(int)level[0]);
		add(panel);
		panel  = new Draw(100,0,(int)level[1]);
		add(panel);
		panel  = new Draw(200,0,(int)level[2]);
		add(panel);
		panel  = new Draw(300,0,(int)level[3]);
		add(panel);
		
	}
	//addTexts
	void setPanel(){
		
		MyPanel panel;
		panel  = new MyPanel(0,100,(int)level[0],1);
		add(panel);
		panel  = new MyPanel(100,100,(int)level[1],2);
		add(panel);
		panel  = new MyPanel(200,100,(int)level[2],3);
		add(panel);
		panel  = new MyPanel(300,100,(int)level[3],4);
		add(panel);
		
	}
	
}
