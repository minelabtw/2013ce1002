package ce1002.a8.s102502017;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
	
	JLabel level;
	JLabel percent;
	int levels;
	
	MyPanel(int x,int y,int levels,int i){
		
		this.levels = levels;
		
		
		setBounds(x,y,100,200);
		
		setLabel(i);
		//These can't be used
		//if use setLayout(null) 
		//the labels will be disappear
		level.	setLocation(x,y+10);
		percent.setLocation(x,y+30);
		
		add(level);
		add(percent);
		
	}
	
	//setTheLabel
	void setLabel(int i){
		
		level = new JLabel();
		level.setText("Level " + i);
		
		percent = new JLabel();
		percent.setText((double)levels/8*100 + "%");
		
	}

	
}
