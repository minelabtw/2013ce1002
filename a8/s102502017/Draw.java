package ce1002.a8.s102502017;

import javax.swing.*;

import java.awt.*;

public class Draw extends JPanel{
	
	int levels;
	
	Draw(int x,int y,int levels){
		
		
		setBounds(x,y,100,100);
		this.levels = levels;
		
	}
	
	//DrawPictures
	public void paint(Graphics g){
		g.setColor(Color.RED);
		g.fillRect(45,20,10,80);
		g.setColor(Color.GRAY);
		g.fillRect(45, 20, 10, 10*(8-levels));
	}

}
