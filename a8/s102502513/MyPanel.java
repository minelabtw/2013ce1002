package ce1002.a8.s102502513;

import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel {
	public int count[] = new int[4];
	//constructor
	public MyPanel(int[] count)
	{
        //放置分級與百分比
		JLabel[] label = new JLabel[8];		
		setLayout(null);
		setLayout(new GridLayout(2,4,-80,-380));
		
		//加入 "Level" label
		for(int i=0;i<4;i++)
		{
			label[i] = new JLabel("Level " + i);
			add(label[i]);
		}
		
		//加入 "percentage" label
		for(int i=4;i<8;i++)
		{
			label[i] = new JLabel((float)count[i-4]/0.08 + "%");
			add(label[i]);
		}
	
		for(int i=0;i<4;i++)
		{
			this.count[i]=count[i];
		}
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fill3DRect(0, 20, 30, 160, true);
		g.fill3DRect(100, 20, 30, 160, true);
		g.fill3DRect(200, 20, 30, 160, true);
		g.fill3DRect(300, 20, 30, 160, true);
	    //先置入全紅的長方形
		g.setColor(Color.GRAY);
		g.fill3DRect(0, 20, 30, 160-160*count[0]/8, true);
		g.fill3DRect(100, 20, 30, 160-160*count[1]/8, true);
		g.fill3DRect(200, 20, 30, 160-160*count[2]/8, true);
		g.fill3DRect(300, 20, 30, 160-160*count[3]/8, true);
		//用灰色去配置所對應之百分比
	}
}
