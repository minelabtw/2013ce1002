package ce1002.a8.s102502513;

import javax.swing.*;

public class MyFrame extends JFrame {
	public MyFrame(int[] count)
	{
		add(new MyPanel(count));
		//設定視窗的屬性
		setSize(500,500);
		setDefaultCloseOperation(MyFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setTitle("102502513");
		setVisible(true);
	}
}

