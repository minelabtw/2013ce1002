package ce1002.a8.s102502534;

import java.awt.*;

import javax.swing.*;

public class MyFrame extends JFrame{
	MyFrame(double[] level_arr)
	{
		setLayout(null);
		setBounds(0, 0, 500, 300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//new 4 個 panel
		MyPanel p0 = new MyPanel(0 , (level_arr[0] / 8) * 100);
		MyPanel p1 = new MyPanel(1 , (level_arr[1] / 8) * 100);
		MyPanel p2 = new MyPanel(2 , (level_arr[2] / 8) * 100);
		MyPanel p3 = new MyPanel(3 , (level_arr[3] / 8) * 100);
		//設定 4 個panel的大小跟位置
		p0.setBounds(0, 10, 50, 300);
		p1.setBounds(60, 10, 50, 300);
		p2.setBounds(120, 10, 50, 300);
		p3.setBounds(180, 10, 50, 300);
		//分別將4個panel加到MyFrame中
		add(p0);
		add(p1);
		add(p2);
		add(p3);
	}
}
