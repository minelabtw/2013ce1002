package ce1002.a8.s102502534;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel {

	// 宣告2個label
	JLabel level = new JLabel();
	JLabel level_percent = new JLabel();

	// 因為畫圖還需要取得percent這歌值所以要另外在這個panel中宣告一個新的變數
	double percent = 0;

	MyPanel(int level, double percent) {
		setLayout(null);

		this.percent = percent;

		this.level = new JLabel("Level " + level);
		this.level.setBounds(10, 110, 40, 20);

		this.level_percent = new JLabel(percent + "%");
		this.level_percent.setBounds(10, 130, 40, 20);

		add(this.level);
		add(this.level_percent);
	}

	// 劃出長條圖
	protected void paintComponent(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(10, 0, 30, 100);
		g.setColor(Color.RED);
		g.fillRect(10, 100 - (int) (percent), 30, (int) (percent));
	}
}
