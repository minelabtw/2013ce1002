package ce1002.a8.s102502534;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int grade = 0;
		double[] level_arr = new double[4];//用陣列來存各個等級有幾個人
		System.out.println("Input grades:");

		for (int i = 0; i < 8; i++) {
			//此do while 迴圈是當輸入的值不在正確範圍之內可以再輸入一次
			do{
				grade = input.nextInt();
				if(grade < 0 || grade > 100){
					System.out.println("Out of range!");
				}
			}while(grade < 0 || grade > 100);
			
			if (grade >= 0 && grade <= 25) {
				level_arr[0]++;
			} else if (grade >= 26 && grade <= 50) {
				level_arr[1]++;
			} else if (grade >= 51 && grade <= 75) {
				level_arr[2]++;
			} else if (grade >= 76 && grade <= 100) {
				level_arr[3]++;
			}
	}
		//印出結果
		for (int i = 0; i < 4; i++) {
			System.out.println("Level " + i + " is " + (level_arr[i] / 8) * 100 + "%");
		}
		input.close();
		MyFrame mf = new MyFrame(level_arr);
	}

}
