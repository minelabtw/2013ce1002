package ce1002.a8.s102502515;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyFrame extends JFrame{
	MyFrame(int levels[])//constructor
	{	
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(300 , 50 , 500,300);//frame的位置
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		MyPanel panel = new MyPanel(levels);//建立panel，傳入levels陣列
		add(panel);//加入panel
	}
}
