package ce1002.a8.s102502515;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
	protected int levels[];
	MyPanel(int []levels){
		this.levels = levels;//set levels
		setBounds(0,0,300,200);//set size and location
	}

	public void paint(Graphics g)//draw method
	{
		super.paint(g);
		int x = 20;//x coordinate
		
		for (int i = 0; i < 4 ; i++){//four Rec for four times
		g.setColor(Color.gray);
		g.fillRect(x, 20, 15, 125);//底長方形
		g.setColor(Color.red);//%數
		g.fillRect(x, 145-125*levels[i]/8, 15, 125*levels[i]/8);
		g.setColor(Color.black);
		g.drawString("Level "+ i, x, 165);//set字
		g.drawString(""+(double)levels[i]*100/8+"%" , x,195);
		x = x + 75;//x coordinate往右
		}
		}
}
