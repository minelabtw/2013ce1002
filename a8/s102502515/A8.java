package ce1002.a8.s102502515;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Scanner;
public class A8 {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);//建立input
		int []grades = new int [8];//存分數的陣列
		int []levels = new int [4];//計算次數的陣列
		
		System.out.println("Input grades: ");
		for (int i = 0 ; i < 8 ; i++)//藉由迴圈讓使用者輸入分數
		{
			grades[i] = input.nextInt();
				while ( grades[i] < 0 || grades[i] > 100)//利用while確定分數在範圍內
				{
					System.out.println("Out of range!");
					grades[i] = input.nextInt();
				}
				
			if(grades[i] >= 0 && grades[i] < 25)//if來計算四階段各自的次數
			{
				levels[0]++;
			}
			else if (grades[i] >= 25 && grades[i] < 50)
			{
				levels[1]++;
			}
			else if (grades[i] >= 50 && grades[i] < 75)
			{
				levels[2]++;
			}
			else
			{
				levels[3]++;
			}
		}
		for (int i = 0 ; i < 4 ; i++)
		{
			System.out.println("Level "+ i +" is " + (double)levels[i]*100/8 + "%");
		}

		MyFrame frame = new MyFrame(levels);
		input.close();//關掉input
	}


}
