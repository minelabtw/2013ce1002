package ce1002.a8.s102502526;
import java.awt.*;
import javax.swing.*;

public class Level0 extends JPanel{
	private float pass0;
	
	Level0(float pass0)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass0 = pass0;
	}
	
	public void paintComponent(Graphics g) 				
	{ 
		int paint0 = 100-(int)this.pass0;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);						//繪製長條圖
		g.setColor(Color.red);
		g.fillRect(40, 10+paint0, 20, (int)this.pass0);		//輸出百分比
		g.setColor(Color.black);
		g.drawString("Level 0", 35, 125);
		g.drawString(this.pass0 + "%", 35, 140);
		
	}

}
