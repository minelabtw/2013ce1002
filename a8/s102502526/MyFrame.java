package ce1002.a8.s102502526;
import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	MyFrame(float pass[])
	{
		setVisible(true);								
		setSize(500, 200);								//設定框架的大小
		setDefaultCloseOperation(EXIT_ON_CLOSE);		//關閉後離開
		setLayout(null);								
		
		Level0 level0 = new Level0(pass[0]);			//設定Level0的Panel
		level0.setLocation(20, 10);
		add(level0);
		Level1 level1 = new Level1(pass[1]);			//設定Level1的Panel
		level1.setLocation(140, 10);
		add(level1);
		Level2 level2 = new Level2(pass[2]);			//設定Level2的Panel
		level2.setLocation(260, 10);
		add(level2);
		Level3 level3 = new Level3(pass[3]);			//設定Level3的Panel
		level3.setLocation(380, 10);
		add(level3);
	}
}
