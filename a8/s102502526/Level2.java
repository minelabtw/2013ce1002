package ce1002.a8.s102502526;
import java.awt.*;
import javax.swing.*;

public class Level2 extends JPanel{    //�PLevel0
	private float pass2;
	
	Level2(float pass2)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass2 = pass2;
	}
	
	public void paintComponent(Graphics g) 					
	{ 
		int paint0 = 100-(int)this.pass2;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);					
		g.setColor(Color.red);
		g.fillRect(40, 10+paint0, 20, (int)this.pass2);		
		g.setColor(Color.black);
		g.drawString("Level 2", 35, 125);
		g.drawString(this.pass2 + "%", 35, 140);
		
	}
}
