package ce1002.a8.s102502526;
import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner a = new Scanner(System.in);
		
		int grade = 0;
		float c[] = {0,0,0,0};
		float print[] = {0,0,0,0};
		
		System.out.println("Input grades: ");
		for( int i=0;i<8;i++ )												//統計成績區間
		{
			grade = a.nextInt();
			while( grade<0 || grade>100 )
			{
				System.out.println("Out of range!");
				grade = a.nextInt();
			}
			if( grade<=25 )
				c[0]++;
			else if( grade>25 && grade <=50 )
				c[1]++;
			else if( grade>50 && grade <=75 )
				c[2]++;
			else
				c[3]++;
		}
		a.close();
		
		for( int k=0;k<4;k++)												//計算成績分布百分比
		{
			float percent = c[k]/8*100;
			System.out.println("Level " + k + " is " + percent + "%");
			print[k] = percent;
		}
		new MyFrame(print);													//繪製圖表
	}

}
