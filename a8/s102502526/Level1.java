package ce1002.a8.s102502526;
import java.awt.*;
import javax.swing.*;

public class Level1 extends JPanel{  //�PLevel0
	private float pass1;
	
	Level1(float pass1)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass1 = pass1;
	}
	
	public void paintComponent(Graphics g) 					
	{ 
		int paint0 = 100-(int)this.pass1;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);						
		g.setColor(Color.red);
		g.fillRect(40, 10+paint0, 20, (int)this.pass1);		
		g.setColor(Color.black);
		g.drawString("Level 1", 35, 125);
		g.drawString(this.pass1 + "%", 35, 140);
		
	}
}
