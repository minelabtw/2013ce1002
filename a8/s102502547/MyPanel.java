package ce1002.a8.s102502547;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class MyPanel extends JPanel {
	JLabel level = new JLabel();
	JLabel per = new JLabel();
	float x;

	MyPanel(int i, float p) {
		setLayout(null);
		setLocation(10 + 50 * i, 10); // 設定panel大小、位置
		setSize(50, 240); 
		x = p;
		level.setText("Level " + i);
		level.setLocation(10, 100);
		level.setSize(50, 10); // 設定level
		add(level);
		per.setText(p / 8 * 100 + "%");
		per.setLocation(10, 110);
		per.setSize(50, 10);  // 設定per
		add(per);
	}

	protected void paintComponent(Graphics g) { // 畫圖
		super.paintComponent(g);
		g.setColor(Color.gray);
		g.fillRect(10, 10, 10, (int) (10 * (8 - x)));
		g.setColor(Color.red);
		g.fillRect(10, 10 + (int) (10 * (8 - x)), 10, (int) (10 * x));
	}
}
