package ce1002.a8.s102502547;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		float p[] = new float[4];

		for (int i = 0; i < 4; i++) {
			p[i] = 0;
		}

		System.out.println("Input grades: ");
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < 8; i++) {
			int x = 0;
			do {
				x = scanner.nextInt();
				if (x < 0 || x > 100)
					System.out.println("Out of range!");
			} while (x < 0 || x > 100);
			if (x != 100)
				p[x / 25]++;
			else
				p[3]++;
		}

		for (int i = 0; i < 4; i++) {
			System.out.println("Level " + i + " is " + p[i] / 8 * 100 + "%");
		}

		MyFrame frame = new MyFrame(p);

		frame.setVisible(true);

	}
}
