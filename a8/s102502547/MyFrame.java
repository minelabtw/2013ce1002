package ce1002.a8.s102502547;

import javax.swing.*;

public class MyFrame extends JFrame {

	MyFrame(float a[]) {
		setLayout(null); // 設定各物件在視窗中的布局方式
		setSize(250, 200); // 設定視窗大小
		setLocationRelativeTo(null); // 設定視窗位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 按視窗右上角的"X"會關閉程序

		for (int i = 0; i < 4; i++) {
			MyPanel x = new MyPanel(i, a[i]);
			add(x);
		}
	}
}
