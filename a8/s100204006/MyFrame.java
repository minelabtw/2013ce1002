package ce1002.a8.s100204006;

import java.awt.*;

import javax.swing.*;

public class MyFrame extends JFrame
{
	 private Panel2 panel1;
	 private float a;
	 private float b;
	 private float c;
	 private float d;
	 
	 
	
	    class Panel2 extends JPanel 
	    {

	        Panel2() 
	        {
	            // set a preferred size for the custom panel.
	            setPreferredSize(new Dimension(420,420));
	        }

	        @Override
	        public void paintComponent(Graphics g) 
	        {
	            super.paintComponent(g);
    
	            
	            g.drawString("Level0", 30, 200);
	            g.drawString(String.valueOf(a*100/8)+"%", 30, 250);
	            
	            g.drawString("Level1", 80, 200);
	            g.drawString(String.valueOf(b*100/8)+"%", 80, 250);
	            
	            g.drawString("Level2", 130, 200);
	            g.drawString(String.valueOf(c*100/8)+"%", 130, 250);
	            
	            g.drawString("Level3", 180, 200);
	            g.drawString(String.valueOf(d*100/8)+"%", 180, 250);
	            
	            g.drawRect(30,80,10,50);  
	            g.setColor(Color.RED);  
	            g.fillRect(30,80,10,50);
	            
	            g.drawRect(80,80,10,50);  
	            g.setColor(Color.RED);  
	            g.fillRect(80,80,10,50);
	            
	            g.drawRect(130,80,10,50);  
	            g.setColor(Color.RED);  
	            g.fillRect(130,80,10,50);
	            
	            g.drawRect(180,80,10,50);  
	            g.setColor(Color.RED);  
	            g.fillRect(180,80,10,50);
	            
	            /////////////////////////////////////////////////////////////////////////////////////
	            g.drawRect(30,80,10,50-(int)(50*a));  
	            g.setColor(Color.WHITE);  
	            g.fillRect(30,80,10,50-(int)(50*a));
	            
	            g.drawRect(80,80,10,50-(int)(50*b));  
	            g.setColor(Color.WHITE);  
	            g.fillRect(80,80,10,50-(int)(50*b));
	            
	            g.drawRect(130,80,10,50-(int)(50*c));  
	            g.setColor(Color.WHITE);  
	            g.fillRect(130,80,10,50-(int)(50*c));
	            
	            g.drawRect(180,80,10,50-(int)(50*d));  
	            g.setColor(Color.WHITE);  
	            g.fillRect(180,80,10,50-(int)(50*d));
	            
	            
	            
	        }
	    }
	    
	 public MyFrame(float a, float b, float c, float d) 
	 {
		 this.a=a;
		 this.b=b;
		 this.c=c;
		 this.d=d;
		 this.setLayout(null);
		 newRolePos(0,0);
	 }
		  
	 public void newRolePos(int x, int y) 
	{
	  panel1 = new Panel2();
	  panel1.setLayout(null);
	  panel1.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
	  panel1.setBounds(x, y, 406, 406);
	  this.add(panel1);
	 } 
	 

}
