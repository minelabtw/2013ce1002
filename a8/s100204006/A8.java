package ce1002.a8.s100204006;

import java.util.Scanner;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;

public class A8 
{

	public static void main (String arg[]) 
	{ 
		int count=0,grade,level,level0=0,level1=0,level2=0,level3=0; 
		System.out.println("Input grades:") ; 
		while(count<8) 
		{ 
			Scanner scanner = new Scanner(System.in) ;            //System.in用來取得使用者的輸入 
			grade=scanner.nextInt(); 
			if(grade<=0||grade>=100) 
			{ 
				System.out.println("Out of range!") ; 
				continue; 
			} 
			level = grade/25; 
			switch(level)
			{ 
				case 0 : level0++; 
				count++; 
				break; 
				
				case 1 : level1++; 
				count++; 
				break; 
				
				case 2 : level2++; 
				count++; 
				break; 
				
				case 3 : level3++; 
				count++; 
				break; 
			} 
		} 
			System.out.println("Level 0 is "+(float)level0*100/8+"%") ; 
			System.out.println("Level 1 is "+(float)level1*100/8+"%") ; 
			System.out.println("Level 2 is "+(float)level2*100/8+"%") ; 
			System.out.println("Level 3 is "+(float)level3*100/8+"%") ; 
			
			JFrame frame = new MyFrame((float)level0/8,(float)level1/8,(float)level2/8,(float)level3/8);
			
			frame.setSize(406, 406);
			frame.setLocationRelativeTo(null);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setTitle(null);
			frame.setVisible(true);
			
			
	} 
}

