package ce1002.a8.s102502509;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	JLabel lab1; // 宣告標籤
	JLabel lab2;
	Figure fi; // 宣告圖表變數
	
	
	MyPanel(int level, double percent) // 創造建構子方法
	{
		setLayout(null);
		// 可直接使用變數
		fi = new Figure(percent);
		lab1 = new JLabel("Level " + level);
		lab2 = new JLabel(percent + "%");
		
		fi.setBounds(20, 0, 10, 100);
		lab1.setBounds(5, 110, 50, 10);
		lab2.setBounds(10, 145, 50, 10);
		
		add(fi);
		add(lab1);
		add(lab2);
	}
	
	
	
}
