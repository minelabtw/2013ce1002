package ce1002.a8.s102502509;

import java.util.Scanner;

public class A8 
{

	public static void main(String[] args) 
	{
		int[] score = new int[8]; // 成績紀錄陣列
		double[] time = new double[4]; // 成績次數陣列
		double[] percent = new double[4]; // 百分比儲存陣列
		MyFrame fram; // 宣告陣列
		
		Scanner input = new Scanner(System.in);
		System.out.println("Input grades: ");
		
		for(int i = 0; i < 8; i++)
		{
			do 
			{
				score[i] = input.nextInt(); // 成績存入
				
				if(score[i] < 0 || score[i] > 100)
				{
					System.out.println("Out of range!");
				}
			}while (score[i] < 0 || score[i] > 100);
		}
		
		for(int k = 0; k < 8; k++)
		{
			if(score[k] >= 0 && score[k] <= 24)
			{
				time[0]++; // 範圍成績次數紀錄
			}
			
			if(score[k] >= 25 && score[k] <= 50)
			{
				time[1]++;
			}
			
			if(score[k] >= 51 && score[k] <= 75)
			{
				time[2]++;
			}
			
			if(score[k] >= 76 && score[k] <= 100)
			{
				time[3]++;
			}
		}
		
		for(int i = 0; i < 4; i++) // 
		{
			percent[i] = time[i] / 8 * 100; // 存入成績出現比例
		}
		
		for(int j = 0; j < 4; j++)
		{
			System.out.println("Level " + j + " is " + percent[j] + " %");
		}
		
		fram = new MyFrame(percent); // call frame 並傳入比例數字陣列
		
		input.close();
	}

}
