package ce1002.a8.s102502014;

import java.util.Scanner;
import ce1002.a8.s102502014.MyFrame;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int arr[] = new int[10];
		float g1 = 0, g2 = 0, g3 = 0, g4 = 0,a=0;
		for (int i = 0; i < 8; i++) {
			System.out.println("Input grades: ");
			arr[i] = scanner.nextInt(); // scanner物件使用nextInt()方法
			while (arr[i] > 100 || arr[i] < 0) {
				System.out.println("Out of range!");
				System.out.println("Input grades: ");
				arr[i] = scanner.nextInt();
			}
			if (0 <= arr[i] && arr[i] <= 24) {
				g1++;
			} else if (25 <= arr[i] && arr[i] <= 50) {
				g2++;
			} else if (51 <= arr[i] && arr[i] <= 75) {
				g3++;
			} else if (75 <= arr[i] && arr[i] <= 100) {
				g4++;
			}
		}
		a=g1+g2+g3+g4;
		g1=(g1/a)*100;
		g2=(g2/a)*100;
		g3=(g3/a)*100;
		g4=(g4/a)*100;
		System.out.println("Level 0 is "+g1+"%");
		System.out.println("Level 1 is "+g2+"%");
		System.out.println("Level 2 is "+g3+"%");
		System.out.println("Level 3 is "+g4+"%");
		
		MyFrame demo =new MyFrame();
		demo.setPanel(0, g1, demo.p1, 0, 0);
		demo.setPanel(1, g2, demo.p2, 60, 0);
		demo.setPanel(2, g3, demo.p3, 120, 0);
		demo.setPanel(3, g4, demo.p4, 180, 0);
		
		scanner.close();
	}
	
}
