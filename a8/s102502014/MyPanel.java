package ce1002.a8.s102502014;

import java.awt.*;
import javax.swing.*;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	public JLabel l1;//黑
	public JLabel l2;//紅
	public JLabel l3;
	public JLabel l4;

	public MyPanel() {
		this.setLayout(null); // 非預設排版
		this.setSize(60, 150); // 大小
	}
	public void setBar(int a,float b) {
		l1 = new JLabel(); //黑色BAR
		l1.setSize(20, (int) (100-b));
		l1.setBackground(Color.GRAY);
		l1.setLocation(10, 10);
		l1.setOpaque(true);//不透明
		add(l1); // 加到Panel�堶�
		
		l2 = new JLabel(); //紅色BAR
		l2.setSize(20, (int) b);
		l2.setBackground(Color.RED);
		l2.setLocation(10, (int) (110-b));
		l2.setOpaque(true);//不透明
		add(l2); // 加到Panel�堶�
		
		l3 = new JLabel(); //level文字
		l3.setSize(50, 20);
		l3.setText("Level "+a);
		l3.setLocation(10, 120);
		add(l3); // 加到Panel�堶�
		
		l4 = new JLabel(); //%數
		l4.setSize(50, 20);
		l4.setText(b+"%");
		l4.setLocation(10, 130);
		add(l4); // 加到Panel�堶�
		}
}
