package ce1002.a8.s102502010;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel{
	
	protected JLabel[] level = new JLabel[4];
	protected JLabel[] label = new JLabel[4];
	private double leng[];
	
	MyPanel(double[] percent)
	{
		setLayout(null);
		setBounds(0,0,300,300);  //setBounds
		leng = percent;
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		for(int i=0; i<4; i++) //setpicture
		{
			g.setColor(Color.red);
			g.fillRect(20+i*50, 30, 30, 100);
			g.setColor(Color.gray);
			g.fillRect(20+i*50, 30, 30, (int) (100-leng[i]/8*100)); //fillrectangle
			level[i] = new JLabel();
			level[i].setBounds(20+i*50, 130, 50, 20);
			level[i].setText("Level " + i);
			label[i] = new JLabel();
			label[i].setBounds(20+i*50, 140, 50, 20);
			label[i].setText(leng[i]/8*100 + "%");
			add(level[i]);
			add(label[i]);
		}
	}
	
}
