package ce1002.a8.s102502010;

import javax.swing.JFrame;

public class MyFrame extends JFrame{
	
	MyFrame(double[] percent)
	{
		MyPanel panel = new MyPanel(percent);
		setLayout(null);
		setBounds(300 , 200 , 250 , 280);
		setVisible(true);  //setVisible
		setDefaultCloseOperation(EXIT_ON_CLOSE);  //close
		add(panel);
	}
}
