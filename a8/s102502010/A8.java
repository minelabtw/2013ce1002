package ce1002.a8.s102502010;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		double grade[] = new double[8];
		double percent[] = new double[4];
		Scanner input = new Scanner(System.in);
		System.out.println("Input grades:");
		for(int i=0; i<8; i++)  //input
		{
			grade[i]=input.nextDouble();
			while(grade[i]<0 || grade[i]>100)
			{
				System.out.println("Out of range!");
				grade[i]=input.nextDouble();
			}
			if(grade[i]>=76)
				percent[3]++;
			else if(grade[i]>=51)
				percent[2]++;
			else if(grade[i]>=26)
				percent[1]++;
			else
				percent[0]++;
		}
		for(int i=0; i<4; i++)  //cout
			System.out.println("Level " + i + " is " + percent[i]/8*100 + "%");
		MyFrame myframe = new MyFrame(percent);
		input.close();
	}
}
