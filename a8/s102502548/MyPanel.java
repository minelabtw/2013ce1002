package ce1002.a8.s102502548;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.text.StyledEditorKit.ForegroundAction;

public class MyPanel extends JPanel{//繼承JPanel
	private double percent ;
	private int locate=0 ;
	
	MyPanel(double percent, int x){//傳入參數
		setLayout(null) ;
		
		JLabel[] label=new JLabel[2] ;
		
		label[0]=new JLabel() ;//陣列使體化
		label[1]=new JLabel() ;
		
		for (int y=0;y<2;y++){
			label[y].setBounds(0,200+20*y,130,30) ;
		}
		
		setBounds(5+80*x, 10, 50, 300) ;
		
		this.percent=percent/100 ;
		
		if (x==0){
			label[0].setText("Level 0") ;//決定字的內容
		}
		
		if (x==1){
			label[0].setText("Level 1") ;
		}
		
		if (x==2){
			label[0].setText("Level 2") ;
		}
		
		if (x==3){
			label[0].setText("Level 3") ;
		}
		
		label[1].setText(this.percent*100+"%") ;
		
		locate=x ;
		
		for (int z=0;z<2;z++){
			add(label[z]) ;
		}
	}
	
	@Override
	public void paintComponent(Graphics g){
		
		g.setColor(Color.RED) ;//畫紅色的矩形
			
		g.fillRect(0, 5, 10, 200) ;
		
		g.setColor(Color.GRAY) ;//用灰色的矩形蓋掉部分紅色的
			
		g.fillRect(0, 5, 10, (int)(200*(1-percent))) ;//強制轉性
		
	}
}

