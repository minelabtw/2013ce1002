package ce1002.a8.s102502548;

import java.util.Scanner; ;

public class A8 {

	public static void main(String[] args) {
		Level[] level=new Level[4] ;//宣告
		int[] grade=new int[8] ;
		int number=0 ;
		double[] percent=new double[4] ;
		Scanner sc=new Scanner(System.in) ;
		
		System.out.println("Input grades:") ;
		
		level[0]=new Level(0) ;//需 要實體化
		level[1]=new Level(1) ;
		level[2]=new Level(2) ;
		level[3]=new Level(3) ;
		
		for (int x=0;x<8;x++){//判斷範圍
			number=sc.nextInt() ;
				
			if (number>=0&&number<=100){
				grade[x]=number ;
			}
			else{
				System.out.println("Out of range!") ;
				
				x=x-1 ;
			}
		}
		
		for (int x=0;x<8;x++){//分類
			if (grade[x]<=50){
				if (grade[x]<=24){
					level[0].counter() ; 
				}
				else {
					level[1].counter() ;
				}
			}
			else {
				if (grade[x]>=75){
					level[3].counter() ;
				}
				else {
					level[2].counter() ;
				}
				
			}
		}
		
		for (int x=0;x<4;x++){
			percent[x]=level[x].getcounter()/8.0*100 ;
		}
		
		for (int x=0;x<4;x++){
			System.out.println(level[x].getname()+" is "+percent[x]+"%") ;
		}
		
		MyFrame frame=new MyFrame(percent) ;
	}

}
