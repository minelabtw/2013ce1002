package ce1002.a8.s102503017;

import javax.swing.*;

import java.awt.*;

public class Panel extends JPanel
{
	//This is the panel for the basic background.
	JLabel label0 = new JLabel();
	JLabel label1 = new JLabel();
	JLabel label2 = new JLabel();
	JLabel label3 = new JLabel();
	
	
	Panel(float[] score)
	{
		setLayout(null);
		setBounds(0,0,900,450);
		
		//setting the labels and putting the input results into them.
		label0.setBounds(50,310,100,70);
		label1.setBounds(250,310,100,70);
		label2.setBounds(450,310,100,70);
		label3.setBounds(650,310,100,70);

		label0.setText("<html>Level 0<br>" + score[0]*100 + "%</html>");
		label1.setText("<html>Level 1<br>" + score[1]*100 + "%</html>");
		label2.setText("<html>Level 2<br>" + score[2]*100 + "%</html>");
		label3.setText("<html>Level 3<br>" + score[3]*100 + "%</html>");
		
		add(label0);
		add(label1);
		add(label2);
		add(label3);
		
	}
	
	protected void paintComponent(Graphics g)
	{
		//draw the bar statistics background.
		g.setColor(Color.gray);
		g.fillRect(50,30 , 35, 250);
		g.fillRect(250,30 , 35, 250);
		g.fillRect(450,30 , 35, 250);
		g.fillRect(650,30 , 35, 250);

	}
				
		
	}
	
	
	