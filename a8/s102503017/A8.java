package ce1002.a8.s102503017;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) 
	{
		Scanner scanner = new Scanner(System.in);
	
		int temp;
		float[] level = new float [4];
		
		System.out.println("Input grades:");
		
		//The determination of the inputs and put it into an counting array "level".
		for(int i = 0; i < 8 ; i++)
		{
			do
			{
				temp = scanner.nextInt();
				if(temp < 0 || temp > 100)
				{
					System.out.println("Out of range!");
				}
			}while(temp <= 0 || temp >= 100);
		
			if(temp <= 50)
			{
				if(temp <= 25)
				{
					level[0]++;
				}
				else
				{
					level[1]++;
				}
			}//end if.
			else
			{
				if(temp <= 75)
				{
					level[2]++;
				}
				else
				{
					level[3]++;
				}
			}//end else.		
		}//end for.
	
		for(int i = 0; i < 4; i++)
		{
			level[i] /= 8;
		}
		//calculate the percentage of the grades.
		
		//incarnate the frame.
		Frame frame = new Frame(level);
		
		
	
	}//end main.

}
