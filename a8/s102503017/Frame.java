package ce1002.a8.s102503017;

import javax.swing.*;

import java.awt.*;

public class Frame extends JFrame
{
	RedBar bar0 = new RedBar();
	RedBar bar1 = new RedBar();
	RedBar bar2 = new RedBar();
	RedBar bar3 = new RedBar();

	//setting the parameters of the frame.
	Frame(float[] score)
	{	
		Panel panel = new Panel(score);
		
		setLayout(null);
		setBounds(0,0,900,450);
		setVisible(true);
		//putting the background panel via using add(component,index).
		add(panel,1,0);

		//using addResult method to putting the RedBar objects after calculations.
		addResult(score[0], bar0, 50);
		addResult(score[1], bar1, 250);
		addResult(score[2], bar2, 450);
		addResult(score[3], bar3, 650);
		
	}
	
	protected void addResult(float num, RedBar bar, int x)
	{
		//setting the bounds of bars to show the result from bottom and rising to the top.
		bar.setBounds(x, (30 + 250 - (int)(250 * num)), 35, (int)(250* num));
		//putting the RedBars via using add(component,index).
		add(bar,2,0);
		
	}
	
}
