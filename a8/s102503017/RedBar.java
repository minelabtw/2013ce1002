package ce1002.a8.s102503017;

import javax.swing.*;

import java.awt.*;

public class RedBar extends JPanel
{
	//Draw a bar in red which will be controlled the size by the result of the inputs.
	RedBar()
	{
		setLayout(null);
		setBounds(0,0,35, 250);
		
	}
	
	protected void paintComponent(Graphics g)
	{
		g.setColor(Color.red);
		g.fillRect(0, 0, 35, 250);
	}



}
