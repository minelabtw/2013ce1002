package ce1002.a8.s101201524;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JLabel;

public class MyPanel extends JPanel{
	private double[] percent;//declare a matrix for paint
	
	MyPanel(double[] percent){
		//set percent become each level's percentage
		this.percent = percent;
		setSize(500, 500);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		for(int i = 0; i < 4; i++){
			//draw stems
			g.setColor(Color.GRAY);
			g.fillRect((2*i+1)*getWidth()/8-(int)(getWidth()*0.025), 15, (int)(getWidth()*0.05), (int)(getHeight()*0.8*(1-percent[i])));
			g.setColor(Color.RED);
			g.fill3DRect((2*i+1)*getWidth()/8-(int)(getWidth()*0.025), 15+(int)(getHeight()*0.8*(1-percent[i])), (int)(getWidth()*0.05), (int)(getHeight()*0.8*percent[i]), true);
			//draw description of percentages
			g.setColor(Color.BLACK);
			g.drawString("Level " + i, (int)((2*i+1)*getWidth()/8)-15, (int)(getHeight()*0.9));
			g.drawString(percent[i]*100 + "%", (int)((2*i+1)*getWidth()/8)-13, (int)(getHeight()*0.9)+12);
			
		}
	}
}
