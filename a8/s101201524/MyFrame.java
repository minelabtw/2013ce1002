package ce1002.a8.s101201524;
import javax.swing.JFrame;

public class MyFrame extends JFrame{
	//create a myframe and set its status 
	MyFrame(double[] percent){
		MyPanel grade = new MyPanel(percent);
		add(grade);
		setSize(500, 500);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
