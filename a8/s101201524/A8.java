package ce1002.a8.s101201524;
import java.util.Scanner;

public class A8 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int count = 8;
		double grade = 0 ;
		double[] level = new double[4];
		//input grades(0 <= grade <= 100)
		System.out.println("Input grades: ");
		while(count > 0){
			grade = input.nextDouble();
			//check legal and count member of each level
			if(0 <= grade && grade <= 100){
				level[((int)grade-1)/25]++;
				count--;
			}
			else
				System.out.println("Out of range!");
		}
		//count and output each level's percentage
		for(int i = 0; i < 4; i++){
			level[i] = (level[i] / 8);
			System.out.println("Level " + i + " is " + (level[i] * 100) + "%");
		}
		//show the percentage graph
		MyFrame frame = new MyFrame(level);
	}
}
