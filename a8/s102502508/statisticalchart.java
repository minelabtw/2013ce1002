package ce1002.a8.s102502508;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
public class statisticalchart extends JPanel {
	
    public statisticalchart() {
		
		
		setBounds(10, 10, 500, 300);//設定Panel的大小
	
	}
	
	//用同一個函式分別畫出其四個同形狀但隨著所代入值不同而顏色不同的長方形
	public void paintComponent(Graphics g)
	{
	  	
		g.drawRect(10, 10, 20, 100);//設定所畫長方形的起始點及邊長大小
		g.setColor(Color.RED);//將該長方形著色(紅色)
		g.fillRect(10, 10, 20, 100);//著色範圍
		g.setColor(Color.GRAY);//將該長方形著色(灰色)
		g.fillRect(10, 10, 20, (int) (100-(A8.A)));
		g.setColor(Color.BLACK);//字體顏色
		g.drawString("Level 0", 10, 125);//在圖案中寫字
		g.drawString(String.valueOf(A8.A+"%"),10,145);//字的位子
		
		g.drawRect(130, 10, 20, 100);
		g.setColor(Color.RED);
		g.fillRect(130, 10, 20, 100);
		g.setColor(Color.GRAY);
		g.fillRect(130, 10, 20, (int) (100-(A8.B)));
		g.setColor(Color.BLACK);
		g.drawString("Level 1", 130, 125);
		g.drawString(String.valueOf(A8.B+"%"),130,145);
		
		g.drawRect(250, 10, 20, 100);
		g.setColor(Color.RED);
		g.fillRect(250, 10, 20, 100);
		g.setColor(Color.GRAY);
		g.fillRect(250, 10, 20, (int) (100-(A8.C)));
		g.setColor(Color.BLACK);
		g.drawString("Level 2", 250, 125);
		g.drawString(String.valueOf(A8.C+"%"),250,145);
		
		g.drawRect(370, 10, 20, 100);
		g.setColor(Color.RED);
		g.fillRect(370, 10, 20, 100);
		g.setColor(Color.GRAY);
		g.fillRect(370, 10, 20, (int) (100-(A8.D)));
		g.setColor(Color.BLACK);
		g.drawString("Level 3", 370, 125);
		g.drawString(String.valueOf(A8.D+"%"),370,145);
	}

}
