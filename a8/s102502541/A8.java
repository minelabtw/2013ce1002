package ce1002.a8.s102502541;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		int num = 4;
		int temp = 0;
		float level[] = new float[num];
		System.out.println("Input grades: ");
		for(int i = 0; i<8; i++)
		{
			do
			{
				temp = scanner.nextInt();
				if(temp<0 || temp >100)
					System.out.println("Out of range!");
				else if(temp>=0 && temp<=25)
					level[0] ++;
				else if(temp>26 && temp<=50)
					level[1] ++;
				else if(temp>51 && temp<=75)
					level[2] ++;
				else
					level[3] ++;
			}while(temp<0 || temp >100);	
		}
		System.out.println("Level 0 is "+level[0]*100/8 +"%");
		System.out.println("Level 1 is "+level[1]*100/8 +"%");
		System.out.println("Level 2 is "+level[2]*100/8 +"%");
		System.out.println("Level 3 is "+level[3]*100/8 +"%");
		panel p = new panel();
		for(int i= 0; i<4; i++)
		{
			p.level[i] = level[i]*100/8;//將統計數值傳給panel
		}
		p.state0.setText(Float.toString(p.level[0])+"%");
		p.state1.setText(Float.toString(p.level[1])+"%");
		p.state2.setText(Float.toString(p.level[2])+"%");
		p.state3.setText(Float.toString(p.level[3])+"%");//幫label設定統計數值
		frame f = new frame();
		f.add(p);	
	}
}
