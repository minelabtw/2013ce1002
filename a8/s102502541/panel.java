package ce1002.a8.s102502541;
import javax.swing.*;
import java.awt.Color;
import java.awt.Graphics;

public class panel extends JPanel{
	public float level[] = new float[4];
	JLabel title = new JLabel("Level 0   Level 1   Level 2   Level 3");
	JLabel state0 = new JLabel();
	JLabel state1 = new JLabel();
	JLabel state2 = new JLabel();
	JLabel state3 = new JLabel();
	public panel()
	{
		setLayout(null);
		setBounds(0,0,200,150);
		title.setBounds(10, 110, 200, 20);
		state0.setBounds(10,130,50,20);
		state1.setBounds(60,130,50,20);
		state2.setBounds(110,130,50,20);
		state3.setBounds(160,130,50,20);
		add(title);
		add(state0);
		add(state1);
		add(state2);
		add(state3);
	}
		protected void paintComponent(Graphics g)
		{
			super.paintComponents(g);
			for(int i=0; i<4; i++)
			{
				Color c = new Color(128,128,128);
				g.setColor(c);
				g.fillRect(10+50*i,10,10,100);
			}//畫灰色長條
			for(int i=0; i<4; i++)
			{
				Color c = new Color(253,1,0);
				g.setColor(c);
				g.fillRect(10+50*i,110-(int)(level[i]),10,(int)(level[i]));
			}//依統計數值畫紅色長條
		}	
}
