package ce1002.a8.s100502022;

import java.awt.Color;
import java.awt.GridLayout;
import java.text.NumberFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
 
public class MyFrame extends JFrame{
	public MyFrame(double[] data){
		setLayout(new GridLayout(1,4));
		setBounds(300 , 50 , 400 , 300);
		//four data
		levelPanel l1 = new levelPanel(data[0]);
		add(l1);
		levelPanel l2 = new levelPanel(data[1]);
		add(l2);
		levelPanel l3 = new levelPanel(data[2]);
		add(l3);
		levelPanel l4 = new levelPanel(data[3]);
		add(l4);
		
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
}
