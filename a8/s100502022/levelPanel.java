package ce1002.a8.s100502022;

import java.awt.Color;
import java.awt.Graphics;
import java.text.NumberFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class levelPanel extends JPanel{
	//global varible used to count level
	public static int x=0;
	private double data;
	private JLabel t1 = new JLabel();
	public levelPanel(double data){
		//fraction output
		NumberFormat nf = NumberFormat.getPercentInstance();
	    nf.setMaximumFractionDigits( 2 );
		this.data = data;
		setBounds(0,0,100,300);
		//settext
		t1.setText("<html>Level "+x+"<br>"+nf.format(data)+"</html>");
		x++;
		add(t1);
	}
	//draw histogram
	public void paintComponent(Graphics g){
		g.setColor(Color.gray);
		g.fillRect(30, 50, 20, 200);
		g.setColor(Color.red);
		g.fillRect(30, 250-(int)(data*200), 20, (int)(data*200));
	}
}
