package ce1002.a8.s100502022;

import java.text.NumberFormat;
import java.util.Scanner;

public class A8 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		//set MaximumFractionDigits
		NumberFormat nf = NumberFormat.getPercentInstance();
		nf.setMaximumFractionDigits(2);
		
		int[] level = new int[4];
		//input design
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
			int grade;
			do{
				grade = input.nextInt();
				if(grade>=0&&grade<=100){
					if(grade<25&&grade>=0){
						level[0]++;
					}
					else if(grade<50&&grade>=25){
						level[1]++;
					}
					else if(grade<75&&grade>=50){
						level[2]++;
					}
					else{
						level[3]++;
					}
				}
				else{
					System.out.println("Out of range!");
				}
			}while(grade<0||grade>100);
		}
		double[] level_d=new double[4];
		//output
		for(int i=0;i<4;i++){
			level_d[i]=(double)level[i]/8;
			System.out.println("Level "+i+" is "+nf.format((double)level[i]/8));
		}
		input.close();
		MyFrame mf = new MyFrame(level_d);
	}
}
