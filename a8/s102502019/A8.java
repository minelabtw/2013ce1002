package ce1002.a8.s102502019;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
double [] grade = new double [8];
double [] level = new double [4];
double [] plevel = new double [4];
Scanner input = new Scanner(System.in);
System.out.print("Input grades: ");
for (int i=0; i<8; i++)
{
	grade[i] = input.nextInt();
	while(grade[i]<0 || grade[i]>100)
	{
		System.out.print("Out of range!\n");
		grade[i] = input.nextInt();
	}
	if(grade[i]>=0 && grade[i]<=25)
	{
		level[0]++;
	}else if(grade[i]>=26 && grade[i]<=50)
	{
		level[1]++;
	}else if(grade[i]>=51 && grade[i]<=75)
	{
		level[2]++;
	}else 
	{
		level[3]++;
	}
}
	plevel[0] = (level[0]/(level[0]+level[1]+level[2]+level[3]))*100;
	plevel[1] = (level[1]/(level[0]+level[1]+level[2]+level[3]))*100;
	plevel[2] = (level[2]/(level[0]+level[1]+level[2]+level[3]))*100;
	plevel[3] = (level[3]/(level[0]+level[1]+level[2]+level[3]))*100;
	for(int k=0; k<4; k++)
	{
		System.out.print("Level "+k+" is "+plevel[k]+"%\n");
	}
	MyFrame frame = new MyFrame(plevel);
	}

}
