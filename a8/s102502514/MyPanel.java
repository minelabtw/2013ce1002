package ce1002.a8.s102502514;

import java.awt.*;

import javax.swing.*;

public class MyPanel extends JPanel{
	private JLabel levelnumber = new JLabel();
	private JLabel levelpercentage = new JLabel();
	private int percentage;
	
	MyPanel(){
		setLayout(null);
		setSize(55,180);
	}
	public void setState(int number,double percentage){
		levelnumber.setText("Level " + number);
		levelpercentage.setText(percentage + "%");
		this.percentage = (int)percentage;
		levelnumber.setBounds(10,120,55,15);  //設定label相對於panel的位置
		levelpercentage.setBounds(10,135,55,15);  //設定label相對於panel的位置
		add(levelnumber);
		add(levelpercentage);
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.setColor(Color.red);
		g.fillRect(10, 10, 10, 100);
		g.setColor(Color.gray);
		g.fillRect(10, 10, 10, (100-percentage));
	}
}
