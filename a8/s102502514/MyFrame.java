package ce1002.a8.s102502514;

import javax.swing.*;

public class MyFrame extends JFrame{
	MyFrame(double arr[]){
		setLayout(null);
		setSize(300,200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		MyPanel panel[] = new MyPanel[4];
		for(int i=0; i<4; i++){
			panel[i] = new MyPanel();
			panel[i].setState(i,arr[i]);  //設定panel的數值狀態
			panel[i].setLocation(55*i, 0);  //設定panel相對於frame的位置
			add(panel[i]);
		}
	}
}
