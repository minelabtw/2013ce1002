package ce1002.a8.s102502514;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double level[] = new double [4];  //創立四個等級的陣列
		int temp = 0;  //計算有效輸入次數
		System.out.println("Input grades: ");
		
		for (; temp<8; temp++){  //跑有效次數8次
			int grades = input.nextInt();
			while (grades<0||grades>100){
				System.out.println("Out of range!");
				grades = input.nextInt();
			}
			if (grades<=25 && grades>=0)
				level[0]++;
			else if (grades<=50 && grades>=26)
				level[1]++;
			else if (grades<=75 && grades>=51)
				level[2]++;
			else
				level[3]++;
		}
		for (int i = 0; i<4; i++){
			level[i] = level[i]/8*100;  //將level[i]由計次改為百分比
		    System.out.println("Level " + i + " is " + level[i] + "%");
		}
		MyFrame frame = new MyFrame(level);//將level陣列的位址傳入MyFrame的建構式中
		input.close();
	}
}
