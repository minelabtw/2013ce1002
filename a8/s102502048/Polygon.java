package ce1002.a8.s102502048;
import javax.swing.*;
import java.awt.*;

public class Polygon extends JPanel
{
	private int h=0;
	JLabel lab;
	JLabel lab1;
	Polygon(float a,int b)
	{
		h=(int)a;
		lab = new JLabel("Level "+b);
		lab1 = new JLabel(a+"%");
	}
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(15, 10+(100-h), 20, h);
		g.setColor(Color.GREEN);
		int x[]={15,35,49,29};
		int y[]={10+(100-h),10+(100-h),10+(100-h)-14,10+(100-h)-14};
		g.fillPolygon(x,y,y.length);
		g.setColor(Color.BLUE);
		int i[]={35,49,49,35};
		int j[]={110,110-14,10+(100-h)-14,10+(100-h)};
		g.fillPolygon(i,j,i.length);
		/*set label font*/
		Font font = new Font("Time New Roman", Font.BOLD, 14);		
		lab.setFont(font);
		lab.setBounds(10,120,100,15);
		add(lab);
		lab1.setFont(font);
		lab1.setBounds(10,140,100,15);
		add(lab1);
	}
}
