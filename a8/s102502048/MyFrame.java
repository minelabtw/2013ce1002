package ce1002.a8.s102502048;
import javax.swing.*;


public class MyFrame extends JFrame 
{
	MyFrame(float a,float b,float c,float d)
	{
		/*Create three role panel*/
		Polygon p0 = new Polygon(a,0);
		Polygon p1 = new Polygon(b,1);
		Polygon p2 = new Polygon(c,2);
		Polygon p3 = new Polygon(d,3);
		/*set frame's size , layout...*/
		setLayout(null);
		setBounds(0 , 0 , 485 , 330);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
		
		setp(p0,10,10);
		setp(p1,110,10);
		setp(p2,210,10);
		setp(p3,310,10);
	}	
	public void setp(JPanel panel, int x, int y)
	{
		/*set panel's position and size*/
		panel.setBounds(x, y, 100 , 300);
		/*add panel to frame*/
		add(panel);
	}
}
