package ce1002.a8.s102502048;
import java.util.Scanner;
import java.awt.*;
import javax.swing.*;

public class A8 
{
	public static void main(String[] args) 
	{
		Scanner input = new Scanner(System.in);
		int[] z = new int[8];//宣告,
		float a=0,b=0,c=0,d=0;
		System.out.println("Input grades: ");
		for(int i=0; i<8; i++)
		{
			do
			{
				z[i] = input.nextInt();//輸入		
				if(z[i] <0 || z[i] >100)
					System.out.println("Out of range!");//顯示
			}while(z[i] <0 || z[i] >100);
		}
		for(int i=0; i<8; i++)
		{
			if(z[i]>=76)
				a++;
			else if(51<=z[i] && z[i]<=75)
				b++;
			else if(26<=z[i] && z[i]<=50)
				c++;
			else
				d++;
		}
		System.out.println("Level 0 is "+d*12.5+"%");//顯示
		System.out.println("Level 1 is "+c*12.5+"%");//顯示
		System.out.println("Level 2 is "+b*12.5+"%");//顯示
		System.out.println("Level 3 is "+a*12.5+"%");//顯示
		
		MyFrame frame = new MyFrame(d/8*100,c/8*100,b/8*100,a/8*100);
		//frame((float)a,(float)b,(float)c,(float)d)
	}

}
