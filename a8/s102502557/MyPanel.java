package ce1002.a8.s102502557;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;

public class MyPanel extends JPanel
{
	private double per;
	int n ;
	MyPanel(int i)
	{
		setLayout(null);
		setSize(75,200);
		this.n = i;
	}
	public void set_per(double percent)//作一個set
	{
		this.per = percent;
	}
	public void print_words()
	{
		JLabel level = new JLabel();
		JLabel percentage = new JLabel();
		level.setText("level"+n);
		level.setBounds(20,80,100,100);//位置不變 都在那個panel的絕對位置上 因為他是label
		percentage.setText(""+per+"%");//per 才是int 所以要強制轉型
		percentage.setBounds(20,100,100,100);
		add(level);
		add(percentage);
	}
	
	protected void paintComponent(Graphics g)//只要要畫圖的 一定要在這裡面畫
	{
		super.paintComponent(g);
		g.setColor(Color.GRAY);
		g.fillRect(20,10,15,100);
		g.setColor(Color.RED);//先設顏色 在畫圖行
		g.fillRect(20, (int) (10+(100-per)), 15, (int) (0.5+per));//0.5是因為要向上取整數
	}
}
