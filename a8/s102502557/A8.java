package ce1002.a8.s102502557;

import java.util.Scanner;
import java.awt.*;

import javax.swing.*;

public class A8 {
	
	static Scanner input = new Scanner(System.in);
	static float grade;
	static int level[] = new int[4];
	static double Percentage[] = new double[4];
	
	//函數 用來輸入成績
	public static void InputGrade()
	{
		System.out.println("Input grades: ");
		
		for(int n=0;n<8;n++)
		{
		 do
		 {
			grade = input.nextFloat();
			if( grade<0 || grade>100)
			{
				System.out.println("Out of range!");
			}	
			if(grade>=0&&grade<=25)
				level[0]++;
			if(grade>=26&&grade<=50)
				level[1]++;
			if(grade>=51&&grade<=75)
				level[2]++;
			if(grade>=76&&grade<=100)
				level[3]++;
	 	 }while(grade<0 || grade>100);
		}		
		for(int i=0;i<4;i++)
		{
			Percentage[i] = ((double)level[i]/8)*100;
			System.out.print("Level "+ i + " is " +  Percentage[i] + "%" + "\n");//算百分比那邊記得要轉型 不然他會當成int 
		}
	}//輸入成績的函數結束
     
	public static void main(String[] args) 
	{
		MyPanel[] panel = new MyPanel[4];
		MyFrame frame = new MyFrame();
		InputGrade();
		for(int n=0;n<4;n++)
		{
			panel[n] = new MyPanel(n);//每一個都初始化  這個n是把n傳進myPanel class的關鍵
			panel[n].setLocation(n*100, 10);
			frame.addPanel(Percentage[n],n,panel[n]);
		}
		frame.setVisible(true);
	}	
}
/*這次的關鍵就在 怎麼把百分比傳給myframe mypanel 
  先藉由myframe裡面的addpanel函式 這個函式裡用各種引數 包括 Mypanel 類別，計數器n，百分比
  再藉由mypanel裡面的setter 百分比再丟回給mypanel設的double per */ 