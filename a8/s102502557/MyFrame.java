package ce1002.a8.s102502557;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame
{
	MyFrame()
	{
		setLayout(null);//取消預設的排版
		setSize(400,300);
		setLocationRelativeTo(null);//視窗置中
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void addPanel(double percent, int n, MyPanel panel)
	{
		panel.set_per(percent);
		panel.print_words();
		this.add(panel);//MyPanel的函式
		
	}
}
