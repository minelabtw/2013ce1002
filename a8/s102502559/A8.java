package ce1002.a8.s102502559;
import java.util.Scanner;
public class A8 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] score = new int[8];//陣列存放分數
		int[] Level = new int[4]; //陣列存放各Level人數
		double[] Percentage = new double[4];//存放百分比
		
		System.out.println("Input grades: ");
		for(int i = 0 ; i < 8 ; i++)
		{
			score[i]=input.nextInt();
			while(score[i]<0 || score[i]>100)
			{
				System.out.println("Out of range !");
				score[i]=input.nextInt();
			}
			if(score[i]>=0 && score[i]<=25)
				Level[0]++;
			if(score[i]>=26 && score[i]<=50)
				Level[1]++;
			if(score[i]>=51 && score[i]<=75)
				Level[2]++;
			if(score[i]>=76 && score[i]<=100)
				Level[3]++;
		}
		for(int j=0;j<4;j++)
		{
			Percentage[j] = (double)Level[j]/8*100;
			System.out.println("Level " + j + " is " + Percentage[j] +"%");
		}
		/*---------------------------------------------------------------------------*/
		MyPanel[] panel = new MyPanel[4];//宣告面板
		MyFrame frame = new MyFrame();//宣告框架
		for(int k=0;k<4;k++)
		{
			panel[k] = new MyPanel();//呼叫建構式初始化panel
			frame.addPanel(Percentage[k],k,panel[k]);
		}		
		frame.setVisible(true);//顯示視窗
	}

}
