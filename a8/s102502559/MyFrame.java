package ce1002.a8.s102502559;

import java.awt.GridLayout;

import javax.swing.*;

public class MyFrame extends JFrame {
	MyFrame() {
		setSize(400,200);//設定大小
		setLocationRelativeTo(null);//設定視窗起始位置
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//關閉鈕動作
		setLayout(new GridLayout(1,4,5,10));//排版方式
		
	}
	//將加入長條圖的Panel加入Frame中
	public void addPanel(double Percentage,int k,MyPanel panel)
	{	panel.setPer(Percentage);
		panel.subPanel1();
		panel.subPanel2(k);
		this.add(panel);
	}
}
