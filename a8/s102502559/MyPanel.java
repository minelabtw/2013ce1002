package ce1002.a8.s102502559;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class MyPanel extends JPanel{
	public double Per;
	MyPanel()
	{
		setSize(110,150);
		setLayout(null);
		//LineBorder LB = new LineBorder(Color.black,5);
		//this.setBorder(LB);
	}
	public void setPer(double Percentage)
	{
		this.Per = Percentage;//取得百分比值
	}
	public void subPanel1()
	{
		StatusPanel sub1 = new StatusPanel(Per);
		this.add(sub1);
	}
	public void subPanel2(int k)
	{
		JPanel sub2 = new JPanel();
		sub2.setBounds(0, 100, 90,50);
		sub2.setLayout(null);
		//LineBorder LB = new LineBorder(Color.black,5);
		//sub2.setBorder(LB);
		JLabel label1 = new JLabel("Level"+k);//Label顯示等級
		label1.setBounds(20,0,50,50);
		JLabel label2 = new JLabel(""+Per+"%");//Label顯示Lavel百分比
		label2.setBounds(20,10,50,50);
		sub2.add(label1);
		sub2.add(label2);
		this.add(sub2);
	}
}
