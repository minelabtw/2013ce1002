package ce1002.a8.s102502525;

import java.awt.*;
import javax.swing.*;

public class MyFrame extends JFrame{
	
	MyFrame(float pass[])
	{
		setVisible(true);								//框架可見
		setSize(380, 200);								//框架大小為500*200
		setDefaultCloseOperation(EXIT_ON_CLOSE);		//關閉後離開
		setLayout(null);								//將Panel排版關閉
		
		Panel1 p1 = new Panel1(pass[0]);			//設定Level0的Panel
		p1.setLocation(10, 10);
		add(p1);
		Panel2 p2 = new Panel2(pass[1]);			//設定Level1的Panel
		p2.setLocation(90, 10);
		add(p2);
		Panel3 p3 = new Panel3(pass[2]);			//設定Level2的Panel
		p3.setLocation(170, 10);
		add(p3);
		Panel4 p4 = new Panel4(pass[3]);			//設定Level3的Panel
		p4.setLocation(250, 10);
		add(p4);
	}

}
