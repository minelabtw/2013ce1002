package ce1002.a8.s102502525;

import java.awt.*;

import javax.swing.*;

public class Panel3 extends JPanel {
	
	private float pass2;
	
	Panel3(float pass2)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass2 = pass2;
	}
	
	public void paintComponent(Graphics g) 					//繪圖
	{ 
		int paint0 = 100-(int)this.pass2;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);						//長條圖空間
		g.setColor(Color.yellow);
		g.fillRect(40, 10+paint0, 20, (int)this.pass2);		//百分比空間
		g.setColor(Color.black);
		g.drawString("Level 2", 35, 125);
		g.drawString(this.pass2 + "%", 35, 140);
		
	}

}
