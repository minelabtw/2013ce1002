package ce1002.a8.s102502525;

import java.awt.*;

import javax.swing.*;

public class Panel4 extends JPanel {
	
	private float pass3;
	
	Panel4(float pass3)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass3 = pass3;
	}
	
	public void paintComponent(Graphics g) 					//繪圖
	{ 
		int paint0 = 100-(int)this.pass3;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);						//長條圖空間
		g.setColor(Color.green);
		g.fillRect(40, 10+paint0, 20, (int)this.pass3);		//百分比空間
		g.setColor(Color.black);
		g.drawString("Level 3", 35, 125);
		g.drawString(this.pass3 + "%", 35, 140);
		
	}

}
