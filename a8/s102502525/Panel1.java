package ce1002.a8.s102502525;

import java.awt.*;

import javax.swing.*;

public class Panel1 extends JPanel {
	
	private float pass0;
	
	Panel1(float pass0)
	{
		setBounds( 0 , 0 , 100 , 160);
		this.pass0 = pass0;
	}
	
	public void paintComponent(Graphics g) 					//繪圖
	{ 
		int paint0 = 100-(int)this.pass0;
		
		g.setColor(Color.gray);
		g.fillRect(40, 10, 20, 100);						//長條圖空間
		g.setColor(Color.red);
		g.fillRect(40, 10+paint0, 20, (int)this.pass0);		//百分比空間
		g.setColor(Color.black);
		g.drawString("Level 0", 35, 125);
		g.drawString(this.pass0 + "%", 35, 140);
		
	}

}
