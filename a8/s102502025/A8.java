package ce1002.a8.s102502025;

import java.util.Scanner;
import javax.swing.JFrame;

public class A8 {
	public static double number[] = new double[4];

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		double inputgrade = -1;//成績變數
		double grade[] = new double[8];//成績的矩陣

		System.out.println("Input Grade:");

		for (int k = 0; k < 8; k++) {//輸入成績
			do {
				inputgrade = input.nextDouble();
				if (inputgrade < 0 || inputgrade > 100) {
					System.out.println("Out of range!");
				}
			} while (inputgrade < 0 || inputgrade > 100);

			grade[k] = inputgrade;

			inputgrade = 0;
		}

		for (int k = 0; k < 8; k++) {//計算數量
			if (grade[k] >= 0 && grade[k] <= 25) {
				number[0]++;
			} else if (grade[k] >= 26 && grade[k] <= 50) {
				number[1]++;
			} else if (grade[k] >= 51 && grade[k] <= 75) {
				number[2]++;
			} else {
				number[3]++;
			}
		}

		for (int k = 0; k < 4; k++) {//輸出統計
			number[k] = (number[k] / 8) * 100;
			System.out.println("level " + k + " is " + number[k] + "%");
		}

		Frame frame = new Frame();//輸出圖形
		frame.setSize(350, 190);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		input.close();
	}

}
