package ce1002.a8.s102502025;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Graph extends JPanel {
	public Graph() {
		setBounds(10, 10, 500, 500);//panel 大小
	}

	public void paint(Graphics g) {
		int Xaxis = 10;//x軸變數
		for (int a = 0; a < 4; a++) {//輸出圖形
			g.setColor(Color.red);//定色
			g.drawRect(Xaxis, 0, 15, 100);//畫長方形
			g.fillRect(Xaxis, 0, 15, 100);//塗色
			g.setColor(Color.gray);
			g.fillRect(Xaxis, 0, 15, 100 - (int) A8.number[a]);
			g.setColor(Color.BLACK);
			g.drawString("Level " + a, Xaxis, 120);//寫字
			g.drawString(String.valueOf(A8.number[a] + "%"), Xaxis, 140);//輸出數字
			Xaxis = Xaxis + 50;
		}
	}
}
