package ce1002.a8.s102502034;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ce1002.a8.s102502034.MyPanel;

public class A8 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		double[] grade = new double[8];
		double level0 = 0, level1 = 0, level2 = 0, level3 = 0;
		System.out.println("Input grades: ");
		/* DATA of test
		  grade[0] = 0;
		grade[1] = 15;
		grade[2] = 90;
		grade[3] = 100;
		grade[4] = 100;
		grade[5] = 60;
		grade[6] = 20;
		grade[7] = 70;*/
		for (int i = 0; i < 8; i++) {
			 grade[i] = scan.nextInt();
			while (grade[i] < 0 || grade[i] > 100) {
				System.out.println("Out of range!");
				grade[i] = scan.nextInt();
			}
			if (grade[i] < 24 && grade[i] >= 0)
				level0++;
			else if (grade[i] >= 24 && grade[i] < 50)
				level1++;
			else if (grade[i] >= 50 && grade[i] < 75)
				level2++;
			else if (grade[i] >= 75 && grade[i] <= 100)
				level3++;
			// input data and count numbers of every level

		}
		//output porpotion of every Level
		System.out.println("Level 0 is " + (level0 / 8) * 100 + "%");
		System.out.println("Level 1 is " + (level1 / 8) * 100 + "%");
		System.out.println("Level 2 is " + (level2 / 8) * 100 + "%");
		System.out.println("Level 3 is " + (level3 / 8) * 100 + "%");

		JFrame f = new JFrame();
		// create a frame
		f.setSize(350, 290);
		// set the size of frame
		MyPanel myPanel = new MyPanel();
		JPanel panel = new JPanel();

		myPanel.setPorpotion0(level0 / 8);
		myPanel.setPorpotion1(level1 / 8);
		myPanel.setPorpotion2(level2 / 8);
		myPanel.setPorpotion3(level3 / 8);
		JLabel label1 = new JLabel("Level0");
		JLabel label2 = new JLabel("Level1");
		JLabel label3 = new JLabel("Level2");
		JLabel label4 = new JLabel("Level3");
		
		JLabel label1_1 = new JLabel( (level0 / 8)* 100 + "%");
		JLabel label2_1 = new JLabel( (level1 / 8)* 100 + "%");
		JLabel label3_1 = new JLabel( (level2 / 8)* 100 + "%");
		JLabel label4_1 = new JLabel( (level3 / 8)* 100 + "%");
		
		
		
		panel.setLayout(new GridLayout(2, 4, 30, 0));
		
		panel.add(label1);
		panel.add(label2);
		panel.add(label3);
		panel.add(label4);
		panel.add(label1_1);
		panel.add(label2_1);
		panel.add(label3_1);
		panel.add(label4_1);
		//add label to panel

		f.add(panel, BorderLayout.SOUTH);
		f.add(myPanel, BorderLayout.CENTER);

		//Set Layout
		f.setVisible(true);

		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		//close

	}

}
