package ce1002.a8.s102502034;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel {
	private double p0, p1, p2, p3;
	MyPanel panel;

	MyPanel() {
		setBounds(0, 0, 250, 250);
		setVisible(true);
	}
//painting
	public void paint(Graphics g) {
		g.setColor(Color.GRAY);//設定顏色
		g.fillRect(5, 10, 20, 200);
		g.fillRect(95, 10, 20, 200);
		g.fillRect(185, 10, 20, 200);
		g.fillRect(275, 10, 20, 200);
		g.setColor(Color.RED);//設定顏色
		g.fillRect(5, 10 + (int) (200 * (1 - getPorpotion0())), 20,
				200 - (int) (200 * (1 - getPorpotion0())));
		g.fillRect(95, 10 + (int) (200 * (1 - getPorpotion1())), 20,
				200 - (int) (200 * (1 - getPorpotion1())));
		g.fillRect(185, 10 + (int) (200 * (1 - getPorpotion2())), 20,
				200 - (int) (200 * (1 - getPorpotion2())));
		g.fillRect(275, 10 + (int) (200 * (1 - getPorpotion3())), 20,
				200 - (int) (200 * (1 - getPorpotion3())));
	}
//set porpotion
	public void setPorpotion0(double p) {
		this.p0 = p;
	}

	public void setPorpotion1(double p) {
		this.p1 = p;
	}

	public void setPorpotion2(double p) {
		this.p2 = p;
	}

	public void setPorpotion3(double p) {
		this.p3 = p;
	}

	public double getPorpotion0() {
		return p0;
	}

	public double getPorpotion1() {
		return p1;
	}

	public double getPorpotion2() {
		return p2;
	}

	public double getPorpotion3() {
		return p3;
	}

}
