package ce1002.a8.s101201005;

import java.util.Scanner;

public class A8 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int n; //grades
		float level[]=new float [4];
		
		System.out.println("Input grades: ");
		for (int i=0;i<8;i++) 
		{
			do
			{
				n=input.nextInt();
				if(n<0||100<n)
				{
					System.out.println("Out of range!");
				}
			}
			while(n<0||100<n);
			//level
			if (n>=0 && n<=25)
				level[0]++;
			else if (n>=26 && n<=50)
				level[1]++;
			else if (n>=51 && n<=75)
				level[2]++;
			else if (n>=76 && n<=100)
				level[3]++;
		}

		for (int j=0;j<4;j++)
		{
			
			level[j]=level[j]*25/2;
			System.out.println("Level " + j + " is " + level[j] + "%");
		}
		MyFrame frame=new MyFrame(level);
	}

}
