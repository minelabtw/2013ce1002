package ce1002.a8.s101201005;

import java.awt.*;
import javax.swing.*;

public class MyPanel extends JPanel{
	
	protected int level=0;
	protected float percentage=0;
	protected JLabel LLabel=new JLabel(); //level
	protected JLabel PLabel =new JLabel(); //percentage
	MyPanel(int l,float p)
	{//input the data
		level=l;
		percentage=p;
		setLayout(null);
		setBounds(40,20,40,100); //大小範圍
		LLabel.setBounds(0,100,40,40); //level大小範圍
		PLabel.setBounds(0,120,40,40); //percentage大小範圍
		LLabel.setText("Level " + level);
		PLabel.setText(percentage + "%");
		add(LLabel);
		add(PLabel);
	}
	
	@Override
	protected void paintComponent(Graphics g) //use the graphics to print the graph
	{//show the graph
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(0, 0, 13, 100);
		g.setColor(Color.gray);
		g.fillRect(0, 0, 12, 100-(int)percentage);
	}
}
