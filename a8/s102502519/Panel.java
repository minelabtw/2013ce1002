package ce1002.a8.s102502519;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
	
	protected int[] num = new int[4];

	Panel(int[] s) {
		for(int i=0;i<4;i++){
			this.num[i] = s[i];
		}
		JLabel[] label = new JLabel[8];    //宣告label陣列儲存
		setLayout(new GridLayout(2,4,-80,-380));
		
		for(int i=0;i<4;i++)
		{
			label[i] = new JLabel("Level " + i);
			add(label[i]);
		}
		
		for(int i=4;i<8;i++)
		{
			label[i] = new JLabel((double)s[i-4]/0.08 + "%");
			add(label[i]);
		}
	}

	public void paintComponent(Graphics g) {    //畫出長方形
		g.setColor(Color.RED);
		g.fillRect(0, 20, 30, 160);
		g.fillRect(100, 20, 30, 160);
		g.fillRect(200, 20, 30, 160);
		g.fillRect(300, 20, 30, 160);
		g.setColor(Color.GRAY);
		g.fillRect(0, 20, 30, 160-160*num[0]/8);
		g.fillRect(100, 20, 30, 160-160*num[1]/8);
		g.fillRect(200, 20, 30, 160-160*num[2]/8);
		g.fillRect(300, 20, 30, 160-160*num[3]/8);

	}

}
