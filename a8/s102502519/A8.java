package ce1002.a8.s102502519;

import java.util.*;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = new int[8];
		int[] str = new int[4];
		Scanner scn = new Scanner(System.in);
		System.out.println("Input grades: ");
		for(int i=0;i<8;i++){
			do{
				arr[i] = scn.nextInt();
				if(arr[i]<0 ||arr[i]>100)
					System.out.println("Out of range!");
			}
			while(arr[i]<0 ||arr[i]>100);    //將符合的值輸入至陣列中
			
			if(0<=arr[i]&&arr[i]<=25)
				++str[0];
			else if(26<=arr[i]&&arr[i]<=50)
				++str[1];
			else if(51<=arr[i]&&arr[i]<=75)
				++str[2];
			else if(76<=arr[i]&&arr[i]<=100)
				++str[3];
		}
		scn.close();
		for(int j=0;j<4;j++){    //輸出結果
			System.out.println("Level " + j + " is " + (double)str[j]*100/8 + "%");
		}	
		new Frame(str);
	}

}
