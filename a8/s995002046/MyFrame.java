package ce1002.a8.s995002046;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

@SuppressWarnings("serial")
class MyFrame extends JFrame{
	List<Integer>grades=new ArrayList<Integer>();
	MyPanel panel;
	int[] lv =  new int[4];
	MyFrame(List<Integer> g){
		grades=g;//save input number
		levelData();
		this.setLayout(null);//set layout
		panel=new MyPanel(grades,lv);//add panel in
		this.add(panel);
		panel.setBounds(0, 0, 350, 300);
		this.setSize(450,350); //set size
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	private void levelData(){//level the input number 
		for(Integer g:grades){
			if(g>=0 && g<=25){
				lv[0]++;
			}
			else if(g>=26 && g<=50){
				lv[1]++;
			}
			else if(g>=51 && g<=75){
				lv[2]++;
			}
			else{
				lv[3]++;
			}
			
		}
		//output % of levels
		System.out.println("Level 0 is "+lv[0]*(float)100/8+" %");
		System.out.println("Level 1 is "+lv[1]*(float)100/8+" %");
		System.out.println("Level 2 is "+lv[2]*(float)100/8+" %");
		System.out.println("Level 3 is "+lv[3]*(float)100/8+" %");
	}
}