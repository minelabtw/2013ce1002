package ce1002.a8.s995002046;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

@SuppressWarnings("serial")
class MyPanel extends JPanel{
	List<Integer>grades=new ArrayList<Integer>();
	int[] lv =  new int[4];
	MyPanel(List<Integer> g, int[] lv){
		grades=g;//save input number
		this.lv=lv;//save number in every level
		this.setVisible(true);
	}
	 @Override public void paintComponent(Graphics g) {//draw chart of levels
		 for(int j=0;j<lv.length;j++){
				g.setColor(Color.gray);
				g.fillRect(30+70*j, 50, 20, 160-lv[j]*20);
				g.setColor(Color.red);   
				g.fillRect(30+70*j, 50+160-lv[j]*20, 20, lv[j]*20);
				g.setColor(Color.gray);
				g.drawString("Level "+j, 15+70*j, 225);
				g.drawString(""+(lv[j]*(float)100/8)+"%", 15+70*j, 240);
			}
	        
	}
	
}