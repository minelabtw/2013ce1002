package ce1002.a8.s102502028;
import javax.swing.* ;
import java.awt.* ;
import java.util.Scanner ;
public class A8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int input = 0 ; 
		double[] level = new double [4] ; //存放4個等級的數量並初始為0
		for (int a = 0 ; a < 4 ; a++)
		{
			level[a] = 0 ;
		}
		      
		int a = 0 ;
        Scanner cin = new Scanner(System.in) ;
		System.out.println("Input grades: ") ; 
        do //輸入8個成績
        {      	       
            input = cin.nextInt();
            
            while (input < 0 || input > 100) //判斷是否超出合理範圍
            {
            	System.out.println("Out of range!") ;
            	input = cin.nextInt() ;
            }
            
            if (input >= 0 && input <= 25)   //存入陣列	
            	level[0]++ ;
            else if (input >= 26 && input <= 50)
            	level[1]++ ;
            else if (input >= 51 && input <= 75)
            	level[2]++ ;
            else 
            	level[3]++ ;
            a++ ;
        }while(a < 8) ;
        
        JFrame frame = new JFrame() ; //設定框架
        frame.setSize(480,350);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null) ;
        
        double result = 0 ;
        for (int i = 0 ; i < 4 ; i++) //輸出結果
        {
        	result = level[i]/8 ;
        	System.out.println("Level "+i+" is "+ result*100+"%") ;
        	
        	MyPanel panel = new MyPanel() ;
        	panel.setResult(result);
        	panel.setBounds(10+100*i,10,80,350) ;
        	
        	JLabel label = new JLabel("Level "+i) ;
        	label.setBounds(6,200,80,15) ;
        	panel.add(label) ;
        	
        	JLabel label2 = new JLabel(""+result*100+"%") ;
        	label2.setBounds(6,220,80,15) ;
        	panel.add(label2) ;
        	frame.add(panel) ;
        }
        
        frame.setVisible(true);       
	}
        
}
       