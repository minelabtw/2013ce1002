package ce1002.a8.s102502028;
import javax.swing.* ;
import java.awt.Color;
import java.awt.Graphics;

public class MyPanel extends JPanel{ 
	public double result;   //宣告
	public double portion;
	
	MyPanel() //建構子
	{
		setLayout(null) ;
	}
		
  
     public void setResult(double result) //傳值
     {
    	 this.result = result ;
    	 portion = 180 - (result*180) ;
    	 
     }
     protected void paintComponent(Graphics g)  //畫圖
     {
    	super.paintComponent(g) ;  	    
		g.setColor(Color.RED) ;   //比例	    
		g.fillRect(5,5,20,180);	    
		g.setColor(Color.LIGHT_GRAY);	 //底
		g.fillRect(5,5,20,(int)portion); 
		g.setColor(Color.BLACK);  //長方形的框
		g.drawRect(5,5,20,180);
     }
     
}
