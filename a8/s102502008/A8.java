package ce1002.a8.s102502008;
import java.util.Scanner ;

import javax.swing.JFrame;
public class A8
{
	public static void main(String[] args) 
	{
		Scanner input= new Scanner(System.in);
		int number ;
		//they are sum
		int level0= 0 ;
		int level1= 0 ;
		int level2= 0;
		int level3= 0;
		
		System.out.println("Input grades: ") ;
		//for scan the grade
		for(int i= 0; i< 8; i++)
		{
			
			do 
			{
				number= input.nextInt();
				if(number<0 || number >100)
					System.out.println("Out of range!");
			}while(number<0 || number >100);
			if(number>= 0&& number<=25)
				level0++ ;
			else if (number>= 26&& number<=50)
				level1++ ;
			else if (number>= 51&& number<=75)
				level2++ ;
			else
				level3++ ;
		}
		//output the percent of level
		System.out.println("Level 0 is "+level0/8.0*100+"%");
		System.out.println("Level 1 is "+level1/8.0*100+"%");
		System.out.println("Level 2 is "+level2/8.0*100+"%");
		System.out.println("Level 3 is "+level3/8.0*100+"%");
		
	
		//make the GUI
		MyFrame frame= new MyFrame(level0/8.0*100,level1/8.0*100,level2/8.0*100,level3/8.0*100);
		frame.setSize(300,200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
