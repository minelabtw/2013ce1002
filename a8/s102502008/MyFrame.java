package ce1002.a8.s102502008;
import javax.swing.JFrame ;
public class MyFrame extends JFrame
{
	public MyFrame(double percent0,double percent1,double percent2,double percent3)
	{
		setLayout(null) ;
		Level level0= new Level(percent0,0);
		Level level1= new Level(percent1,1);
		Level level2= new Level(percent2,2);
		Level level3= new Level(percent3,3);
		//set localtion
		level0.setBounds(20,20,50,180);	
		level1.setBounds(70,20,50,180);
		level2.setBounds(120,20,50,180);
		level3.setBounds(170,20,50,180);
		//add
		add(level0);
		add(level1);
		add(level2);
		add(level3);
	}
}
