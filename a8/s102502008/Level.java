package ce1002.a8.s102502008;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics ;

import javax.swing.JLabel ;
public class Level extends JPanel 
{
	public Level(double percent,int level)
	{
		setLayout(null);
		setPercent(percent) ;
		setLevel(level) ;
		add(Jlevel);
		add(Jpercent) ;
	}
	
	private JLabel Jlevel ;
	private JLabel Jpercent ;
	private double percent;
	private int level ;
	protected void setLevel(int level)
	{
		this.Jlevel= new JLabel("Level "+level) ;
		this.level=level ;
	}
	protected void setPercent(double percent)
	{
		this.Jpercent= new JLabel(""+percent+"%") ;
		this.percent= percent ;
	}
	public double getPercent()
	{
		return percent ;
	}
	public int getLevel()
	{
		return level ;
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.fillRect(10, 10, 10, 100);
		g.setColor(Color.GRAY);
		g.fillRect(10,10,10,100-(int)getPercent());
		Jlevel.setBounds(10, 115,50,10);
		Jpercent.setBounds(10,130,50,10) ;
		
	}
}
