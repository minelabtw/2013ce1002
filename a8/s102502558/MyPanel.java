package ce1002.a8.s102502558;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class MyPanel extends JPanel
{
	private float[] level = new float[4];
	MyPanel()
	{
		
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
        for(int i=0;i<4;i++)
        {
        	g.setColor(Color.black);
        	g.drawString("Level " + i, 10 + 50 * i, 120);
        	g.drawString(level[i] * 100 + "%", 10 + 50 * i, 135);
        	g.fillRect(10 + 50 * i, 10, 20, 100);
        	g.setColor(Color.red);
        	g.fillRect(10 + 50 * i, 10 + (int)(100 * (1 - level[i])), 20, (int)(100 * level[i]));
        	
        	//System.out.println("asdf" + (level[i]));
        }
    }
	
	public void addGrade(int id, float f)
	{
		level[id] = f;
	}
}
