package ce1002.a8.s102502558;

import java.util.Scanner;

import javax.swing.JFrame;

public class A8 extends JFrame
{
	private MyPanel panel;
	A8()
	{
		setSize(300, 200);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panel = new MyPanel();
		setContentPane(panel);
	}
	
	public void addGrade(int id, float f)
	{
		panel.addGrade(id, f);
	}
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		A8 a8 = new A8();
		float[] level = new float[4];
		int grade = 0;
		System.out.println("Input grades: ");
		for (int i=0;i<8;i++)
		{
			grade = input.nextInt();
			while (grade < 0 || grade > 100)
			{
				System.out.println("Out of range!");
				grade = input.nextInt();
			}
			level[grade / 25 + (grade % 25 == 0 && grade > 0 ? -1 : 0)]++;
		}
		for (int i=0;i<4;i++)
		{
			System.out.println("Level " + i + " is " + level[i] / 8 * 100 + "%");
			a8.addGrade(i, level[i] / 8);
		}
		input.close();
		a8.setVisible(true);
	}

}
