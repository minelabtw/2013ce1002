package ce1002.e4.s102502527;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Dog dog = new Dog();//叫出函示
		Cat cat = new Cat();

		System.out.println("Input Dog's sound:");
		dog.setSound(input.next());//將叫聲直接輸入setSound函示
		System.out.println("Input Cat's sound:");
		cat.setSound(input.next());//將叫聲直接輸入setSound函示
		
		System.out.println(dog.getType()+": "+dog.getSound());//輸出字彙
		System.out.println(cat.getType()+": "+cat.getSound());
		
		input.close();
	}

}
