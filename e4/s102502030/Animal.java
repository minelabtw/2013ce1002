package ce1002.e4.s102502030;

public class Animal {
	private String type;
	private String sound;
	
	public Animal ()
	{
		
	}
	
	public void setType ( String x )
	{
		type = x;
	}
	
	public void setSound ( String x )
	{
		sound = x;
	}
	
	public String getType ()
	{
		return type;
	}
	
	public String getSound ()
	{
		return sound;
	}
}
