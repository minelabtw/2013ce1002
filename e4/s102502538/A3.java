package ce1002.a3.s102502538;
import java.util.Scanner;
public class A3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner( System.in );
		int number;
		float maxspeed;
		float[] arr;
		System.out.println("Please input the number of cars: ");
		number = input.nextInt();
		while(number<0){//判定是否在正確範圍內
			System.out.println("Out of range!");
			System.out.println("Please input the number of cars: ");
			number = input.nextInt();
		}
		arr = new float [number];
		for(int i=0;i<number;i++){
			System.out.println("Please input the max speed of this car("+i+"):");
			maxspeed= input.nextFloat();
			while(maxspeed<=0){//判定是否在正確範圍內
				System.out.println("Out of range!");
				System.out.println("Please input the max speed of this car("+i+"):");
				maxspeed = input.nextFloat();
			}
			arr[i] = maxspeed;//將maxspeed的值存入arr中
		}
		for(int i=0;i<number;i++){		
			Car car = new Car();//導入class  		
			System.out.println("Car id is "+car.id(i)+".");
		    car.maxspeed(arr[i]);
		    System.out.println("Car max speed is "+car.maxspeed(arr[i])+".");
		    car.turbo();
		 	System.out.println("Car turbo is "+car.turbo()+" .");
		}
	}
}
