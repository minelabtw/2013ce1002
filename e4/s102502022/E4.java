package ce1002.e4.s102502022;
import java.util.Scanner;
public class E4 {

	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("Input Dog's sound:");
		Scanner scn=new Scanner(System.in);
		String s1= scn.next();      //declare string variables
		System.out.println("Input Cat's sound:");
        String s2= scn.next();
        Dog dogs= new Dog();     //build up class variables
        System.out.print(dogs.getType()+": ");
        dogs.setSound(s1);     //input the variables into the function
        System.out.println(dogs.getSound());
        Cat cats= new Cat();
        System.out.print(cats.getType()+": ");
        cats.setSound(s2);
        System.out.println(cats.getSound());
        scn.close();
	}

}
