package ce1002.e4.s102502022;

public class Animal 
{
	private String type;	//type of animal
	private String sound;	//sound of animal

	public Animal()
	{
		
	}
	public void setType(String type)
	{
		this.type=type;        
	}
	public String getType()
	{
		return type;
	}
	public void setSound(String sound)
	{
		this.sound=sound;
	}
	public String getSound()
	{
		return sound;
	}

}
