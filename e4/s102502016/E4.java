package ce1002.e4.s102502016;
import java.util.Scanner;
import ce1002.e4.s102502016.Cat;
import ce1002.e4.s102502016.Dog;
public class E4 {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		Dog dog = new Dog("Dog");//清空
		Cat cat = new Cat("Cat");
		System.out.println("Input Dog's sound: ");
		dog.setSound(input.next());//直接在()中輸入
		System.out.println("Input Cat's sound: ");
		cat.setSound(input.next());
		System.out.println(dog.getType()+": "+dog.getSound());
		System.out.println(cat.getType()+": "+cat.getSound());
		input.close();
	}

}
