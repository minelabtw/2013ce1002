package ce1002.e4.s102502016;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void setType(String name){
		type = name;
	}
	public void setSound(String voice){
		sound = voice;
	}
	public String getType(){
		return type;
	}
	public String getSound(){
		return sound;
	}
}
