package ce1002.e4.s102502048;

public class Animal 
{
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal()//建構子
	{
		
	}
	//設定
	public void setType(String type)
	{
		this.type=type;
	}
	public void setSound(String sound)
	{
		this.sound=sound;
	}
	//取得
	public String getType() 
	{
		return type;
	}
	public String getSound() 
	{
		return sound;
	}
}
