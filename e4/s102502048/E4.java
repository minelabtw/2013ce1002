package ce1002.e4.s102502048;
import java.util.Scanner;
import java.io.*;
public class E4 
{
	public static void main(String[] args) throws IOException
	{
		Scanner input = new Scanner(System.in);
		BufferedReader buf;
		buf=new BufferedReader(new InputStreamReader(System.in));
		Dog _dog = new Dog();
		Cat _cat = new Cat();
		
		System.out.println("Input Dog's sound: ");//顯示
			String dog =  buf.readLine();//輸入
			_dog.setSound(dog);
			
		System.out.println("Input Cat's sound: ");//顯示
			String cat =  buf.readLine();//輸入
			_cat.setSound(cat);
		//顯示數據
		System.out.println(_dog.getType()+": "+_dog.getSound());
		System.out.println(_cat.getType()+": "+_cat.getSound());
			
	}

}
