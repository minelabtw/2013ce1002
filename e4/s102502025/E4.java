package ce1002.e4.s102502025;

import java.util.Scanner;		//宣告指令

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);		//開啟指令

		System.out.println("Input Dog's sound: ");
		Dog Dog = new Dog();		//宣告dog.class
		Dog.setSound(scanner.next());		//輸入叫聲

		System.out.println("Input Cat's sound: ");
		Cat Cat = new Cat();		//宣告cat.class
		Cat.setSound(scanner.next());		//輸入叫聲

		System.out.println(Dog.getType() + ": " + Dog.getSound());		//輸出
		System.out.println(Cat.getType() + ": " + Cat.getSound());
		scanner.close();
	}

}
