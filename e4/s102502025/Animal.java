package ce1002.e4.s102502025;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public String getType() {		//輸出名稱
		return type;
	}

	public void setType(String type) {		//輸入名稱
		this.type = type;
	}

	public String getSound() {		//輸出叫聲
		return sound;
	}

	public void setSound(String sound) {		//輸入叫聲
		this.sound = sound;
	}
}
