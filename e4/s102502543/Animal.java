package ce1002.e4.s102502543;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	public void setType(String newType){
		type = newType;
	}
	public void setSound(String newSound){
		sound = newSound;
	}
	public String getType(){
		return type;
	}
	public String getSound(){
		return sound;
	}
}
