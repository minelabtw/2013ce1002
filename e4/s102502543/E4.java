package ce1002.e4.s102502543;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();
		Cat cat = new Cat();
		System.out.println("Input "+dog.getType()+"'s sound: ");  //輸入
		dog.setSound(input.nextLine());
		System.out.println("Input "+cat.getType()+"'s sound: ");
		cat.setSound(input.nextLine()); 
		System.out.println(dog.getType()+": "+dog.getSound()); //輸出
		System.out.println(cat.getType()+": "+cat.getSound());
	}

}
