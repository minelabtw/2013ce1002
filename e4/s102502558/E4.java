package ce1002.e4.s102502558;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();
		Cat cat = new Cat();
		
		System.out.println("Input Dog's sound: ");
		dog.setSound(input.next());
		System.out.println("Input Cat's sound: ");
		cat.setSound(input.next());
		System.out.println(dog.getType()+": "+dog.getSound());
		System.out.println(cat.getType()+": "+cat.getSound());
		input.close();
	}

}
