package ce1002.e4.s101201508;

public class Animal {
	private String type;
	private String sound;
	public void set_type(String in_type)//store
	{
		type=in_type;
	}
	public void set_sound(String in_sound)//store
	{
		sound=in_sound;
	}
	public String get_type()//out
	{
		return type;
	}
	public String get_sound()//out
	{
		return sound;
	}
}
