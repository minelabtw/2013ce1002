package ce1002.e4.s101201508;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		String dog_sound;//Temporary storage data
		String cat_sound;
		Dog dog=new Dog("Dog");//new class
		System.out.println("Input Dog's sound: ");
		dog_sound=sc.next();
		dog.set_sound(dog_sound);//store
		Cat cat=new Cat("Cat");//new class
		System.out.println("Input Cat's sound: ");
		cat_sound=sc.next();
		cat.set_sound(cat_sound);//store
		System.out.println(dog.get_type()+": "+dog.get_sound());//out printer
		System.out.println(cat.get_type()+": "+cat.get_sound());
	}

}
