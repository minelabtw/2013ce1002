package ce1002.e4.s102502019;
import java.util.Scanner;
public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.print("Input Dog's sound: \n");            //輸出
		Scanner input = new Scanner(System.in);
		String dogsound = input.nextLine();                   //存入名稱
		System.out.print("Input Cat's sound: \n");
		String catsound = input.nextLine();                   //存入名稱
		Dog t1 = new Dog();                                   //建立新物件
		t1.setsound(dogsound);                                //傳入值
		Cat t2 = new Cat();                                   //建立新物件
		t2.setsound(catsound);                                //傳入值
		System.out.print(t1.gettype()+": "+t1.getsound());
		System.out.print("\n"+t2.gettype()+": "+t2.getsound());
		input.close();
	}

}
