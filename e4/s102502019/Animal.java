package ce1002.e4.s102502019;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	public void setsound(String a)    //設定聲音
	{
		sound = a;
	}
	public String getsound()         //取得聲音
	{
		return sound;
	}
	public void settype(String b)    //設定種類
	{
		type = b;
	}
	public String gettype()         //取得種類
	{
		return type;
	}
	

}
