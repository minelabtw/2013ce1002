package ce1002.e4.s102502562;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	public void setType(String Type)//設定種類
	{
		type=Type;
	}
	public void setSound(String Sound)//設定叫聲
	{
		sound=Sound;
	}
	public String getType()//取得種類
	{
		return type;
	}
	public String getSound()//取得叫聲
	{
		return sound;
	}
}
