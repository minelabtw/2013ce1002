package ce1002.e4.s102502562;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		String sound;
		Dog dog=new Dog();//產生新的Class object
		Cat cat=new Cat();//產生新的Class object
		Scanner input=new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		sound=input.next();
		dog.setSound(sound);//設定聲音
		System.out.println("Input Cat's sound: ");
		sound=input.next();
		cat.setSound(sound);//設定聲音
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
	}

}
