package ce1002.e4.s102502552;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String sound;//宣告輸入用的叫聲變數
		
		Dog dog = new Dog();//建立狗的物件
		System.out.println("Input " + dog.getType() + "'s sound: ");
		sound = sc.next();
		dog.setSounds(sound);//設定狗的叫聲
		
		Cat cat = new Cat();//建立貓的物件
		System.out.println("Input " + cat.getType() + "'s sound: ");
		sound = sc.next();
		cat.setSounds(sound);//設定貓的叫聲
		
		System.out.println(dog.getType() + ": " + dog.getSounds());
		System.out.println(cat.getType() + ": " + cat.getSounds());//將兩隻動物的參數直接顯示出來
		
	sc.close();	
	}

}
