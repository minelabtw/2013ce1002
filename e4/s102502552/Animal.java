package ce1002.e4.s102502552;

public class Animal {
	private String type;//種類
	private String sounds;//叫聲
	
	public Animal()
	{
	}//建構子
	
	public void setType(String type)
	{
		this.type = type;
	}//設定名稱函式
	
	public void setSounds(String sounds)
	{
		this.sounds = sounds;
	}//設定叫聲函式
	
	public String getType()
	{
		return type;
	}//取得名稱函式
	
	public String getSounds()
	{
		return sounds;
	}//取得叫聲函式

}
