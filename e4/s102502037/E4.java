package ce1002.e4.s102502037;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		String dog = input.next();
		System.out.println("Input Cat's sound: ");
		String cat = input.next();
		Cat cat1 = new Cat();
		Dog dog1 = new Dog();
		cat1.setSound(cat);
		dog1.setSound(dog);
		System.out.println(dog1.getType()+": "+dog1.getSound());
		System.out.println(cat1.getType()+": "+cat1.getSound());
	}

}
