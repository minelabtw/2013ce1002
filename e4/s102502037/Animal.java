package ce1002.e4.s102502037;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲

	public void setType(String type)//設定ID
	{
		this.type=type;
	}
	public void setSound(String sound)//設定ID
	{
		this.sound=sound;
	}
	public String getType()//回傳ID
	{
		return type;
	}
	public String getSound()//回傳ID
	{
		return sound;
	}
}
