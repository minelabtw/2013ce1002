package ce1002.e4.s102502003;
import java.util.Scanner;;

public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scanner = new Scanner(System.in);
		String catsound;
		String dogsound;
		Cat c = new Cat();
		Dog d = new Dog();
		String animal1=d.getType();  //set type
		String animal2=c.getType();  //set type
		
		System.out.println("Input "+animal1+"'s sound: ");
		dogsound=scanner.next();
		d.setSound(dogsound);  //set dog's sound
		System.out.println("Input "+animal2+"'s sound: ");
		catsound=scanner.next();
		c.setSound(catsound);  //set sound
		System.out.println(animal1+": "+dogsound);  //print result
		System.out.println(animal2+": "+catsound);
		
		scanner.close();

	}

}
