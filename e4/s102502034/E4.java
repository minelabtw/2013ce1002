package ce1002.e4.s102502034;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String DogSounds, CatSounds;
		// 設定變數 叫出scanner class並創立物件(scan)
		System.out.println("Input Dog's sound: ");
		DogSounds = scan.next();
		Dog dog = new Dog("Dog");
		dog.set_sound(DogSounds);
		// 設定狗的聲音

		System.out.println("Input Cat's sound: ");
		CatSounds = scan.next();
		Cat cat = new Cat("Cat");
		cat.set_sound(CatSounds);
		// 設定貓的聲音
		System.out.println(dog.get_type() + ": " + dog.get_sound());
		System.out.println(cat.get_type() + ": " + cat.get_sound());
		// 輸出題目所需

	}

}
