package ce1002.e4.s102502034;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲
	// 設定種類 聲音

	public void set_type(String type) {
		this.type = type;

	}

	public void set_sound(String sound) {
		this.sound = sound;

	}

	// 傳回種類 聲音
	public String get_type() {
		return type;

	}

	public String get_sound() {
		return sound;

	}

}
