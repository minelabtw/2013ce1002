package ce1002.e4.s102502043;
import java.util.Scanner;
public class E4
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);     
		System.out.println("Input Dog's sound: ");
		String s1 = input.next(); 						//輸入.存取字串
		System.out.println("Input Cat's sound: ");
		String s2 = input.next();
		Dog d = new Dog();								//宣告物件
		Cat c = new Cat();
		d.setsound(s1);									//將字串輸入member function
		System.out.println(d.gettype()+": "+d.getsound());
		c.setsound(s2);
		System.out.println(c.gettype()+": "+c.getsound());
		input.close();
	}

}
