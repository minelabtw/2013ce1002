package ce1002.e4.s102502043;

public class Animal 
{
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	Animal()
	{
		
	}
	
	void settype(String itype)
	{
		this.type = itype ; 				//存入type
	}
	void setsound(String isound)
	{
		this.sound = isound ;				//存入sound
	}
	String gettype()
	{
		return type;						//回傳type
	}
	String getsound()
	{
		return sound;						//回傳sound
	}
}
