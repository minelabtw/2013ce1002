package ce1002.a4.s995002014;

import java.util.Scanner;

public class A4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String cats,dogs; //String for 貓狗的叫聲
		
		// 創建貓狗各一隻
		Dog mydog = new Dog();
		Cat mycat = new Cat();
		
		//使用者出入，使用貓狗繼承來的方法 setSound
		System.out.println("Input Dog's sound:");
		dogs=input.next();
		mydog.setSound(dogs);
		System.out.println("Input Cat's sound:");
		cats=input.next();
		mycat.setSound(cats);
		
		//output 動物叫聲
		System.out.println("Dog: "+mydog.getSound());
		System.out.println("Cat: "+mycat.getSound());
		
		input.close();//close scanner 養成好習慣
	}
}
