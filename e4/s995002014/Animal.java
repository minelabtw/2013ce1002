package ce1002.a4.s995002014;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	//methods
	protected void setType(String x) {
		this.type=x;
	}
	protected void setSound(String x) {
		this.sound=x;
	}
	protected String getType() {
		return type;
	}
	protected String getSound(){
		return sound;
	}
}
