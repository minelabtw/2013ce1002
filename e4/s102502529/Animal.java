package ce1002.e4.s102502529;
public class Animal {
		private String type; // 動物種類
		private String sound; // 動物叫聲

		public void SetType(String type) {					//設定
			this.type= type;
		}

		public String GetType() {							//輸出
			return type;
		}

		public void SetSound(String sound) {			//設定		
			this.sound = sound;
		}

		public String GetSound() {					//輸出
			return sound;
		}
	}
