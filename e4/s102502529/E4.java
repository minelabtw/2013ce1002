package ce1002.e4.s102502529;
import java.util.Scanner;		
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in);		

		System.out.println("Input Dog's sound: ");
		Dog Dog = new Dog();					//宣告
		Dog.SetSound(s.next());					//輸入

		System.out.println("Input Cat's sound: ");
		Cat Cat = new Cat();					//宣告
		Cat.SetSound(s.next());					//輸入

		System.out.println(Dog.GetType() + ": " + Dog.GetSound());		//輸出
		System.out.println(Cat.GetType() + ": " + Cat.GetSound());
		s.close();
	}

}