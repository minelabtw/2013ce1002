package ce1002.e4.s102502020;

public class Animal {

	private String type;	  //動物種類
	private String sound;	  //動物叫聲
	
	public Animal() {	
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
}
