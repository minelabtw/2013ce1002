package ce1002.e4.s102502510;
import java.util.Scanner;
public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);//create a new Scanner
		Dog d=new Dog();
		Cat c=new Cat();
		String AnimalSound;
		System.out.println("Input Dog's sound: ");
		AnimalSound=input.nextLine();
		d.setSound(AnimalSound);
		System.out.println("Input Cat's sound: ");
		AnimalSound=input.nextLine();
		c.setSound(AnimalSound);
		input.close();//close the Scanner
		System.out.println(d.getType()+": "+d.getSound());
		System.out.println(c.getType()+": "+c.getSound());
	}

}
