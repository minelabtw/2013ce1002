package ce1002.e4.s102502501;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		String dogsound; //宣告字串
		String catsound;
		Dog dog = new Dog(); //將class宣告成物件
		Cat cat = new Cat();
		System.out.println("Input "+dog.getType()+"'s sound: ");
		dogsound = input.nextLine(); //輸入字串
		System.out.println("Input "+cat.getType()+"'s sound: ");
		catsound = input.nextLine();
		input.close();
		
		dog.setSound (dogsound); //設定聲音
		cat.setSound (catsound);
		System.out.println("Dog: "+dog.getSound());
		System.out.println("Cat: "+cat.getSound());
		
	}
	
}
