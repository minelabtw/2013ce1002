package ce1002.e4.s102502519;

import java.util.*;

public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name1,name2;    //宣告2個字串
		
		System.out.println("Input Dog's sound: ");
		Scanner input = new Scanner(System.in);
		name1 = input.nextLine();
		
		System.out.println("Input Cat's sound: ");
		name2 = input.nextLine();
		
		input.close();
		
		Dog dog = new Dog();    //宣告dog當物件
		dog.setSound(name1);
		System.out.println(dog.getType() + ": " + dog.getSound());
		
		Cat cat = new Cat();    //宣告cat當物件
		cat.setSound(name2);
		System.out.println(cat.getType() + ": " + cat.getSound());
	}

}
