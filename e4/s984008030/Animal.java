package ce1002.e4.s984008030;

public class Animal {
	
	private String type;// 動物種類
	private String sound;// 動物叫聲
	
	public void setType(String _type) {// 設定動物種類
		this.type = _type;
	}
	
	public void setSound(String _sound) {// 設定動物叫聲
		this.sound = _sound;
	}

	public String getType() {// 取得動物種類
		return this.type;
	}
	
	public String getSound() {// 取得動物叫聲
		return this.sound;
	}
	
}
