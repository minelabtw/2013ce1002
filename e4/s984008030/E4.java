package ce1002.e4.s984008030;
import ce1002.e4.s984008030.Cat;
import ce1002.e4.s984008030.Dog;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Dog dog = new Dog();// 宣告Dog形態的狗並初始化
		Cat cat = new Cat();// 宣告Cat形態的貓並初始化
		Scanner scanner = new Scanner(System.in);//處理輸入的scanner 
		System.out.println("Input Dog's sound: ");
		dog.setSound(scanner.nextLine());
		System.out.println("Input Cat's sound: ");
		cat.setSound(scanner.nextLine());
		scanner.close();
		System.out.println("Dog: " + dog.getSound());
		System.out.println("Cat: " + cat.getSound());
	}

}
