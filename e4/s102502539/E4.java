package ce1002.e4.s102502539;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();
		Cat cat = new Cat();
		
		System.out.println("Input Dog's sound: ");
		dog.setSound( input.next() );
		System.out.println("Input Cat's sound: ");
		cat.setSound( input.next() );
		
		System.out.println("Dog: " + dog.getsSound() );
		System.out.println("Cat: " + cat.getsSound() );

	}

}
