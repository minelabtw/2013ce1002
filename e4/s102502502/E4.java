package ce1002.e4.s102502502;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Dog d = new Dog();                                       // call Class Dog
		System.out.println("Input " + d.getType() +"'s sound: ");
		String dsd = input.next();                               // input dog's sound
		d.setSound(dsd);                                         // send dsd to setSound as sound
		Cat c = new Cat();                                       // call Class Cat
		System.out.println("Input " + c.getType() +"'s sound: ");
		String csd = input.next();                               // input cat's sound
		c.setSound(csd);                                         // send csd to setSound as sound
		System.out.println("Dog: " + d.getSound());
		System.out.println("Cat: " + c.getSound());
	}

}
