package ce1002.e4.s102502502;

public class Animal {
	private String type;	         // type of animal
	private String sound;	         // sound of animal
	public Animal() {                // the constructor of Animal
		}
	// set the type, sound of Animal
	public void setType(String type) {
		this.type = type;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
	// get the type, sound of Animal
	public String getType() {
		return type;
	}
	public String getSound() {
		return sound;
	}	
}
