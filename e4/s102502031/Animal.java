package ce1002.e4.s102502031;

public class Animal {
	private String type; // animal name
	private String sound; // animal sound set by user

	public Animal(){
	}
	
	public void setType(String newType) {
		type = newType;
	}

	public void setSound(String newSound) {
		sound = newSound;
	}

	public String getType() {
		return type;
	}

	public String getSound() {
		return sound;
	}
}