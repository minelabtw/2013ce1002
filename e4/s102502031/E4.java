package ce1002.e4.s102502031;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Dog dog = new Dog();
		Cat cat = new Cat();
		Scanner input = new Scanner(System.in);
		prompt(dog, input);
		prompt(cat, input);
		input.close();
		printResult(dog);
		printResult(cat);
	}

	public static void prompt(Dog name, Scanner jin) {
		System.out.println("Input " + name.getType() + "'s sound: ");
		name.setSound(jin.nextLine());
	} // prompt for dog's sound

	public static void prompt(Cat name, Scanner jin) {
		System.out.println("Input " + name.getType() + "'s sound: ");
		name.setSound(jin.nextLine());
	} // prompt for cat's sound

	public static void printResult(Dog dogName) {
		System.out.println(dogName.getType() + ": " + dogName.getSound());
	} // print result

	public static void printResult(Cat catName) {
		System.out.println(catName.getType() + ": " + catName.getSound());
	} // print result
}
