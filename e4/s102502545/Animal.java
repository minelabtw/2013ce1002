package ce1002.e4.s102502545;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public Animal() 
	{

	}

	public Animal(String type, String sound) 
	{
		this.type = type;
		this.sound = sound;
	}

	public void settype(String type) 
	{
		this.type = type;
	}

	public void gettype() 
	{
		System.out.println("Input " + type + "'s sound:");
	}

	public void setsound(String sound) 
	{
		this.sound = sound;
	}

	public void getsound() 
	{
		System.out.println(type + ": " + sound);
	}
}
