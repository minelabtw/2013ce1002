package ce1002.e4.s102502010;

public class Animal {
	private String type;	
	private String sound;
	
	Animal()
	{
		type="";
		sound="";
	}
	
	public void settype(String type)
	{
		this.type=type;
	}
	
	public void setsound(String sound)
	{
		this.sound=sound;
	}
	
	public String gettype()
	{
		return type;
	}
	
	public String getsound()
	{
		return sound;
	}
	
}
