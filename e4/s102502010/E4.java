package ce1002.e4.s102502010;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		String dog,cat;
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound:");
		dog = input.next();
		Dog dogs = new Dog();
		dogs.setsound(dog);  //setsound
		System.out.println("Input Cat's sound:");
		cat = input.next();
		Cat cats = new Cat();
		cats.setsound(cat);  //setsound
		System.out.println(dogs.gettype() + ": " + dogs.getsound());
		System.out.println(cats.gettype() + ": " + cats.getsound());
		input.close();
	}
}
