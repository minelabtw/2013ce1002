package ce1002.e4.s102502012;

public class Animal {
	private String type;
	private String sound;
	
	public void setType(String type){
		this.type = type;
	}
	
	public void setSound(String sound){
		this.sound = sound;
	}
	
	public String getType(){
		return type;
	}
	
	public String getSound(){
		return sound;
	}
}
