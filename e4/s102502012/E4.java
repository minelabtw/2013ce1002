package ce1002.e4.s102502012;
import java.util.*;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();
		Cat cat = new Cat();
		String sound;
		System.out.println("Input Dog's sound: ");
		sound = input.nextLine();
		dog.setSound(sound);
		System.out.println("Input Cat's sound: ");
		sound = input.nextLine();
		cat.setSound(sound);
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		input.close();
	}

}
