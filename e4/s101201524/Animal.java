package ce1002.e4.s101201524;

public class Animal {
	private String type;
	private String sound;
	
	public Animal(){
	}
	//set animal'stype
	public void setType(String type){
		this.type = type;
	}
	//set animal's sound
	public void setSound(String sound){
		this.sound = sound;
	}
	//get animal's type
	public String getType(){
		return type;
	}
	//get animals'sound
	public String getSound(){
		return sound;
	}
}
