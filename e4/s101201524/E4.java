package ce1002.e4.s101201524;
import java.util.Scanner;
public class E4 {
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		//declare a dog
		Dog dog = new Dog();
		System.out.println("Input Dog's sound: ");
		dog.setSound(input.next()); //set dog's sound
		//declare a cat
		Cat cat = new Cat();
		System.out.println("Input Cat's sound: ");
		cat.setSound(input.next()); //set cat's sound
		//output dog's and cat's sound
		System.out.println("Dog: " + dog.getSound());
		System.out.print("Cat: " + cat.getSound());
		input.close();
	}
}
