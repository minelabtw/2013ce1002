package ce1002.e4.s102502555;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();  //狗
		Cat cat = new Cat();  //貓
		
		//設定叫聲
		System.out.println("Input " + dog.getType() + "'s sound:");
		dog.setSound(input.next());
		System.out.println("Input " + cat.getType() + "'s sound:");
		cat.setSound(input.next());
		
		//叫出來
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		
		input.close();
	}

}
