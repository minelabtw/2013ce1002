package ce1002.e4.s102502555;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	Animal(){
		
	}
	
	//設定種類
	public void setType(String type){
		this.type = type;
	}
	
	//設定聲音
	public void setSound(String sound){
		this.sound = sound;
	}
	
	//得到種類
	public String getType(){
		return type;
	}
	
	//得到聲音
	public String getSound(){
		return sound;
	}
	
}
