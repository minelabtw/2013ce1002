package ce1002.e4.s102502528;

public class Animal {
	private String type;	//type
	private String sound;	//sound
	public void setType(String type)
	{
		this.type = type;
	}
	public void setSound(String sound)
	{
		this.sound = sound;
	}
	public String getType()
	{
		return type;
	}
	public String getSound()
	{
		return sound;
	}

}
