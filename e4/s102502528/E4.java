package ce1002.e4.s102502528;
import java.util.Scanner;

public class E4 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();
		Cat cat = new Cat();
		System.out.println("Input " + dog.getType() + "'s sound:"); //getType 
		dog.setSound(input.next());                                 //setsoound of dog
		System.out.println("Input " + cat.getType() + "'s sound:"); //getype
		cat.setSound(input.next());                                 //setsound of cat
		System.out.println("Dog: " + dog.getSound());               //output sound of dog
		System.out.println("Cat: " + cat.getSound());               //output sound of cat
		input.close();
	}

}
