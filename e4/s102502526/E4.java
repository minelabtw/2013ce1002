package ce1002.e4.s102502526;
import java.util.Scanner;
import ce1002.e4.s102502526.Dog;
import ce1002.e4.s102502526.Cat;

public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Input dog's sound: ");
		Dog dog=new Dog("Dog");	  //大寫Dog呼叫class，前面的dog是我們設定的變數名稱，"Dog"是Dog class裡的建構式		
		dog.setSound(scanner.next());  //輸入叫聲到Dog class裡的dog建構式，回到他所繼承的Animal class的 setSound function，回到物件sound，經由getSound function return final sound.
		System.out.print("Input Cat's sound: ");  
		Cat cat=new Cat("Cat");			//同dog
		cat.setSound(scanner.next());   
		
		System.out.print(dog.getType() + ": " + dog.getSound() + "\n");
		System.out.print(cat.getType() + ": " + cat.getSound());
		
		scanner.close();
	}

}
