package ce1002.e4.s101201003;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input=new Scanner(System.in);
		String dogsound,catsound;
		
		Dog dog=new Dog("Dog");//new class
		System.out.println("Input Dog's sound: ");
		dogsound=input.next();//input dogsound
		dog.setsound(dogsound);
	
		Cat cat=new Cat("Cat");//new class
		System.out.println("Input Cat's sound: ");
		catsound=input.next();//input catsound
		cat.setsound(catsound);
	
		System.out.println(dog.gettype()+": "+dog.getsound());//out print
		System.out.println(cat.gettype()+": "+cat.getsound());
		
		input.close();
	}

}
