package ce1002.e4.s101201003;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void settype(String type){//settype
		this.type=type;		
	}
	public void setsound(String sound){//setsound
		this.sound=sound;
	}	
	public String gettype(){//gettype
		return type;
	}
	public String getsound(){//getsound
		return sound;
	}
	
}
