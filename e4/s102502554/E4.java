package ce1002.e4.s102502554;

import	java.util.Scanner;

public class E4 {
	public static void main (String [] args){
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		Dog dog = new Dog();
		String i = input.next();
		dog.setSound(i);//input the sound of cat
		System.out.println("Input Cat's sound: ");
		Cat cat = new Cat();
		String k = input.next();
		cat.setSound(k);//input the sound of dog
		
		System.out.print("Dog: "+dog.getSound());
		System.out.print("\n");
		System.out.print("Cat: "+cat.getSound());
		
		input.close(); //close the Scanner
	}

}
