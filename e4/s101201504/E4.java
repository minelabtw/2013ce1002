package ce1002.e4.s101201504;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner (System.in);
		String dogsound ;//claim string
		Dog dog=new Dog("Dog");  
		System.out.println("Input Dog's sound: ");//output    
		dogsound=input.next();     //input dog sound
		dog.setSound(dogsound);//claim function of class
		String catsound ;//claim string
		Cat cat =new Cat("Cat");
		System.out.println("Input Cat's sound: ");//output
		catsound=input.next();       //input cat sound
		cat.setSound(catsound);//claim function of class
		System.out.println("Dog: "+dog.getSound()); //output
		System.out.println("Cat: "+cat.getSound());	//output
	}
}
