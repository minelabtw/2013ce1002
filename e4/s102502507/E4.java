package ce1002.e4.s102502507;

import java.util.Scanner;

public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);

		System.out.println("Input Dog's sound: ");
		Dog Dog = new Dog();//宣告new class
		Dog.setSound(scanner.next());//存入聲音

		System.out.println("Input Dog's sound: ");
		Cat Cat = new Cat();//宣告new class
		Cat.setSound(scanner.next());//存入聲音

		System.out.println(Dog.getType() + ": " + Dog.getSound());//輸出
		System.out.println(Cat.getType() + ": " + Cat.getSound());//輸出

		scanner.close();

	}

}
