package ce1002.e4.s102502005;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void setType(String type){	//設定動物種類。
		this.type = type;
	}
	
	public void setSound(String sound){	//設定動物叫聲。
		this.sound = sound;
	}
	
	public String getType(){			//傳回動物種類。
		return this.type;
	}
	
	public String getSound(){			//傳回動物聲音。
		return this.sound;
	}
}
