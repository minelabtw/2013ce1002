package ce1002.e4.s102502005;
/*import ce1002.e4.s102502005.Animal;同一個package內都互相可見，不用import。
但在不同package時需要import。*/

public class Cat extends Animal{

	public Cat(){			//建構子
		setType("Cat");		//建立物件時呼叫settype()函數。
	}
}
