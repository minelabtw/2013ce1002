package ce1002.e4.s102502005;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();//建立新的Dog物件。
		Cat cat = new Cat();//建立新的Cat物件。
		
		System.out.println("Input Dog's sound: ");//要求使用者輸入貓和狗的聲音。
		dog.setSound(input.next());
		System.out.println("Input Cat's sound: "); 
		cat.setSound(input.next());
		
		System.out.println("Dog: " + dog.getSound());//印出貓和狗的聲音。
		System.out.print("Cat: " + cat.getSound());


	}

}
