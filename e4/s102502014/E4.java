package ce1002.e4.s102502014;
import java.util.Scanner;
import ce1002.e4.s102502014.Cat;
import ce1002.e4.s102502014.Dog;
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		Dog Dog=new Dog("Dog");
		Dog.setSound(scanner.next());
		System.out.println("Input Cat's sound: ");
		Cat Cat=new Cat("Cat");
		Cat.setSound(scanner.next());
		System.out.println(Dog.getType()+": "+Dog.getSound());
		System.out.println(Cat.getType()+": "+Cat.getSound());
		scanner.close();
	}

}
