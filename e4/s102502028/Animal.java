package ce1002.e4.s102502028;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void setType(String type)  //設定種類
	{
		this.type = type ;
	}
	
	public void setSound(String sound)  //設定叫聲
	{
		this.sound = sound ;
	}
	
	public String getType()  //取得種類
	{
		return type ;
	}
	
	public String getSound()  //取得叫聲
	{
		return sound ;
	}
	

}
