package ce1002.e4.s102502028;
import java.util.Scanner ;
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String type = "";  //宣告
		String sound = "";
		type = "Dog" ;  
		Dog dog = new Dog(type) ;
		System.out.println("Input " + dog.getType() + "'s sound: ") ;  //輸入狗的叫聲
		Scanner cin = new Scanner(System.in) ;
		sound = cin.next() ;
		dog.setSound(sound) ;  //設定狗的叫聲
		
		type = "Cat" ;
		Cat cat = new Cat(type) ;
		System.out.println("Input " + cat.getType() + "'s sound: ") ;  //輸入貓的叫聲
		sound = cin.next() ;
		cat.setSound(sound) ;  //設定貓的叫聲
		
		System.out.println(dog.getType()+": "+dog.getSound()) ;  //輸出結果
		System.out.println(cat.getType()+": "+cat.getSound()) ;
		
	}

}
