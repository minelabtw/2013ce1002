package ce1002.e4.s102502549;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	//設定，取得動物種類，叫聲
	public void setType(String type)
	{
		this.type=type;
	}
	
	public void setSound(String sound)
	{
		this.sound=sound;
	}
	
	public String getType()
	{
		return type;
	}
	
	public String getSound()
	{
		return sound;
	}
}
