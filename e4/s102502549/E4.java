package ce1002.e4.s102502549;

import java.util.Scanner;

public class E4 {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		
		Dog dog=new Dog();
		Cat cat=new Cat();
		
		//輸入
		System.out.println("Input Dog's sound: ");
		dog.setSound(input.next());
		System.out.println("Input Cat's sound: ");
		cat.setSound(input.next());
		
		//輸出
		System.out.println("Dog: "+dog.getSound());
		System.out.println("Cat: "+cat.getSound());
	}
}
