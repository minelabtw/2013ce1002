package ce1002.e4.s102502521;
import java.util.*;

public class E4 {

	/**
	 * @param args
	 */
	private static Scanner scanner;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		scanner=new Scanner(System.in);
		
		//Set dog
		System.out.println("Input Dog's sound: ");
		
		Dog dog=new Dog();
		
		dog.setSound(scanner.next());
		
		//Set cat
		System.out.println("Input Cat's sound: ");
		
		Cat cat=new Cat();
		
		cat.setSound(scanner.next());
		
		//Print
		System.out.println("Dog: "+dog.getSound());
		System.out.println("Cat: "+cat.getSound());
	}

}
