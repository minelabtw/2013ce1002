package ce1002.e4.s102502521;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal(){
		
	}
	
	//setType
	public void setType(String type){
		this.type=type;
	}
	
	//setSound
	public void setSound(String sound){
		this.sound=sound;
	}
	
	//getType
	public String getType(){
		return type;
	}
	
	//getSound
	public String getSound(){
		return sound;
	}
}
