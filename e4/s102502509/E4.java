package ce1002.e4.s102502509;

import java.util.Scanner;

public class E4 
{

	public static void main(String[] args) 
	{ 
		Scanner input = new Scanner(System.in);
		String dog = null;
		String cat = null;
		
		Dog dogs = new Dog(); // 宣告呼叫式
		Cat cats = new Cat();
		
		System.out.println("Input Dog's sound: ");
		dog = input.next(); 
		dogs.setSound(dog); // 取得輸入值
		
		System.out.println("Input Cat's sound: ");
		cat = input.next();
		cats.setSound(cat);
		
		System.out.println("Dog: " + dogs.getSound() ); // 讀出輸入值
		System.out.println("Cat: " + cats.getSound());
		input.close();
	}
	
}
