package ce1002.e4.s102502524;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		Dog d = new Dog();
		Cat c = new Cat();
		
		String sound;											//save sound
		
		System.out.println("Input Dog's sound: ");				//input Dog's sound
		sound = input.next();
		d.setSound(sound);
		
		System.out.println("Input Cat's sound: ");				//input Cat's sound
		sound = input.next();
		c.setSound(sound);
		
		input.close();
		
		System.out.println(d.getType() + ": " + d.getSound());	//print Dog's sound
		System.out.println(c.getType() + ": " + c.getSound());	//print Cat's sound
		
	}

}
