package ce1002.e4.s102502524;

public class Animal {
	
	Animal(){}
	
	private String type;	//動物種類
	private String sound;	//動物叫聲

	public void setType(String type)		//set type
	{
		this.type = type;
	}
	
	public void setSound(String sound)		//set sound
	{
		this.sound = sound;
	}
	
	public String getType()					//get type
	{
		return this.type;
	}
	
	public String getSound()				//get sound
	{
		return this.sound;
	}

}
