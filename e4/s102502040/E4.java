package ce1002.e4.s102502040;

import java.util.Scanner;


public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String sound;
		Scanner scan = new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		sound=scan.next();	//輸入聲音儲存至sound字串
		Dog dog = new Dog();
		dog.set_sound(sound);	//將字串傳入函式裡
		System.out.println("Input Cat's sound: ");
		sound=scan.next();
		Cat cat = new Cat();
		cat.set_sound(sound);
		System.out.println(dog.get_type()+ ": " + dog.get_sound());	//顯示種類,還有儲存函式之字串
		System.out.println(cat.get_type()+ ": " + cat.get_sound());
	}
}
