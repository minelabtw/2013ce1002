package ce1002.e4.s102502532;

public class Animal {

	private String type; // 動物種類
	private String sound; // 動物叫聲

	public Animal() { // 最好要有public

	}

	public void settype(String newtype) {
		type = newtype;
	}

	public void setsound(String newsound) { // 設定 及 取得
		sound = newsound;
	}

	public void gettype() {
		System.out.print(type + ": ");
	}

	public void getsound() {
		System.out.println(sound);
	}
}
