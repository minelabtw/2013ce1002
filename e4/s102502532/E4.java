package ce1002.e4.s102502532;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Input Dog's sound:");
		String Dogsound = input.nextLine(); // 讀取一整行字串
		System.out.println("Input Cat's sound:");
		String Catsound = input.nextLine();

		Dog dog1 = new Dog("Dog"); // 建立物件 不用建Animal
		Cat cat1 = new Cat("Cat");

		dog1.setsound(Dogsound);
		cat1.setsound(Catsound);

		dog1.gettype();
		dog1.getsound();
		cat1.gettype();
		cat1.getsound();

		input.close(); // 用不到了 關掉
	}

}
