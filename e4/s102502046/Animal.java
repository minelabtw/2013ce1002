package ce1002.e4.s102502046;

public class Animal 
{
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void settype(String x) 
	{
		this.type = x;
	}
	public void setsound(String x) 
	{
		this.sound = x;
	}
	public String gettype() 
	{
		return type;
	}
	public String getsound() 
	{
		return sound;
	}
}
