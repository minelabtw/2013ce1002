package ce1002.e4.s102502042;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		//建立物件
		Dog dog = new Dog();
		Cat cat = new Cat();
		//輸入叫聲
		System.out.println("Input Dog's sound: ");
		String Dsound = input.next();
		System.out.println("Input Cat's sound: ");
		String Csound = input.next();
		dog.setSound(Dsound);
		cat.setSound(Csound);
		//輸出
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		input.close();
	}

}
