package ce1002.e4.s102502506;

public class Animal {
	private String type;
	private String sound;
	public void setType(String T){
		this.type = T;
	}
	public void setSound(String S){
		this.sound = S;
	}
	public String getType(){
		return type;
	}
	public String getSound(){
		return sound;
	}
}
