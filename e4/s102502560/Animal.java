package ce1002.e4.s102502560;

public class Animal {
	private String type;	//animal's type
	private String sound;	//animal's sound
	
	//getters and setters
	public void setType(String type) {
		this.type = type;
	}
	
	public void setSound(String sound) {
		this.sound = sound;
	}
	
	public String getType() {
		return type;
	}
	
	public String getSound() {
		return sound;
	}
	
}
