package ce1002.e4.s102502560;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {

		Scanner STDIN=new Scanner(System.in);
		
		Dog dog1=new Dog();
		Cat cat1=new Cat();
		
		System.out.println("Input Dog's sound: ");
		dog1.setSound(STDIN.next());
		System.out.println("Input Cat's sound: ");
		cat1.setSound(STDIN.next());
		
		STDIN.close();
		
		System.out.println("Dog: "+dog1.getSound());
		System.out.println("Cat: "+cat1.getSound());
		
	}

}
