package ce1002.e4.s101201046;

public class Animal {
	private String type;
	private String sound;
	
	public void setType (String type) { //set the type of animal
		this.type = type;
	}
	
	public void setSound (String sound) { //set the sound of the animal
		this.sound = sound;
	}
	
	public String getType () { //get the type of animal
		return type;
	}
	
	public String getSound () { //get the sound of animal
		return sound;
	}
}