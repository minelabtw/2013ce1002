package ce1002.e4.s101201046;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Cat cat = new Cat();
		Dog dog = new Dog();
		Scanner cin = new Scanner(System.in); //set input
		
		System.out.println("Input " + dog.getType() + "'s sound:"); //input the sound of dog
		dog.setSound(cin.nextLine());
		
		System.out.println("Input " + cat.getType() + "'s sound:"); // input the sound of cat
		cat.setSound(cin.nextLine());
		
		System.out.println("Dogs: " + dog.getSound()); //output the sound of dog
		System.out.println("Cats: " + cat.getSound()); //output the sound of cat

		cin.close();
	}

}
