package ce1002.e4.s101201506;

import java.util.Scanner;

public class E4 {
	private static Scanner scanner;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		String dogbarking;// 宣告字串 狗的聲音
		String catbarking;// 宣告字串 貓的聲音
		Dog dog = new Dog("Dog");

		System.out.println("Input Dog's sound: ");
		dogbarking = scanner.next();

		dog.setSound(dogbarking);// 傳到 setSound 裡

		Cat cat = new Cat("Cat");

		System.out.println("Input Cat's sound: ");
		catbarking = scanner.next();

		cat.setSound(catbarking);// 傳到 setSound 裡
		// 輸出
		System.out.println("Dog: " + dog.getSound());
		System.out.println("Cat: " + cat.getSound());

	}

}
