package ce1002.e4.s102502026;
import java.util.Scanner;
public class E4 {
public static Scanner input;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		input = new Scanner(System.in);
		String dog;
		String cat;
		System.out.println("Input Dog's sound: ");
		dog =input.next();	//the sound of dog
		System.out.println("Input Cat's sound: ");
		cat =input.next();	//the sound of cat
		Dog d= new Dog();	//get Dog.java
		d.setSound(dog);	//set the Sound into Dog-Animal
		System.out.println(d.getType()+": " + d.getSound());
		Cat c= new Cat();	//get Cat.java
		c.setSound(cat);	//set the Sound into Cat-Animal
		System.out.println(c.getType()+": "+ c.getSound());
	   input.close();	//end Scanner
	}
}
