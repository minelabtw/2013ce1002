package ce1002.e4.s102502505;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input Dog's sound: ");//輸出字串
		Dog dog = new Dog();//建立dog物件
		dog.setSound(input.nextLine());//去Dog.java拿取setSound的method，去儲存dog聲音
		
		System.out.println("Input Cat's sound: ");
		Cat cat = new Cat();
		cat.setSound(input.nextLine());
		
		System.out.println("Dog: "+dog.getSound());//去Dog.java拿取getSound的method，去顯示dog聲音
		System.out.println("Cat: "+cat.getSound());

	}

}
