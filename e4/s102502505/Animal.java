package ce1002.e4.s102502505;



public class Animal {

	private String type;	//定義動物種類
	private String sound;	//定義動物叫聲


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSound() {
		return sound//輸出動物聲音
	}

	public void setSound(String sound) {
		this.sound = sound;//儲存動物聲音
	}

}
