package ce1002.e4.s102502556;

public class Animal {
	private String type; //動物種類
	private String sound; //動物叫聲
	void setType ( String type ) //設定種類
	{
		this.type = type;
	}
	void setSound ( String sound ) //設定叫聲
	{
		this.sound = sound;
	}
	String getType () //回傳種類
	{
		return type;
	}
	String getSound () //回傳叫聲
	{
		return sound;
	}
}
