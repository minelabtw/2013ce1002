package ce1002.e4.s102502556;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //宣告一個Scanner
		Dog dog = new Dog(); //建立一個名為dog的Dog物件
		Cat cat = new Cat(); //建立一個名為cat的Cat物件		
		System.out.println("Input Dog's sound: ");
		String dogsound = input.next(); 
		dog.setSound(dogsound); //設定dog的sound屬性
		System.out.println("Input Cat's sound: ");
		String catsound = input.next();
		cat.setSound(catsound); //設定cat的sound屬性
		input.close(); //關閉Scanner
		System.out.println(dog.getType()+": "+dog.getSound());
		System.out.println(cat.getType()+": "+cat.getSound());
	}

}
