package ce1002.e4.s102502035;

import java.util.Scanner;//include input

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);// Scanner class input object
		System.out.println("Input Dog's sound: ");// remind input
		Dog dog = new Dog("Dog");
		dog.setsound(input.next());// input
		System.out.println("Input Cat's sound: ");// remind input
		Cat cat = new Cat("Cat");
		cat.setsound(input.next());// input
		System.out.println(dog.getType() + ": " + dog.getSound());// output
		System.out.print(cat.getType() + ": " + cat.getSound());// output
	}
}
