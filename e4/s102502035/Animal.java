package ce1002.e4.s102502035;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public Animal() {

	}

	public void setType(String newType) {
		type = newType;
	}

	public void setsound(String newSound) {
		sound = newSound;
	}

	public String getType() {
		return type;
	}

	public String getSound() {
		return sound;
	}
}
