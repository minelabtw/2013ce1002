package ce1002.e4.s102502513;

import java.util.Scanner;

public class E4 
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		Cat cat = new Cat();
		Dog dog = new Dog();  //加入貓狗物件
		
		System.out.print("Input Dog's sound: \n");
		dog.setSound(input.next());
		
		System.out.print("Input Cat's sound: \n");
		cat.setSound(input.next());
		
		System.out.print(dog.getType() + ": " + dog.getSound() + "\n"
				       + cat.getType() + ": " + cat.getSound());  //輸出type和sound
		
		input.close();
	}
}
