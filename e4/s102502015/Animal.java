package ce1002.e4.s102502015;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	public void setType(String Type) {
		type=Type;
	}
	public void setSound(String Sound) {
		sound=Sound;
	}
	public String getType()
	{
		return type;
	}
	public String getSound()
	{
		return sound;
	}
}
