package ce1002.e4.s102502015;

import java.util.Scanner;
import ce1002.e4.s102502015.Cat;
import ce1002.e4.s102502015.Dog;
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner=new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		Dog Dog=new Dog("Dog");			//創物件 初始化
		Dog.setSound(scanner.next());	//設定聲音
		System.out.println("Input Cat's sound: ");
		Cat Cat=new Cat("Cat");			//創物件 初始化
		Cat.setSound(scanner.next());   //設定聲音
		System.out.println(Dog.getType()+": "+Dog.getSound());
		System.out.println(Cat.getType()+": "+Cat.getSound());
		scanner.close();
	}

}
