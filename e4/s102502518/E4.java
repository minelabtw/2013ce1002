package ce1002.e4.s102502518;
import java.util.Scanner;
public class E4 {

	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Dog dog = new Dog();//set dog's object
		Cat cat = new Cat();//set dog's object
	
		System.out.println("Input " + dog.getType() + "'s sound: ");
		dog.setSound(input.next());// input the sound of dog 
		
		System.out.println("Input " + cat.getType() + "'s sound: "); 
		cat.setSound(input.next());// input the sound of cat 
		
		input.close();// close scanner
 
		System.out.println(dog.getType() + ": " + dog.getSound());// output the sound of dog 
		System.out.println(cat.getType() + ": " + cat.getSound());// output the sound of cat 
			
 
	}

}
