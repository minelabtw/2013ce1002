package ce1002.e4.s102502518;

public class Animal {

	private String type;	//animal type
	private String sound;	//animal sound
	
	public void setType(String type) {// set type
		this.type = type;
	}
	
	public void setSound(String sound) {// set sound
		this.sound = sound;
	}
 
	public String getType() {// get type
		return type;
	}
 
	public String getSound() {// get sound
		return sound;
	}
 
	

 
	

}
