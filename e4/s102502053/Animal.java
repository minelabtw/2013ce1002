package ce1002.e4.s102502053;

public class Animal {
	
	private String type;
	private String sound;
	
	Animal() //constructor
	{
		
	}
	
	void setType(String Type) //input type	
	{
		type = Type;
	}
	
	void setSound(String Sound)// input sound
	{
		sound = Sound;
	}
	
	String getType() //return result
	{
		return  type;
	}
	
	String getSound()
	{
		return sound;
	}

}
