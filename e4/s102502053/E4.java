package ce1002.e4.s102502053;

import java.util.Scanner; 

public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in); //create a scanner object
		Dog dog = new Dog("Dog");  //create objects
		Cat cat = new Cat("Cat");
		
		System.out.print("Input Dog's sound: \n"); // output
		dog.setSound(input.nextLine()); //input 
		System.out.print("Input Cat's sound: \n");
		cat.setSound(input.nextLine());
		
		System.out.println(dog.getType() + ": " + dog.getSound()); //output
		System.out.println(cat.getType() + ": " + cat.getSound());
		
	}

}
