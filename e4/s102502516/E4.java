package ce1002.e4.s102502516;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		Cat cat = new Cat();
		Dog dog = new Dog();// 新增貓狗物件
		System.out.print("Input Dog's sound: \n");
		dog.setSound(scanner.next());// 取得狗叫
		System.out.print("Input Cat's sound: \n");
		cat.setSound(scanner.next());// 取得貓叫
		System.out.print(dog.getType() + ": " + dog.getSound() + "\n"
				+ cat.getType() + ": " + cat.getSound());// 印出type和sound
		scanner.close();
	}

}
