package ce1002.e4.s102502013;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		String Sod;
		Sod = input.next();
		Dog dog = new Dog();
		dog.setSound(Sod);
		System.out.println("Input Cat's sound: ");
		String Sod1;
		Sod1 = input.next();
		Cat cat = new Cat();
		cat.setSound(Sod1);
		System.out.println(dog.getType() +": " + dog.getSound());
		System.out.println(cat.getType() +": " + cat.getSound());
	}
}
