package ce1002.e4.s102502007;
import java.util.Scanner;
public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		Dog dog = new Dog();//create the object dog
		Cat cat = new Cat();//create the object cat
		System.out.println("Input " + dog.getType() + "'s sound:");
		dog.setSound(input.next());//input the sound of dog type animal
		System.out.println("Input " + cat.getType() + "'s sound:");
		cat.setSound(input.next());//input the sound of cat type animal
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		//display the animal's sound
	}

}
