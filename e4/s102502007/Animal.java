package ce1002.e4.s102502007;

public class Animal {
	public Animal()
	{
		
	}//constructor
	private String type;	//動物種類
	private String sound;	//動物叫聲
	public void setType(String type)
	{
		this.type=type;
	}//set up a method to store the animal's type
	public void setSound(String sound)
	{
		this.sound=sound;
	}//set up a method to store the animal's sound
	public String getType()
	{
		return type;
	}//to get the type of the animal
	public String getSound()
	{
		return sound;
	}//to get the sound of the animal
}
