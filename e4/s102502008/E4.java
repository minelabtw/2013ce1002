package ce1002.e4.s102502008;
import java.util.Scanner ;
public class E4 
{
	public static void main(String args[])
	{
		Scanner input= new Scanner(System.in) ;
		System.out.println("Input Dog's sound: ") ;
		String dogSound= input.next() ;
		//get dog's sound
		Dog animal1= new Dog() ;
		animal1.setSound(dogSound) ;
		//build the dog's type and sound
		System.out.println("Input Cat's sound:") ;
		String catSound= input.next() ;
		//get cat's sound
		Cat animal2= new Cat();
		animal2.setSound(catSound) ;
		//build the cat's type and sound
		System.out.println("Dog: "+ animal1.getSound()) ;
		System.out.println("Cat: "+ animal2.getSound()) ;
		//output 
		input.close();
	}
}
