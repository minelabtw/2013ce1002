package ce1002.e4.s102502553;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		String dog;
		String cat;
		
		Dog d = new Dog();//建立型態為Dog，名字為d
		System.out.println("Input Dog's sound:");
        dog = input.next();
        d.SetSound(dog);
        
        Cat c = new Cat();////建立型態為Cat，名字為c
        System.out.println("Input Cat's sound:");
        cat = input.next();
        c.SetSound(cat);
        
        System.out.println(d.getType() + ": " + d.getsound());
        System.out.println(c.getType() + ": " + c.getsound());
        
        input.close();
	}

}
