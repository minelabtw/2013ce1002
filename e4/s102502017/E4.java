package ce1002.e4.s102502017;
import java.util.Scanner;


public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Dog dog = new Dog();
		Cat cat = new Cat();
		
		//set the animals' sound
		System.out.println("Input Dog's sound: ");
		dog.setSound(input.next());
		System.out.println("Input Cat's sound: ");
		cat.setSound(input.next());
		
		//print the animals' type and sound
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		
		input.close();
	}

}
