package ce1002.e4.s102502017;

public class Animal {
	
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	void setType(String x){
		type = x;
	}
	
	void setSound(String y){
		sound = y;
	}
	
	String getType(){
		return type;
	}
	
	String getSound(){
		return sound;
	}
	 
}
