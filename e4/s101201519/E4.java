package ce1002.e4.s101201519;
import java.util.Scanner;
public class E4 {

	public static void main(String[]args)
	{
		Cat cat = new Cat();
		Dog dog = new Dog();
		Scanner input = new Scanner(System.in);
		
		System.out.println("Input " + dog.getType() + "'s sound:");
		dog.setSound(input.nextLine());
		
		System.out.println("Input " + cat.getType() + "'s sound:");
		cat.setSound(input.nextLine());
		
		System.out.println("Dogs: " + dog.getSound());
		System.out.println("Cats: " + cat.getSound());
		
		input.close();
	}

}
