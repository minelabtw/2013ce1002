package ce1002.e4.s102502525;
import java.util.Scanner;
public class E4 {
	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		String cat;
		String dog;
		Animal a=new Animal();
		Dog d=new Dog();
		d.setType("Dog");
		System.out.println("Input Dog's sound: ");
		dog=input.nextLine();
		d.setSound(dog);
		Cat c=new Cat();
		c.setType("Cat");
		System.out.println("Input Cat's sound: ");
		cat=input.nextLine();
		c.setSound(cat);
		System.out.println(d.getType()+":"+d.getSound());
		System.out.println(c.getType()+":"+c.getSound());
	}

}
