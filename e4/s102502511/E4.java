package ce1002.e4.s102502511;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		Cat cats = new Cat(); //呼叫建構值
		Dog dogs = new Dog();
		
		
		System.out.println("Input " + dogs.getType()+"'s sound:");
		String dogsound = input.next(); 
		dogs.setSound(dogsound); //回傳dogsound
		
		System.out.println("Input " + cats.getType() + "'s sound:");
		String catsound = input.next();
		cats.setSound(catsound); //回傳catsound
		
		System.out.println(dogs.getType()+ ": " +dogs.getSound());
		System.out.println(cats.getType()+ ": " +cats.getSound());
		
		input.close();
	}

}
