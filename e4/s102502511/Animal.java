package ce1002.e4.s102502511;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public String getType() { //儲存種類
		return type;
	}
	public void setType(String type) { 	//回傳種類
		this.type = type;
	}
	public String getSound() { //儲存聲音
		return sound;
	}
	public void setSound(String sound) { //回傳聲音
		this.sound = sound;
	}
	

}
