package ce1002.e4.s102502011;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in) ;
		
		System.out.println("Input Dog's sound: " ) ;
		Dog dogs =new Dog("Dog") ; //大寫Dog呼叫class，前面的dog是我們設定的變數名稱，"Dog"是Dog class裡的建構式	
		dogs.setSound( input.next() );  //輸入叫聲到Dog class裡的dog建構式，回到他所繼承的Animal class的 setSound function，回到物件sound，經由getSound function return final sound.
		
		System.out.println("Input Cat's sound: " ) ;
		Cat cats =new Cat("Cat") ;
		cats.setSound( input.next() ) ;
		
		
		System.out.println( dogs.getType() + ": " + dogs.getSound() ) ;
		System.out.print( cats.getType() + ": " + cats.getSound() ) ;
		
		input.close();
	}

}
