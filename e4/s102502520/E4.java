package ce1002.e4.s102502520;
import java.util.Scanner;
public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner cin = new Scanner(System.in);
		Dog Dog = new Dog();
		System.out.println("Input Dog's Sound");//output
		String dogsound =cin.next();//input
		Dog.setSound(dogsound);//call
		
		Cat Cat = new Cat();
		System.out.println("Input Cat's Sound");//output
		String catsound =cin.next();//input
		Cat.setSound(catsound);//call
		
		
		System.out.println(Dog.getType()+": "+Dog.getSound(dogsound));//output
		System.out.println(Cat.getType()+": "+Cat.getSound(catsound));//output
		cin.close();//close cin
	}

}
