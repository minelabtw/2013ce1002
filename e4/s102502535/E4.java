package ce1001.e4.s102502535;

import java.util.Scanner; //import scanner 

public class E4 {

	public static void main(String[] args) {

		String dog;
		String cat; // 設兩字串。

		Scanner input = new Scanner(System.in);

		System.out.println("Input dog's sound: ");
		dog = input.next(); // 輸入叫聲。

		System.out.println("Input cat's sound: ");
		cat = input.next(); // 輸入叫聲。

		Dog D = new Dog(dog);
		Cat C = new Cat(cat); // 呼叫class

		D.setSound(dog);
		C.setSound(cat); // 存叫聲。

		System.out.println("Dog: " + D.getSound());
		System.out.println("Cat: " + C.getSound()); // 輸出。

		input.close(); // close input

	}

}
