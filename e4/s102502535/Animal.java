package ce1001.e4.s102502535;

public class Animal {

	private String type; // 動物種類
	private String sound; // 動物叫聲

	public void setType(String type) {
		this.type = type;
	} // 存type。

	public String getType() {
		return type;
	} // 回傳type。

	public void setSound(String sound) {
		this.sound = sound;
	} // 存叫聲。

	public String getSound() {
		return sound;
	} // 回傳叫聲。
}
