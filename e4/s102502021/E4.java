package ce1002.e4.s102502021;
import java.util.Scanner;
public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound: ");
		Dog dog = new Dog("dog");
		dog.setsound(input.next());
		System.out.println("Input Cat's sound: ");
		Cat cat = new Cat("cat");
		cat.setsound(input.next());
		System.out.println(dog.getType()+": "+dog.getSound());
		System.out.println(cat.getType()+": "+cat.getSound());
		input.close();
	}

}
