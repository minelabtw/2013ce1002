package ce1002.e4.s995001561;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		
		// create object cat and dog
		Cat cat = new Cat();
		Dog dog = new Dog();
		
		// create String dogsound and catsound
		String dogsound = new String();
		String catsound = new String();
		Scanner input = new Scanner(System.in);
		
		// input dogsound and insert into object dog
		System.out.println("Input Dog's sound: ");
		dogsound = input.next();
		dog.setSound(dogsound);
		
		// input catsound and insert into object cat
		System.out.println("Input Cat's sound: ");
		catsound = input.next();
		cat.setSound(catsound);

		// print outcome
		System.out.println(dog.getType()+ ": " + dog.getSound());
		System.out.println(cat.getType()+ ": " + cat.getSound());
		input.close();
		
	}
}
