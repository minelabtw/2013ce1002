package ce1002.e4.s102502544;

public class Animal {
	
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	Animal(){
		
	}
	void settype(String type){//動物種類函式
		this.type=type;
	}
	void setsound(String sound){//動物叫聲函式
		this.sound=sound;
	}
	String gettype(){//動物種類輸出
		return type;
	}
	String getsound(){//動物叫聲輸出
		return sound;
	}

}
