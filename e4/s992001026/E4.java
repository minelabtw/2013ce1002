package ce1002.e4.s100201023;

import java.util.Scanner;

public class E4
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		Cat cat = new Cat();
		Dog dog = new Dog();
		
		//input dog's sound
		System.out.println("Input Dog's sound: ");
		dog.setsound(input.next());
		
		//input cat's sound
		System.out.println("Input Cat's sound: ");
		cat.setsound(input.next());

		//output dog's sound
		System.out.println("Dog: " + dog.getsound());
		
		//output cat's sound
		System.out.println("Cat: " + cat.getsound());
	}

}
