package ce1002.e4.s102502018;
import java.util.Scanner;
public class E4 {

	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		String sound;
		Dog dog = new Dog();
		Cat cat = new Cat();		
		System.out.println("Input "+dog.gettype()+"'s sound: ");
		sound=scan.next();
		dog.setsound(sound);
		System.out.println("Input "+cat.gettype()+"'s sound: ");
		sound=scan.next();
		cat.setsound(sound);
		System.out.println(dog.gettype()+": "+dog.getsound());
		System.out.println(cat.gettype()+": "+cat.getsound());
		scan.close();
	}

}
