package ce1002.e4.s102502018;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal()
	{
		
	}
	public void settype(String type)
	{
		this.type=type;
	}
	public void setsound(String sound)
	{
		this.sound=sound;
	}
	public String gettype()
	{
		return type;
	}
	public String getsound()
	{
		return sound;
	}


}
