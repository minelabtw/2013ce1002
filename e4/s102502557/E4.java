package ce1002.e4.s202502557;

import java.util.Scanner;

public class E4 {
    
	static Scanner input = new Scanner(System.in);
    
	public static void main(String[] args) 
   {
	
        Cat cat = new Cat();//宣告一個在E4的新class
		Dog dog = new Dog();
		String d,c;
		
		
		System.out.println("Input "+dog.get_type() +"'s sound:");
		d = input.next();//next本身就是回傳string
		dog.set_sound(d);
		
		System.out.println("Input "+cat.get_type() +"'s sound:");
		c = input.next();//next本身就是回傳string
		cat.set_sound(c);

		
		System.out.print(dog.get_type()+": "+dog.get_sound()+"\n");//get函式return但是仍要print才會有回傳
		System.out.print(cat.get_type()+": "+cat.get_sound() );//get函式return但是仍要print

	}

}
