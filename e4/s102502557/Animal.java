package ce1002.e4.s202502557;
public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void set_type(String _type)
	{
		type = _type;
	}
	
	public String get_type()
	{
		return type;
	}
	
	public void set_sound(String _sound)
	{
		sound = _sound;
	}
	
	public String get_sound()
	{
		return sound;
	}
	
	
}
