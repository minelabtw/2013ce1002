package ce1002.e4.s101201521;
//class Cat is a subclass of class Animal
public class Cat extends Animal{
	public Cat(String sound){
		super(sound);
		//animal type is always "Cat" in class Cat
		super.setType("Cat");
	}
}
