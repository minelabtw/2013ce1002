package ce1002.e4.s101201521;
//class Dog is a subclass of class Animal
public class Dog extends Animal{
	public Dog(String sound){
		super(sound);
		//type of animal is always "Dog" in class Dog
		super.setType("Dog");
	}
}
