package ce1002.e4.s101201521;
//class Animal is the super class of classes Cat and Dog
public class Animal {
	//constructor with initial sound
	public Animal(String sound){
		setSound(sound);
	}
	//type of animal
	private String type;
	//sound of the animal
	private String sound;
	//set type of the animal
	public void setType(String type){
		this.type = type;
	}
	//set sound of the animal
	public void setSound(String sound){
		this.sound = sound;
	}
	//return type of the animal
	public String getType(){
		return type;
	}
	//return sound of the animal
	public String getSound(){
		return sound;
	}
}
