package ce1002.e4.s101201521;
import java.util.Scanner;
public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		String soundOfDog, soundOfCat;
		//prompt for sound of dog
		System.out.println("Input Dog's sound: ");
		soundOfDog = input.next();
		//prompt for sound of cat
		System.out.println("Input Cat's sound: ");
		soundOfCat = input.next();
		input.close();
		Dog dog = new Dog(soundOfDog);
		Cat cat = new Cat(soundOfCat);
		//print sound of dog, cat respectively
		System.out.println("Dog: " + dog.getSound());
		System.out.println("Cat: " + cat.getSound());
	}
}
