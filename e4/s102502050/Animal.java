package ce1002.e4.s102502050;

public class Animal {
	private String type;//動物種類
	private String sound;//動物聲音
	public Animal()
	{
		
	}
	public void Settype(String type)//改變 type
	{
		this.type=type;
	}
	public String Gettype()//回傳 type的值
	{
		return type;
	}
	public void Setsound(String sound)// 改變 sound
	{
		this.sound=sound;
	}
	public String Getsound()// 回傳 sound的值
	{
		return sound;
	}
}
