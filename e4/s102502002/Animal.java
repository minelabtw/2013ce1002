package ce1002.e4.s1020502002;

public class Animal {
	//set animal sound and get animal sound
	private String type;	//type of animal
	private String sound;	//sound of animal
	
	public void setType(String type){
		this.type=type;
	}
	
	public String getType(){
		return type;
	}
	
	public void setSound(String sound){
		this.sound=sound;
	}
	
    public String getSound(){
		return sound;
	}

}