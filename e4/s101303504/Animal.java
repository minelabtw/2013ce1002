package ce1002.e4.s101303504;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲


public Animal(){
}

public Animal(String type,String sound){
	this.type=type;
	this.sound=sound;
}
public String getType(){
	return type;
}
public String getSound(){
	return sound;
}
public void setSound(String sound){
	this.sound=sound;
}
public void setType(String type){
	this.type=type;
}
}
