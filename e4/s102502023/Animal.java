package ce1002.e4.s102502023;

class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void setType(String xxx) {
		type = xxx;
	}
	
	public String getType() {
		return type;
	}
	
	public void setSound(String xxx) {
		sound = xxx;
	}
	
	public String getSound() {
		return sound;
	}
}
