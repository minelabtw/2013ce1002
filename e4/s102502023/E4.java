package ce1002.e4.s102502023;

import java.util.Scanner;

public class E4 {
  public static void main(String[] args) {
	  Scanner input = new Scanner(System.in); // initialize Scanner input
	  Cat cat = new Cat(); // initialize Cat cat
	  Dog dog = new Dog(); // initialize Dog dog
	  System.out.println("Input Dog's sound:");
	  dog.setSound(input.next());
	  System.out.println("Input Cat's sound:");
	  cat.setSound(input.next());
	  System.out.println("Dog: " + dog.getSound() + "\nCat: " + cat.getSound());
	  input.close();
  }
}
