package ce1002.e4.s102502530 ;   //package name
import ce1002.e4.s102502530.Dog ;   //import
import ce1002.e4.s102502530.Cat ;
import java.util.Scanner ;

public class E4
{
   public static void main(String[] args)
   {
      Cat cat = new Cat() ;   //declare
      Dog dog = new Dog() ;
      Scanner scanner = new Scanner(System.in) ;

      System.out.println("Input Dog's sound: ") ;  //input
      dog.setSound(scanner.next()) ;
      System.out.println("Input Cat's sound: ") ;
      cat.setSound(scanner.next()) ;

      scanner.close() ;    //close scanner

      System.out.println(dog.getType() + ": " + dog.getSound()) ;    //output
      System.out.println(cat.getType() + ": " + cat.getSound()) ;
   }
}
