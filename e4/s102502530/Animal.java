package ce1002.e4.s102502530 ;   //package name

public class Animal
{
   private String type ;   //declare
   private String sound ;

   public void setType(String type)    //set type
   {
      this.type = type ;
   }
   public void setSound(String sound)  //set sound
   {
      this.sound = sound ;
   }
   public String getType()    //get type
   {
      return type ;
   }
   public String getSound()   //get sound
   {
      return sound ;
   }
}
