package ce1002.e4.s102502536;

import java.util.Scanner;

public class E4 {

	public static void main(String [] args){
		
		String Cat; 
		String Dog;
		
		Scanner input = new Scanner(System.in); // print out the line
		System.out.println("Input Dog's sound:");
		Dog = input.next();
		Dog dog = new Dog();  // instantiation
		dog.setSound(Dog);  // set the sound of dog
		System.out.println("Input Cat's sound:");
		Cat = input.next();
		Cat cat = new Cat();
		cat.setSound(Cat);
		
		System.out.println(dog.getType() + ": " + dog.getSound());  //print out the line
		System.out.println(cat.getType() + ": " + cat.getSound());
	
		input.close();
	}
}
