package ce1002.e4.s102502536;

public class Animal {
	
	private String type;	
	private String sound;	
	
	public Animal(){
	}
	// set type, sound
	public void setType(String type){
		this.type = type;
	}
	
	public void setSound(String sound){
		this.sound = sound;
	}
	// get type, sound
	public String getType(){
		return type;
	}
	
	public String getSound(){
		return sound;
	}

}
