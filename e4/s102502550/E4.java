package ce1002.e4.s102502550;

import java.util.Scanner;


public class E4 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);                                  //輸入物件
		Dog d = new Dog(); 
		Cat c = new Cat();
		
		System.out.println("Input Dog's sound:");                             //輸入 
		d.setSound(in.next());
		System.out.println("Input Cat's sound:");
		c.setSound(in.next());

		System.out.println("Dog: "+d.getSound());                             //輸出
		System.out.println("Cat: "+c.getSound()); 
		
		in.close();                                                           
	}     

}
