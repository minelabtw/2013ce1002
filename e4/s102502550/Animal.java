package ce1002.e4.s102502550;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal(){
		
	}
	public void setType(String s){                    //get && set 方法 
		type = s;
	}
	public void setSound(String s){
		sound = s;
	}
	public String getType(){
		return type;
	}
	public String getSound(){
		return sound;
	}
	
}
