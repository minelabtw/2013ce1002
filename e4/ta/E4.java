package ce1002.e4.ta;

import java.util.Scanner;


public class E4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		Dog d = new Dog();	//創建一個dog
		System.out.println("Input " + d.getType() + "'s " + "sound: ");
		d.setSound(input.next());	//設定叫聲,ex:"WOOF!"

		Cat c = new Cat();	//創建一個cat
		System.out.println("Input " + c.getType() + "'s " + "sound: ");
		c.setSound(input.next());	//設定叫聲,ex:"MEOW~"
		
		System.out.println(d.getType() + ": " + d.getSound());
		System.out.println(c.getType() + ": " + c.getSound());
	}

}
