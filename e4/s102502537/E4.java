package ce1002.e4.s102502537;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Dog dog = new Dog();// 宣告狗+貓
		Cat cat = new Cat();
		Scanner cin = new Scanner(System.in);
		System.out.println("Input " + dog.getType() + "'s sound: ");
		dog.setSound(cin.next());
		System.out.println("Input " + cat.getType() + "'s sound: ");
		cat.setSound(cin.next());
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());// 輸出所需
	}

}