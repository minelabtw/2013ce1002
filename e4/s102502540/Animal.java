package ce1002.e4.s102502540;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public String getType() { //getter of tyoe
		return type;
	}

	public String getSound() {
		return sound;
	}

	public void setType(String newtype) { //setter of type
		type = newtype;
	}

	public void setSound(String newsound) {
		sound = newsound;
	}

}
