package ce1002.e4.s102502559;

import java.util.Scanner;

public class Dog extends Animal {

	public Dog() {
		Scanner input = new Scanner(System.in);
		String dogType = "Dog";
		this.setType(dogType);
		System.out.println("Input Dog's sound: ");
		String dogsound = input.nextLine();
		this.setSound(dogsound);

	}
}
