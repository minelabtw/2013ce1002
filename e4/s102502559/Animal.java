package ce1002.e4.s102502559;

import java.util.Scanner;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public Animal() {

	}

	public void setType(String s) {
		this.type = s;
	}

	public void setSound(String s) {
		this.sound = s;
	}

	public void getType() {
		System.out.print(this.type + ": ");
	}

	public void getSound() {
		System.out.print(this.sound + "\n");
	}
}
