package ce1002.e4.s102502559;

import java.util.Scanner;

public class Cat extends Animal {
	public Cat() {
		Scanner input = new Scanner(System.in);
		String catType = "Cat";
		this.setType(catType);
		System.out.println("Input Cat's sound: ");
		String catsound = input.nextLine();
		this.setSound(catsound);

	}
}
