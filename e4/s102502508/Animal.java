package ce1002.e4.s102502508;

public class Animal {

	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal()
	{
		
	}
	public void Settype(String type)
	{
		this.type= type ;
	}
	public void Setsound(String sound)
	{
		this.sound=sound ;
	}
	public String Gettype()
	{
		 return type ;
	}
	public String Getsound()
	{
		return sound ;
	}

}
