package ce1002.e4.s102502032;

import java.util.Scanner;

public class E4
{
	public static void main(String[] args)
	{
		Cat cat = new Cat();
		Dog dog = new Dog();
		Scanner jin = new Scanner(System.in);
		System.out.println("Input " + dog.getType() + "'s sound: ");
		dog.setSound(jin.next());
		System.out.println("Input " + cat.getType() + "'s sound: ");
		cat.setSound(jin.next());
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		jin.close();
	}
}
