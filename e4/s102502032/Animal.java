package ce1002.e4.s102502032;

public class Animal
{
	private String	type;
	private String	sound;

	// constructor
	Animal()
	{
	}

	// set
	public void setType(String str)
	{
		type = str;
	}

	public void setSound(String str)
	{
		sound = str;
	}

	// get
	public String getType()
	{
		return type;
	}

	public String getSound()
	{
		return sound;
	}
}
