package ce1002.e4.s102502006;

import java.util.Scanner;

public class E4 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Input Dog's sound: ");
		Dog dog = new Dog();
		dog.setSound(scanner.next());
		
		System.out.println("Input Cat's sound: ");
		Cat cat = new Cat();
		cat.setSound(scanner.next());
		
		System.out.println("Dog: " + dog.getSound());
		System.out.println("Cat: " + cat.getSound());
		
	}

}
