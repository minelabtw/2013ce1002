package ce1002.e4.s102502515;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);//creat input
		String doggy = new String();
		String kitty = new String();
		Cat cat = new Cat();//call Cat class
		Dog dog = new Dog();//call Dog class
		
		System.out.println("Input Dog's sound: ");
		doggy = input.nextLine();//let user input string
		dog.setSound(doggy);//transmit string to setSound
		
		System.out.println("Input Cat's sound: ");
		kitty = input.nextLine();//let user input string
		cat.setSound(kitty);//transmit string to setSound
		
		System.out.println( "Dog: " + dog.getSound());//call getSound
		System.out.print( "Cat: " + cat.getSound());
		input.close();
	}

}
