package ce1002.e4.s102502515;

public class Animal {
	private String type;	//kind of animal
	private String sound;	//bark of animal
	
	public Animal(){
	}
	
	public String getType() {//return kind of animal
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSound() {//return sound
		return sound;
	}
	
	public void setSound(String sound) {
		this.sound = sound;
	}
}
