package ce1002.e4.s102502517;

import java.util.Scanner;
import ce1002.e4.s102502517.Cat;
import ce1002.e4.s102502517.Dog;

public class E4 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Cat cat = new Cat("Cat");
		Dog dog = new Dog("Dog");
		
		System.out.println("Input " + dog.getType() +"'s sound:"); //輸入叫聲
		dog.setSound(scanner.next());
		System.out.println("Input " + cat.getType() +"'s sound:"); //輸入叫聲
		cat.setSound(scanner.next());
		
		System.out.println(dog.getType() +": " + dog.getSound()); //輸出叫聲
		System.out.println(cat.getType() +": " + cat.getSound()); //輸出叫聲
	}

}
