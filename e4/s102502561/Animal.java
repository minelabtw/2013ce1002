package ce1002.e4.s102502561;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲
	
	public Animal()
	{
				//建構子
	}

	public void settype(String type) 
	{
		 this.type = type;	//this=cat,dog 把animal type傳進去
	}

	public String gettype() 
	{
		return type;//種類cat.gettype=>cat dog.gettype=>dog
	}
	
	public void setsound(String voice)
	{
		this.sound = voice;	//this=>cat,dog 把貓狗聲音存進去
	}
	public String getsound()
	{
		return sound;
	}
	
	

}
