package ce1002.e4.s102502041;
import java.util.Scanner;
public class Animal {
	Scanner cin = new Scanner(System.in);
	private String type;
	private String sound;
	void setType(String a)
	{
		type = a;
	}
	void setSound()
	{
		System.out.println("Input "+type+"'s sound: ");
		String s = cin.next();
		sound = s;
	}
	String getType()
	{
		return type;
	}
	String getSound()
	{
		return sound;
	}
}
