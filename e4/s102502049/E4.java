package ce1002.e4.s102502049;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String DogSound;
		String CatSound;
		
		System.out.println("Input Dog's sound: "); // set sound
		DogSound = input.next();
		System.out.println("Input Cat's sound: ");
		CatSound = input.next();
		
		Dog dog = new Dog(); // objects
		Cat cat = new Cat();
		
		dog.setSound(DogSound); // setter
		cat.setSound(CatSound);
		
		System.out.println(dog.getType() + ": " + DogSound); // print type & sound
		System.out.print(cat.getType() + ": " + CatSound);
	}

}
