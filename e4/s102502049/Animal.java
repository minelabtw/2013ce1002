package ce1002.e4.s102502049;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public Animal(){
		
	}
	
	// set properties
	public void setType(String x) {
		type = x;
	}
	
	public void setSound(String x) {
		sound = x;
	}
	
	// get properties
	public String getType() {
		return type;
	}
	
	public String getSound() {
		return sound;
	}
	
}
