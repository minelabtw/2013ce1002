package ce1002.e4.s100502022;

public class Animal {
	private String type;
	private String sound;
	public void setType(String t){
		this.type=t;
	}
	public void setSound(String s){
		this.sound=s;
	}
	public String getType(){
		return type;
	}
	public String getSound(){
		return sound;
	}
}
