package ce1002.e4.s100502022;

import java.util.Scanner;

public class E4 {
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		//initial animals
		Cat cat = new Cat();
		Dog dog = new Dog();
		System.out.println("Input Dog's sound:");
		String dogsound = s.next();
		dog.setSound(dogsound);
		System.out.println("Input Cat's sound:");
		String catsound = s.next();
		cat.setSound(catsound);
		//output
		System.out.println("Dog: "+dog.getSound());
		System.out.println("Cat: "+cat.getSound());
	}
}
