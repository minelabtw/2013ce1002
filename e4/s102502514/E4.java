package ce1002.e4.s102502514;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		Dog dog = new Dog();  //新增一個Dog類別的物件dog
		Cat cat = new Cat();  //新增一個Cat類別的物件cat
		
		System.out.println("Input Dog's sound: ");
		String dogsound = input.next();
		dog.GetAnimalType();  //dog是父類別Animal的子類別,呼叫取得動物種類的方法
		dog.SetAnimalSound(dogsound);  //dog是父類別Animal的子類別,呼叫設定動物叫聲的方法
		
		dog.GetAnimalSound();
		System.out.println("Input Cat's sound: ");
		String catsound = input.next();
		cat.GetAnimalType();  //cat是父類別Animal的子類別,呼叫取得動物種類的方法
		cat.SetAnimalSound(catsound);  //cat是父類別Animal的子類別,呼叫設定動物叫聲的方法
		
		System.out.println("Dog: " + dog.GetAnimalSound());  //dog是父類別Animal的子類別,呼叫取得動物叫聲的方法
		System.out.println("Cat: " + cat.GetAnimalSound());  //cat是父類別Animal的子類別,呼叫取得動物叫聲的方法
		
		input.close();
	}
}
