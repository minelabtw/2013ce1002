package ce1002.e4.s102502514;

public class Animal {
	private String type;	//動物種類
	private String sound;	//動物叫聲
	
	public void SetAnimalType(String type){
		this.type = type;
	}
	
	public String GetAnimalType(){
		return type;
	}
	
	public void SetAnimalSound(String sound){
		this.sound = sound;
	}
	
	public String GetAnimalSound(){
		return sound;
	}
}
