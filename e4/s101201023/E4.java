package ce1002.e4.s101201023;

import java.util.Scanner;

public class E4 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		String Ds;
		String Cs;
		Scanner input = new Scanner(System.in);
		
		Dog dog = new Dog("Dog");
		Cat cat = new Cat("Cat");
		System.out.println("Input Dog's sound: ");
		Ds = input.next();
		System.out.println("Input Cat's sound: ");
		Cs = input.next();
		
		
		dog.setSound(Ds);                                              //將輸入的值傳入，並回傳值
		System.out.println(dog.getType()+": "+dog.getSound());
		cat.setSound(Cs);
		System.out.println(cat.getType()+": "+cat.getSound());
		
	}

}
