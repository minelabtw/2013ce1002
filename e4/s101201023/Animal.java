package ce1002.e4.s101201023;

public class Animal 
{
	private String type;
	private String sound;
	
	void setType(String T)
	{
		type = T;
	}
	
	void setSound(String S)
	{
		sound = S;
	}
	
	String getType()
	{
		return type;
	}
	
	String getSound()
	{
		return sound;
	}
}
