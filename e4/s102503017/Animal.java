package ce1002.e4.s102503017;

public class Animal {
	
	private String type;
	private String sound;
	
	Animal()
	{

	}
	//default constructor.
	
	Animal(String inputType, String inputSound)
	{
		type = inputType;
		sound = inputSound;		
	}
	//setting the type and the sound of the animal object with parameter constructor.
	void setType(String inputType)
	{
		type = inputType;
	}
	//set the type of object.
	void setSound(String inputSound)
	{
		sound = inputSound;
	}
	//set the sound of the object.
	String getType()
	{
		return type;
	}
	//get the type of the object.
	String getSound()
	{
		return sound;
	}
	//get the sound of the object.
}
