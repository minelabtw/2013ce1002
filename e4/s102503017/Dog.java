package ce1002.e4.s102503017;

public class Dog extends Animal 
{
	//same as Class Cat.
	
	Dog(String type)
	{
		super.setType(type);
			
	}
	
	void setType(String type)
	{
		super.setType(type);
	}
	
	void setSound(String sound)
	{
		super.setSound(sound);
	}
	
	String getType()
	{
		return super.getType();
	}
	
	String getSound()
	{
		return super.getSound();		
	}
}

