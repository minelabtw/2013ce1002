package ce1002.e4.s102503017;

public class Cat extends Animal 
{
	
	Cat(String type)
	{
		super.setType(type);		
	}
	//Setting type of the Cat with parameter constructor via superclass Animal.
	
	void setType(String type)
	{
		super.setType(type);
	}
	//Set type via superclass.
	void setSound(String sound)
	{
		super.setSound(sound);
	}
	//Set sound via superclass.
	String getType()
	{
		return super.getType();
	}
	//Get type via superclass.
	String getSound()
	{
		return super.getSound();		
	}
	//Get sound via superclass.
}
