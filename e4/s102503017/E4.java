package ce1002.e4.s102503017;

import java.util.Scanner;

public class E4 
{
	private static Scanner scanner;
	
	public static void main(String[] args)
	{
		scanner = new Scanner(System.in);
		
		String sound1, sound2;
		
		System.out.println("Input Dog's sound:");
		sound1 = scanner.nextLine();
		Dog dog = new Dog("Dog");
		dog.setSound(sound1);
		//grabbing dog's sound and create Dog's object.
		System.out.println("Input Cat's sound:");
		sound2 = scanner.nextLine();
		Cat cat = new Cat("Cat");
		cat.setSound(sound2);
		//grabbing cat's sound and create Cat's object.
		System.out.print(dog.getType() + " : " + dog.getSound() + "\n" + cat.getType() + " : " + cat.getSound());
		//output result.
		
		
	}

}
