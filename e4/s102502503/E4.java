package ce1002.e4.s102502503;
import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		String dogsound;  //dog's sound
		String catsound;  //cat's sound
		Scanner scanner = new Scanner(System.in);
		
		Dog dog = new Dog();
		Cat cat = new Cat();
		
		System.out.println("Input "+dog.getType()+"'s sound: ");  //output string
		dogsound = scanner.nextLine();  //input sound
		System.out.println("Input "+cat.getType()+"'s sound: ");
		catsound = scanner.nextLine();
		
		scanner.close();
		
		dog.setSound(dogsound);  //set sound
		cat.setSound(catsound);  //set sound
		
		System.out.println("Dog: "+dog.getSound());  //output
		System.out.println("Cat: "+cat.getSound());
	}

}
