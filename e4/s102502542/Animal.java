package ce1002.e4.s102502542;

public class Animal {
	private String type; // 動物種類
	private String sound; // 動物叫聲

	public Animal() {

	}

	public void setType(String a) {
		type = a;
	}

	public void setSound(String a) {
		sound = a;
	}

	public String getType() {
		return type;
	}

	public String getSound() {
		return sound;
	}
}
