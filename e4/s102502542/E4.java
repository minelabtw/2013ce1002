package ce1002.e4.s102502542;

import java.util.Scanner;

public class E4 {

	public static void main(String[] args) {
		Dog dog = new Dog();//設定建構式
		Cat cat = new Cat();
		Scanner input = new Scanner(System.in);
		System.out.println("Input Dog's sound:");
		String a = input.next();
		dog.setSound(a);
		System.out.println("Input Cat's sound:");
		String b = input.next();
		cat.setSound(b);
		//輸出題目所需
		System.out.println(dog.getType() + ": " + dog.getSound());
		System.out.println(cat.getType() + ": " + cat.getSound());
		// TODO Auto-generated method stub

	}

}
